<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// =========================== API Angular ==============================================
// Todos los controladores consumidos por peticiones Rest
// tienen como prefijo api/ y estan en el namespace Api
Route::prefix('v1')->as('api.v1.')->namespace('Api\V1')->group(function () {
    Route::get('/', function () {
        return response()->jsonSuccess([
            'message' => 'API v1.0 eGlow',
        ]);
    })->name('home');

    // JSON Web Token Authentication
    Route::post('auth/login', 'AuthController@login')->name('auth.login');

    Route::middleware('jwt.auth')->group(function () {
        // JSON Web Token Authentication
        Route::post('auth/refresh', 'AuthController@refresh')->name('auth.refresh');
        Route::get('auth/user', 'AuthController@user')->name('auth.user');

        // End Points Resources
        Route::resource('users', 'UserController', ['except' => ['create', 'edit']]);
    });
});
// ========================= End API Angular =============================================

Route::group(['prefix' => 'etl/twitter/influenciadores', 'namespace' => 'Etl\Twitter', 'middleware' => ['apiService']], function () {
    Route::post('{id}/perfil', 'InfluenciadorController@perfil')
        ->name('etl.twitter.influenciador.perfil');

    Route::post('{id}/top_tweets', 'InfluenciadorController@topTweets')
        ->name('etl.twitter.influenciador.top_tweets');

    Route::post('{id}/top_followers', 'InfluenciadorController@topFollowers')
        ->name('etl.twitter.influenciador.top_followers');

    Route::post('{id}/palabras_clave', 'InfluenciadorController@palabrasClave')
        ->name('etl.twitter.influenciador.palabras_clave');

    Route::post('{id}/hashtags', 'InfluenciadorController@hashtags')
        ->name('etl.twitter.influenciador.hashtags');

    Route::post('{id}/distribucion_genero', 'InfluenciadorController@distribucionGenero')
        ->name('etl.twitter.influenciador.distribucion_genero');

    Route::post('{id}/followers_activos', 'InfluenciadorController@followersActivos')
        ->name('etl.twitter.influenciador.followers_activos');

    Route::post('{id}/distribucion_pais', 'InfluenciadorController@distribucionPais')
        ->name('etl.twitter.influenciador.distribucion_pais');

    Route::post('{id}/comunidad/top_tweets', 'InfluenciadorComunidadController@topTweets')
        ->name('etl.twitter.influenciador.comunidad.top_tweets');

    Route::post('{id}/comunidad/hashtags', 'InfluenciadorComunidadController@hashtags')
        ->name('etl.twitter.influenciador.comunidad.hashtags');

    Route::post('{id}/comunidad/palabras_clave', 'InfluenciadorComunidadController@palabrasClave')
        ->name('etl.twitter.influenciador.comunidad.palabras_clave');
});

Route::group(['prefix' => 'etl/twitter/campana', 'namespace' => 'Etl\Twitter', 'middleware' => ['apiService']], function () {
    Route::post('{id}/top_tweets', 'CampanaController@topTweets')
        ->name('etl.twitter.campana.top_tweets');

    Route::post('{id}/palabras_clave', 'CampanaController@palabrasClave')
        ->name('etl.twitter.campana.palabras_clave');

    Route::post('{id}/distribucion_genero', 'CampanaController@distribucionGenero')
        ->name('etl.twitter.campana.distribucion_genero');

    Route::post('{id}/distribucion_pais', 'CampanaController@distribucionPais')
        ->name('etl.twitter.campana.distribucion_pais');

    Route::post('{id}/top_followers', 'CampanaController@topFollowers')
        ->name('etl.twitter.campana.top_followers');

    Route::post('{id}/hashtags', 'CampanaController@hashtags')
        ->name('etl.twitter.campana.hashtags');
});

Route::group(['prefix' => 'etl/twitter/publicaciones', 'namespace' => 'Etl\Twitter', 'middleware' => ['apiService']], function () {
    Route::post('/estadisticas', 'PublicacionesController@estadisticasdiezmin')
        ->name('etl.twitter.publicaciones.estadisticas10min');

    Route::post('{id}/estadisticas', 'PublicacionesController@estadisticas')
        ->name('etl.twitter.publicaciones.estadisticas');

    Route::post('{id}', 'PublicacionesController@update')
        ->name('etl.twitter.publicaciones.update');

    Route::post('{id}/replies', 'ReplyController@store')
        ->name('etl.twitter.campana.replies.store');

    Route::post('{id}/replies/distribucion-genero', 'ReplyController@distGenero')
        ->name('etl.twitter.publicacion.replies.distgenero');

    Route::post('{id}/replies/distribucion-pais', 'ReplyController@distPais')
        ->name('etl.twitter.publicacion.replies.distpais');

    Route::post('{id}/replies/perfiles', 'ReplyController@perfiles')
        ->name('etl.twitter.publicacion.replies.perfiles');

    // saving retweeters from backend
    Route::post('{id}/retweeters', 'RetweetersController@store')
        ->name('etl.twitter.publicacion.retweeters');

    // saving accounts from backend
    Route::post('{id}/retweeters/perfiles', 'RetweetersController@perfiles')
        ->name('etl.twitter.publicacion.retweeters.perfiles');
});

Route::group(['prefix' => 'etl/twitter', 'namespace' => 'Etl\Twitter'], function () {
    Route::post('comunidad/top-hashtags', 'EstadisticasController@topHashtagsComunidad')
        ->name('etl.twitter.comunidad.top-hashtags');

    Route::post('comunidad/top-keywords', 'EstadisticasController@topKeywordsComunidad')
        ->name('etl.twitter.comunidad.top-keywords');

    Route::post('accounts', 'AccountController@store')
        ->name('etl.twitter.account.store');
});

Route::group(['prefix' => 'etl/facebook', 'namespace' => 'Etl\Facebook', 'middleware' => ['apiService']], function () {
    Route::group(['prefix' => 'publicaciones'], function () {
        Route::post('{id}', 'PublicacionController@update');
        Route::post('{id}/statistics', 'PublicacionController@statistics');
        Route::post('{id}/comments', 'PublicacionController@comments');
        Route::post('{id}/reactions', 'PublicacionController@reactions');
    });

    Route::group(['prefix' => 'influenciadores'], function () {
        Route::post('{id}/perfil', 'InfluenciadorController@perfil');
        Route::post('{id}/followers', 'InfluenciadorController@followers');
        Route::post('{id}/hashtags', 'InfluenciadorController@hashtags');
        Route::post('{id}/keywords', 'InfluenciadorController@keywords');
        Route::post('{id}/posts', 'InfluenciadorController@posts');
        Route::post('posts/{id}/comments', 'InfluenciadorController@comments');
    });

    Route::group(['prefix' => 'campanas'], function () {
        Route::post('{id}/followers', 'CampanaController@followers');
        Route::post('{id}/hashtags', 'CampanaController@hashtags');
        Route::post('{id}/keywords', 'CampanaController@keywords');
    });

    Route::post('fb-users', 'UserController@store');
});
