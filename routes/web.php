<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('home');

// Sobreescribe la ruta de logout para que soporte get
Route::get('logout', function () {

    if (Auth::check()) {
        $isInfluencer = Auth::user()->isCelebridad();

        Auth::logout();
        request()->session()->flush();
        request()->session()->regenerate();

        if ($isInfluencer) {
            return redirect('influenciadores/login');
        }
    }

    return redirect('/');
});

Auth::routes();

Route::resource('servicios', 'ServiciosController');

Route::group(['prefix' => 'auth/twitter', 'namespace' => 'Auth'], function () {
    Route::get('login', 'TwitterController@redirectToProvider')->name('twitter.login');
    Route::get('logout', 'TwitterController@logout')->name('twitter.logout');
    Route::get('callback', 'TwitterController@handleProviderCallback')->name('twitter.callback');
    // Route::get('posttweet', 'TwitterController@postTweet')->name('twitter.post');
});

Route::group(['prefix' => 'auth/facebook', 'namespace' => 'Auth'], function () {
    Route::get('login', 'FacebookController@redirectToProvider')->name('facebook.login');
    Route::get('logout', 'FacebookController@logout')->name('facebook.logout');
    Route::get('callback', 'FacebookController@handleProviderCallback')->name('facebook.callback');
    Route::get('publicar', 'FacebookController@postTweet')->name('facebook.post');
});

Route::get('influenciadores/login', 'InfluenciadorController@login');

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => ['perfil']], function () {
        Route::get('/', 'RedirectController@index')->name('index');
        Route::get('palabrasclaves/searchbytext', 'PalabraClaveController@searchByText');
        Route::get('agencias/logotipo', 'AgenciaController@getLogotipo');
        Route::get('agencias/home', 'AgenciaController@home');
        Route::get('marcas/logotipo', 'MarcaController@getLogotipo');
        Route::get('marcas/byagencia', 'MarcaController@getByAgencia');
        Route::get('productos/imagen', 'ProductoController@getImagen');
        Route::get('productos/bymarca', 'ProductoController@getByMarca');

        Route::get('influenciadores/vincular/twitter', 'VincularController@twitterRedirectToProvider');
        Route::get('influenciadores/vincular/twittercallback', 'VincularController@twitterHandleProviderCallback');
        Route::get('influenciadores/vincular/facebook', 'VincularController@facebookRedirectToProvider');
        Route::get('influenciadores/vincular/instagram', 'VincularController@instagramRedirectToProvider');
        Route::get('influenciadores/vincular/youtube', 'VincularController@youtubeRedirectToProvider');
        Route::get('influenciadores/vincular/snapchat', 'VincularController@snapchatRedirectToProvider');


        Route::get('influenciadores/miscampanas/{id}/posts', 'InfluenciadorController@miscampanasposts');
        Route::get('influenciadores/miscampanas', 'InfluenciadorController@miscampanas');
        Route::get('influenciadores/miinfo', 'InfluenciadorController@miinfo');
        Route::get('influenciadores/searchbytext', 'InfluenciadorController@searchByText');
        Route::get('influenciadores/advancedsearch', 'InfluenciadorController@advancedSearch');
        // Route::get('influenciadores/sincronizacion', 'InfluenciadorController@sincronizacion');
        // Route::get('influenciadores/sincronizarjsons', 'InfluenciadorController@sincronizarjsons');
        // Route::get('influenciadores/sincronizarperfil/{id}', 'InfluenciadorController@sincronizarperfil');
        Route::get('influenciadores/catalogo', 'InfluenciadorController@catalogo');
        Route::get('profile/general/{id}', 'ProfileController@getGeneral');
        Route::get('profile/crecimiento/{id}', 'ProfileController@getCrecimientoSeguidores');
        Route::get('profile/topseguidores/{id}', 'ProfileController@getTopSeguidores');
        Route::get('profile/topword/{id}', 'ProfileController@getTopWord');
        Route::get('profile/hashtag/{id}', 'ProfileController@getHashtag');
    });

    Route::post('/password/change', 'Auth\ChangePasswordController@update');
    Route::get('usuarios/perfil', 'UsuarioController@perfil')->name('usuarios.perfil');
    Route::put('usuarios/updateperfil/{id?}', 'UsuarioController@updateperfil')->name('usuarios.updateperfil');
    Route::resource('usuarios', 'UsuarioController');
    Route::resource('palabrasclaves', 'PalabraClaveController');

    Route::resource('agencias', 'AgenciaController');
    Route::resource('marcas', 'MarcaController');
    Route::resource('productos', 'ProductoController');

    Route::resource('campanas', 'CampanaController');
    Route::get('campanas/{id}/terminos', 'CampanaController@downloadTerminos');
    Route::get('campanas/{id}/estadisticas', 'CampanaController@estadisticas');
    Route::get('campanas/{id}/graficas/top-locations', 'CampanaController@graficaTopLocations');
    Route::get('campanas/{id}/graficas/posts-per-hour', 'CampanaController@graficaPostsPerHour');
    Route::get('campanas/{id}/graficas/share-social', 'CampanaController@graficaShareSocial');
    Route::get('campanas/{id}/graficas/shares-twitter', 'CampanaController@graficaSharesTwitter');

    Route::post('influenciadores/clic', 'InfluenciadorController@clic');
    Route::post('influenciadores/aprobarpublicaciones', 'InfluenciadorController@aprobarpublicaciones');
    //Route::get('influenciadores/searchlugarnacimientobytext', 'InfluenciadorController@searchLugarNacimientoByText');
    //Route::get('influenciadores/searchpaisbytext', 'InfluenciadorController@searchPaisByText');
    Route::resource('influenciadores', 'InfluenciadorController');

    Route::get('influenciadores/{influeciadore}/{q}', 'InfluenciadorController@showCustom');

    // estadísticas influencer profile
    Route::get('influenciadores/{id}/graficas/crecimiento-followers', 'InfluenciadorController@crecimientoFollowers');
    Route::get('influenciadores/{id}/graficas/interacciones-mes', 'InfluenciadorController@interaccionesMensual');

    Route::post('publicaciones/publicar/{id?}', 'PublicacionController@publicar');
    Route::post('publicaciones/aprobar', 'PublicacionController@aprobar');
    Route::resource('publicaciones', 'PublicacionController');

    Route::post('publicacionesxcampana/aceptarpropuesta', 'PublicacionXInfluenciadorXCampanaController@aceptarpropuesta');
    Route::post('publicacionesxcampana/rechazarpropuesta', 'PublicacionXInfluenciadorXCampanaController@rechazarpropuesta');
    Route::delete('publicacionesxcampana/destroy/{id?}', 'PublicacionXInfluenciadorXCampanaController@destroy');
    Route::put('publicacionesxcampana/update/{id?}', 'PublicacionXInfluenciadorXCampanaController@update');

    Route::post('influenciadoresxcampana/aceptarpropuesta', 'InfluenciadorXCampanaController@aceptarpropuesta');
    Route::post('influenciadoresxcampana/rechazarpropuesta', 'InfluenciadorXCampanaController@rechazarpropuesta');
    Route::delete('influenciadoresxcampana/destroy/{id?}', 'InfluenciadorXCampanaController@destroy');
    Route::put('influenciadoresxcampana/update/{id?}', 'InfluenciadorXCampanaController@update');

    Route::resource('chat', 'ChatController');

    Route::resource('logeventos', 'LogeventoController', ['only' => ['index']]);
});
