/* globals $, config, urlAprobarPosts, urlAceptarPropuesta, errorAlert, swal */
'use strict';

$(document).ready(function() {
    $('.btn-aprobar-publicacion').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        var $this = $(this),
            cacheBtn = $this.html();

        $this.html(config.spinner);
        $this.tooltip('hide');

        swal({
            title: 'Aprobar Publicación',
            text: '¿Confirma que desea aprobar esta publicación? Después de aprobar la publicación no podrá realizar cambios al contenido.',
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            confirmButtonColor: config.color_red,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        },
        function(isConfirm){
            if (!isConfirm) {
                $this.html(cacheBtn);

                return true;
            }

            $.ajax({
                url: urlAprobarPublicacion,
                type: 'POST',
                dataType: 'json',
                data: {
                    id: $this.data('post'),
                },
            })
            .done(function() {
                swal({
                    title: 'Publicación aceptada exitosamente.',
                    type: 'success',
                    confirmButtonColor: config.color_red,
                    confirmButtonText: 'Aceptar',
                });

                // Restaura el icono inicial
                $this.html(cacheBtn);

                $this.removeClass('btn') // Elimina la clase btn para que el cursor no se convierta en pointer
                    .removeClass('animated') // Elimina la animación del botón
                    .off('click') // Elimina el evento click para que no se vuelva enviar
                    .tooltipster('content', 'Aprobado por Influenciador')
                    .find('.fa-hourglass-o') // Localiza el icono de reloj de arena
                    .removeClass('animated') // Elimina la animación del icono
                    .addClass('fa-thumbs-o-up') // Agrega el icono de buena
                    .removeClass('fa-hourglass-o') // Elimina el reloj de arena
                    .addClass('text-white') // Agrega fondo negro y texto blanco
                    .removeClass('btn-red-rounded'); // Elimina el estilo de botón

                $this.closest('.post-container').addClass('aprobado');
            }).
            fail(function() {
                swal('Error', 'Erorr al procesar los datos.', 'error');
                $this.html(cacheBtn);
            });
        });
    });

    /*$('#btn-aprobar-posts').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        var $this = $(this),
            cacheBtn = $this.html();

        $this.html(config.spinner);

        $.ajax({
            url: urlAprobarPosts,
            type: 'POST',
            dataType: 'json',
            data: {
                campana_id: $this.data('campana'),
            },
        })
        .done(function() {
            swal({
                title: 'Publicaciones aceptadas exitosamente.',
                type: 'success',
                confirmButtonColor: config.color_red,
                confirmButtonText: 'Aceptar',
            });

            $this.hide();
            $this.tooltip('hide');
            $('.btn-send-posts-influencer').hide();
        })
        .fail(function() {
            errorAlert('Error al procesar los datos.');
        })
        .always(function() {
            $this.html(cacheBtn);
            $('#motivo_rechazo_text').val('');
            $('#motivo-rechazo-modal').modal('hide');
        });
    });*/

    /*$('.btn-aceptar').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        $('#btn-send-aceptar').data('campana', $(this).data('campana'));

        $('#aceptar-propuesta-modal').modal('show');
    });

    $('#btn-send-aceptar').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        var $this = $(this),
            cacheBtn = $this.html();

        $this.html(config.spinner);

        $.ajax({
            url: urlAceptarPropuesta,
            type: 'POST',
            dataType: 'json',
            data: {
                campana_id: $this.data('campana'),
            },
        })
        .done(function() {
            swal({
                title: 'Propuesta aceptada exitosamente.',
                type: 'success',
                confirmButtonColor: config.color_red,
                confirmButtonText: 'Aceptar',
            },
            function() {
                // location.reload();
            });
        })
        .fail(function() {
            errorAlert('Error al procesar los datos.');
        })
        .always(function() {
            $this.html(cacheBtn);
            $('#aceptar-propuesta-modal').modal('hide');
        });
    });*/
});

//# sourceMappingURL=influenciador_posts.js.map
