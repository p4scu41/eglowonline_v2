

$(window).resize(function () {
    if (echartTopKeywordsCommunity != null) {
       echartTopKeywordsCommunity.resize();
    }

    if (echartTopHashtagsCommunity != null) {
       echartTopHashtagsCommunity.resize();
    }
});

    function graficasHastagKeyWordCommunity(){
         //grafias de palabras claves y hashtags
        if ($('#container-div-top-palabras-comunidad').length != 0) {
            var jsonTopPalabras = $('#container-div-top-palabras-comunidad').data('json');

            if (jsonTopPalabras.data.length != 0) {
                crearKeyWord('container-div-top-palabras-comunidad', jsonTopPalabras);
            }
        }

        if ($('#container-div-top-hashtags-comunidad').length != 0) {
            var jsonTopHashtags = $('#container-div-top-hashtags-comunidad').data('json');

            if (jsonTopHashtags.data.length != 0) {
                crearHashtang('container-div-top-hashtags-comunidad', jsonTopHashtags);
            }
        }
    }

    var echartTopKeywordsCommunity = null;
    var echartTopHashtagsCommunity = null;

    //Funcion que crea las graficas hashtags y palabra clave
    function crearHashtang(div, datos) {
        echartTopHashtags = echarts.init(document.getElementById(div), theme);
        // window.onresize = echartTopHashtags.resize;
          var options = {

            title: {
              text: 'Frecuencia Escritura',
              bottom: '3%',
              right: 'center',
              textStyle: {color: '#333', fontSize: 14}
              //subtext: 'Subtitle'
            },
            tooltip: {
                show:true,
              trigger: 'axis',
            },
            legend: {
              x: 220,
              y: 50,
              data: ''
            },
            toolbox: {
              show: false,
              feature: {
                saveAsImage: {
                  show: true,
                  title: "Save Image"
                }
              }
            },
            calculable: true,
            grid:{x2:150,x:10},
            yAxis: [{
              type: 'category',
              boundaryGap: false,
              data: datos.etiquetas,
              position:'right',
              nameTextStyle: {
                  color: '#f57c00'
              },
              axisLine:{show: false ,onZero:true },
              splitLine:{
                        onGap:false,
                        lineStyle:{
                            color: ['#D8D8D8'],
                            width: 23,
                            type: 'solid'}

                        },
              splitArea:{show: false},
              axisLabel:{
                  formatter:'{value}',
                  margin:13,

                      textStyle: {
                          color: '#0B0339',
                      }
              }
            }],
            xAxis: [{
              type: 'value',
              axisLine:{show:false}
            }],
            series: {
                name   : 'Menciones/Semana',
                type   : 'line',
                smooth : true,
                symbol : 'circles',
                step: false,
                itemStyle : {
                    normal : {
                        color: '#00BDC5',
                        borderColor: '#0B0339',
                        shadowColor: 'rgba(0, 0, 0, 0.5)',
                        shadowBlur: 10

                    }
                },
                 lineStyle : {
                    normal : {
                        color: 'rgba(138,221,45,0)',


                    }
                },

                data:  datos.data,


            }
          };

        echartTopHashtags.setOption(options);

        return echartTopHashtags;
    }

    function crearKeyWord(div, datos) {
        echartTopKeywords = echarts.init(document.getElementById(div), theme);
        // window.onresize = echartTopKeywords.resize;
          var options = {

            title: {
              text: 'Frecuencia Escritura',
              bottom: '3%',
              right: 'center',
              textStyle: {color: '#333', fontSize: 14}
              //subtext: 'Subtitle'
            },
            tooltip: {
                show: true,
              trigger: 'axis',
            },
            legend: {
              x: 220,
              y: 50,
              data: ''
            },
            toolbox: {
              show: false,
              feature: {
                saveAsImage: {
                  show: true,
                  title: "Save Image"
                }
              }
            },
            calculable: true,
            grid:{x2:150,x:10},
            yAxis: [{
              type: 'category',
              boundaryGap: false,
              data: datos.etiquetas,
              position:'right',
              nameTextStyle: {
                  color: '#f57c00'
              },
              axisLine:{show: false ,onZero:true },
              splitLine:{
                        onGap:false,
                        lineStyle:{
                            color: ['#D8D8D8'],
                            width: 23,
                            type: 'solid'}

                        },
              splitArea:{show: false},
              axisLabel:{
                  formatter:'{value}',
                  margin:13,

                      textStyle: {
                          color: '#0B0339',
                      }
              }
            }],
            xAxis: [{
              type: 'value',
              axisLine:{show:false}
            }],
            series: {
                name   : 'Menciones/Semana',
                type   : 'line',
                smooth : true,
                symbol : 'circles',
                step: false,
                itemStyle : {
                    normal : {
                        color: '#00BDC5',
                        borderColor: '#0B0339',
                        shadowColor: 'rgba(0, 0, 0, 0.5)',
                        shadowBlur: 10

                    }
                },
                 lineStyle : {
                    normal : {
                        color: 'rgba(138,221,45,0)',


                    }
                },

                data:  datos.data,


            }
          };

        echartTopKeywords.setOption(options);

        return echartTopKeywords;
    }

window.resize = function () {
    echartTopKeywords.resize();
    echartTopHashtags.resize();
};
//# sourceMappingURL=influenciador_comunidad_v2.js.map
