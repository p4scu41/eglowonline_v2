/* globals $, Handlebars, urlPalabraClaveSearchByText, errorAlert, swal, config, urlInfluenciadoresIndex, twttr */
'use strict';

$(document).ready(function() {
    var palabraClaveItemHbs = Handlebars.compile($('#palabra-clave-item-hbs').html()),
        $influenciadorForm = $('#influenciador-form'),
        $formFactura       = $('#formFactura'),
        selectedPage       = $('#selectedPage').val();

    $('#industria_id').selectToList({});
    $('#tipo_influenciador_id').selectToList({});
    $('#genero').selectToList({});
    $('#rango_etario_id').selectToList({});
    $('#notificar_x_correo').selectToList({});
    $('[name="usuario[activo]"]').selectToList({});

    $('#genero').change(function(event) {
        // Cuando Género No aplica
        if ($(this).val() == 3) {
            // Seleccionamos en Rango Etario No aplica
            $('#rango_etario_id').find('option[value="7"]').prop('selected', true);
            $('#div-rango-etario').find('.img_option').css('background-color','rgb(216, 216, 216)');
            $('#div-rango-etario').find('.img_option[data-value="7"]').css('background-color','rgb(11, 3, 57)');
        }
    });

    function gatherDataInfluenciador() {
        var datos = $influenciadorForm.serializeObject();

        // mandar los datos de la factura
        datos.factura = $formFactura.serializeObject();

        datos.palabras_clave = $('#palabra-clave-list li')
            .map(function(index, elem) {
                return $(elem).data('id');
            })
            .get();

        datos.profesiones = $('#profesion-list li')
            .map(function(index, elem) {
                return $(elem).data('id');
            })
            .get();

        datos.palabras_clave_otros = $('#container-marcas .relacion_marca')
            .map(function(indexRelacion, elemRelacion) {
                return $('#relacion_marca_'+$(elemRelacion).data('id')+' .palabra-clave-item')
                    .map(function(indexPalabra, elemPalabra) {
                        return {
                            palabra_id:  $(elemPalabra).data('id'),
                            relacion_id: $(elemRelacion).data('id')
                        };
                    })
                    .get();
            })
            .get();

        datos.redes_sociales = $('.red_social_tbl')
            .map(function(indexRedSocial, elemRedSocial) {
                var $tblRedSocial = $('#tabla_red_social_' + $(elemRedSocial).data('id')),
                    pageId        = selectedPage === '0' ? $tblRedSocial.find('input[name="pageId"]:checked').val() : null;

                return {
                        id:  $(elemRedSocial).data('id'),
                        pageId: pageId,
                        perfil: {
                            screen_name:         $tblRedSocial.find('[name="screen_name"]').val(),
                            cantidad_seguidores: $tblRedSocial.find('[name="cantidad_seguidores"]').val(),
                            precio_publicacion:  $tblRedSocial.find('[name="precio_publicacion"]').val()
                        }
                    };
            })
            .get();

        datos.redes_sociales_to_delete = $('[name="delete_red_social_id"]')
            .map(function(indexRedSocial, elemRedSocial) {
                if ($(elemRedSocial).val() !== '') {
                    return $(elemRedSocial).val();
                }
            })
            .get();

        return datos;
    }

    function sendDataInfluenciador($btn) {
        var cacheBtn = $btn.html();

        $btn.html(config.spinner);

        $.ajax({
            url: $influenciadorForm.attr('action'),
            type: $influenciadorForm.attr('method'),
            dataType: 'json',
            data: gatherDataInfluenciador(),
            beforeSend: function () {
                $('#loading').modal('show');
            }
        })
        .done(function(response) {
            $('#loading').modal('hide');

            swal({
                    title: 'Datos guardados exitosamente.',
                    type: 'success',
                    confirmButtonColor: config.color_red,
                    confirmButtonText: 'Aceptar',
                },
                function() {
                    if (response.data.redirect) {
                        window.location.href = urlInfluenciadoresIndex;
                    }
                });
        })
        .fail(function(jqXHR) {
            $('#loading').modal('hide');
            errorAlert('Error al procesar los datos. ' + jqXHR.responseJSON.message);
        })
        .always(function() {
            $btn.html(cacheBtn);
        });
    }

    function validateForm($form) {
        if ($form.length == 0) {
            return true;
        }

        if ( !$form.valid() ) {
            errorAlert('Revise sus datos, algo esta mal.');
            return false;
        }

        // Se valida el screen_name de Twitter
        // se utiliza la librería twitter-text-js
        // https://github.com/twitter/twitter-text/tree/master/js
        var twitter_account   = $('[name="screen_name"][data-redsocial="twitter"]').val(),
            precioPublicacion = $('[name="precio_publicacion"][data-redsocial="twitter"]').val();

        /*if ($('#tab_redes_sociales').hasClass('active') && twitter_account == '') {
            errorAlert('Debe de escribir una cuenta de Twitter.');
            $('a[href="#tab_redes_sociales"]').trigger('click');
            return false;
        }

        if ($('#tab_redes_sociales').hasClass('active') && precioPublicacion == '') {
            errorAlert('Debe de escribir un precio de publicación.');
            $('a[href="#tab_redes_sociales"]').trigger('click');
            return false;
        }*/

        if (twitter_account !== '') {
            if (!twttr.txt.isValidUsername(twitter_account)) {
                errorAlert('El nombre de usuario de Twitter no es válido.');
                $('a[href="#tab_redes_sociales"]').trigger('click');
                return false;
            }
        }

        if ($('#genero').val() != 3 && ($('#rango_etario_id').val() == 7 || $('#rango_etario_id').val() == '')) {
            errorAlert('Debe seleccionar su Rango Etario.');
            return false;
        }

        // evento para validar la selección de paginas, marcalos como obligados
        if (selectedPage === '0') {
            let atLeastOneSelected = false;
            $('#tab_redes_sociales').find('input[name="pageId"]').each(function (index) {
                var $parent = $(this).parents('div.iradio_square-blue');

                if ($parent.hasClass('checked')) {
                    atLeastOneSelected = true;
                    return;
                }
                // if ($(this).is(':checked')) {
                //     return true;
                // }
            });

            if (!atLeastOneSelected) {
                errorAlert('Debe seleccionar una página de facebook para continuar');
                $('a[href="#tab_redes_sociales"]').trigger('click');
                return false;
            }
        }

        return true;
    }

    $influenciadorForm.validate({
        debug: true,
        errorElement: 'span',
        errorClass: 'help-block',
        ignore: '.ignore',
        highlight: function(element) {
            $(element)
                .closest('.form-group')
                .addClass('has-error')
                .removeClass('has-success');
        },
        unhighlight: function(element) {
            $(element)
                .closest('.form-group')
                .removeClass('has-error');
        },
        submitHandler: function(form) {
            if (validateForm($(form))) {
                sendDataInfluenciador($('.btn-save-influencer'));
            }
        },
        invalidHandler: function(event, validator) {
            if (validator.errorList.length > 0) {
                $('a[href="#'+$(validator.errorList[0].element).closest('.tab-pane').attr('id')+'"]').trigger('click');
                $('body').scrollTop( $(validator.errorList[0].element).scrollTop() );
            }
        },
        success: function(label) {}
    });

    $formFactura.validate({
        debug: true,
        errorElement: 'span',
        errorClass: 'help-block',
        ignore: '.ignore',
        highlight: function(element) {
            $(element)
                .closest('.form-group')
                .addClass('has-error')
                .removeClass('has-success');
        },
        unhighlight: function(element) {
            $(element)
                .closest('.form-group')
                .removeClass('has-error');
        },
    });

    $('.btn-save-influencer').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        if (validateForm($influenciadorForm) && validateForm($formFactura)) {
            sendDataInfluenciador($(this));
        }
    });

    $('.btn-next-to').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        if (validateForm($influenciadorForm)) {
            $( $(this).data('tab') ).trigger('click');
        }
    });

    $('.btn-delete-red-social').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        var button = $(this);

        swal({
            title:              '¿Estás seguro que deseas eliminar tus datos?',
            type:               'warning',
            confirmButtonColor: config.color_blue,
            confirmButtonText:  'Aceptar',
            showCancelButton:   true,
            cancelButtonColor:  config.color_red,
            closeOnConfirm:     false,
        },
        function() {
            button
                .siblings('input[name="delete_red_social_id"]')
                .val(button.data('id'));

            button
                .parents('div.panel-body')
                .find('input.form-control')
                .val('');

            swal('Datos eliminados', 'La red social actual se ha eliminado de tu perfil. Presiona GUARDAR para confirmar los cambios.', 'success');
        });
    });

    $('#palabra-clave-list, .container-item-list, #profesion-list').on('click', '.btn-delete', function(event) {
        event.stopPropagation();
        event.preventDefault();

        $(this).closest('li').fadeOut('slow', function() {
            $(this).remove();
        });
    });

    $('#palabra_clave_search').select2({
        placeholder: 'Intereses',
        language: 'es',
        allowClear: true,
        minimumInputLength: 3,
        ajax: {
            url: urlPalabraClaveSearchByText,
            data: function (params) {
                params.palabra_clave = true;

                return params;
            },
            processResults: function(result) {
                return {
                    results: $.map(result, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.palabra,
                        };
                    })
                };
            }
        },
    })
    .on('select2:select', function(e) {
        // Detecta si la palabra clave ya esta en la lista de seleccionados
        if ($('#palabra-clave-list li[data-id=' + e.params.data.id + ']').length === 0) {
            $('#palabra-clave-list').append(palabraClaveItemHbs(e.params.data));
        }

        $(this).val(null).trigger('change');
    });

    $('.relacion_marca_search').select2({
        placeholder: 'Escribe una marca',
        language: 'es',
        allowClear: true,
        minimumInputLength: 3,
        ajax: {
            url: urlPalabraClaveSearchByText,
            data: function (params) {
                params.related_marca = true;

                return params;
            },
            processResults: function(result) {
                return {
                    results: $.map(result, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.palabra,
                        };
                    })
                };
            }
        },
    })
    .on('select2:select', function(e) {
        var id = $(e.currentTarget).data('id');

        // Detecta si la palabra clave ya esta en la lista de seleccionados
        if ($('#relacion_marca_'+id+' li[data-id=' + e.params.data.id + ']').length === 0) {
            $('#relacion_marca_'+id+'').append(palabraClaveItemHbs(e.params.data));
        }

        $(this).val(null).trigger('change');
    });

    $('#profesion_search').select2({
        placeholder: 'Profesiones',
        language: 'es',
        allowClear: true,
        minimumInputLength: 3,
        ajax: {
            url: urlPalabraClaveSearchByText,
            data: function (params) {
                params.profesion = true;

                return params;
            },
            processResults: function(result) {
                return {
                    results: $.map(result, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.palabra,
                        };
                    })
                };
            }
        },
    })
    .on('select2:select', function(e) {
        // Detecta si la profesion ya esta en la lista de seleccionados
        if ($('#profesion-list li[data-id=' + e.params.data.id + ']').length === 0) {
            $('#profesion-list').append(palabraClaveItemHbs(e.params.data));
        }

        $(this).val(null).trigger('change');
    });

    // evento para colorear los íconos de las redes sociales
    // $('a.panel-heading').on('click', function () {
    //     var hasClass = $(this).siblings('div.panel-collapse').hasClass('in');

    //     hasClass ? $(this).children('span.circle').css('background', '#676767') : $(this).children('span.circle').css('background', '#020833');
    // });

    // evento para validar url de tagsinput
    $('#website').on('beforeItemAdd', function (event) {
        // let url = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;
        let url = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$";

        if (!event.item.match(url)) {
            swal('Por favor escriba una URL válida');
            event.cancel = true;
        }
    });
});
//# sourceMappingURL=influenciador_form.js.map
