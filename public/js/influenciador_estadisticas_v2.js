/* globals $, errorAlert, swal, echarts, config */
'use strict';
var echartsProfessions            = null,
    echartFollowersMonthlyGrowing = null,
    echartsReachment              = null,
    echartCountryDistribution     = null,
    echartInteractions            = null

$(document).ready(function () {
    let graphLoading                  = '<span><i class="fa fa-spinner fa-spin fa-2x"></i> Cargando gráfica ...</span>',
        $profileStatistics            = $('#profile-statistics'),
        $graphReachment               = $('#graph-reachment'),
        $graphCountryDist             = $('#graph-country-distribution'),
        $graphProfessions             = $('#graph-professions'),
        alcanceReal                   = $graphReachment.data('real'),
        alcancePotencial              = $graphReachment.data('potencial'),
        countryTexts                  = $graphCountryDist.data('texts'),
        countryValues                 = $graphCountryDist.data('values'),
        professionsData               = $graphProfessions.data('elements'),
        colors                        = ['#0B0339', '#92D5F7', '#6B649B', '#00BDC5', '#544991'];

    $profileStatistics.on('click', function () {
        // builds graphic for followers monthly growing
        loadGraphics(graphLoading, 'followers-monthly-growing');

        // builds graphics for interactions by day of the week
        loadGraphics(graphLoading, 'interactions');

        // timeout for correct rendering
        setTimeout(function () {
            // builds graphics for real and potential reachment
            buildReachmentGraphics(alcanceReal, alcancePotencial);

            // builds graphics for country distribution
            buildCountryDistributionGraphics(countryTexts, countryValues, colors);

            // builds graphics for professions
            buildProfessionsGraphics(professionsData, colors);
        }, 1500);
    });

    // add resize event
    $(window).on('resize', function(){
        if (echartFollowersMonthlyGrowing !== null)
            echartFollowersMonthlyGrowing.resize();

        if (echartsReachment !== null)
            echartsReachment.resize();

        if (echartInteractions !== null)
            echartInteractions.resize();

        if (echartCountryDistribution !== null)
            echartCountryDistribution.resize();

        if (echartsProfessions !== null)
            echartsProfessions.resize();
    });
});

/**
 * loads data for building top locations graphics
 *
 * @param {string} graphLoading
 */
function loadGraphics(graphLoading, containerId) {
    let $graphContainer  = '',
        functionCallback = '',
        influencerId     = null,
        url              = '',
        ajax             = null;

    switch (containerId) {
        case 'followers-monthly-growing':
            $graphContainer  = $('#graph-followers-monthly-growing');
            functionCallback = 'buildFollowersMonthlyGrowing';
            break;

        case 'reachment':
            $graphContainer  = $('#graph-reachment');
            functionCallback = 'buildReachmentGraphics';
            break;

        case 'interactions':
            $graphContainer  = $('#graph-interactions');
            functionCallback = 'buildInteractionsGraphics';
            break;

        case 'professions':
            $graphContainer  = $('#graph-professions');
            functionCallback = 'buildProfessionsGraphics';
            break;
    }

    influencerId = $graphContainer.data('id');
    url          = $graphContainer.data('url');
    ajax         = loadData(url, graphLoading, $graphContainer);

    ajax.done(function(response) {
        $graphContainer.html('');
        $('#total').text('Total interactions: ' + response.total);
        if (response.status === 'success') {
            window[functionCallback](response)
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        $graphContainer.html('<p class="no-data text-center">No existe información para graficar</p>');
        console.log(textStatus + ': ' + errorThrown);
    })
}

/**
 * loads data from server
 *
 * @param {string} url
 * @param {string} graphLoading
 * @param {Object} $graphContainer
 */
function loadData(url, graphLoading, $graphContainer) {
    return $.ajax({
        url:        url,
        type:       'GET',
        dataType:   'json',
        beforeSend: function () {
            $graphContainer.html(graphLoading);
        }
    });
}

/**
 * builds followers monthly growing graphics using echarts
 *
 * @param {Object} response
 */
function buildFollowersMonthlyGrowing (response) {
    let followersMonthlyGrowingData  = {
            color: ['#92D5F7'],
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                orient: 'vertical',
                x: 'right',
                y: 'middle',
                data: response.categories
            },
            grid: {
                left: '10%',
                right: '18%',
                bottom: '10%',
                containLabel: true
            },
            xAxis: response.xAxis,
            yAxis: {
                type:  'value',
                scale: true,
                max:   response.max,
                min:   response.min,
                name: 'Followers',
                nameLocation: 'middle',
                nameGap: 70
            },
            series: response.series
        };

    echartFollowersMonthlyGrowing = echarts.init(document.getElementById('graph-followers-monthly-growing'));
    echartFollowersMonthlyGrowing.setOption(followersMonthlyGrowingData);
}

/**
 * builds shares social graphics on echarts
 *
 * @param {float} alcanceReal
 * @param {float} alcancePotencial
 */
function buildReachmentGraphics (alcanceReal, alcancePotencial) {
    let reachmentData  = {
            tooltip : {
                trigger: 'item',
                formatter: function (params) {
                    return params.name +' '+
                        params.value.formater() +' '+
                        '(' + params.percent  +'%)';
                }//"{a} <br/>{b}: {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                x: 'right',
                y: 'middle',
                data: ['Real', 'Potential']
            },
            series: [{
                name: 'Reachment',
                type: 'pie',
                radius: '50%',
                center: ['50%', '60%'],
                data: [{
                    name: 'Real',
                    value: alcanceReal,
                    itemStyle: {
                        normal: {
                            color: '#322774'
                            // borderWidth: 100
                        }
                    }
                }, {
                    name: 'Potential',
                    value: alcancePotencial,
                    itemStyle: {
                        normal: {
                            color: '#6B649B'
                            // borderWidth: 100
                        }
                    }
                }],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }]
        };

    echartsReachment = echarts.init(document.getElementById('graph-reachment'));
    echartsReachment.setOption(reachmentData);
}

/**
 * builds interactions by day of the week (influencer and followers) graphics using echarts
 *
 * @param {Object} response
 */
function buildInteractionsGraphics (response) {
    let interactionsData  = {
            color: ['#92D5F7'],
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                orient: 'vertical',
                x: 'right',
                y: 'middle',
                right: 0,
                data: response.categories
            },
            grid: {
                left: '3%',
                right: '28%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: response.data,
            yAxis: {
                type:  'value'
            },
            series: response.series
        };

    echartInteractions = echarts.init(document.getElementById('graph-interactions'));
    echartInteractions.setOption(interactionsData);
}

/**
 * builds top location graphics
 *
 * @param {Array} countryTexts
 * @param {Array} countryValues
 * @param {Array} colors
 */
function buildCountryDistributionGraphics(countryTexts, countryValues, colors) {
    let newValues = [];

    if (countryValues.length === 0) {
        $('#graph-country-distribution').html('<p class="no-data text-center">No existe información para graficar</p>');
        return;
    }

    for (let i = 0; i < countryValues.length; i++) {
        newValues.push({
            value: countryValues[i],
            itemStyle: {
                normal: {
                    color: colors[i],
                }
            }
        })
    }

    let countryDistributionsData  = {
        tooltip: {
            trigger: "axis",
            formatter: "{a} <br/>{b}: {c}%"
        },
        grid: {
            left: '3%',
            right: '10%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: {
            show: false,
        },
        yAxis: {
            type: "category",
            data: countryTexts,
            splitLine: {
                show: false
            }
        },
        series: [{
            name: "Top Locations",
            type: "bar",
            data: newValues,
            label: {
                normal: {
                    show: true,
                    position: 'right',
                    formatter: '{c}%',
                }
            },
        }]
    };

    echartCountryDistribution = echarts.init(document.getElementById('graph-country-distribution'));
    echartCountryDistribution.setOption(countryDistributionsData);
}

/**
 * builds top professions graphic
 *
 * @param {Object} data
 * @param {Array} colors
 */
function buildProfessionsGraphics(data, colors) {
    let newValues  = [],
        categories = [];

    if ($.isEmptyObject(data)) {
        $('#graph-professions').html('<p class="no-data text-center">No existe información para graficar<p>');
        return;
    }

    for (let i = 0; i < data.length; i++) {
        categories.push(data[i].palabra);
        newValues.push({
            name: data[i].palabra,
            value: data[i].cantidad,
            itemStyle: {
                normal: {
                    color: colors[i]
                }
            }
        })
    }
    let professionsData    = {
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b}: {d}%"
            },
            legend: {
                orient: 'vertical',
                x: 'right',
                y: 'middle',
                data: categories
            },
            series: [{
                name: 'Profesiones',
                type: 'pie',
                radius: ['50%', '80%'],
                avoidLabelOverlap: false,
                data: newValues,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '20',
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
            }]
        };

    echartsProfessions = echarts.init(document.getElementById('graph-professions'));
    echartsProfessions.setOption(professionsData);
}
//# sourceMappingURL=influenciador_estadisticas_v2.js.map
