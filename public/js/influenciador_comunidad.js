$(document).ready(function() {
    var hashtags = null,
        keywords = null;

    // construir gráfica hashtags
    $.ajax({
        url            : $('#wordcloudHashtags').data('url'),
        type           : 'POST',
        dataType       : 'json',
        data           : {
            influenciadorId: $('#influenciadorId').val()
        },
        beforeSend     : function () {
            $('#cargarGraficaHashtags').removeClass('hide');
        }
    }).
    done(function (respuesta) {
        $('#cargarGraficaHashtags').addClass('hide');

        if (respuesta.estatus === 'OK') {
            // draw graphic hashtags
            $('#wordcloudHashtags').removeClass('hide');
            $('#wordcloudHashtags').jQCloud(respuesta.words);

            $('[data-toggle="tooltip"]').tooltip();
        }

        if (respuesta.estatus === 'fail') {
            $('#mensajeNoEncontradoHashtags').removeClass('hide');
        }
    }).
    fail(function (jqXHR, textStatus, errorThrown) {
        $('#cargarGraficaHashtags').addClass('hide');
        console.log(textStatus + ': ' + errorThrown);
    });

    // construir gráfica palabras clave
    $.ajax({
        url            : $('#wordcloudPalabrasClave').data('url'),
        type           : 'POST',
        dataType       : 'json',
        data           : {
            influenciadorId: $('#influenciadorId').val()
        },
        beforeSend     : function () {
            $('#cargarGraficaTopKeywords').removeClass('hide');
        }
    }).
    done(function (respuesta) {
        $('#cargarGraficaTopKeywords').addClass('hide');

        if (respuesta.estatus === 'OK') {
            // draw graphic hashtags
            // $('#wordcloudPalabrasClave').removeClass('hide');
            // $('#wordcloudPalabrasClave').jQCloud(respuesta.words);

            $('[data-toggle="tooltip"]').tooltip();
        }

        if (respuesta.estatus === 'fail') {
            $('#mensajeNoEncontradoKeywords').removeClass('hide');
        }
    }).
    fail(function (jqXHR, textStatus, errorThrown) {
        $('#cargarGraficaTopKeywords').addClass('hide');
        console.log(textStatus + ': ' + errorThrown);
    });

    // función que grafica en forma de puntos
    function crearDot(div, datos) {
        var echartLine = echarts.init(document.getElementById(div), theme);
          echartLine.setOption({
            title: {
              text: datos.titulo,
              //subtext: 'Subtitle'
            },
            tooltip: {
                show:true,
              trigger: 'axis',
            },
            legend: {
              x: 220,
              y: 50,
              data: datos.legend
            },
            toolbox: {
              show: true,
              feature: {
                saveAsImage: {
                  show: true,
                  title: "Save Image"
                }
              }
            },
            calculable: true,
            grid:{x2:150,x:10},
            yAxis: [{
              type: 'category',
              boundaryGap: false,
              data: datos.etiquetas,
              position:'right',
              axisLine:{show:false,onZero:true},
              splitLine:{onGap:false,lineStyle:{color: ['#aaa'],width: 30,type: 'solid'}},
              splitArea:{show:false},
              axisLabel:{formatter:'{value}',margin:13}
            }],
            xAxis: [{
              type: 'value',
              axisLine:{show:false}
            }],
            series: datos.series
          });

        return echartLine;
    }
});
//# sourceMappingURL=influenciador_comunidad.js.map
