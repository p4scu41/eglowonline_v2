/* globals $, errorAlert, swal, echarts, config */
'use strict';

$(document).ready(function () {
    let graphLoading = '<span><i class="fa fa-spinner fa-spin fa-2x"></i> Cargando gráfica ...</span>',
        $showTrends  = $('#showTrends');

    $showTrends.on('click', function () {
        // builds graphic for locations
        loadGraphics(graphLoading, 'top-locations');

        // builds graphics for posts per hour
        loadGraphics(graphLoading, 'posts-per-hour');

        // builds graphics for shares social
        loadGraphics(graphLoading, 'share-social');

        // builds graphics for shares on twitter
        loadGraphics(graphLoading, 'share-twitter');
    });

    // loadSharesOfSocial();

    // loadSharesOnTwitter();
});

/**
 * loads data for building top locations graphics
 *
 * @param {string} graphLoading
 */
function loadGraphics(graphLoading, containerId) {
    let $graphContainer  = '',
        functionCallback = '',
        campanaId        = 0,
        url              = '',
        ajax             = null;

    switch (containerId) {
        case 'top-locations':
            $graphContainer  = $('#graph-top-locations');
            functionCallback = 'buildTopLocationsGraphics';
            break;

        case 'posts-per-hour':
            $graphContainer  = $('#graph-posts-per-hour');
            functionCallback = 'buildPostsPerHourGraphics';
            break;

        case 'share-social':
            $graphContainer  = $('#graph-share-social');
            functionCallback = 'buildShareSocialGraphics';
            break;

        case 'share-twitter':
            $graphContainer  = $('#graph-shares-twitter');
            functionCallback = 'buildSharesTwitterGraphics';
            break;

        default:
            // code
    }

    campanaId = $graphContainer.data('id');
    url       = $graphContainer.data('url');
    ajax      = loadData(url, graphLoading, $graphContainer);

    ajax.done(function(response) {
        $graphContainer.html('');
        if (response.status === 'success') {
            window[functionCallback](response)
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        $graphContainer.html('No existe información para graficar');
        console.log(textStatus + ': ' + errorThrown);
    })
}

/**
 * loads data from server
 *
 * @param {string} url
 * @param {string} graphLoading
 * @param {Object} $graphContainer
 */
function loadData(url, graphLoading, $graphContainer) {
    return $.ajax({
        url:        url,
        type:       'GET',
        dataType:   'json',
        beforeSend: function () {
            $graphContainer.html(graphLoading);
        }
    });
}

/**
 * builds top location graphics
 *
 * @param {Object} response
 */
function buildTopLocationsGraphics(response) {
    let echartTopLocation = echarts.init(document.getElementById('graph-top-locations')),
        topLocationsData  = {
        tooltip: {
            trigger: "item"
        },
        toolbox: {
            show: true,
            feature: {
                saveAsImage: {
                    show: true,
                    title: "Descargar"
                }
            }
        },
        grid: {
            left: '3%',
            right: '10%',
            bottom: '3%',
            containLabel: true
        },
        calculable: true,
        xAxis: [{
                show: false,
            }
        ],
        yAxis: [{
            type: "category",
            data: response.yAxis
        }],
        series: [{
            name: "Top Locations",
            type: "bar",
            data: response.data,
            label: {
                normal: {
                    show: true,
                    position: 'right'
                }
            },
        }]
    };

    echartTopLocation.setOption(topLocationsData);
}

/**
 * builds post per hour graphics using echarts
 *
 * @param {Object} response
 */
function buildPostsPerHourGraphics(response) {
    let echartPostsPerHour = echarts.init(document.getElementById('graph-posts-per-hour')),
        postsPerHourData  = {
        tooltip : {
            trigger: 'axis'
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: response.xAxis,
            axisLabel:{
                interval: 1,
                textStyle: {
                    fontSize: 10
                }
            }
        },
        yAxis: {
            type:  'value',
        },
        series: [{
            name: 'Posts Per Hour',
            type: 'line',
            lineStyle: {
                normal: {
                    color: '#92D5F7'
                }
            },
            data: response.data
        }]
    };

    echartPostsPerHour.setOption(postsPerHourData);
}

/**
 * builds shares social graphics on zingcharts
 *
 * @param {Object} response
 */
function buildShareSocialGraphics(response) {
    let echartsSharesSocial = echarts.init(document.getElementById('graph-share-social')),
        sharesSocialData  = {
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            x: 'right',
            data: response.categories
        },
        series: [{
            name: 'Shares of Social',
            type: 'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    show: true,
                    textStyle: {
                        fontSize: '20',
                    }
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data: response.data
        }]
    };

    echartsSharesSocial.setOption(sharesSocialData);
}

/**
 * builds shares on twitter graphic using zingchar
 *
 * @param {Object} response
 */
function buildSharesTwitterGraphics (response) {
    let echartsSharesTwitter = echarts.init(document.getElementById('graph-shares-twitter')),
        sharesTwitterData    = {
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            x: 'center',
            data: response.categories,
            top: 230
        },
        series: [{
            name: 'Shares on Twitter',
            type: 'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    show: true,
                    textStyle: {
                        fontSize: '20'
                    }
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data: response.data
        }]
    };

    echartsSharesTwitter.setOption(sharesTwitterData);
}

/**
 * gets and buils json for printing the graphics
 *
 * @param {string} textCenter
 * @param {array} colors for the graphic
 * @param {array} texts labels
 * @param {array} values the values for the labels
 * @param {string} valueBox formatter for printing
 * @param {array} jsonTextCenter tooltips when mouse scroll
 * @param {string|null} tooltips
 */
function jsonChartDonut(textCenter, colors, texts, values, valueBox, jsonTexCenter, tooltips) {
    let textValueBox     = valueBox || "%t",
        configTextCenter = jsonTexCenter || {
          "text": textCenter,
          "font-size": "15",
          "x": "25%",
          "y": "25%"
        },
        series = [];

    for (let i = 0; i < texts.length; i++) {
        series.push({
            text: texts[i],
            "values": [values[i]],
            "background-color": colors[i],
            marker: {
		        backgroundColor: colors[i]
			}
        });
    }

    console.log(series);

    return {
        type: "ring",
        plot: {
            slice: "50%",
            borderWidth: 0,
            animation:{
                "effect": "2",
                sequence: "3"
            },
            valueBox: {
                type: 'all',
                placement:"out",
                text: textValueBox
            },
            tooltip: {
                fontSize: 12,
                text: ($(tooltips).length) ? "%data-formula" : "%v",
                backgroundColor: config.color_lightgray,
                borderRadius: "10px",
                padding: "10%",
                callout: true,
                short:true, //
                decimals:0,
            },
        },
        series: series
    };
}
//# sourceMappingURL=campana_trends.js.map
