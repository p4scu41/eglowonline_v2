/* global jQuery */
'use strict';

(function ( $ ) {
    $.fn.selectToList = function( options ) {
        var settings = $.extend({
            multiple      : false,
            tooltip       : true,
            width         : '60px',
            height        : '60px',
            container     : '<div class="col-xs-12 col-sm-12 col-md-12"></div>',
            activeColor   : '#0b0339',
            inactiveColor : '#D8D8D8'
        }, options );

        var select        = this,
            maincontainer = select.parent(),
            container     = $(settings.container);

		if (!select.is('select')) {
			console.log('selectToList: El elemento no es de tipo select. ' + select.attr('id'));
			return select;
		}

        // Toma el valor multiple del select
        settings.multiple = select.prop('multiple');

        // Oculta el objeto select
        select.addClass('irs-hidden-input');
        select.addClass('hidden');

		maincontainer.append(container);

        select.find('option').each(function(){
            var valor  = $(this).prop('value'),
                imagen = $(this).data('imagen'),
                texto  = $(this).text(),
                activo = $(this).prop('selected');

			if (valor != '') {
				imagen = $('<img data-active="0" '+
                    'class="img-rounded img-thumbnail img_option img-select" '+
                    'style="display:inline; margin:10px; padding:8px; cursor:pointer; '+
                        'width:'+settings.width+'; height:'+settings.height+'; '+
                        'border-radius:10px; background-color:' +
                            (activo ? settings.activeColor : settings.inactiveColor)+';" '+
                    'data-value="'+valor+'" '+
                    'src="'+imagen+'" '+
                    ((settings.tooltip) ? 'data-toggle="tooltip" title="'+texto+'"' : '')+'>');

                container.append(imagen);

				// Agrega tooltip a las imagenes
				if (settings.tooltip){
					$(imagen).tooltip();
				}

				$(imagen).click(function() {
					var activo = $(this).data('active'),
                        valor  = $(this).data('value');

                    // Si no es multiple, quita el selected a todas las otras opciones
					if (!settings.multiple) {
						$(this).parent().find('.img_option').not(this).each(function() {
							$(this).css({'background-color': settings.inactiveColor});
							$(this).data('active', 0);
							select.find('option[value='+valor+']').prop('selected', false);
						});
					}

					if (activo == 0) {
						$(this).css({'background-color': settings.activeColor});
						$(this).data('active', 1);
						select.find('option[value='+valor+']').prop('selected', true);
					} else {
						$(this).css({'background-color': settings.inactiveColor});
						$(this).data('active', 0);
						select.find('option[value='+valor+']').prop('selected', false);
					}

                    select.trigger('change');
                    select.trigger('focusout');
				});
			}
		});

		return select;
    };
}( jQuery ));
