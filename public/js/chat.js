/* globals $, config, urlChat, errorAlert */
'use strict';

$(document).ready(function() {
    $('.btn-chat').click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        var $this = $(this),
            cacheBtn = $this.html();

        // Agrega los metadatos al botón de enviar mensaje
        $('#btn-send-message-chat')
            .data('usuario', $this.data('usuario'))
            .data('campana', $this.data('campana'))
            .data('influenciador', $this.data('influenciador'));

        $this.html(config.spinner);

        $.ajax({
            url: urlChat,
            type: 'GET',
            data: {
                usuario_id: $this.data('usuario'),
                campana_id: $this.data('campana'),
            },
        })
        .done(function(response) {
            var messages = '';

            if (response.data.length > 0) {
                for (var i = 0; i < response.data.length; i++) {
                    messages += '<div class="row '+response.data[i].tipo_usuario+'">' +
                        '<div class="col-xs-10 col-sm-10 col-md-10 '+response.data[i].tipo_usuario+'">' +
                            response.data[i].contenido +
                            '<div class="date text-right">'+response.data[i].fecha+'</div>'+
                        '</div>' +
                    '</div>';
                }
            }

            $('#list-message-chat').html(messages);
            $('#chat-modal').modal('show');
        })
        .fail(function(jqXHR) {
            errorAlert('Error al procesar los datos. ' + jqXHR.responseJSON.message);
        })
        .always(function() {
            $this.html(cacheBtn);
        });
    });

    $('#btn-send-message-chat').click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        if ($('#message_chat').val() === '') {
            errorAlert('Debe escribir el mensaje a enviar.');
            return false;
        }

        var $this = $(this),
            cacheBtn = $this.html();

        $this.html(config.spinner);

        $.ajax({
            url: urlChat,
            type: 'POST',
            data: {
                usuario_id: $this.data('usuario'),
                campana_id: $this.data('campana'),
                influenciador_id: $this.data('influenciador'),
                asunto: 'Mensaje',
                contenido: $('#message_chat').val(),
                leido: 0,
            },
        })
        .done(function(response) {
            $('#message_chat').val('');

            if (response.status === 'success') {
                swal({
                    title: 'El mensaje se envío de manera exitosa.',
                    type: 'success',
                    confirmButtonColor: config.color_red,
                    confirmButtonText: 'Aceptar',
                });
            }
        })
        .fail(function(jqXHR) {
            errorAlert('Error al procesar los datos. ' + jqXHR.responseJSON.message);
        })
        .always(function() {
            $this.html(cacheBtn);
            $('#chat-modal').modal('hide');
        });
    });
});

//# sourceMappingURL=chat.js.map
