'use strict';

(function () {

    //Clases de CSS
    const BG_BLUE_CLASS_CSS = 'blue';
    const BG_GREEN_CLAS_CSS = 'green';

    //Tipos de alerta disponible
    const ALERT_OPS = 'OPS';
    const ALERT_OK = 'OK';
    const ALERT_CONFIRM = 'CONFIRM';
    const ALERT_WARNING = 'WARNING';

    const URI_IMAGES = app_url + '/img/alert-eglow';

    //Iconos correspondientes a los tipos de alertas
    const ICON_OPS = `${URI_IMAGES}/icon_ops.png`;
    const ICON_OK = `${URI_IMAGES}/icon_happy.png`;
    const ICON_CONFIRM = `${URI_IMAGES}/icon_monkey_with_eyes_close.png`;
    const ICON_WARNING = `${URI_IMAGES}/icon_warning.png`;

    //Titulos de los diferentes tipos de alertas disponibles
    const TITLE_OPS = 'Oops!';
    const TITLE_OK = 'Yes, you did it';
    const TITLE_CONFIRM = 'Oh no';
    const TITLE_WARNING = 'WARNING';

    /**
      * Formas de utilizar los diferentes tipos de alerta
     *
     * Alerta OPS:
     * showAlertOps({message: 'Please check your information. </br> Something went wrong!'});
     *
     * Alerta OK:
     * showAlertOk({message: 'Please check your information. </br> Something went wrong!'});
     *
     * Alerta confirm:
     * showAlertConfirm({message: 'Helo world'});
     *
     * Alerta warning:
     * showAlertWarning({message: 'Encontramos un problema, favor de verificar su conexión a intener!'});
     *
     * Alerta persolizada
     * showAlert({
     *      type: 'OPSsse',
     *      message: 'My message. </br> Chao!',
     *      title: 'MI TITULO',
     *      icon: './images/alert-eglow/icon_monkey_with_eyes_close.png',
     *      buttons: [{
     *              text: 'Cerrar'
     *          },{
     *              text: 'Intenta otra vez',
     *              action: function() {
     *                  alert('okkkkk');
     *             }
     *          }]
     * });
     */

    /**
     * Función para mostrar una alerta personalizada, todos los campos a personalizar se envían en el parametro otpions(JSON)
     * @param {Json} options Recibe un json donde los atributos son: type:string, message:string, title:string, icon:string, buttons:Array, ejemplo de un objeto de bottom: {text: "texto", action: cuerpo_de_la_funcion, theme: green o blue}
     */
    function showAlert(options) {
        if (!(typeof options.type !== 'undefined')) {
            console.error('Es necesario ingresar un type de alertas');
        } else {
            switch (options.type) {
                case TITLE_OPS: break;
                case TITLE_OK: break;
                case TITLE_CONFIRM: break;
                case TITLE_WARNING: break;
                default:
                    console.error('El type es invalido');
                    return;
            }

            let buttonHtmlRender = '';
            let iconHtmlRender = '';
            getHtmlButtons(options.buttons).
                then(buttonHtml => {
                    if (buttonHtml != null) buttonHtmlRender = buttonHtml;
                    return getIcon(options.icon, options.type);
                })
                .then(icon => {
                    iconHtmlRender = icon;
                    return getTitle(options.title, options.type);
                })
                .then(title => {
                    eglowAlert(iconHtmlRender, title, options.message, buttonHtmlRender);
                })
                .catch(error => {
                    console.log('ocurrio un error', error);
                })
        }
    }

    /**
     * Se muestra una alerta de tipo Oops
     * @param {Json} options El Json options, necesita como unico parametro requerdo la propiedad message
     */
    function showAlertOps(options) {
        if (!(typeof options.message !== 'undefined')) {
            console.error('Es necesario enviar el parametro message');
            return;
        }
        let buttonsDefault = [{ text: 'Close', }];
        if (typeof options.buttons !== 'undefined') {
            buttonsDefault = options.buttons;
        }
        let data = {
            title: options.title,
            message: options.message,
            type: ALERT_OPS,
            buttons: buttonsDefault,
            icon: ICON_OPS
        }
        buildAlertDefault(data);
    }

    /**
     * Se muestra una alerta de tipo Confirm
     * @param {Json} options El Json options, necesita como unico parametro requerdo la propiedad message
     */
    function showAlertConfirm(options) {
        if (!(typeof options.message !== 'undefined')) {
            console.error('Es necesario enviar el parametro message');
            return;
        }
        let buttonsDefault = [];
        if (typeof options.buttons !== 'undefined') {
            buttonsDefault = options.buttons;
        }
        let data = {
            title: options.title,
            message: options.message,
            type: ALERT_CONFIRM,
            buttons: buttonsDefault,
            icon: ICON_CONFIRM
        }
        buildAlertDefault(data);
    }

    /**
     * Se muestra una alerta de tipo Warning
     * @param {Json} options El Json options, necesita como unico parametro requerdo la propiedad message
     */
    function showAlertWarning(options) {
        if (!(typeof options.message !== 'undefined')) {
            console.error('Es necesario enviar el parametro message');
            return;
        }
        let buttonsDefault = [{ text: 'Try Again', }];
        if (typeof options.buttons !== 'undefined') {
            buttonsDefault = options.buttons;
        }
        let data = {
            title: options.title,
            message: options.message,
            type: ALERT_WARNING,
            buttons: buttonsDefault,
            icon: ICON_WARNING
        }
        buildAlertDefault(data);
    }

    /**
     * Se muestra una alerta de tipo OK
     * @param {Json} options El Json options, necesita como unico parametro requerdo la propiedad message
     */
    function showAlertOk(options) {
        if (!(typeof options.message !== 'undefined')) {
            console.error('Es necesario enviar el parametro message');
            return;
        }
        let buttonsDefault = [{ text: 'Great', theme: BG_GREEN_CLAS_CSS }];
        if (typeof options.buttons !== 'undefined') {
            buttonsDefault = options.buttons;
        }
        let data = {
            title: options.title,
            message: options.message,
            type: ALERT_OK,
            buttons: buttonsDefault,
            icon: ICON_OK
        }
        buildAlertDefault(data);
    }

    /**
     * Este método se encarga de obtener los datos necesarios para formar la alerta de diferentes tipos
     * @param {Json} options En el paratro options, se reciben a través de sus atributos los campos de title, message, type, buttons, icon
     */
    function buildAlertDefault(options) {
        let buttonHtmlRender = '';
        let iconHtmlRender = '';
        getHtmlButtons(options.buttons)
            .then(buttonHtml => {
                if (buttonHtml != null) buttonHtmlRender = buttonHtml;
                return getIcon(options.icon, options.type);
            })
            .then(icon => {
                iconHtmlRender = icon;
                return getTitle(options.title, options.type);
            })
            .then(title => {
                eglowAlert(iconHtmlRender, title, options.message, buttonHtmlRender);
            })
            .catch(error => {
                console.log('el error es:', error);
            });
    }

    /**
     *
     * @param {string} icon Recibe la URI del icono que se desea mostrar en la alerta
     * @param {string} title Titulo de la alerta
     * @param {string} message  Mensaje que se desea visualizar en la alerta
     * @param {Array} buttons Lista de botones que quieren que se visualicen en la alerta
     */
    function eglowAlert(icon, title, message, buttons) {
        if (!(typeof message !== 'undefined')) {
            message = '';
        }
        let htmlContent = `
        <div class="icon-close-alert" onclick="closeAlert()">x</div>
        <figure class="fg-icon-alert">
            <img src="${icon}"/>
        </figure>
        <h3 class="alert-title color-blue">${title}</h3>
        <div class="container">
            <div class="row>
                <div class="col-md-8">
                    <p class="message-alert">${message}<p>
                </div>
            </div>
        </div>
        <div class="btn-sections">
            ${buttons}
        </div>
    `;

        swal({
            html: htmlContent,
            showConfirmButton: false,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('#swal-input1').val(),
                        $('#swal-input2').val()
                    ])
                })
            },
            onOpen: function () {
                $('#swal-input1').focus()
            }
        }).then(result => {
            console.log('resultado:', result);
        }).catch(swal.noop);
        ;
    }

    /**
     * Función que recibe una lista de botones y regresa el HTML para poder realizar el render
     * @param {Array} buttons Lista de botones a visualizar
     */
    function getHtmlButtons(buttons) {
        return new Promise((resolve, reject) => {

            if (!(typeof buttons !== 'undefined')) {
                resolve(null);
            }

            let htmlButtons = '';
            let i = 1;
            buttons.forEach(function (element) {
                let modulo = i % 2; i++;
                let bgButton = null
                if (typeof element.theme !== 'undefined') {
                    bgButton = element.theme;
                } else {
                    bgButton = modulo == 1 ? BG_BLUE_CLASS_CSS : BG_GREEN_CLAS_CSS;
                }
                if (!(typeof element.action !== 'undefined')) {
                    htmlButtons = htmlButtons + `<div class="${bgButton} btn-eglow" onclick="javascript:typeof(closeAlert())">${element.text}</div>`
                } else {
                    htmlButtons = htmlButtons + `<div class="${bgButton} btn-eglow" onclick="javascript:typeof(${element.action}) == 'function' ? ${element.action}() : ${element.action}">${element.text}</div>`
                }
            });
            resolve(htmlButtons);
        });
    }

    /**
     * Función que devuele el icono a mostrar en la alerta
     * @param {string} icon URI del icono
     * @param {string} type Tipo de alerta
     */
    function getIcon(icon, type) {
        return new Promise((resolve, reject) => {
            if (typeof icon !== 'undefined') {
                resolve(icon);
            } else {
                switch (type) {
                    case ALERT_OPS:
                        resolve(ICON_OPS);
                        break;
                    case ALERT_OK:
                        resolve(ICON_OK);
                        break;
                    case ALERT_CONFIRM:
                        resolve(ICON_CONFIRM);
                        break;
                    case ALERT_WARNING:
                        resolve(ICON_WARNING);
                        break;
                }
            }
        });
    }

    /**
     * Regresa el título que visalizará en la alerta
     * @param {string} title Titulo de la alerta
     * @param {string} type Tipo de la alerta
     */
    function getTitle(title, type) {
        return new Promise((resolve, reject) => {
            if (!(typeof title !== 'undefined')) {
                switch (type) {
                    case ALERT_OPS:
                        resolve(TITLE_OPS);
                        break;
                    case ALERT_OK:
                        resolve(TITLE_OK);
                        break;
                    case ALERT_CONFIRM:
                        resolve(TITLE_CONFIRM);
                        break;
                    case ALERT_WARNING:
                        resolve(TITLE_WARNING);
                        break;
                }
            } else {
                resolve(title);
            }
        });
    }

    /**
     * Cierra la alerta
     */
    function closeAlert() {
        swal.close();
    }

    window.alertEglow = {
        ok: showAlertOk,
        ops: showAlertOps,
        confirm: showAlertConfirm,
        warning: showAlertWarning,
        custom: showAlert
    }
})();

function closeAlert() {
    swal.close();
}
//# sourceMappingURL=alert-eglow.js.map
