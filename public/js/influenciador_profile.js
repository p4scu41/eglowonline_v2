/* globals $, config, urlInfluenciadoresIndex, twttr */
'use strict';
// https://blog.zingchart.com/2016/01/20/4-tips-better-charts-zingchart/
// https://blog.zingchart.com/2016/05/09/make-a-custom-tooltip/?q=make%20a%20custom%20tooltip%20in%20zingchart%20using%20functions
ZC.customFn = {};  //Global wrapper to ZingChart

/**
 * Function under our ZC wrapper. Will
 * be fired when the node is hovered over
 *
 * @param {object} p ZingChart callback argument.
 * is associated to various graph properties
 */
ZC.customFn.myfunc = function(p) {
    console.log(p);

    return {
        text: 'hola',
    }
  /*var value = globalArray[p.nodeindex];
  var textValue = "%" + value;
  var color = "red";

  if (p.nodeindex === 0) //if januaray display no text
    textValue = "";

  if (value > 0)
    color = "green";

  return {
    text: textValue,
    fontColor: color
  }*/

  /*console.log(p);
  var plotIndex = p.plotindex;
  var nodeIndex = p .nodeindex;
  var hashId = 'p:' + plotIndex + 'n:' + nodeIndex;
  var tooltipText = "";

  // Simple logic, if exists grab the value. Else let user
  // know its the first time
  if (tooltipHistoryHash[hashId]) {

    // Get the time elapsed since the last hover
    var elapsed = Math.floor( (new Date().getTime() - tooltipHistoryHash[hashId]) / 1000);
    tooltipText = "Last Time Hovered: ";

    // If it's less than 0, they hovered less than a second ago
    if (elapsed === 0) {
      tooltipText += "less than a second ago!";
    } else {
      tooltipText += elapsed.toString() + " seconds ago";
    }

  } else {
    tooltipText = "First Time Hovering!";
  }

  // Assign into associative array (hash)
  tooltipHistoryHash[hashId] = new Date().getTime();

  // Return and object with multiple properties
  return {
    text : tooltipText,
    backgroundColor : "#222"
  }*/
};

function zingchartRender(id, data) {
    zingchart.render({
        id: id,
        data: data,
        height: "100%",
        width: "100%",
        output : 'canvas'
    });
}

function echartRender(id, data) {
    if (document.getElementById(id) != null) {
        var echarGraph = echarts.init(document.getElementById(id));
        echarGraph.setOption(data);
    }
}

function jsonChartSeguidores($chartSeguidores) {
    return {
        tooltip: {
            trigger: "item"
        },
        toolbox: {
            show: true,
            feature: {
                saveAsImage: {
                    show: true,
                    title: "Descargar"
                }
            }
        },
        grid: {
            left: '3%',
            right: '10%',
            bottom: '3%',
            containLabel: true
        },
        calculable: true,
        xAxis: [{
                show: false,
            }
        ],
        yAxis: [{
                type: "category",
                data: $chartSeguidores.data('texts')
            }
        ],
        series: [{
                name: "Seguidores",
                type: "bar",
                data: $chartSeguidores.data('values'),
                itemStyle: {
                    normal: {
                        color: config.color_blue
                    }
                },
                label: {
                    normal: {
                        show: true,
                        position: 'right',
                        formatter: function (params) {
                            return helper.formatNumber(params.value);
                        }
                    }
                },
            },
        ]
    };

    /*var plotarea = {
            "margin-left": "65px",
        };

    if ($chartSeguidores.data('texts').length > 1) {
        plotarea = {
            "margin-left": "65px",
            "margin-bottom": "0",
            "margin-top": "10",
        };
    }

    return {
        type: "hbar",
        "plotarea": plotarea,
        plot: {
            "animation":{
                "effect":"11",
                "method":"3",
                "sequence":"ANIMATION_BY_PLOT_AND_NODE",
                "speed":8
            },
            valueBox: {
                text: "%v",
                "short":true, //
                "decimals":0,
            },
            tooltip: {
                text: "%t %v",
                "font-color": "black",
                "font-size": 18,
                "short":true, //
                "decimals":0,
                "background-color": config.color_lightgray,
                "border-radius":"9px",
                "padding": "10%",
                "callout": true
            }
        },
        scaleX: {
            labels: $chartSeguidores.data('texts'),
            //width: 10,
        },
        scaleY: {
            visible: false,
        },
        series: [{
            backgroundColor: config.color_blue,
            values: $chartSeguidores.data('values'),
            text: 'Seguidores'
        }]
    };*/
}

function jsonChartCrecimientoFollowers($chartCrecimientoFollowers) {
    return {
        type: "bar",
        plot:{
            "animation":{
                "effect":"11",
                "method":"3",
                "sequence":"ANIMATION_BY_PLOT_AND_NODE",
                "speed":8
            },
            stacked: true,
            stackType: "normal",
            tooltip: {
                text: "%t %v",
                "font-color": "black",
                "font-size": 18,
                "background-color": config.color_lightgray,
                "border-radius":"9px",
                "padding": "10%",
                "callout": true
            }
        },
        scaleX: {
            labels: ["Ene","Feb","Mar","Abr"], // "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"
            tick: { visible: false },
            alpha: 0 // Oculta la linea del eje
        },
        scaleY: {
            "ref-line": {
              "line-width": "2px",
              "line-color": config.color_blue,
              "visible": true
            },
            "ref-value": -1,
            guide: { visible: false },
            item: { visible: false },
            tick: { visible: false },
            alpha: 0 // Oculta la linea del eje
        },
        series: [
            {
                text: "Twitter",
                values:[20, 40, 25, 50]
            },
            {
                text: "Facebook",
                values:[-5, 30, 21, 18]
            },
            {
                text: "Youtube",
                values:[30, 5, 18, 21]
            }
        ]
    };
}

function jsonChartDonut(textCenter, texts, values, valueBox, jsonTexCenter,tooltips) {
    var textValueBox = valueBox || "%t\n%v";
    var configTextCenter = jsonTexCenter || {
          "text": textCenter,
          "font-size": "22",
          "x": "45%",
          "y": "45%"
        };

    return {
        "type": "ring",
        "plotarea":{
            "margin-top":"1%"
        },
        "scale-r": {
            "short":true, //
            "decimals":0,
        },
        "scale": {
            "short":true, //
            "decimals":0,
        },
        "labels":[
            configTextCenter,
          ],
        "plot": {
            "slice": "50%",
            "animation":{
                "effect":"2",
                "method":"3",
                "sequence":"ANIMATION_BY_PLOT",
                "speed":"ANIMATION_FAST"
            },
            "value-box": {
                //jsRule: "ZC.customFn.myfunc",
                "font-color": "black",
                "placement":"out",
                "text": textValueBox,
                "short":true, //
                "decimals":0,
            },
            "tooltip": {
                "text": ($(tooltips).length) ? "%data-formula" : "%v",
                "font-color": "black",
                "background-color": config.color_lightgray,
                "border-radius": "10px",
                "padding": "10%",
                "callout": true,
                "short":true, //
                "decimals":0,
            },
        },
        "series": [
            {
                text: texts[0],
                "values": [values[0]],
                "background-color": config.color_lightgray,
                "detached":true,
                "data-formula":($(tooltips).length) ? tooltips[0] : ""
            },
            {
                text: texts[1],
                "values": [values[1]],
                "background-color": config.color_blue,
                "detached":true,
                "data-formula":($(tooltips).length) ? tooltips[1] : ""
            },
        ]
    };

    // echarts
    /*return {
          title: {
            text: textCenter,
            x: 'center',
            y: 'center',
            textStyle : {
                color : 'rgba(30,144,255,0.8)',
                fontSize : 35,
                fontWeight : 'bolder'
            }
        },
        tooltip: {
            trigger: 'item',
            formatter: "{b} {c}K"
        },
        series: [{
            type:'pie',
            radius: ['50%', '70%'],
            data:[
                {value:values[0], name: texts[0]},
                {value:values[1], name: texts[1]},
            ]
        }]
    };*/
}

function jsonChartEngagementRate($chartEngagementRate,tooltip) {
    return {
        type: "hbar",
        "plotarea": {
            "margin-left": "65px",
        },
        plot: {
            valueBox: {
                text: "%vK"
            },
            tooltip: {
                text: "%data-formula",
                "font-color": "black",
                "font-size": 14,
                "background-color": config.color_lightgray,
                "border-radius":"9px",
                "padding": "10%",
            }
        },
        scaleX: {
            labels: [
                'Última Campaña'.replace(/\s+/g, '\n'),
                'Otra Campaña'.replace(/\s+/g, '\n'),
                'Nombre completo campaña'.replace(/\s+/g, '\n')
            ],
            width: 10,
        },
        scaleY: {
            visible: false,
        },
        series: [{
            backgroundColor: config.color_blue,
            values: [100, 120, 80],
            text: 'Engagement',
            "data-formula":"Fórmula: Total Interacciones / Total Followers del Influencer"
        }]
    };
}

function jsonChartProporcionPais($chartProporcionPais) {
    return {
        tooltip: {
            trigger: "item",
            formatter: '{a} <br />{b0}: {c0}%',
        },
        toolbox: {
            show: true,
            feature: {
                saveAsImage: {
                    show: true,
                    title: "Descargar"
                }
            }
        },
        grid: {
            left: '0%',
            right: '0%',
            top: '0%',
            bottom: '18%',
            containLabel: true
        },
        calculable: true,
        xAxis: [{
                type: "category",
                data: $chartProporcionPais.data('texts'),
                axisLabel: {
                    rotate: 90
                }/*,
                axisPointer: {
                    label: {
                        formatter: function (params) {
                            return params.value
                                + (params.seriesData.length ? '：' + params.seriesData[0].data + '%' : '');
                        }
                    }
                }*/
            }
        ],
        yAxis: [{
                show: false,
            }
        ],
        series: [{
                name: "Paises",
                type: "bar",
                data: $chartProporcionPais.data('values'),
                itemStyle: {
                    normal: {
                        color: config.color_blue
                    }
                },
                label: {
                    normal: {
                        show: true,
                        position: 'top',
                        formatter: function (params) {
                            return params.value + '%';//helper.formatNumber(params.value);
                        }
                    }
                },
            },
        ]
    };
    /*return {
        type: "bar",
        "plotarea": {
            "margin-left": "0",
            "margin-top": "0",
        },
        plot: {
            valueBox: {
                text: "%v"
            },
            tooltip: {
                text: "%kl %v",
                "font-color": "black",
                "font-size": 18,
                "background-color": config.color_lightgray,
                "border-radius":"9px",
                "padding": "10%",
                "callout": true
            }
        },
        scaleX: {
            labels: $chartProporcionPais.data('texts'),
            "item":{
              "angle":90
            }
        },
        scaleY: {
            visible: false,
        },
        series: [{
            backgroundColor: config.color_blue,
            values: $chartProporcionPais.data('values'),
        }]
    };*/
}

$(document).ready(function() {
    var $chartSeguidores = $('#chartSeguidores'),
        // $chartCrecimientoFollowers = $('#chartCrecimientoFollowers'),
        $chartAlcances = $('#chartAlcances'),
        //echartInitAlcances = echarts.init(document.getElementById('chartAlcances')),
        $chartEngagementRate = $('#chartEngagementRate'),
        $chartProporcionGenero = $('#chartProporcionGenero'),
        $chartProporcionPais = $('#chartProporcionPais');

    // zingchartRender($chartSeguidores.attr('id'), jsonChartSeguidores($chartSeguidores));
    echartRender($chartSeguidores.attr('id'), jsonChartSeguidores($chartSeguidores));

    // zingchartRender($chartCrecimientoFollowers.attr('id'), jsonChartCrecimientoFollowers($chartCrecimientoFollowers));

    /*echartInitAlcances.setOption(jsonChartDonut(
        $chartAlcances.data('potencial')+'K',
        ['Potencial', 'Real'],
        [$chartAlcances.data('potencial'), $chartAlcances.data('real')]
    ));*/
    zingchartRender($chartAlcances.attr('id'),
        jsonChartDonut(
            ' ', // helper.formatNumber($chartAlcances.data('potencial'))
            ['Potencial','Real'],
            [$chartAlcances.data('potencial'), $chartAlcances.data('real')], null, null,
            ["Fórmula: Total Followers + Total Followers de sus Followers",
            "Fórmula: Total Followers"]
        )
    );

    zingchartRender($chartEngagementRate.attr('id'), jsonChartEngagementRate($chartEngagementRate));

    //zingchartRender($chartProporcionPais.attr('id'), jsonChartProporcionPais($chartProporcionPais));
    echartRender($chartProporcionPais.attr('id'), jsonChartProporcionPais($chartProporcionPais));

    zingchartRender($chartProporcionGenero.attr('id'),
        jsonChartDonut(
            $chartProporcionGenero.data('followerstwitter') > 0 ?
                Math.round(($chartProporcionGenero.data('hombres')+$chartProporcionGenero.data('mujeres'))/$chartProporcionGenero.data('followerstwitter')*100) :
                '',
            ['Hombres', 'Mujeres'],
            [$chartProporcionGenero.data('hombres'), $chartProporcionGenero.data('mujeres')],
            "%t\n%npv%",
            {
              "text": $chartProporcionGenero.data('followerstwitter') > 0 ?
                Math.round(($chartProporcionGenero.data('hombres')+$chartProporcionGenero.data('mujeres'))/$chartProporcionGenero.data('followerstwitter')*100) + '%':
                '',
              "font-size": "16",
              "x": "46%",
              "y": "46%"
            }
        )
    );

    //Codigo para agregar funcionalidad a los botones custom export de las graficas
    /*$('.btn_custom_export').click(function(){
        var graficaId = $(this).data('target');
        var tipo = $(this).data('type');
        console.log(graficaId);
        console.log(tipo);
        switch(tipo){
            case "png":

                zingchart.exec(graficaId, 'saveasimage');
                var sImageData = zingchart.exec(graficaId, 'getimagedata', {
                    format : 'png'
                });

                console.log(sImageData);
                break;
            case "print":
                zingchart.exec(graficaId, 'print');
                break;

            case "pdf":

                break;
        }
    });*/
});

//# sourceMappingURL=influenciador_profile.js.map
