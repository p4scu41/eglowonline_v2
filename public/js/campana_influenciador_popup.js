/*  globals $ */
'use strict';

$(document).ready(function () {
    // agregar fancybox para mostrar las estadísticas de influenciadores en un pop up
    $('#list-influenciadores-resultado-busqueda').on('click', 'a.link-perfil-influencer', function (event) {
        event.preventDefault();

        let url = $(this).attr('href') + '/q';

        $.fancybox({
			'padding'		: 0,
			'autoScale'		: false,
			'transitionIn'	: 'none',
			'transitionOut'	: 'none',
			'title'			: this.title,
			'width'		    : 800,
			'height'		: 600,
			'href'			: url,
			'type'			: 'iframe'
		});
    });
});
//# sourceMappingURL=campana_influenciador_popup.js.map
