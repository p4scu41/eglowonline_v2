var config = {
    // Habilita el seguimiento de errores
    'debug': false,
    // Icono que se muestra en los botones para indicar que se esta procesando la solicitud
    spinner: '<i class="fa fa-pulse fa-spinner fa-lg"></i>',
    spinner_input: '<i class="fa fa-pulse fa-spinner fa-lg loading-indicator"></i>',
    // Determina si las vistas se cargarán por Ajax
    load_ajax_view: false,
    color_red: '#00bdc5',
    color_blue: '#4E6185',
    color_lightgray: '#D8D8D8',
    color_green: '#00D800',
    color_twitter: '#5BEDEA',
    color_facebook: '#3D5A98',
    color_instagram: '#DCD264',
    color_youtube: '#FF4A4A',
    color_retweets: '#25023E',
    color_favorites: '#4D0282',
    color_replies: '#8C54B4',
    grafica_newbie : '#D8D8D8',
    grafica_amateur: '#4E6185',
    grafica_pro : '#544991',
    grafica_expert:'#322774',
    grafica_mreglow: '#00bdc5',
    grafica_inactivos: '#C68BE1'
};
/*global $, base_url, config, errorAlert, yii, tinyMCE */
'use strict';

var helper = {
    /**
     * Rellena por la izquierda con un caracter hasta llegar a cierta longitud
     *
     * @param {string} nr  Cadena a rellenar
     * @param {string} n   Longitud a completar
     * @param {string} str Caracter a insertar, default 0
     *
     * @return {string} Cadena rellenada por la izquierda
     */
    padLeft: function (nr, n, str) {
        return Array(n-String(nr).length+1).join(str||'0')+nr;
    },

    /**
     * Compara si un valor es undefined o null
     *
     * @param {mixed} val Valor a evaluar
     *
     * @return {boolean}
     */
    isEmpty: function (val) {
        return (typeof(val) === 'undefined' || val === null || val === '');
    },

    tinymceInit: function (selector) {
        selector = selector || 'textarea';

        tinyMCE.init({
            selector: selector,
            language: 'es_MX',
            plugins: [
                'responsivefilemanager advlist autolink lists link image charmap preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'responsivefilemanager | preview media | forecolor backcolor',
            image_advtab: true,
            //relative_urls: false,
            //document_base_url: urlBase,

            external_filemanager_path: base_url + 'filemanager/',
            filemanager_title: 'Archivos',
            filemanager_sort_by: 'name',
            external_plugins: {'filemanager': base_url + 'js/plugins/tinymce/plugins/responsivefilemanager/plugin.min.js'}
        });

        $('textarea').parents('form').on('beforeValidate', function() {
            tinyMCE.triggerSave();
        });
    },

    /**
     * Cargar las vistas por ajax en el modal modalAjaxView
     * @param  event event
     * @return void
     */
    load_ajax_view: function(event, modal) {
        event.preventDefault();
        event.stopPropagation();

        modal = modal || '#modalAjaxView';
        var $this = $(this),
            cache = $this.html();

        $this.html(config.spinner);

        $.get($(this).attr('href'), function(data) {
            $(modal + ' .modal-body').html(data);
            $(modal).modal('show');
        })
        .fail(function () {
            errorAlert('Error al obtener los datos.');
        })
        .always(function () {
            $this.html(cache);
        });

        $(modal).on('show.bs.modal', function () {
            $(this).find('input').iCheck({
                checkboxClass: 'icheckbox_square-orange',
                radioClass: 'iradio_square-orange',
            });
        });
    },

    /**
     * Obtiene información general del error y lo muestra en un alert
     *
     * @param {string} message
     * @param {string} url
     * @param {int} line
     * @param {int} col
     * @param {string} error
     */
    traceError: function (message, url, line, col, error) {
        if (message) {
            var info = 'Error: ' + message + "<br/>\n" +
                'Url: ' + url + "<br/>\n" +
                'Line: ' + line;

            info += !col ? '' : "<br/>\ncolumn: " + col;
            info += !error ? '' : "<br/>\nerror: " + error;

            console.log(info);
            //alert(info);
            errorAlert(info);
        }
    },

    /**
     * Obtiene información general del error Ajax y lo muestra en un alert
     *
     * @param {object} event
     * @param {object} jqxhr
     * @param {object} settings
     * @param {string} thrownError
     */
    traceAjaxError: function(event, jqxhr, settings, thrownError) {
        if (thrownError != '' && jqxhr.statusText != 'abort') {
            var info = 'Error Ajax: ' + thrownError + "<br/>\n" +
                'status: ' + jqxhr.status + "<br/>\n" +
                'statusText: ' + jqxhr.statusText + "<br/>\n" +
                'url: ' + settings.url + "<br/>\n" +
                'data: ' + settings.data + "<br/>\n" +
                'responseText: ' + (jqxhr.responseText ? jqxhr.responseText.substr(0, 1100) : '');

            console.log(info);
            //alert(info);
            errorAlert(info);
        }
    },

    /**
     * Llena un elemento select con el listado de elementos porporcionados
     *
     * @param {string} selector Selector CSS u Objecto jQuery que hace referencia al Select
     * @param {JSON}|{array} list Listado de elementos a insertar, deben ser un arreglo asociativo value -> text, o un JSON con pares [value: '', text: '']
     * @param {boolean} reset Indica si el Select será limpiado antes de insertar los nuevos datos, Default true
     * @param {string} value Si los items de list es un objeto, value es el atributo que contiene el valor
     * @param {string} text Si los items de list es un objeto, text es el atributo que contiene la descripción o etiqueta
     */
    fillSelect: function(selector, list, reset, value, text)
    {
        reset = reset || true;
        value = value || '';
        text = text || '';

        var item = null,
            $select = typeof(selector) === 'string' ? $(selector) : selector;

        if (reset == true) {
            $select.find('option:not(:first-child)').remove();
        }

        if (list.length != 0) {
            for (item in list) {
                // Si es un objeto JSON
                if (value != '' && text != '') {
                    // Si existen los pares de elementos
                    if (list[item][value] != undefined && list[item][text] != undefined) {
                        $select.append('<option value="'+list[item][value]+'">'+list[item][text]+'</option>');
                    }
                } else {
                    // Si es un Array
                    $select.append('<option value="'+item+'">'+list[item]+'</option>');
                }
            }
        }

        // Lanzamos el evento change debido a que se actualizó la lista de elementos
        $select.trigger('change');
    },

    /**
     * Llena un select a partir de la respuesta de una solicitud Ajax
     *
     * @param {JSON} options Las opciones válidas son:
     * @param {string} select Selector CSS u Objecto jQuery que hace referencia al Select
     * @param {boolean} resetSelect: Indica si las opciones del select serán eliminadas antes de cargar los datos, Default true
     * @param {boolean} showLoadIndicator: Indica si se mostrara un indicador de cargando mientras no se devuelva respuesta de la solicitud, Default true
     * @param {'' | string|object} containerIndicator: Indica el contenedor donde se mostrara el spinner, puede ser vacio (mostrará el indicador despues del elemento), selector u objecto (mostrará dentro del contenedor especificado)
     * @param {string} value: Si la respuesta es una lista de objetos este indica el key donde se encuentra el valor
     * @param {string} text: Si la respuesta es una lista de objetos este indica donde se encuentra el texto a utilizar dentro del option
     * @param {string} wrapper: Si la respuesta es una lista de objetos, indica la key donde se encuentra el listado de opciones
     *
     *      Los siguientes parametros son los por defecto de jQuery para la solicitud Ajax
     * @param {string} type Default POST
     * @param {string} url
     * @param {JSON} data
     * @param {string} dataType Default json
     *
     * @return {Promise} Promise del request ajax
     */
    fillSelectByAjax: function(options)
    {
        var request = null;
        var defaultOptions = {
            select: '',
            resetSelect: true,
            showLoadIndicator: true,
            containerIndicator: '',
            value: '',
            text: '',
            wrapper: '',
            type: 'POST',
            url: '',
            data: '',
            dataType: 'json',
        };

        options = $.extend(defaultOptions, options);
        var loadIndicator = $(config.spinner_input);

        // Se agrega el parametro _csrf, necesario para la solicitud Ajax
        if (typeof(options.data) == 'object') {
            options.data = $.extend({_csrf: $('meta[name="csrf-token"]').attr('content')}, options.data);
        } else if (typeof(options.data) == 'string') {
            options.data += '&_csrf='+$('meta[name="csrf-token"]').attr('content');
        } else {
            if (config.debug) {
                console.log('options.data invalid recived by helpers.fillSelectByAjax');
            }
        }

        request = $.ajax({
            type: options.type,
            url: options.url,
            data: options.data,
            dataType: options.dataType,
            beforeSend: function() {
                if (options.showLoadIndicator == true) {
                    if (options.containerIndicator) {
                        if (options.containerIndicator instanceof Object) {
                            options.containerIndicator.html(loadIndicator);
                        } else {
                            $(options.containerIndicator).html(loadIndicator);
                        }
                    } else {
                        $(options.select).after(loadIndicator);
                    }
                }
            }
        });

        request.done(function(data) {
            if (options.wrapper != '') {
                helper.fillSelect(options.select, data[options.wrapper], options.resetSelect, options.value, options.text);
            } else {
                helper.fillSelect(options.select, data, options.resetSelect, options.value, options.text);
            }
        });

        request.always(function() {
            if (options.showLoadIndicator == true) {
                if (options.containerIndicator) {
                    if (options.containerIndicator instanceof Object) {
                        options.containerIndicator.find(loadIndicator).remove();
                    } else {
                        $(options.containerIndicator).find(loadIndicator).remove();
                    }
                } else {
                    $(options.select).parent().find(loadIndicator).remove();
                }
            }
        });

        request.fail(function(xhr, status, error) {
            if (config.debug) {
                console.log('Error '+status+' by helper.fillSelectByAjax: '+error);
            }
        });

        // Se devuelve del Promise de la solicitud Ajax
        return request;
    },

    /**
     * Inicializa el colorpicker
     *
     * @param string selector .input-colorpicker
     */
    colorpickerInit: function (selector) {
        selector = selector || '.input-colorpicker';
        var $selector = typeof(selector) === 'string' ? $(selector) : selector;

        $selector.colorpicker();

        // Si el elemento tiene addon, le agregamos la funcionalidad de mostrar el
        // widget al activar el input y de ocultar al quitar el foco del input
        if ($selector.find('.input-group-addon').length !== 0) {
            $selector.find('input').focus(function() {
                $selector.colorpicker('show');
            });
            $selector.find('input').click(function() {
                $selector.colorpicker('show');
            });
            $selector.find('input').blur(function() {
                $selector.colorpicker('hide');
            });
        }
    },

    /**
     * Devuelve un color aleatorio en formato hexadecimal
     *
     * @return string Color hexadecimal
     */
    randomColor: function (init) {
        init = init || '#';
        var color = '';

        for (var i = 0; i < 6; i++) {
            color += parseInt(Math.random() * 16).toString(16);
        }

        return init + color;
    },

    /**
     * Estiliza el infoWindow en tiempo de ejecución
     */
    styleInfoWindowGMap: function () {
        var $container = $('.gm-style-iw'),
            $rectangulo = $container.prev().find('div:last'),
            $sombraRectangulo = $container.prev().children('div:nth-child(2)'),
            $triangulo = $rectangulo.prev(),
            //$sombraTriangulo = $container.prev().children('div:first'),
            bgColor = '#dd4b39';

        $rectangulo.css({
            backgroundColor: bgColor,
            borderRadius: 10,
        });

        $sombraRectangulo.css({
            borderRadius: 10,
        });

        $triangulo.find('div:first').find('div:first').css({
            backgroundColor: bgColor,
        });

        $triangulo.find('div:last').css({
            backgroundColor: bgColor,
        });
    },

    /**
     * Valida el formato de value como URL
     */
    validateURL: function (value){
      // URL validation from http://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
      var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
      var regex = new RegExp(expression);
      return value.match(regex);
    },

    /**
     * Valida el formato de la contraseña
     */
    validatePassword: function (value) {
        var longitudMinima      = 6;
        var mayusculas          = /[A-Z]/;
        var minusculas          = /[a-z]/;
        var numeros             = /[0-9]/;
        var caracteres_simbolos = /[áÁéÉíÍóÓúÚñÑüÜ\[\]\(\)\{\}\*\,\:\=\;\.\#\+\-\_\~\&\@\/\%\!\¡\¿\?\'\"]/;

        // - La longitud mínima debe ser de 6 caracteres
        if (value.length < longitudMinima) {
            return false;
        }

        // - Letras mayúsculas
        if (!mayusculas.test(value)) {
            return false;
        }

        // - Letras minúsculas
        if (!minusculas.test(value)) {
            return false;
        }

        // - Números
        if (!numeros.test(value)) {
            return false;
        }

        // - Números y/o símbolos {, ^, #, @, $, (, ), =, etc.
        // - Caracteres de puntuación (, ´ . - ! ?)
        if (!caracteres_simbolos.test(value)) {
            return false;
        }

        return true;
    },

    /**
     * Inicializa la validacion con jQuery Validation Plugin para los formularios con el atributo
     * data-validate="true"
     *
     */
    initValidate: function () {
        // Se agrega la función after y before para validar fechas de inicio y fin
        jQuery.validator.addMethod('after', function(value, element, params) {
            var start_date = moment($(params).val(), 'DD-MM-YYYY'),
                end_date   = moment(value, 'DD-MM-YYYY'),
                valid      = false;

            if (!start_date.isValid() || !end_date.isValid()) {
                return false;
            }

            valid = end_date.isAfter(start_date);

            if (valid) {
                // Elimina el mensaje de error
                $('#'+$(element).attr('id')+'-error').html('');
            }

            return valid;
        }, jQuery.validator.format('Fecha incorrecta, debe ser mayor.'));

        jQuery.validator.addMethod('before', function(value, element, params) {
            var end_date   = moment($(params).val(), 'DD-MM-YYYY'),
                start_date = moment(value, 'DD-MM-YYYY'),
                valid      = false;

            if (start_date.isValid() && end_date.isValid()) {
                valid = start_date.isBefore(end_date);

                if (valid) {
                    // Elimina el mensaje de error
                    $('#'+$(element).attr('id')+'-error').html('');
                }

                return valid;
            }

            return true;
        }, jQuery.validator.format('Fecha incorrecta, debe ser menor.'));

        // http://stackoverflow.com/a/13142475
        jQuery.validator.addMethod('urlvalidation', function(value, element, params) {
            return this.optional(element) || helper.validateURL(value);
        }, jQuery.validator.format('Por favor, escribe una URL válida.'));

        jQuery.validator.addMethod('passwordvalidation', function(value, element, params) {
            return helper.validatePassword(value);
        }, jQuery.validator.format('La contraseña debe contener letras en mayúsculas, minúsculas, números y símbolos o signos de puntuación.'));

        $('form[data-validate="true"]').each(function() {
            var validator = $(this).validate({
                errorElement: 'span',
                errorClass: 'help-block',
                ignore: '.ignore',
                /*submitHandler: function(form) {
                    form.submit();
                },*/
                highlight: function(element, errorClass) {
                    $(element)
                        .closest('.form-group')
                        .addClass('has-error')
                        .removeClass('has-success');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element)
                        .closest('.form-group')
                        // .addClass('has-success')
                        .removeClass('has-error');
                },
                /*invalidHandler: function(event, validator) {},*/
                success: function(label) {},
            });

            // validator.focusInvalid = function() {};
        });
    },

    /**
     * Crea dinamicamente el form, lo inserta al dom y lo envía
     *
     * @param  string action
     * @param  string method
     * @return void
     */
    dynamicSendForm: function (action, method) {
        var $form = $('<form style="display:none">');
        $form.attr('action', action);
        $form.attr('method', 'POST');
        $form.append('<input type="hidden" name="_method" value="'+method+'">');
        $form.append('<input type="hidden" name="_token" value="'+$('meta[name="csrf-token"]').attr('content')+'">');

        $('body').append($form);
        $form.trigger('submit');
    },

    fixHeightContent: function () {
        var window_height = $(document).height(),
            main_header = 0,
            main_footer = 0,
            padding = 0,
            new_height = 0;

        // gentelella
        if ($('body').hasClass('nav-md')) {
            main_header = $('.nav_menu').outerHeight();
            main_footer = $('footer').outerHeight();
            padding = 25;
        } else {
        // admin-lte
            main_header = $('.main-header').outerHeight();
            main_footer = $('.main-footer').outerHeight();
            padding = 15*4 + 7;
        }

        new_height = window_height-main_footer-main_header-padding;

        if ($('#main-content').outerHeight() < new_height) {
            $('#main-content').css({'min-height': (window_height-main_footer-main_header-padding)+'px'});
        }
    },

    /**
     * Ajuste decimal de un número.
     *
     * @param {String}  tipo  El tipo de ajuste.
     * @param {Number}  valor El numero.
     * @param {Integer} exp   El exponente (el logaritmo 10 del ajuste base).
     * @returns {Number} El valor ajustado.
     */
    decimalAdjust: function (type, value, exp) {
        // Si el exp no está definido o es cero...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // Si el valor no es un número o el exp no es un entero...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');

        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    },

    /**
     * Revisa si el mimeType proporcionado es tipo video
     *
     * @param  string mimeType
     * @return boolean
     */
    mimeTypeIsVideo: function (mimeType) {
        var pattern = new RegExp('video/');

        return pattern.test(mimeType);
    },

    /**
     * Revisa si el mimeType proporcionado es tipo image
     *
     * @param  string mimeType
     * @return boolean
     */
    mimeTypeIsImage: function (mimeType) {
        var pattern = new RegExp('image/');

        return pattern.test(mimeType);
    },

    formatNumber: function ($cantidad)
    {
        $formatted = parseInt($cantidad);

        if ($cantidad >= 1000000) {
            $formatted = Math.round($cantidad/1000000).formater() + 'M';
        } else if ($cantidad >= 1000) {
            $formatted = Math.round($cantidad/1000).formater() + 'K';
        } else {
            $formatted = ($cantidad*1).formater();
        }

        return $formatted;
    }
};

// http://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-money-in-javascript
Number.prototype.formater = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

Number.prototype.format = function(n, x, s, c) {
    var n = n | 2;
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Math/round
// Decimal round
if (!Math.round10) {
Math.round10 = function(value, exp) {
  return helper.decimalAdjust('round', value, exp);
};
}
// Decimal floor
if (!Math.floor10) {
Math.floor10 = function(value, exp) {
  return helper.decimalAdjust('floor', value, exp);
};
}
// Decimal ceil
if (!Math.ceil10) {
Math.ceil10 = function(value, exp) {
  return helper.decimalAdjust('ceil', value, exp);
};
}

/* globals $, config, helper, Cookies, moment */
'use strict';

$(document).ready(function() {
    // Establece el idioma a español de moment
    moment.locale('es');

    // Detectamos si estaba colapsado el sidebar
    if (Cookies.get('sidebar-collapse') != undefined && Cookies.get('sidebar-collapse') == 1 && $('section.sidebar').length != 0) {
        $('body').addClass('sidebar-collapse');
    }

    // Arregla el bug de Yii2, en la paginación que agrega doble ? a los enlaces
    /*$('a').each(function(){
        if ($(this).attr('href')) {
            $(this).attr('href', $(this).attr('href').replace('??', '?'));
        }
    });*/

    // Agrega un spinner al click del boton, link con la clase btn y los enlaces del paginador
    // omite los link de eliminar ya que causa conflicto al enviar el POST
    $('body').on('click', 'a.btn:not(.no-loading):not([title="Eliminar"]):not(.sidebar-toggle):not([data-original-title="Eliminar"]):not([class*="load-ajax-view"]):not([class^="kv-file"]), '+
        'button:not(.no-loading):not(.btn-box-tool):not([data-dismiss="modal"]):not(.dropdown-toggle):not([id^="mce"]):not([class^="mce"]):not([class*="load-ajax-view"]):not([class^="kv-file"]), '+
        '.pagination a', function (event){
            var $this = $(this),
                cache = $this.html();

            // Si ya tiene agregado el spinner
            // ya no lo volvemos ha agregar
            if ($this.data('spinner') != 1) {
                // Bandera para determinar que el spinner esta agregado
                $this.data('spinner', 1);
                $this.html(config.spinner);

                if (!$this.data('keepspinner')) {
                    setTimeout(function () {
                        $this.html(cache);
                        // Bandera para determinar que el spinner se ha quitado
                        $this.data('spinner', 0);
                    }, 3000);
                }
            }

            //return true;
    });

    // Agrega un spinner a los enlaces del menu
    $('.sidebar-menu, .side-menu').on('click', 'a:not(.no-loading):not(.sidebar-toggle):not([href="#"])', function () {
        $(this).append('<i class="fa fa-spinner fa-pulse" style="width: auto;"></i>');
    });

    // Agrega efecto al alert de notificación
    $('.alert.notification').each(function(){
        var $this = $(this);

        // Efecto blink
        $this.fadeOut(1000);
        $this.fadeIn(1000);

        /*setTimeout(function () {
            $this.fadeOut('slow');
        }, 10000);*/
    });

    // Al colapsar el sidebar se guarda una coockie para mantener su status
    $('.sidebar-toggle').click(function(event) {
        if ($('body').hasClass('sidebar-collapse')) {
            Cookies.set('sidebar-collapse', 0);
        } else {
            Cookies.set('sidebar-collapse', 1);
        }

        return true;
    });

    // Agrega la funcionalidad del box-header para poder ocultar su contenido al hacer click
    $('.box-header').click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        var _this = $(this);

        //Find the box parent
        var box = _this.parents('.box').first();
        //Find the button trigger
        var btnCollapse = _this.find('[data-widget="collapse"]').first();
        //Find the body and the footer
        var box_content = box.find('> .box-body, > .box-footer, > form  >.box-body, > form > .box-footer');

        if (!box.hasClass('collapsed-box')) {
            //Convert minus into plus
            btnCollapse.children(':first')
                .removeClass('fa-minus')
                .addClass('fa-plus');
            //Hide the content
            box_content.slideUp(500, function () {
                box.addClass('collapsed-box');
            });
        } else {
            //Convert plus into minus
            btnCollapse.children(':first')
                .removeClass('fa-plus')
                .addClass('fa-minus');
            //Show the content
            box_content.slideDown(500, function () {
                box.removeClass('collapsed-box');
            });
        }

    });

    // Habilita la carga de vistas por AJAX
    /*if (config.load_ajax_view) {
        $('body').on('click', '.load-ajax-view', helper.load_ajax_view);
    }*/

    $('#modalChangePass').on('shown.bs.modal', function (e) {
        $('#password').focus();
    });

    // Implementa la carga de select dependiente por Ajax
    $('body').on('change', '.load-ajax-onchange', function(event) {
        helper.fillSelectByAjax({
            type: 'GET',
            select: $(this).data('target'),
            url: $(this).data('url'),
            data: {id: $(this).val()},
        });
    });

    // Arregla el bug que cuando se muestra el modal
    // a veces se queda fijo el tooltip y no se oculta
    $('[id^="modal"]').on('shown.bs.modal', function() {
       $('[data-toggle="tooltip"]').tooltip('hide');
    });

    // Implementa el envio de form a partir de los data de un link
    // Por ejemplo el confirm al eliminar un elemento
    $('body').on('click', 'a[data-form]', function(event) {
        event.preventDefault();
        var $this = $(this);

        if ($this.data('confirm')) {
            $.jAlert({
                type: 'confirm',
                confirmQuestion: $this.data('confirm'),
                confirmBtnText: 'Aceptar',
                denyBtnText: 'Cancelar',
                onConfirm: function () {
                    $this.html(config.spinner);
                    helper.dynamicSendForm($this.attr('href'), $this.data('method'));
                }
            });
        } else {
            $this.html(config.spinner);
            helper.dynamicSendForm($this.attr('href'), $this.data('method'));
        }
    });

    // Gentelella
    // Agrega el evento click al title del panel para que colapse
    // o muestre el contenido
    $('body').on('click', '.x_panel .x_title', function(event) {
        event.preventDefault();
        event.stopPropagation();

        console.log(event);

        var $BOX_PANEL = $(this).closest('.x_panel'),
            $ICON = $(this).find('.collapse-link i'),
            $BOX_CONTENT = $BOX_PANEL.find('.x_content');

        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
            $BOX_CONTENT.slideToggle(200, function(){
                $BOX_PANEL.removeAttr('style');
            });
        } else {
            $BOX_CONTENT.slideToggle(200);
            $BOX_PANEL.css('height', 'auto');
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');

        return false;
    });

    // Gentelella
    // Deshabilita el evento debido a que se ejecuta dos veces,
    // debido al evento click que se agrego al x_title
    $('.collapse-link').off('click');

    // helper.fixHeightContent();

    // https://laravel.com/docs/5.3/csrf
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#formCambiarPass').submit(function (event) {
        event.preventDefault();
        event.stopPropagation();

        var $this = $(this);

        if (!$this.valid()) {
            return false;
        }

        $.ajax({
            url: $this.attr('action'),
            type: $this.attr('method'),
            dataType: 'json',
            data: $this.serialize(),
        })
        .done(function(response) {
            swal({
                    title: 'Contraseña actualizada exitosamente.',
                    type: 'success',
                    confirmButtonColor: config.color_red,
                    confirmButtonText: 'Aceptar',
                });
        })
        .fail(function(jqXHR) {
            errorAlert('Error al procesar los datos. ' + jqXHR.responseJSON.message);
        })
        .always(function() {
            $('#modalChangePass').modal('hide');
            $this.trigger('reset');
        });
    });

    // $.notify('Estamos trabajando para mejorar el servicio, <br>algunos datos no estarán disponibles.', 'info');
});

// Muestra el Pace al momento de cambiar de página
window.onbeforeunload = function () {
    // $('.pace').removeClass('pace-inactive').addClass('pace-active');
    NProgress.start();
    NProgress.set(0.3);
};

if (config.debug) {
    $(document).ajaxError(helper.traceAjaxError);
    window.onerror = helper.traceError;
}

/* globals $, helper */
'use strict';

$(document).ready(function() {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
    });

    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY',
        locale: 'es',
        keepOpen: false,
        showClear: true,
        showClose: true,
        showTodayButton: true,
        calendarWeeks: true,
    }).on('dp.change', function (event) {
        $(this).trigger('change');
        $(this).trigger('focusout');
    });

    $('.timepicker').datetimepicker({
        format: 'HH:mm', //'hh:mm a', //'LT',
        locale: 'es',
        keepOpen: false,
        showClear: true,
        showClose: true,
        showTodayButton: true,
        calendarWeeks: true,
    }).on('dp.change', function (event) {
        $(this).trigger('change');
        $(this).trigger('focusout');
    });

    /*$('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
        locale: 'es',
    }).on('dp.change', function (event) {
        $(this).trigger('change');
        $(this).trigger('focusout');
    });*/

    $('.select2').each(function() {
        var options = {
            language: 'es',
            minimumInputLength: ($(this).data('minimuminputlength') ? $(this).data('minimuminputlength') : 0)
        };

        if ($(this).data('ajax--url')) {
            options.initSelection = function(element, callback) {
                var url = $(element).data('ajax--url')+ '?id=' + $(element).val();

                return $.getJSON(url, function(data) {
                        return callback(data.results);
                    });
            };
        }

        $(this).select2(options);

        if ($(this).data('ajax--url')) {
            $(this).trigger('change');
            $(this).trigger('focusout');
        }
    });

    $('.table').stickyTableHeaders();

    // Eliminamos la cabecera que agrega stickyTableHeaders debido a que se duplica
    // al exportar a pdf o excel
    /*$('.kv-grid-toolbar .export-pdf, .kv-grid-toolbar .export-xls').click(function(event) {
        $(this).closest('.grid-view').find('.tableFloatingHeader').remove();
        return true;
    });*/

    $('.btnResetSelect2').click(function() {
        $('#' + $(this).data('select2')).val(null).trigger('change');
    });

    /*$('.fancybox').fancybox();

    helper.colorpickerInit();*/

    helper.initValidate();

    // $('.selectToList').selectToList({});

    // $('.resizable').resizable();

    // $('[data-toggle="popover"]').popover();

    if (typeof(Handlebars) != "undefined") {
        // Registra Helpers para Handlebars
        /* http://stackoverflow.com/questions/8853396/logical-operator-in-a-handlebars-js-if-conditional */
        Handlebars.registerHelper("ifCond",function(v1,operator,v2,options) {
            switch (operator)
            {
                case "==":
                    return (v1==v2)?options.fn(this):options.inverse(this);

                case "!=":
                    return (v1!=v2)?options.fn(this):options.inverse(this);

                case "===":
                    return (v1===v2)?options.fn(this):options.inverse(this);

                case "!==":
                    return (v1!==v2)?options.fn(this):options.inverse(this);

                case "&&":
                    return (v1&&v2)?options.fn(this):options.inverse(this);

                case "||":
                    return (v1||v2)?options.fn(this):options.inverse(this);

                case "<":
                    return (v1<v2)?options.fn(this):options.inverse(this);

                case "<=":
                    return (v1<=v2)?options.fn(this):options.inverse(this);

                case ">":
                    return (v1>v2)?options.fn(this):options.inverse(this);

                case ">=":
                 return (v1>=v2)?options.fn(this):options.inverse(this);

                default:
                    return eval(""+v1+operator+v2)?options.fn(this):options.inverse(this);
            }
        });

        Handlebars.registerHelper('join', function(arreglo, options) {
            var result = '';

            for (var i in arreglo) {
                result += options.fn(arreglo[i]) + ', ';
            }

            return result.substr(0, result.length-2);
        });

        Handlebars.registerHelper('join_slash', function(arreglo, options) {
            var result = '';

            for (var i in arreglo) {
                result += options.fn(arreglo[i]) + ' / ';
            }

            return result.substr(0, result.length-2);
        });

        Handlebars.registerHelper('substr', function(str, start, length) {
            start = start | 0;
            length = length | 120;

            if (str.length > length) {
                return str.substr(start, length) + '...';
            }

            return str;
        });
    }
});

/*!
 * jQuery Form Plugin
 * version: 3.51.0-2014.06.20
 * Requires jQuery v1.5 or later
 * Copyright (c) 2014 M. Alsup
 * Examples and documentation at: http://malsup.com/jquery/form/
 * Project repository: https://github.com/malsup/form
 * Dual licensed under the MIT and GPL licenses.
 * https://github.com/malsup/form#copyright-and-license
 */
/*global ActiveXObject */

// AMD support
(function (factory) {
    "use strict";
    if (typeof define === 'function' && define.amd) {
        // using AMD; register as anon module
        define(['jquery'], factory);
    } else {
        // no AMD; invoke directly
        factory( (typeof(jQuery) != 'undefined') ? jQuery : window.Zepto );
    }
}

(function($) {
"use strict";

/*
    Usage Note:
    -----------
    Do not use both ajaxSubmit and ajaxForm on the same form.  These
    functions are mutually exclusive.  Use ajaxSubmit if you want
    to bind your own submit handler to the form.  For example,

    $(document).ready(function() {
        $('#myForm').on('submit', function(e) {
            e.preventDefault(); // <-- important
            $(this).ajaxSubmit({
                target: '#output'
            });
        });
    });

    Use ajaxForm when you want the plugin to manage all the event binding
    for you.  For example,

    $(document).ready(function() {
        $('#myForm').ajaxForm({
            target: '#output'
        });
    });

    You can also use ajaxForm with delegation (requires jQuery v1.7+), so the
    form does not have to exist when you invoke ajaxForm:

    $('#myForm').ajaxForm({
        delegation: true,
        target: '#output'
    });

    When using ajaxForm, the ajaxSubmit function will be invoked for you
    at the appropriate time.
*/

/**
 * Feature detection
 */
var feature = {};
feature.fileapi = $("<input type='file'/>").get(0).files !== undefined;
feature.formdata = window.FormData !== undefined;

var hasProp = !!$.fn.prop;

// attr2 uses prop when it can but checks the return type for
// an expected string.  this accounts for the case where a form
// contains inputs with names like "action" or "method"; in those
// cases "prop" returns the element
$.fn.attr2 = function() {
    if ( ! hasProp ) {
        return this.attr.apply(this, arguments);
    }
    var val = this.prop.apply(this, arguments);
    if ( ( val && val.jquery ) || typeof val === 'string' ) {
        return val;
    }
    return this.attr.apply(this, arguments);
};

/**
 * ajaxSubmit() provides a mechanism for immediately submitting
 * an HTML form using AJAX.
 */
$.fn.ajaxSubmit = function(options) {
    /*jshint scripturl:true */

    // fast fail if nothing selected (http://dev.jquery.com/ticket/2752)
    if (!this.length) {
        log('ajaxSubmit: skipping submit process - no element selected');
        return this;
    }

    var method, action, url, $form = this;

    if (typeof options == 'function') {
        options = { success: options };
    }
    else if ( options === undefined ) {
        options = {};
    }

    method = options.type || this.attr2('method');
    action = options.url  || this.attr2('action');

    url = (typeof action === 'string') ? $.trim(action) : '';
    url = url || window.location.href || '';
    if (url) {
        // clean url (don't include hash vaue)
        url = (url.match(/^([^#]+)/)||[])[1];
    }

    options = $.extend(true, {
        url:  url,
        success: $.ajaxSettings.success,
        type: method || $.ajaxSettings.type,
        iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank'
    }, options);

    // hook for manipulating the form data before it is extracted;
    // convenient for use with rich editors like tinyMCE or FCKEditor
    var veto = {};
    this.trigger('form-pre-serialize', [this, options, veto]);
    if (veto.veto) {
        log('ajaxSubmit: submit vetoed via form-pre-serialize trigger');
        return this;
    }

    // provide opportunity to alter form data before it is serialized
    if (options.beforeSerialize && options.beforeSerialize(this, options) === false) {
        log('ajaxSubmit: submit aborted via beforeSerialize callback');
        return this;
    }

    var traditional = options.traditional;
    if ( traditional === undefined ) {
        traditional = $.ajaxSettings.traditional;
    }

    var elements = [];
    var qx, a = this.formToArray(options.semantic, elements);
    if (options.data) {
        options.extraData = options.data;
        qx = $.param(options.data, traditional);
    }

    // give pre-submit callback an opportunity to abort the submit
    if (options.beforeSubmit && options.beforeSubmit(a, this, options) === false) {
        log('ajaxSubmit: submit aborted via beforeSubmit callback');
        return this;
    }

    // fire vetoable 'validate' event
    this.trigger('form-submit-validate', [a, this, options, veto]);
    if (veto.veto) {
        log('ajaxSubmit: submit vetoed via form-submit-validate trigger');
        return this;
    }

    var q = $.param(a, traditional);
    if (qx) {
        q = ( q ? (q + '&' + qx) : qx );
    }
    if (options.type.toUpperCase() == 'GET') {
        options.url += (options.url.indexOf('?') >= 0 ? '&' : '?') + q;
        options.data = null;  // data is null for 'get'
    }
    else {
        options.data = q; // data is the query string for 'post'
    }

    var callbacks = [];
    if (options.resetForm) {
        callbacks.push(function() { $form.resetForm(); });
    }
    if (options.clearForm) {
        callbacks.push(function() { $form.clearForm(options.includeHidden); });
    }

    // perform a load on the target only if dataType is not provided
    if (!options.dataType && options.target) {
        var oldSuccess = options.success || function(){};
        callbacks.push(function(data) {
            var fn = options.replaceTarget ? 'replaceWith' : 'html';
            $(options.target)[fn](data).each(oldSuccess, arguments);
        });
    }
    else if (options.success) {
        callbacks.push(options.success);
    }

    options.success = function(data, status, xhr) { // jQuery 1.4+ passes xhr as 3rd arg
        var context = options.context || this ;    // jQuery 1.4+ supports scope context
        for (var i=0, max=callbacks.length; i < max; i++) {
            callbacks[i].apply(context, [data, status, xhr || $form, $form]);
        }
    };

    if (options.error) {
        var oldError = options.error;
        options.error = function(xhr, status, error) {
            var context = options.context || this;
            oldError.apply(context, [xhr, status, error, $form]);
        };
    }

     if (options.complete) {
        var oldComplete = options.complete;
        options.complete = function(xhr, status) {
            var context = options.context || this;
            oldComplete.apply(context, [xhr, status, $form]);
        };
    }

    // are there files to upload?

    // [value] (issue #113), also see comment:
    // https://github.com/malsup/form/commit/588306aedba1de01388032d5f42a60159eea9228#commitcomment-2180219
    var fileInputs = $('input[type=file]:enabled', this).filter(function() { return $(this).val() !== ''; });

    var hasFileInputs = fileInputs.length > 0;
    var mp = 'multipart/form-data';
    var multipart = ($form.attr('enctype') == mp || $form.attr('encoding') == mp);

    var fileAPI = feature.fileapi && feature.formdata;
    log("fileAPI :" + fileAPI);
    var shouldUseFrame = (hasFileInputs || multipart) && !fileAPI;

    var jqxhr;

    // options.iframe allows user to force iframe mode
    // 06-NOV-09: now defaulting to iframe mode if file input is detected
    if (options.iframe !== false && (options.iframe || shouldUseFrame)) {
        // hack to fix Safari hang (thanks to Tim Molendijk for this)
        // see:  http://groups.google.com/group/jquery-dev/browse_thread/thread/36395b7ab510dd5d
        if (options.closeKeepAlive) {
            $.get(options.closeKeepAlive, function() {
                jqxhr = fileUploadIframe(a);
            });
        }
        else {
            jqxhr = fileUploadIframe(a);
        }
    }
    else if ((hasFileInputs || multipart) && fileAPI) {
        jqxhr = fileUploadXhr(a);
    }
    else {
        jqxhr = $.ajax(options);
    }

    $form.removeData('jqxhr').data('jqxhr', jqxhr);

    // clear element array
    for (var k=0; k < elements.length; k++) {
        elements[k] = null;
    }

    // fire 'notify' event
    this.trigger('form-submit-notify', [this, options]);
    return this;

    // utility fn for deep serialization
    function deepSerialize(extraData){
        var serialized = $.param(extraData, options.traditional).split('&');
        var len = serialized.length;
        var result = [];
        var i, part;
        for (i=0; i < len; i++) {
            // #252; undo param space replacement
            serialized[i] = serialized[i].replace(/\+/g,' ');
            part = serialized[i].split('=');
            // #278; use array instead of object storage, favoring array serializations
            result.push([decodeURIComponent(part[0]), decodeURIComponent(part[1])]);
        }
        return result;
    }

     // XMLHttpRequest Level 2 file uploads (big hat tip to francois2metz)
    function fileUploadXhr(a) {
        var formdata = new FormData();

        for (var i=0; i < a.length; i++) {
            formdata.append(a[i].name, a[i].value);
        }

        if (options.extraData) {
            var serializedData = deepSerialize(options.extraData);
            for (i=0; i < serializedData.length; i++) {
                if (serializedData[i]) {
                    formdata.append(serializedData[i][0], serializedData[i][1]);
                }
            }
        }

        options.data = null;

        var s = $.extend(true, {}, $.ajaxSettings, options, {
            contentType: false,
            processData: false,
            cache: false,
            type: method || 'POST'
        });

        if (options.uploadProgress) {
            // workaround because jqXHR does not expose upload property
            s.xhr = function() {
                var xhr = $.ajaxSettings.xhr();
                if (xhr.upload) {
                    xhr.upload.addEventListener('progress', function(event) {
                        var percent = 0;
                        var position = event.loaded || event.position; /*event.position is deprecated*/
                        var total = event.total;
                        if (event.lengthComputable) {
                            percent = Math.ceil(position / total * 100);
                        }
                        options.uploadProgress(event, position, total, percent);
                    }, false);
                }
                return xhr;
            };
        }

        s.data = null;
        var beforeSend = s.beforeSend;
        s.beforeSend = function(xhr, o) {
            //Send FormData() provided by user
            if (options.formData) {
                o.data = options.formData;
            }
            else {
                o.data = formdata;
            }
            if(beforeSend) {
                beforeSend.call(this, xhr, o);
            }
        };
        return $.ajax(s);
    }

    // private function for handling file uploads (hat tip to YAHOO!)
    function fileUploadIframe(a) {
        var form = $form[0], el, i, s, g, id, $io, io, xhr, sub, n, timedOut, timeoutHandle;
        var deferred = $.Deferred();

        // #341
        deferred.abort = function(status) {
            xhr.abort(status);
        };

        if (a) {
            // ensure that every serialized input is still enabled
            for (i=0; i < elements.length; i++) {
                el = $(elements[i]);
                if ( hasProp ) {
                    el.prop('disabled', false);
                }
                else {
                    el.removeAttr('disabled');
                }
            }
        }

        s = $.extend(true, {}, $.ajaxSettings, options);
        s.context = s.context || s;
        id = 'jqFormIO' + (new Date().getTime());
        if (s.iframeTarget) {
            $io = $(s.iframeTarget);
            n = $io.attr2('name');
            if (!n) {
                $io.attr2('name', id);
            }
            else {
                id = n;
            }
        }
        else {
            $io = $('<iframe name="' + id + '" src="'+ s.iframeSrc +'" />');
            $io.css({ position: 'absolute', top: '-1000px', left: '-1000px' });
        }
        io = $io[0];


        xhr = { // mock object
            aborted: 0,
            responseText: null,
            responseXML: null,
            status: 0,
            statusText: 'n/a',
            getAllResponseHeaders: function() {},
            getResponseHeader: function() {},
            setRequestHeader: function() {},
            abort: function(status) {
                var e = (status === 'timeout' ? 'timeout' : 'aborted');
                log('aborting upload... ' + e);
                this.aborted = 1;

                try { // #214, #257
                    if (io.contentWindow.document.execCommand) {
                        io.contentWindow.document.execCommand('Stop');
                    }
                }
                catch(ignore) {}

                $io.attr('src', s.iframeSrc); // abort op in progress
                xhr.error = e;
                if (s.error) {
                    s.error.call(s.context, xhr, e, status);
                }
                if (g) {
                    $.event.trigger("ajaxError", [xhr, s, e]);
                }
                if (s.complete) {
                    s.complete.call(s.context, xhr, e);
                }
            }
        };

        g = s.global;
        // trigger ajax global events so that activity/block indicators work like normal
        if (g && 0 === $.active++) {
            $.event.trigger("ajaxStart");
        }
        if (g) {
            $.event.trigger("ajaxSend", [xhr, s]);
        }

        if (s.beforeSend && s.beforeSend.call(s.context, xhr, s) === false) {
            if (s.global) {
                $.active--;
            }
            deferred.reject();
            return deferred;
        }
        if (xhr.aborted) {
            deferred.reject();
            return deferred;
        }

        // add submitting element to data if we know it
        sub = form.clk;
        if (sub) {
            n = sub.name;
            if (n && !sub.disabled) {
                s.extraData = s.extraData || {};
                s.extraData[n] = sub.value;
                if (sub.type == "image") {
                    s.extraData[n+'.x'] = form.clk_x;
                    s.extraData[n+'.y'] = form.clk_y;
                }
            }
        }

        var CLIENT_TIMEOUT_ABORT = 1;
        var SERVER_ABORT = 2;

        function getDoc(frame) {
            /* it looks like contentWindow or contentDocument do not
             * carry the protocol property in ie8, when running under ssl
             * frame.document is the only valid response document, since
             * the protocol is know but not on the other two objects. strange?
             * "Same origin policy" http://en.wikipedia.org/wiki/Same_origin_policy
             */

            var doc = null;

            // IE8 cascading access check
            try {
                if (frame.contentWindow) {
                    doc = frame.contentWindow.document;
                }
            } catch(err) {
                // IE8 access denied under ssl & missing protocol
                log('cannot get iframe.contentWindow document: ' + err);
            }

            if (doc) { // successful getting content
                return doc;
            }

            try { // simply checking may throw in ie8 under ssl or mismatched protocol
                doc = frame.contentDocument ? frame.contentDocument : frame.document;
            } catch(err) {
                // last attempt
                log('cannot get iframe.contentDocument: ' + err);
                doc = frame.document;
            }
            return doc;
        }

        // Rails CSRF hack (thanks to Yvan Barthelemy)
        var csrf_token = $('meta[name=csrf-token]').attr('content');
        var csrf_param = $('meta[name=csrf-param]').attr('content');
        if (csrf_param && csrf_token) {
            s.extraData = s.extraData || {};
            s.extraData[csrf_param] = csrf_token;
        }

        // take a breath so that pending repaints get some cpu time before the upload starts
        function doSubmit() {
            // make sure form attrs are set
            var t = $form.attr2('target'),
                a = $form.attr2('action'),
                mp = 'multipart/form-data',
                et = $form.attr('enctype') || $form.attr('encoding') || mp;

            // update form attrs in IE friendly way
            form.setAttribute('target',id);
            if (!method || /post/i.test(method) ) {
                form.setAttribute('method', 'POST');
            }
            if (a != s.url) {
                form.setAttribute('action', s.url);
            }

            // ie borks in some cases when setting encoding
            if (! s.skipEncodingOverride && (!method || /post/i.test(method))) {
                $form.attr({
                    encoding: 'multipart/form-data',
                    enctype:  'multipart/form-data'
                });
            }

            // support timout
            if (s.timeout) {
                timeoutHandle = setTimeout(function() { timedOut = true; cb(CLIENT_TIMEOUT_ABORT); }, s.timeout);
            }

            // look for server aborts
            function checkState() {
                try {
                    var state = getDoc(io).readyState;
                    log('state = ' + state);
                    if (state && state.toLowerCase() == 'uninitialized') {
                        setTimeout(checkState,50);
                    }
                }
                catch(e) {
                    log('Server abort: ' , e, ' (', e.name, ')');
                    cb(SERVER_ABORT);
                    if (timeoutHandle) {
                        clearTimeout(timeoutHandle);
                    }
                    timeoutHandle = undefined;
                }
            }

            // add "extra" data to form if provided in options
            var extraInputs = [];
            try {
                if (s.extraData) {
                    for (var n in s.extraData) {
                        if (s.extraData.hasOwnProperty(n)) {
                           // if using the $.param format that allows for multiple values with the same name
                           if($.isPlainObject(s.extraData[n]) && s.extraData[n].hasOwnProperty('name') && s.extraData[n].hasOwnProperty('value')) {
                               extraInputs.push(
                               $('<input type="hidden" name="'+s.extraData[n].name+'">').val(s.extraData[n].value)
                                   .appendTo(form)[0]);
                           } else {
                               extraInputs.push(
                               $('<input type="hidden" name="'+n+'">').val(s.extraData[n])
                                   .appendTo(form)[0]);
                           }
                        }
                    }
                }

                if (!s.iframeTarget) {
                    // add iframe to doc and submit the form
                    $io.appendTo('body');
                }
                if (io.attachEvent) {
                    io.attachEvent('onload', cb);
                }
                else {
                    io.addEventListener('load', cb, false);
                }
                setTimeout(checkState,15);

                try {
                    form.submit();
                } catch(err) {
                    // just in case form has element with name/id of 'submit'
                    var submitFn = document.createElement('form').submit;
                    submitFn.apply(form);
                }
            }
            finally {
                // reset attrs and remove "extra" input elements
                form.setAttribute('action',a);
                form.setAttribute('enctype', et); // #380
                if(t) {
                    form.setAttribute('target', t);
                } else {
                    $form.removeAttr('target');
                }
                $(extraInputs).remove();
            }
        }

        if (s.forceSync) {
            doSubmit();
        }
        else {
            setTimeout(doSubmit, 10); // this lets dom updates render
        }

        var data, doc, domCheckCount = 50, callbackProcessed;

        function cb(e) {
            if (xhr.aborted || callbackProcessed) {
                return;
            }

            doc = getDoc(io);
            if(!doc) {
                log('cannot access response document');
                e = SERVER_ABORT;
            }
            if (e === CLIENT_TIMEOUT_ABORT && xhr) {
                xhr.abort('timeout');
                deferred.reject(xhr, 'timeout');
                return;
            }
            else if (e == SERVER_ABORT && xhr) {
                xhr.abort('server abort');
                deferred.reject(xhr, 'error', 'server abort');
                return;
            }

            if (!doc || doc.location.href == s.iframeSrc) {
                // response not received yet
                if (!timedOut) {
                    return;
                }
            }
            if (io.detachEvent) {
                io.detachEvent('onload', cb);
            }
            else {
                io.removeEventListener('load', cb, false);
            }

            var status = 'success', errMsg;
            try {
                if (timedOut) {
                    throw 'timeout';
                }

                var isXml = s.dataType == 'xml' || doc.XMLDocument || $.isXMLDoc(doc);
                log('isXml='+isXml);
                if (!isXml && window.opera && (doc.body === null || !doc.body.innerHTML)) {
                    if (--domCheckCount) {
                        // in some browsers (Opera) the iframe DOM is not always traversable when
                        // the onload callback fires, so we loop a bit to accommodate
                        log('requeing onLoad callback, DOM not available');
                        setTimeout(cb, 250);
                        return;
                    }
                    // let this fall through because server response could be an empty document
                    //log('Could not access iframe DOM after mutiple tries.');
                    //throw 'DOMException: not available';
                }

                //log('response detected');
                var docRoot = doc.body ? doc.body : doc.documentElement;
                xhr.responseText = docRoot ? docRoot.innerHTML : null;
                xhr.responseXML = doc.XMLDocument ? doc.XMLDocument : doc;
                if (isXml) {
                    s.dataType = 'xml';
                }
                xhr.getResponseHeader = function(header){
                    var headers = {'content-type': s.dataType};
                    return headers[header.toLowerCase()];
                };
                // support for XHR 'status' & 'statusText' emulation :
                if (docRoot) {
                    xhr.status = Number( docRoot.getAttribute('status') ) || xhr.status;
                    xhr.statusText = docRoot.getAttribute('statusText') || xhr.statusText;
                }

                var dt = (s.dataType || '').toLowerCase();
                var scr = /(json|script|text)/.test(dt);
                if (scr || s.textarea) {
                    // see if user embedded response in textarea
                    var ta = doc.getElementsByTagName('textarea')[0];
                    if (ta) {
                        xhr.responseText = ta.value;
                        // support for XHR 'status' & 'statusText' emulation :
                        xhr.status = Number( ta.getAttribute('status') ) || xhr.status;
                        xhr.statusText = ta.getAttribute('statusText') || xhr.statusText;
                    }
                    else if (scr) {
                        // account for browsers injecting pre around json response
                        var pre = doc.getElementsByTagName('pre')[0];
                        var b = doc.getElementsByTagName('body')[0];
                        if (pre) {
                            xhr.responseText = pre.textContent ? pre.textContent : pre.innerText;
                        }
                        else if (b) {
                            xhr.responseText = b.textContent ? b.textContent : b.innerText;
                        }
                    }
                }
                else if (dt == 'xml' && !xhr.responseXML && xhr.responseText) {
                    xhr.responseXML = toXml(xhr.responseText);
                }

                try {
                    data = httpData(xhr, dt, s);
                }
                catch (err) {
                    status = 'parsererror';
                    xhr.error = errMsg = (err || status);
                }
            }
            catch (err) {
                log('error caught: ',err);
                status = 'error';
                xhr.error = errMsg = (err || status);
            }

            if (xhr.aborted) {
                log('upload aborted');
                status = null;
            }

            if (xhr.status) { // we've set xhr.status
                status = (xhr.status >= 200 && xhr.status < 300 || xhr.status === 304) ? 'success' : 'error';
            }

            // ordering of these callbacks/triggers is odd, but that's how $.ajax does it
            if (status === 'success') {
                if (s.success) {
                    s.success.call(s.context, data, 'success', xhr);
                }
                deferred.resolve(xhr.responseText, 'success', xhr);
                if (g) {
                    $.event.trigger("ajaxSuccess", [xhr, s]);
                }
            }
            else if (status) {
                if (errMsg === undefined) {
                    errMsg = xhr.statusText;
                }
                if (s.error) {
                    s.error.call(s.context, xhr, status, errMsg);
                }
                deferred.reject(xhr, 'error', errMsg);
                if (g) {
                    $.event.trigger("ajaxError", [xhr, s, errMsg]);
                }
            }

            if (g) {
                $.event.trigger("ajaxComplete", [xhr, s]);
            }

            if (g && ! --$.active) {
                $.event.trigger("ajaxStop");
            }

            if (s.complete) {
                s.complete.call(s.context, xhr, status);
            }

            callbackProcessed = true;
            if (s.timeout) {
                clearTimeout(timeoutHandle);
            }

            // clean up
            setTimeout(function() {
                if (!s.iframeTarget) {
                    $io.remove();
                }
                else { //adding else to clean up existing iframe response.
                    $io.attr('src', s.iframeSrc);
                }
                xhr.responseXML = null;
            }, 100);
        }

        var toXml = $.parseXML || function(s, doc) { // use parseXML if available (jQuery 1.5+)
            if (window.ActiveXObject) {
                doc = new ActiveXObject('Microsoft.XMLDOM');
                doc.async = 'false';
                doc.loadXML(s);
            }
            else {
                doc = (new DOMParser()).parseFromString(s, 'text/xml');
            }
            return (doc && doc.documentElement && doc.documentElement.nodeName != 'parsererror') ? doc : null;
        };
        var parseJSON = $.parseJSON || function(s) {
            /*jslint evil:true */
            return window['eval']('(' + s + ')');
        };

        var httpData = function( xhr, type, s ) { // mostly lifted from jq1.4.4

            var ct = xhr.getResponseHeader('content-type') || '',
                xml = type === 'xml' || !type && ct.indexOf('xml') >= 0,
                data = xml ? xhr.responseXML : xhr.responseText;

            if (xml && data.documentElement.nodeName === 'parsererror') {
                if ($.error) {
                    $.error('parsererror');
                }
            }
            if (s && s.dataFilter) {
                data = s.dataFilter(data, type);
            }
            if (typeof data === 'string') {
                if (type === 'json' || !type && ct.indexOf('json') >= 0) {
                    data = parseJSON(data);
                } else if (type === "script" || !type && ct.indexOf("javascript") >= 0) {
                    $.globalEval(data);
                }
            }
            return data;
        };

        return deferred;
    }
};

/**
 * ajaxForm() provides a mechanism for fully automating form submission.
 *
 * The advantages of using this method instead of ajaxSubmit() are:
 *
 * 1: This method will include coordinates for <input type="image" /> elements (if the element
 *    is used to submit the form).
 * 2. This method will include the submit element's name/value data (for the element that was
 *    used to submit the form).
 * 3. This method binds the submit() method to the form for you.
 *
 * The options argument for ajaxForm works exactly as it does for ajaxSubmit.  ajaxForm merely
 * passes the options argument along after properly binding events for submit elements and
 * the form itself.
 */
$.fn.ajaxForm = function(options) {
    options = options || {};
    options.delegation = options.delegation && $.isFunction($.fn.on);

    // in jQuery 1.3+ we can fix mistakes with the ready state
    if (!options.delegation && this.length === 0) {
        var o = { s: this.selector, c: this.context };
        if (!$.isReady && o.s) {
            log('DOM not ready, queuing ajaxForm');
            $(function() {
                $(o.s,o.c).ajaxForm(options);
            });
            return this;
        }
        // is your DOM ready?  http://docs.jquery.com/Tutorials:Introducing_$(document).ready()
        log('terminating; zero elements found by selector' + ($.isReady ? '' : ' (DOM not ready)'));
        return this;
    }

    if ( options.delegation ) {
        $(document)
            .off('submit.form-plugin', this.selector, doAjaxSubmit)
            .off('click.form-plugin', this.selector, captureSubmittingElement)
            .on('submit.form-plugin', this.selector, options, doAjaxSubmit)
            .on('click.form-plugin', this.selector, options, captureSubmittingElement);
        return this;
    }

    return this.ajaxFormUnbind()
        .bind('submit.form-plugin', options, doAjaxSubmit)
        .bind('click.form-plugin', options, captureSubmittingElement);
};

// private event handlers
function doAjaxSubmit(e) {
    /*jshint validthis:true */
    var options = e.data;
    if (!e.isDefaultPrevented()) { // if event has been canceled, don't proceed
        e.preventDefault();
        $(e.target).ajaxSubmit(options); // #365
    }
}

function captureSubmittingElement(e) {
    /*jshint validthis:true */
    var target = e.target;
    var $el = $(target);
    if (!($el.is("[type=submit],[type=image]"))) {
        // is this a child element of the submit el?  (ex: a span within a button)
        var t = $el.closest('[type=submit]');
        if (t.length === 0) {
            return;
        }
        target = t[0];
    }
    var form = this;
    form.clk = target;
    if (target.type == 'image') {
        if (e.offsetX !== undefined) {
            form.clk_x = e.offsetX;
            form.clk_y = e.offsetY;
        } else if (typeof $.fn.offset == 'function') {
            var offset = $el.offset();
            form.clk_x = e.pageX - offset.left;
            form.clk_y = e.pageY - offset.top;
        } else {
            form.clk_x = e.pageX - target.offsetLeft;
            form.clk_y = e.pageY - target.offsetTop;
        }
    }
    // clear form vars
    setTimeout(function() { form.clk = form.clk_x = form.clk_y = null; }, 100);
}


// ajaxFormUnbind unbinds the event handlers that were bound by ajaxForm
$.fn.ajaxFormUnbind = function() {
    return this.unbind('submit.form-plugin click.form-plugin');
};

/**
 * formToArray() gathers form element data into an array of objects that can
 * be passed to any of the following ajax functions: $.get, $.post, or load.
 * Each object in the array has both a 'name' and 'value' property.  An example of
 * an array for a simple login form might be:
 *
 * [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ]
 *
 * It is this array that is passed to pre-submit callback functions provided to the
 * ajaxSubmit() and ajaxForm() methods.
 */
$.fn.formToArray = function(semantic, elements) {
    var a = [];
    if (this.length === 0) {
        return a;
    }

    var form = this[0];
    var formId = this.attr('id');
    var els = semantic ? form.getElementsByTagName('*') : form.elements;
    var els2;

    if (els && !/MSIE [678]/.test(navigator.userAgent)) { // #390
        els = $(els).get();  // convert to standard array
    }

    // #386; account for inputs outside the form which use the 'form' attribute
    if ( formId ) {
        els2 = $(':input[form="' + formId + '"]').get(); // hat tip @thet
        if ( els2.length ) {
            els = (els || []).concat(els2);
        }
    }

    if (!els || !els.length) {
        return a;
    }

    var i,j,n,v,el,max,jmax;
    for(i=0, max=els.length; i < max; i++) {
        el = els[i];
        n = el.name;
        if (!n || el.disabled) {
            continue;
        }

        if (semantic && form.clk && el.type == "image") {
            // handle image inputs on the fly when semantic == true
            if(form.clk == el) {
                a.push({name: n, value: $(el).val(), type: el.type });
                a.push({name: n+'.x', value: form.clk_x}, {name: n+'.y', value: form.clk_y});
            }
            continue;
        }

        v = $.fieldValue(el, true);
        if (v && v.constructor == Array) {
            if (elements) {
                elements.push(el);
            }
            for(j=0, jmax=v.length; j < jmax; j++) {
                a.push({name: n, value: v[j]});
            }
        }
        else if (feature.fileapi && el.type == 'file') {
            if (elements) {
                elements.push(el);
            }
            var files = el.files;
            if (files.length) {
                for (j=0; j < files.length; j++) {
                    a.push({name: n, value: files[j], type: el.type});
                }
            }
            else {
                // #180
                a.push({ name: n, value: '', type: el.type });
            }
        }
        else if (v !== null && typeof v != 'undefined') {
            if (elements) {
                elements.push(el);
            }
            a.push({name: n, value: v, type: el.type, required: el.required});
        }
    }

    if (!semantic && form.clk) {
        // input type=='image' are not found in elements array! handle it here
        var $input = $(form.clk), input = $input[0];
        n = input.name;
        if (n && !input.disabled && input.type == 'image') {
            a.push({name: n, value: $input.val()});
            a.push({name: n+'.x', value: form.clk_x}, {name: n+'.y', value: form.clk_y});
        }
    }
    return a;
};

/**
 * Serializes form data into a 'submittable' string. This method will return a string
 * in the format: name1=value1&amp;name2=value2
 */
$.fn.formSerialize = function(semantic) {
    //hand off to jQuery.param for proper encoding
    return $.param(this.formToArray(semantic));
};

/**
 * Serializes all field elements in the jQuery object into a query string.
 * This method will return a string in the format: name1=value1&amp;name2=value2
 */
$.fn.fieldSerialize = function(successful) {
    var a = [];
    this.each(function() {
        var n = this.name;
        if (!n) {
            return;
        }
        var v = $.fieldValue(this, successful);
        if (v && v.constructor == Array) {
            for (var i=0,max=v.length; i < max; i++) {
                a.push({name: n, value: v[i]});
            }
        }
        else if (v !== null && typeof v != 'undefined') {
            a.push({name: this.name, value: v});
        }
    });
    //hand off to jQuery.param for proper encoding
    return $.param(a);
};

/**
 * Returns the value(s) of the element in the matched set.  For example, consider the following form:
 *
 *  <form><fieldset>
 *      <input name="A" type="text" />
 *      <input name="A" type="text" />
 *      <input name="B" type="checkbox" value="B1" />
 *      <input name="B" type="checkbox" value="B2"/>
 *      <input name="C" type="radio" value="C1" />
 *      <input name="C" type="radio" value="C2" />
 *  </fieldset></form>
 *
 *  var v = $('input[type=text]').fieldValue();
 *  // if no values are entered into the text inputs
 *  v == ['','']
 *  // if values entered into the text inputs are 'foo' and 'bar'
 *  v == ['foo','bar']
 *
 *  var v = $('input[type=checkbox]').fieldValue();
 *  // if neither checkbox is checked
 *  v === undefined
 *  // if both checkboxes are checked
 *  v == ['B1', 'B2']
 *
 *  var v = $('input[type=radio]').fieldValue();
 *  // if neither radio is checked
 *  v === undefined
 *  // if first radio is checked
 *  v == ['C1']
 *
 * The successful argument controls whether or not the field element must be 'successful'
 * (per http://www.w3.org/TR/html4/interact/forms.html#successful-controls).
 * The default value of the successful argument is true.  If this value is false the value(s)
 * for each element is returned.
 *
 * Note: This method *always* returns an array.  If no valid value can be determined the
 *    array will be empty, otherwise it will contain one or more values.
 */
$.fn.fieldValue = function(successful) {
    for (var val=[], i=0, max=this.length; i < max; i++) {
        var el = this[i];
        var v = $.fieldValue(el, successful);
        if (v === null || typeof v == 'undefined' || (v.constructor == Array && !v.length)) {
            continue;
        }
        if (v.constructor == Array) {
            $.merge(val, v);
        }
        else {
            val.push(v);
        }
    }
    return val;
};

/**
 * Returns the value of the field element.
 */
$.fieldValue = function(el, successful) {
    var n = el.name, t = el.type, tag = el.tagName.toLowerCase();
    if (successful === undefined) {
        successful = true;
    }

    if (successful && (!n || el.disabled || t == 'reset' || t == 'button' ||
        (t == 'checkbox' || t == 'radio') && !el.checked ||
        (t == 'submit' || t == 'image') && el.form && el.form.clk != el ||
        tag == 'select' && el.selectedIndex == -1)) {
            return null;
    }

    if (tag == 'select') {
        var index = el.selectedIndex;
        if (index < 0) {
            return null;
        }
        var a = [], ops = el.options;
        var one = (t == 'select-one');
        var max = (one ? index+1 : ops.length);
        for(var i=(one ? index : 0); i < max; i++) {
            var op = ops[i];
            if (op.selected) {
                var v = op.value;
                if (!v) { // extra pain for IE...
                    v = (op.attributes && op.attributes.value && !(op.attributes.value.specified)) ? op.text : op.value;
                }
                if (one) {
                    return v;
                }
                a.push(v);
            }
        }
        return a;
    }
    return $(el).val();
};

/**
 * Clears the form data.  Takes the following actions on the form's input fields:
 *  - input text fields will have their 'value' property set to the empty string
 *  - select elements will have their 'selectedIndex' property set to -1
 *  - checkbox and radio inputs will have their 'checked' property set to false
 *  - inputs of type submit, button, reset, and hidden will *not* be effected
 *  - button elements will *not* be effected
 */
$.fn.clearForm = function(includeHidden) {
    return this.each(function() {
        $('input,select,textarea', this).clearFields(includeHidden);
    });
};

/**
 * Clears the selected form elements.
 */
$.fn.clearFields = $.fn.clearInputs = function(includeHidden) {
    var re = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i; // 'hidden' is not in this list
    return this.each(function() {
        var t = this.type, tag = this.tagName.toLowerCase();
        if (re.test(t) || tag == 'textarea') {
            this.value = '';
        }
        else if (t == 'checkbox' || t == 'radio') {
            this.checked = false;
        }
        else if (tag == 'select') {
            this.selectedIndex = -1;
        }
        else if (t == "file") {
            if (/MSIE/.test(navigator.userAgent)) {
                $(this).replaceWith($(this).clone(true));
            } else {
                $(this).val('');
            }
        }
        else if (includeHidden) {
            // includeHidden can be the value true, or it can be a selector string
            // indicating a special test; for example:
            //  $('#myForm').clearForm('.special:hidden')
            // the above would clean hidden inputs that have the class of 'special'
            if ( (includeHidden === true && /hidden/.test(t)) ||
                 (typeof includeHidden == 'string' && $(this).is(includeHidden)) ) {
                this.value = '';
            }
        }
    });
};

/**
 * Resets the form data.  Causes all form elements to be reset to their original value.
 */
$.fn.resetForm = function() {
    return this.each(function() {
        // guard against an input with the name of 'reset'
        // note that IE reports the reset function as an 'object'
        if (typeof this.reset == 'function' || (typeof this.reset == 'object' && !this.reset.nodeType)) {
            this.reset();
        }
    });
};

/**
 * Enables or disables any matching elements.
 */
$.fn.enable = function(b) {
    if (b === undefined) {
        b = true;
    }
    return this.each(function() {
        this.disabled = !b;
    });
};

/**
 * Checks/unchecks any matching checkboxes or radio buttons and
 * selects/deselects and matching option elements.
 */
$.fn.selected = function(select) {
    if (select === undefined) {
        select = true;
    }
    return this.each(function() {
        var t = this.type;
        if (t == 'checkbox' || t == 'radio') {
            this.checked = select;
        }
        else if (this.tagName.toLowerCase() == 'option') {
            var $sel = $(this).parent('select');
            if (select && $sel[0] && $sel[0].type == 'select-one') {
                // deselect all other options
                $sel.find('option').selected(false);
            }
            this.selected = select;
        }
    });
};

// expose debug var
$.fn.ajaxSubmit.debug = false;

// helper fn for console logging
function log() {
    if (!$.fn.ajaxSubmit.debug) {
        return;
    }
    var msg = '[jquery.form] ' + Array.prototype.join.call(arguments,'');
    if (window.console && window.console.log) {
        window.console.log(msg);
    }
    else if (window.opera && window.opera.postError) {
        window.opera.postError(msg);
    }
}

}));
//# sourceMappingURL=app.js.map
