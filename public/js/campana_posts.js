/* globals $, config, moment, urlDeletePropuestaPost, urlUpdatePropuestaPost, urlPublicarPost, errorAlert, successAlert */
'use strict';

$(document).ready(function() {
    $('.tooltipster').tooltipster({
        theme: 'tooltipster-light',
        side: 'left',
        animation: 'grow',
        maxWidth: 300,
    });

    $('.btn-delete').click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        var $this = $(this),
            cacheBtn = $this.html();

        $.jAlert({
            type: 'confirm',
            confirmQuestion: '¿Esta seguro que desea eliminar este registro?',
            confirmBtnText: 'Aceptar',
            denyBtnText: 'Cancelar',
            onConfirm: function () {
                $this.html(config.spinner);

                $.ajax({
                    url: urlDeletePropuestaPost,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        _method: 'DELETE',
                        influenciador_id: $this.data('influenciador'),
                        campana_id: $this.data('campana')
                    },
                })
                .done(function() {
                    $this.closest('.row-post').fadeOut('slow', function() {
                        $(this).remove();
                    });
                })
                .fail(function(jqXHR) {
                    errorAlert('Error al procesar los datos. ' + jqXHR.responseJSON.message);
                })
                .always(function() {
                    $this.html(cacheBtn);
                });
            }
        });
    });

    function setCantidadPublicaciones(btn, counter) {
        var $el = $(btn).closest('.cantidad_publicaciones_widget').find('.cantidad-publicaciones'),
            cantidadPublicaciones = parseInt($el.data('cantidadpublicaciones'));

        cantidadPublicaciones = cantidadPublicaciones + counter;

        if (cantidadPublicaciones >= 0) {
            $el.data('cantidadpublicaciones', cantidadPublicaciones);
            $el.html(cantidadPublicaciones);

            // El siguiente elemento es el precio de las publicaciones
            $el.parent().find('.precio-publicaciones').html('$ ' + Number(cantidadPublicaciones * $el.data('preciopublicacion')).format(-1) + ' MXN');
        }
    }

    $('.btn-plus-cantidad-publicaciones').click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        setCantidadPublicaciones(this, 1);
    });

    $('.btn-minus-cantidad-publicaciones').click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        setCantidadPublicaciones(this, -1);
    });

    function gatherPropuestaPost($btn)
    {
        var data = {
            _method: 'PUT',
            influenciador_id: $btn.data('influenciador'),
            campana_id: $btn.data('campana'),
            redes_sociales: []
        };

        data.redes_sociales = $btn
            .closest('.body')
            .find('.cantidad-publicaciones')
            .map(function(index, element) {
                if ($(element).data('cantidadpublicaciones') != 0) {
                    return {
                        red_social_id:  $(element).data('redsocial'),
                        cantidad_publicaciones: $(element).data('cantidadpublicaciones')
                    };
                }
            })
            .get();

        return data;
    }

    $('.btn-send-propuesta-posts').click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        var $this = $(this),
            cacheBtn = $this.html();

        $this.html(config.spinner);
        $this.tooltip('hide');

        $.ajax({
            url: urlUpdatePropuestaPost,
            type: 'POST',
            dataType: 'json',
            data: gatherPropuestaPost($this),
        })
        .done(function() {
            swal({
                title: 'Propuesta de Publicaciones enviada exitosamente.',
                type: 'success',
                confirmButtonColor: config.color_red,
                confirmButtonText: 'Aceptar',
            });

            $this.fadeOut('slow', function() {
                $this.remove();
            });
        })
        .fail(function(jqXHR) {
            errorAlert('Error al procesar los datos. ' + jqXHR.responseJSON.message);
        })
        .always(function() {
            $this.html(cacheBtn);
        });
    });

    $('.btn-toggle-list-posts').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        var $containerListPosts = $(this).closest('.row-post').find('.container-list-posts');

        if ($containerListPosts.hasClass('off')) {
            $containerListPosts.slideDown('slow');
            $containerListPosts.removeClass('off');
        } else {
            $containerListPosts.slideUp('slow');
            $containerListPosts.addClass('off');
        }

        $(this).find('.fa').toggleClass('fa-chevron-down');
    });

    $('.btn-send-publicar-posts-influencer').click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        var $this = $(this),
            cacheBtn = $this.html();

        $this.html(config.spinner);

        $.ajax({
            url: urlPublicarPost,
            type: 'POST',
            dataType: 'json',
            data: {
                campana_id: $this.data('campana'),
                influenciador_id: $this.data('influenciador'),
            }
        })
        .done(function() {
            swal({
                title: 'Las Publicaciones se agregaron en la lista de espera para ser publicadas según su fecha de publicación.',
                type: 'success',
                confirmButtonColor: config.color_red,
                confirmButtonText: 'Aceptar',
            });

            $this.hide();
        })
        .fail(function(jqXHR) {
            errorAlert('Error al procesar los datos. ' + jqXHR.responseJSON.message);
        })
        .always(function() {
            $this.html(cacheBtn);
        });
    });

    $('#posts_campana').on('click', '.btn-send-posts-influencer', function(event) {
        event.preventDefault();
        event.stopPropagation();

        var $this = $(this),
            cacheBtn = $this.html(),
            // Solo se envian las publicaciones que no estan aprobadas
            $fileUploaders = $this.closest('.container-list-posts').find('.bootstrap-fileinput:not(.aprobado)'),
            $postContainer = null,
            fecha_hora = null,
            contenido = null,
            validate = true;

        $this.html(config.spinner);

        $fileUploaders.each(function(index, el) {
            if (validate) {
                $postContainer = $(el).closest('.post-container');
                contenido = $postContainer.find('[name="contenido"]').val();
                fecha_hora = moment($postContainer.find('[name="fecha"]').val()+' '+ $postContainer.find('[name="hora"]').val(), 'DD-MM-YYYY HH:mm');

                if (typeof(contenido) !== 'undefined') {
                    if (!contenido.trim() || contenido === '' || contenido.length === 0) {
                        errorAlert('Debe escribir un contenido para el Post.');
                        // $postContainer.find('[name="contenido"]').trigger('focus');
                        validate = false;

                        return validate;
                    }

                    // Si la red social es Twitter, el contenido del Post debe ser menor a 140 caracteres
                    if (contenido.length > 140 && $postContainer.data('redsocial') == 1) {
                        errorAlert('El contenido del Post no debe de exceder los 140 carácteres.');
                        // $postContainer.find('[name="contenido"]').trigger('focus');
                        validate = false;

                        return validate;
                    }
                }

                if (typeof($postContainer.find('[name="fecha"]').val()) !== 'undefined') {
                    if (!fecha_hora.isValid()) {
                        infoAlert('Advertencia', 'No especificó de forma correcta la fecha y hora para algunas publicaciones.');
                        // $postContainer.find('[name="fecha"]').trigger('focus');
                        validate = false;

                        return validate;
                    }
                }

                // Delega la responsabilidad de enviar los datos al plugin Bootstrap File Input https://github.com/kartik-v/bootstrap-fileinput
                // El código para la creación y envio de los datos se encuentra en el archivo uploadfile_post.js
                $(el).fileinput('upload');
            }
        });

        // $this.html(cacheBtn);
        setTimeout(function () {
            $this.html('Actualizar contenidos');
        }, 3000);
    });

    $('.btn-publicar-post').click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        var $this = $(this),
            cacheBtn = $this.html();

        swal({
            title: 'Aprobar Publicación',
            text: '¿Confirma que desea aprobar esta publicación? Después de aprobar la publicación no podrá realizar cambios al contenido.',
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            confirmButtonColor: config.color_red,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        },
        function(isConfirm){
            if (!isConfirm) {
                $this.html(cacheBtn);

                return true;
            }

            $this.html(config.spinner);

            $.ajax({
                url: urlPublicarPost + '/' + $this.data('post'),
                type: 'POST',
                dataType: 'json',
            })
            .done(function() {
                swal({
                    title: 'La Publicación se agregó en la lista de espera para ser publicado según su fecha de publicación.',
                    type: 'success',
                    confirmButtonColor: config.color_red,
                    confirmButtonText: 'Aceptar',
                });

                $this.html(cacheBtn);

                $this.removeClass('btn') // Elimina la clase btn para que el cursor no se convierta en pointer
                        .removeClass('animated') // Elimina la animación del botón
                        .off('click') // Elimina el evento click para que no se vuelva enviar
                        .tooltipster('content', 'Aprobado por Agencia')
                        .addClass('text-black') // Agrega fondo negro y texto blanco
                        .find('.fa-thumbs-o-up') // Localiza el icono aprobado por influencer
                        .addClass('fa-check') // Agrega el icono de buena
                        .removeClass('animated') // Elimina la animación del icono
                        .removeClass('fa-thumbs-o-up') // Elimina el icono aprobado por influencer
                        .removeClass('btn-red-rounded');

                $this.closest('.post-container').addClass('aprobado');
            })
            .fail(function(jqXHR) {
                errorAlert('Error al procesar los datos. ' + jqXHR.responseJSON.message);
                $this.html(cacheBtn);
            });
        });
    });
});

//# sourceMappingURL=campana_posts.js.map

//# sourceMappingURL=campana_posts.js.map
