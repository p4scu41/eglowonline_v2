/*global $, base_url, config, errorAlert, yii, tinyMCE */
'use strict';

var fileValidator = {
    twitter: {
        video: {
            duration: function (seconds) {
                var minutos, segundos;

                if (seconds > 140) {
                    minutos = Math.floor(seconds / 60);
                    segundos = Math.round(seconds - (minutos*60));

                    return 'La duración máxima de los videos es de 2 minutos y 20 segundos, '+
                           'el video seleccionado tiene una duración de '+minutos+' minutos con '+segundos+' segundos';
                }

                return false;
            },
            size: function (kbs) {
                if ((kbs/1024) > maxFileSize) {
                    return 'El tamaño máximo para el video es de '+(maxFileSize/1024)+' MB'
                }

                return false;
            }
        },
        imagen: {
            resolution: function (width, height) {
                if (width > 1920 && height > 1200) {
                    return 'La resolución máxima para la imagen es de 1920x1200 o 1200x1900, ' +
                           'la imagen seleccionada tiene una resolución de '+width+'x'+height;
                } else if (width > 1200 && height > 1900) {
                    return 'La resolución máxima para la imagen es de 1200x1900 o 1920x1200, ' +
                           'la imagen seleccionada tiene una resolución de '+width+'x'+height;
                }

                return false;
            },
            size: function (kbs) {
                if ((kbs/1024) > maxFileSize) {
                    return 'El tamaño máximo para el video es de '+(maxFileSize/1024)+' MB'
                }

                return false;
            }
        }
    }
};

//# sourceMappingURL=fileValidator.js.map
