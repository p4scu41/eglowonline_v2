/* globals $, moment, urlSavePublicaciones, app_url, maxFileSize */
'use strict';

$(document).ready(function() {
    window.URL = window.URL || window.webkitURL;

    var totalFiles = 0,
        totalEnviadosExitoso = 0,
        totalEnviadosError = 0,
        defaultSettingsFileUploader = {
            // overwriteInitial: true,
            language: 'es',
            showCaption: false,
            showUpload: false,
            showCancel: false,
            showBrowse: false,
            showRemove: false,
            // showClose: false,
            dropZoneEnabled: true,
            browseOnZoneClick: true,
            uploadAsync: true,
            uploadUrl: urlSavePublicaciones,
            // deleteUrl: urlDeleteMediaPublicacion
            layoutTemplates: {
                actions: '<div class="file-actions">\n' +
                    '    <div class="file-footer-buttons">\n' +
                    '        {zoom} {other}' + // {delete}
                    '    </div>\n' +
                    // '    {drag}\n' +
                    // '    <div class="file-upload-indicator" title="{indicatorTitle}">{indicator}</div>\n' +
                    '    <div class="clearfix"></div>\n' +
                    '</div>',
                footer: '<div class="file-thumbnail-footer">\n' +
                    '    {progress} {actions}\n' +
                    '</div>',
                modal: '<div class="modal-dialog modal-lg" role="document">\n' +
                    '  <div class="modal-content">\n' +
                    '    <div class="modal-header">\n' +
                    '      <div class="kv-zoom-actions pull-right">{close}</div>\n' +
                    '      <h3 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h3>\n' +
                    '    </div>\n' +
                    '    <div class="modal-body">\n' +
                    '      <div class="floating-buttons"></div>\n' +
                    '      <div class="kv-zoom-body file-zoom-content"></div>\n' +
                    '    </div>\n' +
                    '  </div>\n' +
                    '</div>\n',
            },
            previewTemplates: {
                generic: '{content}\n',
                image: '<div class="file-preview-frame" id="{previewId}" data-fileindex="{fileindex}" data-template="{template}">\n' +
                    '   {footer}\n' +
                    '   <div class="kv-file-content">' +
                    '       <img src="{data}" class="kv-preview-data file-preview-image" title="{caption}" alt="{caption}" style="width:{width};height:{height};">\n' +
                    '   </div>\n' +
                    '</div>\n',
                video: '<div class="file-preview-frame" id="{previewId}" data-fileindex="{fileindex}" data-template="{template}" title="{caption}" ' +
                    '   style="width:{width};height:{height};">\n' +
                    '   {footer}\n' +
                    '   <div class="kv-file-content">' +
                    '       <video class="kv-preview-data" width="{width}" height="{height}" controls>\n' +
                    '           <source src="{data}" type="{type}">\n' +
                    '           <div class="file-preview-other">\n' +
                    '               <span class="{previewFileIconClass}">{previewFileIcon}</span>\n' +
                    '           </div>' + '\n' +
                    '       </video>\n' +
                    '   </div>\n' +
                    '</div>\n',
                audio: '<div class="file-preview-frame" id="{previewId}" data-fileindex="{fileindex}" data-template="{template}" title="{caption}" ' +
                    '   style="width:{width};height:{height};">\n' +
                    '   {footer}\n' +
                    '   <div class="kv-file-content">' +
                    '        <audio class="kv-preview-data" controls>\n' +
                    '            <source src="{data}" type="{type}">\n' +
                    '            <div class="file-preview-other">\n' +
                    '                <span class="{previewFileIconClass}">{previewFileIcon}</span>\n' +
                    '            </div>' + '\n' +
                    '        </audio>\n' +
                    '   </div>\n' +
                    '</div>\n',
            },
            previewSettings: {
                image: {width: '100%', height: 'auto'},
                video: {width: '100%', height: 'auto'},
                audio: {width: '100%', height: 'auto'},
                other: {width: '100%', height: 'auto'}
            },
            maxFileSize: maxFileSize, // 2048
            autoReplace: true,
            // allowedFileTypes: ['image', 'video', 'audio'],
            allowedPreviewTypes: ['image', 'video', 'audio'],
            allowedFileExtensions: ['jpg', 'jpeg', 'gif', 'png', 'mp4'], // Los formatos compatibles para Twitter son GIF, JPEG, PNG y MP4
            defaultPreviewContent: '<img src="'+app_url+'/multimedia_posts/placeholder.png" class="file-preview-image">',
            // initialPreviewFileType: 'image',
            initialPreviewAsData: true,
            /*initialPreviewConfig: [
                {type: 'image', caption: 'Moon.jpg', size: 930321, width: '120px', key: 1},
            ],*/
            msgSizeTooLarge: 'El archivo "{name}" (<b>{customSize}</b>) excede el tamaño máximo permitido de <b>{customMaxSize}</b>.',
        };

    // Solo se envian las publicaciones que no estan aprobados
    totalFiles = $('.bootstrap-fileinput:not(.aprobado)').length;
    // console.log(totalFiles);

    $('.bootstrap-fileinput').each(function(index, el) {
        var $el = $(el),
            // Clonamos la variable defaultSettingsFileUploader, para no sobreescribirla
            defaultSettings = $.extend(true, {}, defaultSettingsFileUploader),
            $postContainer = $el.closest('.post-container'),
            settingsFile = {},
            filetype = 'image';

        // Si existe el campo data influenciador, significa que los datos se van a enviar desde Administrador o Agencia
        if (typeof($(el).data('influenciador')) !== 'undefined') {
            settingsFile = {
                uploadExtraData: function uploadExtraDataFromAgencia() {
                    var momentFechaHora = moment($postContainer.find('[name="fecha"]').val()+' '+ $postContainer.find('[name="hora"]').val(), 'DD-MM-YYYY HH:mm');

                    var data = {
                        id: $el.data('publicacion'),
                        campana_id: $el.data('campana'),
                        influenciador_id: $el.data('influenciador'),
                        red_social_id: $el.data('redsocial'),
                        contenido: $postContainer.find('[name="contenido"]').val(),
                        fecha_hora_publicacion: momentFechaHora.format('YYYY-MM-DD HH:mm:ss'),
                    };

                    return data;
                }
            };
        } else {
            // De lo contrario se envia desde influencer
            settingsFile = {
                uploadExtraData: function () {
                    var data = {
                        id: $el.data('publicacion'),
                        campana_id: $el.data('campana'),
                        red_social_id: $el.data('redsocial'),
                    };

                    return data;
                }
            };
        }

        // Si la publicación ya esta aprobada, no se puede cambiar la imagen
        if ($el.hasClass('aprobado')) {
            settingsFile.dropZoneEnabled = false;
            settingsFile.browseOnZoneClick = false;
        }

        // Establece los opciones para la vista previa cuando se enviaron publicaciones con imagen o video
        if ($(el).data('multimedia') !== undefined) {
            // console.log($(el).data('multimedia'));
            settingsFile.initialPreview = [
                $(el).data('multimedia')
            ];

            settingsFile.initialPreviewFileType = $(el).data('type');
            settingsFile.initialPreviewConfig = [
                {
                    type: $(el).data('type'),
                    size: $(el).data('size'),
                    filetype: $(el).data('filetype'),
                    caption: $(el).data('caption'),
                    url: $(el).data('multimedia')
                },
            ];

            // console.log(settingsFile.initialPreviewConfig);
        }

        settingsFile.elErrorContainer = '#errorPublicacion_' + $(el).data('publicacion');

        var settings = $.extend(true, defaultSettings, settingsFile);

        // Crea el componente y agrega listener al evento fileuploaderror
        // para customizar el mensaje de error de tamaño máximo de archivo
        $(el)
            .fileinput(settings)
            .on('fileuploaderror', function(event, data, msg) {
                // http://stackoverflow.com/questions/40656132/can-we-show-error-message-in-mb-regarding-fileinput-plugin-using-msgsizetoolarg
                var size = data.files[0].size,
                    maxFileSize = $(this).data().fileinput.maxFileSize,
                    formatSize = function(s) {
                        var i = Math.floor(Math.log(s) / Math.log(1024));
                        var sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                        var out = (s / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];

                        return out;
                    };

                // Muestra el tamaño máximo en KB o MB, segun sea el caso
                msg = msg.replace('{customSize}', formatSize(size));
                msg = msg.replace('{customMaxSize}', formatSize(maxFileSize * 1024 /* Convert KB to Bytes */));

                $('li[data-file-id="'+data.id+'"]').html(msg);
            });
    });

    // Detiene la reproducción del video cuando se oculta el modal de vista previa
    $('#kvFileinputModal').on('hidden.bs.modal', function() {
        if ($(this).find('video')[0]) {
            $(this).find('video')[0].pause();
        }
    });

    // Agrega listener a los eventos por defecto
    $('.bootstrap-fileinput')
        .on('fileuploaded', function(event, data, previewId, index) {
            // console.log('fileuploaded');
            // console.log(data);

            if (data.jqXHR.status == 200) {
                totalEnviadosExitoso++;
            } else {
                totalEnviadosError++;
            }
        })
        .on('filebatchuploadsuccess', function(event, data, previewId, index) {
            totalEnviadosExitoso++;
            /*console.log('filebatchuploadsuccess');
            console.log(totalEnviadosExitoso);
            console.log(data);*/
        })
        .on('filebatchuploadcomplete', function(event, files, extra) {
            /*console.log('filebatchuploadcomplete');
            console.log('totalEnviadosError ' + totalEnviadosError);
            console.log('totalEnviadosExitoso ' + totalEnviadosExitoso);
            console.log('totalEnviadosExitoso + totalEnviadosError ' + (totalEnviadosExitoso+totalEnviadosError));
            console.log('totalFiles ' + totalFiles);
            console.log(files);*/

            if ((totalEnviadosError+totalEnviadosExitoso) >= totalFiles) {
                swal({
                    title: 'Se terminaron de enviar todas las publicaciones' + (totalEnviadosError > 0 ? ', pero se detectaron ' + totalEnviadosError + ' errores': '') + '.',
                    type: 'info',
                    confirmButtonColor: config.color_red,
                    confirmButtonText: 'Aceptar',
                });
            }
        })
        .on('fileuploaderror', function(event, data, msg) {
            totalEnviadosError++;
            /*console.log('fileuploaderror');
            console.log(totalEnviadosError);
            console.log(data);*/

            swal({
                title: 'Error al enviar la Publicación ' + data.extra.contenido,
                type: 'error',
                confirmButtonColor: config.color_red,
                confirmButtonText: 'Aceptar',
            });
        })
        .on('fileselect', function(event, numFiles, label) {
            // console.log("fileselect");
            var file  = event.delegateTarget.files.length ? event.delegateTarget.files[0] : null,
                $this = $(this),
                error = false;

            // Validación para twitter
            if (helper.mimeTypeIsVideo(file.type)) {
                // console.log('Video');
                var video = document.createElement('video'),
                    minutos = 0,
                    segundos = 0;

                video.preload = 'metadata';

                video.onloadedmetadata = function() {
                    window.URL.revokeObjectURL(this.src);

                    // Puedes subir videos de hasta 512 MB, pero limitamos el tamaño de subida a 50 MB (maxFileSize)
                    // file.size se expresa en bytes
                    error = fileValidator.twitter.video.size(file.size);
                    // console.log(error);

                    if (error == false) {
                        // La duración máxima de los videos es de 2 minutos y 20 segundos
                        error = fileValidator.twitter.video.duration(video.duration);
                        // console.log(error);
                    };

                    if (error != false) {
                        swal({
                            title: error,
                            type: 'error',
                            confirmButtonColor: config.color_red,
                            confirmButtonText: 'Aceptar',
                        });

                        $this.fileinput('clear');
                    }
                }

                video.src = URL.createObjectURL(file);
            } else if (helper.mimeTypeIsImage(file.type)) {
                // Puedes subir imagenes de hasta 5 MB, pero limitamos el tamaño de subida a 50 MB (maxFileSize)
                // file.size se expresa en bytes
                error = fileValidator.twitter.imagen.size(file.size);
                // console.log(error);

                var img = new Image;

                img.onload = function() {
                    var errorResolucion = fileValidator.twitter.imagen.resolution(img.width, img.height);
                    // console.log(errorResolucion);

                    if (errorResolucion != false) {
                        swal({
                            title: errorResolucion,
                            type: 'error',
                            confirmButtonColor: config.color_red,
                            confirmButtonText: 'Aceptar',
                        });

                        $this.fileinput('clear');
                    }
                    // console.log(img.width);
                    // console.log(img.height);
                };

                img.src = URL.createObjectURL(file);
            }

            if (error != false) {
                swal({
                    title: error,
                    type: 'error',
                    confirmButtonColor: config.color_red,
                    confirmButtonText: 'Aceptar',
                });

                $this.fileinput('clear');
            }

            // console.log(event);
            // console.log(file);
        });
        /*.on('filepreupload', function(event, data, previewId, index) {
            console.log('filepreupload');
            console.log(data);
        }).on('filebatchpreupload', function(event, data, previewId, index) {
            console.log('filebatchpreupload');
            console.log(data);
        })
        .on('fileerror', function(event, data, msg) {
           console.log('fileerror');
           console.log(data);
        })
        .on('filebatchuploaderror', function(event, data, msg) {
           console.log('filebatchuploaderror');
           console.log(data);
        })
        .on('change', function(event) {
            console.log('change');
            console.log(event);
        })*/
});

//# sourceMappingURL=campana_posts.js.map

//# sourceMappingURL=uploadfile_post.js.map
