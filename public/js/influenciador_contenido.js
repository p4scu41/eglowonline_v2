$(document).ready(function() {
    $('#myTab li a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    });

    $('#mySelect').on('change', function(e) {
        $('#myTab li a').eq($(this).val()).tab('show');
    });

    $('#comunidad-tab').on('click', function () {
        buscarHashtags('load');
        buscarPalabrasClave('load');
    });

    // opening & closing tabs for interactions, reachment, conversation ratio
    $('button.tab-contenido').on('click', function (event) {
        event.preventDefault();
        var id = $(this).data('id');

        $('#' + id).removeClass('hide');
        $('#' + id).siblings('div.content-tab').addClass('hide');

        $(this).removeClass('btn-default btn-round')
        $(this).addClass('btn-red-rounded')
        $(this).siblings('button').removeClass('btn-red-rounded');
        $(this).siblings('button').addClass('btn-default btn-round');

        if (id === 'topHashtags') {
            buscarHashtags('update');
        }

        if (id === 'topPalabrasClave') {
            buscarPalabrasClave('update');
        }
    });

    // opening videos through fancybox
    $('#tab_contenido').on('click', 'a.fancy', function (event) {
        event.preventDefault();
        var href = $(this).attr('href');

        $.fancybox.open({
            type : 'iframe',
            href : href,
            title: 'Lorem lipsum'
        });
    });

    function buscarHashtags(opcion) {
        $.ajax({
            url            : $('#wordcloudHashtags').data('url'),
            type           : 'POST',
            dataType       : 'json',
            data           : {
                influenciadorId: $('#influenciadorId').val()
            },
            beforeSend     : function () {
                $('#cargarGraficaHashtags').removeClass('hide');
            }
        }).
        done(function (respuesta) {
            $('#cargarGraficaHashtags').addClass('hide');

            if (respuesta.estatus === 'OK') {
                // draw graphic hashtags
                $('#wordcloudHashtags').removeClass('hide');

                if (opcion === 'load') {
                    $('#wordcloudHashtags').jQCloud(respuesta.words);
                }

                if (opcion === 'update') {
                    $('#wordcloudHashtags').jQCloud('update', respuesta.words);
                }

                $('[data-toggle="tooltip"]').tooltip();
            }

            if (respuesta.estatus === 'fail') {
                $('#mensajeNoEncontradoHashtags').removeClass('hide');
            }
        }).
        fail(function (jqXHR, textStatus, errorThrown) {
            $('#cargarGraficaHashtags').addClass('hide');
            console.log(textStatus + ': ' + errorThrown);
        });
    }

    function buscarPalabrasClave(opcion) {
        // construir gráfica palabras clave
        $.ajax({
            url            : $('#wordcloudPalabrasClave').data('url'),
            type           : 'POST',
            dataType       : 'json',
            data           : {
                influenciadorId: $('#influenciadorId').val()
            },
            beforeSend     : function () {
                $('#cargarGraficaTopKeywords').removeClass('hide');
            }
        }).
        done(function (respuesta) {
            $('#cargarGraficaTopKeywords').addClass('hide');

            if (respuesta.estatus === 'OK') {
                // draw graphic hashtags
                $('#wordcloudPalabrasClave').removeClass('hide');

                if (opcion === 'load') {
                    $('#wordcloudPalabrasClave').jQCloud(respuesta.words);
                }

                if (opcion === 'update') {
                    $('#wordcloudPalabrasClave').jQCloud('update', respuesta.words);
                }

                $('[data-toggle="tooltip"]').tooltip();
            }

            if (respuesta.estatus === 'fail') {
                $('#mensajeNoEncontradoKeywords').removeClass('hide');
            }
        }).
        fail(function (jqXHR, textStatus, errorThrown) {
            $('#cargarGraficaTopKeywords').addClass('hide');
            console.log(textStatus + ': ' + errorThrown);
        });
    }
});
//# sourceMappingURL=influenciador_contenido.js.map
