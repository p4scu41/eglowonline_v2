/* globals $, config, urlRechazarPropuesta, urlAceptarPropuesta, errorAlert, swal */
'use strict';

$(document).ready(function() {
    $('.btn-rechazar').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        $('#btn-send-rechazo').data('campana', $(this).data('campana'));

        $('#motivo_rechazo_text').val('');
        $('#motivo-rechazo-modal').modal('show');
    });

    $('#btn-send-rechazo').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        if ($('#motivo_rechazo_text').val() == '') {
            errorAlert('Debe escribir el motivo de rechazo.');
            return false;
        }

        var $this = $(this),
            cacheBtn = $this.html();

        $this.html(config.spinner);

        $.ajax({
            url: urlRechazarPropuesta,
            type: 'POST',
            dataType: 'json',
            data: {
                campana_id: $this.data('campana'),
                motivo_rechazo: $('#motivo_rechazo_text').val(),
            },
        })
        .done(function() {
            swal({
                title: 'Propuesta rechazada exitosamente.',
                type: 'success',
                confirmButtonColor: config.color_red,
                confirmButtonText: 'Aceptar',
            });

            $('.row-propuesta[data-campana="'+$this.data('campana')+'"]').fadeOut('slow', function() {
                $(this).remove();
            });
        })
        .fail(function() {
            errorAlert('Error al procesar los datos.');
        })
        .always(function() {
            $this.html(cacheBtn);
            $('#motivo_rechazo_text').val('');
            $('#motivo-rechazo-modal').modal('hide');
        });
    });

    $('.btn-aceptar').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        var campanaId   = $(this).data('campana'),
            urlTerminos = $(this).data('urlTerminos');

        $('#hrefCampanaTerminos').attr('href', urlTerminos);
        $('#btn-send-aceptar').data('campana', campanaId);

        $('#aceptar-propuesta-modal').modal('show');
    });

    $('#btn-send-aceptar').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        var $this = $(this),
            cacheBtn = $this.html();

        $this.html(config.spinner);

        $.ajax({
            url: urlAceptarPropuesta,
            type: 'POST',
            dataType: 'json',
            data: {
                campana_id: $this.data('campana'),
            },
        })
        .done(function() {
            swal({
                title: 'Propuesta aceptada exitosamente.',
                type: 'success',
                confirmButtonColor: config.color_red,
                confirmButtonText: 'Aceptar',
            },
            function() {
                location.reload();
            });
        })
        .fail(function() {
            errorAlert('Error al procesar los datos.');
        })
        .always(function() {
            $this.html(cacheBtn);
            $('#aceptar-propuesta-modal').modal('hide');
        });
    });
});

//# sourceMappingURL=influenciador_miscampanas.js.map
