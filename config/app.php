<?php

return [

    'title' => 'eGlow',

    'title-header-mini' => 'img/logo.mini.png',

    'title-header-lg' => 'img/logo.png',

    'layout' => 'gentelella',

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'eGlow'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    | Set on layout as result of url('/')
    */

    'url' => env('APP_URL', 'https://eglowonline.com'),

    // Debe incluir la �ltima diagonal
    'url_servicios_backend' => env('URL_SERVICIOS_BACKEND', 'http://sabato.eglowonline.com/WebApi/Api/'),

    // Debe incluir la �ltima diagonal
    'url_servicios_twitter' => env('URL_SERVICIOS_TWITTER', 'https://eglow-dev-eglow.c9users.io/etltwitter/public/api/'),

    'url_servicios_facebook' => env('URL_SERVICIOS_FACEBOOK', 'https://eglow-dev-eglow.c9users.io/etlfacebook/public/api/v1.0/'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'America/Mexico_City',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'es',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'es',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'single'),

    'log_level' => env('APP_LOG_LEVEL', 'debug'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Package Service Providers...
         */

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
        // Agrega funcionalidades para respuestas en formato JSON
        App\Providers\ResponseMacroServiceProvider::class,

        /**
         * BootstrapForm
         * https://github.com/dwightwatson/bootstrap-form
         */
        Collective\Html\HtmlServiceProvider::class,
        // Watson\BootstrapForm\BootstrapFormServiceProvider::class,
        App\Customized\BootstrapForm\BootstrapFormCustomizedServiceProvider::class,

        /**
         * Column sorting for Laravel 5.*
         * https://github.com/Kyslik/column-sortable
         */
        Kyslik\ColumnSortable\ColumnSortableServiceProvider::class,

        /**
         * CORS Middleware for Laravel 5
         * https://github.com/barryvdh/laravel-cors
         */
        Barryvdh\Cors\ServiceProvider::class,

        /**
         * JSON Web Token Authentication for Laravel & Lumen
         * https://github.com/tymondesigns/jwt-auth/wiki/Installation
         */
        Tymon\JWTAuth\Providers\JWTAuthServiceProvider::class,

        /**
         * Laravel Driver for the Database Backup Manager
         * https://github.com/backup-manager/laravel
         */
        BackupManager\Laravel\Laravel55ServiceProvider::class,

        /**
         * Twitter API for Laravel 4/5
         * https://github.com/thujohn/twitter
         */
        \Thujohn\Twitter\TwitterServiceProvider::class,

        /**
         * Laravel Facebook SDK
         * https://github.com/SammyK/LaravelFacebookSdk
         */
        SammyK\LaravelFacebookSdk\LaravelFacebookSdkServiceProvider::class,

        /**
         * Laravel Storage driver with Google Drive API
         * https://gist.github.com/ivanvermeyen/cc7c59c185daad9d4e7cb8c661d7b89b
         */
        App\Providers\GoogleDriveServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,

        /**
         * BootstrapForm
         * https://github.com/dwightwatson/bootstrap-form
         */
        'Form'     => Collective\Html\FormFacade::class,
        'HTML'     => Collective\Html\HtmlFacade::class,
        'BootForm' => Watson\BootstrapForm\Facades\BootstrapForm::class,

        /**
         * JSON Web Token Authentication for Laravel & Lumen
         * https://github.com/tymondesigns/jwt-auth/wiki/Installation
         */
        'JWTAuth'    => Tymon\JWTAuth\Facades\JWTAuth::class,
        'JWTFactory' => Tymon\JWTAuth\Facades\JWTFactory::class,

        /**
         * Twitter API for Laravel 4/5
         * https://github.com/thujohn/twitter
         */
        // 'Twitter' => 'Thujohn\Twitter\Facades\Twitter',
        'Twitter' =>  'App\Customized\Twitter\Facades\Twitter',

        /**
         * Laravel Facebook SDK
         * https://github.com/SammyK/LaravelFacebookSdk
         */
        'Facebook' => SammyK\LaravelFacebookSdk\FacebookFacade::class,

    ],

];
