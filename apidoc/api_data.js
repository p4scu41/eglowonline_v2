define({ "api": [
  {
    "type": "get",
    "url": "/auth/user",
    "title": "Get User logged",
    "version": "1.0.0",
    "name": "GetUserJwt",
    "group": "AuthJWT",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>User Data</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.tipo_usuario_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.nombre",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "data.ultimo_acceso",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.activo",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "data.created_at",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Response 200 (example):",
          "content": "HTTP/1.1 200 OK\n{\n    \"extra\": null,\n    \"data\": {\n        \"id\": 631,\n        \"tipo_usuario_id\": 1,\n        \"nombre\": \"Nuevo Usuario\",\n        \"email\": \"usuario@mail.com\",\n        \"ultimo_acceso\": null,\n        \"activo\": 1,\n        \"created_at\": \"2017-03-06 21:58:46\"\n    },\n    \"message\": \"Solicitud procesada exitosamente\",\n    \"status\": 200,\n    \"error\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/V1/AuthController.php",
    "groupTitle": "AuthJWT",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token (Bearer Token)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Header (example):",
          "content": "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0L2VnbG93b25saW5lX3YyL3B1YmxpYy9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwNTg4MTU2MSwiZXhwIjoxNTA1OTAzMTYxLCJuYmYiOjE1MDU4ODE1NjEsImp0aSI6InZKNXFBd01tbXVZMkdoQzIifQ.DUHWveTuRYnZfdNfdJX47bP2cMuWIckJV0FZcGhZNJM",
          "type": "String"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenNotFound",
            "description": "<p><code>Token</code> absent</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenExpired",
            "description": "<p><code>Token</code> has expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenBlacklisted",
            "description": "<p>The <code>Token</code> has been blacklisted</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response 406 TokenNotFound (example):",
          "content": "HTTP/1.1 406 Not Acceptable\n{\n    \"extra\": null,\n    \"data\": [],\n    \"message\": \"Token ausente\",\n    \"status\": 406,\n    \"error\": 422\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenExpired (example):",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"extra\": {\n        \"message\": \"Token has expired\"\n    },\n    \"data\": [],\n    \"message\": \"Token expirado\",\n    \"status\": 401,\n    \"error\": 401\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenBlacklisted (example):",
          "content": "HTTP/1.1 401 Unauthorized\n    {\n        \"extra\": {\n             \"message\": \"The token has been blacklisted\"\n         },\n        \"data\": [],\n        \"message\": \"Token inválido\",\n        \"status\": 401,\n        \"error\": 401\n    }",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/auth/refresh",
    "title": "Refresh Token",
    "version": "1.0.0",
    "name": "RefreshToken",
    "group": "AuthJWT",
    "filename": "app/Http/Controllers/Api/V1/AuthController.php",
    "groupTitle": "AuthJWT",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token (Bearer Token)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Header (example):",
          "content": "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0L2VnbG93b25saW5lX3YyL3B1YmxpYy9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwNTg4MTU2MSwiZXhwIjoxNTA1OTAzMTYxLCJuYmYiOjE1MDU4ODE1NjEsImp0aSI6InZKNXFBd01tbXVZMkdoQzIifQ.DUHWveTuRYnZfdNfdJX47bP2cMuWIckJV0FZcGhZNJM",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.token",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.user_id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Response 200 Token (example):",
          "content": "HTTP/1.1 200 OK\n    {\n        \"extra\": null,\n        \"data\": {\n            \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0L2VnbG93b25saW5lX3YyL3B1YmxpYy9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwNTkyMzQyNywiZXhwIjoxNTA2MTM5NDI3LCJuYmYiOjE1MDU5MjM0MjcsImp0aSI6InE0MFJsRmN4Mk9qOFp1UHAifQ.mT7OXNhRJotDG3mpJGXIKxhyvyn6rTfYzlXuj_hYQ4w\",\n            \"user_id\": 631\n        },\n        \"message\": \"Solicitud procesada exitosamente\",\n        \"status\": 200,\n        \"error\": null\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenNotFound",
            "description": "<p><code>Token</code> absent</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenExpired",
            "description": "<p><code>Token</code> has expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenBlacklisted",
            "description": "<p>The <code>Token</code> has been blacklisted</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response 406 TokenNotFound (example):",
          "content": "HTTP/1.1 406 Not Acceptable\n{\n    \"extra\": null,\n    \"data\": [],\n    \"message\": \"Token ausente\",\n    \"status\": 406,\n    \"error\": 422\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenExpired (example):",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"extra\": {\n        \"message\": \"Token has expired\"\n    },\n    \"data\": [],\n    \"message\": \"Token expirado\",\n    \"status\": 401,\n    \"error\": 401\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenBlacklisted (example):",
          "content": "HTTP/1.1 401 Unauthorized\n    {\n        \"extra\": {\n             \"message\": \"The token has been blacklisted\"\n         },\n        \"data\": [],\n        \"message\": \"Token inválido\",\n        \"status\": 401,\n        \"error\": 401\n    }",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/auth/login",
    "title": "Users Login",
    "version": "1.0.0",
    "name": "UserLogin",
    "group": "AuthJWT",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request (example):",
          "content": "{\n    \"email\": \"usuario@mail.com\",\n    \"password\": \"asdf1234\",\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCredentials",
            "description": "<p>User or Password incorrect</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response 409 InvalidCredentials (example):",
          "content": "HTTP/1.1 409 Conflict\n    {\n        \"extra\": null,\n        \"data\": [],\n        \"message\": \"Usuario o Contraseña incorrecta\",\n        \"status\": 409,\n        \"error\": 409\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/V1/AuthController.php",
    "groupTitle": "AuthJWT",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.token",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.user_id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Response 200 Token (example):",
          "content": "HTTP/1.1 200 OK\n    {\n        \"extra\": null,\n        \"data\": {\n            \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0L2VnbG93b25saW5lX3YyL3B1YmxpYy9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwNTkyMzQyNywiZXhwIjoxNTA2MTM5NDI3LCJuYmYiOjE1MDU5MjM0MjcsImp0aSI6InE0MFJsRmN4Mk9qOFp1UHAifQ.mT7OXNhRJotDG3mpJGXIKxhyvyn6rTfYzlXuj_hYQ4w\",\n            \"user_id\": 631\n        },\n        \"message\": \"Solicitud procesada exitosamente\",\n        \"status\": 200,\n        \"error\": null\n    }",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/users/:id?return=true",
    "title": "Users Delete",
    "version": "1.0.0",
    "name": "DeleteUser",
    "group": "Users",
    "permission": [
      {
        "name": "Administrador"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_method",
            "description": "<p>DELETE</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request (example):",
          "content": "{\n    \"_method\": \"DELETE\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/V1/UserController.php",
    "groupTitle": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token (Bearer Token)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Header (example):",
          "content": "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0L2VnbG93b25saW5lX3YyL3B1YmxpYy9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwNTg4MTU2MSwiZXhwIjoxNTA1OTAzMTYxLCJuYmYiOjE1MDU4ODE1NjEsImp0aSI6InZKNXFBd01tbXVZMkdoQzIifQ.DUHWveTuRYnZfdNfdJX47bP2cMuWIckJV0FZcGhZNJM",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>User Data</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.tipo_usuario_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.nombre",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "data.ultimo_acceso",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.activo",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "data.created_at",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Response 200 (example):",
          "content": "HTTP/1.1 200 OK\n{\n    \"extra\": null,\n    \"data\": {\n        \"id\": 631,\n        \"tipo_usuario_id\": 1,\n        \"nombre\": \"Nuevo Usuario\",\n        \"email\": \"usuario@mail.com\",\n        \"ultimo_acceso\": null,\n        \"activo\": 1,\n        \"created_at\": \"2017-03-06 21:58:46\"\n    },\n    \"message\": \"Solicitud procesada exitosamente\",\n    \"status\": 200,\n    \"error\": null\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenNotFound",
            "description": "<p><code>Token</code> absent</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenExpired",
            "description": "<p><code>Token</code> has expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenBlacklisted",
            "description": "<p>The <code>Token</code> has been blacklisted</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Data not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Forbidden</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response 406 TokenNotFound (example):",
          "content": "HTTP/1.1 406 Not Acceptable\n{\n    \"extra\": null,\n    \"data\": [],\n    \"message\": \"Token ausente\",\n    \"status\": 406,\n    \"error\": 422\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenExpired (example):",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"extra\": {\n        \"message\": \"Token has expired\"\n    },\n    \"data\": [],\n    \"message\": \"Token expirado\",\n    \"status\": 401,\n    \"error\": 401\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenBlacklisted (example):",
          "content": "HTTP/1.1 401 Unauthorized\n    {\n        \"extra\": {\n             \"message\": \"The token has been blacklisted\"\n         },\n        \"data\": [],\n        \"message\": \"Token inválido\",\n        \"status\": 401,\n        \"error\": 401\n    }",
          "type": "json"
        },
        {
          "title": "Response 404 NotFound (example):",
          "content": "HTTP/1.1 404 Not Found\n    {\n        \"extra\": null,\n        \"data\": [],\n        \"message\": \"Recurso no encontrado\",\n        \"status\": 404,\n        \"error\": 404\n    }",
          "type": "json"
        },
        {
          "title": "Response 403 NoAccessRight (example):",
          "content": "HTTP/1.1 403 Forbidden\n    {\n        \"extra\": null,\n        \"data\": [],\n        \"message\": \"Acceso no autorizado\",\n        \"status\": 403,\n        \"error\": 403\n    }",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/users/:id",
    "title": "Users Show",
    "version": "1.0.0",
    "name": "GetUser",
    "group": "Users",
    "permission": [
      {
        "name": "Administrador"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>User Data</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.tipo_usuario_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.nombre",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "data.ultimo_acceso",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.activo",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "data.created_at",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.tipo_usuario",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.tipo_usuario.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.tipo_usuario.descripcion",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Response 200 (example):",
          "content": "HTTP/1.1 200 OK\n{\n    \"extra\": null,\n    \"data\": {\n        \"id\": 631,\n        \"tipo_usuario_id\": 1,\n        \"nombre\": \"Nuevo Usuario\",\n        \"email\": \"usuario@mail.com\",\n        \"ultimo_acceso\": \"2017-07-13 10:04:27\",\n        \"activo\": 1,\n        \"created_at\": \"2017-03-06 21:58:46\",\n        \"tipo_usuario\": {\n            \"id\": 5,\n            \"descripcion\": \"Administrador\"\n        }\n    },\n    \"message\": \"Solicitud procesada exitosamente\",\n    \"status\": 200,\n    \"error\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/V1/UserController.php",
    "groupTitle": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token (Bearer Token)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Header (example):",
          "content": "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0L2VnbG93b25saW5lX3YyL3B1YmxpYy9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwNTg4MTU2MSwiZXhwIjoxNTA1OTAzMTYxLCJuYmYiOjE1MDU4ODE1NjEsImp0aSI6InZKNXFBd01tbXVZMkdoQzIifQ.DUHWveTuRYnZfdNfdJX47bP2cMuWIckJV0FZcGhZNJM",
          "type": "String"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenNotFound",
            "description": "<p><code>Token</code> absent</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenExpired",
            "description": "<p><code>Token</code> has expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenBlacklisted",
            "description": "<p>The <code>Token</code> has been blacklisted</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Data not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Forbidden</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response 406 TokenNotFound (example):",
          "content": "HTTP/1.1 406 Not Acceptable\n{\n    \"extra\": null,\n    \"data\": [],\n    \"message\": \"Token ausente\",\n    \"status\": 406,\n    \"error\": 422\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenExpired (example):",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"extra\": {\n        \"message\": \"Token has expired\"\n    },\n    \"data\": [],\n    \"message\": \"Token expirado\",\n    \"status\": 401,\n    \"error\": 401\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenBlacklisted (example):",
          "content": "HTTP/1.1 401 Unauthorized\n    {\n        \"extra\": {\n             \"message\": \"The token has been blacklisted\"\n         },\n        \"data\": [],\n        \"message\": \"Token inválido\",\n        \"status\": 401,\n        \"error\": 401\n    }",
          "type": "json"
        },
        {
          "title": "Response 404 NotFound (example):",
          "content": "HTTP/1.1 404 Not Found\n    {\n        \"extra\": null,\n        \"data\": [],\n        \"message\": \"Recurso no encontrado\",\n        \"status\": 404,\n        \"error\": 404\n    }",
          "type": "json"
        },
        {
          "title": "Response 403 NoAccessRight (example):",
          "content": "HTTP/1.1 403 Forbidden\n    {\n        \"extra\": null,\n        \"data\": [],\n        \"message\": \"Acceso no autorizado\",\n        \"status\": 403,\n        \"error\": 403\n    }",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/users",
    "title": "Users List",
    "version": "1.0.0",
    "name": "GetUsers",
    "group": "Users",
    "permission": [
      {
        "name": "Administrador"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>Page</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "sort",
            "defaultValue": "created_at",
            "description": "<p>Field name by which the results will be sorted</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"asc\"",
              "\"desc\""
            ],
            "optional": true,
            "field": "order",
            "defaultValue": "asc",
            "description": "<p>Direction sort</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "filter",
            "description": "<p>Fields to be filtered</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request (example):",
          "content": "/users?page=2&sort=nombre&order=asc&filter[activo]=1",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "extra",
            "description": "<p>Pagintation data</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "extra.current_page",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Url",
            "optional": false,
            "field": "extra.first_page_url",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "extra.from",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "extra.last_page",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Url",
            "optional": false,
            "field": "extra.last_page_url",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Url",
            "optional": false,
            "field": "extra.next_page_url",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Url",
            "optional": false,
            "field": "extra.path",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "extra.per_page",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Url",
            "optional": false,
            "field": "extra.prev_page_url",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "extra.to",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "extra.total",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Users List</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.tipo_usuario_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.nombre",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "data.ultimo_acceso",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.activo",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "data.created_at",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.tipo_usuario",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.tipo_usuario.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.tipo_usuario.descripcion",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Response 200 (example):",
          "content": "HTTP/1.1 200 OK\n{\n    \"extra\": {\n        \"current_page\": 2,\n        \"first_page_url\": \"https://eglowonline.com/api/v1/users?page=1\",\n        \"from\": 16,\n        \"last_page\": 14,\n        \"last_page_url\": \"https://eglowonline.com/api/v1/users?page=14\",\n        \"next_page_url\": \"https://eglowonline.com/api/v1/users?page=3\",\n        \"path\": \"https://eglowonline.com/api/v1/users\",\n        \"per_page\": 15,\n        \"prev_page_url\": \"https://eglowonline.com/api/v1/users?page=1\",\n        \"to\": 30,\n        \"total\": 203\n    },\n    \"data\": [\n        {\n            \"id\": 380,\n            \"tipo_usuario_id\": 5,\n            \"nombre\": \"Ariel Levy\",\n            \"email\": \"Ariel_levy@mail.com\",\n            \"ultimo_acceso\": null,\n            \"activo\": 1,\n            \"created_at\": \"2017-03-06 21:58:46\",\n            \"tipo_usuario\": {\n                \"id\": 5,\n                \"descripcion\": \"Influenciador\"\n            }\n        },\n        {\n            \"id\": 553,\n            \"tipo_usuario_id\": 5,\n            \"nombre\": \"armando casas\",\n            \"email\": \"eglowtest@gmail.com\",\n            \"ultimo_acceso\": \"2017-07-13 10:04:27\",\n            \"activo\": 1,\n            \"created_at\": \"2017-03-06 21:58:46\",\n            \"tipo_usuario\": {\n                \"id\": 5,\n                \"descripcion\": \"Influenciador\"\n            }\n        },\n        {\n            \"id\": 502,\n            \"tipo_usuario_id\": 5,\n            \"nombre\": \"Arturo Supervicios\",\n            \"email\": \"supervicios@mail.com\",\n            \"ultimo_acceso\": null,\n            \"activo\": 1,\n            \"created_at\": \"2017-03-06 21:58:46\",\n            \"tipo_usuario\": {\n                \"id\": 5,\n                \"descripcion\": \"Influenciador\"\n            }\n        }\n    ],\n    \"message\": \"Solicitud procesada exitosamente\",\n    \"status\": 200,\n    \"error\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/V1/UserController.php",
    "groupTitle": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token (Bearer Token)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Header (example):",
          "content": "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0L2VnbG93b25saW5lX3YyL3B1YmxpYy9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwNTg4MTU2MSwiZXhwIjoxNTA1OTAzMTYxLCJuYmYiOjE1MDU4ODE1NjEsImp0aSI6InZKNXFBd01tbXVZMkdoQzIifQ.DUHWveTuRYnZfdNfdJX47bP2cMuWIckJV0FZcGhZNJM",
          "type": "String"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenNotFound",
            "description": "<p><code>Token</code> absent</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenExpired",
            "description": "<p><code>Token</code> has expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenBlacklisted",
            "description": "<p>The <code>Token</code> has been blacklisted</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Forbidden</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response 406 TokenNotFound (example):",
          "content": "HTTP/1.1 406 Not Acceptable\n{\n    \"extra\": null,\n    \"data\": [],\n    \"message\": \"Token ausente\",\n    \"status\": 406,\n    \"error\": 422\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenExpired (example):",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"extra\": {\n        \"message\": \"Token has expired\"\n    },\n    \"data\": [],\n    \"message\": \"Token expirado\",\n    \"status\": 401,\n    \"error\": 401\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenBlacklisted (example):",
          "content": "HTTP/1.1 401 Unauthorized\n    {\n        \"extra\": {\n             \"message\": \"The token has been blacklisted\"\n         },\n        \"data\": [],\n        \"message\": \"Token inválido\",\n        \"status\": 401,\n        \"error\": 401\n    }",
          "type": "json"
        },
        {
          "title": "Response 403 NoAccessRight (example):",
          "content": "HTTP/1.1 403 Forbidden\n    {\n        \"extra\": null,\n        \"data\": [],\n        \"message\": \"Acceso no autorizado\",\n        \"status\": 403,\n        \"error\": 403\n    }",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/users?return=true",
    "title": "Users Store",
    "version": "1.0.0",
    "name": "PostUser",
    "group": "Users",
    "permission": [
      {
        "name": "Administrador"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "allowedValues": [
              "1",
              "2",
              "3",
              "4",
              "5"
            ],
            "optional": false,
            "field": "tipo_usuario_id",
            "description": "<p>1 = Administrador, 2 = Agencia, 3 = Marca, 4 = Producto, 5 = Influenciador</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "allowedValues": [
              "0",
              "1"
            ],
            "optional": false,
            "field": "activo",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request (example):",
          "content": "{\n    \"tipo_usuario_id\": 1,\n    \"nombre\": \"Nuevo Usuario\",\n    \"email\": \"usuario@mail.com\",\n    \"password\": \"asdf1234\",\n    \"activo\": 1,\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "201 Created": [
          {
            "group": "201 Created",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>User Data</p>"
          },
          {
            "group": "201 Created",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": ""
          },
          {
            "group": "201 Created",
            "type": "Number",
            "optional": false,
            "field": "data.tipo_usuario_id",
            "description": ""
          },
          {
            "group": "201 Created",
            "type": "String",
            "optional": false,
            "field": "data.nombre",
            "description": ""
          },
          {
            "group": "201 Created",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": ""
          },
          {
            "group": "201 Created",
            "type": "Number",
            "optional": false,
            "field": "data.activo",
            "description": ""
          },
          {
            "group": "201 Created",
            "type": "DateTime",
            "optional": false,
            "field": "data.created_at",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Response 201 (example):",
          "content": "HTTP/1.1 201 Created\n{\n    \"extra\": null,\n    \"data\": {\n        \"id\": 631,\n        \"tipo_usuario_id\": 1,\n        \"nombre\": \"Nuevo Usuario\",\n        \"email\": \"usuario@mail.com\",\n        \"activo\": 1,\n        \"created_at\": \"2017-03-06 21:58:46\"\n    },\n    \"message\": \"Solicitud procesada exitosamente\",\n    \"status\": 201,\n    \"error\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/V1/UserController.php",
    "groupTitle": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token (Bearer Token)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Header (example):",
          "content": "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0L2VnbG93b25saW5lX3YyL3B1YmxpYy9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwNTg4MTU2MSwiZXhwIjoxNTA1OTAzMTYxLCJuYmYiOjE1MDU4ODE1NjEsImp0aSI6InZKNXFBd01tbXVZMkdoQzIifQ.DUHWveTuRYnZfdNfdJX47bP2cMuWIckJV0FZcGhZNJM",
          "type": "String"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenNotFound",
            "description": "<p><code>Token</code> absent</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenExpired",
            "description": "<p><code>Token</code> has expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenBlacklisted",
            "description": "<p>The <code>Token</code> has been blacklisted</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ValidationInputs",
            "description": "<p>Some field is not valid</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Forbidden</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response 406 TokenNotFound (example):",
          "content": "HTTP/1.1 406 Not Acceptable\n{\n    \"extra\": null,\n    \"data\": [],\n    \"message\": \"Token ausente\",\n    \"status\": 406,\n    \"error\": 422\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenExpired (example):",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"extra\": {\n        \"message\": \"Token has expired\"\n    },\n    \"data\": [],\n    \"message\": \"Token expirado\",\n    \"status\": 401,\n    \"error\": 401\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenBlacklisted (example):",
          "content": "HTTP/1.1 401 Unauthorized\n    {\n        \"extra\": {\n             \"message\": \"The token has been blacklisted\"\n         },\n        \"data\": [],\n        \"message\": \"Token inválido\",\n        \"status\": 401,\n        \"error\": 401\n    }",
          "type": "json"
        },
        {
          "title": "Response 422 ValidationInputs (example):",
          "content": "HTTP/1.1 422 Unprocessable Entity\n    {\n        \"extra\": {\n            \"email\": [\n                \"El campo E-mail no corresponde con una dirección de e-mail válida\"\n            ]\n        },\n        \"data\": [],\n        \"message\": \"Datos incorrectos\",\n        \"status\": 422,\n        \"error\": 422\n    }",
          "type": "json"
        },
        {
          "title": "Response 403 NoAccessRight (example):",
          "content": "HTTP/1.1 403 Forbidden\n    {\n        \"extra\": null,\n        \"data\": [],\n        \"message\": \"Acceso no autorizado\",\n        \"status\": 403,\n        \"error\": 403\n    }",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/users/:id?return=true",
    "title": "Users Update",
    "version": "1.0.0",
    "name": "PutUser",
    "group": "Users",
    "permission": [
      {
        "name": "Administrador"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id Users unique ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "allowedValues": [
              "1",
              "2",
              "3",
              "4",
              "5"
            ],
            "optional": true,
            "field": "tipo_usuario_id",
            "description": "<p>1 = Administrador, 2 = Agencia, 3 = Marca, 4 = Producto, 5 = Influenciador</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "nombre",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "password",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "allowedValues": [
              "0",
              "1"
            ],
            "optional": true,
            "field": "activo",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_method",
            "description": "<p>PUT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request (example):",
          "content": "{\n    \"tipo_usuario_id\": 1,\n    \"nombre\": \"Nuevo Usuario\",\n    \"email\": \"usuario@mail.com\",\n    \"password\": \"asdf1234\",\n    \"activo\": 1,\n    \"_method\": \"PUT\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/V1/UserController.php",
    "groupTitle": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token (Bearer Token)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Header (example):",
          "content": "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0L2VnbG93b25saW5lX3YyL3B1YmxpYy9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwNTg4MTU2MSwiZXhwIjoxNTA1OTAzMTYxLCJuYmYiOjE1MDU4ODE1NjEsImp0aSI6InZKNXFBd01tbXVZMkdoQzIifQ.DUHWveTuRYnZfdNfdJX47bP2cMuWIckJV0FZcGhZNJM",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>User Data</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.tipo_usuario_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.nombre",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "data.ultimo_acceso",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.activo",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "data.created_at",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Response 200 (example):",
          "content": "HTTP/1.1 200 OK\n{\n    \"extra\": null,\n    \"data\": {\n        \"id\": 631,\n        \"tipo_usuario_id\": 1,\n        \"nombre\": \"Nuevo Usuario\",\n        \"email\": \"usuario@mail.com\",\n        \"ultimo_acceso\": null,\n        \"activo\": 1,\n        \"created_at\": \"2017-03-06 21:58:46\"\n    },\n    \"message\": \"Solicitud procesada exitosamente\",\n    \"status\": 200,\n    \"error\": null\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenNotFound",
            "description": "<p><code>Token</code> absent</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenExpired",
            "description": "<p><code>Token</code> has expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenBlacklisted",
            "description": "<p>The <code>Token</code> has been blacklisted</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ValidationInputs",
            "description": "<p>Some field is not valid</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Data not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Forbidden</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response 406 TokenNotFound (example):",
          "content": "HTTP/1.1 406 Not Acceptable\n{\n    \"extra\": null,\n    \"data\": [],\n    \"message\": \"Token ausente\",\n    \"status\": 406,\n    \"error\": 422\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenExpired (example):",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"extra\": {\n        \"message\": \"Token has expired\"\n    },\n    \"data\": [],\n    \"message\": \"Token expirado\",\n    \"status\": 401,\n    \"error\": 401\n}",
          "type": "json"
        },
        {
          "title": "Response 401 TokenBlacklisted (example):",
          "content": "HTTP/1.1 401 Unauthorized\n    {\n        \"extra\": {\n             \"message\": \"The token has been blacklisted\"\n         },\n        \"data\": [],\n        \"message\": \"Token inválido\",\n        \"status\": 401,\n        \"error\": 401\n    }",
          "type": "json"
        },
        {
          "title": "Response 422 ValidationInputs (example):",
          "content": "HTTP/1.1 422 Unprocessable Entity\n    {\n        \"extra\": {\n            \"email\": [\n                \"El campo E-mail no corresponde con una dirección de e-mail válida\"\n            ]\n        },\n        \"data\": [],\n        \"message\": \"Datos incorrectos\",\n        \"status\": 422,\n        \"error\": 422\n    }",
          "type": "json"
        },
        {
          "title": "Response 404 NotFound (example):",
          "content": "HTTP/1.1 404 Not Found\n    {\n        \"extra\": null,\n        \"data\": [],\n        \"message\": \"Recurso no encontrado\",\n        \"status\": 404,\n        \"error\": 404\n    }",
          "type": "json"
        },
        {
          "title": "Response 403 NoAccessRight (example):",
          "content": "HTTP/1.1 403 Forbidden\n    {\n        \"extra\": null,\n        \"data\": [],\n        \"message\": \"Acceso no autorizado\",\n        \"status\": 403,\n        \"error\": 403\n    }",
          "type": "json"
        }
      ]
    }
  }
] });
