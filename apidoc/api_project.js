define({
  "name": "eGlow",
  "version": "1.0.0",
  "description": "Somos un marketplace que utiliza tecnología efectiva para crear campañas de marketing con celebrities en sus redes sociales.",
  "title": "eGlow - Influencers para tu marca",
  "url": "https://eglowonline.com/api/v1",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2017-09-20T18:28:05.842Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
