<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RegistroUsuarioNotification extends Notification
{
    use Queueable;

    public $data = [];

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            //->success()
            ->subject('Bienvenido a eGlow')
            ->greeting('¡Enhorabuena!')
            ->line('Gracias por registrarse en la plataforma, ahora eres parte de la familia eGlow.')
            ->line('Tu cuenta eglow está activa. Puedes acceder a tu perfil para ver cómo van tus estadísticas.')
            ->line('Tu contraseña de acceso es: ')
            ->line($this->data['password'])
            ->action('Incia sesión aquí', str_replace('wwww', '', url('influenciadores/login')));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
