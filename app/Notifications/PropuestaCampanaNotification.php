<?php

namespace App\Notifications;

use App\Models\Entorno;
use App\Models\Influenciador;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PropuestaCampanaNotification extends Notification
{
    use Queueable;

    public $data = [];

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notification = (new MailMessage)
            ->subject('¡Hay una nueva campaña para ti!')
            ->greeting('¡Hola!')
            ->line('Has recibido una propuesta de ' . $this->data['agencia'] . ' para realizar su próxima campaña.')
            ->line('Estos son los datos:')
            ->line('NOMBRE:        ' . $this->data['campana'])
            ->line('RED SOCIAL:    ' . $this->data['red_social'])
            ->line('PUBLICACIONES: ' . $this->data['publicaciones']);

        // if (! Influenciador::isInEntorno($this->data['influenciador_id'], Entorno::TELEVISA)) {
        if (!$this->data['is_televisa']) {
            $notification->line('MONTO:         ' . $this->data['precio']);
        }

        return $notification->line('')
            ->line('Si te interesa participar puedes ingresar a tu panel de eGlow o bien a través del siguiente link:')
            ->action('Ver oferta', str_replace('wwww', '', url('influenciadores/login')))
            ->line('')
            ->line('¡Éxitos!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
