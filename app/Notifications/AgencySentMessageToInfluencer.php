<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AgencySentMessageToInfluencer extends Notification
{
    use Queueable;

    /**
     * @var array the data for creating the notification
     */
    public $data;

    /**
     * Create a new notification instance.
     *
     * @param array $data
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Novedades de tu campaña')
            ->greeting('Hola')
            ->line($this->data['agency_name'] . ' te ha enviado un mensaje sobre su campaña ' . $this->data['campaign_name'])
            ->line('Ingresa tu panel de eGlow para enterarte o da click en el siguiente link:')
            ->action('Ver mensaje', str_replace('wwww', '', url('influenciadores/login')));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
