<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * @package App
 * @subpackage Notifications
 * @category Notification
 * @author Gerardo Adrián Gómez Ruiz <gerardo@eglow.mx>
 */
class CampaingOfferRespondedByInfluencer extends Notification
{
    use Queueable;

    /**
     * @var array the data for creating the notification
     */
    public $data;

    /**
     * Create a new notification instance.
     *
     * @param array $data
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * sends an email notifying the status from the given campaing to the agency
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Influenciador ha ' . $this->data['status_on_past'] . ' las propuestas de publicación.')
            ->greeting('Hola')
            ->line('El influenciador ' . $this->data['influencer_name'] . ' ha ' . $this->data['status_on_past'] . ' tu propuesta para realizar la campaña de "' . $this->data['campaign_name'] . '".')
            ->line('')
            ->line('Para ver su respuesta ingresa a tu panel de eGlow o bien a través del siguiente link:')
            ->action('Ver respuesta', str_replace('wwww', '', url('campanas/'.$this->data['campaign_id'])));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
