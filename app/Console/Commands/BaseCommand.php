<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use App\Support\ExceptionSupport;
use App\Models\Twitter\App;
use Illuminate\Console\Command;

/**
 * Base Command
 *
 * @package App\Console\Commands
 * @author  Pascual <pascual@importare.mx>
 *
 * @property string $signature
 * @property string $description
 *
 * @method public mixed handle()
 * @method public void handleException(Exception $exception)
 */
class BaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->initLogger('handle');
    }

    /**
     * Handle the exception thrown
     *
     * @param Exception $e          Exception instance thrown
     * @param string    $extra_data Extra information
     *
     * @return void
     */
    public function handleException($e, $extra_data)
    {
        $this->error_logger->error('ERROR ' . App::getInfoException($e));
        $this->error_logger->error(ExceptionSupport::getInfo($e, true, $extra_data));
        $this->error('ERROR ' . App::getInfoException($e));
    }
}
