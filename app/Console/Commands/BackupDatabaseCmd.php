<?php

namespace App\Console\Commands;

use App\Models\BackupDatabase;
use Illuminate\Console\Command;

/**
 * Backup Database Manager
 *
 * @package App\Console\Commands
 * @author  Pascual <pascual@importare.mx>
 */
class BackupDatabaseCmd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup database in a dump sql file.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $message = BackupDatabase::execute();

        if (strpos($message, 'ERROR') === false) {
            $this->info($message);
        } else {
            $this->error($message);
        }
    }
}
