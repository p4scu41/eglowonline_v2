<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Para registrar el cron en el servidor
 * php-cli -q /home/eglowusr/public_html/celebrityandvip/artisan schedule:run >> /dev/null 2>&1
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\BackupDatabaseCmd::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('backup:database')
            ->dailyAt('23:59')
            ->twiceDaily(12, 16)
            ->appendOutputTo(storage_path('logs'.DIRECTORY_SEPARATOR.'schedule_backup_database_'.date('Y-m-d').'.log'));

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
