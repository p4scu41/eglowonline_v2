<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 *
 * Implement the policies for index, create, store, show, edit, update and destroy
 */
class BasePolicy
{
    use HandlesAuthorization;

    /**
     * Resource name. It has to be the same as in the controller
     *
     * @var string
     */
    public $resource = '';

    /**
     * Model namespace.
     *
     * @var String
     */
    protected $modelNamespace = 'App\Models\\';

    /**
     * The model class associated with the Policy.
     *
     * @var string
     */
    protected $modelClass = '';

    /**
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  string  $ability
     * @return boolean
     */
    /*public function before($user, $ability)
    {
        return $user->isAdministrador();
    }*/

    /**
     * Determine whether the user can list Models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasPermisoModulo($this->resource, 'index');
    }

    /**
     * Determine whether the user can create Models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermisoModulo($this->resource, 'create');
    }

    /**
     * Determine whether the user can view the Model.
     *
     * @param  App\Models\User  $user
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return mixed
     */
    public function view(User $user, $model)
    {
        return $user->hasPermisoModulo($this->resource, 'view', $model);
    }

    /**
     * Determine whether the user can update the Model.
     *
     * @param  App\Models\User  $user
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return mixed
     */
    public function update(User $user, $model)
    {
        return $user->hasPermisoModulo($this->resource, 'update', $model);
    }

    /**
     * Determine whether the user can delete the Model.
     *
     * @param  App\Models\User  $user
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return mixed
     */
    public function delete(User $user, $model)
    {
        return $user->hasPermisoModulo($this->resource, 'delete', $model);
    }
}
