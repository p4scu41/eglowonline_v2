<?php

namespace App\Policies;

class InfluenciadorXCampanaPolicy extends BasePolicy
{
    public $resource = 'influenciadoresxcampana';
    protected $modelClass = 'InfluenciadorXCampana';
}
