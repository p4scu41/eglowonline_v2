<?php

namespace App\Policies;

class UsuarioPolicy extends BasePolicy
{
    public $resource = 'usuarios';
    protected $modelClass = 'User';
}
