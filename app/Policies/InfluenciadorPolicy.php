<?php

namespace App\Policies;

use App\Models\User;

class InfluenciadorPolicy extends BasePolicy
{
    public $resource = 'influenciadores';
    protected $modelClass = 'Influenciador';

    /**
     * Determine whether the user can update the Model.
     *
     * @param  App\Models\User  $user
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return mixed
     */
    public function update(User $user, $model)
    {
        $hasPermisoModulo = $user->hasPermisoModulo($this->resource, 'update', $model);

        // A los usuarios Influenciadores solo se les permite actualizar su propio perfil
        if ($user->isCelebridad()) {
            $hasPermisoModulo = $user->id == $model->usuario_id;
        }

        return $hasPermisoModulo;
    }

    /**
     * Determine whether the user can sync up the files jsons.
     *
     * @param  App\Models\User  $user
     * @return boolean
     */
    public function syncperfiles(User $user)
    {
        return $user->hasPermisoModulo($this->resource, 'syncperfiles');
    }

    /**
     * Determine whether the user can see the catalogo page
     *
     * @param  App\Models\User  $user
     * @return boolean
     */
    public function catalogo(User $user)
    {
        return $user->hasPermisoModulo($this->resource, 'catalogo');
    }
}
