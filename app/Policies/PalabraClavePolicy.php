<?php

namespace App\Policies;

class PalabraClavePolicy extends BasePolicy
{
    public $resource = 'palabrasclaves';
    protected $modelClass = 'PalabraClave';
}
