<?php

namespace App\Policies;

class PublicacionXInfluenciadorXCampanaPolicy extends BasePolicy
{
    public $resource = 'publicacionesxcampana';
    protected $modelClass = 'PublicacionXInfluenciadorXCampana';
}
