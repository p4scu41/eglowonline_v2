<?php

namespace App\Policies;

class CampanaPolicy extends BasePolicy
{
    public $resource = 'campanas';
    protected $modelClass = 'Campana';
}
