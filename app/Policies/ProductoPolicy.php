<?php

namespace App\Policies;

class ProductoPolicy extends BasePolicy
{
    public $resource = 'productos';
    protected $modelClass = 'Producto';
}
