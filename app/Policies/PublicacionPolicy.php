<?php

namespace App\Policies;

class PublicacionPolicy extends BasePolicy
{
    public $resource = 'publicaciones';
    protected $modelClass = 'Publicacion';
}
