<?php

namespace App\Policies;

class MarcaPolicy extends BasePolicy
{
    public $resource = 'marcas';
    protected $modelClass = 'Marca';
}
