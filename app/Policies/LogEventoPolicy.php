<?php

namespace App\Policies;

class LogEventoPolicy extends BasePolicy
{
    public $resource = 'logeventos';
    protected $modelClass = 'LogEvento';
}
