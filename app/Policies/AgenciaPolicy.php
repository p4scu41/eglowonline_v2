<?php

namespace App\Policies;

class AgenciaPolicy extends BasePolicy
{
    public $resource = 'agencias';
    protected $modelClass = 'Agencia';
}
