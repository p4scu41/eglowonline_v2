<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Implement the JSON Web Token Authentication Event Listeners
 *
 * @package App\Events
 * @author  Pascual <pascual@importare.mx>
 * @link https://github.com/tymondesigns/jwt-auth/wiki/Authentication
 */
class JWTEvents
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function absent()
    {
        return response()->jsonInvalidData(['message' => 'Token ausente', 'status' => 406]);
    }

    public function expired($e)
    {
        return response()->jsonJwtException($e, ['message' => 'Token expirado']);
    }

    public function invalid($e)
    {
        return response()->jsonJwtException($e, ['message' => 'Token inválido']);
    }

    public function user_not_found()
    {
        return response()->jsonNotFound(['message' => 'Token de Usuario no encontrado']);
    }

    /**
    * Check if the user is active
    */
    public function valid($user)
    {
        if (!$user->isActivo()) {
            return response()->jsonInvalidData(['message' => 'Token de Usuario inactivo']);
        }
    }
}
