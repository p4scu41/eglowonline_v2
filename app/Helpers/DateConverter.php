<?php
namespace App\Helpers;

/**
 * converts a date
 *
 * @package App
 * @subpackage Helpers
 * @category Helper
 * @author Gerardo Adrián Gómez Ruiz <gerardo@eglow.mx>
 */
class DateConverter
{
    /**
     * @var array the months with their representation
     */
    private static $months = [
        '01' => 'ENE',
        '02' => 'FEB',
        '03' => 'MAR',
        '04' => 'ABR',
        '05' => 'MAY',
        '06' => 'JUN',
        '07' => 'JUL',
        '08' => 'AGO',
        '09' => 'SEP',
        '10' => 'OCT',
        '11' => 'NOV',
        '12' => 'DIC'
    ];

    /**
     * converts the date: 2017 ABR 18 / 12:00:00
     *
     * @param string $date
     * @return string
     */
    public static function convert($date)
    {
        list($fecha, $hora) = explode(' ', $date);

        list($anio, $mes, $dia) = explode('-', $fecha);

        return $anio . ' ' . static::$months[$mes] . ' ' . $dia . ' / ' . $hora;
    }
}