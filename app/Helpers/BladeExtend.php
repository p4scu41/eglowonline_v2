<?php

namespace App\Helpers;

use Carbon\Carbon;

/**
 * Clase contendedora de funciones útiles para crear directivas de blade
 *
 * @package App\Helpers
 * @author  Pascual <pascual@importare.mx>
 *
 * @method static string boolean(mixed $data)
 * @method static string booleanThumb(mixed $data)
 * @method static string date(mixed $data)
 * @method static string time(mixed $data)
 * @method static string timeLong(mixed $data)
 * @method static string datetime(mixed $data)
 * @method static string datetimeShort(mixed $data)
 * @method static string boolean(mixed $data)
 * @method static string btnPrevius(string $resource)
 */
class BladeExtend
{
    /**
     * Devuelve 'Si' solo si el valor es 1 o true, de lo contrario devuelve 'No'
     *
     * @param mixed $data Dato a evaluar
     *
     * @return string
     */
    public static function boolean($data)
    {
        return $data ? 'Si' : 'No';
    }

    /**
     * Devuelve el html del icono fontasome con el pulgar hacia arriba o abajo
     * dependiendo del valor de $data
     *
     * @param mixed $data Dato a evaluar
     *
     * @return string
     */
    public static function booleanThumb($data)
    {
        return $data ?
            '<i class="fa fa-lg fa-thumbs-up text-success" data-toggle="tooltip" title="Si"></i>' :
            '<i class="fa fa-lg fa-thumbs-o-down text-danger" data-toggle="tooltip" title="No"></i>';
    }

    /**
     * Devuelve la fecha en formato d-m-Y
     *
     * @param mixed $data Dato a evaluar
     *
     * @return string
     */
    public static function date($data)
    {
        if ($data != null) {
            if (is_string($data)) {
                return Carbon::createFromFormat('Y-m-d', $data)->format('d-m-Y');
            } else {
                return $data->format('d-m-Y');
            }
        }
    }

    /**
     * Devuelve la hora en formato h:i a
     *
     * @param mixed $data Dato a evaluar
     *
     * @return string
     */
    public static function time($data)
    {
        if ($data != null) {
            if (is_string($data)) {
                return Carbon::createFromFormat('H:i:s', $data)->format('h:i a');
            } else {
                return $data->format('h:i a');
            }
        }

    }

    /**
     * Devuelve la hora en formato H:i
     *
     * @param mixed $data Dato a evaluar
     *
     * @return string
     */
    public static function timeLong($data)
    {
        if ($data != null) {
            if (is_string($data)) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $data)->format('H:i');
            } else {
                return $data->format('H:i');
            }
        }
    }

    /**
     * Devuelve la fecha hora en formato d-m-Y h:i a
     *
     * @param mixed $data Dato a evaluar
     *
     * @return string
     */
    public static function datetime($data)
    {
        if ($data != null) {
            if (is_string($data)) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $data)->format('d-m-Y h:i a');
            } else {
                return $data->format('d-m-Y h:i a');
            }
        }
    }

    /**
     * Devuelve la fecha en formato d-m-y
     *
     * @param mixed $data Dato a evaluar
     *
     * @return string
     */
    public static function datetimeShort($data)
    {
        if ($data != null) {
            if (is_string($data)) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $data)->format('d-m-Y');
            } else {
                return $data->format('d-m-Y');
            }
        }
    }

    /**
     * Devuelve el HTML de un botón para regresar
     *
     * @param string $resource Resource
     *
     * @return string
     */
    public static function btnPrevius($resource)
    {
        return '<div class="col-md-12 text-right">
                <a class="btn btn-default" href="'.(url()->previous() ?: url($resource)).'" role="button"
                    data-toggle="tooltip" title="Regresar">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>
            </div>';
    }
}
