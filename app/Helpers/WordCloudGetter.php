<?php

namespace App\Helpers;

use SixtyNine\Cloud\Builder\FiltersBuilder;
use SixtyNine\Cloud\Builder\WordsListBuilder;

class WordCloudGetter
{
    public static function build($text, $publicacion_id = null, $ultima_revision = null)
    {
        // $reg_exp_url = '/(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/';
        // $text = preg_replace($reg_exp_url, '', $text);
        $MIN_APARICIONES = 2; // Solo considera las palabras que aparece 2 o mas veces
        $keywords = [];
        $words = [];
        $total = 0;
        $removeWords = ['pasa', 'bien', 'como', 'hola', 'que', 'tal', 'ante', 'bajo', 'cabe', 'con', 'contra', 'desde',
            'durante', 'entre', 'hacia', 'hasta', 'mediante', 'para', 'por', 'según', 'sin', 'sobre', 'tras', 'vía',
            'uno', 'una', 'unos', 'unas', 'pero', 'solo', 'esta', 'está', 'hacer', 'todo', 'sale', 'más', 'mas', 'hice', 'este'];
        $clean = null;

        foreach ($removeWords as $value) {
            $clean[] = '';
        }

        $text = str_replace($removeWords, $clean, $text);

        $filters = FiltersBuilder::create()
            ->setRemoveNumbers(false)
            ->setRemoveTrailing(true)
            ->setRemoveUnwanted(true)
            ->setMinLength(3)
            ->build();

        $list = WordsListBuilder::create()
            ->setFilters($filters)
            ->importWords($text)
            ->build('palabras_clave_publicacion');

        foreach ($list->getWords() as $word) {
            if ($word->getCount() >= $MIN_APARICIONES) {
                $total += $word->getCount();

                $keywords[] = [
                    // 'publicacion_id'  => $publicacion_id,
                    'palabra'         => $word->getText(),
                    'cantidad'        => $word->getCount(),
                    // 'ultima_revision' => $ultima_revision,
                ];
            }
        }

        foreach ($keywords as $key) {
            $words[] = [
                'text' => $key['palabra'],
                'weight' => round($key['cantidad']/$total*100,1),
            ];
        }

        /*if (!empty($keywords)) {
            return collect($keywords)->sortByDesc('cantidad')->all();
        }*/

        return $words;
    }
}