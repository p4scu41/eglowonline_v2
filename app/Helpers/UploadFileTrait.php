<?php

namespace App\Helpers;

use Illuminate\Http\Request;

trait UploadFileTrait
{
    /**
     * The name of the input file
     *
     * @var string
     */
    // public $fileName = 'inputFile';

    public $fileRule = 'bail|image|max:2048';

    /**
     * Get the url relative to public
     * It has to be overrided to specify the folder to upload
     *
     * @return string
     */
    public function getBaseUploadFolder()
    {
        return 'img';
    }

    /**
     * Get the absolute path of the BaseUploadFolder
     *
     * @return string
     */
    public function getUploadFolder()
    {
        return public_path() . DIRECTORY_SEPARATOR . $this->getBaseUploadFolder();
    }

    /**
     * Find a file in the BaseUploadFolder base on the id name
     *
     * @return string
     */
    public function findFile()
    {
        $path = $this->getUploadFolder() . DIRECTORY_SEPARATOR . $this->id . '.*';
        $file = glob($path);

        if (count($file)) {
            return $file[0];
        }

        return null;
    }

    /**
     * Get the path relative to file base on BaseUploadFolder
     *
     * @return string
     */
    public function getFile()
    {
        $file = $this->findFile();

        if ($file != null) {
            $infoFile = pathinfo($file);

            return $this->getBaseUploadFolder() . DIRECTORY_SEPARATOR . $infoFile['basename'];
        }

        return null;
    }

    /**
     * Upload a file to BaseUploadFolder, the name is the ID
     *
     * @param  Illuminate\Http\UploadedFile $file
     * @return void
     * @throws \Exception
     */
    public function uploadFile($file)
    {
        $attribute = array_add([], $this->fileName, $file);
        $rule = array_add([], $this->fileName, $this->fileRule);

        if (!$file->isValid()) {
            throw new \Exception('Error al subir el archivo. ' . $file->getErrorMessage(), 422);
        }

        $validator = \Validator::make($attribute, $rule);

        if ($validator->fails()) {
            throw new \Exception($validator->errors()->first($this->fileName), 422);
        }

        $oldFile = $this->findFile();

        // Eliminamos el archivo anterior, si existe
        if ($oldFile != null) {
            unlink($oldFile);
        }

        $path = $file->move($this->getUploadFolder(), $this->id . '.' . $file->extension());

        if (!$path) {
            throw new \Exception('Error al subir el archivo.', 422);
        }

        chmod($path, 0777);
    }
}
