<?php

namespace App\Helpers;

use App\Support\ExceptionSupport;
use Exception;
use Illuminate\Support\Facades\Log;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Logger;
use Twitter;

class Helper
{

    /**
     * Implement array_filter recursive
     *
     * @param  array $array
     * @return array
     */
    public static function arrayFilterRecursive($array)
    {
        foreach ($array as &$value) {
            if (is_array($value)) {
                $value = static::array_filter_recursive($value);
            }
        }

        return array_filter($array);
    }

    /**
     * Convierte el array de los errores del model a una cadena de texto
     *
     * @param Array $arrayErrors Errores del modelo
     * @param string $join Default .
     * @return String
     */
    public static function errorsToString($arrayErrors, $join = ". ")
    {
        return array_reduce($arrayErrors, function ($result, $item) use ($join) {
            return $result . (
                empty($item) ? '' :
                (is_array($item) ? static::errorsToString($item) : ' '.$item . $join)
            );
        });
    }

    /**
     * Return the message of the exception if app.debug is true
     *
     * @param  Exception $e
     * @return string
     */
    public static function getMessageIfDebug($e)
    {
        Log::debug($e->getFile() . ' [' . $e->getLine() .']: ' . $e->getMessage());
        Log::debug(request()->input());
        Log::debug(ExceptionSupport::removeNoAppLinesFromTrace($e));

        if (config('app.debug')) {
            return $e->getFile() . ' [' . $e->getLine() .']: ' . $e->getMessage();
        }

        /**
         * Error Code
         * 422 = Validación
         * 404 = No encontrado
         * 403 = Acceso denegado
         */
        if ($e->getCode() == 422 || $e->getCode() == 404 || $e->getCode() == 403) {
            return $e->getMessage();
        }

        return '';
    }

    /**
     * Elimina espacios al inicio y final de la cadena
     * Elimina dobles espacios o más dentro de la cadena
     *
     * @param  string $value
     * @return string
     */
    public static function trim($value)
    {
        return preg_replace("'\s+'", ' ', trim($value));
    }

    /**
     * Elimina espacios al inicio y final de la cadena
     * Elimina dobles espacios o más dentro de la cadena
     *
     * @param  array $data
     * @return array
     */
    public static function trimArray($data)
    {
        return collect($data)->map(function ($value, $key) {
            // Si el valor es una cadena
            if (is_string($value)) {
                // Elimina espacios al inicio y final de la cadena
                // Elimina dobles espacios o más dentro de la cadena
                // Elimina etiquetas HTML y PHP
                return [$key => preg_replace("'\s+'", ' ', trim($value))];
            }

            return $value;
        })->toArray();
    }

    /**
     * Elimina espacios al inicio y final de la cadena
     * Elimina dobles espacios o más dentro de la cadena
     * Elimina etiquetas HTML y PHP
     *
     * @param  string $value
     * @return string
     */
    public static function stripTags($value)
    {
        return strip_tags(preg_replace("'\s+'", ' ', trim($value)));
    }

    /**
     * Elimina espacios al inicio y final de la cadena
     * Elimina dobles espacios o más dentro de la cadena
     * Elimina etiquetas HTML y PHP
     *
     * @param  array $data
     * @return array
     */
    public static function stripTagsArray($data)
    {
        return collect($data)->map(function ($value, $key) {
            // Si el valor es una cadena
            if (is_string($value)) {
                // Elimina espacios al inicio y final de la cadena
                // Elimina dobles espacios o más dentro de la cadena
                // Elimina etiquetas HTML y PHP
                return strip_tags(preg_replace("'\s+'", ' ', trim($value)));
            }

            return $value;
        })->toArray();

        return $result;
    }

    /**
     * Obtiene la cantidad en kilobytes de una expresion en cadena,
     * ejemplo 2M = 2048
     *
     * @param  string $val
     * @return int
     */
    public static function returnKilobytes($val)
    {
        $val = trim($val);
        $last = strtolower($val[strlen($val)-1]);
        $val  = substr($val, 0, -1);

        switch ($last) {
            case 'g':
                $val *= 1024;
                // The 'G' modifier is available since PHP 5.1.0
            case 'm':
                $val *= 1024;
        }

        return $val;
    }

    /**
     * Convierte los bytes a la unidad correspondiente
     * from http://php.net/manual/en/function.filesize.php
     */
    public static function formatBytes($bytes, $precision = 2)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    /**
     * Convierte la cantidad numerica a K o M
     */
    public static function formatNumber($cantidad)
    {
        $formatted = $cantidad;

        if ($cantidad >= 1000000) {
            $formatted = number_format($cantidad/1000000, 0) . 'M';
        } else if ($cantidad >= 1000) {
            $formatted = number_format($cantidad/1000, 0) . 'K';
        } else {
            $formatted = number_format($cantidad, 0);
        }

        return $formatted;
    }

    /**
     * Elimina la parte extra para multiplos de 100
     * ejemplo 1340, lo convierte a 1000
     */
    public static function floorToNumber($cantidad)
    {
        $formatted = $cantidad;

        if ($cantidad > 1000000) {
            $formatted = $cantidad - $cantidad%1000000;
        } else if ($cantidad > 1000) {
            $formatted = $cantidad - $cantidad%1000;
        }

        return $formatted;
    }

    /**
     * Convierte la cantidad númerica a su representativo en Kilobytes
     */
    public static function convertToKb($cantidad)
    {
        $formatted = $cantidad;

        $formatted = number_format(round($cantidad/1000, 0));

        return $formatted;
    }

    /**
     * Crea una instancia de Logger para poder hacer debug a un archivo especifico
     *
     * @param string $file Nombre del archivo donde se guarda el log
     *
     * @return Monolog\Logger
     */
    public static function createLogger($file)
    {
        $stream = new StreamHandler(storage_path().'/logs/' . $file, Logger::INFO);
        $stream->setFormatter(new LineFormatter("[%datetime%] %level_name%: %message% %context%\n", null, true));
        $log = new Logger('log');

        return $log->pushHandler($stream);
    }

    /**
     * Solicita los datos del usuario a Twitter para verificar si existe la cuenta
     *
     * @param $screen_name Nombre de usuario de la cuenta de Twitter
     *
     * @return boolean
     */
    public static function existsTwitter($screen_name)
    {
        try {
            Twitter::getUsers([
                'screen_name'      => str_replace('@', '', $screen_name),
                'include_entities' => 0,
            ]);

            return true;
        } catch (Exception $e) {
            $logs = Twitter::logs();
            $totalLogs = count($logs);
            $posicionInicialParameters = 3;
            $posicionIncrementoParameters = 6;

            // Acorta la longitud de la variable PARAMETERS a 200 caracteres,
            // debido a que cuando se sube una imagen o video este es muy extenso.
            for ($i=$posicionInicialParameters; $i < $totalLogs; $i+=$posicionIncrementoParameters) {
                $logs[$i] = substr($logs[$i], 0, 200);
            }

            error_log($e->getMessage());
            error_log(static::errorsToString($logs, "\r\n"));

            return false;
        }
    }

    /**
     * Convierte una fecha del $formatInput al $formatOutput
     *
     * @param string $date         Fecha
     * @param string $formatOutput Formato de salida. Default d/m/Y h:i a
     * @param string $formatInput  Formato de entrada. Default Y-m-d H:i:s
     *
     * @return string
     */
    public static function formatDate($date, $formatOutput = 'd/m/Y h:i a', $formatInput = 'Y-m-d H:i:s')
    {
        return \Carbon\Carbon::createFromFormat($formatInput, $date)->format($formatOutput);
    }

    /**
     * Valida la contraseña con las políticas de seguridad
     *
     * @throws Exception
     * @return void
     */
    public static function validPassword($password)
    {
        $longitudMinima      = 6;
        $mayusculas          = '/[A-Z]/';
        $minusculas          = '/[a-z]/';
        $numeros             = '/[0-9]/';
        $caracteres_simbolos = '/[áÁéÉíÍóÓúÚñÑüÜ\[\]\(\)\{\}\*\,\:\=\;\.\#\+\-\_\~\&\@\/\%\!\¡\¿\?\'\"]/';

        // - La longitud mínima debe ser de 6 caracteres
        if (strlen($password) < $longitudMinima) {
            throw new \Exception('La contraseña debe tener una longitud mínima de '.$longitudMinima.' caracteres', 422);
        }

        // - Letras mayúsculas
        if (!preg_match($mayusculas, $password)) {
            throw new \Exception('La contraseña debe contener letras en mayúsculas.', 422);
        }

        // - Letras minúsculas
        if (!preg_match($minusculas, $password)) {
            throw new \Exception('La contraseña debe contener letras en minúsculas.', 422);
        }

        // - Números
        if (!preg_match($numeros, $password)) {
            throw new \Exception('La contraseña debe contener números.', 422);
        }

        // - Números y/o símbolos {, ^, #, @, $, (, ), =, etc.
        // - Caracteres de puntuación (, ´ . - ! ?)
        if (!preg_match($caracteres_simbolos, $password)) {
            throw new \Exception('La contraseña debe contener símbolos o signos de puntuación', 422);
        }

        return true;
    }

    /**
     * Genera una contraseña aleatorio de 8 caracteres
     * cumpliendo con los lineamientos de las politicas de seguridad
     *
     * @return string
     */
    public static function passwordRandom()
    {
        $mayusculas          = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $minusculas          = 'abcdefghijklmnopqrstuvwxyz';
        $numeros             = '0123456789';
        $caracteres_simbolos = '[](){}*,:=;.#+-_\&@/%!?';

        return ($minusculas[rand(0,25)].
            $mayusculas[rand(0, 25)].
            $minusculas[rand(0, 25)].
            $caracteres_simbolos[rand(0, 22)].
            $mayusculas[rand(0, 25)].
            $numeros[rand(0, 9)].
            $caracteres_simbolos[rand(0, 22)].
            $numeros[rand(0, 9)]);
    }

    /**
     * Revisa si un elemento esta definido y además no es vacío
     *
     * @param array  $array Arreglo asociativo donde se buscará key
     * @param string $key   Llave a buscar en el arreglo
     *
     * @return boolean
     */
    public static function isSetAndNotEmpty($array, $key)
    {
        if (!is_array($array)) {
            return false;
        }

        if (isset($array[$key])) {
            if (!empty($array[$key])) {
                return true;
            }

            return false;
        }

        return false;
    }

    /**
     * Obtiene el nombre de la clase sin namespace
     *
     * @param Object $obj
     *
     * @return string
     */
    public static function getShortClassName($obj)
    {
        return (new \ReflectionClass($obj))->getShortName();
    }

    /**
     * Return info exception
     *
     * @param  Exception $e
     * @return string
     */
    public static function getInfoException($e)
    {
        return
            'File: ' . $e->getFile() .
            ', Line: ' . $e->getLine() .
            ', Message: ' . $e->getMessage() . PHP_EOL .
            'Input: ' . json_encode(request()->all()) . PHP_EOL .
            'Trace: ' . ExceptionSupport::removeNoAppLinesFromTrace($e);
    }
}
