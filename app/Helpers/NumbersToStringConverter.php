<?php
namespace App\Helpers;

/**
 * @package App
 * @subpackage Helpers
 * @category Converter
 * @author Gerardo Adrián Gómez Ruiz <gerardo@eglow.mx>
 */
class NumbersToStringConverter
{
    /**
     * @var array the ordinaries strings
     */
    private static $ordinaries = [
        '1ERO', '2DO', '3RO', '4TO', '5TO', '6TO', '7MO', '8VO', '9NO', '10MO'
    ];

    /**
     * transform $number to its ordinary equivalent
     *
     * @param int $number
     * @return string
     */
    public static function toOrdinary($number)
    {
        $key = $number;
        if (array_key_exists($key, self::$ordinaries)) {
            return self::$ordinaries[$key];
        }

        return '';
    }

    /**
     * transform the number to K, M, B
     *
     * @return string
     */
    public static function toThousands($number)
    {
        if ($number < 1000) {
            return (string) $number;
        }

        if ($number >= 1000 && $number < 1000000) {
            $result = $number / 1000;

            return round($result) . 'K';
        }

        if ($number >= 1000000 && $number < 1000000000000) {
            $result = $number / 1000000;

            return round($result) . 'M';
        }
    }
}