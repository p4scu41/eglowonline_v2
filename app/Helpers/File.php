<?php

namespace App\Helpers;

use DateTime;

/**
 * Files Helper
 *
 * @package App\Helpers
 * @author  Pascual <pascual@importare.mx>
 *
 * @property static array $ignore_delete
 *
 * @method static void delete()
 */
class File
{
    public static $ignore_delete = [
        '.gitignore',
    ];

    /**
     * Delete all files that match with the condition $pattern and older than $older_than from $path
     *
     * @param string $path       Folder to search files
     * @param string $pattern    Files to search. Default *.*
     * @param int    $older_than Delete file created max days. Default 0
     *
     * @return void
     */
    public static function delete($path, $pattern = '*.*', $older_than = 0)
    {
        if (!is_dir($path)) {
            return false;
        }

        $now = new DateTime('NOW');
        // Avoid repeat the DIRECTORY_SEPARATOR
        $full_path = str_replace(
            DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR,
            DIRECTORY_SEPARATOR,
            $path.DIRECTORY_SEPARATOR.$pattern
        );

        $ignore_delete_files = collect(static::$ignore_delete)->map(function ($item, $key) use ($path) {
            // Add absolute path to $ignore_delete
            return str_replace(
                DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR,
                DIRECTORY_SEPARATOR,
                $path.DIRECTORY_SEPARATOR.$item
            );
        })->all();

        // Array of files with the absolute path
        $files = glob($full_path);

        if (count($files)) {
            // Gets file modification time
            // Create associated array, the key is the file an the value is microtime
            // Example = [
            //   "/var/www/html/project/logs/log_queries.log" => 1495288380
            //   "/var/www/html/project/logs/log_requests.log" => 1495374780
            // ]
            $files = array_combine($files, array_map('filemtime', $files));

            // Sort by the time value, older to newer
            asort($files);

            foreach ($files as $file => $microtime) {
                // skip the ignore files
                if (in_array($file, $ignore_delete_files)) {
                    continue;
                }

                // Cast the microtime to Date object
                $fileDate = new DateTime('@'.$microtime);

                // Compare current date with the file date created
                $timeDiff = $fileDate->diff($now, true);

                // Check if the days diff is greater than $older_than
                if ($timeDiff->format('%a') >= $older_than) {
                    unlink($file);
                } else {
                    // Return due to the files are sort by date, the next files are younger
                    break;
                }
            }
        }
    }

    /**
     * Find the newest file
     *
     * @param string $path    Folder to search files
     * @param string $pattern Files to search. Default *.*
     *
     * @return string Absolute path of the newest file
     */
    public static function findNewest($path, $pattern = '*.*')
    {
        if (!is_dir($path)) {
            return false;
        }

        $now = new DateTime('NOW');
        // Avoid repeat the DIRECTORY_SEPARATOR
        $full_path = str_replace(
            DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR,
            DIRECTORY_SEPARATOR,
            $path.DIRECTORY_SEPARATOR.$pattern
        );

        // Array of files with the absolute path
        $files = glob($full_path);

        if (count($files)) {
            // Gets file modification time
            // Create associated array, the key is the file an the value is microtime
            // Example = [
            //   "/var/www/html/project/logs/log_queries.log" => 1495288380
            //   "/var/www/html/project/logs/log_requests.log" => 1495374780
            // ]
            $files = array_combine($files, array_map('filemtime', $files));

            // Sort by the time value, newer to older
            arsort($files);

            $paths_files = array_keys($files);

            return array_shift($paths_files);
        }

        return null;
    }
}
