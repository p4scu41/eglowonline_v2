<?php

namespace App\Customized\BootstrapForm;

use Watson\BootstrapForm\BootstrapFormServiceProvider;

class BootstrapFormCustomizedServiceProvider extends BootstrapFormServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(base_path() .'/vendor/watson/bootstrap-form/src/config/config.php', 'bootstrap_form');

        $this->app->singleton('bootstrap_form', function ($app) {
            return new BootstrapFormCustomized($app['html'], $app['form'], $app['config']);
        });
    }
}
