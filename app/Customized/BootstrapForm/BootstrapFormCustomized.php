<?php

namespace App\Customized\BootstrapForm;

use Watson\BootstrapForm\BootstrapForm;

/**
 * Customizing form-groups
 *
 * https://github.com/dwightwatson/bootstrap-form/issues/65
 */
class BootstrapFormCustomized extends BootstrapForm
{
    public $backup_options = [];
    public $columnNoLabelClass = '';
    public $divFormGroupClass = '';

    /**
     * Open a form while passing a model and the routes for storing or updating
     * the model. This will set the correct route along with the correct
     * method.
     *
     * @param  array  $options
     * @return string
     */
    public function open(array $options = [])
    {
        // Set the HTML5 role.
        $options['role'] = 'form';

        // Set the class for the form type.
        if (!array_key_exists('class', $options)) {
            $options['class'] = $this->getType();
        }

        if (array_key_exists('left_column_class', $options)) {
            $this->setLeftColumnClass($options['left_column_class']);
        }

        if (array_key_exists('left_column_offset_class', $options)) {
            $this->setLeftColumnOffsetClass($options['left_column_offset_class']);
        }

        if (array_key_exists('right_column_class', $options)) {
            $this->setRightColumnClass($options['right_column_class']);
        }

        if (array_key_exists('right_column_class', $options)) {
            $this->setRightColumnClass($options['right_column_class']);
        }

        if (array_key_exists('column_no_label_class', $options)) {
            $this->setColumnNoLabelClass($options['column_no_label_class']);
        }

        if (array_key_exists('div_form_group_class', $options)) {
            $this->setDivFormGroupClass($options['div_form_group_class']);
        }

        array_forget($options, [
            'left_column_class',
            'left_column_offset_class',
            'right_column_class',
            'column_no_label_class',
            'div_form_group_class',
        ]);

        if (array_key_exists('model', $options)) {
            return $this->model($options);
        }

        return $this->form->open($options);
    }

    /**
     * Backup the originals options of the form
     *
     * @return void
     */
    public function backupOptions()
    {
        $this->backup_options['left_column_class']        = $this->getLeftColumnClass();
        $this->backup_options['right_column_class']       = $this->getRightColumnClass();
        $this->backup_options['left_column_offset_class'] = $this->getLeftColumnOffsetClass();
        $this->backup_options['column_no_label_class']    = $this->getColumnNoLabelClass();
        $this->backup_options['div_form_group_class']     = $this->getDivFormGroupClass();
    }

    /**
     * Restore the originals options of the form
     *
     * @return void
     */
    public function restoreOptions()
    {
        $this->setLeftColumnClass($this->backup_options['left_column_class']);
        $this->setRightColumnClass($this->backup_options['right_column_class']);
        $this->setLeftColumnOffsetClass($this->backup_options['left_column_offset_class']);
        $this->setColumnNoLabelClass($this->backup_options['column_no_label_class']);
        $this->setDivFormGroupClass($this->backup_options['div_form_group_class']);
    }

    /**
     * Set the the options left_column_class, right_column_class, left_column_offset_class if exists
     *
     * @param array $options
     * @return void
     */
    public function setOptions(&$options)
    {
        if (isset($options['left_column_class'])) {
            $this->setLeftColumnClass($options['left_column_class']);
            unset($options['left_column_class']);
        }

        if (isset($options['right_column_class'])) {
            $this->setRightColumnClass($options['right_column_class']);
            unset($options['right_column_class']);
        }

        if (isset($options['left_column_offset_class'])) {
            $this->setLeftColumnOffsetClass($options['left_column_offset_class']);
            unset($options['left_column_offset_class']);
        }

        if (isset($options['column_no_label_class'])) {
            $this->setColumnNoLabelClass($options['column_no_label_class']);
            unset($options['column_no_label_class']);
        }

        if (isset($options['div_form_group_class'])) {
            $this->setDivFormGroupClass($options['div_form_group_class']);
            unset($options['div_form_group_class']);
        }
    }

    /**
     * Get the column class for the right column when no have label.
     *
     * @return string
     */
    public function getColumnNoLabelClass()
    {
        return $this->columnNoLabelClass ?: $this->config->get('bootstrap_form.column_no_label_class');
    }

    /**
     * Set the column class for the right column when no have label.
     *
     * @param  string  $lcass
     * @return void
     */
    public function setColumnNoLabelClass($class)
    {
        $this->columnNoLabelClass = $class;
    }

    /**
     * Get the class for the form-group div.
     *
     * @return string
     */
    public function getDivFormGroupClass()
    {
        return $this->divFormGroupClass ?: $this->config->get('bootstrap_form.div_form_group_class');
    }

    /**
     * Set the class for the form-group div.
     *
     * @param  string  $lcass
     * @return void
     */
    public function setDivFormGroupClass($class)
    {
        $this->divFormGroupClass = $class;
    }

    /**
     * Get a form group comprised of a label, form element and errors.
     *
     * @param  string  $name
     * @param  string  $value
     * @param  string  $element
     * @param  string|array  $divOptions If is string it set as class, defualt col-xs-12 col-sm-12 col-md-6
     * @return string
     */
    protected function getFormGroupWithLabel($name, $value, $element, $divOptions = '')
    {
        if (empty($divOptions)) {
            $divOptions = $this->getDivFormGroupClass();
        }

        # Convert string to class
        if (is_string($divOptions)) {
            $divOptions = ['class' => $divOptions];
        }

        $options = $this->getFormGroupOptions($name, $divOptions);

        return '<div' . $this->html->attributes($options) . '>' .
                $this->label($name, $value) . $element .
            '</div>';
    }

    /**
     * Get a form group.
     *
     * @param  string  $name
     * @param  string  $element
     * @return string
     */
    public function getFormGroup($name = null, $element, $divOptions = '')
    {
        if (empty($divOptions)) {
            $divOptions = $this->getDivFormGroupClass();
        }

        # Convert string to class
        if (is_string($divOptions)) {
            $divOptions = ['class' => $divOptions];
        }

        $options = $this->getFormGroupOptions($name, $divOptions);

        return '<div' . $this->html->attributes($options) . '>' .
                $element .
            '</div>';
    }

    /**
     * Merge the options provided for a form group with the default options
     * required for Bootstrap styling.
     *
     * @param  string $name
     * @param  array  $options
     * @return array
     */
    protected function getFormGroupOptions($name = null, array $options = [])
    {
        $class = 'form-group';

        if ($name) {
            $class .= ' ' . $this->getFieldErrorClass($name);
        }

        if (!empty($options['class'])) {
            $class .= ' '.$options['class']; # Append, not overwrite
            unset($options['class']);
        }

        return array_merge(['class' => $class], $options);
    }

    /**
     * Create a text field with a prepended glyphicon
     * @param string $name
     * @param string $icon
     * @param string $label
     * @param string $value
     * @param array $options
     * @return string
     */
    public function textWithPrepend($name, $icon, $label = null, $value = null, array $options = [])
    {
        $label = $this->getLabelTitle($label, $name);

        $options = $this->getFieldOptions($options);
        $inputElement = $this->form->text($name, $value, $options);

        $inputGroup = '<div class="input-group"><span class="input-group-addon">'.
            '<span class="glyphicon glyphicon-' . $icon .
            '" aria-hidden="true"></span></span>'. $inputElement .'</div>';

        $wrapperOptions = $this->isHorizontal() ? ['class' => $this->getRightColumnClass()] : [];
        $wrapperElement = '<div' . $this->html->attributes($wrapperOptions) . '>' .
                $inputGroup . $this->getFieldError($name) .
            '</div>';

        return $this->getFormGroupWithLabel($name, $label, $wrapperElement);
    }

    /**
     * Create a text field with an appended glyphicon
     * @param string $name
     * @param string $icon
     * @param string $label
     * @param string $value
     * @param array $options
     * @return string
     */
    public function textWithAppend($name, $icon, $label = null, $value = null, array $options = [])
    {
        $label = $this->getLabelTitle($label, $name);

        $options = $this->getFieldOptions($options);
        $inputElement = $this->form->text($name, $value, $options);

        $inputGroup = '<div class="input-group">'. $inputElement .'<span class="input-group-addon">'.
            '<span class="glyphicon glyphicon-' . $icon .
            '" aria-hidden="true"></span></span></div>';

        $wrapperOptions = $this->isHorizontal() ? ['class' => $this->getRightColumnClass()] : [];
        $wrapperElement = '<div' . $this->html->attributes($wrapperOptions) . '>' .
                $inputGroup . $this->getFieldError($name) .
            '</div>';

        return $this->getFormGroupWithLabel($name, $label, $wrapperElement);
    }

    /**
     * Create the input group for an element with the correct classes for errors.
     *
     * @param  string  $type
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function input($type, $name, $label = null, $value = null, array $options = [])
    {
        $label = $label == false ? false : $this->getLabelTitle($label, $name);
        $div_form_group_class = [];

        if (isset($options['div_form_group_class'])) {
            $div_form_group_class = $options['div_form_group_class'];
            unset($options['div_form_group_class']);
        }

        $options = $this->getFieldOptions($options, $name);

        if (isset($options['icon'])) {
            $icon = $options['icon'];
            unset($options['icon']);

            $inputElement = $type === 'password' ?
                $this->form->password($name, $options) :
                $this->form->{$type}($name, $value, $options);

            $inputElement = '<div class="input-group">
                <span class="input-group-addon">'.$icon.'</span>
                '.$inputElement.'
            </div>';
        } else {
            $inputElement = $type === 'password' ?
                $this->form->password($name, $options) :
                $this->form->{$type}($name, $value, $options);
        }

        $wrapperOptions = $this->isHorizontal() ? (
                $label == false ? ['class' => $this->getColumnNoLabelClass()] :
                ['class' => $this->getRightColumnClass()]
            ) : [];
        $wrapperElement = '<div' . $this->html->attributes($wrapperOptions) . '>' .
                $inputElement .
                $this->getFieldError($name) .
                $this->getHelpText($name, $options) .
            '</div>';

        if ($label == false) {
            return $this->getFormGroup(
                $name,
                $wrapperElement,
                $div_form_group_class
            );
        }

        return $this->getFormGroupWithLabel(
            $name,
            $label,
            $wrapperElement,
            $div_form_group_class
        );
    }

    /**
     * Create a select box field.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  array   $list
     * @param  string  $selected
     * @param  array   $options
     * @return string
     */
    public function select($name, $label = null, $list = [], $selected = null, array $options = [])
    {
        $label = $label == false ? false : $this->getLabelTitle($label, $name);
        $div_form_group_class = [];

        if (isset($options['div_form_group_class'])) {
            $div_form_group_class = $options['div_form_group_class'];
            unset($options['div_form_group_class']);
        }

        $options = $this->getFieldOptions($options, $name);

        if (isset($options['icon'])) {
            $icon = $options['icon'];
            unset($options['icon']);

            $inputElement = $this->form->select($name, $list, $selected, $options);

            $inputElement = '<div class="input-group">
                <span class="input-group-addon">'.$icon.'</span>
                '.$inputElement.'
            </div>';
        } else {
            $inputElement = $this->form->select($name, $list, $selected, $options);
        }

        $wrapperOptions = $this->isHorizontal() ? (
                $label == false ? ['class' => $this->getColumnNoLabelClass()] :
                ['class' => $this->getRightColumnClass()]
            ) : [];

        $wrapperElement = '<div' . $this->html->attributes($wrapperOptions) . '>' .
                $inputElement .
                $this->getFieldError($name) .
                $this->getHelpText($name, $options) .
            '</div>';

        if ($label == false) {
            return $this->getFormGroup(
                $name,
                $wrapperElement,
                $div_form_group_class
            );
        }

        return $this->getFormGroupWithLabel(
            $name,
            $label,
            $wrapperElement,
            $div_form_group_class
        );
    }

    /**
     * Create a Bootstrap textarea field input.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function textarea($name, $label = null, $value = null, array $options = [])
    {
        $this->backupOptions($options);
        $this->setOptions($options);

        $textarea = $this->input('textarea', $name, $label, $value, $options);

        $this->restoreOptions();

        return $textarea;
    }

    /**
     * Create a Bootstrap checkbox input.
     *
     * @param  string   $name
     * @param  string   $label
     * @param  string   $value
     * @param  bool     $checked
     * @param  array    $options
     * @return string
     */
    public function checkbox($name, $label = null, $value = 1, $checked = null, array $options = [])
    {
        $div_form_group_class = [];

        if (isset($options['div_form_group_class'])) {
            $div_form_group_class = $options['div_form_group_class'];
            unset($options['div_form_group_class']);
        }

        $inputElement = $this->checkboxElement($name, $label, $value, $checked, false, $options);

        $wrapperOptions = $this->isHorizontal() ?
            ['class' => implode(' ', [$this->getLeftColumnOffsetClass(), $this->getRightColumnClass()])] :
            [];
        $wrapperElement = '<div' . $this->html->attributes($wrapperOptions) . '> ' .
                $inputElement .
            '</div>';

        return $this->getFormGroup(
            null,
            $wrapperElement,
            $div_form_group_class
        );
    }

    /**
     * Create a single Bootstrap checkbox element.
     *
     * @param  string   $name
     * @param  string   $label
     * @param  string   $value
     * @param  bool     $checked
     * @param  bool     $inline
     * @param  array    $options
     * @return string
     */
    public function checkboxElement($name, $label = null, $value = 1, $checked = null, $inline = false, array $options = [])
    {
        $label = $this->getLabelTitle($label, $name);

        $labelOptions = $inline ? ['class' => 'checkbox-inline'] : [];

        $inputElement = $this->form->checkbox($name, $value, $checked, $options);
        $labelElement = '<label ' . $this->html->attributes($labelOptions) . '>' .
                $inputElement . ' ' .
                $label .
            '</label>';

        return $inline ? $labelElement : '<div class="checkbox">' . $labelElement . '</div>';
    }

    /**
     * Create a Bootstrap radio input.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  bool    $checked
     * @param  array   $options
     * @return string
     */
    public function radio($name, $label = null, $value = null, $checked = null, array $options = [])
    {
        $div_form_group_class = [];

        if (isset($options['div_form_group_class'])) {
            $div_form_group_class = $options['div_form_group_class'];
            unset($options['div_form_group_class']);
        }

        $inputElement = $this->radioElement($name, $label, $value, $checked, false, $options);

        $wrapperOptions = $this->isHorizontal() ?
            ['class' => implode(' ', [$this->getLeftColumnOffsetClass(), $this->getRightColumnClass()])] :
            [];
        $wrapperElement = '<div' . $this->html->attributes($wrapperOptions) . '>' .
                $inputElement .
            '</div>';

        return $this->getFormGroup(
            null,
            $wrapperElement,
            $div_form_group_class
        );
    }

    /**
     * Create a Boostrap button.
     *
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function button($value = null, array $options = [])
    {
        $options = array_merge(['class' => 'btn btn-primary'], $options);
        $this->backupOptions($options);
        $this->setOptions($options);

        $inputElement = $this->form->button($value, $options);

        $wrapperOptions = $this->isHorizontal() ?
            ['class' => implode(' ', [$this->getLeftColumnOffsetClass(), $this->getRightColumnClass()])] :
            [];
        $wrapperElement = '<div' . $this->html->attributes($wrapperOptions) . '>'.
                $inputElement .
            '</div>';

        $field = $this->getFormGroup(null, $wrapperElement, $this->getDivFormGroupClass());

        $this->restoreOptions();

        return $field;
    }

    /**
     * Create a Boostrap file upload button.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  array   $options
     * @return string
     */
    public function file($name, $label = null, array $options = [])
    {
        $label = $label == false ? false : $this->getLabelTitle($label, $name);
        $div_form_group_class = [];

        if (isset($options['div_form_group_class'])) {
            $div_form_group_class = $options['div_form_group_class'];
            unset($options['div_form_group_class']);
        }

        $options = array_merge(['class' => 'filestyle', 'data-buttonBefore' => 'true'], $options);

        $options = $this->getFieldOptions($options, $name);

        if (isset($options['icon'])) {
            $icon = $options['icon'];
            unset($options['icon']);

            $inputElement = $this->form->input('file', $name, null, $options);

            $inputElement = '<div class="input-group">
                <span class="input-group-addon">'.$icon.'</span>
                '.$inputElement.'
            </div>';
        } else {
            $inputElement = $this->form->input('file', $name, null, $options);
        }

        $wrapperOptions = $this->isHorizontal() ? (
                $label == false ? ['class' => $this->getColumnNoLabelClass()] :
                ['class' => $this->getRightColumnClass()]
            ) : [];
        $wrapperElement = '<div' . $this->html->attributes($wrapperOptions) . '>' .
                $inputElement .
                $this->getFieldError($name) .
                $this->getHelpText($name, $options) .
            '</div>';



        if ($label == false) {
            return $this->getFormGroup(
                $name,
                $wrapperElement,
                $div_form_group_class
            );
        }

        return $this->getFormGroupWithLabel(
            $name,
            $label,
            $wrapperElement,
            $div_form_group_class
        );
    }

    /**
     * Get the first error for a given field, using the provided
     * format, defaulting to the normal Bootstrap 3 format.
     *
     * @param  string  $field
     * @param  string  $format
     * @return mixed
     */
    protected function getFieldError($field, $format = '<span id="{field}-error" class="help-block">:message</span>')
    {
        $field = $this->flattenFieldName($field);
        // Para compatibilidad con jQuery Validation Plugin
        $format = str_replace('{field}', $field, $format);

        if ($this->getErrors()) {
            $allErrors = $this->config->get('bootstrap_form.show_all_errors');

            if ($allErrors) {
                return implode('', $this->getErrors()->get($field, $format));
            }

            return $this->getErrors()->first($field, $format);
        }

        return str_replace('>:message<', '><', $format);
    }

    /**
     * Return the error class if the given field has associated
     * errors, defaulting to the normal Bootstrap 3 error class.
     *
     * @param  string  $field
     * @param  string  $class
     * @return string
     */
    protected function getFieldErrorClass($field, $class = 'has-error')
    {
        $errors = $this->form->getSessionStore()->get('errors');

        return empty($errors) ? null : ( $errors->has($field) ? $class : null );
    }
}
