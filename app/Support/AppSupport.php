<?php

namespace App\Support;

/**
 * App support helper functions
 *
 * @package App\Support
 * @author  Pascual <pascual@importare.mx>
 *
 * @method static string subOneToBigInt(mixed $big_int)
 */
class AppSupport
{
    /**
     * Substrat one from bigint
     *
     * @param mixed $big_int Int or BigInt
     *
     * @return string
     */
    public static function subOneToBigInt($big_int)
    {
        // Cast as string due to compatibility with 32 bits
        $big_int_str = '' . $big_int;

        // Extract the last 4 digits as string
        $last_four_digits = substr($big_int_str, -4);
        // sub 1 from las 4 digits
        $last_four_digits = intval($last_four_digits) - 1;

        // replace last 4 digits
        $big_int_str = substr($big_int_str, 0, strlen($big_int_str)-4) . $last_four_digits;

        return $big_int_str;
    }
}
