<?php

namespace App\Support;

// use Auth;
use Exception;
use Illuminate\Support\Facades\Log;

/**
 * Exception support helper functions
 *
 * @package App\Support
 * @author  Pascual <pascual@importare.mx>
 *
 * @method static string function removeNoAppLines(Exception $e)
 * @method static string getInfo(Exception $e, boolean $include_trace = true, string $extra_data = '')
 * @method static string getMessageIfDebug(Exception $e, Monolog\Logger $logger = null, string $extra_data = '')
 * @method static boolean isDuplicatedKey(Exception $e)
 * @method static boolean isNotFound(Exception $e)
 * @method static boolean isForeignKeyNoExists(Exception $e)
 */
class ExceptionSupport
{
    /**
     * Remove all lines no reference to App from getTraceAsString
     *
     * @param Exception $e Exception Instance
     *
     * @return string
     */
    public static function removeNoAppLinesFromTrace(Exception $e)
    {
        $lines = explode(PHP_EOL, $e->getTraceAsString());
        $info  = '';

        if (empty($lines) || count($lines) == 1) {
            $lines = explode("\n", $e->getTraceAsString());
        }

        if (!empty($lines)) {
            $filtered = preg_grep('/APP/i', $lines);

            if (!empty($filtered)) {
                $info = implode(PHP_EOL, $filtered);
            }
        }

        return $info;
    }

    /**
     * Get information as string of the Exception
     *
     * @param Exception $e             Exception Instance
     * @param boolean   $include_trace Wheter or not include Trace. Default true
     * @param string    $extra_data    Extra information useful
     *
     * @return string
     */
    public static function getInfo(Exception $e, $include_trace = true, $extra_data = '')
    {
        $file = str_replace(base_path() . DIRECTORY_SEPARATOR, '', $e->getFile()) . ':' . $e->getLine();
        $info = /*PHP_EOL . "\t" .*/
            'Request: ' . request()->method() . ' ' .  request()->fullUrl() . PHP_EOL . "\t" .
            // 'Request IP: '      . request()->ip() . PHP_EOL . "\t" .
            // 'Request Headers: ' . json_encode(request()->header()) . PHP_EOL . "\t" .
            // 'Request Server: '  . json_encode(request()->server()) . PHP_EOL . "\t" .
            'File: '    . $file . PHP_EOL . "\t" .
            // (Auth::check() ? 'Session User: ' . json_encode(Auth::user()) . PHP_EOL . "\t" : '' ) .
            'Exception: ' . get_class($e) . '['.$e->getCode().']: ' . $e->getMessage() .
            (count(request()->all()) ? PHP_EOL . "\t" . 'Data: ' . json_encode(request()->all()) : '').
            (!empty($extra_data) ? PHP_EOL . "\t" . 'Extra Data: ' . $extra_data : '').
            ($include_trace ? PHP_EOL . static::removeNoAppLinesFromTrace($e) : '');

        return $info;
    }

    /**
     * Return the message of the exception if app.debug is true
     *
     * @param Exception      $e          Instancia de Exception
     * @param Monolog\Logger $logger     Logger Instance
     * @param string         $extra_data Extra information useful
     *
     * @return string
     */
    public static function getMessageIfDebug(Exception $e, $logger = null, $extra_data = '')
    {
        $info = static::getInfo($e, true, $extra_data);

        if (!empty($logger)) {
            $logger->error($info);
        } else {
            Log::error($info);
        }

        if (config('app.debug')) {
            return str_replace(base_path() . DIRECTORY_SEPARATOR, '', $e->getFile()) . ':' . $e->getLine() .
                '. Code ' . $e->getCode() . '. ' .$e->getMessage();
        }

        /**
         * Error Code
         * 422 = Validación
         * 404 = No encontrado
         * 403 = Acceso denegado
         */
        if ($e->getCode() == 422 || $e->getCode() == 404 || $e->getCode() == 403) {
            return $e->getMessage();
        }

        return '';
    }

    /**
     * Check if the Exception Thrown is by duplicaded key
     *
     * @param Exception $e Exception trhown
     *
     * @return boolean
     */
    public static function isDuplicatedKey(Exception $e)
    {
        // PostgreSQL => SQLSTATE[23505]: Unique violation: 7 ERROR:  llave duplicada viola restricción de unicidad
        return $e->getCode() == 23505;
    }

    /**
     * Check if the code of the exception is 404
     *
     * @param Exception $e Exception Instance
     *
     * @return boolean
     */
    public static function isNotFound(Exception $e)
    {
        return $e->getCode() == 404;
    }

    /**
     * Check if the Exception Thrown is by foreign key not exists
     *
     * @param Exception $e Exception thrown
     *
     * @return boolean
     */
    public static function isForeignKeyNoExists(Exception $e)
    {
        // PostgreSQL => SQLSTATE[23503]: Foreign key violation 7 ERROR:  inserción o actualización
        // en la tabla A viola la llave foránea FK
        return $e->getCode() == 23503;
    }
}
