<?php

namespace App\Exceptions;

use App\Support\ExceptionSupport;
use Auth;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        // Add extra data to debug de Exception
        if ($this->shouldReport($exception)) {
            Log::debug(ExceptionSupport::getInfo($exception, false));
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception->getCode() == 404) {
            return redirect()->route('home', ['URI' => $request->getPathInfo()]);
        }

        // Verifica si la respuesta debe retornar en formato JSON
        if ($request->expectsJson() || $request->ajax()) {
            // return response()->jsonException($exception);
            return response()->json([
                'extra'   => ExceptionSupport::removeNoAppLinesFromTrace($exception),
                'data'    => $request->all(),
                'message' => $exception->getFile() .
                            ' ('.$exception->getLine().')' .
                            $exception->getMessage(),
                'status'  => 500,
                'error'   => get_class($exception) . ' ('.$exception->getCode().')'
            ], 500);
        }

        return parent::render($request, $exception);
    }
}
