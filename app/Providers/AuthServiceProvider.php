<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\User'                              => 'App\Policies\UsuarioPolicy',
        'App\Models\Agencia'                           => 'App\Policies\AgenciaPolicy',
        'App\Models\Marca'                             => 'App\Policies\MarcaPolicy',
        'App\Models\PalabraClave'                      => 'App\Policies\PalabraClavePolicy',
        'App\Models\Producto'                          => 'App\Policies\ProductoPolicy',
        'App\Models\Campana'                           => 'App\Policies\CampanaPolicy',
        'App\Models\Influenciador'                     => 'App\Policies\InfluenciadorPolicy',
        'App\Models\Publicacion'                       => 'App\Policies\PublicacionPolicy',
        'App\Models\PublicacionXInfluenciadorXCampana' => 'App\Policies\PublicacionXInfluenciadorXCampanaPolicy',
        'App\Models\LogEvento'                         => 'App\Policies\LogEventoPolicy',
        'App\Models\InfluenciadorXCampana'             => 'App\Policies\InfluenciadorXCampanaPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
