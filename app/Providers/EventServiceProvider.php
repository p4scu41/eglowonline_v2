<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Specifies the Listeners of the Events
 *
 * @package App\Providers
 * @author  Pascual <pascual@importare.mx>
 *
 * @property array $listen Listeners
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        /*'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],*/
        // JSON Web Token Authentication Event Listeners
        'tymon.jwt.absent' => [
            'App\Events\JWTEvents@absent'
        ],
        'tymon.jwt.expired' => [
            'App\Events\JWTEvents@expired'
        ],
        'tymon.jwt.invalid' => [
            'App\Events\JWTEvents@invalid'
        ],
        'tymon.jwt.user_not_found' => [
            'App\Events\JWTEvents@user_not_found'
        ],
        'tymon.jwt.valid' => [
            'App\Events\JWTEvents@valid',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
