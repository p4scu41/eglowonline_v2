<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Laravel Storage driver with Google Drive API
 * https://gist.github.com/ivanvermeyen/cc7c59c185daad9d4e7cb8c661d7b89b#file-googledriveserviceprovider-php
 *
 * @package App\Providers
 * @author  Pascual <pascual@importare.mx>
 */
class GoogleDriveServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \Storage::extend('google', function($app, $config) {
            $client = new \Google_Client();

            $client->setClientId($config['clientId']);
            $client->setClientSecret($config['clientSecret']);
            $client->refreshToken($config['refreshToken']);

            $service = new \Google_Service_Drive($client);
            $adapter = new \Hypweb\Flysystem\GoogleDrive\GoogleDriveAdapter($service, $config['folderId']);

            return new \League\Flysystem\Filesystem($adapter);
        });
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
