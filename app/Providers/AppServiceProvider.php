<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('boolean', function ($expression) {
            return "<?php echo $expression ? 'Si' : 'No'; ?>";
        });

        Blade::directive('boolean_thumb', function ($expression) {
            return "<?php echo $expression ?
                '<img src=\"" . asset('img/iconos/activo.png') .  "\" width=\"32\">' :
                '<img src=\"" . asset('img/iconos/no.png') .  "\" width=\"32\">';?>";
        });

        Blade::directive('date', function ($expression) {
            return "<?php
                if ($expression != null) {
                    if (is_string({$expression})) {
                        echo \Carbon\Carbon::createFromFormat('Y-m-d', {$expression})->format('d-m-Y');
                    } else {
                        echo {$expression}->format('d-m-Y');
                    }
                }
            ?>";
        });

        Blade::directive('date_short', function ($expression) {
            return "<?php
                if ($expression != null) {
                    if (is_string({$expression})) {
                        echo \Carbon\Carbon::createFromFormat('Y-m-d', {$expression})->format('d-F');
                    } else {
                        echo {$expression}->format('d-F');
                    }
                }
            ?>";
        });

        Blade::directive('datelocate', function ($expression) {
            // setlocale(LC_TIME, 'Mexico');
            // Carbon::setLocale('es');

            return "<?php
                if ($expression != null) {
                    if (is_string({$expression})) {
                        echo \Carbon\Carbon::createFromFormat('d-m-Y', {$expression})->format('d/m/Y');
                    } else {
                        echo {$expression}->format('d/m/Y');
                    }
                }
            ?>";
        });

        Blade::directive('time', function ($expression) {
            return "<?php
                if ($expression != null) {
                    if (is_string({$expression})) {
                        echo \Carbon\Carbon::createFromFormat('H:i:s', {$expression})->format('h:i a');
                    } else {
                        echo {$expression}->format('h:i a');
                    }
                }
            ?>";
        });

        Blade::directive('time_long', function ($expression) {
            return "<?php
                if ($expression != null) {
                    if (is_string({$expression})) {
                        echo \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', {$expression})->format('H:i');
                    } else {
                        echo {$expression}->format('Y-m-d H:i');
                    }
                }
            ?>";
        });

        Blade::directive('datetime', function ($expression) {
            return "<?php
                if ($expression != null) {
                    if (is_string({$expression})) {
                        echo \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', {$expression})->format('d/m/Y h:i a');
                    } else {
                        echo {$expression}->format('d/m/Y h:i a');
                    }
                }
            ?>";
        });

        Blade::directive('datetime_short', function ($expression) {
            return "<?php
                if ($expression != null) {
                    if (is_string({$expression})) {
                        echo \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', {$expression})->format('d-m-Y');
                    } else {
                        echo {$expression}->format('d-m-Y');
                    }
                }
            ?>";
        });

        Blade::directive('formatNumber', function ($expression) {
            return "<?php
                echo \App\Helpers\Helper::formatNumber({$expression});
            ?>";
        });

        Blade::directive('convertToKb', function ($expression) {
            return "<?php
                echo \App\Helpers\Helper::convertToKb({$expression});
            ?>";
        });

        // Seguridad: Obliga a utilizar ssl
        \URL::forceScheme('https');

        // Para que los enlaces del paginado tambien esten sobre SSL
        $this->app['request']->server->set('HTTPS','on');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            // Loader for registering facades
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            
            // Load third party local providers
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);

            // Load third party local aliases
            $loader->alias('Debugbar', \Barryvdh\Debugbar\Facade::class);
        }
    }
}
