<?php

namespace App\Models;

class PerfilBaseModel extends BaseModel
{
    protected $primaryKey = 'influenciador_id';

    /**
     *
     * @return App\Models\Influenciador $influenciador
     */
    public function influenciador()
    {
        return $this->belongsTo('App\Models\Influenciador', 'influenciador_id', 'id');
    }

    public function avatar()
    {
        if ($this->profile_image_url == '') {
            return url('/') . '/img/user_icon.png';
        }

        return $this->profile_image_url;
    }

    public function getCantidadSeguidoresKAttribute()
    {
        return number_format(($this->cantidad_seguidores / 1000), 1) . 'K';
    }
}
