<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfluenciadorAparicionBusqueda extends Model
{
    protected $table = 'influenciador_x_aparicion_busqueda';
    protected $primaryKey = 'influenciador_id';

    public $fillable = [
        'influenciador_id',
    ];

    /**
     *
     * @return App\Models\Influenciador $influenciador
     */
    public function influenciador()
    {
        return $this->belongsTo('App\Models\Influenciador', 'influenciador_id', 'id');
    }

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value)
    {}

    public function getUpdatedAtColumn()
    {}
}
