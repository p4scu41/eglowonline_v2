<?php

namespace App\Models;

/**
 * Class PerfilFacebook
 *
 * @package App
 * @subpackage Models
 * @category Model
 * @author --
 */
class PerfilFacebook extends PerfilBaseModel
{
    /**
     * @var string the table name
     */
    protected $table = 'perfil_facebook';

    /**
     * @var array the columns that are mass fillable
     */
    protected $fillable = [
        'facebook_id',
        'influenciador_id',
        'screen_name',
        'access_token',
        'about',
        'link',
        'cantidad_seguidores',
        'profile_image_url',
        'precio_publicacion',
        'friends',
        'engagement',
        'page_id',
        'page_name',
        'page_access_token',
        'page_categories',
        'selectedPage',
    ];

    /**
     * @var array the rules for each column when creating
     */
    public $rules_create = [
        'influenciador_id'    => 'bail|required|integer|exists:influenciador,id',
        'screen_name'         => 'bail|nullable|max:45|unique:perfil_facebook',
        'cantidad_seguidores' => 'bail|nullable|integer',
    ];

    /**
     * @var array the rules for each column when updating
     */
    public $rules_update = [
        'influenciador_id'    => 'bail|required|integer|exists:influenciador,id',
        'screen_name'         => 'bail|nullable|max:45',
        'cantidad_seguidores' => 'bail|nullable|integer',
    ];

    /**
     * @var array the labels for each column
     */
    public static $labels = [
        'influenciador_id'    => 'Influenciador',
        'screen_name'         => 'Correo',
        'cantidad_seguidores' => 'Cantidad de Seguidores',
    ];

    /**
     * returns the pages are associated with the current facebook profile
     *
     * @return Illuminate\Eloquent\Relationships\HasMany
     */
    public function pages()
    {
        return $this->hasMany(FacebookPage::class, 'influenciador_id', 'influenciador_id');
    }

    /**
     * updates profile data from page data
     *
     * @param array $page
     */
    public function updateProfileFromPage(array $page)
    {
        $this->cantidad_seguidores = $page['engagement'];
        $this->engagement          = $page['engagement'];
        $this->page_id             = $page['id'];
        $this->screen_name         = $page['name'];
        $this->page_name           = $page['name'];
        $this->about               = $page['about'];
        $this->profile_image_url   = $page['profile_image_url'];
        $this->page_access_token   = $page['access_token'];
        $this->page_categories     = $page['categories'];
        $this->selectedPage        = 1;
    }
}
