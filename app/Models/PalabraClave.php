<?php

namespace App\Models;

class PalabraClave extends BaseModel
{
    protected $table = 'palabra_clave';

    protected $fillable = [
        'tipo_palabra_clave_id',
        'palabra',
        'activo',
    ];

    public $rules_create = [
        'tipo_palabra_clave_id' => 'bail|required|integer|exists:tipo_palabra_clave,id',
        'palabra'               => 'bail|required|min:3|max:100|unique:palabra_clave',
        'activo'                => 'bail|required|integer|in:0,1',
    ];

    public $rules_update = [
        'tipo_palabra_clave_id' => 'bail|required|integer|exists:tipo_palabra_clave,id',
        'palabra'               => 'bail|required|min:3|max:100',
        'activo'                => 'bail|required|integer|in:0,1',
    ];

    public static $labels = [
        'tipo_palabra_clave_id' => 'Tipo Palabra',
        'palabra'               => 'Palabra',
        'activo'                => 'Activo',
    ];

    /**
     * Obtiene las registros de las tablas con las que tiene relación el modelo
     *
     * @param boolean $withLabels Para incluir las etiquetas como primer elemento del array
     * @return array
     */
    public static function getCatalogs($withLabels = true)
    {
        $cataglos = [];

        $catalogs['tipo_palabra_clave'] = TipoPalabraClave::orderBy('tipo', 'asc')
                    ->get()
                    ->pluck('tipo', 'id');

        if ($withLabels == true) {
            $catalogs['tipo_palabra_clave']->prepend(self::$labels['tipo_palabra_clave_id'], '');
        } else {
            $catalogs['tipo_palabra_clave']->prepend('', '');
        }

        return $catalogs;
    }

    /**
     *
     * @return App\Models\TipoPalabraClave $tipoPalabraClave
     */
    public function tipoPalabraClave()
    {
        return $this->belongsTo('App\Models\TipoPalabraClave', 'tipo_palabra_clave_id', 'id');
    }

    /**
     * Busca las palabras claves que coinciden con el text
     *
     * @param  string  $text
     * @return array
     */
    public static function searchByText($text, $whereIn = [])
    {
        $query = static::select([
                'id',
                'palabra',
            ])
            ->where('palabra', 'LIKE', '%'.$text.'%')
            ->where('activo', 1)
            ->limit(10);

        if (!empty($whereIn)) {
            $query->whereIn('tipo_palabra_clave_id', $whereIn);
        }

        return $query->get();
    }

    public function getDataToBackEnd()
    {
        return [
            'id'       => $this->id,
            'palabra'  => strtolower(strtr($this->palabra,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY')),
            'activo'   => $this->activo,
        ];
    }

    public function sendPalabraClaveToBackend($type)
    {
        $serviceBack = new ServiciosTwitter();

        if (strtolower($type) == 'post') {
            $serviceBack->palabraclavePost($this->getDataToBackEnd());
        } elseif (strtolower($type) == 'put') {
            $serviceBack->palabraclavePut($this->id, $this->getDataToBackEnd());
        }
    }
}
