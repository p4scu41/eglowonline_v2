<?php

namespace App\Models;

class Genero
{
    public static $generos = [
        [
            'id'     => 1,
            'genero' => 'Hombre',
            'icono'  => 'img/genero/1.png',
        ],
        [
            'id'     => 2,
            'genero' => 'Mujer',
            'icono'  => 'img/genero/2.png',
        ],
        [
            'id'     => 3,
            'genero' => 'No Aplica',
            'icono'  => 'img/genero/3.png',
        ],
    ];
}
