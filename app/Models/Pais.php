<?php

namespace App\Models;

class Pais extends BaseModel
{
    protected $table = 'pais';

    /**
     *
     * @return App\Models\Influenciador[] $influenciadores
     */
    public function influenciadores()
    {
        return $this->hasMany('App\Models\Influenciador', 'pais_id', 'id');
    }
}
