<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * class that logs activity from received services on failures
 *
 * @package App
 * @subpackage Models
 * @category Model
 * @author Gerardo Adrián Gómez Ruiz <gerardo@eglow.mx>
 */
class ServiciosReceivedBackendLog extends Model
{
    /**
     * @var string table name
     */
    protected $table = 'servicios_received_backend_log';

    /**
     * @var bool flag for created at and updated at
     */
    public $timestamps = false;

    /**
     * @var array columns that are mass assignable
     */
    protected $fillable = [
        'request',
        'uri',
        'start',
        'end',
        'params',
        'response',
        'status_code',
        'status_text'
    ];
}
