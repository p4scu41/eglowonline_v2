<?php

namespace App\Models;

use App\Helpers\UploadFileTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Agencia extends BaseModel
{
    use UploadFileTrait;

    protected $table = 'agencia';

    public $fileName = 'logotipo';

    protected $fillable = [
        'usuario_id',
        'entorno_id',
        'nombre',
        'direccion',
        'activo',
    ];

    public $rules_create = [
        'usuario_id' => 'bail|required|integer|exists:usuario,id|unique:agencia',
        'nombre'     => 'bail|required|min:3|max:45|unique:agencia',
        'direccion'  => 'max:100',
        'activo'     => 'bail|required|integer|in:0,1',
    ];

    public $rules_update = [
        'usuario_id' => 'bail|required|integer|exists:usuario,id',
        'nombre'     => 'bail|required|min:3|max:45',
        'direccion'  => 'max:100',
        'activo'     => 'bail|required|integer|in:0,1',
    ];

    public static $labels = [
        'usuario_id' => 'Usuario',
        'nombre'     => 'Nombre',
        'direccion'  => 'Dirección',
        'activo'     => 'Activo',
        'logotipo'   => 'Logotipo',
    ];

    public $error_messages = [
        'exists'            => 'El usuario ya tiene asociado una cuenta.',
        'usuario_id.exists' => 'El usuario ya tiene asociado una cuenta.',
    ];

    /**
     * Obtiene los registros de las tablas con las que tiene relación el modelo
     *
     * @param  boolean $withLabels Para incluir las etiquetas como primer elemento del array
     * @return array
     */
    public static function getCatalogs($withLabels = true)
    {
        $catalogs = [];

        $catalogs['usuario'] = User::where('tipo_usuario_id', TipoUsuario::AGENCIA)
            ->where('activo', 1)
            ->where(DB::raw('id NOT IN (SELECT usuario_id FROM agencia)'))
            ->orderBy('nombre', 'asc')
            ->get()
            ->pluck('nombre', 'id');

        if ($withLabels == true) {
            $catalogs['usuario']->prepend(self::$labels['usuario_id'], '');
        } else {
            $catalogs['usuario']->prepend('Usuario', '');
        }

        return $catalogs;
    }

    /**
     *
     * @return App\Models\User $usuario
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'usuario_id', 'id');
    }

    /**
     *
     * @return App\Models\entorno $entorno
     */
    public function entorno()
    {
        return $this->belongsTo('App\Models\Entorno', 'entorno_id', 'id');
    }

    public function getBaseUploadFolder()
    {
        return 'img' . DIRECTORY_SEPARATOR . 'agencia';
    }

    /**
     * Path to the logotipo
     *
     * @return string
     */
    public function getLogotipo()
    {
        $logo = $this->getFile();

        if (empty($logo)) {
            $logo = 'img/placehold_logo.png';
        }

        return $logo;
    }

    /**
     *
     * @inheritDoc
     */
    public static function deletedHandler($model)
    {
        $oldFile = $model->findFile();

        // Eliminamos el logotipo, si existe
        if ($oldFile != null) {
            unlink($oldFile);
        }

        return true;
    }

    public function updateCatalogUsuario()
    {
        $usuarios = DB::select("SELECT id, nombre FROM usuario WHERE tipo_usuario_id = 2 AND activo = 1 AND id NOT IN (SELECT usuario_id FROM agencia) UNION SELECT u.id, u.nombre FROM usuario u INNER JOIN agencia m ON m.usuario_id = u.id WHERE m.id = ?", [$this->id]);

        $usuarios = collect($usuarios)->pluck('nombre', 'id')->prepend(self::$labels['usuario_id'], '');

        return $usuarios;
    }
}
