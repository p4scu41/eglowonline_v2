<?php

namespace App\Models;

use App\Helpers\Helper;
use GuzzleHttp\Client as GuzzleHttpClient;
use Exception;
use Illuminate\Support\Facades\Log;
use Twitter;

class RedSocial extends BaseModel
{
    protected $table = 'red_social';

    public static $TWITTER   = 1;
    public static $FACEBOOK  = 2;
    public static $INSTAGRAM = 3;
    public static $YOUTUBE   = 4;
    public static $SNAPCHAT  = 5;

    public static $REDES_SOCIALES = [
        1 => 'twitter',
        2 => 'facebook',
        3 => 'instagram',
        4 => 'snapchat',
        5 => 'youtube',
    ];

    /**
     *
     * @return App\Models\Publicacion[] $publicaciones
     */
    public function publicaciones()
    {
        return $this->hasMany('App\Models\Publicacion', 'red_social_id', 'id');
    }

    public static function getRedSocialText($red_social_id)
    {
        return static::$REDES_SOCIALES[$red_social_id];
    }

    public static function publicarTwitter(Publicacion $publicacion)
    {
        // https://github.com/Seldaek/monolog/blob/master/doc/01-usage.md
        // Para establecer un archivo log de salida
        $stream = new \Monolog\Handler\StreamHandler(storage_path().'/logs/job_publica_twitter.log', \Monolog\Logger::INFO);
        // Para cambiar el formato del output al archivo
        $stream->setFormatter(new \Monolog\Formatter\LineFormatter("[%datetime%] %level_name%: %message% %context%\n", null, true));
        // Se crea el logger
        $log = new \Monolog\Logger('log');
        $log->pushHandler($stream);

        $publicacionLog = new PublicacionLog();

        $publicacionLog->publicacion_id = $publicacion->id;
        $publicacionLog->start          = date('Y-m-d H:i:s');
        $publicacionLog->error          = 0;

        try {
            $perfilTwitter = $publicacion->influenciador->perfilTwitter;
            $post = [];
            $file = null;

            if (!empty($perfilTwitter)) {
                if (empty($perfilTwitter->oauth_token) || empty($perfilTwitter->oauth_token_secret)) {
                    $publicacionLog->error = 0;
                    $publicacionLog->response = 'No existe oauth_token o oauth_token_secret para el influenciador';

                    throw new Exception($publicacionLog->response);
                }

                // thujohn/twitter
                Twitter::reconfig([
                    'token'  => $perfilTwitter->oauth_token,
                    'secret' => $perfilTwitter->oauth_token_secret,
                    // Utiliza el vendor themattharris/tmhOAuth
                    // Se incrementa los tiempos porque manda error
                    // Operation timed out after 10000 milliseconds with 0 bytes received
                    // https://github.com/themattharris/tmhOAuth/blob/master/tmhOAuth.php#L57
                    // 'curl_connecttimeout' => 300,
                    'curl_timeout'        => 100,
                ]);

                $params_file = [
                    'media_file' => $publicacion->getArchivoMultimedia(true)
                ];

                if (!empty($params_file['media_file'])) {
                    // Si el archivo multimedia es una url se descarga el recurso al local
                    if (filter_var($params_file['media_file'], FILTER_VALIDATE_URL) != false) {
                        $httpClient = new GuzzleHttpClient();
                        $name = explode('/', $params_file['media_file']);
                        $path_file = public_path('multimedia_posts/'.($name[count($name)-1]));
                        $httpClient->request('GET', $params_file['media_file'], ['sink' => $path_file]);

                        $params_file['media_file'] = $path_file;
                    }

                    if ($publicacion->multimediaIsVideo()) {
                        $params_file['media_category'] = 'tweet_video';
                    }

                    $uploaded_media = Twitter::uploadMediaChunked($params_file);

                    $post['media_ids'] = $uploaded_media->media_id_string;
                }

                $post['status'] = $publicacion->contenido;

                $result = Twitter::postTweet($post);

                if (!empty($result)) {
                    $publicacionLog->response = json_encode($result);

                    $publicacion->id_publicacion_red_social = $result->id_str;
                }

                // $publicacion->sendPublicacionToBackend();
                $publicacion->save();

                $publicacionLog->end = date('Y-m-d H:i:s');
                $publicacionLog->save();
            }
        } catch (Exception $e) {
            $logs = Twitter::logs();
            $totalLogs = count($logs);
            $posicionInicialParameters = 3;
            $posicionIncrementoParameters = 6;

            // Acorta la longitud de la variable PARAMETERS a 200 caracteres,
            // debido a que cuando se sube una imagen o video este es muy extenso.
            for ($i=$posicionInicialParameters; $i < $totalLogs; $i+=$posicionIncrementoParameters) {
                $logs[$i] = substr($logs[$i], 0, 200);
            }

            $publicacionLog->end = date('Y-m-d H:i:s');
            $publicacionLog->response = $e->getMessage();
            $publicacionLog->error = 1;

            $publicacionLog->save();

            $log->error('Twitter:postTweet -> ID: '.$publicacion->id. ', '.$e->getMessage());
            $log->error($e->getTraceAsString());
            $log->error(Helper::errorsToString($logs, "\r\n"));

            throw new Exception($e->getMessage());
        }
    }
}
