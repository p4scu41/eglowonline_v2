<?php
namespace App\Models;

use GuzzleHttp\Client as GuzzleHttpClient;
use App\Models\ServiciosBackendLog;
use Exception;

class ServiciosBackend
{
    public static $keyWordType = [
        1 => 'Keyword',
        2 => 'Marca',
        3 => 'Producto',
        4 => 'Profesion',
        5 => 'Pais',
    ];

    public static $K_KEYWORD   = 1;
    public static $K_MARCA     = 2;
    public static $K_PRODUCTO  = 3;
    public static $K_PROFESION = 4;
    public static $K_PAIS      = 5;

    public static $reportType = [
        1 => 'WordCloud Keywords',
        2 => 'Hashtags',
    ];

    public static $RT_WORDCLOUD_KEYWORDS = 1;
    public static $RT_HASHTAGS = 2;

    /**
     * Cliente HTTP para realizar las peticiones
     *
     * @var GuzzleHttp\Client
     */
    public $httpClient = null;

    /**
     * Respuesta del servicio
     * @var array
     */
    public $response = null;

    /**
     * Servico consumido
     *
     * @var string
     */
    public $currentService = null;

    /**
     * Nombre de usuario de la red social
     *
     * @var string
     */
    public $screenname = null;

    /**
     * Fecha de incio
     * Formato MM-DD-YYYY
     *
     * @var string Date
     */
    public $datefrom = null;

    /**
     * Fecha de finalización
     * Formato MM-DD-YYYY
     *
     * @var string Date
     */
    public $dateto = null;

    /**
     * oauth_token de la red social
     *
     * @var string Date
     */
    public $hashauth = 'e301aeca08cf84f84e7f74c7d29f1480df379153';

    /**
     *
     * @param GuzzleHttp\Client $guzzleHttpCliente
     */
    public function __construct()
    {
        $this->httpClient = new GuzzleHttpClient([
            'base_uri' => config('app.url_servicios_backend'),
            // 'timeout' => 2,
        ]);
    }

    public function profileCommunity()
    {
        return $this->requestGet('ProfileCommunity');
    }

    public function topHashtags()
    {
        return $this->requestGet('TopWordCloudCom', ['wckeyword' => 3]);
    }

    public function topProfessions()
    {
        return $this->requestGet('Professions');
    }

    public function topFollowers()
    {
        return $this->requestGet('TopFollowers');
    }

    public function locationDistribution()
    {
        return $this->requestGet('LocationDistribution');
    }

    public function genderDistribution()
    {
        return $this->requestGet('GenderDistribution');
    }

    public function topBrands()
    {
        return $this->requestGet('TopBrands');
    }

    public function profileHistoricalStatistics()
    {
        return $this->requestGet('ProfileHistoricalStatistics');
    }

    public function topHashtagsProfile()
    {
        return $this->requestGet('TopHashtags');
    }

    public function topWordCloudProfile()
    {
        return $this->requestGet('TopWordCloudProfile');
    }

    public function topWordCloudCom()
    {
        return $this->requestGet('TopWordCloudCom');
    }

    public function topBrandsProfile()
    {
        return $this->requestGet('TopBrandsProfile');
    }

    public function periodTweetsProfile()
    {
        return $this->requestGet('PeriodTweetsProfile');
    }

    public function bestDayHourProfile()
    {
        return $this->requestGet('BestDayAndHourProfile');
    }

    public function appStatus()
    {
        return $this->requestGet('AppStatus');
    }

    public function usersRunning()
    {
        return $this->requestGet('UsersRunning');
    }

    public function twitterProfile()
    {
        return $this->requestGet('TwitterProfile/Profiles');
    }

    public function twitterProfilePost()
    {
        return $this->requestPost('TwitterProfile');
    }

    public function profileStatistics()
    {
        return $this->requestGet('ProfileStatistics');
    }

    public function configDays($FTD)
    {
        // Default 7
        return $this->requestPost('ConfigDays', ['FTD' => $FTD]);
    }

    public function followersRangeGet()
    {
        // Devuelve: FollowersRangeSelect
        return $this->requestGet('FollowersRangeGet');
    }

    public function followersRange($data)
    {
        $parameters = collect([
                'A' => 100,
                'B' => 50,
                'C' => 30,
                'D' => 5,
                'E' => 1,
            ])
            ->merge($data)
            ->all();

        return $this->requestPost('FollowersRange', $parameters);
    }

    public function hashtagCampaign($hashtag)
    {
        return $this->requestPost('HashtagCampaign', ['hashtag' => $hashtag]);
    }

    public function hashtagReport($reportID)
    {
        // 11 Noche se crea el reporte
        // reportID se obtiene del listado devuelto por "Report"
        return $this->requestGet('HashtagReport', ['reportID' => $reportID]);
    }

    public function report()
    {
        // Listado de ID para HashtagReport
        // SeguimientoHashtag cantidad de tweets que se hicieron en el día
        // Intereses, basado en las palabras de los tweets
        //   "WORD": Keyword, WourdCloud,
        //   "AMOUNT": Cantidad de veces que se mencono
        // UserAction,
        return $this->requestGet('Report');
    }

    public function reportType()
    {
        return $this->requestGet('ReportType');
    }

    public function keywords($type = null, $name = null, $post = false)
    {
        $data = [
            'hashauth' => $this->hashauth,
        ];

        if (!empty($type)) {
            $data['KeywordTypeID'] = $type;
        }

        if (!empty($type)) {
            $data['name'] = $name;
        }

        if ($post) {
            return $this->requestPost('Keywords', $data);
        } else {
            return $this->requestGet('Keywords', $data);
        }
    }

    public function keywordType($name = null, $post = false)
    {
        $data = [
            'hashauth' => $this->hashauth,
        ];

        if (!empty($type)) {
            $data['name'] = $name;
        }

        if ($post) {
            return $this->requestPost('KeywordType', $data);
        } else {
            return $this->requestGet('KeywordType', $data);
        }
    }

    /**
     * Petición GET
     *
     * @param  string $uri Segmento a agregar en la base uri
     * @return aray
     */
    public function requestGet($uri, $data = [])
    {
        $params = collect([
                'screenname' => $this->screenname,
                'datefrom'   => $this->datefrom,
                'dateto'     => $this->dateto,
                'hashauth'   => $this->hashauth,
            ])
            ->merge($data)
            ->all();
        $this->currentService = $uri;

        return $this->sendRequest('GET', $uri, $params);
    }

    /**
     * Petición POST
     *
     * @param  string $uri Segmento a agregar en la base uri
     * @return aray
     */
    public function requestPost($uri, $data = [])
    {
        $params = collect([
                'screenname' => $this->screenname,
                'hashauth'   => $this->hashauth,
            ])
            ->merge($data)
            ->all();
        $this->currentService = $uri;

        return $this->sendRequest('POST', $uri, $params);
    }

    /**
     * Realiza la petición con el método request de GuzzleHttp\Client
     * Utiliza app.url_servicios_backend como base uri
     *
     * Para obtener la duración de la petición:
     *
     *   SELECT
     *       REPLACE(uri, 'http://sabato.eglowonline.com/WebApi/Api/', '') AS service,
     *       TIMEDIFF(END, START) AS duration_response,
     *       status_code
     *   FROM
     *       request_log
     *
     * @param  string  $method  GET | POST
     * @param  string  $uri     Segmento a agregar en la base uri
     * @param  array   $data    Datos a enviar en la petición
     * @return array   Response Body
     */
    public function sendRequest($method, $uri, $data = [])
    {
        $options = [];
        $data    = array_filter($data, 'strlen'); // Elimina elementos vacios
        $requestLog = [
            'request'     => $method,
            'uri'         => config('app.url_servicios_backend') . $uri,
            'start'       => date('Y-m-d H:i:s'),
            'end'         => null,
            'params'      => json_encode($data),
            'response'    => null,
            'status_code' => null,
            'status_text' => null,
        ];

        $this->response = null;

        if (!empty($data)) {
            switch (strtoupper($method)) {
                case 'GET':
                    $options = ['query' => $data];
                    break;

                case 'POST':
                    $options = ['query' => $data, /*'form_params' => $data*/];
                    break;
            }
        }

        try {
            $response = $this->httpClient->request($method, $uri, $options);

            $requestLog['status_code'] = $response->getStatusCode();
            $requestLog['status_text'] = $response->getReasonPhrase();
            $requestLog['response']    = $response->getBody();

            $this->response = json_decode($response->getBody());
        } catch (Exception $e) {
            $requestLog['status_code'] = $e->getCode();
            $requestLog['response']    = $e->getMessage();
        } finally {
            $requestLog['end'] = date('Y-m-d H:i:s');
            ServiciosBackendLog::create($requestLog);
        }

        return $this->response;
    }

    /*public function saveResponse()
    {
        switch ($this->currentService) {
            case 'ProfileCommunity':

                break;
            case 'TopHashtags':

                break;
            case 'Professions':

                break;
            case 'TopFollowers':

                break;
            case 'LocationDistribution':

                break;
            case 'GenderDistribution':

                break;
            case 'TopBrands':

                break;
            case 'ProfileHistoricalStatistics':

                break;
            case 'TopHashtags':

                break;
            case 'TopWordCloudProfile':

                break;
            case 'TopBrandsProfile':

                break;
            case 'PeriodTweetsProfile':

                break;
            case 'BestDayAndHourProfile':

                break;
            case 'AppStatus':

                break;
            case 'UsersRunning':

                break;
            case 'TwitterProfile/Profiles':

                break;
            case 'TwitterProfile':

                break;
            case 'ProfileStatistics':

                break;
        }
    }*/
}
