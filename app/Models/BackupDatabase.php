<?php

namespace App\Models;

use App\Helpers\File as HelperFile;
// use App\Helpers\PerformanceLogger;
use App\Support\LoggerSupport;
use App\Support\ExceptionSupport;
use Artisan;
use Exception;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Log;
use Storage;

/**
 * Backup Database Manager
 *
 * @package App\Models
 * @author  Pascual <pascual@importare.mx>
 *
 * @method static string execute()
 */
class BackupDatabase
{
    /**
     * Backup the database in a sql dump file
     *
     * @return string
     */
    public static function execute()
    {
        $configDB    = config('database.connections.' . config('database.default'));
        $logger      = LoggerSupport::createLogger('backups_'.date('Y-m-d').'.log');
        $file        = $configDB['database'] . '_' . date('Y_m_d_H_i_s') . '.sql';
        $dest        = config('database.storage_backups') . DIRECTORY_SEPARATOR . $file;
        $compression = 'gzip';
        $ext_compres = 'gz';
        $message     = 'Database -> Backup database to file storage' .
            DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . $dest;

        /*$performance           = new PerformanceLogger();
        $performance->class    = __CLASS__;
        $performance->function = __FUNCTION__ ;

        $performance->start();*/

        try {
            // backup-manager
            // https://github.com/backup-manager/laravel
            Artisan::call('db:backup', [
                '--database'        => config('database.default'),
                '--destination'     => 'local',
                '--destinationPath' => $dest,
                '--compression'     => $compression,
            ]);

            // Upload file to cloud
            $contents = Storage::disk('local')->get($dest . ($compression ? '.'.$ext_compres : ''));

            Storage::disk(config('filesystems.cloud'))
                ->write($file . ($compression ? '.'.$ext_compres : ''), $contents);

            // Delete files older than 30 days
            HelperFile::delete(storage_path('app\local'), '*.zip', 30);
        } catch (Exception $e) {
            $message = 'Database -> ERROR: '.ExceptionSupport::getMessageIfDebug($e);
        } /*finally {
            $performance->finish();
            $message = $message . $performance->getInfo();
        }*/

        $logger->info($message);

        return $message;
    }
}
