<?php

namespace App\Models;

use App\Notifications\ResetPasswordNotification;
use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use DB;

class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable;

    public $table = 'usuario';

    public $fillable = [
        'tipo_usuario_id',
        'nombre',
        'email',
        'password',
        'ultimo_acceso',
        'activo',
    ];

    public $hidden = [
        'password',
        'remember_token',
        'auth_key',
        'access_token',
    ];

    public $rules_create = [
        'tipo_usuario_id'       => 'bail|required|integer|exists:tipo_usuario,id',
        'nombre'                => 'bail|required|min:3|max:55',
        'email'                 => 'bail|required|min:3|max:40|email|unique:usuario',
        'password'              => 'bail|min:6|max:61',
        'password_confirmation' => 'bail|min:6|max:61',
        'activo'                => 'bail|required|integer|in:0,1',
    ];

    public $rules_update = [
        'tipo_usuario_id'       => 'bail|required|integer|exists:tipo_usuario,id',
        'nombre'                => 'bail|required|min:3|max:55',
        'email'                 => 'bail|required|min:3|max:40|email',
        'password'              => 'bail|min:6|max:61',
        'password_confirmation' => 'bail|min:6|max:61',
        'activo'                => 'bail|required|integer|in:0,1',
    ];

    public static $labels = [
        'tipo_usuario_id'       => 'Tipo de Usuario',
        'nombre'                => 'Nombre',
        'email'                 => 'E-mail',
        'password'              => 'Contraseña',
        'password_confirmation' => 'Confirmar Contraseña',
        'activo'                => 'Activo',
        'ultimo_acceso'         => 'Último acceso',
    ];

    public $dates = [
        'ultimo_acceso',
    ];

    // Ids de los usuarios que pertenecen a Televisa
    public static $IDS_TELEVISA = [9, 10, 415];

    /**
     * Check if the user is active
     *
     * @return boolean
     */
    public function isActivo()
    {
        return $this->activo == 1;
    }

    /**
     * Revisa si el usuario pertenece a Televisa
     *
     * @return boolean
     */
    public function inEntornoTelevisa()
    {
        return in_array($this->id, static::$IDS_TELEVISA);
    }

    /**
     * {@inheritdoc}
     *
     * @param  App\Models\Usuario $model
     * @return boolean true
     */
    public static function creatingHandler($model)
    {
        $model->auth_key = str_random(40);
        $model->access_token = str_random(40);
        $model->password = bcrypt($model->password); // \Hash::make($model->password);

        return true;
    }

    /**
     * Obtiene los registros de las tablas con las que tiene relación el modelo
     *
     * @param  boolean $withLabels Para incluir las etiquetas como primer elemento del array
     * @return array
     */
    public static function getCatalogs($withLabels = true)
    {
        $catalogs = [];

        $catalogs['tipo_usuario'] = TipoUsuario::all()
            ->pluck('descripcion', 'id');

        if ($withLabels == true) {
            $catalogs['tipo_usuario']->prepend(self::$labels['tipo_usuario_id'], '');
        } else {
            $catalogs['tipo_usuario']->prepend('', '');
        }

        return $catalogs;
    }

    /**
     * {@inheritdoc}
     * No se implementa porque causa conflicto cuando se resetea la contraseña
     *
     * @param  App\Models\Usuario $model
     * @return boolean true
     */
    /*public static function updatingHandler($model)
    {
        // If password changed
        if ($model->isDirty('password')) {
            $model->password = bcrypt($model->password); // \Hash::make($model->password);
        }

        return true;
    }*/

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     *
     * @return App\Models\TipoUsuario $tipoUsuario
     */
    public function tipoUsuario()
    {
        return $this->belongsTo('App\Models\TipoUsuario', 'tipo_usuario_id', 'id');
    }

    /**
     * Check if the user is Administrador
     *
     * @return boolean
     */
    public function isAdministrador()
    {
        return $this->tipo_usuario_id == TipoUsuario::ADMINISTRADOR;
    }

    /**
     * Check if the user is Agencia
     *
     * @return boolean
     */
    public function isAgencia()
    {
        return $this->tipo_usuario_id == TipoUsuario::AGENCIA;
    }

    /**
     * Check if the user is Marca
     *
     * @return boolean
     */
    public function isMarca()
    {
        return $this->tipo_usuario_id == TipoUsuario::MARCA;
    }

    /**
     * Check if the user is Producto
     *
     * @return boolean
     */
    public function isProducto()
    {
        return $this->tipo_usuario_id == TipoUsuario::PRODUCTO;
    }

    /**
     * Check if the user is Celebridad
     *
     * @return boolean
     */
    public function isCelebridad()
    {
        return $this->tipo_usuario_id == TipoUsuario::CELEBRIDAD;
    }

    /**
     * Obtiene los entornos con los que esta asociado el usuario
     * Detecta que tipo de usuario es y en base a eso devuelve los entornos
     *
     * @return array
     */
    public function getEntornos()
    {
        $entornos = [];

        if ($this->isAgencia()) {
            if ($this->agencia) {
                $entornos[] = $this->agencia->entorno_id;
            }
        }

        if ($this->isMarca()) {
            Log::error('getEntornos::marca. '.json_encode($this->marca));
            if ($this->marca) {
                Log::error('agencia. '.json_encode($this->marca->agencia));
                if ($this->marca->agencia) {
                    $entornos[] = $this->marca->agencia->entorno_id;
                }
            }
        }

        if ($this->isProducto()) {
            Log::error('getEntornos::producto. '.json_encode($this->producto));
            if ($this->producto) {
                if ($this->producto->marca) {
                    if ($this->producto->marca->agencia) {
                        $entornos[] = $this->producto->marca->agencia->entorno_id;
                    }
                }
            }
        }

        if ($this->isCelebridad()) {
            Log::error('getEntornos::influenciador. '.json_encode($this->influenciador));
            if ($this->influenciador) {
                $entornos = $this->influenciador->entornos->pluck('id');
            }
        }

        // Si el usuario no esta asociado a un entorno se devuelve [null]
        // por eso se aplica array_filter
        return array_filter($entornos);
    }

    /**
     * Verifica si el usuario es el creador o dueño del modelo relacionado
     *
     * @param  \Illuminate\Database\Eloquent\Model $related [description]
     * @return boolean
     */
    public function owns($related)
    {
        if (Schema::hasColumn($related->getTable(), 'created_by')) {
            return $this->id == $related->created_by;
        }

        return $this->id == $related->usuario_id;
    }

    /**
     *
     * @return App\Models\Agencia $agencia
     */
    public function agencia()
    {
        return $this->hasOne('App\Models\Agencia', 'usuario_id', 'id');
    }

    /**
     *
     * @return App\Models\Marca $marca
     */
    public function marca()
    {
        return $this->hasOne('App\Models\Marca', 'usuario_id', 'id');
    }

    /**
     *
     * @return App\Models\Producto $producto
     */
    public function producto()
    {
        return $this->hasOne('App\Models\Producto', 'usuario_id', 'id');
    }

    /**
     *
     * @return Illuminate\Support\Collection $permisosModulos
     */
    public function permisosModulos()
    {
        // Se guarda en cache el resultado para no consultar nuevamente la base de datos
        if (!Cache::has('UsuarioPermisosModulos_' . $this->id)) {
            // Primero se obtiene los permisos del tipo de usuario
            $permisos_tipo_usuario = $this->tipoUsuario->permisosModulos();

            /* SELECT
                modulo_sistema.resource,
                modulo_sistema.action,
                usuario_permiso.allow
            FROM usuario_permiso
            INNER JOIN modulo_sistema ON
                modulo_sistema.id = usuario_permiso.modulo_sistema_id
            WHERE
                usuario_permiso.usuario_id = 1
            ORDER BY
                modulo_sistema.resource */

            // Después se obtienen los permisos especificos del usuario
            $permisos_usuario = DB::table('usuario_permiso')
                ->select('resource', 'action', 'allow')
                ->join('modulo_sistema', 'modulo_sistema.id', '=', 'usuario_permiso.modulo_sistema_id')
                ->where('usuario_permiso.usuario_id', '=', $this->id)
                ->orderBy('resource')
                ->get()
                ->keyBy(function ($item) {
                    return $item->resource . '_' . $item->action;
                });

            // Se hace la unión de los dos permisos,
            // tiene mas relevancia el permiso especifico del usuario
            // return $permisos_tipo_usuario->merge($permisos_usuario);
            // return $permisos_usuario->union($permisos_tipo_usuario);}

            // Guarda en cache para no volver a realizar la consulta
            // en caso de requerir los mismos datos
            Cache::put('UsuarioPermisosModulos_' . $this->id, $permisos_tipo_usuario->merge($permisos_usuario), 1440);
        }

        return Cache::get('UsuarioPermisosModulos_' . $this->id);
    }

    /**
     *
     * @param  string $resource
     * @param  string $action
     * @param  Illuminate\Database\Eloquent\Model $model
     * @return boolean
     */
    public function hasPermisoModulo($resource, $action, $model = null)
    {
        return $this->permisosModulos()->contains(function ($model, $key) use ($resource, $action) {
            return ($model->resource == $resource && $model->action == $action && $model->allow == 1);
        });
    }

    /**
     *
     * @return App\Models\Influenciador $influenciador
     */
    public function influenciador()
    {
        return $this->hasOne('App\Models\Influenciador', 'usuario_id', 'id');
    }
}
