<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entorno extends Model
{
    protected $table = 'entorno';

    const EGLOW    = 1;
    const TELEVISA = 2;

    public $timestamps = false;

    protected $fillable = [
        'descripcion',
    ];
}
