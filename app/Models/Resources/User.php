<?php

namespace App\Models\Resources;

use App\Models\TipoUsuario;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;

/**
 * Class associated with the user table
 *
 * @package App\Models
 * @author  Pascual <pascual@importare.mx>
 *
 */
class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable;

    public $table = 'usuario';

    public $fillable = [
        'tipo_usuario_id',
        'nombre',
        'email',
        'password',
        'ultimo_acceso',
        'activo',
    ];

    public $hidden = [
        'password',
        'remember_token',
        'auth_key',
        'access_token',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public static $rules_create = [
        'tipo_usuario_id'       => 'bail|required|integer|exists:tipo_usuario,id',
        'nombre'                => 'bail|required|min:3|max:55',
        'email'                 => 'bail|required|min:3|max:40|email|unique:usuario',
        'password'              => 'bail|min:6|max:61',
        'password_confirmation' => 'bail|min:6|max:61',
        'activo'                => 'bail|required|integer|in:0,1',
    ];

    public static $rules_update = [
        'tipo_usuario_id'       => 'bail|required|integer|exists:tipo_usuario,id',
        'nombre'                => 'bail|required|min:3|max:55',
        'email'                 => 'bail|required|min:3|max:40|email',
        'password'              => 'bail|min:6|max:61',
        'password_confirmation' => 'bail|min:6|max:61',
        'activo'                => 'bail|required|integer|in:0,1',
    ];

    public static $labels = [
        'tipo_usuario_id'       => 'Tipo de Usuario',
        'nombre'                => 'Nombre',
        'email'                 => 'E-mail',
        'password'              => 'Contraseña',
        'password_confirmation' => 'Confirmar Contraseña',
        'activo'                => 'Activo',
        'ultimo_acceso'         => 'Último acceso',
    ];

    public $dates = [
        'ultimo_acceso',
    ];

    /**
     * inheritDoc
     */
    public $error_messages = [
        'tipo_usuario_id.required'   => 'No se especificó el Tipo de Usuario',
        'tipo_usuario_id.exis'       => 'El Tipo de Usuario especificado no existe',
        'password_confirmation.same' => 'Las Contraseñas no Coinciden',
    ];

    /**
     * @inheritDoc
     */
    public static function creatingHandler($model)
    {
        // Encrypt the password before insert
        $model->password = bcrypt($model->password);

        return true;
    }

    /**
     * @inheritDoc
     */
    public static function updatingHandler($model)
    {
        // Encrypt the password before update
        if ($model->isDirty('password')) {
            $model->password = bcrypt($model->password);
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public static function getCatalogs($withLabels = true)
    {
        $catalogs = [];

        if (!Cache::has('tipo_usuario')) {
            $data = TipoUsuario::orderBy('descripcion', 'ASC')->get()
                ->pluck('descripcion', 'id');

            if ($withLabels == true) {
                $data->prepend(self::$labels['tipo_usuario_id'], '');
            } else {
                $data->prepend('', '');
            }

            // Guarda en cache para no volver a realizar la consulta
            // en caso de requerir los mismos datos
            Cache::put('tipo_usuario', $data->all(), config('cache.minutes'));
        }

        $catalogs['tipo_usuario'] = Cache::get('tipo_usuario');

        return $catalogs;
    }

    /**
     * Role Model related with role_id
     *
     * @return App\Models\TipoUsuario $tipoUsuario
     */
    public function tipoUsuario()
    {
        return $this->belongsTo('App\Models\TipoUsuario', 'tipo_usuario_id', 'id');
    }

    /**
     * Check if the user is active
     *
     * @return boolean
     */
    public function isActivo()
    {
        return $this->activo == 1;
    }

    /**
     * Check if the user is the creator (created_by) or owner (user_id) of the $related
     *
     * @param \Illuminate\Database\Eloquent\Model $related Object to check
     *
     * @return boolean
     */
    public function owns($related)
    {
        if (Schema::hasColumn($related->getTable(), 'created_by')) {
            return $this->id == $related->created_by;
        }

        if (Schema::hasColumn($related->getTable(), 'user_id')) {
            return $this->id == $related->user_id;
        }

        return false;
    }
}
