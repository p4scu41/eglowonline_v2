<?php

namespace App\Models\Resources;

use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Schema;
use Validator;

/**
 * Implements the process base for Model Class
 *
 * @package App\Models
 * @author  Pascual <pascual@importare.mx>
 *
 * @property string DO_CREATE
 * @property string DO_UPDATE
 * @property string $ORDER_BY
 * @property string $table
 * @property string $primaryKey
 * @property string $incrementing
 * @property int $perPage
 * @property boolean $timestamps
 * @property array $appends
 * @property array $fillable
 * @property array $dates
 * @property array $casts
 * @property array $with
 * @property array $withRelations
 * @property array $rules_create
 * @property array $rules_update
 * @property array $error_messages
 * @property array $errors
 * @property \Illuminate\Validation\Validator $validator
 * @property array $touches
 * @property array $labels
 * @property array $sortable
 * @property int $created_by
 * @property int $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property-read \App\Models\User $createdBy
 * @property-read \App\Models\User $updatedBy
 *
 * @method static void boot()
 * @method static boolean creatingHandler(\Illuminate\Database\Eloquent\Model $model)
 * @method static boolean createdHandler(\Illuminate\Database\Eloquent\Model $model)
 * @method static boolean updatingHandler(\Illuminate\Database\Eloquent\Model $model)
 * @method static boolean updatedHandler(\Illuminate\Database\Eloquent\Model $model)
 * @method static boolean deletingHandler(\Illuminate\Database\Eloquent\Model $model)
 * @method static boolean deletedHandler(\Illuminate\Database\Eloquent\Model $model)
 * @method static boolean isValid(string $action)
 * @method static array filter(array $params, boolean $paginate, string $order_by)
 * @method public \Illuminate\Database\Eloquent\Builder scopeSearch(\Illuminate\Database\Eloquent\Builder $query, array $params)
 * @method public \App\Models\User createdBy()
 * @method public \App\Models\User updatedBy()
 * @method static array getCatalogs(boolean $withLabels)
 * @method static array listToSelect(boolean $withLabels)
 *
 * @mixin \Eloquent
 */
class BaseModel extends Model
{
    use Sortable;

    /**
     * The indicator to create.
     *
     * @var string
     */
    const DO_CREATE = 'create';

    /**
     * The indicator to update.
     *
     * @var string
     */
    const DO_UPDATE = 'update';

    /**
     * The name of the column to sort.
     *
     * @var string
     */
    protected static $ORDER_BY = 'created_at';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var boolean
     */
    public $incrementing = true;

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    // protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be visible in serialization.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The relations to eager load on filter.
     *
     * @var array
     */
    public static $withRelations = [];

    /**
     * Validation rules to create a new record
     *
     * @var array
     */
    public static $rules_create = [];

    /**
     * Validation rules to update a existing record
     *
     * @var array
     */
    public static $rules_update = [];

    /**
     * Custom messages for validation errors
     *
     * @var array
     */
    public $error_messages = [];

    /**
     * List of errors thrown by validation
     *
     * @var array
     */
    public $errors = [];

    /**
     * Result of Validator::make
     *
     * @var array
     */
    public $validator = null;

    /**
     * The relationships that should be touched on save.
     *
     * @var array
     */
    protected $touches = [];

    /**
     * The labels associated to the attributes
     *
     * @var array
     */
    public static $labels = [];

    /**
     * List of attributes availables to sort
     *
     * @var array
     */
    public $sortable = [];

    /**
     * Set $sortable = $fillable
     */
    public function __construct()
    {
        parent::__construct();

        $this->sortable = $this->fillable;
    }

    /**
     * The "booting" method of the model.
     *
     * Register the handlers for the events:
     *  creating, created, updating, updated, deleting and deleted
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            // If the user is logged in and the table has the column created_by
            // store the ID who created the record
            if (Auth::check() && Schema::hasColumn($model->getTable(), 'created_by')) { // $model->getAttribute('created_by')
                $model->created_by = Auth::user()->id;
            }

            static::creatingHandler($model);
        });

        static::created(function ($model) {
            static::createdHandler($model);
        });

        static::updating(function ($model) {
            // If the user is logged in and the table has the column updated_by
            // store the ID who updated the record
            if (Auth::check() && Schema::hasColumn($model->getTable(), 'updated_by')) { // $model->getAttribute('updated_by')
                $model->updated_by = Auth::user()->id;
            }

            static::updatingHandler($model);
        });

        static::updated(function ($model) {
            static::updatedHandler($model);
        });

        static::deleting(function ($model) {
            static::deletingHandler($model);
        });

        static::deleted(function ($model) {
            static::deletedHandler($model);
        });
    }

    /**
     * Implement the handler to creating event.
     *
     * @param Illuminate\Database\Eloquent\Model $model Model to create
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static function creatingHandler($model)
    {
        return true;
    }

    /**
     * Implement the handler to created event.
     *
     * @param Illuminate\Database\Eloquent\Model $model Model created
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static function createdHandler($model)
    {
        return true;
    }

    /**
     * Implement the handler to updating event.
     *
     * @param Illuminate\Database\Eloquent\Model $model Model to update
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static function updatingHandler($model)
    {
        return true;
    }

    /**
     * Implement the handler to updated event.
     *
     * @param Illuminate\Database\Eloquent\Model $model Model updated
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static function updatedHandler($model)
    {
        return true;
    }

    /**
     * Implement the handler to deleting event.
     *
     * @param Illuminate\Database\Eloquent\Model $model Model to delete
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static function deletingHandler($model)
    {
        return true;
    }

    /**
     * Implement the handler to deleted event.
     *
     * @param Illuminate\Database\Eloquent\Model $model Model deleted
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static function deletedHandler($model)
    {
        return true;
    }

    /**
     * Get created_at
     *
     * @param string $value
     *
     * @return string
     */
    /*public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format($this->dateFormat);
    }*/

    /**
     * Set created_at
     *
     * @param string $value
     *
     * @return void
     */
    /*public function setCreatedAtAttribute($value)
    {
        $this->attributes['created_at'] = Carbon::parse($value)->format($this->dateFormat);
    }*/

    /**
     * Validate the model against the rules based on the action.
     * If the validation fails return false and populate the
     * errors array with the messages return by the Validator.
     *
     * @param string $action static::DO_CREATE, static::DO_UPDATE
     *
     * @return boolean
     *
     * @throws Exception
     */
    public function isValid($action)
    {
        $rules = [];

        switch ($action) {
            case static::DO_CREATE:
                $rules = static::$rules_create;
                break;
            case static::DO_UPDATE:
                $rules = static::$rules_update;
                break;
            default:
                throw new Exception('No valid action to validate.');
        }

        $this->validator = Validator::make($this->getAttributes(), $rules, $this->error_messages, static::$labels);

        if ($this->validator->fails()) {
            $this->errors = $this->validator->errors()->toArray();
            return false;
        }

        return true;
    }

    /**
     * Filter or search the records based on the params.
     *
     * @param array   $params   Associative array with the format
     *                          column => value, or
     *                          [column, operator, value] or
     *                          [column, operator, value, orWhere]
     *                          column = string, operator = string, value = string, orWhere = boolean
     * @param boolean $paginate Default true
     * @param string  $order_by Default null
     *
     * @return array   Result of the query
     */
    public static function filter($params, $paginate = true, $order_by = null)
    {
        $result = [];
        $query = static::search($params)->sortable(); // call scopeSearch, use kyslik/column-sortable

        $query->orderByRaw(!empty($order_by) ? $order_by : static::$ORDER_BY);

        // Eager Loading Relationship
        if (!empty(static::$withRelations)) {
            $query->with(static::$withRelations);
        }

        /*dd([
            'sql' => $query->toSql(),
            'binds' => $query->getBindings(),
            'params' => $query->getRawBindings(),
        ]);*/

        if ($paginate === true) {
            $result = $query->paginate();
        } else {
            $result = $query->get();
        }

        return $result;
    }

    /**
     * Scope a query to search by a given parameters.
     * The array of params could be associative
     *     column => value or
     *     column => function or
     *     [column, operator, value] or
     *     [column, operator, value, orWhere]
     * column = string, operator = string, value = string, orWhere = boolean
     * Valid operators [=, <=, >=, !=, <>, like, in, not in]
     * Validate if the column is part of the fillables
     *
     * @param \Illuminate\Database\Eloquent\Builder $query  Instance
     * @param array                                 $params Variables to set in where
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $params)
    {
        $attributes = $this->getFillable();

        // Add extra fields to be available in the search
        array_push($attributes, 'id', 'created_by', 'updated_by', 'created_at', 'updated_at');

        if (count($params)) {
            foreach ($params as $colum => $value) {
                // format operator [column, operator, value]
                if (is_array($value)) {
                    // Validate the column is part of the fillables
                    if (in_array($value[0], $attributes)) {
                        // If has the four parameter, it will be orWhere
                        // [column, operator, value, orWhere]
                        if (isset($value[3])) {
                            $query->orWhere($value[0], $value[1], $value[2]);
                            switch (strtolower($value[1])) {
                                case 'in':
                                    $query->orWhereIn($value[0], $value[2]);
                                    break;
                                case 'not in':
                                    $query->orWhereNotIn($value[0], $value[2]);
                                    break;
                                case 'between':
                                    $query->orWhereBetween($value[0], $value[2]);
                                    break;
                                case '=':
                                case '<=':
                                case '>=':
                                case '!=':
                                case '<>':
                                case 'like':
                                    $query->where($value[0], $value[1], $value[2]);
                                    break;
                                default:
                                    throw new Exception('Error: Operador no válido. '.json_encode($value), 422);
                            }
                        } else {
                            switch (strtolower($value[1])) {
                                case 'in':
                                    $query->whereIn($value[0], $value[2]);
                                    break;
                                case 'not in':
                                    $query->whereNotIn($value[0], $value[2]);
                                    break;
                                case 'between':
                                    $query->whereBetween($value[0], $value[2]);
                                    break;
                                case '=':
                                case '<=':
                                case '>=':
                                case '!=':
                                case '<>':
                                case 'like':
                                    $query->where($value[0], $value[1], $value[2]);
                                    break;
                                default:
                                    throw new Exception('Error: Operador no válido. '.json_encode($value), 422);
                            }
                        }
                    }
                } else { // format associative column => value
                    // Validate the column is part of the fillables
                    if (in_array($colum, $attributes)) {
                        if (is_callable($value)) { // Cuando se pasa una función como parámetro
                            $query->where($value);
                        } else {
                            $query->where($colum, $value);
                        }
                    }
                }
            }
        }

        return $query;
    }

    /**
     * Relation to get de user owner
     *
     * @return App\Models\User $usuario
     */
    public function createdBy()
    {
        if (Schema::hasColumn($this->getTable(), 'created_by')) {
            return $this->belongsTo('App\Models\User', 'created_by', 'id');
        }

        return null;
    }

    /**
     * Relation to get de user who updated
     *
     * @return App\Models\User $usuario
     */
    public function updatedBy()
    {
        if (Schema::hasColumn($this->getTable(), 'updated_by')) {
            return $this->belongsTo('App\Models\User', 'id', 'updated_by');
        }

        return null;
    }

    /**
     * Get the Catalogs list associated with the Model
     *
     * @param boolean $withLabels Whether or not to inject the label as a first element of each Catalog
     *
     * @return array
     */
    public static function getCatalogs($withLabels = true)
    {
        $catalogs = [];

        return $catalogs;
    }

    /**
     * Obtiene la lista de elementos para ser utilizados en un elemento select
     *
     * @param boolean $withLabels Para incluir la etiqueta como primer elemento del array
     *
     * @return array
     */
    public static function listToSelect($withLabels = true)
    {
        return [];
    }
}
