<?php

namespace App\Models;

class PublicacionXInfluenciadorXCampana extends BaseModel
{
    protected $table = 'publicacion_x_influenciador_x_campana';

    protected $primaryKey = 'influenciador_id';

    public static $INSERTADO           = -1;
    public static $ESPERANDO_RESPUESTA =  0;
    public static $ACEPTADO            =  1;
    public static $RECHAZADO           =  2;

    public static $ESTADO = [
        -1 => 'Insertado',
        0  => 'Esperando Respuesta',
        1  => 'Aceptado',
        2  => 'Rechazado',
    ];

    public static $PRECIO_X_PUBLICACION = [
        1 => 1000, // Twitter
        2 => 1000, // Facebook
        3 => 1000, // Instagram
        4 => 1000, // Snapchat
        5 => 1000, // Youtube
    ];

    protected $fillable = [
        'influenciador_id',
        'campana_id',
        'red_social_id',
        'cantidad_publicaciones',
        'precio_publicacion',
        'estado',
    ];

    public $rules_create = [
        'influenciador_id'       => 'bail|required|integer|exists:influenciador,id',
        'campana_id'             => 'bail|required|integer|exists:campana,id',
        'red_social_id'          => 'bail|required|integer|exists:red_social,id',
        'cantidad_publicaciones' => 'bail|required|integer',
        'precio_publicacion'     => 'bail|required|numeric',
        'estado'                 => 'bail|required|integer|in:-1,0,1,2',
    ];

    public $rules_update = [
        'influenciador_id'       => 'bail|required|integer',
        'campana_id'             => 'bail|required|integer',
        'red_social_id'          => 'bail|required|integer',
        'cantidad_publicaciones' => 'bail|required|integer',
        'precio_publicacion'     => 'bail|required|numeric',
        'estado'                 => 'bail|required|integer|in:-1,0,1,2',
    ];

    public static $labels = [
        'influenciador_id'       => 'Influenciador',
        'campana_id'             => 'Campaña',
        'red_social_id'          => 'Red Social',
        'cantidad_publicaciones' => 'Cantidad de Publicaciones',
        'precio_publicacion'     => 'Precio por Publicación',
        'estado'                 => 'Status',
    ];

    /**
     *
     * @return App\Models\Influenciador $influenciador
     */
    public function influenciador()
    {
        return $this->belongsTo('App\Models\Influenciador', 'influenciador_id', 'id');
    }

    /**
     *
     * @return App\Models\Campana $campana
     */
    public function campana()
    {
        return $this->belongsTo('App\Models\Campana', 'campana_id', 'id');
    }

    /**
     *
     * @return App\Models\RedSocial $redSocial
     */
    public function redSocial()
    {
        return $this->belongsTo('App\Models\RedSocial', 'red_social_id', 'id');
    }

    /**
     *
     * @return App\Models\Publicacion $publicacion
     */
    public function publicaciones()
    {
        return Publicacion::where([
            ['campana_id', $this->campana_id],
            ['influenciador_id', $this->influenciador_id],
            ['red_social_id', $this->red_social_id],
        ])->get();
    }

    public function isInsertado()
    {
        return $this->estado == self::$INSERTADO;
    }

    public function isEsperandoRespuesta()
    {
        return $this->estado == self::$ESPERANDO_RESPUESTA;
    }

    public function isAceptado()
    {
        return $this->estado == self::$ACEPTADO;
    }

    public function isRechazado()
    {
        return $this->estado == self::$RECHAZADO;
    }

    /**
     * Calcula el costo total las publicaciones a partir de la
     * cantidad de publicaciones por el precio de individual de publicación
     */
    public function calcPrecioPublicaciones()
    {
        // Precio por defecto por publicación
        /*$precio_publicacion = 1000;

        if ($this->influenciador) {
            switch ($this->red_social_id) {
                case RedSocial::$TWITTER:
                    if ($this->influenciador->perfilTwitter) {
                        $precio_publicacion = $this->influenciador->perfilTwitter->precio_publicacion;
                    }
                    break;
                case RedSocial::$FACEBOOK:
                    if ($this->influenciador->perfilFacebook) {
                        $precio_publicacion = $this->influenciador->perfilFacebook->precio_publicacion;
                    }
                    break;
                case RedSocial::$INSTAGRAM:
                    if ($this->influenciador->perfilInstagram) {
                        $precio_publicacion = $this->influenciador->perfilInstagram->precio_publicacion;
                    }
                    break;
                case RedSocial::$YOUTUBE:
                    if ($this->influenciador->perfilYoutube) {
                        $precio_publicacion = $this->influenciador->perfilYoutube->precio_publicacion;
                    }
                    break;
                case RedSocial::$SNAPCHAT:
                    if ($this->influenciador->perfilSnapchat) {
                        $precio_publicacion = $this->influenciador->perfilSnapchat->precio_publicacion;
                    }
                    break;
            }
        }

        $this->precio_publicacion = intval($this->cantidad_publicaciones) * intval($precio_publicacion);*/

        return intval($this->cantidad_publicaciones) * intval($this->precio_publicacion);
    }

    /**
     * Obtiene el precio_publicacion en formato de moneda
     *
     * @param  int  $value
     * @return string
     */
    public function getPrecioPublicacionCurrencyAttribute()
    {
        if (empty($this->cantidad_publicaciones)) {
            return '';
        }

        // $this->calcPrecioPublicacion();

        return '$ ' . number_format(doubleval($this->precio_publicacion)) . ' MXN';
    }
}
