<?php

namespace App\Models;

class PerfilInstagram extends PerfilBaseModel
{
    protected $table = 'perfil_instagram';

    protected $fillable = [
        'influenciador_id',
        'screen_name',
        'cantidad_seguidores',
        'profile_image_url',
    ];

    public $rules_create = [
        'influenciador_id'    => 'bail|required|integer|exists:influenciador,id',
        'screen_name'         => 'bail|nullable|max:45|unique:perfil_instagram',
        'cantidad_seguidores' => 'integer',
    ];

    public $rules_update = [
        'influenciador_id'    => 'bail|required|integer|exists:influenciador,id',
        'screen_name'         => 'bail|nullable|max:45',
        'cantidad_seguidores' => 'integer',
    ];

    public static $labels = [
        'influenciador_id'    => 'Influenciador',
        'screen_name'         => 'Usuario',
        'cantidad_seguidores' => 'Cantidad de Seguidores',
    ];
}
