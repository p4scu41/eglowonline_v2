<?php

namespace App\Models;

/**
 * @package App
 * @subpackage Models
 * @category Model
 * @author Gerardo Adrián Gómez Ruiz <gerardo@eglow.mx>
 */
class FacebookPage extends BaseModel
{
    /**
     * @var string The table name
     */
    protected $table = 'facebook_page';

    /**
     * @var bool The flag for disabling autoincrementing primary key
     */
    public $incrementing = false;

    /**
     * @var array The columns that are mass fillable
     */
    protected $fillable = ['id', 'influenciador_id', 'name', 'about', 'link', 'profile_image_url', 'engagement', 'access_token', 'categories'];
}
