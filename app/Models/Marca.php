<?php

namespace App\Models;

use App\Helpers\UploadFileTrait;
use Illuminate\Support\Facades\DB;

class Marca extends BaseModel
{
    use UploadFileTrait;

    protected $table = 'marca';

    public $fileName = 'logotipo';

    protected $fillable = [
        'agencia_id',
        'usuario_id',
        'nombre',
        'logotipo',
        'direccion',
        'activo',
    ];

    public $rules_create = [
        'agencia_id' => 'bail|required|integer|exists:agencia,id',
        'usuario_id' => 'bail|nullable|integer|exists:usuario,id|unique:marca',
        'nombre'     => 'bail|required|min:3|max:45',
        'logotipo'   => 'image',
        'direccion'  => 'max:100',
        'activo'     => 'bail|required|integer|in:0,1',
    ];

    public $rules_update = [
        'agencia_id' => 'bail|required|integer|exists:agencia,id',
        'usuario_id' => 'bail|nullable|integer|exists:usuario,id',
        'nombre'     => 'bail|required|min:3|max:45',
        'logotipo'   => 'image',
        'direccion'  => 'max:100',
        'activo'     => 'bail|required|integer|in:0,1',
    ];

    public static $labels = [
        'agencia_id' => 'Agencia',
        'usuario_id' => 'Usuario',
        'nombre'     => 'Nombre',
        'logotipo'   => 'Logotipo',
        'direccion'  => 'Dirección',
        'activo'     => 'Activo',
    ];

    public function getBaseUploadFolder()
    {
        return 'img' . DIRECTORY_SEPARATOR . 'marca';
    }

    /**
     * Path to the logotipo
     *
     * @return string
     */
    public function getLogotipo()
    {
        $logo = $this->getFile();

        if (empty($logo)) {
            $logo = 'img/placehold_logo.png';
        }

        return $logo;
    }

    /**
     *
     * @inheritDoc
     */
    public static function deletedHandler($model)
    {
        $oldFile = $model->findFile();

        // Eliminamos el logotipo, si existe
        if ($oldFile != null) {
            unlink($oldFile);
        }

        return true;
    }

    /**
     * Obtiene los registros de las tablas con las que tiene relación el modelo
     *
     * @param boolean $withLabels Para incluir las etiquetas como primer elemento del array
     * @return array
     */
    public static function getCatalogs($withLabels = true)
    {
        $catalogs = [];

        $catalogs['agencia'] = Agencia::where('activo', 1)
            ->orderBy('nombre', 'asc')
            ->get()
            ->pluck('nombre', 'id');

        $catalogs['usuario'] = User::where('tipo_usuario_id', TipoUsuario::MARCA)
            ->where('activo', 1)
            ->where(DB::raw('id NOT IN (SELECT usuario_id FROM marca)'))
            ->orderBy('nombre', 'asc')
            ->get()
            ->pluck('nombre', 'id');

        if ($withLabels == true) {
            $catalogs['agencia']->prepend(self::$labels['agencia_id'], '');
            $catalogs['usuario']->prepend(self::$labels['usuario_id'], '');
        } else {
            $catalogs['agencia']->prepend('', '');
            $catalogs['usuario']->prepend('', '');
        }

        return $catalogs;
    }

    /**
     *
     * @return App\Models\User $usuario
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'usuario_id', 'id');
    }

    /**
     *
     * @return App\Models\Agencia $agencia
     */
    public function agencia()
    {
        return $this->belongsTo('App\Models\Agencia', 'agencia_id', 'id');
    }

    public function updateCatalogUsuario()
    {
        $usuarios = DB::select("SELECT id, nombre FROM usuario WHERE tipo_usuario_id = 3 AND activo = 1 AND id NOT IN (SELECT usuario_id FROM marca) UNION SELECT u.id, u.nombre FROM usuario u INNER JOIN marca m ON m.usuario_id = u.id WHERE m.id = ?", [$this->id]);

        $usuarios = collect($usuarios)->pluck('nombre', 'id')->prepend(self::$labels['usuario_id'], '');

        return $usuarios;
    }
}
