<?php

namespace App\Models;

class TipoPalabraClave extends BaseModel
{
    const PALABRA_CLAVE = 1;
    const MARCA         = 2;
    const PRODUCTO      = 3;
    const PAIS          = 4;
    const PROFESION     = 5;

    protected $table = 'tipo_palabra_clave';

    protected $fillable = [
        'id',
        'tipo',
    ];

    /**
     *
     * @return App\Models\User $usuario
     */
    public function palabrasClaves()
    {
        return $this->hasMany('App\Models\PalabraClave', 'tipo_palabra_clave_id', 'id');
    }
}
