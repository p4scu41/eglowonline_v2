<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublicacionLog extends Model
{
    protected $table = 'publicacion_log';

    public $timestamps = false;

    protected $fillable = [
        'publicacion_id',
        'start',
        'end',
        'response',
        'error',
    ];
}
