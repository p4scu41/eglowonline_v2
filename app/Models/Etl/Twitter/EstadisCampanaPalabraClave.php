<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class EstadisCampanaPalabraClave extends Model
{
    public $table = 'estadis_campana_palabras_clave_tw';

    public $fillable = [
            'hashtag_id',
            'palabra',
            'cantidad',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
