<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class EstadisCampanaTopFollower extends Model
{
    public $table = 'estadis_campana_top_follower_tw';

    public $fillable = [
            'campana_id',
            'user_id',
            'screen_name',
            'image_url',
            'followers',
            'friends',
            'favorites',
            'tweets',
            'gender',
            'country',
            'biografia',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}

}
