<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class EstadisInfluenciadorMultimedia extends Model
{
    public $table = 'estadis_influen_multimedias_tw';

    public $fillable = [
            'influenciador_id',
            'tweets',
            'videos',
            'imagenes',
            'total_retweets',
            'total_favorites',
            'urls',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
