<?php

namespace App\Models\Etl\Twitter;

use App\Helpers\Helper;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Logger;
use DB;
use Exception;

class Campana extends Model
{
    public static $debug = true;

    /**
     * Save data on table
     *
     * @param integer  $id ID campaña
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveTopTweets($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_campana.log');

        if (self::$debug) {
            $logger->info('Campana::TopTweets('.$id.')', $data);
        }

        $ultima_revision = $data['ultima_revision'];
        $datos = collect($data['top_tweets'])
            ->map(function ($item, $key) use ($id, $ultima_revision) {
                $item['tweet_id'] = '' . $item['tweet_id'];
                $item['hashtag_id'] = $id;
                $item['ultima_revision'] = $ultima_revision;
                $item['created_at'] = date('Y-m-d H:i:s');

                return $item;
            })->toArray();

        foreach ($datos as $tweet) {
            try {
                $topTweet = EstadisCampanaTopTweet::find($tweet['tweet_id']);

                if (empty($topTweet)) {
                    $topTweet = new EstadisCampanaTopTweet();
                }

                $topTweet->fill($tweet);
                $topTweet->save();
            } catch (Exception $e) {
                $logger->error($e->getCode().' - '.$e->getMessage());
            }
        }

        // EstadisCampanaTopTweet::insert($datos->toArray());

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID campaña
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function savePalabrasClave($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_campana.log');

        if (self::$debug) {
            $logger->info('Campana::PalabrasClave('.$id.')', $data);
        }

        $ultima_revision = $data['ultima_revision'];
        $datos = collect($data['palabras_clave'])
            ->map(function ($item, $key) use ($id, $ultima_revision) {
                $item['hashtag_id'] = $id;
                $item['ultima_revision'] = $ultima_revision;
                $item['created_at'] = date('Y-m-d H:i:s');

                return $item;
            });

        EstadisCampanaPalabraClave::insert($datos->toArray());

        return true;
    }

    /**
     * Update data on table
     *
     * @param integer  $id ID campaña
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveDistribucionGenero($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_campana.log');

        if (self::$debug) {
            $logger->info('Campana::DistribucionGenero('.$id.')', $data);
        }

        $datos = $data['distribucion_genero'];
        $datos['ultima_revision'] = $data['ultima_revision'];
        $datos['hashtag_id'] = $id;

        EstadisCampanaGenero::create($datos);

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID campaña
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveDistribucionPais($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_campana.log');

        if (self::$debug) {
            $logger->info('Campana::DistribucionPais('.$id.')', $data);
        }

        $ultima_revision = $data['ultima_revision'];
        $datos = collect($data['distribucion_pais'])
            ->map(function ($item, $key) use ($id, $ultima_revision) {
                $item['hashtag_id'] = $id;
                $item['ultima_revision'] = $ultima_revision;
                $item['created_at'] = date('Y-m-d H:i:s');

                return $item;
            });

        EstadisCampanaPais::insert($datos->toArray());

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID Campaña
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveHashtags($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_campana.log');

        if (self::$debug) {
            $logger->info('Campana::Hashtags('.$id.')', $data);
        }

        $ultima_revision = $data['ultima_revision'];
        $datos = collect($data['hashtags'])
            ->map(function ($item, $key) use ($id, $ultima_revision) {
                $item['campana_id'] = $id;
                $item['ultima_revision'] = $ultima_revision;
                $item['created_at'] = date('Y-m-d H:i:s');

                return $item;
            });

        EstadisCampanaHashtag::insert($datos->toArray());

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveTopFollowers($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_campana.log');

        if (self::$debug) {
            $logger->info('Campana::TopFollowers('.$id.')', $data);
        }

        $ultima_revision = $data['ultima_revision'];
        $datos = collect($data['top_followers'])
            ->map(function ($item, $key) use ($id, $ultima_revision) {
                $item['user_id'] = '' . $item['user_id'];
                $item['campana_id'] = $id;
                $item['ultima_revision'] = $ultima_revision;
                $item['created_at'] = date('Y-m-d H:i:s');

                return $item;
            })->toArray();

        foreach ($datos as $user) {
            $topUser = EstadisCampanaTopFollower::find($user['user_id']);

            if (empty($topUser)) {
                $topUser = new EstadisCampanaTopFollower();
            }

            $topUser->fill($user);
            $topUser->save();
        }

        // EstadisCampanaTopFollower::insert($datos->toArray());

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID Campaña
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveMultimedias($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_campana.log');
        $model = new EstadisCampanaMultimedia();
        $model->campana_id = $id;

        if (self::$debug) {
            $logger->info('Campana::saveMultimedias('.$id.')', $data);
        }

        $model->fill($data);
        $model->save();

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID campaña
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveReplies($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_campana.log');

        if (self::$debug) {
            $logger->info('Campana::saveReplies('.$id.')', $data);
        }

        foreach($data as $reply) {
            try {
                $model = Reply::find('' . $reply['id']);

                if (empty($model)) {
                    $model = new Reply();
                }

                $model->fill($reply);
                $model->save();
            } catch (Exception $e) {
                $logger->error($e->getCode().' - '.$e->getMessage());
            }
        }

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID campaña
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function savePerfilesReplies($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_campana.log');

        if (self::$debug) {
            $logger->info('Campana::savePerfilesReplies('.$id.')', $data);
        }

        foreach($data as $perfil) {
            try {
                $account = Account::where('id', $perfil['id'].'')->first();

                if (empty($account)) {
                    $account = new Account();
                }

                $account->fill($perfil);
                $account->save();
            } catch (Exception $e) {
                $logger->error($e->getCode().' - '.$e->getMessage());
            }
        }

        return true;
    }
}
