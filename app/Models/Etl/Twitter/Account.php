<?php

namespace App\Models\Etl\Twitter;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use DB;

/**
 * Table account
 *
 * @package App
 * @subpackage Models\Etl\Twitter
 * @category Model
 * @author Pascual <pascual@eglow.mx>
 */
class Account extends Model
{
    protected $table = 'account';

    public $incrementing = false;

    protected $fillable = [
        'id',
        'name',
        'screen_name',
        'description',
        'followers_count',
        'friends_count',
        'statuses_count',
        'profile_image',
        'gender',
        'country',
        'created_at',
        'updated_at',
    ];
}
