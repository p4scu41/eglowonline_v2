<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class EstadisCampanaMultimedia extends Model
{
    public $table = 'estadis_campana_multimedias_tw';

    public $fillable = [
            'campana_id',
            'tweets',
            'videos',
            'imagenes',
            'urls',
            'total_followers',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
