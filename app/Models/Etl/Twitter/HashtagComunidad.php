<?php

namespace App\Models\Etl\Twitter;

use DB;
use Illuminate\Database\Eloquent\Model;

class HashtagComunidad extends Model
{
    public $table = 'estadis_influen_comu_hashtag_tw';

    public $fillable = [
            'influenciador_id',
            'hashtag',
            'cantidad',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}

    /**
     * get a list of top hashtags from influencer id
     *
     * @param int $influencerId
     * @return Illuminate\Support\Collection
     */
  /*  public static function getTopHashtags($influencerId)
    {
        return static::select('hashtag', DB::raw('SUM(cantidad) AS cantidad'))
            ->whereRaw('ultima_revision = (SELECT MAX(ultima_revision) FROM estadis_influen_comu_hashtag_tw WHERE influenciador_id = '. $influencerId .' )')
            ->where('influenciador_id', $influencerId)
            ->groupBy('hashtag')
            ->orderBy('cantidad', 'desc')
            ->take(10)
            ->get();
    }*/
}
