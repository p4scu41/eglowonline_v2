<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class EstadisCampanaGenero extends Model
{
    public $table = 'estadis_campana_genero_tw';

    public $fillable = [
            'hashtag_id',
            'hombres',
            'mujeres',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
