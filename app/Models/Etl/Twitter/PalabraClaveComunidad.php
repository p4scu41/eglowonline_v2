<?php

namespace App\Models\Etl\Twitter;

use DB;
use Illuminate\Database\Eloquent\Model;

class PalabraClaveComunidad extends Model
{
    public $table = 'estadis_influen_comu_palabra_clave_tw';

    public $fillable = [
            'influenciador_id',
            'palabra',
            'cantidad',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}

    /**
     * get a list of top keywords from influencer id
     *
     * @param int $influencerId
     * @return Illuminate\Support\Collection

    public static function getTopKeywords($influencerId)
    {
        return static::select('palabra', DB::raw('SUM(cantidad) AS cantidad'))
            ->whereRaw('ultima_revision = (SELECT MAX(ultima_revision) FROM estadis_influen_comu_palabra_clave_tw WHERE influenciador_id = '. $influencerId .' )')
            ->where('influenciador_id', $influencerId)
            ->groupBy('palabra')
            ->orderBy('cantidad', 'desc')
            ->take(10)
            ->get();
    }*/
}
