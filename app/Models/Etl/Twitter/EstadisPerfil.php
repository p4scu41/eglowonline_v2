<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class EstadisPerfil extends Model
{
    public $table = 'estadis_influenciador_tw';

    public $fillable = [
            'influenciador_id',
            'followers',
            'friends',
            'favorites',
            'tweets',
            'retweets',
            'replies',
            'ultima_revision',
        ];

    public static function getEstadisticas($id)
    {
        $query = 'SELECT
                estadis_influenciador_tw.followers,
                estadis_influenciador_tw.friends,
                estadis_influenciador_tw.favorites,
                estadis_influenciador_tw.tweets,
                estadis_influenciador_tw.retweets,
                estadis_influenciador_tw.replies
            FROM estadis_influenciador_tw
            WHERE estadis_influenciador_tw.influenciador_id = '.$id.'
            ORDER BY estadis_influenciador_tw.ultima_revision DESC
            LIMIT 1';
    }

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
