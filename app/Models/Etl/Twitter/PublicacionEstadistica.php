<?php

namespace App\Models\Etl\Twitter;

use App\Models\BaseModel;

/**
 * Manages publicacion_estadistica table
 *
 * @package App
 * @subpackage Models\Etl\Twitter
 * @category Model
 * @author Gerardo Adrián Gómez Ruiz <gerardo@eglow.mx>
 */
class PublicacionEstadistica extends BaseModel
{
    /**
     * @var string The table name
     */
    protected $table = 'publicacion_estadistica';

    /**
     * @var bool The flag indicates column created_at and updated_at should be used
     */
    public $timestamps = false;

    /**
     * @var array The columns are mass assigned
     */
    protected $fillable = ['publicacion_id', 'retweets', 'favorites', 'total_followers_rt', 'fecha'];

    /**
     * Obtiene el total de interacciones de la publicación (retweets + favorites)
     *
     * @return int
     */
    public function totalInteracciones()
    {
        return $this->retweets + $this->favorites;
    }
}
