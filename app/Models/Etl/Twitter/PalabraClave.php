<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class PalabraClave extends Model
{
    public $table = 'estadis_influen_palabra_clave_tw';

    public $fillable = [
            'influenciador_id',
            'palabra',
            'cantidad',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
