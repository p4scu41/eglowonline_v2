<?php

namespace App\Models\Etl\Twitter;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use DB;

/**
 * Table reply
 *
 * @package App
 * @subpackage Models\Etl\Twitter
 * @category Model
 * @author Pascual <pascual@eglow.mx>
 */
class Reply extends Model
{
    protected $table = 'reply';

    protected $fillable = [
        'id',
        'campana_id',
        'publicacion_id',
        'account_id',
        'texto',
        'media',
        'image_count',
        'video_count',
        'url_count',
        'retweet_count',
        'favorite_count',
        'reply_count',
        'type',
        'is_eglow',
        'published_at',
        'created_at',
        'updated_at',
    ];

    /**
     * @return App\Models\Publicacion $publicacion
     */
    public function publicacion()
    {
        return $this->belongsTo('App\Models\Publicacion', 'publicacion_id', 'id_publicacion_red_social');
    }

    /**
     * @return App\Models\Campana $campana
     */
    public function campana()
    {
        return $this->belongsTo('App\Models\Campana', 'campana_id', 'id');
    }

    /**
     * @return App\Models\Etl\Twitter\Account $account
     */
    public function account()
    {
        return $this->belongsTo('App\Models\Etl\Twitter\Account', 'account_id', 'id');
    }

    /**
     * Disable updated_at
     */
    public function setUpdatedAt($value) { }
    public function getUpdatedAtColumn() { }
}
