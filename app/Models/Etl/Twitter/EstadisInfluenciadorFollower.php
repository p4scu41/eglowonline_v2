<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class EstadisInfluenciadorFollower extends Model
{
    public $table = 'estadis_influen_followers_tw';

    public $fillable = [
            'influenciador_id',
            'hombres',
            'mujeres',
            'followers_followers',
            'followers_followers_activos',
            'followers_fakes',
            'followers_revisados',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
