<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class TopTweet extends Model
{
    public $table = 'estadis_influen_top_tweet_tw';

    public $fillable = [
            'influenciador_id',
            'retweets',
            'favorites',
            'texto',
            'fecha',
            'multimedia',
            'total_followers_rt',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}

    /**
     * verifies current tweet has multimedia
     *
     * @return bool
     */
    public function hasMultimedia()
    {
        return !empty($this->multimedia);
    }

    public function getMimeTypeMultimedia()
    {
        $ext  = explode('.', $this->multimedia);
        $long = count($ext);
        $ext  = $ext[$long - 1];
        $file = 0;

        switch ($ext) {
            case 'jpeg':
            case 'jpg':
            case 'png':
            case 'gif':
            case 'bmp':
                $file = 1;
                break;

            case 'mp4':
            case 'mp3':
            case 'wmv':
            case 'avi':
                $file = 2;
                break;
        }

        return $file;
    }

    public function isImage()
    {
        return $this->getMimeTypeMultimedia() === 1;
    }

    public function isVideo()
    {
        return $this->getMimeTypeMultimedia() === 2;
    }
}
