<?php

namespace App\Models\Etl\Twitter;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use DB;

/**
 * Table genero_publicacion
 *
 * @package App
 * @subpackage Models\Etl\Twitter
 * @category Model
 * @author Pascual <pascual@eglow.mx>
 */
class HashtagPublicacion extends Model
{
    protected $table = 'hashtag_publicacion';

    protected $fillable = [
        'publicacion_id',
        'hashtag',
        'total',
        'ultima_revision',
        'created_at',
    ];

    /**
     * Disable updated_at
     */
    public function setUpdatedAt($value) { }
    public function getUpdatedAtColumn() { }
}
