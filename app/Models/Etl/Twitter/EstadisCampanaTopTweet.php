<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class EstadisCampanaTopTweet extends Model
{
    public $table = 'estadis_campana_top_tweet_tw';

    public $fillable = [
            'hashtag_id',
            'tweet_id',
            'screen_name',
            'image_url',
            'followers',
            'retweets',
            'favorites',
            'texto',
            'multimedia',
            'total_followers_rt',
            'fecha',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}

    /**
     * verifies current tweet has multimedia
     *
     * @return bool
     */
    public function hasMultimedia()
    {
        return !is_null($this->multimedia) && strlen($this->multimedia > 0);
    }

    public function getMimeTypeMultimedia()
    {
        $ext  = explode('.', $this->multimedia);
        $long = count($ext);
        $ext  = $ext[$long - 1];
        $file = 0;

        switch ($ext) {
            case 'jpeg':
            case 'jpg':
            case 'png':
            case 'gif':
            case 'bmp':
                $file = 1;
                break;

            case 'mp4':
            case 'mp3':
            case 'wmv':
            case 'avi':
                $file = 2;
                break;
        }

        return $file;
    }

    public function isImage()
    {
        return $this->getMimeTypeMultimedia() === 1;
    }

    public function isVideo()
    {
        return $this->getMimeTypeMultimedia() === 2;
    }

    /**
     * Obtiene el engagement de la publicación
     * (Total interacciones / Total followers del influencer)
     * Devuelve procentaje con 4 dígitos
     *
     * @return int
     */
    public function engagement()
    {
       $engagement = 0;

       if ($this->followers > 0) {
           $engagement = round(
               ($this->retweets + $this->favorites) /
               $this->followers * 100, 2);
       }

       return $engagement;
    }

    /**
     * Obtiene el alcance de la publicación
     * (Seguidores del Influencer + Total followers de los seguidores del influenciador)
     *
     * @return int
     */
    public function alcance()
    {
       return $this->followers + $this->total_followers_rt;
    }
}
