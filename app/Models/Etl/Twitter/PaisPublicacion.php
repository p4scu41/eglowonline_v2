<?php

namespace App\Models\Etl\Twitter;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use DB;

/**
 * Table pais_publicacion
 *
 * @package App
 * @subpackage Models\Etl\Twitter
 * @category Model
 * @author Pascual <pascual@eglow.mx>
 */
class PaisPublicacion extends Model
{
    protected $table = 'pais_publicacion';

    protected $fillable = [
        'publicacion_id',
        'pais_id',
        'cantidad',
        'ultima_revision',
        'created_at',
    ];

    /**
     * Disable updated_at
     */
    public function setUpdatedAt($value) { }
    public function getUpdatedAtColumn() { }
}
