<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class TopFollower extends Model
{
    public $table = 'estadis_influen_top_follower_tw';

    public $fillable = [
            'influenciador_id',
            'screen_name',
            'image_url',
            'followers',
            'friends',
            'favorites',
            'tweets',
            'gender',
            'country',
            'biografia',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}

}
