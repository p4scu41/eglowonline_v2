<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class Hashtag extends Model
{
    public $table = 'estadis_influen_hashtag_tw';

    public $fillable = [
            'influenciador_id',
            'hashtag',
            'cantidad',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
