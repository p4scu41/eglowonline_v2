<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class EstadisCampanaHashtag extends Model
{
    public $table = 'estadis_campana_hashtag_tw';

    public $fillable = [
            'campana_id',
            'hashtag',
            'cantidad',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
