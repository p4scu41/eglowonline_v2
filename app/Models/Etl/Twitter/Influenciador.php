<?php

namespace App\Models\Etl\Twitter;

use App\Helpers\Helper;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\Models\PerfilTwitter;
use DB;

class Influenciador extends Model
{
    protected $table = 'estadis_influenciador_tw';

    public static $debug = true;

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function savePerfilInfluenciador($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_influenciador.log');

        if (self::$debug) {
            $logger->info('Influenciador::Perfil('.$id.')', $data);
        }

        $datos = $data['perfil'];
        $datos['influenciador_id'] = $id;
        $datos['ultima_revision'] = $data['ultima_revision'];
        $count = self::where('influenciador_id', $id)->count();

        if (isset($datos['biografia'])) {
            unset($datos['biografia']);
        }

        EstadisPerfil::create($datos);

        try {
            if ($data['perfil']['image_url'] != '') {
                $httpClient = new GuzzleHttpClient();
                $image_url = str_replace('_normal.', '.', $data['perfil']['image_url']);

                $httpClient->request('GET', $image_url, ['sink' => public_path('profile_image/'.$id.'.jpg')]);

                chmod(public_path('profile_image/'.$id.'.jpg'), 0777);
            }
        } catch(\Exception $e) {
            // No hacemos nada si la imagen no se encuentra
        }

        // Actualiza los datos en la tabla perfil
        PerfilTwitter::where(['influenciador_id' => $id])->update([
            'profile_image_url'   => str_replace('_normal.', '.', $datos['image_url']),
            'cantidad_seguidores' => $datos['followers'],
        ]);

        if ($count == 0) {
            $influenciador = \App\Models\Influenciador::find($id);
            $influenciador->biografia = $influenciador->biografia.'. Twitter: '.$data['perfil']['biografia'];
            $influenciador->save();
        }
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveTopTweets($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_influenciador.log');

        if (self::$debug) {
            $logger->info('Influenciador::TopTweets('.$id.')', $data);
        }

        $ultima_revision = $data['ultima_revision'];
        $datos = collect($data['top_tweets'])
            ->map(function ($item, $key) use ($id, $ultima_revision) {
                $item['influenciador_id'] = $id;
                $item['ultima_revision'] = $ultima_revision;

                return $item;
            });

        TopTweet::insert($datos->toArray());

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveTopFollowers($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_influenciador.log');

        if (self::$debug) {
            $logger->info('Influenciador::TopFollowers('.$id.')', $data);
        }

        $ultima_revision = $data['ultima_revision'];
        $datos = collect($data['top_followers'])
            ->map(function ($item, $key) use ($id, $ultima_revision) {
                $item['influenciador_id'] = $id;
                $item['ultima_revision'] = $ultima_revision;

                return $item;
            });

        TopFollower::insert($datos->toArray());

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function savePalabrasClave($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_influenciador.log');

        if (self::$debug) {
            $logger->info('Influenciador::PalabrasClave('.$id.')', $data);
        }

        $ultima_revision = $data['ultima_revision'];
        $datos = collect($data['palabras_clave'])
            ->map(function ($item, $key) use ($id, $ultima_revision) {
                $item['influenciador_id'] = $id;
                $item['ultima_revision'] = $ultima_revision;

                return $item;
            });

        PalabraClave::insert($datos->toArray());

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveHashtags($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_influenciador.log');

        if (self::$debug) {
            $logger->info('Influenciador::Hashtags('.$id.')', $data);
        }

        $ultima_revision = $data['ultima_revision'];
        $datos = collect($data['hashtags'])
            ->map(function ($item, $key) use ($id, $ultima_revision) {
                $item['influenciador_id'] = $id;
                $item['ultima_revision'] = $ultima_revision;

                return $item;
            });

        Hashtag::insert($datos->toArray());

        return true;
    }

    /**
     * Update data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveDistribucionGenero($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_influenciador.log');

        if (self::$debug) {
            $logger->info('Influenciador::DistribucionGenero('.$id.')', $data);
        }

        $datos = $data['distribucion_genero'];
        $datos['ultima_revision'] = $data['ultima_revision'];
        $datos['followers_followers'] = $data['followers_followers'];
        $datos['influenciador_id'] = $id;

        $estadis = EstadisInfluenciadorFollower::where('influenciador_id', $id)
            ->where('ultima_revision', $data['ultima_revision'])
            ->first();

        if (!empty($estadis)) {
            $estadis->fill($datos);
            $estadis->save();
        } else {
            EstadisInfluenciadorFollower::create($datos);
        }

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function updateFollowersActivos($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_influenciador.log');

        if (self::$debug) {
            $logger->info('Influenciador::FollowersActivos('.$id.')', $data);
        }

        $estadis = EstadisInfluenciadorFollower::where('influenciador_id', $id)
            ->where('ultima_revision', $data['ultima_revision'])
            ->first();

        if (empty($estadis)) {
            $estadis = new EstadisInfluenciadorFollower();
            $estadis->influenciador_id = $id;
            $estadis->ultima_revision = $data['ultima_revision'];
        }

        $estadis->fill($data);
        $estadis->save();

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveDistribucionPais($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_influenciador.log');

        if (self::$debug) {
            $logger->info('Influenciador::DistribucionPais('.$id.')', $data);
        }

        $ultima_revision = $data['ultima_revision'];
        $datos = collect($data['distribucion_pais'])
            ->map(function ($item, $key) use ($id, $ultima_revision) {
                $item['influenciador_id'] = $id;
                $item['ultima_revision'] = $ultima_revision;

                return $item;
            });

        Pais::insert($datos->toArray());

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveTopTweetsComunidad($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_influenciador.log');

        if (self::$debug) {
            $logger->info('Influenciador::TopTweetsComunidad('.$id.')', $data);
        }

        $ultima_revision = $data['ultima_revision'];
        $datos = collect($data['top_tweets'])
            ->map(function ($item, $key) use ($id, $ultima_revision) {
                $item['influenciador_id'] = $id;
                $item['ultima_revision'] = $ultima_revision;

                return $item;
            });

        TopTweetComunidad::insert($datos->toArray());

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function savePalabrasClaveComunidad($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_influenciador.log');

        if (self::$debug) {
            $logger->info('Influenciador::PalabrasClaveComunidad('.$id.')', $data);
        }

        $ultima_revision = $data['ultima_revision'];
        $datos = collect($data['palabras_clave'])
            ->map(function ($item, $key) use ($id, $ultima_revision) {
                $item['influenciador_id'] = $id;
                $item['ultima_revision'] = $ultima_revision;

                return $item;
            });

        PalabraClaveComunidad::insert($datos->toArray());

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveHashtagsComunidad($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_influenciador.log');

        if (self::$debug) {
            $logger->info('Influenciador::HashtagsComunidad('.$id.')', $data);
        }

        $ultima_revision = $data['ultima_revision'];
        $datos = collect($data['hashtags'])
            ->map(function ($item, $key) use ($id, $ultima_revision) {
                $item['influenciador_id'] = $id;
                $item['ultima_revision'] = $ultima_revision;

                return $item;
            });

        HashtagComunidad::insert($datos->toArray());

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer $id ID influenciador
     * @param integer $followers_fakes Cantidad de followers fakes
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function updateFollowersFakesRevisados($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_influenciador.log');
        $datos = [];

        if (isset($data['followers_fakes'])) {
            $datos['followers_fakes'] = $data['followers_fakes'];
        }

        if (isset($data['followers_revisados'])) {
            $datos['followers_revisados'] = $data['followers_revisados'];
        }

        if (self::$debug) {
            $logger->info('Influenciador::FollowersFakes('.$id.')', $datos);
        }

        $estadis = EstadisInfluenciadorFollower::where('influenciador_id', $id)
            ->where('ultima_revision', $data['ultima_revision'])
            ->first();

        if (empty($estadis)) {
            $estadis = new EstadisInfluenciadorFollower();
            $estadis->influenciador_id = $id;
            $estadis->ultima_revision = $data['ultima_revision'];
        }

        $estadis->fill($datos);
        $estadis->save();

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveMultimedias($id, $data)
    {
        $logger = Helper::createLogger('estadisticas_influenciador.log');
        $model = new EstadisInfluenciadorMultimedia();
        $model->influenciador_id = $id;

        if (self::$debug) {
            $logger->info('Influenciador::saveMultimedias('.$id.')', $data);
        }

        $model->fill($data);
        $model->save();

        return true;
    }
}
