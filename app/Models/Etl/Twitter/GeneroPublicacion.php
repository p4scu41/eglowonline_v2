<?php

namespace App\Models\Etl\Twitter;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use DB;

/**
 * Table genero_publicacion
 *
 * @package App
 * @subpackage Models\Etl\Twitter
 * @category Model
 * @author Pascual <pascual@eglow.mx>
 */
class GeneroPublicacion extends Model
{
    protected $table = 'genero_publicacion';

    protected $fillable = [
        'publicacion_id',
        'hombres',
        'mujeres',
        'ultima_revision',
        'created_at',
    ];

    /**
     * Disable updated_at
     */
    public function setUpdatedAt($value) { }
    public function getUpdatedAtColumn() { }
}
