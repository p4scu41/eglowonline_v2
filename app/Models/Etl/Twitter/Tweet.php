<?php

namespace App\Models\Etl\Twitter;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use DB;

/**
 * Table tweet
 *
 * @package App
 * @subpackage Models\Etl\Twitter
 * @category Model
 * @author Pascual <pascual@eglow.mx>
 */
class Tweet extends Model
{
    protected $table = 'tweet';

    public $incrementing = false;

    protected $fillable = [
        'id',
        'account_id',
        'in_reply_to',
        'in_retweet_to',
        'in_quote_to',
        'text',
        'media',
        'image_count',
        'video_count',
        'url_count',
        'retweet_count',
        'favorite_count',
        'reply_count',
        'type',
        'published_at',
        'created_at',
        'updated_at',
    ];
}
