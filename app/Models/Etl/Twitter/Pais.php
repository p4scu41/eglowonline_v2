<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    public $table = 'estadis_influen_pais_tw';

    public $fillable = [
            'influenciador_id',
            'pais',
            'cantidad',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
