<?php

namespace App\Models\Etl\Twitter;

use Illuminate\Database\Eloquent\Model;

class EstadisCampanaPais extends Model
{
    public $table = 'estadis_campana_pais_tw';

    public $fillable = [
            'hashtag_id',
            'pais',
            'cantidad',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
