<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;

class EstadisDistPais extends Model
{
    public $table = 'estadis_influen_pais_fb';

    public $fillable = [
            'influenciador_id',
            'pais',
            'cantidad',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
