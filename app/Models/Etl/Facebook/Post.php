<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $table = 'facebook_post';

    public $fillable = [
        'owner_type',
        'owner_id',
        'facebook_id',
        'created_time',
        'message',
        'picture',
        'type',
        'ultima_revision',
        'created_at',
        'updated_at'
    ];

    const OWNER_INFLUENCIADOR = 1;
    const OWNER_CAMPANA = 2;

    public function storePost($data)
    {
        $model = Post::where('facebook_id', $data["facebook_id"])->first();

        if(empty($model)) {
            $model = new Post();
        }

        $model->owner_type      = isset($data['owner_type'])      ? $data['owner_type']       : null;
        $model->owner_id        = isset($data['owner_id'])        ? $data['owner_id']         : null;
        $model->facebook_id     = isset($data['facebook_id'])     ? $data['facebook_id']      : null;
        $model->created_time    = isset($data['created_time'])    ? $data['created_time']     : null;
        $model->message         = isset($data['message'])         ? $data['message']          : null;
        $model->picture         = isset($data['picture'])         ? $data['picture']          : null;
        $model->type            = isset($data['type'])            ? $data['type']             : null;
        $model->ultima_revision = isset($data['ultima_revision']) ? $data['ultima_revision']  : null;
        $model->save();
        return $model->id;
    }

}