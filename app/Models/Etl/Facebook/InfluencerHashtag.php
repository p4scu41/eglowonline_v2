<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;

class InfluencerHashtag extends Model
{
    protected $table = 'facebook_influencer_hashtag';

    public $fillable = [
        'influenciador_id',
        'hashtag',
        'total',
        'ultima_revision',
        'created_at'
    ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}