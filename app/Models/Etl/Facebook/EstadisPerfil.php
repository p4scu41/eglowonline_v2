<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;

class EstadisPerfil extends Model
{
    public $table = 'estadis_influenciador_fb';

    public $fillable = [
            'influenciador_id',
            'engagement',
            'followers',
            'friends',
            'favorites',
            'publications',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
