<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class User extends Model
{
    protected $table = 'facebook_user';

    public $fillable = [
        'id',
        'name',
        'created_at',
        'updated_at'
    ];

    public function storeUser($data)
    {
        $user = User::where('id', $data['id'])->first();
        if(empty($user))
        {
            $user       = new User();
            $user->id   = $data['id'];
            $user->name = $data['name'];
            $user->save();
            return $user->id;
        }
        else
        {
            $user->name = $data['name'];
            $user->updated_at = date('Y-m-d H:i:s');
            $user->save();
            return $user->id;
        }

    }
}