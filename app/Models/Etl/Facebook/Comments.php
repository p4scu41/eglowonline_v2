<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = 'facebook_comments';

    protected $fillable = [
        'facebook_post_id',
        'follower_id',
        'message',
        'created_time',
        'created_at'
    ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}

    public function storeComments($data)
    {
        $this->facebook_post_id = $data['facebook_post_id'];
        $this->follower_id      = $data['follower_id'];
        $this->message          = $data['message'];
        $this->created_time     = $data['created_time'];
        $this->save();
    }
}