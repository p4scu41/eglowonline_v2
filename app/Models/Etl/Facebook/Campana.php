<?php

namespace App\Models\Etl\Facebook;

use App\Helpers\Helper;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Campana extends Model
{
    protected $table = 'estadis_campana_fb';

    public static $debug = true;

    /**
     * Save data on table
     *
     * @param integer  $id ID campana
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveFollowers($id, $data)
    {
        foreach ($data as $follower) {
            try {
                $attributes                     = $follower;
                $attributes['reactions']        = json_encode($attributes['reactions']);
                $attributes['campana_id'] = $id;

                $model = FollowerCampana::where([
                        'campana_id' => $attributes['campana_id'],
                        'follower_id'      => $attributes['follower_id'],
                    ])->first();

                if (empty($model)) {
                    $model = new FollowerCampana();
                }

                $model->fill($attributes);
                $model->save();
            } catch(Exception $e) {
                Log::error(Helper::getInfoException($e));
            }
        }
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveKeywords($id, $data)
    {
        $ultima_revision = $data['last_keywords_checking'];

        foreach ($data['keywords'] as $keyword) {
            try {
                $attributes                     = $keyword;
                $attributes['campana_id'] = $id;
                $attributes['ultima_revision']  = $ultima_revision;

                $model = new CampanaKeyword();
                $model->fill($attributes);
                $model->save();
            } catch(Exception $e) {
                Log::error(Helper::getInfoException($e));
            }
        }
    }

        /**
     * Save data on table
     *
     * @param integer  $id ID Campana
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveHashtags($id, $data)
    {
        $ultima_revision = $data['last_hashtags_checking'];

        foreach ($data['hashtags'] as $hashtag) {
            try {
                $attributes                     = $hashtag;
                $attributes['campana_id'] = $id;
                $attributes['ultima_revision'] = $ultima_revision;

                $model = new CampanaHashtag();
                $model->fill($attributes);
                $model->save();
            } catch(Exception $e) {
                Log::error(Helper::getInfoException($e));
            }
        }
    }
}
