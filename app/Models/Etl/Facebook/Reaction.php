<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;
class Reaction extends Model
{
    protected $table = 'facebook_reactions';

    protected $primaryKey = 'statistics_id';

    public $incrementing = false;

    public $fillable = [
        'statistics_id',
        'wow',
        'haha',
        'like',
        'love',
        'anger',
        'sorry',
        'created_at'
    ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}

    public function storeReaction($data)
    {
        $model = new Reaction();
        $model->statistics_id   = $data["statistics_id"];
        $model->wow             = isset($data["wow"])   ? $data["wow"]   : 0;
        $model->haha            = isset($data["haha"])  ? $data["haha"]  : 0;
        $model->like            = isset($data["like"])  ? $data["like"]  : 0;
        $model->love            = isset($data["love"])  ? $data["love"]  : 0;
        $model->anger           = isset($data["anger"]) ? $data["anger"] : 0;
        $model->sorry           = isset($data["sorry"]) ? $data["sorry"] : 0;
        $model->save();
    }
}