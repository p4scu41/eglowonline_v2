<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;

class FollowerInfluenciador extends Model
{
    protected $table = 'facebook_follower_influenciador';

    public $fillable = [
        'influenciador_id',
        'follower_id',
        'comments',
        'reactions',
    ];
}