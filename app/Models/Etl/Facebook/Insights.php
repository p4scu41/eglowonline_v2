<?php
namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;

class Insights extends Model
{
    protected $table = 'facebook_insights';

    protected $primaryKey = 'statistics_id';

    public $incrementing = false;

    public $fillable = [
        'statistics_id',
        'post_impressions',
        'post_video_views',
        'post_engaged_users',
        'post_video_view_time',
        'post_video_views_10s',
        'post_video_views_paid',
        'post_impressions_unique',
        'post_video_views_unique',
        'post_video_views_organic',
        'post_video_views_10s_paid',
        'post_video_avg_time_watched',
        'post_video_views_10s_unique',
        'post_video_views_autoplayed',
        'post_video_views_10s_organic',
        'post_video_views_paid_unique',
        'post_video_complete_views_paid',
        'post_video_views_organic_unique',
        'post_video_views_clicked_to_play',
        'post_video_complete_views_organic',
        'post_video_complete_views_paid_unique',
        'post_video_complete_views_organic_unique',
        'created_at'
    ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}

    public function storeInsights($data)
    {
        $model = new Insights;
        $model->statistics_id                               = isset($data["statistics_id"]) ? $data["statistics_id"] : null;
        $model->post_impressions                            = isset($data["post_impressions"]) ? $data["post_impressions"] : null;
        $model->post_impressions_unique                     = isset($data["post_impressions_unique"]) ? $data["post_impressions_unique"] : null;
        $model->post_engaged_users                          = isset($data["post_engaged_users"]) ? $data["post_engaged_users"] : null;
        $model->post_video_views                            = isset($data["post_video_views"]) ? $data["post_video_views"] : null;
        $model->post_video_view_time                        = isset($data["post_video_view_time"]) ? $data["post_video_view_time"] : null;
        $model->post_video_views_10s                        = isset($data["post_video_views_10s"]) ? $data["post_video_views_10s"] : null;
        $model->post_video_views_paid                       = isset($data["post_video_views_paid"]) ? $data["post_video_views_paid"] : null;
        $model->post_video_views_unique                     = isset($data["post_video_views_unique"]) ? $data["post_video_views_unique"] : null;
        $model->post_video_views_organic                    = isset($data["post_video_views_organic"]) ? $data["post_video_views_organic"] : null;
        $model->post_video_views_10s_paid                   = isset($data["post_video_views_10s_paid"]) ? $data["post_video_views_10s_paid"] : null;
        $model->post_video_avg_time_watched                 = isset($data["post_video_avg_time_watched"]) ? $data["post_video_avg_time_watched"] : null;
        $model->post_video_views_10s_unique                 = isset($data["post_video_views_10s_unique"]) ? $data["post_video_views_10s_unique"] : null;
        $model->post_video_views_autoplayed                 = isset($data["post_video_views_autoplayed"]) ? $data["post_video_views_autoplayed"] : null;
        $model->post_video_views_10s_organic                = isset($data["post_video_views_10s_organic"]) ? $data["post_video_views_10s_organic"] : null;
        $model->post_video_views_paid_unique                = isset($data["post_video_views_paid_unique"]) ? $data["post_video_views_paid_unique"] : null;
        $model->post_video_complete_views_paid              = isset($data["post_video_complete_views_paid"]) ? $data["post_video_complete_views_paid"] : null;
        $model->post_video_views_organic_unique             = isset($data["post_video_views_organic_unique"]) ? $data["post_video_views_organic_unique"] : null;
        $model->post_video_views_clicked_to_play            = isset($data["post_video_views_clicked_to_play"]) ? $data["post_video_views_clicked_to_play"] : null;
        $model->post_video_complete_views_organic           = isset($data["post_video_complete_views_organic"]) ? $data["post_video_complete_views_organic"] : null;
        $model->post_video_complete_views_paid_unique       = isset($data["post_video_complete_views_paid_unique"]) ? $data["post_video_complete_views_paid_unique"] : null;
        $model->post_video_complete_views_organic_unique    = isset($data["post_video_complete_views_organic_unique"]) ? $data["post_video_complete_views_organic_unique"] : null;
        if($model->save()) return $model->id;
    }
}
