<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;

class InfluencerKeyword extends Model
{
    protected $table = 'facebook_influencer_keywords';

    public $fillable = [
        'influenciador_id',
        'keyword_id',
        'total',
        'ultima_revision',
    ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}