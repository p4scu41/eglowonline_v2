<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;

class FollowerCampana extends Model
{
    protected $table = 'facebook_follower_campana';

    public $fillable = [
        'campana_id',
        'follower_id',
        'comments',
        'reactions',
    ];
}