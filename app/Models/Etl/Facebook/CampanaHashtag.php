<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;

class CampanaHashtag extends Model
{
    protected $table = 'facebook_campana_hashtag';

    public $fillable = [
        'campana_id',
        'hashtag',
        'total',
        'ultima_revision',
        'created_at'
    ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}