<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;

class EstadisDistGenero extends Model
{
    public $table = 'estadis_influenciador_genero_fb';

    public $fillable = [
            'influenciador_id',
            'F_13_17',
            'F_18_24',
            'F_25_34',
            'F_35_44',
            'F_45_54',
            'F_55_64',
            'F_65_MAS',
            'M_13_17',
            'M_18_24',
            'M_25_34',
            'M_35_44',
            'M_45_54',
            'M_55_64',
            'M_65_MAS',
            'U_25_34',
            'ultima_revision',
        ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}
