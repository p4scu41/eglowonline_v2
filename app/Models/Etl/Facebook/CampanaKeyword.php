<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;

class CampanaKeyword extends Model
{
    protected $table = 'facebook_campana_keywords';

    public $fillable = [
        'campana_id',
        'keyword_id',
        'total',
        'ultima_revision',
    ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}
}