<?php

namespace App\Models\Etl\Facebook;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    protected $table = 'facebook_statistics';

    public $fillable = [
        'facebook_post_id',
        'shares',
        'comments',
        'reaches_all',
        'reaches_unique',
        'ultima_revision',
        'created_at'
    ];

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value) {}
    public function getUpdatedAtColumn() {}


    public function storeStatistic($data)
    {
        $this->facebook_post_id = isset($data["facebook_post_id"]) ? $data["facebook_post_id"] : null;
        $this->shares           = isset($data["shares"])           ? $data["shares"]           : null;
        $this->comments         = isset($data["comments"])         ? $data["comments"]         : null;
        $this->reaches_all      = isset($data["reaches_all"])      ? $data["reaches_all"]      : null;
        $this->reaches_unique   = isset($data["reaches_unique"])   ? $data["reaches_unique"]   : null;
        $this->ultima_revision  = isset($data["ultima_revision"])  ? $data["ultima_revision"]  : null;
        $this->save();
    }

}