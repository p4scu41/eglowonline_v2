<?php

namespace App\Models\Etl\Facebook;

use App\Helpers\Helper;
use App\Models\PerfilFacebook;
use DB;
use Exception;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Influenciador extends Model
{
    protected $table = 'estadis_influenciador_fb';

    public static $debug = true;

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function savePerfilInfluenciador($id, $data)
    {
        $logger    = Helper::createLogger('estadisticas_influenciador_fb.log');
        $image_url = '';

        $data['influenciador_id'] = $id;
        $data['ultima_revision'] = $data['last_profile_review'];

        $estadisticas = new EstadisPerfil();

        $estadisticas->fill($data);
        $estadisticas->save();

        try {
            if ($data['profile_image_url'] != '') {
                $httpClient = new GuzzleHttpClient();
                $image_url = $data['profile_image_url'];
                $httpClient->request('GET', $image_url, ['sink' => public_path('profile_image/'.$id.'_fb.jpg')]);
            }
        } catch(\Exception $e) {
            Log::error('Error al guardar la imagen: ' . $image_url . '. ' .
                $e->getMessage() . '. ' . $e->getTraceAsString());
        }

        // Actualiza los datos en la tabla perfil
        PerfilFacebook::where(['influenciador_id' => $id])->update([
            'link'                => $data['link'],
            'profile_image_url'   => $data['profile_image_url'],
            'cantidad_seguidores' => isset($data['followers']) ? $data['followers'] : 0,
            'page_categories'     => $data['categories'],
        ]);
    }

    /**
     * Update data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveDistribucionGenero($id, $data)
    {
        $datos = $data['gender_distribution'];

        if (!empty($datos)) {
            $datos['ultima_revision'] = $data['last_profile_review'];
            $datos['influenciador_id'] = $id;

            $estadis = new EstadisDistGenero();

            $estadis->fill($datos);
            $estadis->save();
        }

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveDistribucionPais($id, $data)
    {
        $ultima_revision = $data['last_profile_review'];

        if (!empty($data['country_distribution'])) {
            $datos = collect($data['country_distribution'])
                ->map(function ($item, $key) use ($id, $ultima_revision) {
                    $pais['pais']             = $item['country_id'];
                    $pais['cantidad']         = $item['total'];
                    $pais['influenciador_id'] = $id;
                    $pais['ultima_revision']  = $ultima_revision;

                    return $pais;
                });

            EstadisDistPais::insert($datos->toArray());
        }

        return true;
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveFollowers($id, $data)
    {
        foreach ($data as $follower) {
            try {
                $attributes                     = $follower;
                $attributes['reactions']        = json_encode($attributes['reactions']);
                $attributes['influenciador_id'] = $id;

                $model = FollowerInfluenciador::where([
                        'influenciador_id' => $attributes['influenciador_id'],
                        'follower_id'      => $attributes['follower_id'],
                    ])->first();

                if (empty($model)) {
                    $model = new FollowerInfluenciador();
                }

                $model->fill($attributes);
                $model->save();
            } catch(Exception $e) {
                Log::error(Helper::getInfoException($e));
            }
        }
    }

    /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveKeywords($id, $data)
    {
        $ultima_revision = $data['last_keywords_checking'];

        foreach ($data['keywords'] as $keyword) {
            try {
                $attributes                     = $keyword;
                $attributes['influenciador_id'] = $id;
                $attributes['ultima_revision']  = $ultima_revision;

                $model = new InfluencerKeyword();
                $model->fill($attributes);
                $model->save();
            } catch(Exception $e) {
                Log::error(Helper::getInfoException($e));
            }
        }
    }

        /**
     * Save data on table
     *
     * @param integer  $id ID influenciador
     * @param array  $data Associative array with:
     *
     * @throws Exception If something goes wrong
     *
     * @return boolean
     */
    public static function saveHashtags($id, $data)
    {
        $ultima_revision = $data['last_hashtags_checking'];

        foreach ($data['hashtags'] as $hashtag) {
            try {
                $attributes                     = $hashtag;
                $attributes['influenciador_id'] = $id;
                $attributes['ultima_revision'] = $ultima_revision;

                $model = new InfluencerHashtag();
                $model->fill($attributes);
                $model->save();
            } catch(Exception $e) {
                Log::error(Helper::getInfoException($e));
            }
        }
    }
}
