<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EntornoXInfluenciador extends Model
{
    protected $table = 'entorno_x_influenciador';

    public $timestamps = false;

    protected $primaryKey = 'influenciador_id';

    protected $fillable = [
        'entorno_id',
        'influenciador_id',
    ];
}
