<?php

namespace App\Models;

class InfluenciadorXPalabraClave extends BaseModel
{
    protected $table = 'influenciador_x_palabra_clave';

    public $timestamps = false;

    protected $fillable = [
        'influenciador_id',
        'tipo_relacion_palabra_clave_id',
        'palabra_clave_id',
    ];

    public $rules_create = [
        'influenciador_id'               => 'bail|required|integer|exists:influenciador,id',
        'palabra_clave_id'               => 'bail|required|integer|exists:palabra_clave,id',
        'tipo_relacion_palabra_clave_id' => 'bail|required|integer|exists:tipo_relacion_palabra_clave,id',
    ];

    public $rules_update = [
        'influenciador_id'               => 'bail|required|integer|exists:influenciador,id',
        'palabra_clave_id'               => 'bail|required|integer|exists:palabra_clave,id',
        'tipo_relacion_palabra_clave_id' => 'bail|required|integer|exists:tipo_relacion_palabra_clave,id',
    ];

    /**
     *
     * @return App\Models\Influenciador $influenciador
     */
    public function influenciador()
    {
        return $this->belongsTo('App\Models\Influenciador', 'influenciador_id', 'id');
    }

    /**
     *
     * @return App\Models\PalabraClave $palabraClave
     */
    public function palabraClave()
    {
        return $this->belongsTo('App\Models\PalabraClave', 'palabra_clave_id', 'id');
    }

    /**
     *
     * @return App\Models\TipoRelacionPalabraClave $tipoRelacionPalabraClave
     */
    public function tipoRelacionPalabraClave()
    {
        return $this->belongsTo('App\Models\TipoRelacionPalabraClave', 'tipo_relacion_palabra_clave_id', 'id');
    }
}
