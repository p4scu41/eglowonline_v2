<?php

namespace App\Models;

use DateInterval;
use DateTime;
use App\Helpers\Helper;
use App\Helpers\UploadFileTrait;
use App\Models\InfluenciadorFacturacion;
use App\Models\Etl\Twitter\Hashtag;
use App\Models\Etl\Twitter\PalabraClave as ETLPalabraClave;
use App\Models\Etl\Twitter\HashtagComunidad;
use App\Models\Etl\Twitter\PalabraClaveComunidad;
use DB;
use Carbon\Carbon;
use Exception;
use Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class Influenciador extends BaseModel
{
    use UploadFileTrait;

    protected $table = 'influenciador';

    public static $GENERO = [
        1 => 'Hombre',
        2 => 'Mujer',
        3 => 'No Aplica'
    ];

    // La carpeta de 'json' se encuentra dentro de storage\app
    public static $folderJsons = 'json/';
    //public $qty_followers    = null;
    //public $engagement_ambos = null;
    //public $engagement_rate  = null;

    protected $with = [
        // 'industria',
        // 'rangoEtario',
        // 'tipoInfluenciador',
    ];

    protected $appends = [
        'qty_followers',
        'engagement_ambos',
        'engagement_rate',
    ];

    protected $fillable = [
        'usuario_id',
        'nombre',
        'biografia',
        'industria_id',
        // 'profesion',
        'rango_etario_id',
        'lugar_nacimiento',
        'pais_id',
        'genero',
        'tipo_influenciador_id',
        'website',
        'telefono',
        'notificar_x_mensaje',
        'notificar_x_correo',
        'manager_nombre',
        'manager_telefono',
    ];

    public $rules_create = [
        'usuario_id'            => 'bail|required|integer|exists:usuario,id',
        'nombre'                => 'bail|required|string|min:3|max:60',
        'biografia'             => 'bail|nullable|string|max:2000',
        'industria_id'          => 'bail|required|integer|exists:industria,id',
        // 'profesion'             => 'bail|nullable|string|max:50',
        'rango_etario_id'       => 'bail|required|integer|exists:rango_etario,id',
        'lugar_nacimiento'      => 'bail|nullable|string|max:100',
        'pais_id'               => 'bail|required|integer|exists:pais,id',
        'genero'                => 'bail|required|integer|in:0,1,2,3',
        'tipo_influenciador_id' => 'bail|required|integer|exists:tipo_influenciador,id',
        'website'               => 'bail|nullable|string|max:100',
        'telefono'              => 'bail|nullable|string|max:20',
        'notificar_x_mensaje'   => 'bail|integer|in:0,1',
        'notificar_x_correo'    => 'bail|required|integer|in:0,1',
        'manager_nombre'        => 'bail|nullable|string|max:70',
        'manager_telefono'      => 'bail|nullable|string|max:20',
    ];

    public $rules_update = [
        'usuario_id'            => 'bail|required|integer|exists:usuario,id',
        'nombre'                => 'bail|required|string|min:3|max:60',
        'biografia'             => 'bail|nullable|string|max:2000',
        'industria_id'          => 'bail|required|integer|exists:industria,id',
        // 'profesion'             => 'bail|nullable|string|max:50',
        'rango_etario_id'       => 'bail|required|integer|exists:rango_etario,id',
        'lugar_nacimiento'      => 'bail|nullable|string|max:100',
        'pais_id'               => 'bail|required|integer|exists:pais,id',
        'genero'                => 'bail|required|integer|in:0,1,2,3',
        'tipo_influenciador_id' => 'bail|required|integer|exists:tipo_influenciador,id',
        'website'               => 'bail|nullable|string|max:100',
        'telefono'              => 'bail|nullable|string|max:20',
        'notificar_x_mensaje'   => 'bail|integer|in:0,1',
        'notificar_x_correo'    => 'bail|required|integer|in:0,1',
        'manager_nombre'        => 'bail|nullable|string|max:70',
        'manager_telefono'      => 'bail|nullable|string|max:20',
    ];

    public static $labels = [
        'usuario_id'                     => 'Usuario',
        'nombre'                         => 'Nombre',
        'biografia'                      => 'Biografía',
        'industria_id'                   => 'Industria',
        'profesion'                      => 'Profesión',
        'rango_etario_id'                => 'Rango Etario del Influencer',
        'lugar_nacimiento'               => 'Lugar de Nacimiento',
        'pais_id'                        => 'País de Residencia',
        'genero'                         => 'Género',
        'tipo_influenciador_id'          => 'Tipo de Influenciador',
        'website'                        => 'Sitio Web (Blog, Foro, ...)',
        'telefono'                       => 'Teléfono',
        'notificar_x_mensaje'            => 'Notificaciones por mensaje',
        'notificar_x_correo'             => 'Notificaciones por correo',
        'manager_nombre'                 => 'Nombre del Manager/Agencia',
        'manager_telefono'               => 'Teléfono del Manager/Agencia',
        /* Estos campos pertenecen a usuario */
        'email'                          => 'E-mail',
        'activo'                         => 'Activo',
        'ultimo_acceso'                  => 'Último acceso',
        /* Estos campos pertenecen a influenciador_x_palabra_clave */
        'tipo_relacion_palabra_clave_id' => 'Tipo de Relación',
        'palabra_clave_id'               => 'Palabras Clave',
    ];

    private $interactionsByCampana;

    /**
     * returns all interactions
     *
     * @var stdClasss
     */
    private $interactions;

    /**
     * Porcentaje de la muestra analizada
     * (followers_revisados / cantidad_seguidores) * 100
     */
    private $porcentaje_muestra;

    /**
     * Obtiene el engagement del influenciador considerando su mejor publicación de todos los tiempos
     * Mutator: engagement_rate
     * Devuelve porcentaje con 2 digitos
     * @return int
     */
    public function getEngagementRateAttribute($value)
    {
        return $this->getEngagement();

        /*$result = 0;

        // TODO: Se calcula el engagement con el total de followers de Twitter
        // NO se debe calcular con el total de followers de todas las redes sociales
        if ($this->perfilTwitter) {
            if ($this->perfilTwitter->estadisPerfil) {
                if ($this->perfilTwitter->estadisPerfil->followers != 0) {
                    $result = round($this->interacciones_mejor_publicacion/$this->perfilTwitter->estadisPerfil->followers * 100, 2);
                    // Actualizar con la nueva formula
                }
            }
        }

        return $result;*/

        /*if (!empty($this->engagement_ambos)) {
            return round($this->qty_followers / $this->engagement_ambos * 100, 1);
        }

        return 0;*/
    }

    public function getQtyFollowersAttribute($value)
    {
        //return $this->qty_followers;
        return $this->total_followers;
    }

    public function getPorcentajeMuestraAttribute($value)
    {
        if (empty($this->porcentaje_muestra)) {
            $result = DB::select('SELECT
                    perfil_twitter.cantidad_seguidores,
                    estadis_influen_followers_tw.followers_fakes,
                    estadis_influen_followers_tw.followers_revisados,
                    ROUND(estadis_influen_followers_tw.followers_revisados/perfil_twitter.cantidad_seguidores, 2)* 100 AS porcentaje_muestra,
                    estadis_influen_followers_tw.ultima_revision
                FROM estadis_influen_followers_tw
                INNER JOIN perfil_twitter ON
                    estadis_influen_followers_tw.influenciador_id = perfil_twitter.influenciador_id
                WHERE estadis_influen_followers_tw.influenciador_id = '.$this->id.'
                ORDER BY ultima_revision DESC
                LIMIT 1');

            if (!empty($result)) {
                $this->porcentaje_muestra = (int) $result[0]->porcentaje_muestra;
            }
        }

        return $this->porcentaje_muestra;
    }


    public function getEngagementAmbosAttribute($value)
    {
        return $this->engagement_rate;
        //return $this->engagement_ambos;
    }

    public function getBaseUploadFolder()
    {
        return 'profile_image';
    }

    public function getImgPerfilAttribute($value)
    {
        $img = $this->getFile();

        if (empty($img) && !empty($this->perfilTwitter)) {
            $img = $this->perfilTwitter->profile_image_url;
        } else {
            $img = empty($img) ? asset('img/user_icon.png') : asset($img);
        }

        return $img;
    }

    /**
     * Obtiene el total de followers de todas las redes sociales
     *
     * Mutator: total_followers
     * @return int
     */
    public function getTotalFollowersAttribute($value)
    {
        $followers = 0;

        if (!empty($this->perfilTwitter)) {
            if ($this->perfilTwitter->estadisPerfil) {
                $followers += $this->perfilTwitter->estadisPerfil->followers;
            }
        }

        if (!empty($this->perfilFacebook)) {
            $followers += $this->perfilFacebook->cantidad_seguidores;
        }

        if (!empty($this->perfilInstagram)) {
            $followers += $this->perfilInstagram->cantidad_seguidores;
        }

        if (!empty($this->perfilSnapchat)) {
            $followers += $this->perfilSnapchat->cantidad_seguidores;
        }

        if (!empty($this->perfilYoutube)) {
            $followers += $this->perfilYoutube->cantidad_seguidores;
        }

        return $followers;
    }

    /**
     * Devuelve arreglo con el total de seguidores por cada red social
     * Ejemplo:
     * [
     *       'red_social' => ['Twitter', 'Facebook'],
     *       'seguidores' => [1567, 4675],
     * ]
     * Mutator: json_grafica_total_seguidores
     *
     * @return array
     */
    public function getJsonGraficaTotalSeguidoresAttribute($value)
    {
        $seguidores = [
            'red_social' => [],
            'seguidores' => [],
            'icons'      => [
                asset('img/influenciador_perfil/1.png'),
                asset('img/influenciador_perfil/2.png'),
                asset('img/influenciador_perfil/3.png'),
                asset('img/influenciador_perfil/4.png'),
            ]
        ];

        $seguidores['red_social'][] = 'Twitter';
        if (!empty($this->perfilTwitter)) {
            if ($this->perfilTwitter->estadisPerfil) {
                $seguidores['seguidores'][] = $this->perfilTwitter->estadisPerfil->followers;
            } else {
                $seguidores['seguidores'][] = 0;
            }
        } else {
            $seguidores['seguidores'][] = 0;
        }

        $seguidores['red_social'][] = 'Facebook';
        if (!empty($this->perfilFacebook)) {
            $seguidores['seguidores'][] = $this->perfilFacebook->cantidad_seguidores;
        } else {
            $seguidores['seguidores'][] = 0;
        }

        $seguidores['red_social'][] = 'Instagram';
        if (!empty($this->perfilInstagram)) {
            $seguidores['seguidores'][] = $this->perfilInstagram->cantidad_seguidores;
        } else {
            $seguidores['seguidores'][] = 0;
        }

        /*$seguidores['red_social'][] = 'Snapchat';
        if (!empty($this->perfilSnapchat)) {
            $seguidores['seguidores'][] = $this->perfilSnapchat->cantidad_seguidores;
        } else {
            $seguidores['seguidores'][] = 0;
        }*/

        $seguidores['red_social'][] = 'Youtube';
        if (!empty($this->perfilYoutube)) {
            $seguidores['seguidores'][] = $this->perfilYoutube->cantidad_seguidores;
        } else {
            $seguidores['seguidores'][] = 0;
        }

        return $seguidores;
    }

    /**
     * Obtiene el total de hombres de todas las redes sociales
     */
    public function getTotalHombresAttribute($value)
    {
        $hombres = 0;

        if (!empty($this->perfilTwitter)) {
            if ($this->perfilTwitter->estadisFollowers) {
                $hombres += $this->perfilTwitter->estadisFollowers->hombres;
            }
        }

        return $hombres;
    }

    /**
     * Obtiene el total de mujeres de todas las redes sociales
     */
    public function getTotalMujeresAttribute($value)
    {
        $mujeres = 0;

        if (!empty($this->perfilTwitter)) {
            if ($this->perfilTwitter->estadisFollowers) {
                $mujeres += $this->perfilTwitter->estadisFollowers->mujeres;
            }
        }

        return $mujeres;
    }

    /**
     * Obtiene el total de followers de sus followers de todas las redes sociales
     * Mutator: total_followers_comunidad
     */
    public function getTotalFollowersComunidadAttribute($value)
    {
        $followers = 0;

        if (!empty($this->perfilTwitter)) {
            if ($this->perfilTwitter->estadisFollowers) {
                $followers += $this->perfilTwitter->estadisFollowers->followers_followers;
            }
        }

        return $followers;
    }

    /**
     * Obtiene el total de followers de sus followers de todas las redes sociales
     *
     * Mutator: total_followers_comunidad_activos
     */
    public function getTotalFollowersComunidadActivosAttribute($value)
    {
        $followers = 0;

        if (!empty($this->perfilTwitter)) {
            if ($this->perfilTwitter->estadisFollowers) {
                $followers += $this->perfilTwitter->estadisFollowers->followers_followers;
            }
        }

        return $followers;
    }

    /**
     * Obtiene el alcance real del influenciador
     * Mutator: alcance_real
     *
     * @return int
     */
    public function getAlcanceRealAttribute($value)
    {
        // Accessors ->https://laravel.com/docs/5.3/eloquent-mutators#defining-an-accessor
        //return $this->total_followers + $this->total_followers_comunidad_activos;
        $alcance = 0; //$this->total_followers_comunidad_activos;

        if ($this->perfilTwitter) {
            if ($this->perfilTwitter->estadisPerfil) {
                $alcance += $this->perfilTwitter->estadisPerfil->followers;
            }
        }

        return $alcance;
    }

    /**
     * Followers + Followers de sus Followers
     * Mutator: alcance_potencial
     *
     * @return int
     */
    public function getAlcancePotencialAttribute($value)
    {
        // Accessors ->https://laravel.com/docs/5.3/eloquent-mutators#defining-an-accessor
        return $this->total_followers + $this->total_followers_comunidad;
    }

    /**
     * Obtiene el total de interacciones de la mejor publicación de todos los tiempos
     * Mutator: interacciones_mejor_publicacion
     *
     * @return int
     */
    public function getInteraccionesMejorPublicacionAttribute($value)
    {
        $interacciones = 0;
        $mejorTweet = !empty($this->perfilTwitter) ? $this->perfilTwitter->mejorTweet() : null;

        if ($mejorTweet) {
            $interacciones = $mejorTweet->retweets + $mejorTweet->favorites;
        }

        return $interacciones;
    }

    public function getInteraccionesTotalesAttribute($value)
    {
        $interacciones = 0;

        if (!empty($this->perfilTwitter)) {
            if ($this->perfilTwitter->estadisPerfil) {
                $interacciones += $this->perfilTwitter->estadisPerfil->favorites + $this->perfilTwitter->estadisPerfil->retweets;
            }
        }

        return $interacciones;
    }

    public function getInteraccionesCada10000Attribute($value)
    {
        $interacciones = 0;

        if ($this->perfilTwitter) {
            if ($this->perfilTwitter->estadisPerfil) {
                if ($this->perfilTwitter->estadisPerfil->followers != 0 ) {
                    $interacciones = ($this->interacciones_totales * 1000) / $this->perfilTwitter->estadisPerfil->followers;
                }
            }
        }

        return $interacciones;
    }

    public function getPublicacionesTotalAttribute($value)
    {
        $publicaciones = 0;

        if (!empty($this->perfilTwitter)) {
            if ($this->perfilTwitter->estadisPerfil) {
                $publicaciones += $this->perfilTwitter->estadisPerfil->tweets + $this->perfilTwitter->estadisPerfil->retweets;
            }
        }

        return $publicaciones;
    }

    /**
     * Obtiene la distribución de paises
     *
     * Variable: distribucion_paises
     */
    public function getDistribucionPaisesAttribute($value)
    {
        $paises = [
            'paises' => [],
            'cantidades' => [],
        ];

        if (!empty($this->perfilTwitter)) {
            if (count($this->perfilTwitter->paises)) {
                $paises = $this->perfilTwitter->getListDistribucionPaises();
            }
        }

        // Transforma las cantidades a porcentajes
        if (!empty($paises['cantidades'])) {
            $collectResult = collect($paises['cantidades']);
            $suma_cantidades = $collectResult->sum();

            $paises['cantidades'] = $collectResult->map(function ($item, $key) use ($suma_cantidades) {
                return round(($item/$suma_cantidades) * 100);
            })->toArray();
        }

        $paises['paises']     = array_reverse($paises['paises']);
        $paises['cantidades'] = array_reverse($paises['cantidades']);
        return $paises;
    }

    /**
     * Obtiene los registros de las tablas con las que tiene relación el modelo
     *
     * @param boolean $withLabels Para incluir las etiquetas como primer elemento del array
     * @return array
     */
    public static function getCatalogs($withLabels = true)
    {
        $catalogs = [];

        $catalogs['industria'] = Industria::where('activo', 1)
            ->orderBy('nombre', 'asc')
            ->get()
            ->pluck('nombre', 'id');

        $catalogs['rango_etario'] = RangoEtario::orderBy('descripcion', 'asc')
            ->get()
            ->pluck('descripcion', 'id');

        $catalogs['pais'] = Pais::where('activo', 1)
            ->orderBy('pais', 'asc')
            ->get()
            ->pluck('pais', 'id');

        $catalogs['tipo_influenciador'] = TipoInfluenciador::where('activo', 1)
            ->orderBy('descripcion', 'asc')
            ->get()
            ->pluck('descripcion', 'id');

        $catalogs['palabra_clave'] = PalabraClave::where('activo', 1)
            ->orderBy('palabra', 'asc')
            ->get()
            ->pluck('palabra', 'id');

        $catalogs['tipo_relacion_palabra_clave'] = TipoRelacionPalabraClave::orderBy('descripcion', 'asc')
            ->get()
            ->pluck('descripcion', 'id');

        $catalogs['genero'] = collect(self::$GENERO);

        $catalogs['relacion_marca'] = TipoRelacionPalabraClave::listRelacionMarca();

        $catalogs['red_social'] = RedSocial::where('activo', 1)->get();

        if ($withLabels == true) {
            $catalogs['industria']->prepend(self::$labels['industria_id'], '');
            $catalogs['rango_etario']->prepend(self::$labels['rango_etario_id'], '');
            $catalogs['pais']->prepend(self::$labels['pais_id'], '');
            $catalogs['tipo_influenciador']->prepend(self::$labels['tipo_influenciador_id'], '');
            $catalogs['palabra_clave']->prepend(self::$labels['palabra_clave_id'], '');
            $catalogs['genero']->prepend(self::$labels['genero'], '');
            $catalogs['tipo_relacion_palabra_clave']->prepend(self::$labels['tipo_relacion_palabra_clave_id'], '');
        } else {
            $catalogs['industria']->prepend('', '');
            $catalogs['rango_etario']->prepend('', '');
            $catalogs['pais']->prepend('', '');
            $catalogs['tipo_influenciador']->prepend('', '');
            $catalogs['palabra_clave']->prepend('', '');
            $catalogs['genero']->prepend('', '');
            $catalogs['tipo_relacion_palabra_clave']->prepend('', '');
        }

        return $catalogs;
    }

    /**
     * Obtiene los registros de las tablas con las que tiene relación el modelo para establecer los Selects
     *
     * @param boolean $withLabels Para incluir las etiquetas como primer elemento del array
     * @return collect
     */
    public static function getCatalogsToSelect($withLabels = true)
    {
        $catalogs = self::getCatalogs($withLabels);

        $catalogs['genero']             = collect(Genero::$generos);
        $catalogs['si_no']              = SiNo::$opciones;
        $catalogs['industria']          = collect(Industria::get()->toArray());
        $catalogs['rango_etario']       = collect(RangoEtario::get()->toArray());
        $catalogs['tipo_influenciador'] = collect(TipoInfluenciador::get()->toArray());

        // Se agrega elementos al inicio, porque selectToList detecta como selected al primer elemento
        // aun y cuando no esta seleccionado, esto pasa al cargar la página
        $catalogs['genero']->prepend(['id'=>'', 'genero'=>'', 'icono'=>'']);
        $catalogs['industria']->prepend(['id'=>'', 'nombre'=>'', 'icono'=>'']);
        $catalogs['rango_etario']->prepend(['id'=>'', 'descripcion'=>'', 'icono'=>'']);
        $catalogs['tipo_influenciador']->prepend(['id'=>'', 'descripcion'=>'', 'icono'=>'']);

        return $catalogs;
    }

    /**
     * Obtiene la descripción del género
     *
     * @param  int $value
     * @return string
     */
    public function getDescripcionGeneroAttribute($value)
    {
        return self::$GENERO[$this->genero];
    }

    /**
     *
     * @inheritDoc
     */
    public static function deletedHandler($model)
    {
        if ($model->usuario) {
            $model->usuario()->delete();
        }

        if ($model->influenciadorXPalabrasClaves) {
            $model->influenciadorXPalabrasClaves()->delete();
        }

        return true;
    }

    /**
     *
     * @return App\Models\User $usuario
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'usuario_id', 'id');
    }

    /**
     *
     * @return App\Models\Industria $industria
     */
    public function industria()
    {
        return $this->belongsTo('App\Models\Industria', 'industria_id', 'id');
    }

    /**
     *
     * @return App\Models\PerfilTwitter $perfilTwitter
     */
    public function perfilTwitter()
    {
        return $this->hasOne('App\Models\PerfilTwitter', 'influenciador_id', 'id');
    }

    /**
     *
     * @return App\Models\PerfilFacebook $perfilFacebook
     */
    public function perfilFacebook()
    {
        return $this->hasOne('App\Models\PerfilFacebook', 'influenciador_id', 'id');
    }

    /**
     *
     * @return App\Models\PerfilInstagram $perfilInstagram
     */
    public function perfilInstagram()
    {
        return $this->hasOne('App\Models\PerfilInstagram', 'influenciador_id', 'id');
    }

    /**
     *
     * @return App\Models\PerfilSnapchat $perfilSnapchat
     */
    public function perfilSnapchat()
    {
        return $this->hasOne('App\Models\PerfilSnapchat', 'influenciador_id', 'id');
    }

    /**
     *
     * @return App\Models\PerfilYoutube $perfilYoutube
     */
    public function perfilYoutube()
    {
        return $this->hasOne('App\Models\PerfilYoutube', 'influenciador_id', 'id');
    }

    /**
     *
     * @return App\Models\InfluenciadorXPalabraClave[] $influenciadorXPalabrasClaves
     */
    public function influenciadorXPalabrasClaves()
    {
        return $this->hasMany('App\Models\InfluenciadorXPalabraClave', 'influenciador_id', 'id');
    }

    /**
     *
     * @return App\Models\Entorno[] $entornos
     */
    public function entornos()
    {
        return $this->belongsToMany('App\Models\Entorno', 'entorno_x_influenciador', 'influenciador_id', 'entorno_id');
    }

    /**
     *
     * @return App\Models\influenciadorXProfesion[] $influenciadorXProfesion
     */
    public function influenciadorXProfesion()
    {
        return $this->hasMany('App\Models\InfluenciadorXProfesion', 'influenciador_id', 'id');
    }

    /**
     * @return InfluenciadorFacturacion
     */
    public function factura()
    {
        return $this->hasOne(InfluenciadorFacturacion::class, 'influenciador_id', 'id');
    }

    /**
     *
     * @param  array  $tiposPalabraClave
     * @param  array  $tipoRelacionPalabraClave
     * @return InfluenciadorXPalabraClave[]
     */
    public function palabrasClaveInfluenciador($tiposPalabraClave = [], $tiposRelacionPalabraClave = [])
    {
        $query = InfluenciadorXPalabraClave::where('influenciador_id', $this->id)
            ->join('palabra_clave', function ($join) {
                $join->on('palabra_clave.id', '=', 'influenciador_x_palabra_clave.palabra_clave_id');
            })
            ->with('palabraClave'); // , 'tipoRelacionPalabraClave'

        if (!empty($tiposPalabraClave)) {
            $query->whereIn('palabra_clave.tipo_palabra_clave_id', $tiposPalabraClave);
        }

        if (!empty($tiposRelacionPalabraClave)) {
            $query->whereIn('tipo_relacion_palabra_clave_id', $tiposRelacionPalabraClave);
        }

        return $query->get();
    }

    /**
     * Obtiene el array de ids de los influenciadores que estan relacionados con la lista de
     * palabras claves pasada como parámetros
     *
     * @param array  $palabras_claves
     * @return array
     **/
    public static function getIdsInflueRelatedWithPalabrasClaves($palabras_claves)
    {
        return InfluenciadorXPalabraClave::select('influenciador_id')
            ->where('tipo_relacion_palabra_clave_id', '!=', TipoRelacionPalabraClave::$NO_DESEO_COLABORAR_CON_LA_MARCA)
            ->whereIn('palabra_clave_id', $palabras_claves)
            ->get()
            ->pluck('influenciador_id');
    }

    /**
     * Obtiene el array de ids de los influenciadores que cumplen con el rango etario determinado
     *
     * @param int Rango etario determinado
     * @return array
     **/
    public static function getIdsInflueRelatedWithDistGenero($dist_genero)
    {
        /*
        SELECT influenciador_id FROM (
            SELECT * FROM (
                SELECT
                    influenciador_id,
                    hombres,
                    mujeres,
                    ROUND((hombres / (hombres + mujeres)) * 100) AS porcentaje_hombres,
                    ROUND((mujeres / (hombres + mujeres)) * 100)  AS porcentaje_mujeres,
                    ultima_revision
                FROM estadis_influen_followers_tw
                ORDER BY ultima_revision DESC
            ) AS distribucion_genero_tw
            GROUP BY influenciador_id
        ) AS dist_genero
        WHERE porcentaje_mujeres BETWEEN 45 AND 55
        */
        // El genero se selecciona en base a la distribución de mujeres
        $limites_genero = [
            0  => [100, 95], // 'Solo Mujeres',
            1  => [85 , 94], // '+40% Mujeres',
            2  => [75 , 84], // '+30% Mujeres',
            3  => [65 , 74], // '+20% Mujeres',
            4  => [55 , 64], // '+10% Mujeres',
            // 5  => [45 , 54], // 'Mujeres y Hombres', // Se ignora el filtro
            6  => [35 , 44], // '+10% Hombres',
            7  => [25 , 34], // '+20% Hombres',
            8  => [15 , 24], // '+30% Hombres',
            9  => [5  , 14], // '+40% Hombres',
            10 => [0  , 5], // 'Solo Hombres',
        ];

        $result = DB::select('
            SELECT influenciador_id FROM (
                SELECT * FROM (
                    SELECT
                        influenciador_id,
                        ROUND((mujeres / (hombres + mujeres)) * 100)  AS porcentaje_mujeres
                    FROM estadis_influen_followers_tw
                    ORDER BY ultima_revision DESC
                ) AS distribucion_genero_tw
                GROUP BY influenciador_id
            ) AS dist_genero
            WHERE porcentaje_mujeres BETWEEN '.$limites_genero[$dist_genero][0].' AND '.$limites_genero[$dist_genero][1].'
        ');

        if (empty($result)) {
            return [-1];
        } else {
            return collect($result)->pluck('influenciador_id');
        }
    }

    /**
     * Obtiene las lista de influenciador_x_palabra_clave que pertenecen a palabras clave del tipo palabra clave
     *
     * @return InfluenciadorXPalabraClave[]
     */
    public function palabrasClavePalabra($to_string = false)
    {
        $palabrasClave = $this->palabrasClaveInfluenciador([TipoPalabraClave::PALABRA_CLAVE]);

        if ($to_string) {
            $palabrasClave = $this->getTextMarcasRelacionadas($palabrasClave);
        }

        return $palabrasClave;
    }

    /**
     * Obtiene las lista de influenciador_x_palabra_clave que no pertenecen a palabras clave del
     * tipo palabra clave, por ejemplo marca y producto
     *
     * @param  array $tipoRelacionPalabraClave
     * @return InfluenciadorXPalabraClave[]
     */
    public function palabrasClaveOtros($tiposRelacionPalabraClave = [])
    {
        return $this->palabrasClaveInfluenciador(
            [TipoPalabraClave::MARCA, TipoPalabraClave::PRODUCTO],
            $tiposRelacionPalabraClave
        );
    }

    /**
     *
     * @param App\Models\InfluenciadorXPalabraClave[] $list_marcas
     *
     * @return string Devuelve la lista de palabras clave en formato texto separado por coma
     *
     */
    public function getTextMarcasRelacionadas($list_marcas)
    {
        $text_marcas = [];

        foreach ($list_marcas as $marca) {
            $text_marcas[] = $marca->palabraClave->palabra;
        }

        return implode(', ', $text_marcas);
    }

    /**
     * @return string Devuelve la lista de profesiones en formato texto separado por coma
     */
    public function getTextProfesiones()
    {
        $list_profesiones = $this->influenciadorXProfesion;
        $text_profesiones = [];

        foreach ($list_profesiones as $profesion) {
            $text_profesiones[] = $profesion->palabraClave->palabra;
        }

        return implode(', ', $text_profesiones);
    }

    /**
     *
     * @param boolean $to_string  Define si el resultado se devuelve como un array de instancias o como cadena de texto
     *                            false = Devuelve el resultado como array de instancias
     *                            true = Devuelve el resultado como cadena de texto separado por comas
     *
     * @return App\Models\InfluenciadorXPalabraClave[]|string $list_marcas
     */
    public function getMarcasMeGusta($to_string = false)
    {
        $marcas = $this->palabrasClaveOtros([TipoRelacionPalabraClave::$ME_GUSTA_LA_MARCA]);

        if ($to_string) {
            $marcas = $this->getTextMarcasRelacionadas($marcas);
        }

        return $marcas;
    }

    /**
     *
     * @param boolean $to_string  Define si el resultado se devuelve como un array de instancias o como cadena de texto
     *                            false = Devuelve el resultado como array de instancias
     *                            true = Devuelve el resultado como cadena de texto separado por comas
     *
     * @return App\Models\InfluenciadorXPalabraClave[]|string $list_marcas
     */
    public function getMarcasHeColaborado($to_string = false)
    {
        $marcas = $this->palabrasClaveOtros([TipoRelacionPalabraClave::$HE_COLABORADO_CON_LA_MARCA]);

        if ($to_string) {
            $marcas = $this->getTextMarcasRelacionadas($marcas);
        }

        return $marcas;
    }

    /**
     *
     * @param boolean $to_string  Define si el resultado se devuelve como un array de instancias o como cadena de texto
     *                            false = Devuelve el resultado como array de instancias
     *                            true = Devuelve el resultado como cadena de texto separado por comas
     *
     * @return App\Models\InfluenciadorXPalabraClave[]|string $list_marcas
     */
    public function getMarcasNoDeseoColaborado($to_string = false)
    {
        $marcas = $this->palabrasClaveOtros([TipoRelacionPalabraClave::$NO_DESEO_COLABORAR_CON_LA_MARCA]);

        if ($to_string) {
            $marcas = $this->getTextMarcasRelacionadas($marcas);
        }

        return $marcas;
    }

    /**
     *
     * @return App\Models\Publicacion[] $publicaciones
     */
    public function publicaciones()
    {
        return $this->hasMany('App\Models\Publicacion', 'influenciador_id', 'id');
    }

    /**
     *
     * @return App\Models\RangoEtario $rangoEtario
     */
    public function rangoEtario()
    {
        return $this->belongsTo('App\Models\RangoEtario', 'rango_etario_id', 'id');
    }

    /**
     *
     * @return App\Models\Pais $pais
     */
    public function pais()
    {
        return $this->belongsTo('App\Models\Pais', 'pais_id', 'id');
    }

    /**
     *
     * @return App\Models\TipoInfluenciador $tipoInfluenciador
     */
    public function tipoInfluenciador()
    {
        return $this->belongsTo('App\Models\TipoInfluenciador', 'tipo_influenciador_id', 'id');
    }

    /**
     * Busca los influenciadores por nombre, correo electrónico o usuario de las redes sociales
     *
     * @param  string  $text
     * @param  integer $page
     * @return array
     */
    public static function searchByText($text, $page = 0)
    {
        /*
        SELECT
            usuario.nombre,
            usuario.email,
            influenciador.usuario_id,
            influenciador.id AS influenciador_id,
            influenciador.nombre AS influenciador_nombre,
            influenciador.industria_id,
            influenciador.profesion,
            influenciador.rango_etario_id,
            influenciador.lugar_nacimiento,
            influenciador.pais_id,
            influenciador.genero,
            influenciador.tipo_influenciador_id,
            perfil_twitter.twitter_id,
            perfil_twitter.screen_name,
            perfil_twitter.profile_image_url,
            perfil_twitter.cantidad_seguidores
        FROM
            influenciador
        INNER JOIN usuario ON
            usuario.id = influenciador.usuario_id AND
            usuario.tipo_usuario_id = 5 AND
            usuario.activo = 1
        LEFT JOIN perfil_twitter ON
            perfil_twitter.influenciador_id = influenciador.id
        WHERE
            (
                usuario.nombre LIKE '%influe%' OR
                usuario.email LIKE  '%influe%' OR
                influenciador.nombre LIKE '%influe%' OR
                perfil_twitter.screen_name LIKE '%influe%'
            )
        LIMIT 10
         */
-
        /**
         * Mostrar
         * 12 resultados para influenciadores/catalogo
         * 10 resultados para campana/{id}/edit
         */
        $items_per_page = strpos(\Request::server('HTTP_REFERER'), 'influenciadores/catalogo')!== false ? 12 : 10;
        $text           = Helper::stripTags($text);
        $redes_sociales = RedSocial::where('activo', 1)->get();
        $columns        = [
                'usuario.email',
                'influenciador.id',
                'influenciador.nombre',
                'industria.nombre AS industria',
                // 'influenciador.profesion',
                'rango_etario.descripcion AS rango_etario',
                'influenciador.lugar_nacimiento',
                'pais.pais',
                'influenciador.genero',
                'tipo_influenciador.descripcion AS tipo_influenciador',
                // 'usuario.nombre AS nombre_usuario',
                // 'influenciador.usuario_id',
                // 'influenciador.industria_id',
                // 'influenciador.rango_etario_id',
                // 'influenciador.pais_id',
                // 'influenciador.tipo_influenciador_id'
            ];

        // Relaciona con todas las redes sociales activas
        // Los atributos screen_name, profile_image_url y cantidad_seguidores
        // les antepone el nombre de la red social en minusculas
        $cantFollowers = '(';
        $totalRedes    = $redes_sociales->count();
        $i             = 1;

        foreach ($redes_sociales as $red_social) {
            $columns[] = 'perfil_'.strtolower($red_social->nombre).'.screen_name AS '.
                strtolower($red_social->nombre).'_screen_name';
            $columns[] = 'perfil_'.strtolower($red_social->nombre).'.profile_image_url AS '.
                strtolower($red_social->nombre).'_profile_image_url';
            $columns[] = 'perfil_'.strtolower($red_social->nombre).'.cantidad_seguidores AS '.
                strtolower($red_social->nombre).'_cantidad_seguidores';
            $columns[] = 'perfil_'.strtolower($red_social->nombre).'.precio_publicacion AS '.
                strtolower($red_social->nombre).'_precio_publicacion';

            $cantFollowers .= 'IFNULL(perfil_'.strtolower($red_social->nombre).'.cantidad_seguidores, 0)';

            if ($i === $totalRedes) {
                $cantFollowers .= ') AS followers';
            } else {
                $cantFollowers .= ' + ';
            }

            $i++;
        }

        // adding a sum for total followers
        $columns[] = $cantFollowers;

        $columns = implode(',', $columns);

        $query = DB::table('influenciador')
            ->select(DB::raw($columns))
            ->join('tipo_influenciador', 'tipo_influenciador.id', '=', 'influenciador.tipo_influenciador_id')
            ->join('rango_etario', 'rango_etario.id', '=', 'influenciador.rango_etario_id')
            ->join('pais', 'pais.id', '=', 'influenciador.pais_id')
            ->join('industria', 'industria.id', '=', 'influenciador.industria_id')
            ->join('usuario', function ($join) {
                $join->on('usuario.id', '=', 'influenciador.usuario_id')
                    ->where([
                        ['usuario.tipo_usuario_id', TipoUsuario::CELEBRIDAD],
                        ['usuario.activo', 1],
                    ]);
                });

        // Relaciona con todas las redes sociales activas
        // implementa leftJoin
        foreach ($redes_sociales as $red_social) {
            $query->leftJoin('perfil_'.strtolower($red_social->nombre), function ($join) use ($red_social, $text) {
                $join->on('perfil_'.strtolower($red_social->nombre).'.influenciador_id', '=', 'influenciador.id');

                    /*$join->where(
                        'perfil_'.strtolower($red_social->nombre).'.screen_name',
                        'LIKE',
                        '%'.$text.'%'
                    );*/
            });
        }

        $entornos = null;

        // Si el usuario no es Administrador
        if (!Auth::user()->isAdministrador()) {
            // Detectamos el entorno
            $entornos = Auth::user()->getEntornos();
        }

        if (!Auth::user()->isAdministrador() && empty($entornos)) {
            throw new \Exception('El Usuario no esta asociado a un Entorno. Contacte con el Administrador de la Plataforma.', 422);
        }

        // Limita los resultados de las busquedas a los influenciadores que
        // pertenecen al entorno del usuario logueado
        if (!empty($entornos)) {
            $query->whereIn('influenciador.id', self::getIdsInflueRelatedByEntorno($entornos));
        }

        if (!empty($text)) {
            $query->where(function($query) use ($text, $redes_sociales) {
                $query->where('usuario.nombre', 'LIKE', '%'.$text.'%')
                    ->orWhere('usuario.email', 'LIKE', '%'.$text.'%')
                    ->orWhere('influenciador.nombre', 'LIKE', '%'.$text.'%');

                foreach ($redes_sociales as $red_social) {
                    $query->orWhere(
                        'perfil_'.strtolower($red_social->nombre).'.screen_name',
                        'LIKE',
                        '%'.$text.'%'
                    );
                }
            });
        }

        $query->groupBy('influenciador.id');
        $query->orderBy('followers', 'DESC');

            // ->offset(intval($page) * $items_per_page)
            // ->limit($items_per_page);
        // Log::info($query->toSql(), $query->getBindings());
        $result = $query->paginate($items_per_page);
        $total = count($result);

        // agrega los datos extras de 'qty_followers', 'engagement_ambos' y 'engagement_rate'
        for ($i=0; $i < $total; $i++) {
            $influencer = self::find($result[$i]->id);
            /*$extraData = self::loadJsonProfileStatistics($result[$i]->id);

            foreach ($extraData as $key => $value) {
                $result[$i]->{$key} = $value;
            }

            PerfilTwitter::where('influenciador_id', $result[$i]->id)
                ->update(['cantidad_seguidores' => $result[$i]->qty_followers]);

            $result[$i]->qty_followers = round($result[$i]->qty_followers/1000, 1);
            $result[$i]->engagement_ambos = round($result[$i]->engagement_ambos/1000, 1);*/
            $result[$i]->qty_followers     = $influencer->totalFollowers;
            $result[$i]->engagement_rate   = $influencer->engagement_rate;
            $result[$i]->engagement_ambos  = $result[$i]->engagement_rate;
            $result[$i]->alcance_potencial = $influencer->alcance_potencial;

            $aparicionBusqueda = new InfluenciadorAparicionBusqueda();
            $aparicionBusqueda->influenciador_id = $result[$i]->id;
            $aparicionBusqueda->save();
        }

        return $result;
    }

    /**
     * Obtiene el arreglo de todas las redes sociales relacionadas con el influenciador
     *
     * @return array
     */
    public function redesSociales()
    {
        $redes_sociales_influenciador = [];
        $redes_sociales = RedSocial::get();

        foreach ($redes_sociales as $red_social) {
            // El atributo se asigna por capitalize del nombre de la red social
            // iniciando por la palabra 'perfil'
            $perfil = $this->getAttribute('perfil'.ucfirst($red_social->nombre));

            if (!empty($perfil)) {
                $redes_sociales_influenciador[$red_social->id] = [
                    'red_social_id'     => $red_social->id,
                    'red_social_nombre' => $red_social->nombre,
                ] + $perfil->toArray();
            }
        }

        return $redes_sociales_influenciador;
    }

    /**
     * Guarda el usuario de tipo Influenciador
     *
     * @param  array $datos
     * @param  boolean $update
     * @return App\Models\User $usuario
     */
    public function saveUsuario($datos, $update = false)
    {
        $usuario = !empty($this->usuario_id) ? $this->usuario : new User();

        $usuario->tipo_usuario_id = TipoUsuario::CELEBRIDAD;
        $usuario->nombre          = $datos['nombre'];
        $usuario->email           = $datos['email'];

        if (isset($datos['activo'])) {
            $usuario->activo = $datos['activo'];
        }

        if (isset($datos['password'])) {
            $usuario->password = $datos['password'];
        }

        if (!$usuario->isValid($update ? User::DO_UPDATE : User::DO_CREATE)) {
            throw new Exception('Error al procesar los datos del usuario. ' .
                Helper::errorsToString($usuario->errors), 422);
        };

        $usuario->save();

        return $usuario;
    }

    public function clearPalabrasClave()
    {
        // Se eliminan todas las palabras relacionadas con el influenciador
        // posteriormente se insertan
        InfluenciadorXPalabraClave::where('influenciador_id', $this->id)->delete();
    }

    /**
     * Guarda la relacion influenciador_x_palabra_clave de las palabras clave del tipo palabra clave
     *
     * @param  array $palabras_clave
     * @return null
     */
    public function savePalabrasClave($palabras_clave)
    {
        $total_palabras = count($palabras_clave);

        for ($i = 0; $i < $total_palabras; $i++) {
            // Evita insertar dos veces la misma palabra clave con diferente relación
            $infPalaCla = InfluenciadorXPalabraClave::where([
                    'influenciador_id' => $this->id,
                    'palabra_clave_id' => $palabras_clave[$i]
                ])->first();

            if (empty($infPalaCla)) {
                $infPalaCla = new InfluenciadorXPalabraClave();
            }

            $infPalaCla->influenciador_id               = $this->id;
            $infPalaCla->palabra_clave_id               = $palabras_clave[$i];
            $infPalaCla->tipo_relacion_palabra_clave_id = TipoRelacionPalabraClave::$ME_INTERESA_PALABRA_CLAVE;

            if (!$infPalaCla->isValid(InfluenciadorXPalabraClave::DO_CREATE)) {
                throw new Exception('Error al procesar los datos de las palabras claves. ' .
                    Helper::errorsToString($infPalaCla->errors), 422);
            };

            $infPalaCla->save();
        }
    }

    /**
     * Guarda la relacion influenciador_x_palabra_clave de las palabras clave que no pertenecen al tipo
     * palabras clave, por ejemplo marca y producto
     *
     * @param  array $palabras_clave
     * @return null
     */
    public function savePalabrasClaveOtros($palabras_clave)
    {
        $total_palabras = count($palabras_clave);

        for ($i = 0; $i < $total_palabras; $i++) {
            // Evita insertar dos veces la misma palabra clave con diferente relación
            $infPalaCla = InfluenciadorXPalabraClave::where([
                    'influenciador_id' => $this->id,
                    'palabra_clave_id' => $palabras_clave[$i]['palabra_id']
                ])->first();

            if (empty($infPalaCla)) {
                $infPalaCla = new InfluenciadorXPalabraClave();
            }

            $infPalaCla->influenciador_id               = $this->id;
            $infPalaCla->palabra_clave_id               = $palabras_clave[$i]['palabra_id'];
            $infPalaCla->tipo_relacion_palabra_clave_id = $palabras_clave[$i]['relacion_id'];

            if (!$infPalaCla->isValid(InfluenciadorXPalabraClave::DO_CREATE)) {
                throw new Exception('Error al procesar los datos de las palabras claves. ' .
                    Helper::errorsToString($infPalaCla->errors), 422);
            };

            $infPalaCla->save();
        }
    }

    /**
     * Guarda las redes sociales asociadas con el influenciador
     *
     * @param  array $redes_sociales
     * @param  boolean $update
     * @param  array $redes_sociales_to_delete
     */
    public function saveRedesSociales($redes_sociales_influenciador, $update = false, $redes_sociales_to_delete = [])
    {
        $redes_sociales = RedSocial::get()->pluck('nombre', 'id');
        $page = null;

        // Eliminamos las redes sociales que se eliminaron de la vista
        if (!empty($redes_sociales_to_delete)) {
            foreach ($redes_sociales_to_delete as $delete_id) {
                $red_social = RedSocial::where('id', $delete_id)->first();

                switch (ucfirst($red_social->nombre)) {
                    case 'Twitter':
                        $perfil = $this->perfilTwitter;
                        break;

                    case 'Facebook':
                        $perfil = $this->perfilFacebook;
                        break;
                }
                $perfil->delete();
            }
        }

        // Guardamos o actualizamos las nuevas redes sociales
        foreach ($redes_sociales_influenciador as $red_social) {
            // Primero validamos que no esten vacio los campos screen_name y cantidad_seguidores
            if (empty($red_social['perfil']['screen_name']) && empty($red_social['perfil']['cantidad_seguidores']) && empty($red_social['perfil']['precio_publicacion'])) {
                continue;
            }

            $redSocialClass = 'App\Models\Perfil'.ucfirst($redes_sociales[$red_social['id']]);

            // Busca si el influenciador tiene asociado una cuenta en la red social
            $perfil = call_user_func_array([$redSocialClass, 'find'], [$this->id]);

            // Si no es asi, crea una nueva cuenta
            if (empty($perfil)) {
                $perfil = new $redSocialClass;
            }

            if ($redes_sociales[$red_social['id']] === 'Facebook') {
                if ($perfil->selectedPage == 0) {
                    $page = FacebookPage::find($red_social['pageId']);

                    // Se actualiza el objeto que cargo en memoria
                    if (!empty($this->perfilFacebook) && !empty($page)) {
                        $this->perfilFacebook->page_id           = $page->id;
                        $this->perfilFacebook->screen_name       = $page->name;
                        $this->perfilFacebook->page_access_token = $page->access_token;
                    }
                }
            }

            $perfil->fill([
                'influenciador_id' => $this->id,
            ]);

            // Agrega como screen_name el nombre que se envia o el nombre de la fan page por defecto
            if (isset($red_social['perfil']['screen_name'])) {
                if (!empty($red_social['perfil']['screen_name'])) {
                    $perfil->screen_name = $red_social['perfil']['screen_name'];
                } else if (!empty($page)) {
                    $perfil->screen_name = $page->name;
                }
            } else if (!empty($page)) {
                $perfil->screen_name = $page->name;
            }

            if (isset($red_social['perfil']['cantidad_seguidores'])) {
                $perfil->cantidad_seguidores = $red_social['perfil']['cantidad_seguidores'];
            }

            // precio publicación en caso de que exista
            if (isset($red_social['perfil']['precio_publicacion'])) {
                $perfil->precio_publicacion = $red_social['perfil']['precio_publicacion'];
            }

            if (!$perfil->isValid($update ? $redSocialClass::DO_UPDATE : $redSocialClass::DO_CREATE)) {
                throw new Exception('Error al procesar los datos de las redes sociales. ' .
                    Helper::errorsToString($perfil->errors), 422);
            };

            // if it is facebook and a pageid was sent, move data to profile and deactivate the pages
            if ($redes_sociales[$red_social['id']] === 'Facebook') {
                if ($perfil->selectedPage == 0) {
                    $page = FacebookPage::find($red_social['pageId']);

                    if (!empty($page)) {
                        $perfil->updateProfileFromPage($page->toArray());
                    }
                }
            }

            $perfil->save();
        }

        return $redes_sociales_influenciador;
    }

    /**
     * save factura
     *
     * @param array $datos the data to be saved
     * @param boolean $update the flag for insert or update
     * @return void
     * @throws Exception when the data is invalid
     */
    public function saveFactura($datos, $update = false)
    {
        // validar request de factura
        $validArray = array_filter($datos);

        if (count($validArray) === 0) {
            // todos los indexes son invalidos y no se devuelve array válido
            return;
        }

        $factura = !$update ? new InfluenciadorFacturacion : $this->factura()->find($this->id);
        $tipoPersona = null;

        // Cuando todavia no tiene asociado alguna Factura
        // se omite el procesamiento de los datos
        if (empty($factura)) {
            $factura = new InfluenciadorFacturacion;
        }

        if (array_key_exists('tipoPersona', $datos)) {
            $tipoPersona = (int) $datos['tipoPersona'] === 1 ? 0 : 1;
        }

        $factura->fill([
            'influenciador_id' => $this->id,
            'razon_social'     => $datos['razonSocial'],
            'rfc'              => $datos['rfc'],
            'dia_facturacion'  => 5,
            'banco'            => $datos['banco'],
            'cuenta'           => $datos['cuenta'],
            'calle'            => $datos['calle'],
            'no_exterior'      => $datos['numExterior'],
            'no_interior'      => $datos['numInterior'],
            'colonia'          => $datos['colonia'],
            'codigo_postal'    => $datos['cp'],
            'localidad'        => $datos['localidad'],
            'municipio'        => $datos['municipio'],
            'estado'           => $datos['estado'],
            'pais_id'          => array_key_exists('pais_id_fact', $datos) ? $datos['pais_id_fact'] : null,
            'es_persona_moral' => $tipoPersona
        ]);

        if (!$factura->isValid($update ? InfluenciadorFacturacion::DO_UPDATE : InfluenciadorFacturacion::DO_CREATE)) {
            throw new Exception('Error al procesar los datos de facturación. ' . Helper::errorsToString($factura->errors), 422);
        };

        $factura->save();
    }

    /**
     * Guarda la relacion influenciador_x_profesion
     *
     * @param  array $profesiones
     * @return null
     */
    public function saveProfesiones($profesiones)
    {
        $total_profesiones = count($profesiones);

        // Elimina las profesiones asociadas al influencer para después crearlas
        InfluenciadorXProfesion::where('influenciador_id', $this->id)->delete();

        for ($i = 0; $i < $total_profesiones; $i++) {
            $profesion = new InfluenciadorXProfesion();
            $profesion->influenciador_id = $this->id;
            $profesion->palabra_clave_id = $profesiones[$i];

            if (!$profesion->isValid(InfluenciadorXProfesion::DO_CREATE)) {
                throw new Exception('Error al procesar los datos de las profesiones. ' .
                    Helper::errorsToString($profesion->errors), 422);
            };

            $profesion->save();
        }
    }

    /**
     * Busca los influenciadores por nombre, correo electrónico o usuario de las redes sociales
     *
     * @param  string  $text
     * @param  integer $page
     * @return array
     */
    public static function searchLugarNacimientoByText($text, $page = 0)
    {
        /*SELECT
            DISTINCT(lugar_nacimiento) AS lugar_nacimiento
        FROM influenciador
        WHERE lugar_nacimiento LIKE '%illi%'*/
        $items_per_page = 10;

        return DB::table('influenciador')
            ->select(DB::raw('DISTINCT(lugar_nacimiento) AS lugar_nacimiento'))
            ->where('lugar_nacimiento', 'LIKE', '%'.$text.'%')
            ->offset(intval($page) * $items_per_page)
            ->limit($items_per_page)
            ->get();
    }

    /**
     * Busca avanzada de influenciadores, parámetros de busqueda:
     *     Nombre: string
     *     Cantidad de Seguidores: integer array [min, max]
     *     Rangos Etarios: integer array
     *     Tipos de Influenciador: integer array
     *     Géneros: integer array
     *     lugar de nacimiento: string
     *     Palabras clave: integer array
     *
     * @param  array  $params
     * @param  integer $page
     * @return array
     */
    public static function advancedSearch($params, $page = 0)
    {
        /*
        SELECT
            usuario.nombre,
            usuario.email,
            influenciador.usuario_id,
            influenciador.id AS influenciador_id,
            influenciador.nombre AS influenciador_nombre,
            influenciador.industria_id,
            influenciador.profesion,
            influenciador.rango_etario_id,
            influenciador.lugar_nacimiento,
            influenciador.pais_id,
            influenciador.genero,
            influenciador.tipo_influenciador_id,
            perfil_twitter.twitter_id,
            perfil_twitter.screen_name,
            perfil_twitter.profile_image_url,
            cantidad_seguidores
        FROM
            influenciador
        INNER JOIN usuario ON
            usuario.id = influenciador.usuario_id AND
            usuario.tipo_usuario_id = 5 AND
            usuario.activo = 1
        LEFT JOIN perfil_twitter ON
            perfil_twitter.influenciador_id = influenciador.id
        INNER JOIN influenciador_x_palabra_clave ON
            influenciador_x_palabra_clave.influenciador_id = influenciador.id AND
            influenciador_x_palabra_clave.tipo_relacion_palabra_clave_id != 3 AND
            influenciador_x_palabra_clave.palabra_clave_id IN (17, 44, 45)
        WHERE
            (
                usuario.nombre LIKE '%influe%' OR
                usuario.email LIKE  '%influe%' OR
                influenciador.nombre LIKE '%influe%' OR
                perfil_twitter.screen_name LIKE '%influe%'
            )
        GROUP BY influenciador_id
        LIMIT 10
         */
        /**
         * Mostrar
         * 12 resultados para influenciadores/catalogo
         * 10 resultados para campana/{id}/edit
         */
        $items_per_page = strpos(\Request::server('HTTP_REFERER'), 'influenciadores/catalogo')!== false ? 12 : 10;
        $redes_sociales = RedSocial::where('activo', 1)->get();
        $params         = Helper::stripTagsArray($params);
        $columns        = [
                'usuario.email',
                'influenciador.id',
                'influenciador.nombre',
                // 'industria.nombre AS industria',
                // 'influenciador.profesion', // Profesión ahora es una relación de uno a muchos
                // 'rango_etario.descripcion AS rango_etario',
                // 'pais.pais',
                'influenciador.genero',
                'tipo_influenciador.descripcion AS tipo_influenciador',
                // 'influenciador.lugar_nacimiento',
                // 'usuario.nombre AS nombre_usuario',
                // 'influenciador.usuario_id',
                // 'influenciador.industria_id',
                // 'influenciador.rango_etario_id',
                // 'influenciador.pais_id',
                // 'influenciador.tipo_influenciador_id',
            ];

        $cantFollowers = '(';
        $totalRedes    = $redes_sociales->count();
        $i             = 1;

        // Relaciona con todas las redes sociales activas
        // Los atributos screen_name, profile_image_url y cantidad_seguidores
        // les antepone el nombre de la red social en minusculas
        foreach ($redes_sociales as $red_social) {
            $columns[] = 'perfil_'.strtolower($red_social->nombre).'.screen_name AS '.
                strtolower($red_social->nombre).'_screen_name';
            $columns[] = 'perfil_'.strtolower($red_social->nombre).'.profile_image_url AS '.
                strtolower($red_social->nombre).'_profile_image_url';
            $columns[] = 'perfil_'.strtolower($red_social->nombre).'.cantidad_seguidores AS '.
                strtolower($red_social->nombre).'_cantidad_seguidores';
            $columns[] = 'perfil_'.strtolower($red_social->nombre).'.precio_publicacion AS '.
                strtolower($red_social->nombre).'_precio_publicacion';

            $cantFollowers .= 'IFNULL(perfil_'.strtolower($red_social->nombre).'.cantidad_seguidores, 0)';

            if ($i === $totalRedes) {
                $cantFollowers .= ') AS followers';
            } else {
                $cantFollowers .= ' + ';
            }

            $i++;
        }

        // adding a sum for total followers
        $columns[] = $cantFollowers;

        $columns = implode(',', $columns);

        $query = DB::table('influenciador')
            ->select(DB::raw($columns))
            ->join('tipo_influenciador', function ($join) use ($params) {
                $join->on('tipo_influenciador.id', '=', 'influenciador.tipo_influenciador_id');

                if (!empty($params['tipo_influenciador_id'])) {
                    $join->whereIn('tipo_influenciador.id', $params['tipo_influenciador_id']);
                }
            })
            ->join('usuario', function ($join) use ($params) {
                $join->on('usuario.id', '=', 'influenciador.usuario_id')
                    ->where([
                        ['usuario.tipo_usuario_id', TipoUsuario::CELEBRIDAD],
                        ['usuario.activo', 1],
                    ]);
            });

        // Relaciona con todas las redes sociales activas
        // implementa leftJoin
        foreach ($redes_sociales as $red_social) {
            $query->leftJoin('perfil_'.strtolower($red_social->nombre), function ($join) use ($red_social) {
                $join->on('perfil_'.strtolower($red_social->nombre).'.influenciador_id', '=', 'influenciador.id');

                // Se omite el where, posteriormente se agrega para cada red social
                /*if (!empty($params['cantidad_seguidores'])) {
                    $join->whereBetween(
                        'perfil_'.strtolower($red_social->nombre).'cantidad_seguidores',
                        $params['cantidad_seguidores']
                    );
                }*/
            });
        }

        /*if (!empty($params['cantidad_seguidores'])) {
            $query->join('perfil_twitter', function ($join) use ($red_social, $params) {
                $join->on('perfil_twitter.influenciador_id', '=', 'influenciador.id');

                $join->whereBetween(
                    'perfil_twitter.cantidad_seguidores',
                    $params['cantidad_seguidores']
                );
            });
        }*/
        // Para el soporte soporte de multiples redes sociales, se debe revisar esta parte
        // para agregar mas tablas de las redes sociales
        if (!empty($params['cantidad_seguidores'])) {
            $query->whereBetween(
                'perfil_twitter.cantidad_seguidores',
                $params['cantidad_seguidores']
            );
        }

        /*if (!empty($params['lugar_nacimiento'])) {
            $query->where('influenciador.lugar_nacimiento', $params['lugar_nacimiento']);
        }*/

        if (!empty($params['rango_etario_id'])) {
            $query->join('rango_etario', function ($join) use ($params) {
                $join->on('rango_etario.id', '=', 'influenciador.rango_etario_id');
                $join->whereIn('rango_etario.id', $params['rango_etario_id']);
            });
        }

        if (!empty($params['industria_id'])) {
            $query->join('industria', function ($join) use ($params) {
                $join->on('industria.id', '=', 'influenciador.industria_id');
                $join->whereIn('industria.id', $params['industria_id']);
            });
        }

        if (!empty($params['pais_id'])) {
            $query->join('pais', function ($join) use ($params) {
                $join->on('pais.id', '=', 'influenciador.pais_id');
                $join->where('pais.id', $params['pais_id']);
            });
        }

        if (!empty($params['genero'])) {
            $query->whereIn('influenciador.genero', $params['genero']);
        }

        if (isset($params['palabra_clave_id'])) {
            if (!empty($params['palabra_clave_id'])) {
                $query->whereIn('influenciador.id', static::getIdsInflueRelatedWithPalabrasClaves($params['palabra_clave_id']));
            }
        }

        if (isset($params['distribucion_genero'])) {
            if (!empty($params['distribucion_genero'])) {
                // 5  => 'Mujeres y Hombres'
                // Se ignora el filtro
                if ($params['distribucion_genero'][0] != 5) {
                    $query->whereIn('influenciador.id', static::getIdsInflueRelatedWithDistGenero($params['distribucion_genero'][0]));
                }
            }
        }

        if (!empty($params['manager_nombre'])) {
            $query->where('influenciador.manager_nombre', 'LIKE', '%'.$params['manager_nombre'].'%');
        }

        if (!empty($params['nombre'])) {
            $query->where(function($query) use ($params, $redes_sociales) {
                $query->where('influenciador.nombre', 'LIKE', '%'.$params['nombre'].'%')
                    ->orWhere('usuario.email', 'LIKE', '%'.$params['nombre'].'%')
                    ->orWhere('usuario.nombre', 'LIKE', '%'.$params['nombre'].'%');

                foreach ($redes_sociales as $red_social) {
                    $query->orWhere(
                        'perfil_'.strtolower($red_social->nombre).'.screen_name',
                        'LIKE',
                        '%'.$params['nombre'].'%'
                    );
                }
            });
        }

        $entornos = null;

        // Si el usuario no es Administrador
        if (!Auth::user()->isAdministrador()) {
            // Detectamos el entorno
            $entornos = Auth::user()->getEntornos();
        }

        if (!Auth::user()->isAdministrador() && empty($entornos)) {
            throw new \Exception('El Usuario no esta asociado a un Entorno.  Contacte con el Administrador de la Plataforma.', 422);
        }

        // Limita los resultados de las busquedas a los influenciadores que
        // pertenecen al entorno del usuario logueado
        if (!empty($entornos)) {
            $query->whereIn('influenciador.id', self::getIdsInflueRelatedByEntorno($entornos));
        }

        $query->groupBy('influenciador.id');
        $query->orderBy('followers', 'DESC');
            // ->offset(intval($page) * $items_per_page)
            // ->limit($items_per_page);

        /*return [
            'sql' => $query->toSql(),
            'binds' => $query->getBindings(),
            'params' => $query->getRawBindings(),
        ];*/
        // dd($query->toSql());
        // Log::info($query->toSql(), $query->getBindings());
        $result = $query->paginate($items_per_page);
        $total = count($result);

        // agrega los datos extras de 'qty_followers', 'engagement_ambos' y 'engagement_rate'
        for ($i=0; $i < $total; $i++) {
            $influencer = self::find($result[$i]->id);
            /*$extraData = self::loadJsonProfileStatistics($result[$i]->id);

            foreach ($extraData as $key => $value) {
                $result[$i]->{$key} = $value;
            }

            PerfilTwitter::where('influenciador_id', $result[$i]->id)
                ->update(['cantidad_seguidores' => $result[$i]->qty_followers]);

            $result[$i]->qty_followers = round($result[$i]->qty_followers/1000, 1);
            $result[$i]->engagement_ambos = round($result[$i]->engagement_ambos/1000, 1);*/
            $result[$i]->qty_followers     = $influencer->totalFollowers;
            $result[$i]->engagement_rate   = $influencer->getEngagement();
            $result[$i]->engagement_ambos  = $result[$i]->engagement_rate;
            $result[$i]->alcance_potencial = $influencer->alcance_potencial;

            $aparicionBusqueda = new InfluenciadorAparicionBusqueda();
            $aparicionBusqueda->influenciador_id = $result[$i]->id;
            $aparicionBusqueda->save();
        }

        return $result;
    }

    /**
     *
     * @return App\Models\Publicacion[] $publicacion
     */
    public function publicacionesCampana($campana_id, $red_social_id = null)
    {
        $query = Publicacion::where([
                ['campana_id', $campana_id],
                ['influenciador_id', $this->id],
            ]);

        if (!empty($red_social_id)) {
            $query->where('red_social_id', $red_social_id);
        }

        return $query->get();
    }

    /**
     *
     * @return App\Models\Publicacion[] $publicacion
     */
    public function publicacionesCampanaTwitter($campana_id)
    {
        return $this->publicacionesCampana($campana_id, RedSocial::$TWITTER);
    }

    /**
     * Devuelve un arreglo si $campana_id = null
     * Devuelve un objeto si $campana_id = entero
     *
     * @param int $campana_id ID de la campaña, default null
     * @param int|array $estado Status de la campaña, default null
     *
     * @return App\Models\InfluenciadorXCampana $influenciadorXCampana
     */
    public function influenciadorXCampana($campana_id = null, $estado = null)
    {
        $query = InfluenciadorXCampana::where([
                'influenciador_id' => $this->id,
            ])
            ->join('campana', 'campana.id', '=', 'influenciador_x_campana.campana_id')
            ->orderBy('fecha_desde', 'desc')
            ->orderBy('fecha_hasta', 'desc')
            ->with('campana');

        if (!empty($campana_id)) {
            $query->where('campana_id', $campana_id);
        }

        if (!empty($estado)) {
            if (is_array($estado)) {
                $query->whereIn('estado', $estado);
            } else {
                $query->where('estado', $estado);
            }
        }

        // Si queremos obtener los datos de una campaña en especifico
        // Se devuelve el objeto
        if (!empty($campana_id)) {
            return $query->first();
        }

        // De lo contrario se devuelve un array de objetos
        return $query->get();
    }

    /**
     *
     * @return App\Models\PublicacionXInfluenciadorXCampana $publicacionxinfluenciador
     */
    public function pubXInfluXCamp($campana_id = null, $red_social_id = null, $estado = null)
    {
        $query = PublicacionXInfluenciadorXCampana::where([
                'influenciador_id' => $this->id,
            ])->with('campana');

        if (!empty($campana_id)) {
            $query->where('campana_id', $campana_id);
        }

        if (!empty($red_social_id)) {
            $query->where('red_social_id', $red_social_id);
        }

        if (!empty($estado)) {
            if (is_array($estado)) {
                $query->whereIn('estado', $estado);
            } else {
                $query->where('estado', $estado);
            }
        }

        return $query->get();
    }

    /**
     *
     * @return App\Models\PublicacionXInfluenciadorXCampana $publicacionxinfluenciador
     */
    public function pubXInfluXCampEnEsperaYAceptados($campana_id = null, $red_social_id = null)
    {
        return $this->pubXInfluXCamp(
            $campana_id,
            $red_social_id,
            [PublicacionXInfluenciadorXCampana::$ESPERANDO_RESPUESTA, PublicacionXInfluenciadorXCampana::$ACEPTADO]
        );
    }

    /**
     *
     * @return App\Models\InfluenciadorXCampana[] $influenciadorXCampana
     */
    public function campanasEnEsperaYAceptadas($campana_id = null)
    {
        return $this->influenciadorXCampana(
            $campana_id,
            [InfluenciadorXCampana::$ESPERANDO_RESPUESTA, InfluenciadorXCampana::$ACEPTADO]
        );
    }

    /**
     *
     * @return App\Models\PublicacionXInfluenciadorXCampana $pubXInfluXCampTwitter
     */
    public function pubXInfluXCampTwitter($campana_id)
    {
        $result = $this->pubXInfluXCamp($campana_id, RedSocial::$TWITTER);

        return $result->first();
    }

    /**
     *
     * @return App\Models\PublicacionXInfluenciadorXCampana $pubXInfluXCampFacebook
     */
    public function pubXInfluXCampFacebook($campana_id)
    {
        $result = $this->pubXInfluXCamp($campana_id, RedSocial::$FACEBOOK);

        return $result->first();
    }

    public function aprobarPublicacionesCampana($campana_id)
    {
        Publicacion::where([
            ['campana_id', $campana_id],
            ['influenciador_id', $this->id],
        ])->update([
            'estado' => Publicacion::$APROBADO_INFLUENCER
        ]);
    }

    public function setJsonProfileStatistics()
    {
        $extraData = self::loadJsonProfileStatistics($this->id);

        $this->qty_followers    = $extraData['qty_followers'];
        $this->engagement_ambos = $extraData['engagement_ambos'];
        $this->engagement_rate  = $extraData['engagement_rate'];
    }

    /**
     *
     * @return mixed
     */
    public static function loadJsonProfileStatistics($id)
    {
        $fileJson = self::$folderJsons . 'ProfileStatistics.'.$id;
        $data = [
            'qty_followers'    => 0,
            'engagement_ambos' => 0,
            'engagement_rate'  => 0,
        ];

        if (Storage::disk('local')->exists($fileJson)) {
            $dataJson = json_decode(Storage::disk('local')->get($fileJson));

            if (!empty($dataJson)) {
                if (!empty($dataJson->ProfileStatistics) && !empty($dataJson->AlcanceEngagement)) {
                    $data['qty_followers']    = $dataJson->ProfileStatistics[0]->QtyFollowers;
                    $data['engagement_ambos'] = $dataJson->AlcanceEngagement[count($dataJson->AlcanceEngagement)-1]->EngagementAmbos;

                    if (!empty($data['engagement_ambos'])) {
                        $data['engagement_rate'] = round($data['qty_followers'] / $data['engagement_ambos'] * 100, 1);
                    }
                }
            }
        } /*else {
            $dataJson = json_decode(Storage::disk('local')->get(self::$folderJsons . 'ProfileStatistics.0'));
        }*/

        return $data;
    }

    /**
     * Sincroniza los archivos JSON con los servicios del backend
     *
     * @return void
     **/
    public static function syncperfiles()
    {
        ini_set('max_execution_time', 3600);
        // set_time_limit(3600);
        $now = Carbon::now();
        $datefrom = Carbon::createFromFormat('d-m-Y', Storage::disk('local')->get('json/fechasincronizacion.json'));

        if ($datefrom === false) {
            $datefrom = $now->startOfWeek();
        }

        $query = User::select('usuario.*')->where([
            'tipo_usuario_id' => TipoUsuario::CELEBRIDAD,
            'activo'          => 1,
        ])
        ->join('influenciador', function ($joinInfluenciador) {
            $joinInfluenciador->on('usuario.id', '=', 'influenciador.usuario_id');
        })
        ->join('perfil_twitter', function ($joinPerfil) use ($now) {
            $joinPerfil->on('perfil_twitter.influenciador_id', '=', 'influenciador.id')
                ->whereRaw('(perfil_twitter.fecha_sincronizacion <= "' . $now->format('Y-m-d H:00:00') .'" OR '.
                           'perfil_twitter.fecha_sincronizacion IS NULL)');
        });
        // ->has('influenciador.perfilTwitter')
        // ->with('influenciador.perfilTwitter');

        $usuariosInfluenciadores = $query->get();

        $usuariosInfluenciadores->load('influenciador.perfilTwitter');

        $serviceBack            = new ServiciosBackend();
        $serviceBack->datefrom  = $datefrom->format('m-d-Y'); // mm-dd-yyyy
        $serviceBack->dateto    = $now->format('m-d-Y'); // mm-dd-yyyy

        foreach ($usuariosInfluenciadores as $usrInflue) {
            $QtyFollowers = null;
            $profileImageUrl = null;

            // Si el atributo influenciador esta vacio continuamos con el siguiente elemento
            if (empty($usrInflue->influenciador)) {
                continue;
            }

            if (empty($usrInflue->influenciador->perfilTwitter)) {
                continue;
            }

            $serviceBack->screenname = str_replace('@', '', $usrInflue->influenciador->perfilTwitter->screen_name);

            // ProfileHistoricalStatistics
            $json = json_encode($serviceBack->profileHistoricalStatistics());

            if (!empty($json)) {
                Storage::disk('local')->put(
                    'json/ProfileHistoricalStatistics.'.$usrInflue->influenciador->id,
                    $json
                );
            }

            // ProfileStatistics
            $json = json_encode($serviceBack->profileStatistics());

            if (!empty($json)) {
                Storage::disk('local')->put(
                    'json/ProfileStatistics.'.$usrInflue->influenciador->id,
                    $json
                );

                $json = json_decode($json);

                if (count($json->ProfileStatistics)) {
                    $QtyFollowers = $json->ProfileStatistics[0]->QtyFollowers;
                    $profileImageUrl = $json->ProfileStatistics[0]->ProfileImage;
                }
            }

            // TopFollowers
            $json = json_encode($serviceBack->topFollowers());

            if (!empty($json)) {
                Storage::disk('local')->put(
                    'json/TopFollowers.'.$usrInflue->influenciador->id,
                    $json
                );
            }

            // TopHashtags
            $json = json_encode($serviceBack->topHashtags());

            if (!empty($json)) {
                Storage::disk('local')->put(
                    'json/TopHashtags.'.$usrInflue->influenciador->id,
                    $json
                );
            }

            // TopWordCloudCom
            $json = json_encode($serviceBack->TopWordCloudCom());

            if (!empty($json)) {
                Storage::disk('local')->put(
                    'json/TopWordCloudCom.'.$usrInflue->influenciador->id,
                    $json
                );
            }

            PerfilTwitter::where('influenciador_id', $usrInflue->influenciador->id)
                ->update([
                    'profile_image_url'    => $profileImageUrl,
                    'cantidad_seguidores'  => $QtyFollowers,
                    'json_sincronizado'    => 1,
                    'fecha_sincronizacion' => date('Y-m-d H:i:s'),
                ]);

            /*$usrInflue->influenciador->perfilTwitter->cantidad_seguidores = $json->ProfileStatistics[0]->QtyFollowers;

            $usrInflue->influenciador->perfilTwitter->json_sincronizado = 1;
            $usrInflue->influenciador->perfilTwitter->fecha_sincronizacion = date('Y-m-d H:i:s');
            $usrInflue->influenciador->perfilTwitter->save();*/
        }

        // Actualiza la fecha de sincronizacion
        Storage::disk('local')->put(
            'json/fechasincronizacion.json',
            $now->format('d-m-Y')
        );
    }

    /**
     * Sincroniza los archivos JSON con los servicios del backend
     *
     * @return void
     **/
    public function syncperfil()
    {
        ini_set('max_execution_time', 3600);
        // set_time_limit(3600);
        $now = Carbon::now();
        $datefrom = Carbon::createFromFormat('d-m-Y', Storage::disk('local')->get('json/fechasincronizacion.json'));

        if ($datefrom === false) {
            $datefrom = $now->startOfWeek();
        }

        $serviceBack            = new ServiciosBackend();
        $serviceBack->datefrom  = $datefrom->format('m-d-Y'); // mm-dd-yyyy
        $serviceBack->dateto    = $now->format('m-d-Y'); // mm-dd-yyyy
        $QtyFollowers = null;
        $profileImageUrl = null;

        if (empty($this->perfilTwitter)) {
            return false;
        }

        $serviceBack->screenname = str_replace('@', '', $this->perfilTwitter->screen_name);

        // ProfileHistoricalStatistics
        $json = json_encode($serviceBack->profileHistoricalStatistics());

        if (!empty($json)) {
            Storage::disk('local')->put(
                'json/ProfileHistoricalStatistics.'.$this->id,
                $json
            );
        }

        // ProfileStatistics
        $json = json_encode($serviceBack->profileStatistics());

        if (!empty($json)) {
            Storage::disk('local')->put(
                'json/ProfileStatistics.'.$this->id,
                $json
            );

            $json = json_decode($json);

            if (count($json->ProfileStatistics)) {
                $QtyFollowers = $json->ProfileStatistics[0]->QtyFollowers;
                $profileImageUrl = $json->ProfileStatistics[0]->ProfileImage;
            }
        }

        // TopFollowers
        $json = json_encode($serviceBack->topFollowers());

        if (!empty($json)) {
            Storage::disk('local')->put(
                'json/TopFollowers.'.$this->id,
                $json
            );
        }

        // TopHashtags
        $json = json_encode($serviceBack->topHashtags());

        if (!empty($json)) {
            Storage::disk('local')->put(
                'json/TopHashtags.'.$this->id,
                $json
            );
        }

        // TopWordCloudCom
        $json = json_encode($serviceBack->TopWordCloudCom());

        if (!empty($json)) {
            Storage::disk('local')->put(
                'json/TopWordCloudCom.'.$this->id,
                $json
            );
        }

        /*PerfilTwitter::where('influenciador_id', $this->id)
            ->update([
                'profile_image_url'    => $profileImageUrl,
                'cantidad_seguidores'  => $QtyFollowers,
                'json_sincronizado'    => 1,
                'fecha_sincronizacion' => date('Y-m-d H:i:s'),
            ]);*/

        $this->perfilTwitter->cantidad_seguidores = $QtyFollowers;
        // $this->perfilTwitter->profile_image_url = $profileImageUrl;
        $this->perfilTwitter->json_sincronizado = 1;
        $this->perfilTwitter->fecha_sincronizacion = date('Y-m-d H:i:s');
        $this->perfilTwitter->save();

        Storage::disk('local')->put(
            'json/fechasincronizacion.json',
            $now->format('d-m-Y')
        );
    }

    /**
     * Obtiene los datos que se enviaran al Backend
     * id, pais_id, activo, name, gender, country, dia_semana, ultima_sincronizacion, oauth_token, oauth_token_secret,
     * para el caso de Twitter también se incluye screen_name
     *
     * @param string $red_social twitter, facebook
     *
     * @return array
     */
    public function getDataToBackEnd($red_social)
    {
        $name = explode(' ', $this->usuario->nombre);
        $dia_semana = static::getBestDiaSemanaToProcess();
        $days = [
            1 => 'monday',
            2 => 'tuesday',
            3 => 'wednesday',
            4 => 'thursday',
            5 => 'friday',
            6 => 'saturday',
            7 => 'sunday',
        ];
        $date = null;

        if (isset($days[$dia_semana])) {
            $date = new \Carbon\Carbon('last ' . $days[$dia_semana]);
        }

        $data = [
            'id'                    => $this->id,
            'pais_id'               => $this->pais_id,
            'activo'                => $this->usuario->activo,
            'name'                  => (count($name) > 1 ? $name[0] : $this->nombre),
            'gender'                => ($this->genero==1 ? 'M': ($this->genero==2 ? 'F' : 'U')) ,
            'country'               => $this->pais_id,
            'dia_semana'            => $dia_semana,
            // 'ultima_sincronizacion' => $date ? $date->toDateString() : null,
        ];

        switch ($red_social) {
            case 'twitter':
                if (!empty($this->perfilTwitter)) {
                    $data['screen_name']        = str_replace('@', '', $this->perfilTwitter->screen_name);
                    $data['oauth_token']        = $this->perfilTwitter->oauth_token;
                    $data['oauth_token_secret'] = $this->perfilTwitter->oauth_token_secret;
                }
                break;
            case 'facebook':
                if (!empty($this->perfilFacebook)) {
                    $data['screen_name']  = $this->perfilFacebook->screen_name;
                    $data['facebook_id']  = $this->perfilFacebook->facebook_id;
                    $data['page_id']      = $this->perfilFacebook->page_id;
                    $data['access_token'] = $this->perfilFacebook->page_access_token;
                }
                break;
        }

        return $data;
    }

    /**
     * Determina si se ha modificado cualquiera de los campos id, pais_id, activo, name, gender, country, dia_semana,
     * ultima_sincronizacion, oauth_token, oauth_token_secret,
     * para el caso de Twitter también se incluye screen_name
     * o si el elemento es un nuevo registro para ser enviado al BackEnd
     *
     * @return boolean
     */
    public function hasChangesToSendBackEnd($red_social)
    {
        // No funciona bien isDirty, no detecta los cambios
        return true;

        if (!$this->exists) {
            return true;
        }

        $hasChangeInRedSocial = false;

        switch ($red_social) {
            case 'twitter':
                if (!empty($this->perfilTwitter)) {
                    $hasChangeInRedSocial = $this->perfilTwitter->isDirty('oauth_token') ||
                                            $this->perfilTwitter->isDirty('oauth_token_secret') ||
                                            $this->perfilTwitter->isDirty('screen_name');
                }
                break;
            case 'facebook':
                if (!empty($this->perfilFacebook)) {
                    $hasChangeInRedSocial = $this->perfilFacebook->isDirty('screen_name') ||
                                            $this->perfilFacebook->isDirty('facebook_id') ||
                                            $this->perfilFacebook->isDirty('page_id') ||
                                            $this->perfilFacebook->isDirty('access_token');
                }
                break;
        }

        if (
            $this->usuario->isDirty('activo') ||
            $this->usuario->isDirty('nombre') ||
            $this->isDirty('pais_id') ||
            $this->isDirty('nombre') ||
            $this->isDirty('genero') ||
            $hasChangeInRedSocial
        ) {
            return true;
        }

        return false;
    }

    /**
     * Envía los datos del usuario al BackEnd
     *
     * @param string $type POST, PUT
     * @param string $red_social twitter, facebook
     * @param boolean $forceToSend Determina si se forza a enviar todos los datos al backend
     */
    public function sendInfluenciadorToBackend($type, $red_social, $forceToSend = false)
    {
        $serviceBack = null;

        if ($this->hasChangesToSendBackEnd($red_social) || $forceToSend) {
            switch ($red_social) {
                case 'twitter':
                        $serviceBack = new ServiciosTwitter();
                    break;
                case 'facebook':
                        $serviceBack = new ServiciosFacebook();
                    break;
            }

            $data = $this->getDataToBackEnd($red_social);

            if (strtolower($type) == 'post') {
                $serviceBack->influenciadorPost($data);
            } elseif (strtolower($type) == 'put') {
                unset($data['dia_semana']);
                $serviceBack->influenciadorPut($this->id, $data);
            }
        }
    }

    /**
     * Obtiene el día de la semana con menor carga de trabajo para procesar
     *
     * @return int
     */
    public static function getBestDiaSemanaToProcess()
    {
        // Solo se consideran los usuarios activos para determinar el dia de la semana a procesar
        $query = 'SELECT dia_semana
            FROM perfil_twitter
            INNER JOIN influenciador ON
                influenciador.id = perfil_twitter.influenciador_id
            INNER JOIN usuario ON
                usuario.id = influenciador.usuario_id AND
                tipo_usuario_id = 5 AND
                usuario.activo = 1
            WHERE dia_semana IS NOT NULL
            GROUP BY dia_semana
            ORDER BY SUM( cantidad_seguidores ) ASC
            LIMIT 1';

        $result = \DB::select($query);

        if (!empty($result)) {
            return $result[0]->dia_semana;
        }

        return 0;
    }

    /**
     * Obtiene el listado de IDs de los influenciadores que estan relacionados con los entornos
     *
     * @param array $entornos
     *
     * @return array Arreglo de IDs de influenciadores
     */
    public static function getIdsInflueRelatedByEntorno($entornos)
    {
        $ids = [];
        $result = \DB::table('entorno_x_influenciador')->whereIn('entorno_id', $entornos)->get();

        if (!empty($result)) {
            $ids = array_pluck($result, 'influenciador_id');
        }

        return $ids;
    }

    /**
     * Revisa si un influenciador pertenece a un entorno específico
     *
     * @param integer $influenciador_id
     * @param integer $entorno_id
     *
     * @return boolean
     */
    public static function isInEntorno($influenciador_id, $entorno_id)
    {
        return EntornoXInfluenciador::where([
                'influenciador_id' => $influenciador_id,
                'entorno_id'       => $entorno_id
            ])->count() > 0;
    }

    /**
     * Revisa si un influenciador pertenece a un entorno específico
     *
     * @param integer $entorno_id
     *
     * @return boolean
     */
    public function inEntorno($entorno_id)
    {
        return static::isInEntorno($this->id, $entorno_id);
    }

    /**
     * Verifica si el usuario pertenece al entorno Televisa
     *
     * @return boolean
     */
    public function inEntornoTelevisa()
    {
        return $this->inEntorno(Entorno::TELEVISA);
    }

    /**
     * Obtener los datos para interacciones del influenciador por la campaña especificada
     *
     * @param integer $campanaId
     *
     * @return stdClass
     */
    public function getDataForInteractionsByCampana($campanaId)
    {
        return DB::select("SELECT
    SUM(retweets) AS retweets,
    SUM(favorites) AS favorites,
    SUM(replies) AS replies,
    SUM(total_followers_rt) AS total_followers_rt,
    fecha
FROM
(
    SELECT * FROM (
        SELECT
            publicacion.id,
            publicacion_estadistica.retweets,
            publicacion_estadistica.favorites,
            (
                SELECT COUNT(*)
                FROM reply
                WHERE reply.publicacion_id = publicacion.id_publicacion_red_social
            ) AS replies,
            publicacion_estadistica.total_followers_rt,
            publicacion_estadistica.fecha
        FROM
            publicacion
        INNER JOIN publicacion_estadistica ON
            publicacion.id = publicacion_estadistica.publicacion_id
        WHERE
            publicacion.campana_id = :campanaId AND
            publicacion.estado = 3 AND
            publicacion.influenciador_id = :influenciadorId
        ORDER BY
            publicacion.id DESC, publicacion_estadistica.fecha DESC
    ) AS result_publicacion GROUP BY id
) AS result_campana", ['campanaId' => $campanaId, 'influenciadorId' => $this->id])[0];
    }

    /**
     * calcula la interacción por campana
     *
     * @param integer $campanaId
     *
     * @return integer
     */
    public function getInteractionsByCampana($campanaId)
    {
        $this->interactionsByCampana = $this->getDataForInteractionsByCampana($campanaId);

        return $this->interactionsByCampana->retweets + $this->interactionsByCampana->favorites + $this->interactionsByCampana->replies;
    }

    /**
     * obtiene el engagement del influencer por campaña
     *
     * @param integer $campanaId
     *
     * @return float
     */
    public function getEngagementByCampana($campanaId)
    {
        if (is_null($this->interactionsByCampana)) {
            $this->interactionsByCampana = $this->getDataForInteractionsByCampana($campanaId);
        }

        if ($this->total_followers > 0 ) {
            return (($this->interactionsByCampana->favorites + (2 * $this->interactionsByCampana->replies) + (3 * $this->interactionsByCampana->retweets)) / $this->total_followers) * 100;
        }

        return 0;
    }

    /**
     * Obtiene las categorias de influenciadores del listado de Replies
     *
     * @return array
     */
    public function getCategoriasTopInfluencers()
    {
        return \DB::select('SELECT tipo, COUNT(*) AS total FROM
                (SELECT DISTINCT(screen_name),
                    (CASE
                        WHEN followers <= 10000 THEN "Newbie"
                        WHEN followers > 10000   AND followers <= 100000  THEN "Amateur"
                        WHEN followers > 100000  AND followers <= 1000000 THEN "Pro"
                        WHEN followers > 1000000 AND followers <= 5000000 THEN "Expert"
                        WHEN followers > 5000000 THEN "Star"
                    END) AS tipo,
                    followers
                FROM estadis_influen_top_follower_tw
                WHERE influenciador_id = '.$this->id.'
                ) AS top_followers
            GROUP BY tipo');
    }


     public function getArrayCategoriasInfluencers(){
        $categorias = ['data' => []];
            // "data" => [
            //     ["name"=>"Newbie","value"=>0],
            //     ["name"=>"Amateur","value"=>0],
            //     ["name"=>"Pro","value"=>0],
            //     ["name"=>"Expert","value"=>0],
            //     ["name"=>"Star","value"=>0],
            // ]
        // ];

        $colors = ['#D8D8D8', '#4E6185','#544991','#322774','#00bdc5','#C68BE1'];

        $catsdb = self::getCategoriasTopInfluencers();
        $i      = 0;

        foreach($catsdb as $cat){
            $categorias["data"][] = [
                'name'   => $cat->tipo,
                'value'  => $cat->total,
                'colors' => $colors[$i]
            ];

            $i++;

            // for($i=0;$i<count($categorias["data"]);$i++){

            //     if ($categorias["data"][$i]["name"] == $cat->tipo){
            //         $categorias["data"][$i]["value"] = $cat->total;
            //     }
            // }

            // $total += $cat->total;
        }
        return $categorias;

    }

    /**
     * returns followers fakes
     *
     * @return stdClass
     */
    public function followersFakes()
    {
        $result = DB::select("SELECT followers_fakes, followers_revisados FROM estadis_influen_followers_tw WHERE influenciador_id = ? AND ultima_revision = (SELECT MAX(ultima_revision) FROM estadis_influen_followers_tw WHERE influenciador_id = ?)", [$this->id, $this->id]);

        if (!empty($result)) {
            return $result[0];
        }

        return (object) [
            'followers_fakes'     => 0,
            'followers_revisados' => 0,
        ];
    }

    /**
     * Revisa si tiene campañas pendientes por aprobar
     *
     * @return boolean
     */
    public function tieneCampanaPendiente()
    {
        return InfluenciadorXCampana::where('estado', 0)
            ->where('influenciador_id', $this->id)
            ->count() > 0;
    }

    /**
     * Revisa si tiene publicaciones pendientes por aprobar
     *
     * @return boolean
     */
    public function tienePublicacionPendiente($campana_id = null)
    {
        $query = Publicacion::where('estado', 0)
            ->where('influenciador_id', $this->id)
            ->whereNotNull('fecha_hora_publicacion');

        if (!empty($campana_id)) {
            $query->where('campana_id', $campana_id);
        }

        return $query->count() > 0;
    }

    /**
     * Obtiene el top 5 de hashtags del influenciador
     *
     * @return array
     */
    public function getTopHashtag()
    {
        $query = Hashtag::select(\DB::raw('hashtag, SUM(cantidad) AS cantidad'))
            ->where('influenciador_id', $this->id)
            ->groupBy('hashtag')
            ->orderBy('cantidad', 'DESC');

        $result = $query->limit(5)
            ->get();

        return $result->toArray();
    }

    public function getTopHashtagsJsonGrafica()
    {
        $top_hashtags = $this->getTopHashtag();
        $data = [
            'etiquetas' => [],
            'data'      => []
        ];

        foreach($top_hashtags as $hashtag)
        {
            $data['etiquetas'][] = $hashtag['hashtag'];
            $data['data'][] = [
                'value' => $hashtag['cantidad'],
                'symbol' => 'circle',
                'symbolSize' => 20,
            ];
        }

        return $data;
    }
    /**
     * Obtiene el top 5 de hashtag de la comunidad
     *
     * @return array
     */
     public function getTopHashtagsComunidad()
     {
        $query = HashtagComunidad::select('hashtag', DB::raw('SUM(cantidad) AS cantidad'))
            ->whereRaw('ultima_revision = (SELECT MAX(ultima_revision) FROM estadis_influen_comu_hashtag_tw WHERE influenciador_id = '.  $this->id .' )')
            ->where('influenciador_id',  $this->id)
            ->groupBy('hashtag')
            ->orderBy('cantidad', 'desc');

        $result = $query->limit(5)
            ->get();

        return $result->toArray();
     }
     /**
      * Json para graficar comunidad de hahstag para influenciador
      *
      */
      public function getTopHashtagsComunidadJsonGrafica()
    {
        $top_hashtags = $this->getTopHashtagsComunidad();
        $data = [
            'etiquetas' => [],
            'data'      => []
        ];

        foreach($top_hashtags as $hashtag)
        {
            $data['etiquetas'][] = $hashtag['hashtag'];
            $data['data'][] = [
                'value' => $hashtag['cantidad'],
                'symbol' => 'circle',
                'symbolSize' => 20,
            ];
        }

        return $data;
    }
    /**
     * Obtiene el top 5 de palabras Clave
     *
     * @return array
     */
    public function getTopPalabrasClaves()
    {
        $response = ETLPalabraClave::select(\DB::raw('tipo_palabra_clave_id, estadis_influen_palabra_clave_tw.palabra,SUM(cantidad) AS cantidad') )
        ->join('palabra_clave', \DB::raw('LOWER(palabra_clave.palabra)'), '=', 'estadis_influen_palabra_clave_tw.palabra')
        ->where('estadis_influen_palabra_clave_tw.influenciador_id', $this->id)
        ->where('palabra_clave.tipo_palabra_clave_id', '!=', '2')
        ->groupBy('estadis_influen_palabra_clave_tw.palabra')
        ->orderBy('cantidad', 'DESC')
        ->limit(5)
        ->get();

        return $response->toArray();
    }

    /**
     * Obtiene el top 5 de palabra clave de la comunidad del influencer
     *
     * @return array
     */
    public function getTopPalabrasClavesComunidad()
    {
        $query = PalabraClaveComunidad::select('palabra', DB::raw('SUM(cantidad) AS cantidad'))
            ->whereRaw('ultima_revision = (SELECT MAX(ultima_revision) FROM estadis_influen_comu_palabra_clave_tw WHERE influenciador_id = '. $this->id .' )')
            ->where('influenciador_id', $this->id)
            ->groupBy('palabra')
            ->orderBy('cantidad', 'desc');


        $result = $query->limit(5)
            ->get();

        return $result->toArray();
    }


    public function getTopPalabrasClavesComunidadJsonGrafica()
    {
        $top_palabras_claves = $this->getTopPalabrasClavesComunidad();
        $data = [
            'etiquetas' => [],
            'data'      => []
        ];

        foreach($top_palabras_claves as $palabra)
        {
            $data['etiquetas'][] = $palabra['palabra'];
            $data['data'][] = [
                'value' => $palabra['cantidad'],
                'symbol' => 'circle',
                'symbolSize' => 20,
            ];
        }

        return $data;
    }

    /**
     * Parsea el array de top palabras claves para utilizarlo en echart
     *
     * @return array
     */
    public function getTopPalabrasClavesJsonGrafica()
    {
        $top_palabras_claves = $this->getTopPalabrasClaves();
        $data = [
            'etiquetas' => [],
            'data'      => []
        ];

        foreach($top_palabras_claves as $palabra)
        {
            $data['etiquetas'][] = $palabra['palabra'];
            $data['data'][] = [
                'value' => $palabra['cantidad'],
                'symbol' => 'circle',
                'symbolSize' => 20,
            ];
        }

        return $data;
    }

    /**
     * Obtiene el top 5 Marcas
     *
     * @return array
     */
    public function getTopPalabrasMarcas()
    {
        $response = ETLPalabraClave::select(\DB::raw(' estadis_influen_palabra_clave_tw.palabra,SUM(cantidad) AS cantidad') )
        ->join('palabra_clave', \DB::raw('LOWER(palabra_clave.palabra)'), '=', 'estadis_influen_palabra_clave_tw.palabra')
        ->where('estadis_influen_palabra_clave_tw.influenciador_id', $this->id)
        ->where('palabra_clave.tipo_palabra_clave_id', '=', '2')
        ->groupBy('estadis_influen_palabra_clave_tw.palabra')
        ->orderBy('cantidad', 'DESC')
        ->limit(5)
        ->get();

        return $response->toArray();
    }
    /**
     * Obtiene el top 5 Marcas JSON
     *
     * @return array
     */
    public function getMarcas(){
        $catsdb = self::getTopPalabrasMarcas();

        return [
            'data' => $catsdb,
        ];
    }

    /**
     * verifies that current interactions are loaded
     *
     * @return stdClass|null
     */
    public function getInteractions()
    {
        return $this->interactions;
    }

    /**
     * loads influencer´s interactions
     *
     * @return stdClass
     */
    public function loadInteractions($lastWeek = null)
    {
        $params = [$this->id, $this->id];

        $this->interactions = DB::select("(SELECT
    topTweetInflu.ultima_revision,
    SUM(retweets) AS retweets,
    SUM(favorites) AS favorites,
    (
        SELECT replies
        FROM estadis_influenciador_tw
        WHERE influenciador_id = topTweetInflu.influenciador_id
        AND ultima_revision <= topTweetInflu.ultima_revision
        ORDER BY ultima_revision DESC
        LIMIT 1
    ) AS replies,
    SUM(total_followers_rt) AS total_followers_rt
FROM estadis_influen_top_tweet_tw AS topTweetInflu
WHERE influenciador_id = ? AND
ultima_revision = (
    SELECT MAX(ultima_revision) FROM estadis_influen_top_tweet_tw
    WHERE influenciador_id = topTweetInflu.influenciador_id
)
ORDER BY ultima_revision DESC)

UNION

(SELECT
    topTweetInflu.ultima_revision,
    SUM(retweets) AS retweets,
    SUM(favorites) AS favorites,
    (
        SELECT replies
        FROM estadis_influenciador_tw
        WHERE influenciador_id = topTweetInflu.influenciador_id
        AND ultima_revision <= topTweetInflu.ultima_revision
        ORDER BY ultima_revision DESC
        LIMIT 1
    ) AS replies,
    SUM(total_followers_rt) AS total_followers_rt
FROM estadis_influen_top_tweet_tw AS topTweetInflu
WHERE influenciador_id = ? AND
ultima_revision = (
    SELECT ultima_revision FROM estadis_influen_top_tweet_tw
    WHERE influenciador_id = topTweetInflu.influenciador_id
    GROUP BY ultima_revision
    ORDER BY ultima_revision DESC
    LIMIT 1,1
)
ORDER BY ultima_revision DESC)", $params);
    }


    private function verifyInteractions($lastWeek = null)
    {
        if (is_null($this->getInteractions())) {
            $this->loadInteractions($lastWeek);
        }
    }

    /**
     * calculates influencer´s engagement
     *
     * Formula: (favorites + 2replies + 3retweets) / alcance_real
     *
     * @return float
     */
    public function getEngagement($lastWeek = null)
    {
        $this->verifyInteractions($lastWeek);

        return $this->calculateEngagement($this->interactions[0]);
    }

    /**
     * calculates engagement with given interactions
     *
     * @return floar
     */
    private function calculateEngagement($interactions)
    {
        if ($this->alcance_real == 0) {
            return 0;
        }

        return (($interactions->favorites + (2 * $interactions->replies) + (3 * $interactions->retweets)) / $this->alcance_real) * 100;
    }

    /**
     * calculates the conversation rate
     *
     * @param $lastWeek
     *
     * @return float
     */
    public function getConversationRate($lastWeek = null)
    {
        $this->verifyInteractions($lastWeek);

        return $this->calculateConversationRate($this->interactions[0]);
    }

    /**
     * calculates conversation rate
     *
     * @return float
     */
    private function calculateConversationRate($interactions)
    {
        if ($this->alcance_real == 0) {
            return 0;
        }

        return (($interactions->favorites + $interactions->retweets) / $this->alcance_real) * 100;
    }

    /**
     * calculates influencer's amplification rate
     *
     * Formula: (2replies + 3retweets) / alcance real
     *
     * @return float
     */
    public function getAmplificationRate($lastWeek = null)
    {
        $this->verifyInteractions($lastWeek);

        return $this->calculateAmplificationRate($this->interactions[0]);
    }

    private function calculateAmplificationRate($interactions)
    {
        if ($this->alcance_real == 0) {
            return 0;
        }

        return (((2 * $interactions->replies) + (3 * $interactions->retweets)) / $this->alcance_real) * 100;
    }

    /**
     * calculates the echo rate
     *
     * Formula: (retweets + replies) / following
     *
     * @return float
     */
    public function getEchoRate($lastWeek = null)
    {
        $this->verifyInteractions($lastWeek);

        return $this->calculateEchoRate($this->interactions[0]);
    }

    private function calculateEchoRate($interactions)
    {
        if (empty($this->perfilTwitter)) {
            return 0;
        }

        if (empty($this->perfilTwitter->estadisPerfil)) {
            return 0;
        }

        if ($this->perfilTwitter->estadisPerfil->friends == 0) {
            return 0;
        }

        return (($interactions->retweets + $interactions->replies) / $this->perfilTwitter->estadisPerfil->friends) * 100;
    }

    /**
     * calculates interactions 1000
     *
     * @param $lastWeek
     *
     * @return float
     */
    public function getInteractions1000($lastWeek = null)
    {
        $this->verifyInteractions($lastWeek);
        return $this->calculateInteractions1000($this->interactions[0]);
    }

    private function calculateInteractions1000($interactions)
    {
        if ($this->alcance_real == 0) {
            return 0;
        }

        return ((($interactions->retweets + $interactions->replies + $interactions->favorites) * 1000) / $this->alcance_real);
    }

    /**
     * loads interactions from last week
     *
     * @param integer $id influencer's id
     *
     * @return stdClass
     */
    public static function interactionsLastMonth($id)
    {
        return DB::select("SELECT
    SUM(retweets) AS retweets,
    SUM(favorites) AS favorites,
    SUM(retweets + favorites) interactions,
    DATE_FORMAT(fecha, '%b %d') AS month_day,
    ultima_revision
FROM estadis_influen_top_tweet_tw
WHERE influenciador_id = ? AND
ultima_revision >= (
    SELECT ultima_revision
    FROM estadis_influen_top_tweet_tw
    WHERE influenciador_id = ?
    GROUP BY ultima_revision
    ORDER BY ultima_revision DESC
    LIMIT 3,1
)
GROUP BY DATE_FORMAT(fecha, '%b %d')
ORDER BY ultima_revision, fecha", [$id, $id]);
    }

    /**
     * get professions
     *
     * @return array
     */
    public function getProfessions()
    {
        $bios_followers = (array) DB::select('SELECT
                REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(biografia, "#", ""), ".", ""), "@", ""), "(", ""), ")", ""), "!", ""), "¡", ""), \'"\', "") AS biografia
            FROM estadis_influen_top_follower_tw
            WHERE influenciador_id = '.$this->id.'
            GROUP BY screen_name');
        $profesiones = [];
        $result = [];

        // echo '<pre>';
        // print_r($bios_followers);
        // echo '</pre>';

        foreach($bios_followers as $bio) {
            $bio_array = explode(' ', $bio->biografia);
            $bio_clean = array_filter($bio_array, function ($value) {
                return strlen($value) > 3;
            });
            $bio_clean = array_map('strtolower', $bio_clean);

            $palabras = (array) DB::select('SELECT palabra
                FROM palabra_clave
                WHERE tipo_palabra_clave_id = 5 AND
                activo = 1 AND
                LOWER(palabra) IN ("'.implode('","', $bio_clean).'")');

            foreach($palabras as $palabra) {
                if (!isset($profesiones[$palabra->palabra])) {
                    $profesiones[$palabra->palabra] = 1;
                } else {
                    $profesiones[$palabra->palabra] += 1;
                }
            }
        }

        // dump($profesiones);
        // dd();

        arsort($profesiones);

        foreach ($profesiones as $profesion => $cantidad) {
            $result[] = [
                'palabra'  => $profesion,
                'cantidad' => $cantidad,
            ];
        }

        // $result = collect($result)->sortByDesc('cantidad')->toArray();

        return array_slice($result, 0, 8);

        /*if (count($bio)) {
            $bio = array_map('strtolower', $bio);
        }

        return DB::select('SELECT palabra, 1 AS cantidad
            FROM palabra_clave
            WHERE tipo_palabra_clave_id = 5 AND
                activo = 1 AND
                (
                    id IN (
                        SELECT palabra_clave_id
                        FROM influenciador_x_profesion
                        WHERE influenciador_id = '.$this->id.'
                    ) OR
                    LOWER(palabra) IN ("'.implode('","', $bio).'")
                )');*/

        /*return DB::select('SELECT DISTINCT tw.palabra, tw.cantidad FROM estadis_influen_palabra_clave_tw tw INNER JOIN palabra_clave p ON tw.palabra = p.palabra AND tw.ultima_revision = (SELECT MAX(ultima_revision) FROM estadis_influen_palabra_clave_tw WHERE influenciador_id = ?) WHERE influenciador_id = ? AND p.tipo_palabra_clave_id = 5 ORDER BY tw.cantidad DESC LIMIT 5', [$this->id, $this->id]);*/
    }

    /**
     * checks the engagements behaviour comparing lastet engagement against last week's
     *
     * @return array
     */
    public function engagementGrow()
    {
        if (count($this->interactions) < 2) {
            return [
                'type'    => '',
                'percent' => 0
            ];
        }

        $currentEngagement = $this->calculateEngagement($this->interactions[0]);
        $lastEngagement    = $this->calculateEngagement($this->interactions[1]);

        if ($currentEngagement === $lastEngagement) {
            return [
                'type'    => '',
                'percent' => 0
            ];
        }

        if ($lastEngagement == 0) {
            return [
                'type'    => '',
                'percent' => 100
            ];
        }

        $growth  = ($currentEngagement * 100) / $lastEngagement;

        if ($growth > 100) {
            // incremento
            return [
                'type'    => 'increment',
                'percent' => $growth - 100
            ];
        } else {
            return [
                'type'    => 'decrement',
                'percent' => 100 - $growth
            ];
        }
    }

    /**
     * checks the interactions 1000 behaviour comparing lastet interactions 1000 against last week's
     *
     * @return array
     */
    public function interactions1000Grow()
    {
        if (count($this->interactions) < 2) {
            return [
                'type'    => '',
                'percent' => 0
            ];
        }

        $currentInteractions = $this->calculateInteractions1000($this->interactions[0]);
        $lastInteractions    = $this->calculateInteractions1000($this->interactions[1]);

        if ($currentInteractions === $lastInteractions) {
            return [
                'type'    => '',
                'percent' => 0
            ];
        }

        if ($lastInteractions == 0) {
            return [
                'type'    => '',
                'percent' => 100
            ];
        }

        $growth  = ($currentInteractions * 100) / $lastInteractions;

        if ($growth > 100) {
            // incremento
            return [
                'type'    => 'increment',
                'percent' => $growth - 100
            ];
        } else {
            return [
                'type'    => 'decrement',
                'percent' => 100 - $growth
            ];
        }
    }

    /**
     * checks the convsersation behaviour comparing lastet convsersation against last week's
     *
     * @return array
     */
    public function conversationGrow()
    {
        if (count($this->interactions) < 2) {
            return [
                'type'    => '',
                'percent' => 0
            ];
        }

        $currentConversations = $this->calculateConversationRate($this->interactions[0]);
        $lastConversations    = $this->calculateConversationRate($this->interactions[1]);

        if ($currentConversations === $lastConversations) {
            return [
                'type'    => '',
                'percent' => 0
            ];
        }

        if ($lastConversations == 0) {
            return [
                'type'    => '',
                'percent' => 100
            ];
        }

        $growth  = ($currentConversations * 100) / $lastConversations;

        if ($growth > 100) {
            // incremento
            return [
                'type'    => 'increment',
                'percent' => $growth - 100
            ];
        } else {
            return [
                'type'    => 'decrement',
                'percent' => 100 - $growth
            ];
        }
    }

    /**
     * checks the amplification behaviour comparing lastet amplification against last week's
     *
     * @return array
     */
    public function amplificationGrow()
    {
        if (count($this->interactions) < 2) {
            return [
                'type'    => '',
                'percent' => 0
            ];
        }

        $currentAmplification = $this->calculateAmplificationRate($this->interactions[0]);
        $lastAmplification    = $this->calculateAmplificationRate($this->interactions[1]);

        if ($currentAmplification === $lastAmplification) {
            return [
                'type'    => '',
                'percent' => 0
            ];
        }

        if ($lastAmplification == 0) {
            return [
                'type'    => '',
                'percent' => 100
            ];
        }

        $growth  = ($currentAmplification * 100) / $lastAmplification;

        if ($growth > 100) {
            // incremento
            return [
                'type'    => 'increment',
                'percent' => $growth - 100
            ];
        } else {
            return [
                'type'    => 'decrement',
                'percent' => 100 - $growth
            ];
        }
    }

    /**
     * checks the echo behaviour comparing lastet echo against last week's
     *
     * @return array
     */
    public function echoGrow()
    {
        if (count($this->interactions) < 2) {
            return [
                'type'    => '',
                'percent' => 0
            ];
        }

        $currentEcho = $this->calculateEchoRate($this->interactions[0]);
        $lastEcho    = $this->calculateEchoRate($this->interactions[1]);

        if ($currentEcho === $lastEcho) {
            return [
                'type'    => '',
                'percent' => 0
            ];
        }

        if ($lastEcho == 0) {
            return [
                'type'    => '',
                'percent' => 100
            ];
        }

        $growth  = ($currentEcho * 100) / $lastEcho;

        if ($growth > 100) {
            // incremento
            return [
                'type'    => 'increment',
                'percent' => $growth - 100
            ];
        } else {
            return [
                'type'    => 'decrement',
                'percent' => 100 - $growth
            ];
        }
    }
}