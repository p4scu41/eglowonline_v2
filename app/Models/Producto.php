<?php

namespace App\Models;

use App\Helpers\UploadFileTrait;
use Illuminate\Support\Facades\DB;

class Producto extends BaseModel
{
    use UploadFileTrait;

    protected $table = 'producto';

    public $fileName = 'imagen';

    protected $fillable = [
        'marca_id',
        'usuario_id',
        'nombre',
        'imagen',
        'activo',
    ];

    public $rules_create = [
        'marca_id'   => 'bail|required|integer|exists:marca,id',
        'usuario_id' => 'bail|nullable|integer|exists:usuario,id|unique:producto',
        'nombre'     => 'bail|required|min:3|max:45',
        'imagen'     => 'image',
        'activo'     => 'bail|required|integer|in:0,1',
    ];

    public $rules_update = [
        'marca_id'   => 'bail|integer|exists:marca,id',
        'usuario_id' => 'bail|nullable|integer|exists:usuario,id',
        'nombre'     => 'bail|min:3|max:45',
        //'imagen'   => 'image',
        'activo'     => 'bail|integer|in:0,1',
    ];

    public static $labels = [
        'marca_id'   => 'Marca',
        'usuario_id' => 'Usuario',
        'nombre'     => 'Nombre',
        'imagen'     => 'Imágen',
        'activo'     => 'Activo',
    ];

    public function getBaseUploadFolder()
    {
        return 'img' . DIRECTORY_SEPARATOR . 'producto';
    }

    /**
     * Path to the logotipo
     *
     * @return string
     */
    public function getImagen()
    {
        $logo = $this->getFile();

        if (empty($logo)) {
            $logo = 'img/placehold_logo.png';
        }

        return $logo;
    }

    /**
     *
     * @inheritDoc
     */
    public static function deletedHandler($model)
    {
        $oldFile = $model->findFile();

        // Eliminamos la imagen, si existe
        if ($oldFile != null) {
            unlink($oldFile);
        }

        return true;
    }

    /**
     * Obtiene los registros de las tablas con las que tiene relación el modelo
     *
     * @param boolean $withLabels Para incluir las etiquetas como primer elemento del array
     * @return array
     */
    public static function getCatalogs($withLabels = true)
    {
        $catalogs = [];

        $catalogs['usuario'] = User::where('tipo_usuario_id', TipoUsuario::PRODUCTO)
            ->where('activo', 1)
            ->where(DB::raw('id NOT IN (SELECT usuario_id FROM producto)'))
            ->orderBy('nombre', 'asc')
            ->get()
            ->pluck('nombre', 'id');

        $catalogs['marca'] = Marca::orderBy('nombre', 'asc')->get()
            ->pluck('nombre', 'id');

        if ($withLabels == true) {
            $catalogs['usuario']->prepend(self::$labels['usuario_id'], '');
            $catalogs['marca']->prepend(self::$labels['marca_id'], '');
        } else {
            $catalogs['usuario']->prepend('', '');
            $catalogs['marca']->prepend('', '');
        }

        return $catalogs;
    }

    /**
     *
     * @return App\Models\User $usuario
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'usuario_id', 'id');
    }

    /**
     *
     * @return App\Models\Marca $marca
     */
    public function marca()
    {
        return $this->belongsTo('App\Models\Marca', 'marca_id', 'id');
    }

    public function updateCatalogUsuario()
    {
        $usuarios = DB::select("SELECT id, nombre FROM usuario WHERE tipo_usuario_id = 4 AND activo = 1 AND id NOT IN (SELECT usuario_id FROM producto) UNION SELECT u.id, u.nombre FROM usuario u INNER JOIN producto m ON m.usuario_id = u.id WHERE m.id = ?", [$this->id]);

        $usuarios = collect($usuarios)->pluck('nombre', 'id')->prepend(self::$labels['usuario_id'], '');

        return $usuarios;
    }
}
