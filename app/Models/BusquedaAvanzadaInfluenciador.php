<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BusquedaAvanzadaInfluenciador
 *
 * @package App
 * @subpackage Models
 * @category Model
 * @author Gerardo Adrián Gómez Ruiz <gerardo@eglow.mx>
 */
class BusquedaAvanzadaInfluenciador extends Model
{
    /**
     * @var string table name
     */
    protected $table = 'busqueda_avanzada_influenciador';

    /**
     * @var boolean disable auto increment
     */
    public $incrementing = false;

    /**
     * @var array the attributes that are mass assignable
     */
    protected $fillable = ['id', 'params', 'origin', 'method', 'session_id'];
}
