<?php

namespace App\Models;

/**
 * Clase que relaciona al influenciador con la campaña
 * Son las propuestas de campañas que recibe el influenciador en su listado de "Mis Campañas"
 *
 * @package App
 * @subpackage Models
 * @category Datos
 * @author Pascual Pérez <pascual@eglow.mx>
 */
class InfluenciadorXCampana extends BaseModel
{
    protected $table = 'influenciador_x_campana';

    protected $primaryKey = 'id';

    // Quiere decir que el usuario fue agregado a la campaña, pero todavía no se le hace propuesta de publicaciones
    // por lo tanto al influenciador NO le aparecerá en su listado de "Mis Campañas"
    public static $INSERTADO           = -1;
    // Ya se le hicieron propuestas de publicaciones al influenciador, por lo tanto ya podrá ver la campaña en su
    // listado de "Mis Campañas"
    public static $ESPERANDO_RESPUESTA =  0;
    // Cuando el influenciador acepta la propuesta de campaña, ahora ya puede ver los post que se incluyen en la campaña
    public static $ACEPTADO            =  1;
    // Cuando el influenciador rechaza la propuesta de campaña, se deja de listar la campaña en su lista de "Mis Campañas"
    public static $RECHAZADO           =  2;

    public static $ESTADO = [
        -1 => 'Insertado',
        0  => 'Esperando Respuesta',
        1  => 'Aceptado',
        2  => 'Rechazado',
    ];

    protected $fillable = [
        'influenciador_id',
        'campana_id',
        'estado',
    ];

    public $rules_create = [
        'influenciador_id'       => 'bail|required|integer|exists:influenciador,id',
        'campana_id'             => 'bail|required|integer|exists:campana,id',
        'estado'                 => 'bail|required|integer|in:-1,0,1,2',
    ];

    public $rules_update = [
        'influenciador_id'       => 'bail|required|integer',
        'campana_id'             => 'bail|required|integer',
        'estado'                 => 'bail|required|integer|in:-1,0,1,2',
    ];

    public static $labels = [
        'influenciador_id'       => 'Influenciador',
        'campana_id'             => 'Campaña',
        'estado'                 => 'Status',
    ];

    /**
     *
     * @return App\Models\Influenciador $influenciador
     */
    public function influenciador()
    {
        return $this->belongsTo('App\Models\Influenciador', 'influenciador_id', 'id');
    }

    /**
     *
     * @return App\Models\Campana $campana
     */
    public function campana()
    {
        return $this->belongsTo('App\Models\Campana', 'campana_id', 'id');
    }

    /**
     *
     * @return App\Models\Publicacion $publicacion
     */
    public function publicaciones()
    {
        return Publicacion::where([
            ['campana_id', $this->campana_id],
            ['influenciador_id', $this->influenciador_id],
        ])->get();
    }

    public function isInsertado()
    {
        return $this->estado == self::$INSERTADO;
    }

    public function isEsperandoRespuesta()
    {
        return $this->estado == self::$ESPERANDO_RESPUESTA;
    }

    public function isAceptado()
    {
        return $this->estado == self::$ACEPTADO;
    }

    public function isRechazado()
    {
        return $this->estado == self::$RECHAZADO;
    }

    /**
     * Obtiene la cantidad de publicaciones que relacionan al influenciador con la campaña
     *
     * @return int
     */
    public function getCantidadPublicacionesAttribute()
    {
        $result = \DB::select('SELECT SUM(cantidad_publicaciones) AS total_publicaciones
            FROM publicacion_x_influenciador_x_campana
            WHERE influenciador_id = '.$this->influenciador_id.' AND campana_id = '.$this->campana_id);

        return !empty($result) ? $result[0]->total_publicaciones : null;
    }

    /**
     * Obtiene el total de dinero que acumulan las publicaciones que relacionan al influenciador con la campaña
     * Mutator: precio_publicaciones
     *
     * @return int
     */
    public function getPrecioPublicacionesAttribute()
    {
        $result = \DB::select('SELECT SUM(cantidad_publicaciones * precio_publicacion) AS total_precio_publicaciones
            FROM publicacion_x_influenciador_x_campana
            WHERE influenciador_id = '.$this->influenciador_id.' AND campana_id = '.$this->campana_id);

        return !empty($result) ? $result[0]->total_precio_publicaciones : null;
    }

    /**
     * Obtiene el precio_publicaciones en formato de moneda
     * Mutator: precio_publicaciones_currency
     *
     * @param  int  $value
     * @return string
     */
    public function getPrecioPublicacionesCurrencyAttribute()
    {
        $total = $this->precio_publicaciones;

        if (empty($total)) {
            return '';
        }

        return '$ ' . number_format(doubleval($this->precio_publicaciones)) . ' MXN';
    }
}
