<?php

namespace App\Models;

use DateInterval;
use DateTime;
use DB;

class PerfilTwitter extends PerfilBaseModel
{
    protected $table = 'perfil_twitter';

    protected $primary_key = 'influenciador_id';

    /*public $estadisticasPerfil = null;
    public $topFollowers = null;
    public $topTweets = null;
    public $palabrasClave = null;
    public $hashtags = null;
    public $distribucionPais = null;*/

    protected $fillable = [
        'influenciador_id',
        'twitter_id',
        'screen_name',
        'profile_image_url',
        'oauth_token',
        'oauth_token_secret',
        'cantidad_seguidores',
        'json_sincronizado',
        'fecha_sincronizacion',
        'precio_publicacion'
    ];

    public $rules_create = [
        'influenciador_id'    => 'bail|required|integer|exists:influenciador,id',
        'screen_name'         => 'bail|nullable|max:45|unique:perfil_twitter',
        'cantidad_seguidores' => 'bail|nullable|integer',
        'precio_publicacion'  => 'bail|nullable|numeric'
    ];

    public $rules_update = [
        'influenciador_id'    => 'bail|required|integer|exists:influenciador,id',
        'twitter_id'          => 'bail|nullable|max:25',
        'screen_name'         => 'bail|nullable|max:45',
        'profile_image_url'   => 'bail|nullable|max:100',
        'oauth_token'         => 'bail|nullable|max:55',
        'oauth_token_secret'  => 'bail|nullable|max:50',
        'cantidad_seguidores' => 'bail|nullable|integer',
        'precio_publicacion'  => 'bail|nullable|numeric'
    ];

    public static $labels = [
        'influenciador_id'    => 'Influenciador',
        'twitter_id'          => 'twitter_id',
        'screen_name'         => 'Usuario',
        'profile_image_url'   => 'Imagen de Perfil',
        'oauth_token'         => 'oauth_token',
        'oauth_token_secret'  => 'oauth_token_secret',
        'cantidad_seguidores' => 'Cantidad de Seguidores',
    ];

    /**
     *
     * @return App\Models\Etl\Twitter\EstadisPerfil $estadisPerfil
     */
    public function estadisPerfil()
    {
        return $this->hasOne('App\Models\Etl\Twitter\EstadisPerfil', 'influenciador_id', 'influenciador_id')
            ->whereRaw('ultima_revision = ( SELECT MAX(ultima_revision)
                    FROM estadis_influenciador_tw
                    WHERE influenciador_id = '.$this->influenciador_id.' )');
    }

    /**
     *
     * @return App\Models\Etl\Twitter\EstadisPerfil $estadisPerfil
     */
    public function estadisFollowers()
    {
        return $this->hasOne('App\Models\Etl\Twitter\EstadisInfluenciadorFollower', 'influenciador_id', 'influenciador_id')
            ->whereRaw('hombres IS NOT NULL AND
                    mujeres IS NOT NULL AND
                    followers_followers IS NOT NULL')
            ->orderBy('ultima_revision', 'DESC')
            ->limit(1); // Parche: el BackEnd esta devolviendo NULL. OJO REVISAR
            /*->whereRaw('ultima_revision = ( SELECT MAX(ultima_revision)
                        FROM estadis_influen_followers_tw
                        WHERE influenciador_id = '.$this->influenciador_id.' )');*/
    }

    /**
     *
     * @return App\Models\Etl\Twitter\TopFollower $topFollower
     */
    public function topFollowers()
    {
        return $this->hasMany('App\Models\Etl\Twitter\TopFollower', 'influenciador_id', 'influenciador_id')
            ->whereRaw('ultima_revision = ( SELECT MAX(ultima_revision)
                    FROM estadis_influen_top_follower_tw
                    WHERE influenciador_id = '.$this->influenciador_id.' )')
            ->orderBy('estadis_influen_top_follower_tw.followers', 'DESC')
            ->take(5);
    }

    /**
     *
     * @return App\Models\Etl\Twitter\TopTweet $topTweet
     */
    public function topTweets()
    {
        return $this->hasMany('App\Models\Etl\Twitter\TopTweet', 'influenciador_id', 'influenciador_id')
            ->whereRaw('ultima_revision = ( SELECT MAX(ultima_revision)
                    FROM estadis_influen_top_tweet_tw
                    WHERE influenciador_id = '.$this->influenciador_id.' )')
            ->orderBy(\DB::raw('(estadis_influen_top_tweet_tw.retweets + estadis_influen_top_tweet_tw.favorites)'), 'desc')
            ->take(5);
    }

    /**
     * Obtiene el mejor tweet de todos los tiempos
     *
     * @return App\Models\Etl\Twitter\TopTweet
     */
    public function mejorTweet()
    {
        return $this->hasMany('App\Models\Etl\Twitter\TopTweet', 'influenciador_id', 'influenciador_id')
            /*->whereRaw('ultima_revision = ( SELECT MAX(ultima_revision)
                    FROM estadis_influen_top_tweet_tw
                    WHERE influenciador_id = '.$this->influenciador_id.' )')*/
            ->orderBy(\DB::raw('(estadis_influen_top_tweet_tw.retweets + estadis_influen_top_tweet_tw.favorites)'), 'desc')
            ->take(1)
            ->first();
    }

    /**
     * get a list of tweets with more interaction
     *
     * @return App\Models\Etl\Twitter\TopTweet $topTweet
     */
    public function topTweetsInteraction()
    {
        return $this->hasMany('App\Models\Etl\Twitter\TopTweet', 'influenciador_id', 'influenciador_id')
            ->whereRaw('ultima_revision = ( SELECT MAX(ultima_revision)
                    FROM estadis_influen_top_tweet_tw
                    WHERE influenciador_id = '.$this->influenciador_id.' )')
            ->orderBy(\DB::raw('(estadis_influen_top_tweet_tw.retweets + estadis_influen_top_tweet_tw.favorites)'), 'desc')
            ->take(5);
    }

    /**
     * get a list of tweets with more interaction
     *
     * @return App\Models\Etl\Twitter\TopTweet $topTweet
     */
    public function topTweetsAlcance()
    {
        return $this->hasMany('App\Models\Etl\Twitter\TopTweet', 'influenciador_id', 'influenciador_id')
            ->whereRaw('ultima_revision = ( SELECT MAX(ultima_revision)
                    FROM estadis_influen_top_tweet_tw
                    WHERE influenciador_id = '.$this->influenciador_id.' )')
            ->orderBy('estadis_influen_top_tweet_tw.total_followers_rt', 'desc')
            ->take(5);
    }

    /**
     *
     * @return App\Models\Etl\Twitter\TopTweetComunidad $topTweet
     */
    public function topTweetsComunidad()
    {
        return $this->hasMany('App\Models\Etl\Twitter\TopTweetComunidad', 'influenciador_id', 'influenciador_id')
            ->whereRaw('ultima_revision = ( SELECT MAX(ultima_revision)
                    FROM estadis_influen_comu_top_tweet_tw
                    WHERE influenciador_id = '.$this->influenciador_id.' )')
            ->orderBy(\DB::raw('(estadis_influen_comu_top_tweet_tw.retweets + estadis_influen_comu_top_tweet_tw.favorites)'), 'desc')
            ->take(10);
    }

    /**
     *
     * @return App\Models\Etl\Twitter\PalabraClave $palabrasClave
     */
    public function palabrasClave()
    {
        return $this->hasMany('App\Models\Etl\Twitter\PalabraClave', 'influenciador_id', 'influenciador_id')
            ->whereRaw('ultima_revision = ( SELECT MAX(ultima_revision)
                    FROM estadis_influen_palabra_clave_tw
                    WHERE influenciador_id = '.$this->influenciador_id.' )');
    }

    /**
     *
     * @return App\Models\Etl\Twitter\PalabraClaveComunidad $palabrasClave
     */
    public function palabrasClaveComunidad()
    {
        return $this->hasMany('App\Models\Etl\Twitter\PalabraClaveComunidad', 'influenciador_id', 'influenciador_id')
            ->whereRaw('ultima_revision = ( SELECT MAX(ultima_revision)
                    FROM estadis_influen_comu_palabra_clave_tw
                    WHERE influenciador_id = '.$this->influenciador_id.' )');
    }

    /**
     *
     * @return App\Models\Etl\Twitter\Hashtag $hashtags
     */
    public function hashtags()
    {
        return $this->hasMany('App\Models\Etl\Twitter\Hashtag', 'influenciador_id', 'influenciador_id')
            ->whereRaw('ultima_revision = ( SELECT MAX(ultima_revision)
                    FROM estadis_influen_hashtag_tw
                    WHERE influenciador_id = '.$this->influenciador_id.' )');
    }

    /**
     *
     * @return App\Models\Etl\Twitter\HashtagComunidad $hashtags
     */
    public function hashtagsComunidad()
    {
        return $this->hasMany('App\Models\Etl\Twitter\HashtagComunidad', 'influenciador_id', 'influenciador_id')
            ->whereRaw('ultima_revision = ( SELECT MAX(ultima_revision)
                    FROM estadis_influen_comu_hashtag_tw
                    WHERE influenciador_id = '.$this->influenciador_id.' )');
    }

    /**
     *
     * @return App\Models\Etl\Twitter\Pais $paises
     */
    public function paises()
    {
        return $this->hasMany('App\Models\Etl\Twitter\Pais', 'influenciador_id', 'influenciador_id')
            ->whereRaw('ultima_revision = ( SELECT MAX(ultima_revision)
                    FROM estadis_influen_pais_tw
                    WHERE influenciador_id = '.$this->influenciador_id.' )');
    }

    public function getListDistribucionPaises()
    {
        $query = 'SELECT
                pais.pais,
                estadis_influen_pais_tw.cantidad
            FROM
                estadis_influen_pais_tw
            INNER JOIN
                pais ON pais.id = estadis_influen_pais_tw.pais
            WHERE
                estadis_influen_pais_tw.influenciador_id = '.$this->influenciador_id.' AND
                estadis_influen_pais_tw.ultima_revision = (SELECT MAX(ultima_revision)
                FROM estadis_influen_pais_tw
                WHERE influenciador_id = '.$this->influenciador_id.')
            ORDER BY estadis_influen_pais_tw.cantidad DESC
            LIMIT 5';

        $result = [
            'paises' => [],
            'cantidades' => [],
        ];

        $paises = \DB::select($query);

        if (count($paises)) {
            foreach ($paises as $pais) {
                $result['paises'][] = $pais->pais;
                $result['cantidades'][] = $pais->cantidad;
            }
        }

        return $result;
    }

    /*public function loadEstadisticas()
    {
        if (empty($this->estadisticasPerfil)) {
            $this->estadisticasPerfil = DB::select('SELECT
                    followers,
                    friends,
                    favorites,
                    tweets,
                    retweets,
                    gender,
                    country,
                    hombres,
                    mujeres,
                    ultima_revision
                FROM
                    estadis_influenciador_tw
                WHERE
                    influenciador_id = ?
                ORDER BY ultima_revision DESC
                LIMIT 1', [$this->influenciador_id]
            );
        }

        if (empty($this->topFollowers)) {
            $this->topFollowers = DB::select('SELECT
                    screen_name,
                    image_url,
                    followers,
                    friends,
                    favorites,
                    tweets,
                    gender,
                    country,
                    ultima_revision
                FROM
                    estadis_influen_top_follower_tw
                WHERE
                    influenciador_id = ? AND
                    ultima_revision = (
                        SELECT MAX(ultima_revision) FROM estadis_influen_top_follower_tw
                    )', [$this->influenciador_id]
            );
        }

        if (empty($this->topTweets)) {
            $this->topTweets = DB::select('SELECT
                    retweets,
                    favorites,
                    texto,
                    fecha,
                    multimedia,
                    ultima_revision
                FROM
                    estadis_influen_top_tweet_tw
                WHERE
                    influenciador_id = ? AND
                    ultima_revision = (
                        SELECT MAX(ultima_revision) FROM estadis_influen_top_tweet_tw
                    )', [$this->influenciador_id]
            );
        }

        if (empty($this->palabrasClave)) {
            $this->palabrasClave = DB::select('SELECT
                    palabra,
                    cantidad,
                    ultima_revision,
                FROM
                    estadis_influen_palabra_clave_tw
                WHERE
                    influenciador_id = ? AND
                    ultima_revision = (
                        SELECT MAX(ultima_revision) FROM estadis_influen_palabra_clave_tw
                    )', [$this->influenciador_id]
            );
        }

        if (empty($this->hashtags)) {
            $this->hashtags = DB::select('SELECT
                    hashtag,
                    cantidad,
                    ultima_revision
                FROM
                    estadis_influen_hashtag_tw
                WHERE
                    influenciador_id = ? AND
                    ultima_revision = (
                        SELECT MAX(ultima_revision) FROM estadis_influen_hashtag_tw
                    )', [$this->influenciador_id]
            );
        }

        if (empty($this->distribucionPais)) {
            $this->distribucionPais = DB::select('SELECT
                    pais,
                    cantidad,
                    ultima_revision
                FROM
                    estadis_influen_pais_tw
                WHERE
                    influenciador_id = ? AND
                    ultima_revision = (
                        SELECT MAX(ultima_revision) FROM estadis_influen_pais_tw
                    )', [$this->influenciador_id]
            );
        }
    }*/

    public function getTopTweets()
    {
        return $this->select('estadis_influen_top_tweet_tw.id', 'estadis_influen_top_tweet_tw.retweets', 'estadis_influen_top_tweet_tw.favorites', \DB::raw('(estadis_influen_top_tweet_tw.retweets + estadis_influen_top_tweet_tw.favorites) as interaccion'), 'estadis_influen_top_tweet_tw.texto', 'estadis_influen_top_tweet_tw.multimedia')
            ->join('estadis_influen_top_tweet_tw', 'estadis_influen_top_tweet_tw.influenciador_id', '=', 'perfil_twitter.influenciador_id')
            ->where('estadis_influen_top_tweet_tw.ultima_revision', \DB::raw('(SELECT MAX(ultima_revision) FROM estadis_influen_top_tweet_tw)'))
            ->orderBy('interaccion', 'desc')
            ->get();
    }

    /**
     * get total multimedia from influencer
     *
     * @return stdClass $object
     */
    public function getTotalMultimedia()
    {
        return $this->select(DB::raw('SUM(estadis_influen_multimedias_tw.imagenes) AS imagenes'), DB::raw('SUM(estadis_influen_multimedias_tw.videos) AS videos'))
            ->join('estadis_influen_multimedias_tw', 'estadis_influen_multimedias_tw.influenciador_id', '=', 'perfil_twitter.influenciador_id')
            ->where('estadis_influen_multimedias_tw.ultima_revision', DB::raw('(SELECT MAX(ultima_revision) FROM estadis_influen_multimedias_tw WHERE influenciador_id = ' . $this->influenciador_id . ')'))
            ->where('estadis_influen_multimedias_tw.influenciador_id', $this->influenciador_id)
            ->orderBy('estadis_influen_multimedias_tw.imagenes', 'desc')
            ->orderBy('estadis_influen_multimedias_tw.videos', 'desc')
            ->first();
    }

    /**
     * obtain the best day to publish
     *
     * @return stdClass
     */
    public function bestDayToPublish($lastWeek = null)
    {
        $result = \DB::select('
            SELECT
                CASE WEEKDAY(fecha)
                    WHEN 0 THEN "Lunes"
                    WHEN 1 THEN "Martes"
                    WHEN 2 THEN "Miércoles"
                    WHEN 3 THEN "Jueves"
                    WHEN 4 THEN "Viernes"
                    WHEN 5 THEN "Sábado"
                    WHEN 6 THEN "Domingo"
                END AS dia_semana,
                SUM(retweets + favorites) AS total_interacciones,
                COUNT(*) AS total_publicaciones,
                ROUND(SUM(retweets + favorites) / COUNT(id)) AS promedio_interacciones
            FROM estadis_influen_top_tweet_tw
            WHERE influenciador_id = '.$this->influenciador_id.'
            AND ultima_revision >= (
                SELECT ultima_revision FROM (
                    SELECT DISTINCT(ultima_revision) ultima_revision
                    FROM estadis_influen_top_tweet_tw
                    WHERE influenciador_id = '.$this->influenciador_id.'
                    ORDER BY ultima_revision DESC
                    LIMIT 4
                ) AS estadis_mes
                ORDER BY ultima_revision ASC
                LIMIT 1
            )
            GROUP BY WEEKDAY(fecha)
            ORDER BY promedio_interacciones DESC
        ');

        $best = collect($result)->first();

        if (empty($best)) {
            return null;
        } else {
            return (object) $best;
        }

        /*$query = $this->select(DB::raw("CASE WEEKDAY(fecha)
            WHEN 0 THEN 'Lunes'
            WHEN 1 THEN 'Martes'
            WHEN 2 THEN 'Miércoles'
            WHEN 3 THEN 'Jueves'
            WHEN 4 THEN 'Viernes'
            WHEN 5 THEN 'Sábado'
            WHEN 6 THEN 'Domingo'
        END AS dia_semana"), DB::raw('SUM(retweets + favorites) AS total_interacciones'), DB::raw('COUNT(id) AS total_publicaciones'), DB::raw('ROUND(SUM(retweets + favorites) / COUNT(id)) AS promedio_interacciones'))
            ->join('estadis_influen_top_tweet_tw', 'estadis_influen_top_tweet_tw.influenciador_id', '=', 'perfil_twitter.influenciador_id')
            ->where('estadis_influen_top_tweet_tw.influenciador_id', $this->influenciador_id)
            ->groupBy(DB::raw('WEEKDAY(fecha)'))
            ->orderBy('promedio_interacciones', 'DESC');

        if (!empty($lastWeek)) {
            $currentDate  = new DateTime;
            $fecha1       = $currentDate->format('Y-m-d');
            $endDate      = $currentDate->sub(new DateInterval('P7D'));
            $fecha2       = $endDate->format('Y-m-d');

            $query->whereBetween('estadis_influen_top_tweet_tw.fecha', [$fecha2, $fecha1]);
        }

        // dump($query->toSql());
        // dump($query->getBindings());
        return $query->first();*/
    }

    /**
     * obtain the best hour to publish
     *
     * @return stdClass
     */
    public function bestHourToPublish($lastWeek = null)
    {
        $result = \DB::select('
            SELECT
                DATE_FORMAT(fecha, "%h:00 %p") AS hora,
                SUM(retweets + favorites) AS total_interacciones,
                COUNT(*) AS total_publicaciones,
                ROUND(SUM(retweets + favorites) / COUNT(id)) AS promedio_interacciones
            FROM estadis_influen_top_tweet_tw
            WHERE influenciador_id = '.$this->influenciador_id.'
            AND ultima_revision >= (
                SELECT ultima_revision FROM (
                    SELECT DISTINCT(ultima_revision) ultima_revision
                    FROM estadis_influen_top_tweet_tw
                    WHERE influenciador_id = '.$this->influenciador_id.'
                    ORDER BY ultima_revision DESC
                    LIMIT 4
                ) AS estadis_mes
                ORDER BY ultima_revision ASC
                LIMIT 1
            )
            GROUP BY DATE_FORMAT(fecha, "%H")
            ORDER BY promedio_interacciones DESC;
        ');

        $best = collect($result)->first();

        if (empty($best)) {
            return null;
        } else {
            return (object) $best;
        }

        /*$query = $this->select(DB::raw("DATE_FORMAT(fecha,'%h:00 %p') AS hora"), DB::raw('SUM(retweets + favorites) AS total_interacciones'), DB::raw('COUNT(id) AS total_publicaciones'), DB::raw('ROUND(SUM(retweets + favorites) / COUNT(id)) AS promedio_interacciones'))
            ->join('estadis_influen_top_tweet_tw', 'estadis_influen_top_tweet_tw.influenciador_id', '=', 'perfil_twitter.influenciador_id')
            ->where('estadis_influen_top_tweet_tw.influenciador_id', $this->influenciador_id)
            ->groupBy(DB::raw("DATE_FORMAT(fecha, '%H')"))
            ->orderBy('promedio_interacciones', 'desc');

        if (!empty($lastWeek)) {
            $currentDate  = new DateTime;
            $fecha1       = $currentDate->format('Y-m-d');
            $endDate      = $currentDate->sub(new DateInterval('P7D'));
            $fecha2       = $endDate->format('Y-m-d');

            $query->whereBetween('estadis_influen_top_tweet_tw.fecha', [$fecha2, $fecha1]);
        }

        return $query->first();*/
    }
}