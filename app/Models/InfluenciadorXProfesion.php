<?php

namespace App\Models;

class InfluenciadorXProfesion extends BaseModel
{
    protected $table = 'influenciador_x_profesion';

    public $timestamps = false;

    protected $fillable = [
        'influenciador_id',
        'palabra_clave_id',
    ];

    public $rules_create = [
        'influenciador_id' => 'bail|required|integer|exists:influenciador,id',
        'palabra_clave_id' => 'bail|required|integer|exists:palabra_clave,id',
    ];

    public $rules_update = [
        'influenciador_id' => 'bail|required|integer|exists:influenciador,id',
        'palabra_clave_id' => 'bail|required|integer|exists:palabra_clave,id',
    ];

    /**
     *
     * @return App\Models\Influenciador $influenciador
     */
    public function influenciador()
    {
        return $this->belongsTo('App\Models\Influenciador', 'influenciador_id', 'id');
    }

    /**
     *
     * @return App\Models\PalabraClave $palabraClave
     */
    public function palabraClave()
    {
        return $this->belongsTo('App\Models\PalabraClave', 'palabra_clave_id', 'id');
    }
}
