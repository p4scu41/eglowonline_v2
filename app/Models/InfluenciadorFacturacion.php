<?php

namespace App\Models;

use App\Models\Model;

/**
 * this class represents table influenciador_facturacion
 *
 * @package App
 * @subpackage Models
 * @category Model
 * @author Gerardo Adrián Gómez Ruiz <gerardo@eglow.mx>
 */
class InfluenciadorFacturacion extends BaseModel
{
    /**
     * @var string the table name
     */
    protected $table = 'influenciador_facturacion';

    /**
     * @var boolean flag for created at and updated at columns
     */
    public $timestamps = false;

    /**
     * @var string the primary key column name, overriding default id
     */
    protected $primaryKey = 'influenciador_id';

    /**
     * @var array the columns that are mass fillable
     */
    protected $fillable = [
        'influenciador_id', 'razon_social', 'dia_facturacion', 'banco', 'cuenta', 'rfc', 'calle', 'no_exterior', 'no_interior', 'no_interior', 'colonia', 'codigo_postal', 'localidad', 'municipio', 'estado', 'pais_id', 'es_persona_moral'
    ];

    /**
     * @var array the rules for inserting
     */
    public $rules_create = [
        'influenciador_id' => 'bail|required|integer',
        'razon_social'     => 'bail',
        'rfc'              => 'bail|string|max:13',
        'banco'            => 'bail',
        'cuenta'           => 'bail',
        'calle'            => 'bail',
        'no_exterior'      => 'bail',
        'colonia'          => 'bail',
        'codigo_postal'    => 'bail',
        'localidad'        => 'bail',
        'municipio'        => 'bail',
        'estado'           => 'bail',
        'pais_id'          => 'bail|required',
        'es_persona_moral' => 'bail'
    ];

    /**
     * @var array the rules for updating
     */
    public $rules_update = [
        'influenciador_id' => 'bail|required|integer',
        'razon_social'     => 'bail',
        'rfc'              => 'bail|string|max:13',
        'banco'            => 'bail',
        'cuenta'           => 'bail',
        'calle'            => 'bail',
        'no_exterior'      => 'bail',
        'colonia'          => 'bail',
        'codigo_postal'    => 'bail',
        'localidad'        => 'bail',
        'municipio'        => 'bail',
        'estado'           => 'bail',
        'pais_id'          => 'bail|required',
        'es_persona_moral' => 'bail'
    ];
}
