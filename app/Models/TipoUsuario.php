<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use DB;

class TipoUsuario extends BaseModel
{
    protected $table = 'tipo_usuario';

    /**
     * Tipos de usuario
     *
     * @var int
     */
    const ADMINISTRADOR = 1;
    const AGENCIA       = 2;
    const MARCA         = 3;
    const PRODUCTO      = 4;
    const CELEBRIDAD    = 5;

    /**
     *
     * @return App\Models\Usuario[] $usuarios
     */
    public function usuarios()
    {
        return $this->hasMany('App\Models\Usuario', 'tipo_usuario_id', 'id');
    }

    /**
     *
     * @return Illuminate\Support\Collection $permisosModulos
     */
    public function permisosModulos()
    {
        // Se guarda en cache el resultado para no consultar nuevamente la base de datos
        if (!Cache::has('TipoUsuarioPermisosModulos_' . $this->id)) {
            /* SELECT
                modulo_sistema.resource,
                modulo_sistema.action,
                tipo_usuario_permiso.allow
            FROM
                tipo_usuario_permiso
            INNER JOIN modulo_sistema ON
                modulo_sistema.id = tipo_usuario_permiso.modulo_sistema_id
            WHERE
                tipo_usuario_permiso.tipo_usuario_id = 1
            ORDER BY
                modulo_sistema.resource,
                modulo_sistema.action */

            $permisosModulos = DB::table('tipo_usuario_permiso')
                ->select('resource', 'action', 'allow')
                ->join('modulo_sistema', 'modulo_sistema.id', '=', 'tipo_usuario_permiso.modulo_sistema_id')
                ->where('tipo_usuario_permiso.tipo_usuario_id', '=', $this->id)
                ->orderBy('resource')
                ->get()
                ->keyBy(function ($item) {
                    return $item->resource . '_' . $item->action;
                });

            // Guarda en cache para no volver a realizar la consulta
            // en caso de requerir los mismos datos
            Cache::put('TipoUsuarioPermisosModulos_' . $this->id, $permisosModulos, 1440);
        }

        return Cache::get('TipoUsuarioPermisosModulos_' . $this->id);
    }

    /**
     *
     * @param  string $resource
     * @param  string $action
     * @return boolean
     */
    public function hasPermisoModulo($resource, $action)
    {
        return $this->permisosModulos()->contains(function ($model, $key) use ($resource, $action) {
            return ($model->resource == $resource && $model->action == $action && $model->allow == 1);
        });
    }
}
