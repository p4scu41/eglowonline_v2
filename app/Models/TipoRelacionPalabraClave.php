<?php

namespace App\Models;

class TipoRelacionPalabraClave extends BaseModel
{
    protected $table = 'tipo_relacion_palabra_clave';

    public static $ME_GUSTA_LA_MARCA               = 1;
    public static $HE_COLABORADO_CON_LA_MARCA      = 2;
    public static $NO_DESEO_COLABORAR_CON_LA_MARCA = 3;
    public static $ME_INTERESA_PALABRA_CLAVE       = 4;

    public static function listRelacionMarca()
    {
        return self::where('id', '!=', static::$ME_INTERESA_PALABRA_CLAVE)->get();
    }
}
