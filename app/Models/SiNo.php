<?php

namespace App\Models;

class SiNo
{
    public static $opciones = [
        [
            'id'          => '',
            'descripcion' => '',
            'icono'       => '',
        ],
        [
            'id'          => 0,
            'descripcion' => 'No',
            'icono'       => 'img/si_no/no.png',
        ],
        [
            'id'          => 1,
            'descripcion' => 'Si',
            'icono'       => 'img/si_no/si.png',
        ],
    ];
}
