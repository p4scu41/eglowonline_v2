<?php
namespace App\Models;

use GuzzleHttp\Client as GuzzleHttpClient;
use Exception;

class ServiciosFacebook extends BaseServicios
{
    public $endpoints = [
        'influenciadores'   => 'influencers',
        'campanas'          => 'campaigns',
        'palabrasclave'     => 'keywords',
        'publicaciones'     => 'publications',
        'publicaciones_etl' => 'publications',
    ];

    public function __construct()
    {
        parent::__construct(config('app.url_servicios_facebook'));
    }
}
