<?php

namespace App\Models;

use User;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Schema;
use Auth;

/**
 * Implements the process base for Model Class
 */
class BaseModel extends Model
{

    /**
     * The indicator to create.
     *
     * @var string
     */
    const DO_CREATE = 'create';

    /**
     * The indicator to update.
     *
     * @var string
     */
    const DO_UPDATE = 'update';

    /**
     * The name of the column to sort.
     *
     * @var string
     */
    protected static $ORDER_BY = 'created_at';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [];

    /**
     * Validation rules to create a new record
     *
     * @var array
     */
    public $rules_create = [];

    /**
     * Validation rules to update a existing record
     *
     * @var array
     */
    public $rules_update = [];

    /**
     * Custom messages for validation errors
     *
     * @var array
     */
    public $error_messages = [];

    /**
     * List of errors thrown by validation
     *
     * @var array
     */
    public $errors = [];

    /**
     * Result of Validator::make
     *
     * @var array
     */
    public $validator = null;

    /**
     * The relationships that should be touched on save.
     *
     * @var array
     */
    protected $touches = [];

    /**
     * The labels associated to the attributes
     *
     * @var array
     */
    public static $labels = [];

    /**
     * The "booting" method of the model.
     *
     * Register the handlers for the events:
     *  creating, created, updating, updated, deleting and deleted
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            // If the user is logged in and the table has the column created_by
            // store the ID who created the record
            if (Auth::check() && Schema::hasColumn($model->getTable(), 'created_by')) { // $model->getAttribute('created_by')
                $model->created_by = Auth::user()->id;
            }

            static::creatingHandler($model);
        });

        static::created(function ($model) {
            static::createdHandler($model);
        });

        static::updating(function ($model) {
            // If the user is logged in and the table has the column updated_by
            // store the ID who updated the record
            if (Auth::check() && Schema::hasColumn($model->getTable(), 'updated_by')) { // $model->getAttribute('updated_by')
                $model->updated_by = Auth::user()->id;
            }

            static::updatingHandler($model);
        });

        static::updated(function ($model) {
            static::updatedHandler($model);
        });

        static::deleting(function ($model) {
            static::deletingHandler($model);
        });

        static::deleted(function ($model) {
            static::deletedHandler($model);
        });
    }

    /**
     * Implement the handler to creating event.
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @return boolean
     * @throws Exception
     */
    protected static function creatingHandler($model)
    {
        return true;
    }

    /**
     * Implement the handler to created event.
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @return boolean
     * @throws Exception
     */
    protected static function createdHandler($model)
    {
        return true;
    }

    /**
     * Implement the handler to updating event.
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @return boolean
     * @throws Exception
     */
    protected static function updatingHandler($model)
    {
        return true;
    }

    /**
     * Implement the handler to updated event.
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @return boolean
     * @throws Exception
     */
    protected static function updatedHandler($model)
    {
        return true;
    }

    /**
     * Implement the handler to deleting event.
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @return boolean
     * @throws Exception
     */
    protected static function deletingHandler($model)
    {
        return true;
    }

    /**
     * Implement the handler to deleted event.
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @return boolean
     * @throws Exception
     */
    protected static function deletedHandler($model)
    {
        return true;
    }

    /**
     * Validate the model against the rules based on the action.
     * If the validation fails return false and populate the
     * errors array with the messages return by the Validator.
     *
     * @param string $action static::DO_CREATE, static::DO_UPDATE
     * @return boolean
     * @throws \Exception
     */
    public function isValid($action)
    {
        $rules = [];

        switch ($action) {
            case static::DO_CREATE:
                $rules = $this->rules_create;
                break;
            case static::DO_UPDATE:
                $rules = $this->rules_update;
                break;

            default:
                throw new \Exception('No valid action to validate.');
        }

        $this->validator = Validator::make($this->getAttributes(), $rules, $this->error_messages);

        if ($this->validator->fails()) {
            $this->errors = $this->validator->errors()->toArray();
            return false;
        }

        return true;
    }

    /**
     * Filter or search the records based on the params.
     *
     * @param array $params Associative array with the format column => value, or [column, operator, value]
     * @param integer $paginate
     * @param string $order_by
     * @param string $order_direction
     * @return array Result of the query
     */
    public static function filter($params, $paginate = 15, $order_by = null, $order_direction = '')
    {
        $result = [];
        $query = static::search($params); // call scopeSearch
        $order = str_ireplace(
            ['ASC ASC', 'DESC DESC'],
            ['ASC', 'DESC'],
            (!empty($order_by) ? $order_by : static::$ORDER_BY) . ' ' .  $order_direction
        );

        $query->orderByRaw($order);

        /*dd([
            'sql' => $query->toSql(),
            'binds' => $query->getBindings(),
            'params' => $query->getRawBindings(),
        ]);*/

        if (!empty($paginate)) {
            $result = $query->paginate(is_integer($paginate) ? $paginate : 15);
        } else {
            $result = $query->get();
        }

        return $result;
    }

    /**
     * Scope a query to search by a given parameters.
     * The array of params could be associative column => value or
     * items groups like [column, operator, value].
     * Validate the column is part of the fillables
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $params
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $params)
    {
        $attributes = $this->getFillable();

        // Se agrega el campo id y los timestamp para que sean opción de filtro
        array_push($attributes, 'id', 'created_by', 'updated_by', 'created_at', 'updated_at');

        if (count($params)) {
            foreach ($params as $colum => $value) {
                // format operator [column, operator, value]
                if (is_array($value)) {
                    // Validate the column is part of the fillables
                    if (in_array($value[0], $attributes)) {
                        // If has the four parameter, it will be orWhere
                        // [column, operator, value, orwhere]
                        if (isset($value[3])) {
                            $query->orWhere($value[0], $value[1], $value[2]);
                        } else {
                            switch ($value[1]) {
                                case 'in':
                                    $query->whereIn($value[0], $value[2]);
                                    break;
                                case 'in':
                                    $query->whereNotIn($value[0], $value[2]);
                                    break;
                                default:
                                    $query->where($value[0], $value[1], $value[2]);
                            }
                        }
                    }
                }
                // format associative column => value
                else {
                    // Validate the column is part of the fillables
                    if (in_array($colum, $attributes)) {
                        $query->where($colum, $value);
                    }
                }
            }
        }

        return $query;
    }

    /**
     *
     * @return App\Models\User $usuario
     */
    public function createdBy()
    {
        if (Schema::hasColumn($this->getTable(), 'created_by')) {
            return $this->belongsTo('App\Models\User', 'created_by', 'id');
        }

        return null;
    }

    /**
     *
     * @return App\Models\User $usuario
     */
    public function updatedBy()
    {
        if (Schema::hasColumn($this->getTable(), 'updated_by')) {
            return $this->belongsTo('App\Models\User', 'id', 'updated_by');
        }

        return null;
    }

    /**
     * Obtiene los registros de las tablas con las que tiene relación el modelo
     *
     * @param  boolean $withLabels Para incluir las etiquetas como primer elemento del array
     * @return array
     */
    public static function getCatalogs($withLabels = true)
    {
        $catalogs = [];

        return $catalogs;
    }

    /**
     * Obtiene la lista de elementos para ser utilizado en un elemento select
     *
     * @param  boolean $withLabels Para incluir la etiqueta como primer elemento del array
     * @return array
     */
    public static function listToSelect($withLabels = true)
    {
        return [];
    }
}
