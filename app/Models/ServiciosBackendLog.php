<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiciosBackendLog extends Model
{
    protected $table = 'servicios_backend_log';

    public $timestamps = false;

    protected $fillable = [
        'request',
        'uri',
        'start',
        'end',
        'params',
        'response',
        'status_code',
        'status_text',
    ];

    /*
    CREATE TABLE request_log (
        id int(11) NOT NULL AUTO_INCREMENT,
        request varchar(10) NOT NULL,
        uri varchar(100) NOT NULL,
        start datetime DEFAULT NULL,
        end datetime DEFAULT NULL,
        params varchar(100) DEFAULT NULL,
        reponse text,
        status_code int(11) DEFAULT NULL,
        status_text varchar(50) DEFAULT NULL,
        PRIMARY KEY (id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
     */
}
