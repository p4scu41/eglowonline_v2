<?php

namespace App\Models;

use App\Helpers\Helper;

class Mensaje extends BaseModel
{
    protected $table = 'mensaje';

    protected $fillable = [
        'usuario_id',
        'campana_id',
        'asunto',
        'contenido',
        'leido',
    ];

    public $rules_create = [
        'usuario_id' => 'bail|required|integer|exists:usuario,id',
        'campana_id' => 'bail|required|integer|exists:campana,id',
        'asunto'     => 'bail|required|max:30',
        'contenido'  => 'bail|required|max:500',
        'leido'      => 'bail|required|integer|in:0,1',
    ];

    public $rules_update = [
        'usuario_id' => 'bail|required|integer|exists:usuario,id',
        'campana_id' => 'bail|required|integer|exists:campana,id',
        'asunto'     => 'bail|required|max:30',
        'contenido'  => 'bail|required|max:500',
        'leido'      => 'bail|required|integer|in:0,1',
    ];

    protected $appends = [
        'fecha',
        'tipo_usuario',
    ];

    /**
     *
     * @return App\Models\User $usuario
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'usuario_id', 'id');
    }

    /**
     *
     * @return App\Models\Campana $campana
     */
    public function campana()
    {
        return $this->belongsTo('App\Models\Campana', 'campana_id', 'id');
    }

    public function setLeido($leido = 1)
    {
        $this->leido = $leido;
        $this->save();
    }

    public function getFechaAttribute()
    {
        return Helper::formatDate($this->created_at);
    }

    public function getTipoUsuarioAttribute()
    {
        return $this->createdBy->tipoUsuario->descripcion;
    }

    /**
     *
     * @param string  [description]
     * @return
     **/
    public static function getMensajesUsuarioCampana($campana_id, $usuario_id, $leido = null)
    {
        $query = self::where('campana_id', $campana_id)
                ->where(function ($query) use ($usuario_id) {
                    $query->orWhere('usuario_id', $usuario_id);
                    $query->orWhere('created_by', $usuario_id);
                });

        if ($leido !== null) {
            $query->where('leido', $leido);
        }

        return $query->limit(10)->get();
    }
}
