<?php

namespace App\Models;

use DB;
use Exception;
use App\Helpers\WordCloudGetter;
// use App\Models\Etl\Twitter\EstadisCampanaTopFollower;

class Campana extends BaseModel
{
    protected $table = 'campana';

    protected $fillable = [
        'agencia_id',
        'tipo_campana_id',
        'industria_id',
        'marca_id',
        'producto_id',
        'nombre',
        'hashtags',
        'fecha_desde',
        'presupuesto',
        'fecha_hasta',
        'activo',
    ];

    public $rules_create = [
        'agencia_id'      => 'bail|required|integer|exists:agencia,id',
        'tipo_campana_id' => 'bail|required|integer|exists:tipo_campana,id',
        'industria_id'    => 'bail|required|integer|exists:industria,id',
        'marca_id'        => 'bail|integer|exists:marca,id',
        'producto_id'     => 'bail|integer|exists:producto,id',
        'nombre'          => 'bail|required|min:3|max:45',
        'hashtags'        => 'bail|required|min:3|max:200',
        'fecha_desde'     => 'bail|required|date_format:Y-m-d',
        'fecha_hasta'     => 'bail|required|date_format:Y-m-d|after:fecha_desde',
        'presupuesto'     => 'bail|required|numeric',
        'activo'          => 'bail|required|integer|in:0,1',
    ];

    public $rules_update = [
        'agencia_id'      => 'bail|integer|exists:agencia,id',
        'tipo_campana_id' => 'bail|integer|exists:tipo_campana,id',
        'industria_id'    => 'bail|required|integer|exists:industria,id',
        'marca_id'        => 'bail|nullable|integer|exists:marca,id',
        'producto_id'     => 'bail|nullable|integer|exists:producto,id',
        'nombre'          => 'bail|string|min:3|max:45',
        'hashtags'        => 'bail|string|min:3|max:200',
        'fecha_desde'     => 'bail|date_format:Y-m-d',
        'fecha_hasta'     => 'bail|date_format:Y-m-d',
        'presupuesto'     => 'bail|required|numeric',
        'activo'          => 'bail|integer|in:0,1',
    ];

    public static $labels = [
        'agencia_id'      => 'Agencia',
        'tipo_campana_id' => 'Objetivo de la Campaña',
        'industria_id'    => 'Industria',
        'marca_id'        => 'Marca',
        'producto_id'     => 'Producto',
        'nombre'          => 'Nombre',
        'hashtags'        => 'hashtags',
        'fecha_desde'     => 'Desde',
        'fecha_hasta'     => 'Hasta',
        'presupuesto'     => 'Presupuesto',
        'activo'          => 'Activo',
        'influenciadores' => 'Influenciadores',
    ];

    public $influenciadores = null;
    public $influenciadoresAprobados = null;
    public $estadisticas = null;

    /**
     * Guarda las interacciones (cantidades) de la publicación
     * retweets, favorites, replies, total_followers_rt, fecha
     *
     * @var array
     */
    public $interacciones = [];

    /**
     * Guarda suma del total de seguidores de los influenciadores que
     * aceptaron participar en la campaña
     *
     * @var int
     */
    public $alcance_organico = 0;

    /**
     * Obtiene el presupuesto en formato de moneda
     *
     * @param  int  $value
     * @return string
     */
    public function getPresupuestoCurrencyAttribute()
    {
        return '$ ' . number_format(doubleval($this->presupuesto)) . ' MXN';
    }

    /**
     * Obtiene las registros de las tablas con las que tiene relación el modelo
     *
     * @param boolean $withLabels Para incluir las etiquetas como primer elemento del array
     * @return array
     */
    public static function getCatalogs($withLabels = true)
    {
        $cataglos = [];

        $catalogs['agencia'] = Agencia::orderBy('nombre', 'asc')
            ->where('activo', 1)
            ->get()
            ->pluck('nombre', 'id');

        $catalogs['tipo_campana'] = TipoCampana::orderBy('tipo', 'asc')
            ->get()
            ->pluck('tipo', 'id');

        $catalogs['industria'] = Industria::orderBy('nombre', 'asc')
            ->where('activo', 1)
            ->get()
            ->pluck('nombre', 'id');

        $catalogs['marca'] = Marca::orderBy('nombre', 'asc')
            ->where('activo', 1)
            ->get()
            ->pluck('nombre', 'id');

        $catalogs['producto'] = Producto::orderBy('nombre', 'asc')
            ->where('activo', 1)
            ->get()
            ->pluck('nombre', 'id');

        if ($withLabels == true) {
            $catalogs['agencia']->prepend(self::$labels['agencia_id'], '');
            $catalogs['tipo_campana']->prepend(self::$labels['tipo_campana_id'], '');
            $catalogs['industria']->prepend(self::$labels['industria_id'], '');
            $catalogs['marca']->prepend(self::$labels['marca_id'], '');
            $catalogs['producto']->prepend(self::$labels['producto_id'], '');
        } else {
            $catalogs['agencia']->prepend('', '');
            $catalogs['tipo_campana']->prepend('', '');
            $catalogs['industria']->prepend('', '');
            $catalogs['marca']->prepend('', '');
            $catalogs['producto']->prepend('', '');
        }

        return $catalogs;
    }

    public function hasMarca()
    {
        return !empty($this->marca_id);
    }

    public function hasProducto()
    {
        return !empty($this->producto_id);
    }

    /**
     *
     * @return App\Models\Agencia $agencia
     */
    public function agencia()
    {
        return $this->belongsTo('App\Models\Agencia', 'agencia_id', 'id');
    }

    /**
     *
     * @return App\Models\TipoCampana $tipoCampana
     */
    public function tipoCampana()
    {
        return $this->belongsTo('App\Models\TipoCampana', 'tipo_campana_id', 'id');
    }

    /**
     *
     * @return App\Models\Marca $marca
     */
    public function marca()
    {
        return $this->belongsTo('App\Models\Marca', 'marca_id', 'id');
    }

    /**
     *
     * @return App\Models\Producto $producto
     */
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'producto_id', 'id');
    }

    /**
     *
     * @return App\Models\Industria $industria
     */
    public function industria()
    {
        return $this->belongsTo('App\Models\Industria', 'industria_id', 'id');
    }

    /**
     *
     * @return App\Models\PublicacionXInfluenciadorXCampana $industria
     */
    public function pubXInfluXCamp()
    {
        return $this->hasMany('App\Models\PublicacionXInfluenciadorXCampana', 'campana_id', 'id');
    }

    /**
     *
     * @return App\Models\Influenciador[] $influenciadores
     */
    public function getInfluenciadores($estado = null)
    {
        $campana_id = $this->id;

        return Influenciador::select('influenciador.*')->join(
            'influenciador_x_campana',
            function ($join) use ($campana_id, $estado) {
                $query = $join->on(
                    'influenciador_x_campana.influenciador_id',
                    '=',
                    'influenciador.id'
                )->where('campana_id', $campana_id);

                if ($estado !== null) {
                    if (is_array($estado)) {
                        $query->whereIn('estado', $estado);
                    } else {
                        $query->where('estado', $estado);
                    }
                }
            }
        )->with('perfilTwitter', 'tipoInfluenciador')->get();
    }

    public function loadInfluenciadores()
    {
        $this->influenciadores = $this->getInfluenciadores();
    }

    /**
     *
     * @return App\Models\Influenciador[] $influenciadores
     */
    public function influenciadoresAprobados()
    {
        return $this->getInfluenciadores(InfluenciadorXCampana::$ACEPTADO);
    }

    public function loadInfluenciadoresAprobados()
    {
        $this->influenciadoresAprobados = $this->influenciadoresAprobados();
    }

    /**
     *
     * @return App\Models\Influenciador[] $influenciadores
     */
    public function influenciadoresPorAprobar()
    {
        return $this->getInfluenciadores([
            InfluenciadorXCampana::$ESPERANDO_RESPUESTA,
            InfluenciadorXCampana::$INSERTADO
        ]);
    }

    /**
     *
     * @return App\Models\Influenciador[] $influenciadores
     */
    public function influenciadoresAprobadosYPorAprobar()
    {
        return $this->getInfluenciadores([
            InfluenciadorXCampana::$ACEPTADO,
            InfluenciadorXCampana::$ESPERANDO_RESPUESTA
        ]);
    }

    /**
     * Guarda los influenciadores relacionados a la campaña
     *
     * @param  array $influenciadores
     * @param  boolean $update
     * @param  array $influenciadores_to_delete
     */
    public function saveInfluenciadores($influenciadores, $update = false, $influenciadores_to_delete = [])
    {
        // Elimina la asociacion entre influenciador y campaña
        InfluenciadorXCampana::where('campana_id', $this->id)
            ->whereIn('influenciador_id', $influenciadores_to_delete)
            ->delete();

        // Elimina todas las Publicaciones asociadas con el Influenciador y la Campaña
        PublicacionXInfluenciadorXCampana::where('campana_id', $this->id)
            ->whereIn('influenciador_id', $influenciadores_to_delete)
            ->delete();

        // Elimina el detalle de las Publicaciones
        Publicacion::where('campana_id', $this->id)
            ->whereIn('influenciador_id', $influenciadores_to_delete)
            ->delete();

        // Solo se procesa si se envia el listado de influenciadores
        if (count($influenciadores)) {
            foreach ($influenciadores as $influenciador) {
                $influenciadorCampana = InfluenciadorXCampana::where([
                        'influenciador_id' => $influenciador,
                        'campana_id'       => $this->id,
                    ])->first();

                if (empty($influenciadorCampana)) {
                    $influenciadorCampana = new InfluenciadorXCampana();
                }

                $influenciadorCampana->influenciador_id       = $influenciador;
                $influenciadorCampana->campana_id             = $this->id;

                // Cuando el estatus es -1 Insertado quiere decir que todavia no se le ha echo propuesta de Publicaciones
                // al influenciador, por lo tanto la campaña no se visualizará en la sección de "Mis Campañas"
                // del Influenciador
                if (!$influenciadorCampana->exists) {
                    $influenciadorCampana->estado = InfluenciadorXCampana::$INSERTADO;
                }

                if (!$influenciadorCampana->isValid(
                    $update ?
                    InfluenciadorXCampana::DO_UPDATE :
                    InfluenciadorXCampana::DO_CREATE
                )) {
                    throw new Exception('Error al procesar los influenciadores. ' .
                        Helper::errorsToString($influenciadorCampana->errors), 422);
                };

                $influenciadorCampana->save();
            }
        }
    }

    /**
     *
     * @return App\Models\Publicacion[] $publicaciones
     */
    public function publicaciones()
    {
        return $this->hasMany('App\Models\Publicacion', 'campana_id', 'id');
    }

    /**
     * Obtiene la cantidad de influenciadores que ya enviaron por lo menos una publicación de la campaña
     *
     * @return int
     */
    public function getCountInfluenciadoresPublicados()
    {
        $result = \DB::select('SELECT COUNT(DISTINCT(influenciador_id)) AS total
            FROM publicacion WHERE
            estado = '.Publicacion::$APROBADO_PUBLICADO.' AND
            campana_id = '.$this->id);

        if (!empty($result)) {
            return $result[0]->total;
        }

        return 0;
    }

    /**
     * Obtiene la cantidad de publicaciones (enviadas) que participan en la campaña
     *
     * @return int
     */
    public function getCountPublicacionesEnviadas()
    {
        return Publicacion::where([
                'campana_id' => $this->id,
                'estado'     => Publicacion::$APROBADO_PUBLICADO,
            ])->count();
    }

    /**
     * Obtiene la cantidad de influenciadores (Aceptados) que participan en la campaña
     *
     * @return int
     */
    public function getCountAutores()
    {
        return InfluenciadorXCampana::where([
                'campana_id' => $this->id,
                'estado'     => InfluenciadorXCampana::$ACEPTADO,
            ])->count();
    }

    /**
     * Obtiene los datos que se enviaran al Backend
     * id, hashtag, fecha_desde, fecha_hasta, activo
     *
     * @return array
     */
    public function getDataToBackEnd()
    {
        $hashtags = explode(' ', str_replace('#', '', $this->hashtags)); // Elimina el #

        return [
            'id'          => $this->id,
            'hashtag'     => $hashtags[0],
            'fecha_desde' => $this->fecha_desde,
            'fecha_hasta' => $this->fecha_hasta,
            'activo'      => $this->activo,
        ];
    }

    /**
     * Determina si se ha modificado cualquiera de los campos hashtag, fecha_desde, fecha_hasta, activo
     * o si el elemento es un nuevo registro para ser enviado al BackEnd
     *
     * @return boolean
     */
    public function hasChangesToSendBackEnd()
    {
        // No funciona bien isDirty, no detecta los cambios
        return true;

        if (!$this->exists) {
            return true;
        }

        if (
            $this->isDirty('hashtag') ||
            $this->isDirty('fecha_desde') ||
            $this->isDirty('fecha_hasta') ||
            $this->isDirty('activo')
        ) {
            return true;
        }

        return false;
    }

    /**
     * Envia los datos de la campaña al BackEnd
     *
     * @param string $type POST, PUT
     * @param boolean $forceToSend Determina si se forza a enviar todos los datos al backend
     */
    public function sendCampanaToBackend($type, $forceToSend = false)
    {
        if ($this->hasChangesToSendBackEnd() || $forceToSend) {
            $serviceBackTwitter = new ServiciosTwitter();
            $serviceBackFacebook = new ServiciosFacebook();

            if (strtolower($type) == 'post') {
                $serviceBackTwitter->campanaPost($this->getDataToBackEnd());
                $serviceBackFacebook->campanaPost($this->getDataToBackEnd());
            } elseif (strtolower($type) == 'put') {
                $serviceBackTwitter->campanaPut($this->id, $this->getDataToBackEnd());
                $serviceBackFacebook->campanaPut($this->id, $this->getDataToBackEnd());
            }
        }
    }

    public function getReportFromBackend()
    {
        $hashtags = explode(' ', str_replace('#', '', $this->hashtags)); // Elimina el #
        $serviceBack = new ServiciosBackend();
        $resultReport = $serviceBack->report();
        $listReports = collect($resultReport->Report);

        if (count($hashtags)) {
            $hashtag = $hashtags[0];
            $report = $listReports->where('name', 'Hashtags')
                ->where('Value', $hashtag)
                ->sortByDesc('id_report')
                ->toArray();

            if (!empty($report)) {
                $first = array_shift($report);
                $return = $serviceBack->hashtagReport($first->id_report);

                return $return;
            }
        }

        return [];
    }

    public function tieneTerminosYCondiciones()
    {
        return \Storage::disk('local')->exists('campanas/' . (string) $this->id . '.' . $this->terminos_extension);
    }

    /**
     * returns the top followers for the given campaing
     *
     * @return Illuminate\Support\Collection
     */
    public function topFollowers()
    {
        /*return \DB::table('estadis_campana_top_follower_tw')
            // ->select(\DB::raw('DISTINCT(screen_name), image_url, followers, biografia, friends, favorites, tweets'))
            // ->join(DB::raw('estadis_campana_top_follower_tw AS ect'), 'campana_id', '=', 'campana.id')
            ->where('campana_id', $this->id)
            ->groupBy('screen_name')
            ->orderBy('followers', 'desc')
            ->take(5)
            ->get();*/

        return \DB::select('
            SELECT
                DISTINCT(topFollower.screen_name),
                topFollower.ultima_revision,
                topFollower.user_id,
                topFollower.image_url,
                topFollower.followers,
                topFollower.friends,
                topFollower.favorites,
                topFollower.tweets,
                (
                    SELECT COUNT(*)
                    FROM estadis_campana_top_tweet_tw
                    WHERE hashtag_id = topFollower.campana_id AND
                    screen_name = topFollower.screen_name
                )
                AS tweets_top,
                (
                    SELECT COUNT(*)
                    FROM publicacion
                    INNER JOIN perfil_twitter ON
                        perfil_twitter.influenciador_id = publicacion.influenciador_id
                    WHERE publicacion.campana_id = topFollower.campana_id AND
                    publicacion.estado = 3 AND
                    perfil_twitter.screen_name = \'@\' + topFollower.screen_name
                ) AS tweets_eglow
            FROM estadis_campana_top_follower_tw AS topFollower
            WHERE topFollower.campana_id = '.$this->id.'
            GROUP BY topFollower.screen_name
            ORDER BY topFollower.followers DESC, topFollower.ultima_revision DESC
            LIMIT 5
        ');
    }

    /**
     *
     * @return App\Models\Etl\Twitter\EstadisCampanaTopTweet[] $topTweets
     */
    public function topTweets()
    {
        /*return \App\Models\Etl\Twitter\EstadisCampanaTopTweet::where('hashtag_id', $this->id)
            ->groupBy('screen_name, texto ')
            ->orderByRaw('ORDER BY (retweets + favorites) DESC');*/
        return $this->hasMany('App\Models\Etl\Twitter\EstadisCampanaTopTweet', 'hashtag_id', 'id')
            ->groupBy(\DB::raw('estadis_campana_top_tweet_tw.screen_name, estadis_campana_top_tweet_tw.texto'))
            ->orderBy(\DB::raw('(retweets + favorites)'), 'desc');
    }

    /**
    * Obtiene:
    *   total_publicaciones
    *   total_autores
    *   retweets
    *   cantidad_seguidores
    *
    * @return array
    */
    public function getReport()
    {
        $query = 'SELECT
            COUNT(*) AS total_publicaciones,
            COUNT(DISTINCT(publicacion.influenciador_id)) AS total_autores,
            SUM(publicacion_estadistica.retweets) AS retweets,
            SUM(publicacion_estadistica.favorites) AS favorites,
            SUM(publicacion_estadistica.total_followers_rt) AS total_followers_rt,
            SUM(perfil_twitter.cantidad_seguidores) AS cantidad_seguidores
        FROM publicacion
        INNER JOIN publicacion_estadistica ON
            publicacion.id = publicacion_estadistica.publicacion_id
        INNER JOIN perfil_twitter ON
            perfil_twitter.influenciador_id = publicacion.influenciador_id
        WHERE
            publicacion.campana_id = '.$this->id.' AND
            publicacion_estadistica.fecha = (
                SELECT MAX(publicacion_estadistica.fecha)
                FROM publicacion
                INNER JOIN publicacion_estadistica ON
                publicacion.id = publicacion_estadistica.publicacion_id
                WHERE publicacion.campana_id = '.$this->id.'
            )';

        $this->estadisticas = \DB::select($query);

        if (!empty($this->estadisticas)) {
            $this->estadisticas = $this->estadisticas[0];
        }

        return $this->estadisticas;
    }

    /**
     *
     * @return int
     */
    public function interacciones()
    {
        if (!empty($this->estadisticas)) {
            return $this->estadisticas->retweets + $this->estadisticas->favorites;
        }

        return 0;
    }

    /**
     * Obtiene el engagement de la Camapaña
     * ( (Favorites + 2*Replies + 3*Retweets) / Alcance Real ) * 100
     *
     * @return int
     */
    public function engagement()
    {
        if (empty($this->interacciones)) {
            $this->loadInteracciones();
        }

        if (empty($this->alcance_organico)) {
            $this->alcanceOrganico();
        }

        if (!empty($this->alcance_organico)) {
            return round((1 * $this->interacciones['favorites'] +
                    2 * $this->interacciones['replies'] +
                    3 * $this->interacciones['retweets']) /
                    $this->alcance_organico, 4) * 100;
        }
        /*if (!empty($this->estadisticas)) {
            if ($this->estadisticas->cantidad_seguidores > 0) {
                return round($this->interacciones()/$this->estadisticas->cantidad_seguidores, 2);
            }
        }*/

        return 0;
    }

    /**
     *
     * @return App\Models\Etl\EstadisCampanaMultimedia[] $estadisticasMultimedias
     */
    public function estadisticasMultimedias()
    {
        return $this->hasOne('App\Models\Etl\Twitter\EstadisCampanaMultimedia', 'campana_id', 'id');
    }

    /**
     * Obtiene el alcance orgánico de la Camapaña
     * Total de seguidores de los influencers que participan en la campaña
     *
     * @return int
     */
    public function alcanceOrganico()
    {
        $total_seguidores = 0;

        if (empty($this->alcance_organico)) {
            $influenciadores = $this->influenciadoresAprobados();

            foreach ($influenciadores as $influencer) {
                if ($influencer->perfilTwitter) {
                    $total_seguidores += $influencer->perfilTwitter->cantidad_seguidores;
                }
            }

            $this->alcance_organico = $total_seguidores;
        }

        return $this->alcance_organico;
    }

    /**
     * Obtiene el alcance total de la Camapaña
     * Total de seguidores de los influencers que utilizaron el hashtag
     *
     * @return int
     */
    public function alcanceTotal()
    {
        return $this->getAllFollowersOfTopTweets();
    }

    /**
     * Obtiene la suma de los followers de todos los top tweets de la campaña
     *
     * @return integer
     */
    public function getAllFollowersOfTopTweets()
    {
        $result = \DB::select('SELECT
                DISTINCT(screen_name),
                SUM(followers) AS followers
            FROM estadis_campana_top_tweet_tw
            WHERE hashtag_id = '.$this->id);

        if (!empty($result)) {
            return $result[0]->followers;
        }

        return 0;
    }

    /**
     * Obtiene la suma de los followers de todos los top followers de la campaña
     *
     * @return integer
     */
    public function getAllFollowersOfTopFollowers()
    {
        $result = \DB::select('SELECT
                DISTINCT(screen_name),
                SUM(followers) AS followers
            FROM estadis_campana_top_follower_tw
            WHERE campana_id = '.$this->id);

        if (!empty($result)) {
            return $result[0]->followers;
        }

        return 0;
    }

    /**
     * Obtiene la instancia de Usario al que pertenece la campaña
     *
     * @return \App\Models\User $user
     */
    public function getUserOwner()
    {
        // Revisa si la Campaña pertenece a un Producto
        if (!empty($this->producto_id) && !empty($this->producto->usuario)) {
            return $this->producto->usuario;
        }

        // Revisa si la Campaña pertenece a una Marca
        if (!empty($this->marca_id) && !empty($this->marca->usuario)) {
            return $this->marca->usuario;
        }

        // De lo contrario la Campaña pertenece a una Agencia
        return $this->agencia->usuario;
    }

    /**
     * Obtiene todas las palabras clave del listado de Replies
     *
     * @return array
     */
    public function WordCloudFromReplies()
    {
        $textosReplies = \DB::select('SELECT MID(texto, LOCATE(" ", texto), LENGTH(texto)) AS texto
            FROM reply WHERE publicacion_id IN (
            	SELECT id_publicacion_red_social FROM publicacion WHERE campana_id = '.$this->id.'
            )
            UNION
            SELECT MID(texto, LOCATE(" ", texto), LENGTH(texto)) AS texto
            FROM estadis_campana_top_tweet_tw
            WHERE hashtag_id = '.$this->id.'
            GROUP BY screen_name, texto
            UNION
            SELECT contenido AS texto
            FROM publicacion WHERE campana_id = '.$this->id);

        $joinTextosReplies = collect($textosReplies)->implode('texto', ' ');

        return WordCloudGetter::build($joinTextosReplies);
    }

    /**
     * Obtiene todas las palabras clave del Top Tweets
     *
     * @return array
     */
    public function WordCloudFromTopTweets()
    {
        $textosTweets = \DB::select('SELECT MID(texto, LOCATE(" ", texto), LENGTH(texto)) AS texto
            FROM estadis_campana_top_tweet_tw
            WHERE hashtag_id = '.$this->id.'
            GROUP BY screen_name, texto');

        $joinTextosReplies = collect($textosTweets)->implode('texto', '');

        return WordCloudGetter::build($joinTextosReplies);
    }

    /**
     * Obtiene las categorias de influenciadores del listado de Replies
     *
     * @return array
     */
    public function getCategoriasInfluencers()
    {
        return \DB::select('SELECT tipo, count(*) AS total FROM
            (
                SELECT
                    (CASE
                        WHEN followers_count=0 THEN "Inactivo"
                        WHEN followers_count <= 10000 THEN "Newbie"
                        WHEN followers_count > 10000   AND followers_count <= 100000  THEN "Amateur"
                        WHEN followers_count > 100000  AND followers_count <= 1000000 THEN "Pro"
                        WHEN followers_count > 1000000 AND followers_count <= 5000000 THEN "Expert"
                        WHEN followers_count > 5000000 THEN "MrTweet"
                    END) AS tipo,
                    followers_count
                FROM account
                WHERE id in (
                    SELECT account_id FROM reply WHERE publicacion_id IN (
                        SELECT id_publicacion_red_social FROM publicacion WHERE campana_id = '.$this->id.'
                    )
                )
            ) AS tipo_influenciador
            GROUP BY tipo');
    }

    /**
     * Obtiene las cantidades de publicaciones para cada red social
     *
     * @return array
     */
    public function getCountPostBySocialNetwork()
    {
        $result = \DB::select('SELECT red_social_id, SUM(cantidad_publicaciones) AS total
            FROM publicacion_x_influenciador_x_campana
            WHERE campana_id = '.$this->id.'
            AND estado = '.PublicacionXInfluenciadorXCampana::$ACEPTADO.'
            GROUP BY red_social_id');
        $social_networks = [
            RedSocial::$TWITTER   => 0,
            RedSocial::$FACEBOOK  => 0,
            RedSocial::$INSTAGRAM => 0,
            RedSocial::$YOUTUBE   => 0,
            RedSocial::$SNAPCHAT  => 0,
        ];

        foreach ($result as $item) {
            $social_networks[$item->red_social_id] = $item->total;
        }

        return $social_networks;
    }

    /**
     * Carga las cantidades de interacciones de la publicacion
     *
     * @return array
     */
    public function loadInteracciones()
    {
        $this->interacciones = \DB::select('SELECT
                SUM(retweets) AS retweets,
                SUM(favorites) AS favorites,
                SUM(replies) AS replies,
                SUM(total_followers_rt) AS total_followers_rt,
                fecha
            FROM
            (
                SELECT * FROM (
                    SELECT
                        publicacion.id,
                        publicacion_estadistica.retweets,
                        publicacion_estadistica.favorites,
                        (
                            SELECT COUNT(*)
                            FROM reply
                            WHERE reply.publicacion_id = publicacion.id_publicacion_red_social
                        ) AS replies,
                        publicacion_estadistica.total_followers_rt,
                        publicacion_estadistica.fecha
                    FROM
                        publicacion
                    INNER JOIN publicacion_estadistica ON
                        publicacion.id = publicacion_estadistica.publicacion_id
                    WHERE
                        publicacion.campana_id = '.$this->id.' AND
                        publicacion.estado = '.Publicacion::$APROBADO_PUBLICADO.'
                    ORDER BY
                        publicacion.id DESC, publicacion_estadistica.fecha DESC
                ) AS result_publicacion GROUP BY id
            ) AS result_campana');

        if (!empty($this->interacciones)) {
            $this->interacciones = (array) $this->interacciones[0];

            $this->interacciones['total'] = $this->interacciones['retweets'] + $this->interacciones['favorites'] + $this->interacciones['replies'];
        }

        return $this->interacciones;
    }

    /**
     * Obtiene las cantidades de interacciones para cada red social
     *
     * @return array
     */
    public function getCountInteractionsBySocialNetwork()
    {
        $social_networks = [
            RedSocial::$TWITTER   => ['retweets' => 0, 'favorites' => 0, 'replies' => 0],
            RedSocial::$FACEBOOK  => ['retweets' => 0, 'favorites' => 0, 'replies' => 0],
            RedSocial::$INSTAGRAM => ['retweets' => 0, 'favorites' => 0, 'replies' => 0],
            RedSocial::$YOUTUBE   => ['retweets' => 0, 'favorites' => 0, 'replies' => 0],
            RedSocial::$SNAPCHAT  => ['retweets' => 0, 'favorites' => 0, 'replies' => 0],
        ];

        $resultTwitter = \DB::select('SELECT
                SUM(retweets) AS retweets,
                SUM(favorites) AS favorites,
                SUM(replies) AS replies
            FROM
            (
                SELECT * FROM (
                    SELECT
                        publicacion.id,
                        publicacion_estadistica.retweets,
                        publicacion_estadistica.favorites,
                        (
                            SELECT COUNT(*)
                            FROM reply
                            WHERE reply.publicacion_id = publicacion.id_publicacion_red_social
                        ) AS replies
                    FROM
                        publicacion
                    INNER JOIN publicacion_estadistica ON
                        publicacion.id = publicacion_estadistica.publicacion_id
                    WHERE
                        publicacion.campana_id = '.$this->id.' AND
                        publicacion.estado = '.Publicacion::$APROBADO_PUBLICADO.' AND
                        publicacion.red_social_id = '.RedSocial::$TWITTER.'
                    ORDER BY
                        publicacion.id DESC, publicacion_estadistica.fecha DESC
                ) AS result_publicacion GROUP BY id
            ) AS result_campana');

        if (!empty($resultTwitter)) {
            $social_networks[RedSocial::$TWITTER] = (array) $resultTwitter[0];
        }

        return $social_networks;
    }

    /**
     * obtiene el total de género de top followers
     *
     * @return stdClass
     */
    public function getDistribucionGenero()
    {
        $dist = DB::select("SELECT (SELECT DISTINCT COUNT(gender) AS total FROM estadis_campana_top_follower_tw WHERE campana_id = :campanaId AND gender = 'M') AS male, (SELECT DISTINCT COUNT(gender) AS total FROM estadis_campana_top_follower_tw WHERE campana_id = :campanaId2 AND gender = 'F') AS female", ['campanaId' => $this->id, 'campanaId2' => $this->id]);

        if (empty($dist)) {
            return (object) [
                'male' => 0,
                'female' => 0,
                'total' => 0,
            ];
        }

        $dist = $dist[0];
        $dist->total = $dist->male + $dist->female;

        return $dist;
    }

    /**
     * Obtener la distribución de país de la campana
     *
     * @param integer $campanaId
     *
     * @return Illuminate\Support\Collection
     */
    public static function getTopLocations($campanaId)
    {
        /*dump(
         self::select('p.pais', DB::raw('COUNT(ectf.country) AS total'))
            ->join(DB::raw('estadis_campana_top_follower_tw AS ectf'), 'ectf.campana_id', '=', 'campana.id')
            ->join(DB::raw('pais AS p'), 'ectf.country', '=', 'p.id')
            ->where('ectf.campana_id', $campanaId)
            ->groupBy('ectf.country')
            ->toSql());*/


        $result = self::select('p.pais', DB::raw('COUNT(ectf.country) AS total'))
            ->join(DB::raw('estadis_campana_top_follower_tw AS ectf'), 'ectf.campana_id', '=', 'campana.id')
            ->join(DB::raw('pais AS p'), 'ectf.country', '=', 'p.id')
            ->where('ectf.campana_id', $campanaId)
            ->groupBy('ectf.country')
            ->take(5)
            ->get();

       /* \Log::info(json_encode($result));
         \Log::info(self::select('p.pais', DB::raw('COUNT(ectf.country) AS total'))
            ->join(DB::raw('estadis_campana_top_follower_tw AS ectf'), 'ectf.campana_id', '=', 'campana.id')
            ->join(DB::raw('pais AS p'), 'ectf.country', '=', 'p.id')
            ->where('ectf.campana_id', $campanaId)
            ->groupBy('ectf.country')
            ->take(5)
            ->toSql());*/
        return $result;




    }

    /**
     * get total posts per hour
     *
     * @param integer $campanaId
     *
     * @return Illuminate\Support\Collection
     */
    public static function getPostsPerHour($campanaId)
    {
        /*return self::select(DB::raw("DATE_FORMAT( fecha_hora_publicacion,  '%H:%i') AS hora"), DB::raw("COUNT(DATE_FORMAT( fecha_hora_publicacion,  '%H:%i')) AS total"))
            ->join(DB::raw('publicacion AS p'), 'p.campana_id', '=', 'campana.id')
            ->where('p.campana_id', $campanaId)
            ->where('p.red_social_id', 1)
            ->where('p.estado', 3)
            ->groupBy(DB::raw("DATE_FORMAT( fecha_hora_publicacion,  '%H:%i')"))
            ->get();
            */
        return DB::select("SELECT hora, total FROM (SELECT DATE_FORMAT(fecha_hora_publicacion,  '%H') AS hora, COUNT(DATE_FORMAT(fecha_hora_publicacion,  '%H')) AS total FROM publicacion WHERE campana_id = ? AND red_social_id = 1 AND estado = 3 GROUP BY DATE_FORMAT(fecha_hora_publicacion,  '%H')

UNION

SELECT DATE_FORMAT(fecha,  '%H') AS hora, COUNT(DATE_FORMAT(fecha,  '%H')) AS total FROM estadis_campana_top_tweet_tw WHERE hashtag_id = ? GROUP BY screen_name, texto, DATE_FORMAT(fecha,  '%H')

UNION

SELECT DATE_FORMAT(created_at,  '%H') AS hora, COUNT(DATE_FORMAT(created_at,  '%H')) AS total FROM reply WHERE campana_id = ? and is_eglow = 1 GROUP BY DATE_FORMAT(created_at,  '%H')) t GROUP BY hora ORDER BY hora", [$campanaId, $campanaId, $campanaId]);
    }
}