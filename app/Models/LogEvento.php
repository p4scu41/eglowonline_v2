<?php

namespace App\Models;

use Auth;

class LogEvento extends BaseModel
{
    protected $table = 'log_evento';

    public static $INICIO_SESION = 1;
    public static $REGISTRO_USUARIO = 2;
    public static $REGISTRO_CAMPANA = 3;
    public static $OFERTA_CAMPANA_INFLUENCIADOR = 4;
    public static $ACEPTACION_CAMPANA_INFLUENCIADOR = 5;
    public static $CREACION_PUBLICACIONES = 6;
    public static $ACEPTACION_PUBLICACIONES_INFLUENCIADOR = 7;
    public static $RECHAZO_CAMPANA_INFLUENCIADOR = 8;

    public static $tipo = [
        1 => 'Inicio de Sesión',
        2 => 'Registro de Usuario/Influenciador',
        3 => 'Registro de Nueva Campaña',
        4 => 'Oferta de Campaña a Influenciador',
        5 => 'Aceptación de Ofertas de Campaña por Influenciador',
        6 => 'Creación de Publicaciones en Campaña',
        7 => 'Aceptación de Publicaciones por Influenciador',
        8 => 'Rechazo de Oferta de Campaña por Influenciador',
    ];

    protected $fillable = [
        'tipo',
        'campana_id',
        'usuario_id',
        'red_social_id',
        'created_by',
    ];

    public $rules_create = [
        'tipo'          => 'bail|required|integer|in:0,1,2,3,4,5,6,7',
        'campana_id'    => 'bail|nullable|integer|exists:campana,id',
        'usuario_id'    => 'bail|nullable|integer|exists:usuario,id',
        'red_social_id' => 'bail|nullable|integer|exists:red_social,id',
    ];

    public static $labels = [
        'tipo'          => 'Evento',
        'campana_id'    => 'Campaña',
        'usuario_id'    => 'Dirigido a',
        'red_social_id' => 'Red Social',
        'created_by'    => 'Usuario',
        'created_at'    => 'Fecha y Hora',
        // Campos extras, no pertenecen a la tabla
        'tipo_usuario'  => 'Tipo de Usuario',
        'fecha_desde'   => 'Desde',
        'fecha_hasta'   => 'Hasta',
    ];

    public $with = ['usuario', 'createdBy'];

    public function getTipoDescripcionAttribute($value)
    {
        return self::$tipo[$this->tipo];
    }

    /**
     * @return App\Models\User $usuario
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'usuario_id', 'id');
    }

    /**
     * @return App\Models\Agencia $agencia
     */
    public function agencia()
    {
        return $this->belongsTo('App\Models\Agencia', 'usuario_id', 'id');
    }

    /**
     * @return App\Models\Influenciador $influenciador
     */
    public function influenciador()
    {
        return $this->belongsTo('App\Models\Influenciador', 'usuario_id', 'id');
    }

    /**
     * @return App\Models\Campana $campana
     */
    public function campana()
    {
        return $this->belongsTo('App\Models\Campana', 'campana_id', 'id');
    }

    public static function getCatalogs($withLabels = true)
    {
        $catalogs = [];
        $catalogs['tipo'] = collect(self::$tipo);
        $catalogs['tipo']->prepend('', '');

        return $catalogs;
    }

    public function isInicioSesion()
    {
        return $this->tipo ==  self::$INICIO_SESION;
    }

    public function isRegistroUsuario()
    {
        return $this->tipo ==  self::$REGISTRO_USUARIO;
    }

    public function isRegistroCampana()
    {
        return $this->tipo ==  self::$REGISTRO_CAMPANA;
    }

    public function isOfertaCampanaInfluenciador()
    {
        return $this->tipo ==  self::$OFERTA_CAMPANA_INFLUENCIADOR;
    }

    public function isAceptacionCampanaInfluenciador()
    {
        return $this->tipo ==  self::$ACEPTACION_CAMPANA_INFLUENCIADOR;
    }

    public function isCreacionPublicaciones()
    {
        return $this->tipo ==  self::$CREACION_PUBLICACIONES;
    }

    public function isAceptacionPublicacionesInfluenciador()
    {
        return $this->tipo ==  self::$ACEPTACION_PUBLICACIONES_INFLUENCIADOR;
    }

    public static function saveInicioSesion()
    {
        self::create([
            'tipo' => self::$INICIO_SESION,
        ]);
    }

    public static function saveRegistroUsuario($usuario_id)
    {
        self::create([
            'tipo' => self::$REGISTRO_USUARIO,
            'usuario_id' => $usuario_id,
        ]);
    }

    public static function saveRegistroCampana($campana_id)
    {
        self::create([
            'tipo' => self::$REGISTRO_CAMPANA,
            'campana_id' => $campana_id,
        ]);
    }

    public static function saveOfertaCampanaInfluenciador($campana_id, $usuario_id, $red_social_id = null)
    {
        self::create([
            'tipo' => self::$OFERTA_CAMPANA_INFLUENCIADOR,
            'campana_id' => $campana_id,
            'usuario_id' => $usuario_id,
            'red_social_id' => $red_social_id,
        ]);
    }

    public static function saveAceptacionCampanaInfluenciador($campana_id)
    {
        self::create([
            'tipo' => self::$ACEPTACION_CAMPANA_INFLUENCIADOR,
            'campana_id' => $campana_id,
        ]);
    }

    public static function saveRechazoCampanaInfluenciador($campana_id, $red_social_id = null)
    {
        self::create([
            'tipo' => self::$RECHAZO_CAMPANA_INFLUENCIADOR,
            'campana_id' => $campana_id,
            'red_social_id' => $red_social_id,
        ]);
    }

    public static function saveCreacionPublicaciones($campana_id, $usuario_id, $red_social_id = null)
    {
        self::create([
            'tipo' => self::$CREACION_PUBLICACIONES,
            'campana_id' => $campana_id,
            'usuario_id' => $usuario_id,
            'red_social_id' => $red_social_id,
        ]);
    }

    public static function saveAceptacionPublicacionesInfluenciador($campana_id, $red_social_id = null)
    {
        self::create([
            'tipo' => self::$ACEPTACION_PUBLICACIONES_INFLUENCIADOR,
            'campana_id' => $campana_id,
            'red_social_id' => $red_social_id,
        ]);
    }

    /********************************************************************************
    Disable updated_at
    ********************************************************************************/
    public function setUpdatedAt($value){ }
    public function getUpdatedAtColumn(){ }
}
