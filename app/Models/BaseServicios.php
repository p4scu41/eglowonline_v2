<?php
namespace App\Models;

use GuzzleHttp\Client as GuzzleHttpClient;
use Exception;

class BaseServicios
{
    public $base_uri = null;

    public $endpoints = [
        'influenciadores'   => 'influenciadores',
        'campanas'          => 'campanas',
        'palabrasclave'     => 'palabrasclave',
        'publicaciones'     => 'publicaciones',
        'publicaciones_etl' => 'publicaciones/redsocial',
    ];

    /**
     * Cliente HTTP para realizar las peticiones
     *
     * @var GuzzleHttp\Client
     */
    public $httpClient = null;

    /**
     * Respuesta del servicio
     *
     * @var array
     */
    public $response = null;

    /**
     * Servico consumido
     *
     * @var string
     */
    public $currentService = null;

    /**
     *
     * @param GuzzleHttp\Client $guzzleHttpCliente
     */
    public function __construct($base_uri)
    {
        $this->base_uri = $base_uri;

        $this->httpClient = new GuzzleHttpClient([
            'base_uri' => $this->base_uri,
            // 'timeout' => 2,
        ]);
    }


    /**
     * Petición GET
     *
     * @param  string $uri Segmento a agregar en la base uri
     * @return aray
     */
    public function requestGet($uri, $data = [])
    {
        $this->currentService = $uri;

        return $this->sendRequest('GET', $uri, $data);
    }

    /**
     * Petición POST
     *
     * @param  string $uri Segmento a agregar en la base uri
     * @return aray
     */
    public function requestPost($uri, $data = [])
    {
        $this->currentService = $uri;

        return $this->sendRequest('POST', $uri, $data);
    }

    /**
     * Petición PUT
     *
     * @param  string $uri Segmento a agregar en la base uri
     * @return aray
     */
    public function requestPut($uri, $data = [])
    {
        $this->currentService = $uri;
        $data['_method'] = 'PUT';

        return $this->sendRequest('POST', $uri, $data);
    }

    /**
     * Realiza la petición a base_uri + uri con el método request de GuzzleHttp\Client
     *
     * Para obtener la duración de la petición:
     *
     *   SELECT
     *       REPLACE(uri, $base_uri, '') AS service,
     *       TIMEDIFF(END, START) AS duration_response,
     *       status_code
     *   FROM
     *       request_log
     *
     * @param  string  $method  GET | POST
     * @param  string  $uri     Segmento a agregar en la base uri
     * @param  array   $data    Datos a enviar en la petición
     * @return array   Response Body
     */
    public function sendRequest($method, $uri, $data = [])
    {
        $options = [];
        // $data    = array_filter($data, 'strlen'); // Elimina elementos vacios
        $requestLog = [
            'request'     => $method,
            'uri'         => $this->base_uri . $uri,
            'start'       => date('Y-m-d H:i:s'),
            'end'         => null,
            'params'      => json_encode($data),
            'response'    => null,
            'status_code' => null,
            'status_text' => null,
        ];

        $this->response = null;

        if (!empty($data)) {
            // declare headers & sending backend key
            $headers = ['X-CSRF-TOKEN' => env('APP_KEY')];
            switch (strtoupper($method)) {
                case 'GET':
                    $options = ['query' => $data, 'headers' => $headers];
                    break;

                case 'POST':
                    $options = ['json' => $data, 'headers' => $headers];
                    break;
            }
        }

        try {
            $response = $this->httpClient->request($method, $uri, $options);

            $requestLog['status_code'] = $response->getStatusCode();
            $requestLog['status_text'] = $response->getReasonPhrase();
            $requestLog['response']    = $response->getBody();

            $this->response = json_decode($response->getBody());
        } catch (Exception $e) {
            $requestLog['status_code'] = $e->getCode();
            $requestLog['response']    = $e->getMessage();
        } finally {
            $requestLog['end'] = date('Y-m-d H:i:s');
            ServiciosBackendLog::create($requestLog);
        }

        return $this->response;
    }

    public function influenciadorPost($data)
    {
        return $this->requestPost($this->endpoints['influenciadores'], $data);
    }

    public function influenciadorPut($id, $data)
    {
        return $this->requestPut($this->endpoints['influenciadores'] . '/' . $id, $data);
    }

    public function campanaPost($data)
    {
        return $this->requestPost($this->endpoints['campanas'], $data);
    }

    public function campanaPut($id, $data)
    {
        return $this->requestPut($this->endpoints['campanas'] . '/' . $id, $data);
    }

    public function palabraclavePost($data)
    {
        return $this->requestPost($this->endpoints['palabrasclave'], $data);
    }

    public function palabraclavePut($id, $data)
    {
        return $this->requestPut($this->endpoints['palabrasclave'] . '/' . $id, $data);
    }

    public function publicacionPost($data)
    {
        return $this->requestPost($this->endpoints['publicaciones'], $data);
    }

    public function publicacionesRedSocialPost($data)
    {
        return $this->requestPost($this->endpoints['publicaciones_etl'], $data);
    }
}
