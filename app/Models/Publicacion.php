<?php

namespace App\Models;

use App\Helpers\UploadFileTrait;
use App\Helpers\Helper;
use App\Helpers\WordCloudGetter;
use App\Models\ServiciosTwitter;
use Illuminate\Support\Facades\Log;

class Publicacion extends BaseModel
{
    use UploadFileTrait;

    protected $table = 'publicacion';

    public $fileName = 'archivo_multimedia';

    public $filePath           = null;
    public $multimediaType     = null;
    public $multimediaSize     = null;
    public $multimediaFiletype = null;

    /** @var integer $MAX_INTENTOS_FALLIDOS Cantidad máxima de intentos fallidos al publicar en la red social */
    const MAX_INTENTOS_FALLIDOS = 3;

    /*public static $ESPERANDO_APROBACION_INFLUENCER           = 0;
    public static $POR_PUBLICAR                              = 1;
    public static $PUBLICADO                                 = 2;
    public static $ESPERANDO_APROBACION_MODIFICACION_AGENCIA = 3;
    public static $RECHAZADO                                 = 4;
    public static $ERROR_PUBLICAR                            = 5;*/

    public static $ESPERANDO_APROBACION_INFLUENCER = 0; // Gris
    public static $APROBADO_INFLUENCER             = 1; // Negro
    public static $APROBADO_POR_PUBLICAR           = 2; // Negro Cron debe de buscar estos
    public static $APROBADO_PUBLICADO              = 3; // Rojo
    public static $ERROR_PUBLICAR                  = 4; // Morado

    /*public static $ESTADO = [
        0 => 'Esperando Aprobación - Influencer',
        1 => 'Por Publicar',
        2 => 'Publicado',
        3 => 'Esperando Aprobación de Modificación - Agencia',
        4 => 'Rechazado',
        5 => 'Error al publicar',
    ];*/

    public static $ESTADO = [
        0 => 'Esperando Aprobación - Influencer',
        1 => 'Aprobado Influencer',
        2 => 'Por Publicar', // Cron debe de buscar estos
        3 => 'Publicado',
        4 => 'Error al publicar',
    ];

    protected $fillable = [
        'campana_id',
        'influenciador_id',
        'red_social_id',
        'contenido',
        'fecha_hora_publicacion',
        'archivo_multimedia',
        'estado',
        'activo',
        'id_publicacion_red_social',
        'intentos_fallidos',
        'en_proceso',
        'error_publicar',
    ];

    // El valor de upload_max_filesize se establece en /public/index.php
    public $rules_create = [
        'campana_id'             => 'bail|required|integer|exists:campana,id',
        'influenciador_id'       => 'bail|required|integer|exists:influenciador,id',
        'red_social_id'          => 'bail|required|integer|exists:red_social,id',
        'contenido'              => 'bail|nullable|max:140',
        'fecha_hora_publicacion' => 'bail|nullable|date',
        'estado'                 => 'bail|required|integer|in:0,1,2,3',
        'activo'                 => 'bail|required|integer|in:0,1',
        'intentos_fallidos'      => 'integer',
        'archivo_multimedia'     => 'bail|nullable|file|mimes:gif,jpeg,jpg,png,svg+xml,mp4,mpeg,ogg,x-msvideo,'.
                                    'avi,x-msvideo,quicktime,wav,x-wav,wma,x-ms-wma,mp3,mp4',
    ];

    public $rules_update = [
     'campana_id'             => 'bail|required|integer|exists:campana,id',
     'influenciador_id'       => 'bail|required|integer|exists:influenciador,id',
     'red_social_id'          => 'bail|required|integer|exists:red_social,id',
     'contenido'              => 'bail|required|max:140',
     'fecha_hora_publicacion' => 'bail|required|date',
     'estado'                 => 'bail|required|integer|in:0,1,2,3',
     'activo'                 => 'bail|required|integer|in:0,1',
     'intentos_fallidos'      => 'integer',
     'archivo_multimedia'     => 'bail|nullable|file|mimes:gif,jpeg,jpg,png,svg+xml,mp4,mpeg,ogg,x-msvideo,'.
                                 'avi,x-msvideo,quicktime,wav,x-wav,wma,x-ms-wma,mp3,mp4',
    ];

    public static $labels = [
        'campana_id'             => 'Campaña',
        'influenciador_id'       => 'Influenciador',
        'red_social_id'          => 'Red Social',
        'contenido'              => 'Contenido',
        'fecha_hora_publicacion' => 'Fecha y Hora',
        'archivo_multimedia'     => 'Archivo Multimedia',
        'estado'                 => 'Status',
        'activo'                 => 'Activo',
        'created_at'             => 'Fecha de Registro',
        'intentos_fallidos'      => 'Intentos Fallidos',
    ];

    protected $appends = ['url_multimedia'];

    /**
     * Guarda las interacciones (cantidades) de la publicación
     * id_publicacion_red_social, retweets, favorites, replies, total_followers_rt, fecha
     *
     * @var array
     */
    public $interacciones = [];

    public function __construct()
    {
        $this->estado = 0;
        $this->activo = 1;

        $this->rules_create['archivo_multimedia'] = 'bail|nullable|file|max:'.
                Helper::returnKilobytes(ini_get('upload_max_filesize')).
                '|mimes:gif,jpeg,jpg,png,mp4';

        $this->rules_update['archivo_multimedia'] = 'bail|nullable|file|max:'.
                Helper::returnKilobytes(ini_get('upload_max_filesize')).
                '|mimes:gif,jpeg,jpg,png,mp4';

        $this->fileRule = $this->rules_create['archivo_multimedia'];
    }

    public function isValid($action)
    {
        if (!parent::isValid($action)) {
            return false;
        }

        return true;
    }

    /**
     * Obtiene los registros de las tablas con las que tiene relación el modelo
     *
     * @param  boolean $withLabels Para incluir las etiquetas como primer elemento del array
     * @return array
     */
    public static function getCatalogs($withLabels = true)
    {
        $catalogs = [];

        $catalogs['campana'] = Campana::where('activo', 1)
            ->orderBy('nombre', 'asc')
            ->get()
            ->pluck('nombre', 'id');

        $catalogs['red_social'] = RedSocial::where('activo', 1)
            ->orderBy('nombre', 'asc')
            ->get()
            ->pluck('nombre', 'id');

        $catalogs['influenciador'] = Influenciador::select('influenciador.*')
            ->join('usuario', 'usuario.id', '=', 'influenciador.usuario_id')
            ->where('usuario.activo', 1)
            ->orderBy('nombre', 'asc')
            ->get()
            ->pluck('nombre', 'id');

        if ($withLabels == true) {
            $catalogs['campana']->prepend(self::$labels['campana_id'], '');
            $catalogs['red_social']->prepend(self::$labels['red_social_id'], '');
            $catalogs['influenciador']->prepend(self::$labels['influenciador_id'], '');
        } else {
            $catalogs['campana']->prepend('', '');
            $catalogs['red_social']->prepend('', '');
            $catalogs['influenciador']->prepend('', '');
        }

        return $catalogs;
    }

    public function getBaseUploadFolder()
    {
        return 'multimedia_posts';
    }

    /**
     * Path to the logotipo
     *
     * @param boolean  $fullPath  Si es true, obtiene la ruta absoluta al archvio. Default false
     * @return string
     */
    public function getArchivoMultimedia($fullPath = false)
    {
        $this->filePath = $this->getFile();

        if (!empty($this->filePath)) {
            $this->filePath = $fullPath ? public_path($this->filePath) : $this->filePath;
            $infoFile = pathinfo($this->filePath);
            $finfo = finfo_open(FILEINFO_MIME_TYPE);

            $this->multimediaFiletype = finfo_file($finfo, $this->filePath);
            $this->multimediaType     = explode('/', $this->multimediaFiletype)[0];
            $this->multimediaSize     = filesize($this->filePath);
        }

        return $this->filePath;
    }

    public function getTypeMultimedia()
    {
        return $this->multimediaType;
    }

    public function getFileTypeMultimedia()
    {
        return $this->multimediaFiletype;
    }

    public function getSizeMultimedia()
    {
        return $this->multimediaSize;
    }

    public function multimediaIsVideo()
    {
        return $this->multimediaType == 'video';
    }
    /**
     *
     * @inheritDoc
     */
    public static function deletedHandler($model)
    {
        $oldFile = $model->findFile();

        // Eliminamos el logotipo, si existe
        if ($oldFile != null) {
            unlink($oldFile);
        }

        return true;
    }

    /**
     * Obtiene la URL del archivo multimedia
     *
     * @return string
     */
    public function getUrlMultimediaAttribute()
    {
        if ($this->getArchivoMultimedia()) {
            return asset($this->getArchivoMultimedia());
        }

        return null;
    }

    /**
     *
     * @return App\Models\Campana $campana
     */
    public function campana()
    {
        return $this->belongsTo('App\Models\Campana', 'campana_id', 'id');
    }

    /**
     *
     * @return App\Models\Influenciador $influenciador
     */
    public function influenciador()
    {
        return $this->belongsTo('App\Models\Influenciador', 'influenciador_id', 'id');
    }

    /**
     *
     * @return App\Models\RedSocial $redSocial
     */
    public function redSocial()
    {
        return $this->belongsTo('App\Models\RedSocial', 'red_social_id', 'id');
    }

    /**
     * retweeters
     *
     * @method retweeters
     *
     * @return \Illuminate\Database\Eloquent\BelongsToMany
     */
    public function retweeters()
    {
        return $this->belongsToMany(Account::class, 'retweeters_publicacion', 'publicacion_id', 'account_id')
            ->withTimestamps();
    }

    public function isEsperandoAprobacionInfluencer()
    {
        return $this->estado == self::$ESPERANDO_APROBACION_INFLUENCER;
    }

    public function isAprobadoInfluencer()
    {
        return $this->estado == self::$APROBADO_INFLUENCER;
    }

    public function isAprobadoPorPublicar()
    {
        return $this->estado == self::$APROBADO_POR_PUBLICAR;
    }

    public function isAprobadoPublicado()
    {
        return $this->estado == self::$APROBADO_PUBLICADO;
    }

    public function isErrorPublicar()
    {
        return $this->estado == self::$ERROR_PUBLICAR;
    }

    public function setEsperandoAprobacionInfluencer()
    {
        return $this->estado = self::$ESPERANDO_APROBACION_INFLUENCER;
    }

    public function setAprobadoInfluencer()
    {
        $this->estado = self::$APROBADO_INFLUENCER;
    }

    public function setAprobadoPorPublicar()
    {
        $this->estado = self::$APROBADO_POR_PUBLICAR;
    }

    public function setAprobadoPublicado()
    {
        $this->estado = self::$APROBADO_PUBLICADO;
    }

    public function setErrorPublicar()
    {
        $this->estado = self::$ERROR_PUBLICAR;
        $this->intentos_fallidos++;
    }

    public static function aprobarPublicar($campana_id, $influenciador_id)
    {
        Publicacion::where([
            ['campana_id', $campana_id],
            ['influenciador_id', $influenciador_id],
        ])->update([
            'estado' => Publicacion::$APROBADO_POR_PUBLICAR
        ]);
    }

    /**
     * Envia todas las publicaciones del influenciador, que pertenecen a la campaña especificada, al BackEnd
     *
     * @param int $campana_id
     * @param int $influenciador_id
     */
    public static function sendPublicacionToETL($campana_id, $influenciador_id)
    {
        $publicaciones = Publicacion::where([
            ['campana_id', $campana_id],
            ['influenciador_id', $influenciador_id],
            ['estado', Publicacion::$APROBADO_POR_PUBLICAR],
            ['red_social_id', RedSocial::$TWITTER],
        ])->get()->toArray();

        $serviceBack = new ServiciosTwitter();

        // ETL Twitter recibe un arreglo de publicaciones
        $serviceBack->publicacionesRedSocialPost($publicaciones);

        $publicaciones = Publicacion::where([
            ['campana_id', $campana_id],
            ['influenciador_id', $influenciador_id],
            ['estado', Publicacion::$APROBADO_POR_PUBLICAR],
            ['red_social_id', RedSocial::$FACEBOOK],
        ])->get()->toArray();

        $serviceBack = new ServiciosFacebook();

        // ETL Facebook recibe cada publicación de forma individual
        foreach ($publicaciones as $publicacion) {
            $serviceBack->publicacionesRedSocialPost($publicacion);
        }
    }

    /**
     * Envia una publicación de forma individual al BackEnd
     */
    public function sendToETL()
    {
        $serviceBack = null;
        $data = [];

        switch ($this->red_social_id) {
            case RedSocial::$TWITTER:
                    $serviceBack = new ServiciosTwitter();
                    // En el caso de Twitter recibe un arreglo de publicaciones
                    array_push($data, $this->toArray());
                break;
            case RedSocial::$FACEBOOK:
                    $serviceBack = new ServiciosFacebook();
                    // Para el caso de Facebook solo recibe una publicación
                    $data = $this->toArray();
                break;
        }

        $serviceBack->publicacionesRedSocialPost($data);
    }

    /**
     *
     * @param string $fecha_hora_publicacion  Fecha de publicación en formato yyyy-mm-dd hh:ii,
     *                                        ejemplo: 2017-01-02 08:58. Default null.
     * @param int $limit                      Si limit es 1, devuelve solo el elemento como objeto. Default null.
     * @param int $en_proceso                 Determina si devuelve o no las publicaciones que estan en proceso de ser publicadas. Values 0 y 1. Default null.
     * @return App\Models\Publicacion[]  $publicaciones  Para el caso de que limit es 1, no se devuelve el array, solo se devuelve el elemento encontrado
     */
    public static function getAprobadosPorPublicar($fecha_hora_publicacion = null, $limit = null, $en_proceso = null)
    {
        $query = null;

        if (empty($fecha_hora_publicacion)) {
            $query = Publicacion::where('estado', self::$APROBADO_POR_PUBLICAR);
        } else {
            $query = Publicacion::where(function($query) use ($fecha_hora_publicacion) {
                $query->where(function ($query) use ($fecha_hora_publicacion) {
                    $query->where('estado', self::$APROBADO_POR_PUBLICAR)
                    ->where('fecha_hora_publicacion', '<=', $fecha_hora_publicacion.':59');
                })
                ->orWhere(function ($query) {
                    $query->where('estado', self::$ERROR_PUBLICAR)
                        ->where('intentos_fallidos', '<', self::MAX_INTENTOS_FALLIDOS);
                });
            });
        }

        if ($en_proceso !== null) {
            $query->where('en_proceso', $en_proceso);
        }

        // Log::info($query->toSql());
        // Log::info($query->getBindings());

        if (!empty($limit)) {
            $query->limit($limit);
        }

        if ($limit == 1) {
            return $query->first();
        }

        return $query->get();
    }

    public function publicar()
    {
        switch ($this->red_social_id) {
            case RedSocial::$TWITTER:
                RedSocial::publicarTwitter($this);
                break;
        }
    }

    /**
     *
     * @return App\Models\Etl\Twitter\PublicacionEstadistica $estadisTwitter
     */
    public function estadisTwitter()
    {
        return $this->hasOne('App\Models\Etl\Twitter\PublicacionEstadistica', 'publicacion_id', 'id')
            ->whereRaw('fecha = ( SELECT MAX(fecha)
                    FROM publicacion_estadistica
                    WHERE publicacion_id = '.$this->id.' )');
    }

    /**
     * Obtiene el engagement de la publicación
     * (Favorites + 2*Replies + 3*Retweets) / Total Followers Influenciador
     * Devuelve porcentaje con 2 dígitos
     *
     * @return int
     */
    public function engagement()
    {
        $engagement = 0;
        $this->loadInteracciones();

        // dump(!empty($this->estadisTwitter) ? $this->estadisTwitter->toArray() : null);
        // dump(!empty($this->influenciador) ? $this->influenciador->toArray() : null);
        // dump(!empty($this->influenciador->perfilTwitter) ? $this->influenciador->perfilTwitter->toArray() : null);
        // dump(!empty($this->interacciones) ? $this->interacciones : null);

    // TODO: switch con red_social_id
        if (!empty($this->influenciador)) {
            if (!empty($this->influenciador->perfilTwitter)) {
                if ($this->influenciador->perfilTwitter->cantidad_seguidores > 0 && !empty($this->interacciones)) {
                    $engagement = round(
                        (
                            (
                                $this->interacciones['favorites'] +
                                2*$this->interacciones['replies'] +
                                3*$this->interacciones['retweets']
                            ) /
                            $this->influenciador->perfilTwitter->cantidad_seguidores
                        ) * 100, 2);
                }
            }
        }

        return $engagement;
    }

    /**
     * Obtiene el tooltip con la formula del engagement de la publicación
     * (Favorites + 2*Replies + 3*Retweets) / Total Followers Influenciador
     *
     * @return string
     */
    public function engagement_tooltip()
    {
        $this->loadInteracciones();

        // TODO: switch con red_social_id
        if (!empty($this->influenciador)) {
            if (!empty($this->influenciador->perfilTwitter)) {
                if ($this->influenciador->perfilTwitter->cantidad_seguidores > 0 && !empty($this->interacciones)) {
                    return '('.number_format($this->interacciones['favorites']).' Fav + '.
                         '2*'.number_format($this->interacciones['replies']).' Rep + '.
                         '3*' . number_format($this->interacciones['retweets']).' Ret) / '.
                         number_format($this->influenciador->perfilTwitter->cantidad_seguidores).' Fw)';
                }
            }
        }

        return '';
    }

    /**
     * Obtiene el alcance de la publicación
     * (Seguidores del Influencer + Total followers de los seguidores del influenciador)
     *
     * @return int
     */
    public function alcance()
    {
       $alcance = 0;

       if ($this->influenciador) {
           if ($this->influenciador->perfilTwitter) {
               $alcance += $this->influenciador->perfilTwitter->cantidad_seguidores;
           }
       }

       if ($this->estadisTwitter) {
           $alcance += $this->estadisTwitter->total_followers_rt;
       }

       return $alcance;
    }

    /*public function getDataToBackEnd()
    {
        return [
            'publicacion_id'  => $this->id_publicacion_red_social,
            'campana_id'      => $this->campana_id,
            'influenciador_id'=> $this->influenciador_id,
            'red_social_id'   => $this->red_social_id,
        ];
    }

    public function sendPublicacionToBackend()
    {
        $serviceBack = null;

        switch ($this->red_social_id) {
            case RedSocial::$TWITTER:
                    $serviceBack = new ServiciosTwitter();
                break;
            case RedSocial::$FACEBOOK:
                    $serviceBack = new ServiciosFacebook();
                break;
        }

        $serviceBack->publicacionPost($this->getDataToBackEnd());
    }*/

    /**
     * Devuelve verdadero si la publicación des de Twitter
     *
     * @return boolean
     */
    public function isTwitter()
    {
        return $this->red_social_id == RedSocial::$TWITTER;
    }

    /**
     * Devuelve verdadero si la publicación es de Facebook
     *
     * @return boolean
     */
    public function isFacebook()
    {
        return $this->red_social_id == RedSocial::$FACEBOOK;
    }

    /**
     * Devuelve verdadero si la publicación es de Instagram
     *
     * @return boolean
     */
    public function isInstagram()
    {
        return $this->red_social_id == RedSocial::$INSTAGRAM;
    }

    /**
     * Devuelve verdadero si la publicación es de Youtube
     *
     * @return boolean
     */
    public function isYoutube()
    {
        return $this->red_social_id == RedSocial::$YOUTUBE;
    }

    /**
     * Devuelve verdadero si la publicación es de Snapchat
     *
     * @return boolean
     */
    public function isSnapchat()
    {
        return $this->red_social_id == RedSocial::$SNAPCHAT;
    }

    /**
     * @return App\Models\Etl\Twitter\Reply[] $replies
     */
    public function replies()
    {
        return $this->hasMany('App\Models\Etl\Twitter\Reply', 'publicacion_id', 'id_publicacion_red_social');
    }

    /**
     * Obtiene todas las palabras clave del listado de Replies
     *
     * @return array
     */
    public function WordCloudFromReplies()
    {
        $textosReplies = \DB::select('SELECT MID(texto, LOCATE(" ", texto), LENGTH(texto)) AS texto
            FROM reply WHERE publicacion_id =  "'.$this->id_publicacion_red_social.'"');

        $joinTextosReplies = collect($textosReplies)->implode('texto', '');

        return WordCloudGetter::build($joinTextosReplies);
    }

    /**
     * Obtiene las categorias de influenciadores del listado de Replies
     *
     * @return array
     */
    public function getCategoriasInfluencerReplies()
    {
        return \DB::select('SELECT tipo, count(*) AS total FROM
            (
                SELECT
                    (CASE
                        WHEN followers_count=0 THEN "Inactivo"
                        WHEN followers_count <= 10000 THEN "Newbie <10K"
                        WHEN followers_count > 10000   AND followers_count <= 100000  THEN "Amateur 10K-100K"
                        WHEN followers_count > 100000  AND followers_count <= 1000000 THEN "Pro 100K-1M"
                        WHEN followers_count > 1000000 AND followers_count <= 5000000 THEN "Expert 1M-5M"
                        WHEN followers_count > 5000000 THEN "Star >5M"
                    END) AS tipo,
                    followers_count
                FROM account
                WHERE id in (
                    SELECT account_id FROM reply WHERE publicacion_id = "'.$this->id_publicacion_red_social.'"
                )
            ) AS tipo_influenciador
            GROUP BY tipo');
    }

    public function getArrayCategoriasInfluencers(){
        $categorias = [
            "labels" => [
                "Newbie <10K",
                "Amateur 10K-100K",
                "Pro 100K-1M",
                "Expert 1M-5M",
                "Star >5M"
            ],
            "data" => [
                ["name"=>"Newbie <10K","value"=>0],
                ["name"=>"Amateur 10K-100K","value"=>0],
                ["name"=>"Pro 100K-1M","value"=>0],
                ["name"=>"Expert 1M-5M","value"=>0],
                ["name"=>"Star >5M","value"=>0],
            ]
        ];

        $catsdb = self::getCategoriasInfluencerReplies();


        foreach($catsdb as $cat){

            for($i=0;$i<count($categorias["data"]);$i++){

                if ($categorias["data"][$i]["name"] == $cat->tipo){
                    $categorias["data"][$i]["value"] = $cat->total;
                }
            }
        }



        return $categorias;

    }

    /**
     * Carga las cantidades de interacciones de la publicacion
     *
     * @return array
     */
    public function loadInteracciones()
    {
        if (empty($this->interacciones)) {
            $this->interacciones = \DB::select('SELECT * FROM (
                SELECT
                    publicacion.id,
                    publicacion.id_publicacion_red_social,
                    publicacion_estadistica.retweets,
                    publicacion_estadistica.favorites,
                    (
                        SELECT COUNT(*)
                        FROM reply
                        WHERE reply.publicacion_id = publicacion.id_publicacion_red_social
                    ) AS replies,
                    publicacion_estadistica.total_followers_rt,
                    publicacion_estadistica.fecha
                FROM
                    publicacion
                INNER JOIN publicacion_estadistica ON
                    publicacion.id = publicacion_estadistica.publicacion_id
                WHERE
                    publicacion.id = '.$this->id.'
                ORDER BY
                    publicacion.id DESC, publicacion_estadistica.fecha DESC
            ) AS result GROUP BY id');

            if (!empty($this->interacciones)) {
                $this->interacciones = (array) $this->interacciones[0];

                $this->interacciones['total'] = $this->interacciones['retweets'] + $this->interacciones['favorites'] + $this->interacciones['replies'];
            }
        }

        return $this->interacciones;
    }


    /**
     * Obtiene los usuarios que dieron reply a la publicación
     *
     * @return array
     */
    public function getUsersReply()
    {
        $ids_apple = ['878391191267667969','884953757007663104','879787643638341632','883483819868692480', '878311450682327041'];

        return \DB::select("SELECT a.id, a.name, a.screen_name, a.description, a.followers_count, a.friends_count, a.statuses_count, a.profile_image, a.gender, a.country, 'img/campana/twitter.png' AS icon, r.texto AS post FROM account AS a INNER JOIN reply AS r ON a.id = r.account_id WHERE r.publicacion_id = ? ORDER BY a.followers_count, a.name DESC", [$this->id_publicacion_red_social]);
    }

    /**
     * Obtiene los usuarios que dieron reply a la publicación
     *
     * @return array
     */
    public function getUsersRetweet()
    {
        if ($this->campana_id != 86) {
            return [];
        }

        switch ($this->id_publicacion_red_social) {
            case '878391191267667969':
                    return [
                        [ 'name' => 'Goycita (Oficial) ','screen_name' => 'Adrys151Adriana', 'followers_count' => '934','profile_image' => 'https://pbs.twimg.com/profile_images/880177248397033476/iIx7FbTg_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'MacStoreMx','screen_name' => 'MacStoreMx', 'followers_count' => '6366','profile_image' => 'https://pbs.twimg.com/profile_images/648634996102574080/2YVxqsoL_400x400.png', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Faby Nava Hernández','screen_name' => 'traviesa77', 'followers_count' => '1077','profile_image' => 'https://pbs.twimg.com/profile_images/885278628178776064/RmfYW8eV_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                    ];
                break;
            case '884953757007663104':
                    return [
                        [ 'name' => 'Soy Mapache','screen_name' => 'mapachemaster01', 'followers_count' => '59','profile_image' => 'https://pbs.twimg.com/profile_images/826437276116873216/_cIU1ecU_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'jesus stark','screen_name' => 'jesusstark72', 'followers_count' => '198','profile_image' => 'https://pbs.twimg.com/profile_images/772917060728860673/QqjHuUyL_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Jose Rodolfo Aguilar','screen_name' => 'jraguilaro', 'followers_count' => '41','profile_image' => 'https://pbs.twimg.com/profile_images/834995294295187456/ZFbg9p2Y_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'huggo fonse','screen_name' => 'Hugo_voltage', 'followers_count' => '767','profile_image' => 'https://pbs.twimg.com/profile_images/861025021304745984/jkqfY44A_400x400.jpg', 'icon' => url('img/campana/twitter.png')]
                    ];
                break;
            case '879787643638341632':
                    return [
                        [ 'name' => 'Ixzel Ferraez','screen_name' => 'Ixz_Ferraez', 'followers_count' => '302','profile_image' => 'https://pbs.twimg.com/profile_images/866745671940014081/WzhKhaIN_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Viviana Sánchez','screen_name' => 'Vivi_SGarcia', 'followers_count' => '202','profile_image' => 'https://pbs.twimg.com/profile_images/880232524617850880/1uU0hW0S_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Patry Zavala','screen_name' => 'PatryZavala', 'followers_count' => '413','profile_image' => 'https://pbs.twimg.com/profile_images/763933335722655745/ZTJGBz2C_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'La silla reñoña','screen_name' => 'Lasillarenona', 'followers_count' => '88','profile_image' => 'https://pbs.twimg.com/profile_images/887152550205562881/zsM8KFn6_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Vale','screen_name' => 'valeriagomz', 'followers_count' => '1903','profile_image' => 'https://pbs.twimg.com/profile_images/880144909935681536/GAGXM_QQ_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Nat SilvaR','screen_name' => 'misskaramelo', 'followers_count' => '134','profile_image' => 'https://pbs.twimg.com/profile_images/851983758626607104/XJaU2_lZ_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Peachy','screen_name' => 'nathaliehuergo', 'followers_count' => '300','profile_image' => 'https://pbs.twimg.com/profile_images/888404905882513409/NVS4hkht_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'phantøm  ','screen_name' => 'Aprilubius', 'followers_count' => '1649','profile_image' => 'https://pbs.twimg.com/profile_images/879825500818079744/E4w2lq7x_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Jackie boy','screen_name' => 'pablolan3', 'followers_count' => '480','profile_image' => 'https://pbs.twimg.com/profile_images/885748279492972544/i0Q16mKW_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Esparza','screen_name' => 'Dana0317', 'followers_count' => '319','profile_image' => 'https://pbs.twimg.com/profile_images/836118056304799745/POz7Jbc6.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'glox avila','screen_name' => 'glox06', 'followers_count' => '28','profile_image' => 'https://pbs.twimg.com/profile_images/749266265483587585/lihIUxtY.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'NV','screen_name' => 'NarvezVzquez1', 'followers_count' => '0','profile_image' => 'https://abs.twimg.com/sticky/default_profile_images/default_profile.png', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Oscar Ramírez','screen_name' => 'MoscaSka', 'followers_count' => '28','profile_image' => 'https://pbs.twimg.com/profile_images/872323691811663878/wzhivg2X.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'k','screen_name' => 'gbsxo_', 'followers_count' => '1035','profile_image' => 'https://pbs.twimg.com/profile_images/888238936438460416/Z-VBLsEI.jpg', 'icon' => url('img/campana/twitter.png')],
                    ];
                break;
            case '883483819868692480':
                    return [
                        [ 'name' => 'eGlow','screen_name' => 'Eglow_Latam', 'followers_count' => '232','profile_image' => 'https://pbs.twimg.com/profile_images/722470064729882624/7VB2L8Dg.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'RonyR','screen_name' => 'Ronyrosenberg9R', 'followers_count' => '90','profile_image' => 'https://pbs.twimg.com/profile_images/857799129539506179/93zc19Gw.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Marco Ramírez','screen_name' => 'Marcoaramirez8', 'followers_count' => '88','profile_image' => 'https://pbs.twimg.com/profile_images/870375176529104897/rr6AAUDN.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'María Coria Galindo','screen_name' => 'Goodbye_bye_bye', 'followers_count' => '428','profile_image' => 'https://pbs.twimg.com/profile_images/866011807076536320/eMNcYwcw.jpg', 'icon' => url('img/campana/twitter.png')]
                    ];
                break;
            case '883062741782679553':
                    return [
                        [ 'name' => 'eGlow','screen_name' => 'Eglow_Latam', 'followers_count' => '232','profile_image' => 'https://pbs.twimg.com/profile_images/722470064729882624/7VB2L8Dg.jpg', 'icon' => url('img/campana/twitter.png')],
                    ];
                break;
            case '885224355642437632':
                    return [
                        [ 'name' => 'LaNefasta','screen_name' => 'lanefasta', 'followers_count' => '235','profile_image' => 'https://pbs.twimg.com/profile_images/2281658156/6ki4wplv61ikm34ieouj.jpeg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'eGlow','screen_name' => 'Eglow_Latam', 'followers_count' => '232','profile_image' => 'https://pbs.twimg.com/profile_images/722470064729882624/7VB2L8Dg.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'BLONDIE','screen_name' => 'Camacri', 'followers_count' => '975','profile_image' => 'https://pbs.twimg.com/profile_images/491077197201829888/fQV2RA3o.jpeg', 'icon' => url('img/campana/twitter.png')],
                    ];
                break;
            case '878311450682327041':
                    return [
                        [ 'name' => 'Rive ','screen_name' => 'rive_17', 'followers_count' => '3','profile_image' => 'https://pbs.twimg.com/profile_images/871541989019238400/EGJ65OWz_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Gerardo Gómez ','screen_name' => '_GeraGomez', 'followers_count' => '78','profile_image' => 'https://pbs.twimg.com/profile_images/420568785641160705/2YLMaO9g_400x400.jpeg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Arsenio Sordo Aleman ','screen_name' => 'ArseSordo', 'followers_count' => '15','profile_image' => 'https://pbs.twimg.com/profile_images/1170920834/Familia_400x400.JPG', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Jorge Alday','screen_name' => 'jorgealday1953', 'followers_count' => '10','profile_image' => 'https://pbs.twimg.com/profile_images/875936784370290689/1MiNLSKZ_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                        [ 'name' => 'Sergio Sala Cota ','screen_name' => 'SalaCota', 'followers_count' => '9','profile_image' => 'https://pbs.twimg.com/profile_images/619714497062637572/w080eXEj_400x400.jpg', 'icon' => url('img/campana/twitter.png')],
                    ];
                break;
        }
    }

    /**
     * adds retweeters
     *
     * @method addRetweeters
     *
     * @param array $retweeters
     */
    public function addRetweeters(array $retweeters)
    {
        $this->retweeters()->sync($retweeters);
    }
}