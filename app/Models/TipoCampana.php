<?php

namespace App\Models;

use App\Helpers\UploadFileTrait;

class TipoCampana extends BaseModel
{
    use UploadFileTrait;

    protected $table = 'tipo_campana';

    public $fileName = 'icono';

    public $icono = '';

    protected $appends = ['icono'];

    /**
     *
     * @return App\Models\Campana[] $campanas
     */
    public function campanas()
    {
        return $this->hasMany('App\Models\Campana', 'tipo_campana_id', 'id');
    }

    /**
     * Obtiene la ruta del icono para el select
     *
     * @return string
     */
    public function getIconoAttribute()
    {
        $this->icono = $this->getFile();

        if (empty($this->icono)) {
            $this->icono = 'http://placehold.it/60x60';
        }

        return $this->icono;
    }

    public function getBaseUploadFolder()
    {
        return 'img' . DIRECTORY_SEPARATOR . 'tipo_campana';
    }
}
