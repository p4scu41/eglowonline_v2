<?php
namespace App\Models;

use GuzzleHttp\Client as GuzzleHttpClient;
use Exception;

class ServiciosTwitter extends BaseServicios
{
    public function __construct()
    {
        parent::__construct(config('app.url_servicios_twitter'));
    }
}
