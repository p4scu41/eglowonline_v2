<?php

namespace App\Http\Middleware;

use App\Support\ArraySupport;
use App\Support\StringSupport;
use Illuminate\Foundation\Http\Middleware\TransformsRequest;

/**
 * Middleware que limpia los valores enviados por el usuario,
 * utiliza las funciones de StringSupport::stripTags y ArraySupport::stripTags
 *
 * @package App\Http\Middleware
 * @author  Pascual <pascual@importare.mx>
 */
class SanitazeInputs extends TransformsRequest
{
    /**
     * The names of the attributes that should not be sanitaze.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    /**
     * Transform the given value.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function transform($key, $value)
    {
        if (in_array($key, $this->except)) {
            return $value;
        }

        return is_string($value) ? StringSupport::stripTags($value) :
            (is_array($value) ? ArraySupport::stripTags($value) : $value);
    }
}
