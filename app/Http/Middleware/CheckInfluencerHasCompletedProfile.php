<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

/**
 * this middleware checks if the influencer has completed to fill out his/her profile
 *
 * @package App
 * @subpackage Http\Middleware
 * @category Middleware
 * @author Gerardo Adrián Gómez Ruiz <gerardo@eglow.mx>
 */
class CheckInfluencerHasCompletedProfile
{
    /**
     * Handle an incoming request.
     * If the authenticated user is influencer & he/she hasn't completed his/her profile
     * redirects to profile view in order to fill it out
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->isCelebridad()) {
            if (!$user->influenciador->perfil_completado) {
                return redirect('usuarios/perfil')
                    ->with('message', 'No puedes realizar ninguna acción hasta completar tus datos.')
                    ->with('type', 'danger');
            }
        }

        return $next($request);
    }
}
