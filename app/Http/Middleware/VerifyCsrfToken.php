<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Se actualiza el método para crear la coockie del token
 * para establecer la bandera httponly a true
 *
 * @package App\Http\Middleware
 * @author  Pascual <pascual@importare.mx>
 */
class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'logout',
        'api/*',
    ];

    /**
     * Add the CSRF token to the response cookies.
     * Overrided to set the flag httponly to true
     *
     * @param \Illuminate\Http\Request                   $request  Request
     * @param \Symfony\Component\HttpFoundation\Response $response Response
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function addCookieToResponse($request, $response)
    {
        $config = config('session');

        $response->headers->setCookie(
            new Cookie(
                'XSRF-TOKEN',
                $request->session()->token(),
                Carbon::now()->getTimestamp() + 60 * $config['lifetime'],
                $config['path'],
                $config['domain'],
                $config['secure'],
                true // httponly
            )
        );

        return $response;
    }
}
