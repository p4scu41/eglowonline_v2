<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\ServiciosReceivedBackendLog;

/**
 * class checks api token was sent through guzzle client on backend
 *
 * @package App
 * @subpackage Http\Middleware
 * @category Middleware
 * @author Gerardo Adrián Gómez Ruiz <gerardo@eglow.mx>
 */
class ValidateApiToken
{
    /**
     * Handle an incoming request. Checks token exists on the header and compares it against
     * this app's token. Saves log in DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('X-CSRF-TOKEN');

        if ($token !== env('API_BACKEND_KEY')) {
            $data = [
                'expected' => env('API_BACKEND_KEY'),
                'sent'     => $request->header('X-CSRF-TOKEN'),
                'data'     => $request->all()
            ];

            // log and block access
            $requestLog = [
                'request'     => $request->method(),
                'uri'         => $request->fullUrl(),
                'start'       => date('Y-m-d H:i:s'),
                'end'         => date('Y-m-d H:i:s'),
                'params'      => serialize($data),
                'response'    => 'Token was not sent or is invalid.',
                'status_code' => '500',
                'status_text' => 'Fail'
            ];

            ServiciosReceivedBackendLog::create($requestLog);
            return response()->json($requestLog);
        }

        return $next($request);
    }
}
