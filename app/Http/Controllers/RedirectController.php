<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        if ($user->isAdministrador()) {
            return redirect('logeventos');
        } elseif ($user->isAgencia()) {
            return redirect('influenciadores/catalogo');
        } elseif ($user->isCelebridad()) {
            return redirect('influenciadores/miscampanas');
        } elseif ($user->isProducto()) {
            return redirect('influenciadores/catalogo');
        } elseif ($user->isMarca()) {
            return redirect('influenciadores/catalogo');
        }
        else {
            echo 'Tipo de Usuario no soportado';
        }
    }
}
