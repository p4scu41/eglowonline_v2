<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Models\User;
use App\Models\Influenciador;
use App\Models\PerfilTwitter;
use Illuminate\Support\Facades\Log;
use Exception;
use Twitter;
use File;
use DB;

class VincularController extends Controller
{
    /**
     * process login through twitter app
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function twitterRedirectToProvider(Request $request)
    {
        try {
            Twitter::reconfig(['token' => '', 'secret' => '']);
            $requestToken = Twitter::getRequestToken(url('influenciadores/vincular/twittercallback'));

            if (isset($requestToken['oauth_token_secret'])) {
                $url = Twitter::getAuthorizeURL($requestToken);

                $request->session()->put('oauth_request_token', $requestToken['oauth_token']);
                $request->session()->put('oauth_request_token_secret', $requestToken['oauth_token_secret']);
                $request->session()->put('url_previous', \URL::previous());

                return redirect($url);
            }

            return back()->withErrors(['status' => 'Error al establecer la comunicación con Twitter.']);
        } catch (Exception $e) {
            return back()
                ->withErrors(['status' => 'Error al establecer la comunicación con Twitter.'.
                        (config('app.debug') ?
                            'Line ' . $e->getLine().' '.$e->getMessage().' '. Helper::errorsToString(Twitter::logs(), "\n") : ''
                        )
                    ]);
        }
    }

    public function twitterHandleProviderCallback(Request $request)
    {
        try {
            if ($request->session()->has('oauth_request_token')) {
                DB::beginTransaction();

                Twitter::reconfig([
                    'token'  => $request->session()->get('oauth_request_token'),
                    'secret' => $request->session()->get('oauth_request_token_secret'),
                ]);

                $accessToken = Twitter::getAccessToken($request->get('oauth_verifier'));

                if (!isset($accessToken['oauth_token_secret'])) {
                    return redirect($request->session()->get('url_previous'))
                        ->withErrors(['status' => 'Error al iniciar sesión con Twitter. No se puede obtener el token.']);
                }

                $credentials = Twitter::getCredentials(['include_email' => 'true']);

                $perfilTwitter = null;
                $usuario       = null;
                $influenciador = null;
                $isNew         = false; // Bandera para determinar si un usuaria al loguear se registrará por primera vez

                if (is_object($credentials) && !isset($credentials->error)) {
                    $perfilTwitter = PerfilTwitter::where('screen_name', '@'.$accessToken['screen_name'])->first();

                    // Si el usuario de Twitter no existe en la Base de Datos
                    if (empty($perfilTwitter)) {
                        $influenciador = Auth::user()->influenciador;

                        // registramos la cuenta de Twitter
                        $perfilTwitter = new PerfilTwitter;

                        $perfilTwitter->fill([
                            'influenciador_id'    => $influenciador->id,
                            'twitter_id'          => $accessToken['user_id'],
                            'profile_image_url'   => $credentials->profile_image_url_https,
                            'cantidad_seguidores' => $credentials->followers_count,
                            'screen_name'         => '@' . $accessToken['screen_name'],
                            'oauth_token'         => $accessToken['oauth_token'],
                            'oauth_token_secret'  => $accessToken['oauth_token_secret'],
                        ]);

                        if (!$perfilTwitter->isValid(PerfilTwitter::DO_CREATE)) {
                            \Log::error(json_encode($perfilTwitter->errors));

                            return redirect($request->session()->get('url_previous'))
                                ->withErrors(['status', 'Error al registrar el usuario de Twitter en el sistema. '.
                                    Helper::errorsToString($perfilTwitter->errors)]);
                        }

                        // insert
                        $perfilTwitter->save();
                    } else {
                        // Actualizamos los datos con la información devuelta por Twitter
                        $perfilTwitter->fill([
                            'twitter_id'          => $accessToken['user_id'], //  $credentials->id_str,
                            'profile_image_url'   => $credentials->profile_image_url_https,
                            'cantidad_seguidores' => $credentials->followers_count,
                            'oauth_token'         => $accessToken['oauth_token'],
                            'oauth_token_secret'  => $accessToken['oauth_token_secret'],
                        ]);

                        if (!$perfilTwitter->isValid(PerfilTwitter::DO_UPDATE)) {
                            \Log::error(json_encode($perfilTwitter->errors));

                            return redirect($request->session()->get('url_previous'))
                                ->withErrors(['status', 'Error al actualizar los datos de la cuenta de Twitter en el sistema. '.
                                    Helper::errorsToString($perfilTwitter->errors)]);
                        }

                        // update
                        $perfilTwitter->save();

                        $usuario = $perfilTwitter->influenciador->usuario;
                    }

                    Auth::user()->influenciador->sendInfluenciadorToBackend('put', 'twitter');

                    DB::commit();

                    $request->session()->flash('response_info', [
                        'error'   => false,
                        'type'    => 'success',
                        'show'    => true,
                        'message' => 'Datos guardados exitosamente.',
                    ]);

                    return redirect($request->session()->get('url_previous'));
                }

                return redirect($request->session()->get('url_previous'))
                    ->withErrors(['status' => 'Error al iniciar sesión con Twitter.']);
            }
        } catch (Exception $e) {
            DB::rollBack();

            return redirect($request->session()->get('url_previous'))
                ->withErrors(['status' => 'Error al iniciar sesión con Twitter. '.
                        (config('app.debug') ?
                            'Line ' . $e->getLine().' '.$e->getMessage().' '. Helper::errorsToString(Twitter::logs(), "\n") : ''
                        )
                    ]);
        }

        return redirect($request->session()->get('url_previous'))
            ->withErrors('status', 'Error al establecer la comunicación con Twitter.');
    }

    public function facebookRedirectToProvider(Request $request)
    {
        return back();
    }

    public function instagramRedirectToProvider(Request $request)
    {
        return back();
    }

    public function youtubeRedirectToProvider(Request $request)
    {
        return back();
    }

    public function snapchatRedirectToProvider(Request $request)
    {
        return back();
    }
}
