<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\BaseWebController;
use App\Models\Campana;
use App\Models\Genero;
use App\Models\Industria;
use App\Models\Influenciador;
use App\Models\RangoEtario;
use App\Models\SiNo;
use App\Models\TipoCampana;
use App\Models\TipoInfluenciador;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogeventoController extends BaseWebController
{
    public $modelClass = 'LogEvento';
    public $resource   = 'logeventos';
    public $title_sin  = 'Evento';
    public $title_plu  = 'Eventos';
    public $order_by   = 'created_at';

    public function onBeforeIndex(Request $request)
    {
        parent::onBeforeIndex($request);

        if ($request->has('filter.tipo')) {
            $this->filter[] = ['tipo', '=', $request->input('filter.tipo')];
        }

        if ($request->has('filter.fecha_desde')) {
            $this->filter[] = [
                'created_at',
                '>=',
                Carbon::createFromFormat(
                    'd-m-Y',
                    Helper::stripTags($request->input('filter.fecha_desde'))
                )->format('Y-m-d')
            ];
        }

        if ($request->has('filter.fecha_hasta')) {
            $this->filter[] = [
                'created_at',
                '<=',
                Carbon::createFromFormat(
                    'd-m-Y',
                    Helper::stripTags($request->input('filter.fecha_hasta'))
                )->format('Y-m-d')
            ];
        }
    }

    public function index(Request $request)
    {
        // Solo obtiene todos los eventos generados o dirigidos por la Agencia
        if (Auth::user()->isAgencia()) {
            $this->filter[] = ['created_by', '=', Auth::user()->id, true];
            $this->filter[] = ['usuario_id', '=', Auth::user()->id, true];
        }

        $request->merge([
            'order_direction' => 'DESC',
        ]);

        return parent::index($request);
    }
}
