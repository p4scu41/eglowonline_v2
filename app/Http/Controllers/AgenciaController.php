<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseWebController;
use App\Helpers\Helper;
use App\Models\Agencia;
use App\Models\User;
use App\Models\TipoUsuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use DB;

class AgenciaController extends BaseWebController
{
    public $modelClass = 'Agencia';
    public $resource   = 'agencias';
    public $title_sin  = 'Agencia';
    public $title_plu  = 'Agencias';
    public $order_by   = 'nombre';

    public function store(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        $this->validPolicy('create', $modelCls);

        try {
            DB::beginTransaction();
            $this->modelObj = new $modelCls;
            $this->modelObj->fill(Helper::stripTagsArray($request->input()));

            if ($this->modelObj->isValid($modelCls::DO_CREATE)) {
                $this->modelObj->save();

                if ($request->hasFile('logotipo')) {
                    $this->modelObj->uploadFile($request->file('logotipo'));
                }

                DB::commit();

                $this->response_info['show']    = true;
                $this->response_info['message'] = 'Datos guardados exitosamente.';
                $this->response_info['type']    = 'success';
            } else {
                return redirect($this->resource . '/create')
                    ->withErrors($this->modelObj->validator)
                    ->withInput();
            }
        } catch (Exception $e) {
            DB::rollBack();

            return redirect($this->resource . '/create')
                ->withErrors([Helper::getMessageIfDebug($e)])
                ->withInput();
        }

        $request->session()->flash('response_info', $this->response_info);

        return redirect($this->resource);
    }

    public function update(Request $request, $id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        try {
            DB::beginTransaction();

            if (empty($this->modelObj)) {
                throw new Exception('Datos no disponibles.', 404);
            }

            $this->validPolicy('update', $this->modelObj);

            $this->modelObj->fill(Helper::stripTagsArray($request->input()));

            if ($this->modelObj->isValid($modelCls::DO_UPDATE)) {
                $this->modelObj->save();

                if ($request->hasFile('logotipo')) {
                    $this->modelObj->uploadFile($request->file('logotipo'));
                }

                DB::commit();

                $this->response_info['show']    = true;
                $this->response_info['message'] = 'Datos actualizados exitosamente.';
                $this->response_info['type']    = 'success';
            } else {
                return redirect($this->resource . '/' . $this->modelObj->id . '/edit')
                    ->withErrors($this->modelObj->validator)
                    ->withInput();
            }
        } catch (Exception $e) {
            DB::rollBack();
            return redirect($this->resource . '/' . $this->modelObj->id . '/edit')
                ->withErrors([Helper::getMessageIfDebug($e)])
                ->withInput();
        }

        $request->session()->flash('response_info', $this->response_info);

        return redirect($this->resource);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view($this->resource.'.home', [
            'title' => 'Agencia',
        ]);
    }

    /**
     * Obtiene el logotipo de la marca
     *
     * @param  lluminate\Http\Request $request
     * @return json
     */
    public function getLogotipo(Request $request)
    {
        $agencia = Agencia::where('id', $request->input('id'))->first();

        return response()->json([
            'logotipo' => $agencia ? asset($agencia->getLogotipo()) : '',
        ]);
    }
}
