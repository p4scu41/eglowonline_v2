<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\BaseWebController;
use App\Models\BusquedaAvanzadaInfluenciador;
use App\Models\Campana;
use App\Models\EntornoXInfluenciador;
use App\Models\Entorno;
use App\Models\Genero;
use App\Models\Industria;
use App\Models\Influenciador;
use App\Models\InfluenciadorClic;
use App\Models\InfluenciadorXPalabraClave;
use App\Models\PublicacionXInfluenciadorXCampana;
use App\Models\LogEvento;
use App\Models\Pais;
use App\Models\PalabraClave;
use App\Models\RangoEtario;
use App\Models\RedSocial;
use App\Models\SiNo;
use App\Models\TipoCampana;
use App\Models\TipoInfluenciador;
use App\Models\TipoRelacionPalabraClave;
use App\Models\TipoUsuario;
use App\Models\User;
use App\Models\Etl\Twitter\EstadisPerfil;
use App\Notifications\RegistroUsuarioNotification;
use App\Notifications\InfluencerAcceptedPublicationProposals;
use DB;
use Exception;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Storage;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

class InfluenciadorController extends BaseWebController
{
    public $modelClass = 'Influenciador';
    public $resource   = 'influenciadores';
    public $title_sin  = 'Influenciador';
    public $title_plu  = 'Influenciadores';
    public $order_by   = 'nombre';

    public $sessionId;

    public function onBeforeIndex(Request $request)
    {
        parent::onBeforeIndex($request);

        // $busquedaAvanzada = BusquedaAvanzadaInfluenciador::where('session_id', $this->sessionId)
        //     ->where('method', 'GET');
        // dd($busquedaAvanzada->getBindings());
        $busquedaAvanzada = BusquedaAvanzadaInfluenciador::where('session_id', $this->sessionId)
            ->where('method', 'GET')
            ->first();

        if (!empty($busquedaAvanzada)) {
            $this->filter             = json_decode($busquedaAvanzada->params);
            $this->filter['busqueda'] = false;
        } else {
            if ($request->has('filter.nombre')) {
                $this->filter[] = ['nombre', 'like', '%'.Helper::stripTags($request->input('filter.nombre')).'%'];
            }

            if ($request->has('filter.industria_id')) {
                $this->filter[] = ['industria_id', '=', $request->input('filter.industria_id')];
            }

            if ($request->has('filter.rango_etario_id')) {
                $this->filter[] = ['rango_etario_id', '=', $request->input('filter.rango_etario_id')];
            }

            if ($request->has('filter.pais_id')) {
                $this->filter[] = ['pais_id', '=', $request->input('filter.pais_id')];
            }

            if ($request->has('filter.genero')) {
                $this->filter[] = ['genero', '=', $request->input('filter.genero')];
            }

            if ($request->has('filter.tipo_influenciador_id')) {
                $this->filter[] = ['tipo_influenciador_id', '=', $request->input('filter.tipo_influenciador_id')];
            }

            $this->filter['busqueda'] = count($this->filter) > 0;
        }

        $this->order_by = 'nombre';
    }

    public function onAfterCreate($request = null, $data = null)
    {
        $this->catalogs = Influenciador::getCatalogsToSelect();
    }

    public function onAfterEdit($request = null, $data = null)
    {
        $this->catalogs = Influenciador::getCatalogsToSelect();
    }

    public function onAfterShow($request = null, $data = null)
    {
        $entornos = null;

        // Si el usuario no es Administrador
        if (!Auth::user()->isAdministrador()) {
            // Detectamos el entorno
            $entornos = Auth::user()->getEntornos();
        }

        if (!Auth::user()->isAdministrador() && empty($entornos)) {
            $this->response_info['error']   = true;
            $this->response_info['type']    = 'danger';
            $this->response_info['message'] = 'El usuario no esta asociado a un Entorno.';
            $this->modelObj = null;
        }

        // Limita los resultados de las busquedas a los influenciadores que
        // pertenecen al entorno del usuario logueado
        if (!empty($entornos)) {
            if (!in_array($this->modelObj->id, Influenciador::getIdsInflueRelatedByEntorno($entornos))) {
                $this->response_info['error']   = true;
                $this->response_info['type']    = 'danger';
                $this->response_info['message'] = 'Influenciador no disponible.';
                $this->modelObj = null;
            }
        }

        if (!empty($this->modelObj)) {
            $this->catalogs["precio_publicacion"] = [
                [
                    'red'    => 'twitter',
                    'precio' => $this->modelObj->perfilTwitter ? $this->modelObj->perfilTwitter->precio_publicacion : 0,
                ],
            ];
        }
    }

    public function store(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $redirect = !Auth::user()->isCelebridad() ? true : false;

        $this->validPolicy('create', $modelCls);

        try {
            DB::beginTransaction();

            $redesSociales = $request->input('redes_sociales');

            foreach ($redesSociales as $redSocial) {
                switch($redSocial['id']) {
                    case RedSocial::$TWITTER:
                            if (!empty($redSocial['perfil']['screen_name'])) {
                                if ( !Helper::existsTwitter(str_replace('@', '', $redSocial['perfil']['screen_name'])) ) {
                                    throw new Exception('La cuenta de Twitter NO existe.', 422);
                                }
                            }
                        break;
                }
            }

            $this->modelObj = new $modelCls;

            // Primero guardamos los datos del usuario
            $inputUsuario             = Helper::stripTagsArray($request->input('usuario'));
            $inputUsuario['nombre']   = Helper::stripTags($request->input('nombre'));
            $inputUsuario['password'] = Helper::passwordRandom();

            $usuario = $this->modelObj->saveUsuario($inputUsuario);

            // Después procesamos los datos del influenciador
            $this->modelObj->usuario_id = $usuario->id;

            $this->modelObj->fill(Helper::stripTagsArray(
                $request->except('usuario', 'palabras_clave', 'palabras_clave_otros')
            ));

            if (!$this->modelObj->isValid($modelCls::DO_CREATE)) {
                throw new Exception('Error al procesar los datos. ' .
                    Helper::errorsToString($this->modelObj->errors), 422);
            }

            // marcar que ya completó el perfil
            $this->modelObj->perfil_completado = true;

            $this->modelObj->save();
            $this->modelObj->savePalabrasClave($request->input('palabras_clave'));
            $this->modelObj->savePalabrasClaveOtros($request->input('palabras_clave_otros'));
            $this->modelObj->saveRedesSociales($request->input('redes_sociales'));
            $this->modelObj->saveFactura($request->input('factura', []));
            $this->modelObj->saveProfesiones($request->input('profesiones'));

            if (!empty($this->modelObj->perfilTwitter)) {
                $this->modelObj->sendInfluenciadorToBackend('post', 'twitter', true);
            }

            if (!empty($this->modelObj->perfilFacebook)) {
                $this->modelObj->sendInfluenciadorToBackend('post', 'facebook', true);
            }

            // Por defecto se asocia el influenciador al entorno 1 (eGlow)
            EntornoXInfluenciador::create([
                'entorno_id' => Entorno::EGLOW,
                'influenciador_id' => $this->modelObj->id,
            ]);

            if (Auth::user()->isAgencia()) {
                if (Auth::user()->agencia->entorno_id != Entorno::EGLOW) {
                    EntornoXInfluenciador::create([
                        'entorno_id' => Auth::user()->agencia->entorno_id,
                        'influenciador_id' => $this->modelObj->id,
                    ]);
                }
            }

            LogEvento::saveRegistroUsuario($this->modelObj->id);

            $usuario->notify(new RegistroUsuarioNotification($inputUsuario));

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        $this->onAfterStore($request);

        return response()->jsonSuccess(['data' => ['redirect' => $redirect]]);
    }

    public function update(Request $request, $id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $redirect = Auth::user()->isAdministrador() ? true : false;
        // Revisa si con anterioridad ya tiene guardado un perfil de facebook
        $existFacebook = !empty($this->modelObj->perfilFacebook);

        try {
            DB::beginTransaction();

            if (empty($this->modelObj)) {
                throw new Exception('Datos no disponibles.', 404);
            }

            $this->validPolicy('update', $this->modelObj);

            $redesSociales = $request->input('redes_sociales');

            foreach ($redesSociales as $redSocial) {
                switch($redSocial['id']) {
                    case RedSocial::$TWITTER:
                            if ( !Helper::existsTwitter(str_replace('@', '', $redSocial['perfil']['screen_name'])) ) {
                                throw new Exception('Error al procesar los datos: La cuenta de Twitter NO existe.', 422);
                            }
                        break;
                }
            }

            // Primero guardamos los datos del usuario
            $inputUsuario           = Helper::stripTagsArray($request->input('usuario'));
            $inputUsuario['nombre'] = Helper::stripTags($request->input('nombre'));

            $usuario = $this->modelObj->saveUsuario($inputUsuario, true);

            // Después procesamos los datos del influenciador
            $this->modelObj->fill(Helper::stripTagsArray(
                $request->except('usuario', 'palabras_clave', 'palabras_clave_otros')
            ));

            if ($this->modelObj->isValid($modelCls::DO_UPDATE)) {
                throw new Exception('Error al procesar los datos. ' .
                    Helper::errorsToString($this->modelObj->errors), 422);
            }

            // marcar que ya completó el perfil
            if (!$this->modelObj->perfil_completado) {
                $this->modelObj->perfil_completado = true;
            }

            $this->modelObj->save();
            $this->modelObj->clearPalabrasClave();
            $this->modelObj->savePalabrasClave($request->input('palabras_clave'));
            $this->modelObj->savePalabrasClaveOtros($request->input('palabras_clave_otros'));
            $this->modelObj->saveRedesSociales(
                $request->input('redes_sociales'),
                true,
                $request->input('redes_sociales_to_delete', [])
            );
            $this->modelObj->saveProfesiones($request->input('profesiones'));
            // facturación modo update
            $this->modelObj->saveFactura($request->input('factura'), true);

            if (!empty($this->modelObj->perfilTwitter)) {
                $this->modelObj->sendInfluenciadorToBackend('put', 'twitter');
            }

            if (!empty($this->modelObj->perfilFacebook)) {
                if ($existFacebook) { // Si ya existe un perfil de Facebook, lo actualizamos en el ETL
                    $this->modelObj->sendInfluenciadorToBackend('put', 'facebook');
                } else { // Si NO existia el perfil de Facebook, lo creamos en el ETL
                    $this->modelObj->sendInfluenciadorToBackend('post', 'facebook', true);
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        $this->onAfterUpdate($request);

        return response()->jsonSuccess(['data' => ['redirect' => $redirect]]);
    }

    /**
     * Búsqueda de influenciador por Nombre
     */
    public function searchByText(Request $request)
    {
        // guardar parametros de busqueda avanzada en base a session id
        $data           = ['session_id' => \Session::getId(), 'params' => json_encode($request->all()), 'origin' => 1, 'method' => 'POST'];
        $advancedSearch = BusquedaAvanzadaInfluenciador::where('session_id', $data['session_id'])
            ->where('method', 'POST')
            ->first();

        if (empty($advancedSearch)) {
            $advancedSearch = new BusquedaAvanzadaInfluenciador;
        }
        $advancedSearch->fill($data);
        $advancedSearch->save();

        return response()->json(
            Influenciador::searchByText(
                $request->input('q'),
                $request->input('page', 0)
            )
        );
    }

    public function searchLugarNacimientoByText(Request $request)
    {
        return response()->json(
            Influenciador::searchLugarNacimientoByText(
                $request->input('q'),
                $request->input('page', 0)
            )
        );
    }

    public function searchPaisByText(Request $request)
    {
        return response()->json(
            Pais::where('pais', 'LIKE', '%'.$request->input('q').'%')->get()
        );
    }

    /**
     * Búsqueda avanzada de influenciadores
     */
    public function advancedSearch(Request $request)
    {
        // guardar parametros de busqueda avanzada en base a session id - origin 2 advanced
        $data           = ['session_id' => \Session::getId(), 'params' => json_encode($request->all()), 'origin' => 2, 'method' => 'POST'];
        $advancedSearch = BusquedaAvanzadaInfluenciador::where('session_id', $data['session_id'])
            ->where('method', 'POST')
            ->first();

        if (empty($advancedSearch)) {
            $advancedSearch = new BusquedaAvanzadaInfluenciador;
        }
        $advancedSearch->fill($data);
        $advancedSearch->save();

        return response()->json(
            Influenciador::advancedSearch(
                $request->input(),
                $request->input('page', 0)
            )
        );
    }

    /**
     * Muestra el perfil del influenciador
     *
     * @return \Illuminate\Http\Response
     */
    public function miinfo()
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $this->modelObj = Auth::user()->influenciador;

        if (empty($this->modelObj)) {
            abort(404);
        }

        $this->catalogs["precio_publicacion"] = [
            [
                'red'    => 'twitter',
                'precio' => $this->modelObj->perfilTwitter ? $this->modelObj->perfilTwitter->precio_publicacion : 0,
            ],
        ];

        return view($this->resource.'.show_v2', [
            'model'         => $this->modelObj,
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => $this->title_sin,
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        $this->validPolicy('view', $this->modelObj);

        $this->onAfterShow($id, $this->response_info);

        if (empty($this->modelObj)) {
            $this->response_info['error']   = true;
            $this->response_info['type']    = 'danger';
            $this->response_info['message'] = 'Datos no disponibles.';

            request()->session()->flash('response_info', $this->response_info);
            return redirect($this->resource);
        }

        if (Auth::user()->isAgencia()) {
            if (!empty(Auth::user()->agencia->entorno_id)) {
                if (!Influenciador::isInEntorno($this->modelObj->id, Auth::user()->agencia->entorno_id)) {
                    return redirect($this->resource)
                        ->withErrors(['influenciador_id' => 'Usuario no disponible']);
                }
            }
        }

        return view($this->resource.'.show_v2', [
            'model'         => $this->modelObj,
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => $this->title_sin,
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    /**
     * Muestra el listado de campañas a las que esta asociado el influenciador
     *
     * @return \Illuminate\Http\Response
     */
    public function miscampanas()
    {
        $this->modelObj = Auth::user()->influenciador;

        if (empty($this->modelObj)) {
            abort(404);
        }

        return view($this->resource.'.miscampanas', [
            'campanas' => $this->modelObj->campanasEnEsperaYAceptadas(),
            'title'    => 'Mis Campañas',
        ]);
    }

    /**
     * Muestra el listado de posts propuestos por la campaña
     *
     * @return \Illuminate\Http\Response
     */
    public function miscampanasposts(Request $request, $id)
    {
        $campana = Campana::where('id', $id)->first();
        $influenciador = Auth::user()->influenciador;

        if (empty($campana) || empty($influenciador)) {
            abort(404);
        }

        return view($this->resource.'.miscampanas_posts', [
            'campana' => $campana,
            'influenciador' => $influenciador,
            'title'    => 'Mis Campañas',
        ]);
    }

    public function aprobarpublicaciones(Request $request)
    {
        $influenciador = Auth::user()->influenciador;

        if (empty($influenciador)) {
            return response()->jsonNotFound();
        }

        try {
            DB::beginTransaction();

            $influenciador->aprobarPublicacionesCampana($request->input('campana_id'));

            LogEvento::saveAceptacionPublicacionesInfluenciador($request->input('campana_id'));

            DB::commit();

            // 7.- Influencer accepted publications
            $campana = Campana::find((int)$request->get('campana_id'));
            $campana->agencia->usuario->notify(new InfluencerAcceptedPublicationProposals([
                'influencer_name' => $influenciador->nombre,
                'campaign_id'     => $campana->id
            ]));

        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([]);
    }

    /**
     * Muestra el formulario para sincronizar los datos
     *
     * @return \Illuminate\Http\Response
     */
    public function sincronizacion()
    {
        $fecha_sincronizacion = Storage::disk('local')->get('json/fechasincronizacion.json');

        $influenciadores = User::where([
            'tipo_usuario_id' => TipoUsuario::CELEBRIDAD,
            'activo'          => 1,
        ])
        ->has('influenciador.perfilTwitter')
        ->with('influenciador.perfilTwitter')
        ->get();

        return view($this->resource.'.sincronizacion', [
            'fecha_sincronizacion' => $fecha_sincronizacion,
            'response_info'        => $this->response_info,
            'title'                => 'Sincronización de perfiles',
            'modelClass'           => $this->modelNamespace . $this->modelClass,
            'influenciadores'      => $influenciadores,
        ]);
    }

    public function sincronizarjsons(Request $request)
    {
        try {
            DB::beginTransaction();

            $this->validPolicy('syncperfiles', new Influenciador());

            Influenciador::syncperfiles();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([]);
    }

    public function sincronizarperfil(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $influenciador = Influenciador::find($id);

            if (empty($influenciador)) {
                return response()->jsonNotFound();
            }

            $this->validPolicy('syncperfiles', $influenciador);

            $influenciador->syncperfil();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([]);
    }

    public function catalogo()
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        $this->validPolicy('catalogo', $modelCls);

        $this->catalogs['genero']             = collect(Genero::$generos);
        $this->catalogs['si_no']              = SiNo::$opciones;
        $this->catalogs['industria']          = collect(Industria::get()->toArray());
        $this->catalogs['tipo_campana']       = collect(TipoCampana::get()->toArray());
        $this->catalogs['rango_etario']       = collect(RangoEtario::get()->toArray());
        $this->catalogs['tipo_influenciador'] = collect(TipoInfluenciador::get()->toArray());
        $this->catalogs['pais']               = collect(Pais::where('activo', 1)->get()->toArray())
                                                ->prepend(['id'=>'', 'pais'=>Influenciador::$labels['pais_id']]); // Se agrega el inicio la etiqueta de pais

        // Se agrega elementos al inicio, porque selectToList detecta como selected al primer elemento
        // aun y cuando no esta seleccionado, esto pasa al cargar la página
        /*$this->catalogs['genero']->prepend(['id'=>'', 'genero'=>'', 'icono'=>'']);
        $this->catalogs['industria']->prepend(['id'=>'', 'nombre'=>'', 'icono'=>'']);
        $this->catalogs['tipo_campana']->prepend(['id'=>'', 'tipo'=>'', 'icono'=>'']);
        $this->catalogs['rango_etario']->prepend(['id'=>'', 'descripcion'=>'', 'icono'=>'']);
        $this->catalogs['tipo_influenciador']->prepend(['id'=>'', 'descripcion'=>'', 'icono'=>'']);
        $this->catalogs['pais']->prepend(['id'=>'', 'pais'=>$modelCls::$labels['pais_id']]);*/

        // check if exist any parameter for searching
        $foundSearch = false;
        $params      = null;
        $origin      = null;
        $busqueda    = BusquedaAvanzadaInfluenciador::where('session_id', \Session::getId())
            ->where('method', 'POST')
            ->first();

        if (!empty($busqueda)) {
            $foundSearch = true;
            $params      = json_decode($busqueda->params);
            $origin      = (int) $busqueda->origin;
        }

        if (isset($params->palabra_clave_id)) {
            foreach ($params->palabra_clave_id as $palabraClave) {
                $palabra = PalabraClave::find($palabraClave);
                $params->palabra_clave[] = ['id' => $palabra->id, 'palabra' => $palabra->palabra];
            }
        }

        return view($this->resource.'.catalogo', [
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => 'Catálogo de Influenciadores',
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
            'search'        => ['params' => $params, 'foundSearch' => $foundSearch, 'origin' => $origin]
        ]);
    }

    /**
     * Vista para el login de influencer
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        // $url_login_fb = $fb->getLoginUrl(['email']);

        if (Auth::check()) {
            if (Auth::user()->isCelebridad()) {
                return redirect($this->resource . '/miscampanas');
            }
        }

        return view($this->resource.'.login');
    }

    /**
     * Registra el clic sobre el perfil del influencer
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function clic(Request $request)
    {
        $aparicionBusqueda = new InfluenciadorClic();
        $aparicionBusqueda->influenciador_id = $request->input('id');
        $aparicionBusqueda->save();

        return response()->json('Registro exitoso', 200);
    }

    public function showCustom($id, $q)
    {
        $this->modelObj                  = $this->findModel($id);
        $this->response_info['resource'] = $this->resource;
        $modelCls                        = $this->modelNamespace . $this->modelClass;

        $this->validPolicy('view', $this->modelObj);
        $this->onAfterShow($id, $this->response_info);

        return view($this->resource.'.show_custom', [
            'model'         => $this->modelObj,
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => $this->title_sin,
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    /**
     * checks if the request has filters and then save it on db
     *
     * @param array $request
     */
    public function onAfterIndex($request = null, $data = null)
    {
         if (count($this->filter) > 0 && $this->filter['busqueda']) {
            // hay filtros, guardar
            $data = ['session_id' => \Session::getId(), 'params' => json_encode($this->filter), 'origin' => 2, 'method' => 'GET'];

            $busqueda = BusquedaAvanzadaInfluenciador::where('session_id', $data['session_id'])
                ->where('method', 'GET')
                ->first();
            // dd($busqueda);
            if (empty($busqueda)) {
                $busqueda = new BusquedaAvanzadaInfluenciador;
            }

            $busqueda->fill($data);
            $busqueda->save();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $models   = [];

        $this->validPolicy('index', $modelCls);

        if ($request->session()->has('response_info')) {
            $this->response_info = $request->session()->get('response_info');
        }

        try {
            $entornos = null;

            // Si el usuario no es Administrador
            if (!Auth::user()->isAdministrador()) {
                // Detectamos el entorno
                $entornos = Auth::user()->getEntornos();
            }

            if (!Auth::user()->isAdministrador() && empty($entornos)) {
                $this->filter[] = ['id', '=', -1];
                $this->response_info['error']   = true;
                $this->response_info['type']    = 'danger';
                $this->response_info['message'] = 'El usuario no esta asociado a un Entorno.';
            }

            // Limita los resultados de las busquedas a los influenciadores que
            // pertenecen al entorno del usuario logueado
            if (!empty($entornos)) {
                $this->filter[] = ['id', 'in', Influenciador::getIdsInflueRelatedByEntorno($entornos)];
            }

            $models = call_user_func_array([$modelCls, 'filter'], [
                $this->filter,//array_filter($request->input('filter', [])),
                true,
                $request->input('order_by', $this->order_by),
                $request->input('order_direction', 'ASC'),
            ]);
        } catch (Exception $e) {
            $this->response_info['error']   = true;
            $this->response_info['type']    = 'danger';
            $this->response_info['message'] = 'Error al procesar los datos. ' .
                                               Helper::getMessageIfDebug($e);
        }

        $this->onAfterIndex($request, $this->response_info);

        return view($this->resource.'.index', [
            'models'        => $models,
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => $this->title_plu,
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    /**
     * search for followers growing by influencer id. search from today to 30 days earlier
     *
     * data gathered for graphics
     *
     * @param string $id
     * @param Request $request
     * @return Illuminate\Response\JsonResponse
     */
    public function crecimientoFollowers($id)
    {
        $response    = ['status' => 'success'];
        $currentDate = new DateTime;
        $fecha1      = $currentDate->format('Y-m-d');
        $endDate     = $currentDate->sub(new DateInterval('P7D'));
        $fecha2      = $endDate->format('Y-m-d');
        $followers   = EstadisPerfil::select('ultima_revision', DB::raw("SUM(followers) AS followers"))
            ->where('influenciador_id', (int)$id)
            ->whereBetween('ultima_revision', [$fecha2, $fecha1])
            ->groupBy('ultima_revision')
            ->orderBy('ultima_revision')
            ->get();

        if ($followers->count() === 0) {
            $response['status']  = 'fail';
            $response['message'] = 'No hay followers para graficar';

            return response()->json($response);
        }

        $series     = [];
        $data       = [];
        $xAxis      = [
            [
                'type'         => 'category',
                'data'         => [],
                'offset'       => 0,
                'boundaryGap'  => false,
                'name'         => 'Days',
                'nameLocation' => 'middle',
                'nameGap'      => 25
            ],
            // ['data' => [], 'offset' => 1]
        ];
        $categories = ['Twitter'];

        foreach ($followers as $follower) {
            $data[]  = $follower->followers;
            $xAxis[0]['data'][] = $follower->ultima_revision;
        }

        $response['series'][] = [
            'xAxisIndex' => 0,
            'name'   => 'Twitter',
            'type'   => 'line',
            'lineStyle' => [
                'normal' => [
                    'color' => '#92D5F7'
                ]
            ],
            'data' => $data
        ];

        /*$response['series'][] = [
            'xAxisIndex' => 1,
            'name'   => 'Growing on facebook',
            'type'   => 'line',
            'lineStyle' => [
                'normal' => [
                    'color' => '#6E95E4'
                ]
            ],
            'data' => [1010, 1020]
        ];
        $xAxis[1]['data'][] = '2017-06-28';
        $xAxis[1]['data'][] = '2017-07-14';*/

        sort($data, SORT_NUMERIC);
        $total = count($data);

        $response['xAxis']      = $xAxis;
        $response['max']        = $data[$total - 1] + 50;
        $response['min']        = $data[0] - 50;
        $response['categories'] = $categories;

        return response()->json($response);
    }

    /**
     * builds data for weekely interactions
     *
     * @param string $id
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function interaccionesMensual($id)
    {
        $interactions = Influenciador::interactionsLastMonth((int)$id);

        if (count($interactions) === 0) {
            $response['status']  = 'fail';
            $response['message'] = 'No hay interacciones para graficar';

            return response()->jsonNotFound($response);
        }

        $series               = [];
        $data                 = [];
        $xAxisData            = [[
            'type'         => 'category',
            'data'         => [],
            'offset'       => 0,
            'boundaryGap'  => false
        ]];
        $actividadTotal       = 0;
        $interaccionesTotales = 0;

        foreach ($interactions as $interaction) {
            $data[]                 = $interaction->interactions;
            $xAxisData[0]['data'][] = $interaction->month_day;
            $interaccionesTotales += $interaction->interactions;
        }

        $categories = ['Twitter'];

        $response['series'] = [[
            'xAxisIndex' => 0,
            'name'       => 'Twitter',
            'type'       => 'line',
            'lineStyle' => [
                'normal' => [
                    'color' => '#92D5F7'
                ]
            ],
            'data' => $data
        ]];

        $response['total']      = number_format($interaccionesTotales);
        $response['data']       = $xAxisData;
        $response['categories'] = $categories;
        $response['status']     = 'success';

        return response()->json($response);
    }
}