<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseWebController;
use App\Helpers\Helper;
use App\Models\Campana;
use App\Models\Mensaje;
use App\Models\Influenciador;
use App\Models\LogEvento;
use App\Models\Publicacion;
use App\Models\InfluenciadorXCampana;
use App\Models\PublicacionXInfluenciadorXCampana;
use App\Models\RedSocial;
use App\Models\User;
use App\Models\TipoUsuario;
use App\Notifications\PropuestaCampanaNotification;
use App\Notifications\CampaingOfferRespondedByInfluencer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Exception;
use DB;

class PublicacionXInfluenciadorXCampanaController extends BaseWebController
{
    public $modelClass = 'PublicacionXInfluenciadorXCampana';
    public $resource   = 'publicacionesxcampana';
    public $title_sin  = 'Publicación Campaña';
    public $title_plu  = 'Publicaciones Campaña';
    public $order_by   = 'created_at';

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id = null)
    {
        try {
            DB::beginTransaction();

            $this->modelObj = PublicacionXInfluenciadorXCampana::where([
                    'influenciador_id' => $request->input('influenciador_id'),
                    'campana_id' => $request->input('campana_id'),
                ])->first();

            if (empty($this->modelObj)) {
                return response()->jsonNotFound();
            }

            $this->validPolicy('delete', $this->modelObj);

            $this->modelObj->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        $this->onAfterDestroy($request, $this->response_info);

        return response()->jsonSuccess([]);
    }

    /**
     * Actualiza la cantidad de publicaciones que la campaña propone al influenciador
     * en cada red social
     *
     * Recibe un JSON con el siguiente formato
     *
     * {
     *      influenciador_id: 169,
     *      campana_id: 49,
     *      redes_sociales: [
     *           {red_social_id: 1, cantidad_publicaciones: 2}, // Twitter
     *           {red_social_id: 2, cantidad_publicaciones: 5}, // Facebook
     *      ]
     * }
     *
     * Se ejecuta desde la sesión de Administrador o Agencia
     */
    public function update(Request $request, $id = null)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $datos = $request->input();
        $influenciador = Influenciador::where('id', $datos['influenciador_id'])->first();
        $precio_publicacion = null;

        try {
            DB::beginTransaction();

            if (!isset($datos['redes_sociales'])) {
                throw new Exception('Debe especificar la cantidad de Publicaciones por lo menos para una Red Social.', 422);
            }

            if (empty($datos['redes_sociales'])) {
                throw new Exception('Debe especificar la cantidad de Publicaciones por lo menos para una Red Social.', 422);
            }

            if (empty($influenciador)) {
                throw new Exception('Datos no disponibles del Influenciador.', 404);
            }

            // Actualizamos el "estado" de la propuesta de campaña a "Esperando Respuesta" para que esta se pueda visualizar en
            // la vista "Mis Campañas" del Influenciador
            InfluenciadorXCampana::where([
                    'influenciador_id' => $datos['influenciador_id'],
                    'campana_id' => $datos['campana_id'],
                ])->update([
                    'estado' => InfluenciadorXCampana::$ESPERANDO_RESPUESTA
                ]);

            // Buscamos las propuestas de publicaciones para cada red social
            foreach($datos['redes_sociales'] as $red_social) {
                // Revisamos si tenemos los datos de la red social del influenciador
                switch ($red_social['red_social_id']) {
                    case RedSocial::$TWITTER:
                        if ($influenciador->perfilTwitter) {
                            $precio_publicacion = $influenciador->perfilTwitter->precio_publicacion;
                        }
                        break;
                    case RedSocial::$FACEBOOK:
                        if ($influenciador->perfilFacebook) {
                            $precio_publicacion = $influenciador->perfilFacebook->precio_publicacion;
                        }
                        break;
                    case RedSocial::$INSTAGRAM:
                        if ($influenciador->perfilInstagram) {
                            $precio_publicacion = $influenciador->perfilInstagram->precio_publicacion;
                        }
                        break;
                    case RedSocial::$YOUTUBE:
                        if ($influenciador->perfilYoutube) {
                            $precio_publicacion = $influenciador->perfilYoutube->precio_publicacion;
                        }
                        break;
                    case RedSocial::$SNAPCHAT:
                        if ($influenciador->perfilSnapchat) {
                            $precio_publicacion = $influenciador->perfilSnapchat->precio_publicacion;
                        }
                        break;
                }

                if (is_null($precio_publicacion)) {
                    throw new Exception('No se tiene asociada la Red Social "'.ucfirst(RedSocial::getRedSocialText($red_social['red_social_id'])).'" para el influenciador seleccionado, '.
                        'o no se ha establecido el precio por publicación. ', 422);
                }

                $this->modelObj = PublicacionXInfluenciadorXCampana::where([
                    'influenciador_id' => $datos['influenciador_id'],
                    'campana_id'       => $datos['campana_id'],
                    'red_social_id'    => $red_social['red_social_id'],
                ])->first();

                // Si no existe la propuesta, la creamos
                if (empty($this->modelObj)) {
                    $this->modelObj = new PublicacionXInfluenciadorXCampana();

                    $this->modelObj->fill([
                        'influenciador_id'   => $datos['influenciador_id'],
                        'campana_id'         => $datos['campana_id'],
                        'red_social_id'      => $red_social['red_social_id'],
                        'precio_publicacion' => $precio_publicacion,
                    ]);
                }

                // $this->validPolicy('update', $this->modelObj);

                $this->modelObj->cantidad_publicaciones = Helper::stripTags($red_social['cantidad_publicaciones']);
                $this->modelObj->estado                 = PublicacionXInfluenciadorXCampana::$ESPERANDO_RESPUESTA;

                if (!$this->modelObj->isValid($this->modelObj->exists ? $modelCls::DO_UPDATE : $modelCls::DO_CREATE)) {
                    throw new Exception('Error al procesar los datos. ' .
                        Helper::errorsToString($this->modelObj->errors), 422);
                }

                $this->modelObj->save();

                // TODO: Actualizar la notificación para que se incluya todas las redes sociales
                // if ($red_social['red_social_id'] == RedSocial::$TWITTER) {
                    LogEvento::saveOfertaCampanaInfluenciador(
                        $datos['campana_id'],
                        $influenciador->usuario_id,
                        $red_social['red_social_id']
                    );

                    // 1.- notificación a influenciador por oferta de campaña
                    $influenciador->usuario->notify(new PropuestaCampanaNotification([
                        'campana'       => $this->modelObj->campana->nombre,
                        'red_social'    => $this->modelObj->redSocial->nombre,
                        'publicaciones' => $this->modelObj->cantidad_publicaciones,
                        'agencia'       => $this->modelObj->campana->agencia->nombre,
                        'precio'        => '$' . number_format(doubleval($this->modelObj->precio_publicacion) * $this->modelObj->cantidad_publicaciones) . ' MXN',
                        'is_televisa'   => Auth::user()->inEntornoTelevisa(),
                        'influenciador_id' => $this->modelObj->influenciador_id,
                    ]));
                // }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([]);
    }

    public function rechazarpropuesta(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        try {
            DB::beginTransaction();

            $campana = Campana::where('id', $request->input('campana_id'))->first();
            $propuestas = PublicacionXInfluenciadorXCampana::where([
                'influenciador_id' => Auth::user()->influenciador->id,
                'campana_id'       => $request->input('campana_id'),
                // 'red_social_id'    => $request->input('red_social_id'),
            ])->update([
                'estado' => PublicacionXInfluenciadorXCampana::$RECHAZADO
            ]);

            // Publicaciones
            Publicacion::where([
                    'influenciador_id' => Auth::user()->influenciador->id,
                    'campana_id'       => $request->input('campana_id'),
                    // 'red_social_id'    => $request->input('red_social_id'),
                ])->delete();

            Mensaje::create([
                'usuario_id' => $campana->created_by,
                'campana_id' => $request->input('campana_id'),
                'asunto' => 'Rechazo a Propuesta de Campaña' ,
                'contenido' => $request->input('motivo_rechazo'),
            ]);

            LogEvento::saveRechazoCampanaInfluenciador($request->input('campana_id'), RedSocial::$TWITTER);

            DB::commit();

            // 2A.- notificación a agencia por rechazo de oferta de campaña
            $agency = $campana->agencia;
            $agency->usuario->notify(new CampaingOfferRespondedByInfluencer([
                'status_on_past'  => 'rechazado',
                'influencer_name' => Auth::user()->influenciador->nombre,
                'campaign_name'   => $campana->nombre,
                'campaign_id'     => $campana->id
            ]));

        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([]);
    }

    public function aceptarpropuesta(Request $request)
    {
        try {
            DB::beginTransaction();

            $campana = Campana::where('id', $request->input('campana_id'))->first();
            $influenciador_id = Auth::user()->influenciador->id;
            $propuestas = PublicacionXInfluenciadorXCampana::where([
                    'influenciador_id' => $influenciador_id,
                    'campana_id'       => $request->input('campana_id'),
                    // 'red_social_id'    => $request->input('red_social_id'),
                ])->update([
                    'estado' => PublicacionXInfluenciadorXCampana::$ACEPTADO,
                ]);

            Mensaje::create([
                'usuario_id' => $campana->created_by,
                'campana_id' => $request->input('campana_id'),
                'asunto'     => 'Aceptación a Propuesta de Publicación para Campaña' ,
                'contenido'  => 'Acepto la propuesta de publicación.',
            ]);

            // Cuando se acpeta la propuesta se crea las publicaciones
            $propuestas = PublicacionXInfluenciadorXCampana::where([
                    'influenciador_id' => Auth::user()->influenciador->id,
                    'campana_id'       => $request->input('campana_id'),
                    // 'red_social_id'    => $request->input('red_social_id'),
                ])->get();

            for ($i=0; $i < count($propuestas); $i++) {
                for ($j=0; $j < $propuestas[$i]->cantidad_publicaciones; $j++) {
                    $publicacion = new Publicacion();

                    $publicacion->campana_id       = $propuestas[$i]->campana_id;
                    $publicacion->influenciador_id = $influenciador_id;
                    $publicacion->red_social_id    = $propuestas[$i]->red_social_id;
                    $publicacion->contenido        = $campana->hashtags;
                    $publicacion->estado           = Publicacion::$ESPERANDO_APROBACION_INFLUENCER;
                    $publicacion->activo           = 1;

                    $publicacion->save();
                }
            }

            LogEvento::saveAceptacionCampanaInfluenciador($request->input('campana_id'), RedSocial::$TWITTER);
            DB::commit();

            // 2B.- notificación a agencia por aceptación de oferta de campaña
            $agency = $campana->agencia;
            $agency->usuario->notify(new CampaingOfferRespondedByInfluencer([
                'status_on_past'  => 'aceptado',
                'influencer_name' => Auth::user()->influenciador->nombre,
                'campaign_name'   => $campana->nombre,
                'campaign_id'     => $campana->id
            ]));

        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([]);
    }
}
