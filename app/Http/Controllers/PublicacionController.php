<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Campana;
use App\Models\Influenciador;
use App\Models\LogEvento;
use App\Models\Publicacion;
use App\Models\RedSocial;
use App\Helpers\Helper;
use App\Notifications\InfluencerUpdatedImageOnPublicationProposal;
use App\Notifications\PublicationProposalCreatedForInfluencer;
use App\Notifications\AgencyAcceptedPublications;
use App\Notifications\InfluencerAcceptedPublicationProposals;
use Illuminate\Http\Request;
use Exception;
use DB;

class PublicacionController extends BaseWebController
{
    public $modelClass = 'Publicacion';
    public $resource   = 'publicaciones';
    public $title_sin  = 'Publicación';
    public $title_plu  = 'Publicaciones';
    public $order_by   = 'created_at';

    public function store(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        $this->validPolicy('create', $modelCls);

        try {
            DB::beginTransaction();
            // Permite actualizar o crear un nuevo registro
            // debido a que los archivos no se pueden enviar por PUT
            $this->modelObj = $this->findModel($request->input('id'));

            // this is a flag that indicates which user sent the info: influencer or agency/admin
            $mode = 'influencer';

            if (empty($this->modelObj)) {
                $this->modelObj = new $modelCls;
            }

            $this->modelObj->fill(Helper::stripTagsArray($request->input()));

            if ($this->modelObj->isValid($modelCls::DO_UPDATE)) {
                $this->modelObj->save();

                if ($request->hasFile($this->modelObj->fileName)) {
                    $this->modelObj->uploadFile($request->file($this->modelObj->fileName));
                }

                if (Auth::user()->isAgencia() || Auth::user()->isAdministrador()) {
                    $influenciador = Influenciador::where('id', $request->input('influenciador_id'))->first();
                    LogEvento::saveCreacionPublicaciones(
                        $this->modelObj->campana_id,
                        $influenciador->usuario_id,
                        $this->modelObj->red_social_id
                    );

                    $mode = 'agency';
                }

                DB::commit();

                // 3.- send notification to influencer for publication proposal when creating
                if($mode === 'agency') {
                    if (!isset($influenciador) && is_null($influenciador)) {
                        $influenciador = Influenciador::where('id', $request->input('influenciador_id'))->first();
                    }

                    $influenciador->usuario->notify(new PublicationProposalCreatedForInfluencer([
                        'agency_name' => $this->modelObj->campana->agencia->nombre,
                        'campaign_id' => $request->input('campana_id')
                    ]));
                }

                // 4 influencer changed images
                if($mode === 'influencer') {
                    $campana = Campana::find((int)$request->get('campana_id'));
                    $campana->agencia->usuario->notify(new InfluencerUpdatedImageOnPublicationProposal([
                        'campaign_name'   => $campana->nombre,
                        'influencer_name' => Auth::user()->influenciador->nombre,
                        'campaign_id'     => $request->input('campana_id')
                    ]));
                }

            } else {
                return response()->jsonInvalidData([
                    'extra' => $this->modelObj->errors
                ]);
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([]);
    }

    public function publicar(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            // Envía una publicación de forma individual
            if (!empty($id)) {
                $publicacion = Publicacion::find($id);

                if (empty($publicacion)) {
                    return response()->jsonNotFound(['message' => 'Publicación no disponible.']);
                }

                $publicacion->setAprobadoPorPublicar();
                $publicacion->save();
                $publicacion->sendToETL();

                $campana       = Campana::find((int) $publicacion->campana_id);
                $influenciador = Influenciador::find((int) $publicacion->influenciador_id);
            }
            // Envía todas las publicaciones de un influenciador y campaña en específico
            else if(!empty($request->input('campana_id')) && !empty($request->input('influenciador_id'))) {
                Publicacion::aprobarPublicar($request->input('campana_id'), $request->input('influenciador_id'));
                Publicacion::sendPublicacionToETL($request->input('campana_id'), $request->input('influenciador_id'));

                $campana       = Campana::find((int) $request->input('campana_id'));
                $influenciador = Influenciador::find((int) $request->input('influenciador_id'));
            } else {
                return response()->jsonInvalidData(['message' => 'Datos inválidos, faltan parámetros a la solicitud.']);
            }

            DB::commit();

            // 8.- Agency published posts
            $influenciador->usuario->notify(new AgencyAcceptedPublications([
                'campaign_name' => $campana->nombre,
                'agency_name'   => $campana->agencia->nombre,
                'campaign_id'   => $campana->id
            ]));

        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([]);
    }

    /**
     * Aprueba una publicación desde la sesión del influencer
     *
     */
    public function aprobar(Request $request)
    {
        try {
            DB::beginTransaction();

            $this->modelObj = $this->findModel($request->input('id'));

            if (empty($this->modelObj)) {
                return response()->jsonNotFound();
            }

            $this->modelObj->setAprobadoInfluencer();
            $this->modelObj->save();

            // 7.- Influencer accepted publications
            $campana = $this->modelObj->campana;
            $campana->agencia->usuario->notify(new InfluencerAcceptedPublicationProposals([
                'influencer_name' => Auth::user()->nombre,
                'campaign_id'     => $campana->id,
                'publication'     => $this->modelObj->contenido,
            ]));

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([]);
    }
}
