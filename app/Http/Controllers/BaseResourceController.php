<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route; // Get the parameter $route in method find
use App\Http\Controllers\Controller;
use DB;

class BaseResourceController extends Controller
{
    /**
     * Model namespace.
     *
     * @var String
     */
    protected $modelNamespace = 'App\Models\\';

    /**
     * The model class associated with the controller.
     *
     * @var string
     */
    protected $modelClass = '';

    /**
     * Current model instance.
     *
     * @var object
     */
    protected $modelObj = null;

    /**
     * Resource name.
     *
     * @var string
     */
    protected $resource = '';

    /**
     * By default, Route::resource will create the route parameters based on the "singularized" version
     * of the resource name. You can easily override this on a per resource basis by passing parameters
     * in the options array. The parameters array should be an associative array of resource names
     * and parameter names:
     *
     * Route::resource('user', 'AdminUserController', ['parameters' => [
     *     'user' => 'admin_user'
     * ]]);
     *
     * @var string
     */
    protected $resourceParameter = null;

    /**
     * The middleware registered on the controller.
     *
     * @var array
     */
    protected $middleware = [];

    /**
     * The name of the column to sort.
     *
     * @var string
     */
    public $order_by = null;

    /**
     * Call findModel to get the resource
     *
     * @param  Illuminate\Routing\Route $route
     * @return void
     */
    public function __construct(\Illuminate\Http\Request $request)
    {
        $route = $request->route(); // \Illuminate\Support\Facades\Route::getCurrentRoute();
        $action = \Illuminate\Support\Facades\Route::currentRouteName(); // $route->getAction();
        $id = null;
        $this->resourceParameter = $this->resourceParameter ?: str_singular($this->resource);

        // When execute php artisan route:list
        // $route is null
        if (!empty($route)) {
            // The ID could be in the parameter 'id' or in the parameter call as the name of the resource
            $id = $route->getParameter($this->resourceParameter) ?
                $route->getParameter($this->resourceParameter) :
                $route->getParameter('id');
        }

        $this->modelObj = $this->findModel($id);

        $this->onBeforeAction($action, $request);
    }

    /**
     * Execute before action event
     *
     * @param  array $action
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function onBeforeAction($action, $request)
    {
        //$action['as'] == \Illuminate\Support\Facades\Route::currentRouteName()
        $action_name = str_replace($this->resource.'.', '', $action);

        switch ($action_name) {
            case 'index':
                $this->onBeforeIndex($request);
                break;
            case 'create':
                $this->onBeforeCreate($request);
                break;
            case 'store':
                $this->onBeforeStore($request);
                break;
            case 'show':
                $this->onBeforeShow($request);
                break;
            case 'edit':
                $this->onBeforeEdit($request);
                break;
            case 'update':
                $this->onBeforeUpdate($request);
                break;
            case 'destroy':
                $this->onBeforeDestroy($request);
                break;
        }
    }

    public function onBeforeIndex(\Illuminate\Http\Request $request)
    {
        //
    }

    public function onBeforeCreate(\Illuminate\Http\Request $request)
    {
        //
    }

    public function onBeforeStore(\Illuminate\Http\Request $request)
    {
        //
    }

    public function onBeforeShow(\Illuminate\Http\Request $request)
    {
        //
    }

    public function onBeforeEdit(\Illuminate\Http\Request $request)
    {
        //
    }

    public function onBeforeUpdate(\Illuminate\Http\Request $request)
    {
        //
    }

    public function onBeforeDestroy(\Illuminate\Http\Request $request)
    {
        //
    }

    /**
     * Finds the model based on its primary key value.
     *
     * @param  string $id
     * @return \Object the loaded model
     */
    protected function findModel($id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        // Call the method like $modelCls::find($id)
        return call_user_func_array([$modelCls, 'find'], [$id]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     *         200 OK, if the process was successful
     *         500 Internal Server Error, if something was wrong
     *         Any other valid http status code throw by the exception
     */
    public function index(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $paginator = [];

        try {
            $result = call_user_func_array([$modelCls, 'filter'], [
                $request->input('filter', $this->filter),
                $request->has('page'),
                $request->input('order_by', $this->order_by),
                $request->input('order_direction', 'ASC'),
            ]);

            // When paginator exists
            if ($request->has('page')) {
                // The paginator contents metadata of pagination
                $paginator = $result->toArray();
                $result = $paginator['data'];

                unset($paginator['data']);
            }
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'data' => $result,
            'extra' => $paginator
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *         201 Created, if the process was successful
     *         422 Unprocessable Entity, if the validation fail
     *         500 Internal Server Error, if something was wrong
     *         Any other valid http status code throw by the exception
     */
    public function store(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $data = [];

        try {
            DB::beginTransaction();
            $this->modelObj = new $modelCls;
            $this->modelObj->fill($request->input());

            if ($this->modelObj->isValid($modelCls::DO_CREATE)) {
                $this->modelObj->save();
                DB::commit();
            } else {
                return response()->jsonInvalidData([
                    'extra' => $this->modelObj->errors
                ]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        // If the parameter return is presented
        // the object created will be return
        if ($request->input('return')) {
            $data = [
                'data' => $this->modelObj->toArray(),
            ];
        }

        return response()->jsonSuccess($data, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *         200 Ok, if the process was successful
     *         404 Not Found, if the resource doesn't exist
     *         500 Internal Server Error, if something was wrong
     *         Any other valid http status code throw by the exception
     */
    public function show($id)
    {
        try {
            if (empty($this->modelObj)) {
                return response()->jsonNotFound();
            }
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'data' => $this->modelObj->toArray(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *         200 Ok, if the process was successful
     *         422 Unprocessable Entity, if the validation fail
     *         500 Internal Server Error, if something was wrong
     *         Any other valid http status code throw by the exception
     */
    public function update(Request $request, $id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $data = [];

        try {
            if (empty($this->modelObj)) {
                return response()->jsonNotFound();
            }

            DB::beginTransaction();
            $this->modelObj->fill($request->input());

            if ($this->modelObj->isValid($modelCls::DO_UPDATE)) {
                $this->modelObj->save();
                DB::commit();
            } else {
                return response()->jsonInvalidData([
                    'extra' => $this->modelObj->errors
                ]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        // If the parameter return is presented
        // the object updated will be return
        if ($request->input('return')) {
            $data = [
                'data' => $this->modelObj->toArray(),
            ];
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *         200 Ok, if the process was successful
     *         404 Not Found, if the resource doesn't exist
     *         500 Internal Server Error, if something was wrong
     *         Any other valid http status code throw by the exception
     */
    public function destroy(Request $request, $id)
    {
        $data = [];

        try {
            if (empty($this->modelObj)) {
                return response()->jsonNotFound();
            }

            $deleted = $this->modelObj->toArray();

            DB::beginTransaction();
            $this->modelObj->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        // If the parameter return is presented
        // the object deleted will be return
        if ($request->input('return')) {
            $data = [
                'data' => $deleted,
            ];
        }

        return response()->jsonSuccess($data);
    }
}
