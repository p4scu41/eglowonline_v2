<?php

namespace App\Http\Controllers;

use Storage;

use App\Http\Controllers\Controller;
use App\Models\ServiciosBackend;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getGeneral($id = 0)
    {
        if(Storage::disk('local')->exists('json/ProfileStatistics.'.$id))
            $json = Storage::disk('local')->get('json/ProfileStatistics.'.$id);
        else
            $json = Storage::disk('local')->get('json/ProfileStatistics.0');
        
        $result = json_decode ($json);
        return response()->json($result);
    }
    
    public function getCrecimientoSeguidores($id = 0)
    {
        if(Storage::disk('local')->exists('json/ProfileHistoricalStatistics.'.$id))
            $json = Storage::disk('local')->get('json/ProfileHistoricalStatistics.'.$id);
        else
            $json = Storage::disk('local')->get('json/ProfileHistoricalStatistics.0');
        
        $result = json_decode ($json);
        return response()->json($result);
    }
    
    public function getTopSeguidores($id = 0)
    {
        if(Storage::disk('local')->exists('json/TopFollowers.'.$id))
            $json = Storage::disk('local')->get('json/TopFollowers.'.$id);
        else
            $json = Storage::disk('local')->get('json/TopFollowers.0');
        
        $result = json_decode ($json);
        return response()->json($result);
    }
     
    public function getTopWord($id = 0)
    {
        if(Storage::disk('local')->exists('json/TopWordCloudCom.'.$id))
            $json = Storage::disk('local')->get('json/TopWordCloudCom.'.$id);
        else
            $json = Storage::disk('local')->get('json/TopWordCloudCom.0');
        
        $result = json_decode ($json);
        return response()->json($result);
    }
    
    public function getHashtag($id = 0)
    {
        if(Storage::disk('local')->exists('json/TopHashtags.'.$id))
            $json = Storage::disk('local')->get('json/TopHashtags.'.$id);
        else
            $json = Storage::disk('local')->get('json/TopHashtags.0');
        
        $result = json_decode ($json);
        return response()->json($result);
    }

}
