<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseWebController;
use App\Helpers\Helper;
use App\Models\Campana;
use App\Models\Mensaje;
use App\Models\Influenciador;
use App\Models\LogEvento;
use App\Models\Publicacion;
use App\Models\PublicacionXInfluenciadorXCampana;
use App\Models\InfluenciadorXCampana;
use App\Models\RedSocial;
use App\Models\User;
use App\Models\TipoUsuario;
use App\Notifications\PropuestaCampanaNotification;
use App\Notifications\CampaingOfferRespondedByInfluencer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Exception;
use DB;

class InfluenciadorXCampanaController extends BaseWebController
{
    public $modelClass = 'InfluenciadorXCampana';
    public $resource   = 'influenciadoresxcampana';
    public $order_by   = 'created_at';

    /**
     * Elimina al influenciador de la campaña, incluyendo a todas las publicaciones propuestas
     * Se ejecuta desde la sesión de Administrador o Agencia
     *
     * Recibe: influenciador_id, campana_id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id = null)
    {
        try {
            DB::beginTransaction();

            // Selecciona al usuario que esta asociado con la campaña
            $this->modelObj = InfluenciadorXCampana::where([
                    'influenciador_id' => $request->input('influenciador_id'),
                    'campana_id' => $request->input('campana_id'),
                ])->first();

            if (empty($this->modelObj)) {
                return response()->jsonNotFound();
            }

            $this->validPolicy('delete', $this->modelObj);

            $this->modelObj->delete();

            // Elimina todas las Publicaciones asociadas con el Influenciador y la Campaña
            PublicacionXInfluenciadorXCampana::where([
                'influenciador_id' => $request->input('influenciador_id'),
                'campana_id' => $request->input('campana_id'),
            ])->delete();

            // Elimina el detalle de las Publicaciones
            Publicacion::where([
                'influenciador_id' => $request->input('influenciador_id'),
                'campana_id' => $request->input('campana_id'),
            ])->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        $this->onAfterDestroy($request, $this->response_info);

        return response()->jsonSuccess([]);
    }

    /**
     * Actualiza el estado de la propuesta de campaña a Rechazado
     * Se ejecuta desde la sesión de influenciador
     * Recibe: campana_id
     */
    public function rechazarpropuesta(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        try {
            DB::beginTransaction();

            $campana = Campana::where('id', $request->input('campana_id'))->first();
            $propuesta = InfluenciadorXCampana::where([
                'influenciador_id' => Auth::user()->influenciador->id,
                'campana_id'       => $request->input('campana_id'),
            ])->update([
                'estado' => InfluenciadorXCampana::$RECHAZADO
            ]);

            Mensaje::create([
                'usuario_id' => $campana->created_by,
                'campana_id' => $request->input('campana_id'),
                'asunto' => 'Rechazo la Propuesta de Campaña' ,
                'contenido' => $request->input('motivo_rechazo'),
            ]);

            LogEvento::saveRechazoCampanaInfluenciador($request->input('campana_id'));

            DB::commit();

            // 2A.- notificación a agencia por rechazo de oferta de campaña
            $agency = $campana->agencia;
            $agency->usuario->notify(new CampaingOfferRespondedByInfluencer([
                'status_on_past'  => 'rechazado',
                'influencer_name' => Auth::user()->influenciador->nombre,
                'campaign_name'   => $campana->nombre,
                'campaign_id'     => $campana->id
            ]));
        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([]);
    }

    /**
     * Actualiza el estado de las propuestas de publicacion a aceptado.
     * Se ejecuta desde la sesión de Influenciador
     * Recibe: campana_id
     */
    public function aceptarpropuesta(Request $request)
    {
        try {
            DB::beginTransaction();

            $campana = Campana::where('id', $request->input('campana_id'))->first();
            $influenciador_id = Auth::user()->influenciador->id;

            // Actualizamos el estado de la propuesta de campaña
            InfluenciadorXCampana::where([
                'influenciador_id' => $influenciador_id,
                'campana_id'       => $request->input('campana_id'),
            ])->update([
                'estado' => InfluenciadorXCampana::$ACEPTADO,
            ]);

            // Actualizamos el estado de las propuestas de publicaciones
            PublicacionXInfluenciadorXCampana::where([
                'influenciador_id' => $influenciador_id,
                'campana_id'       => $request->input('campana_id'),
            ])->update([
                'estado' => PublicacionXInfluenciadorXCampana::$ACEPTADO,
            ]);

            Mensaje::create([
                'usuario_id' => $campana->created_by,
                'campana_id' => $request->input('campana_id'),
                'asunto'     => 'Aceptación a Propuesta de Publicación para Campaña' ,
                'contenido'  => 'Aceptó la propuesta de publicación.',
            ]);

            // Cuando se acepta la propuesta se crea las publicaciones
            // Con contenido por defecto el hashtag de la campaña
            // y estado "Esperando aprobación del influenciador"
            $propuestas = PublicacionXInfluenciadorXCampana::where([
                    'influenciador_id' => Auth::user()->influenciador->id,
                    'campana_id'       => $request->input('campana_id'),
                ])->get();

            for ($i=0; $i < count($propuestas); $i++) {
                for ($j=0; $j < $propuestas[$i]->cantidad_publicaciones; $j++) {
                    $publicacion = new Publicacion();

                    $publicacion->campana_id       = $propuestas[$i]->campana_id;
                    $publicacion->influenciador_id = $influenciador_id;
                    $publicacion->red_social_id    = $propuestas[$i]->red_social_id;
                    $publicacion->contenido        = $campana->hashtags;
                    $publicacion->estado           = Publicacion::$ESPERANDO_APROBACION_INFLUENCER;
                    $publicacion->activo           = 1;

                    $publicacion->save();
                }
            }

            LogEvento::saveAceptacionCampanaInfluenciador($request->input('campana_id'));

            // 2B.- notificación a agencia por aceptación de oferta de campaña
            $agency = $campana->agencia;
            $agency->usuario->notify(new CampaingOfferRespondedByInfluencer([
                'status_on_past'  => 'aceptado',
                'influencer_name' => Auth::user()->influenciador->nombre,
                'campaign_name'   => $campana->nombre,
                'campaign_id'     => $campana->id
            ]));

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([]);
    }
}
