<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Agencia;
use App\Models\Influenciador;
use App\Notifications\CampaingOfferRespondedByInfluencer;
use App\Notifications\InfluencerUpdatedImageOnPublicationProposal;
use App\Notifications\PublicationProposalCreatedForInfluencer;
use App\Notifications\InfluencerSentMessageToAgency;
use App\Notifications\AgencySentMessageToInfluencer;
use App\Notifications\InfluencerAcceptedPublicationProposals;
use App\Notifications\AgencyAcceptedPublications;

class TestingMailsController extends Controller
{
    public function testSendingCampaingOfferAccepted()
    {
        $data = [
            'status_on_past'    => 'aceptada',
            'influencer_name'   => 'Gerardo Gomez',
            'status_infinitive' => 'aceptar',
            'campaign_name'     => 'Joga bonito',
            'status'            => 1
        ];

        $agency = Agencia::find(54);
        $agency->usuario->notify(new CampaingOfferRespondedByInfluencer($data));
    }

    public function testSendingCampaingOfferRejected()
    {
        $data = [
            'status_on_past'    => 'rechazada',
            'influencer_name'   => 'Gerardo Gomez',
            'status_infinitive' => 'rechazar',
            'campaign_name'     => 'Joga bonito',
            'status'            => 2
        ];

        $agency = Agencia::find(54);
        $agency->usuario->notify(new CampaingOfferRespondedByInfluencer($data));
    }

    public function testPublicationProposalCreatedForInfluencer()
    {
        $data = [
            'campaign_name'    => 'Joga Bonito',
            'agency_name'      => 'Agencia eGlow',
            'publication'      => '#cuidaelplaneta no dejes de cuidarlo',
            'publication_date' => '01/01/2018 10:00 am',
        ];

        $influenciador = Influenciador::find(54);
        $influenciador->usuario->notify(new PublicationProposalCreatedForInfluencer($data));
    }

    public function testInfluencerChangedImageFromPublication()
    {
        $data = [
            'campaign_name' => 'Joga bonito',
            'agency_name'   => 'Agencia eGlow',
            'publication'   => '#cuidaelplaneta no dejes de cuidarlo'
        ];

        $agency = Agencia::find(54);
        $agency->usuario->notify(new InfluencerUpdatedImageOnPublicationProposal($data));
    }

    public function testInfluencerSentMessageToAgency()
    {
        $data = [
            'campaign_name' => 'Joga bonito',
            'agency_name'   => 'Agencia eGlow'
        ];

        $agency = Agencia::find(54);
        $agency->usuario->notify(new InfluencerSentMessageToAgency($data));
    }

    public function testAgencySentMessageToInfluencer()
    {
        $data = [
            'campaign_name' => 'Joga bonito',
            'agency_name'   => 'Agencia eGlow'
        ];

        $influenciador = Influenciador::find(54);
        $influenciador->usuario->notify(new AgencySentMessageToInfluencer($data));
    }

    public function testInfluencerHasAcceptedPublicationProposal()
    {
        $data = [
            'campaign_name' => 'Joga bonito',
            'agency_name'   => 'Agencia eGlow'
        ];

        $agency = Agencia::find(54);
        $agency->usuario->notify(new InfluencerAcceptedPublicationProposals($data));
    }

    public function testAgencyAcceptedPublications()
    {
        $data = [
            'campaign_name' => 'Joga bonito',
            'agency_name'   => 'Agencia eGlow'
        ];

        $influenciador = Influenciador::find(54);
        $influenciador->usuario->notify(new AgencyAcceptedPublications($data));
    }
}