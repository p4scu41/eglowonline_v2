<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseWebController;
use App\Helpers\Helper;
use App\Models\Influenciador;
use App\Models\TipoUsuario;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use DB;

class UsuarioController extends BaseWebController
{
    public $modelClass = 'User';
    public $modelObj   = null;
    public $resource   = 'usuarios';
    public $title_sin  = 'Usuario';
    public $title_plu  = 'Usuarios';

    public function onBeforeIndex(Request $request)
    {
        parent::onBeforeIndex($request);

        if ($request->has('filter.tipo_usuario_id')) {
            $this->filter[] = ['tipo_usuario_id', '=', $request->input('filter.tipo_usuario_id')];
        } else {
            $this->filter[] = ['tipo_usuario_id', '!=', TipoUsuario::CELEBRIDAD];
        }

        if ($request->has('filter.nombre')) {
            $this->filter[] = ['nombre', 'like', '%'.Helper::stripTags($request->input('filter.nombre')).'%'];
        }

        if ($request->has('filter.email')) {
            $this->filter[] = ['email', 'like', '%'.Helper::stripTags($request->input('filter.email')).'%'];
        }

        $this->order_by = 'nombre';

        $this->catalogs['tipo_usuario'] = TipoUsuario::where('id', '!=', TipoUsuario::CELEBRIDAD)->get()
            ->pluck('descripcion', 'id');
    }

    /**
     * @inheritDoc
     */
    public function index(Request $request)
    {
        // Se omite del listado al usuario que inició sesión
        $this->filter[] = ['id', '!=', Auth::user()->id];

        return parent::index($request);
    }

    public function onBeforeCreate(\Illuminate\Http\Request $request)
    {
        $this->getCatalogs();

        $this->catalogs['tipo_usuario'] = TipoUsuario::where('id', '!=', TipoUsuario::CELEBRIDAD)->get()
            ->pluck('descripcion', 'id');
    }

    public function onBeforeEdit(\Illuminate\Http\Request $request)
    {
        $this->getCatalogs();

        $this->catalogs['tipo_usuario'] = TipoUsuario::where('id', '!=', TipoUsuario::CELEBRIDAD)->get()
            ->pluck('descripcion', 'id');
    }

    /**
     * Muestra la vista para editar el perfil
     *
     * @return \Illuminate\Http\Response
     */
    public function perfil()
    {
        $this->title_sin = 'Perfil del usuario';
        $view = $this->resource.'.edit';
        $this->modelObj = Auth::user();
        $this->catalogs = User::getCatalogs();
        $this->response_info = session('response_info') ? session('response_info') : $this->response_info;

        // Si el usuario es Influenciador
        // carga la vista de edición del influenciador
        if (Auth::user()->isCelebridad()) {
            $view             = 'influenciadores.edit';
            $this->modelClass = 'Influenciador';
            $this->modelObj   = Auth::user()->influenciador;
            $this->catalogs   = collect(Influenciador::getCatalogsToSelect())->merge($this->catalogs);
            $this->response_info['resource'] = 'influenciadores';
        }

        $modelCls = $this->modelNamespace . $this->modelClass;

        if (empty($this->modelObj)) {
            $this->response_info['error']   = true;
            $this->response_info['type']    = 'danger';
            $this->response_info['message'] = 'Datos no disponibles.';

            request()->session()->flash('response_info', $this->response_info);
            return redirect('index');
        }

        return view($view, [
            'model'         => $this->modelObj,
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => 'Perfil del usuario',
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    /**
     * Guarda los cambios en la actualización del perfil
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateperfil(Request $request, $id = null)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $this->modelObj = Auth::user();

        try {
            if (empty($this->modelObj)) {
                throw new Exception('Datos no disponibles.', 404);
            }

            DB::beginTransaction();
            $this->modelObj->fill(Helper::stripTagsArray($request->only(['nombre', 'email'])));

            if ($this->modelObj->isValid($modelCls::DO_UPDATE)) {
                $this->modelObj->save();
                DB::commit();

                $this->response_info['show']    = true;
                $this->response_info['message'] = 'Datos actualizados exitosamente.';
                $this->response_info['type']    = 'success';
            } else {
                return redirect($this->resource . '/perfil')
                    ->withErrors($this->modelObj->validator)
                    ->withInput();
            }
        } catch (Exception $e) {
            DB::rollBack();
            $this->response_info['error']   = true;
            $this->response_info['message'] = 'Error al procesar los datos. '.Helper::getMessageIfDebug($e);
            $this->response_info['type']    = 'danger';
        }

        $request->session()->flash('response_info', $this->response_info);

        return redirect($this->resource . '/perfil');
    }
}
