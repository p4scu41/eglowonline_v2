<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ServiciosBackend;
use Illuminate\Http\Request;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use App\Notifications\RegistroUsuarioNotification;
use App\Models\User;

class ServiciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // \App\Models\Influenciador::find(169)->sendInfluenciadorToBackend('post', 'twitter'); // caro
        // \App\Models\Influenciador::find(200)->sendInfluenciadorToBackend('post', 'twitter'); // jaime
        //dump(\App\Models\Publicacion::sendPublicacionToETL(26, 208));

        /*$backend = new ServiciosBackend();

        $backend->screenname = 'pascual2';
        $backend->datefrom   = '01-01-2016'; // mm-dd-yyyy
        $backend->dateto     = '08-08-2016'; // mm-dd-yyyy

        // https://github.com/Seldaek/monolog/blob/master/doc/01-usage.md
        // Para establecer un archivo log de salida
        $stream = new StreamHandler(storage_path().'/logs/job_publica_twitter.log', Logger::INFO);
        // Para cambiar el formato del output al archivo
        $stream->setFormatter(new LineFormatter("[%datetime%] %level_name%: %message% %context%\n"));
        // Se crea el logger
        $log = new Logger('log');
        $log->pushHandler($stream);

        $log->info('My Logging', ['hola'=>'mundo']);

        User::find(368)->notify(new RegistroUsuarioNotification(['password' => 'password']));*/

        //return response()->json(\App\Models\Campana::find(5)->sendCampanaToBackend('post'));
        //return response()->json(\App\Models\PalabraClave::first()->sendPalabraClaveToBackend('put'));
        // return response()->json(\App\Models\Influenciador::first()->sendInfluenciadorToBackend('put', 'twitter'));
        // return response()->json(\App\Models\Publicacion::first()->sendPublicacionToBackend());

        // dd($backend->profileCommunity()); // devuelve CommunityStatistics, Top10Brands, Top10Hashtags,
        //                                   TopTenFollowers, GenderDistribution, LocationDistribution, Professions
        // dd($backend->topHashtags()); // empty
        // dd($backend->topProfessions()); // empty
        // dd($backend->topFollowers());
        // dd($backend->locationDistribution());
        // dd($backend->genderDistribution());
        // dd($backend->topBrands()); // empty
        // dd($backend->profileHistoricalStatistics()); // devuelve HistoricalData, Top10Hashtags,
        //                                                 WordCloud, Top10Brands, LastTweets, BestDayAndHour
        // dd($backend->topHashtagsProfile()); // ERROR 404 -> mal URI TopHashtags
        // dd($backend->topWordCloudProfile()); // ERROR 404
        // dd($backend->topBrandsProfile()); // empty
        // dd($backend->periodTweetsProfile()); // devuelve TopBrands
        // dd($backend->bestDayHourProfile()); // nombre del arreglo TopBrands
        // dd($backend->appStatus());
        // dd($backend->usersRunning());
        // dd($backend->twitterProfile());
        // dd($backend->twitterProfilePost()); // ERROR cambiar name por username, devuelve TwitterProfiles
        // dd($backend->profileStatistics());
        // dd($backend->keywords('Marca', 'PRUEBA', true));
        // dd($backend->keywordType('PRUEBA', true));
    }
}
