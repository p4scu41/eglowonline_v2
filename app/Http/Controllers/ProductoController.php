<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Marca;
use App\Models\PalabraClave;
use App\Models\Producto;
use App\Models\TipoPalabraClave;
use App\Models\TipoUsuario;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Exception;
use DB;

class ProductoController extends BaseWebController
{
    public $modelClass = 'Producto';
    public $resource   = 'productos';
    public $title_sin  = 'Producto';
    public $title_plu  = 'Productos';
    public $order_by   = 'nombre';

    public function onBeforeIndex(Request $request)
    {
        parent::onBeforeIndex($request);

        if ($request->has('filter.nombre')) {
            $this->filter[] = ['nombre', 'like', '%'.Helper::stripTags($request->input('filter.nombre')).'%'];
        }

        if ($request->has('filter.marca_id')) {
            $this->filter[] = ['marca_id', '=', $request->input('filter.marca_id')];
        }

        $this->order_by = 'nombre';
    }

    public function onAfterCreate($request = null, $data = null)
    {
        if (Auth::user()->isAgencia()) {
            $this->catalogs['marca'] = Marca::where('agencia_id', Auth::user()->agencia->id)
                ->orderBy('nombre')
                ->get()
                ->pluck('nombre', 'id')
                ->prepend(Producto::$labels['marca_id'], '');
        }
    }

    public function onAfterEdit($request = null, $data = null)
    {
        if (Auth::user()->isAgencia()) {
            $this->catalogs['marca'] = Marca::where('agencia_id', Auth::user()->agencia->id)
                ->orderBy('nombre')
                ->get()
                ->pluck('nombre', 'id')
                ->prepend(Producto::$labels['marca_id'], '');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $models   = [];

        $this->validPolicy('index', $modelCls);

        if ($request->session()->has('response_info')) {
            $this->response_info = $request->session()->get('response_info');
        }

        try {
            if (Auth::user()->isAgencia()) {
                $catalogs['marca'] = Marca::where('agencia_id', Auth::user()->agencia->id)
                    ->orderBy('nombre', 'asc')->get()
                    ->pluck('nombre', 'id');
            }

            $query = Producto::search($this->filter);

            if (Auth::user()->isAgencia()) {
                $query->whereIn('marca_id', Marca::where('agencia_id', Auth::user()->agencia->id)->get()->pluck('id'));
            } elseif (Auth::user()->isMarca()) {
                $query->where('marca_id', Auth::user()->marca->id);
            }

            $query->orderByRaw($this->order_by);

            $models = $query->paginate(15);
        } catch (Exception $e) {
            $this->response_info['error']   = true;
            $this->response_info['type']    = 'danger';
            $this->response_info['message'] = 'Error al procesar los datos. ' .
                                               Helper::getMessageIfDebug($e);
        }

        $this->onAfterIndex($request, $this->response_info);

        return view($this->resource.'.index', [
            'models'        => $models,
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => $this->title_plu,
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    public function store(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $data = Helper::stripTagsArray($request->input());

        if (empty($data['usuario_id'])) {
            unset($data['usuario_id']);
        }

        $this->validPolicy('create', $modelCls);

        try {
            DB::beginTransaction();
            $this->modelObj = new $modelCls;
            $this->modelObj->fill($data);

            if ($this->modelObj->isValid($modelCls::DO_CREATE)) {
                $this->modelObj->save();

                if ($request->hasFile('imagen')) {
                    $this->modelObj->uploadFile($request->file('imagen'));
                }

                PalabraClave::create([
                    'tipo_palabra_clave_id' => TipoPalabraClave::PRODUCTO,
                    'palabra' => $this->modelObj->nombre,
                ]);

                DB::commit();

                $this->response_info['show']    = true;
                $this->response_info['message'] = 'Datos guardados exitosamente.';
                $this->response_info['type']    = 'success';
            } else {
                return redirect($this->resource . '/create')
                    ->withErrors($this->modelObj->validator)
                    ->withInput();
            }
        } catch (Exception $e) {
            DB::rollBack();

            return redirect($this->resource . '/create')
                ->withErrors([Helper::getMessageIfDebug($e)])
                ->withInput();
        }

        $request->session()->flash('response_info', $this->response_info);

        return redirect($this->resource);
    }

    public function update(Request $request, $id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $data = Helper::stripTagsArray($request->input());

        if (empty($data['usuario_id'])) {
            unset($data['usuario_id']);
        }

        try {
            DB::beginTransaction();

            if (empty($this->modelObj)) {
                throw new Exception('Datos no disponibles.', 404);
            }

            $this->validPolicy('update', $this->modelObj);

            $this->modelObj->fill($data);

            if ($this->modelObj->isValid($modelCls::DO_UPDATE)) {
                $this->modelObj->save();

                if ($request->hasFile('imagen')) {
                    $this->modelObj->uploadFile($request->file('imagen'));
                }

                DB::commit();

                $this->response_info['show']    = true;
                $this->response_info['message'] = 'Datos actualizados exitosamente.';
                $this->response_info['type']    = 'success';
            } else {
                return redirect($this->resource . '/' . $this->modelObj->id . '/edit')
                    ->withErrors($this->modelObj->validator)
                    ->withInput();
            }
        } catch (Exception $e) {
            DB::rollBack();
            return redirect($this->resource . '/' . $this->modelObj->id . '/edit')
                ->withErrors([Helper::getMessageIfDebug($e)])
                ->withInput();
        }

        $request->session()->flash('response_info', $this->response_info);

        return redirect($this->resource);
    }

    /**
     * Devuelve el listado de productos que pertenecen a una marca determinada
     *
     * @param  lluminate\Http\Request $request [description]
     * @return json
     */
    public function getByMarca(Request $request)
    {
        return response()->json(
            Producto::orderBy('nombre', 'asc')
                ->where('activo', 1)
                ->where('marca_id', $request->input('id'))
                ->get()
                ->pluck('nombre', 'id')
        );
    }

    /**
     * Obtiene la imagen del producto
     *
     * @param  lluminate\Http\Request $request
     * @return json
     */
    public function getImagen(Request $request)
    {
        $producto = Producto::where('id', $request->input('id'))->first();

        return response()->json([
            'imagen' => $producto ? asset($producto->getImagen()) : '',
        ]);
    }
}
