<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\BaseWebController;
use App\Models\Campana;
use App\Models\Genero;
use App\Models\Industria;
use App\Models\Influenciador;
use App\Models\LogEvento;
use App\Models\Marca;
use App\Models\Producto;
use App\Models\RangoEtario;
use App\Models\SiNo;
use App\Models\TipoCampana;
use App\Models\TipoInfluenciador;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CampanaController extends BaseWebController
{
    public $modelClass = 'Campana';
    public $resource   = 'campanas';
    public $title_sin  = 'Campaña';
    public $title_plu  = 'Campañas';
    public $order_by   = 'nombre';

    public function transformFecha(Request $request)
    {
        // removes all NULL, FALSE and Empty Strings but leaves 0 (zero) values
        // $request->replace(array_filter($request->input(), 'strlen'));

        // Convertimos las fecha de formato dd/mm/yyyy a yyyy/mmm/ddd
        // porque es el formato que soporta mysql
        if (!empty($request->input('fecha_desde'))) {
            $request->merge([
                'fecha_desde' => Carbon::createFromFormat('d-m-Y', $request->input('fecha_desde'))
                                 ->toDateString(),
            ]);
        }

        if (!empty($request->input('fecha_hasta'))) {
            $request->merge([
                'fecha_hasta' => Carbon::createFromFormat('d-m-Y', $request->input('fecha_hasta'))
                                 ->toDateString(),
            ]);
        }
    }

    public function onBeforeStore(Request $request)
    {
        parent::onBeforeStore($request);
        $this->transformFecha($request);
    }

    public function onBeforeUpdate(Request $request)
    {
        parent::onBeforeUpdate($request);
        $this->transformFecha($request);
    }

    public function onBeforeEdit(Request $request)
    {
        parent::onBeforeEdit($request);

        if (!empty($this->modelObj)){
            // Convertimos las fecha de formato yyyy-mmm-dd a dd-mm-yyyy
            $this->modelObj->fecha_desde = Carbon::createFromFormat('Y-m-d', $this->modelObj->fecha_desde)
                                           ->format('d-m-Y');
            $this->modelObj->fecha_hasta = Carbon::createFromFormat('Y-m-d', $this->modelObj->fecha_hasta)
                                           ->format('d-m-Y');
            $this->modelObj->hashtags = explode(' ', $this->modelObj->hashtags);
        }
    }

    public function getCatalogs()
    {
        $this->catalogs   = Campana::getCatalogs() + Influenciador::getCatalogs();
        Campana::$labels += Influenciador::$labels;
    }

    public function onAfterCreate($request = null, $data = null)
    {
        $this->catalogs['genero']             = collect(Genero::$generos);
        $this->catalogs['si_no']              = SiNo::$opciones;
        $this->catalogs['industria']          = collect(Industria::get()->toArray());
        $this->catalogs['tipo_campana']       = collect(TipoCampana::get()->toArray());
        $this->catalogs['rango_etario']       = collect(RangoEtario::get()->toArray());
        $this->catalogs['tipo_influenciador'] = collect(TipoInfluenciador::get()->toArray());

        // Se agrega elementos al inicio, porque selectToList detecta como selected al primer elemento
        // aun y cuando no esta seleccionado, esto pasa al cargar la página
        $this->catalogs['genero']->prepend(['id'=>'', 'genero'=>'', 'icono'=>'']);
        $this->catalogs['industria']->prepend(['id'=>'', 'nombre'=>'', 'icono'=>'']);
        $this->catalogs['tipo_campana']->prepend(['id'=>'', 'tipo'=>'', 'icono'=>'']);
        $this->catalogs['rango_etario']->prepend(['id'=>'', 'descripcion'=>'', 'icono'=>'']);
        $this->catalogs['tipo_influenciador']->prepend(['id'=>'', 'descripcion'=>'', 'icono'=>'']);
    }

    public function onAfterEdit($request = null, $data = null)
    {
        $this->catalogs['genero']             = collect(Genero::$generos);
        $this->catalogs['si_no']              = SiNo::$opciones;
        $this->catalogs['industria']          = collect(Industria::get()->toArray());
        $this->catalogs['tipo_campana']       = collect(TipoCampana::get()->toArray());
        $this->catalogs['rango_etario']       = collect(RangoEtario::get()->toArray());
        $this->catalogs['tipo_influenciador'] = collect(TipoInfluenciador::get()->toArray());

        // Se agrega elementos al inicio, porque selectToList detecta como selected al primer elemento
        // aun y cuando no esta seleccionado, esto pasa al cargar la página
        $this->catalogs['genero']->prepend(['id'=>'', 'genero'=>'', 'icono'=>'']);
        $this->catalogs['industria']->prepend(['id'=>'', 'nombre'=>'', 'icono'=>'']);
        $this->catalogs['tipo_campana']->prepend(['id'=>'', 'tipo'=>'', 'icono'=>'']);
        $this->catalogs['rango_etario']->prepend(['id'=>'', 'descripcion'=>'', 'icono'=>'']);
        $this->catalogs['tipo_influenciador']->prepend(['id'=>'', 'descripcion'=>'', 'icono'=>'']);
    }

    public function onBeforeIndex(Request $request)
    {
        parent::onBeforeIndex($request);

        if ($request->has('filter.nombre')) {
            $this->filter[] = ['nombre', 'like', '%'.Helper::stripTags($request->input('filter.nombre')).'%'];
        }

        if ($request->has('filter.agencia_id')) {
            $this->filter[] = ['agencia_id', '=', $request->input('filter.agencia_id')];
        }

        if ($request->has('filter.tipo_campana_id')) {
            $this->filter[] = ['tipo_campana_id', '=', $request->input('filter.tipo_campana_id')];
        }

        if ($request->has('filter.industria_id')) {
            $this->filter[] = ['industria_id', '=', $request->input('filter.industria_id')];
        }

        if ($request->has('filter.fecha_desde')) {
            $this->filter[] = [
                'fecha_desde',
                '>=',
                Carbon::createFromFormat(
                    'd-m-Y',
                    Helper::stripTags($request->input('filter.fecha_desde'))
                )->format('Y-m-d')
            ];
        }

        if ($request->has('filter.fecha_hasta')) {
            $this->filter[] = [
                'fecha_hasta',
                '<=',
                Carbon::createFromFormat(
                    'd-m-Y',
                    Helper::stripTags($request->input('filter.fecha_hasta'))
                )->format('Y-m-d')
            ];
        }

        $this->order_by = 'created_at DESC, fecha_desde DESC, fecha_hasta DESC, nombre ASC';
    }

    public function index(Request $request)
    {
        // Cuando un tipo de usuario Agencia se loguea, solo le mostramos las campañas que le pertenecen
        if (Auth::user()->isAgencia()) {
            $this->filter[] = ['agencia_id', '=', Auth::user()->agencia->id];
        }

        return parent::index($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $model    = new $modelCls;

        $this->validPolicy('create', $modelCls);

        $this->onAfterCreate();

        if (Auth::user()->isAgencia()) {
            $this->catalogs['marca'] = Marca::orderBy('nombre', 'asc')
                ->where('agencia_id', Auth::user()->agencia->id)
                ->where('activo', 1)
                ->get();

            $this->catalogs['producto'] = Producto::orderBy('nombre', 'asc')
                ->whereIn('marca_id', $this->catalogs['marca']->pluck('id'))
                ->where('activo', 1)
                ->get()
                ->prepend(['id'=>'', 'nombre'=>'Producto'])
                ->pluck('nombre', 'id');

            $this->catalogs['marca'] = $this->catalogs['marca']->prepend(['id'=>'', 'nombre'=>'Marca'])
                ->pluck('nombre', 'id');
        }

        if (Auth::user()->isMarca()) {
            $this->catalogs['producto'] = Producto::orderBy('nombre', 'asc')
                ->where('marca_id', Auth::user()->marca->id)
                ->where('activo', 1)
                ->get()
                ->prepend(['id'=>'', 'nombre'=>'Producto'])
                ->pluck('nombre', 'id');
        }

        return view($this->resource.'.create', [
            'model'         => $model,
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => $this->title_sin,
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    public function store(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        $this->validPolicy('create', $modelCls);

        try {
            DB::beginTransaction();

            $this->modelObj = new $modelCls;
            $this->modelObj->activo = 1; // Activo default = 1

            $this->modelObj->fill(Helper::stripTagsArray(
                $request->except('influenciadores', '_token', 'hashtag')
            ));

            if (Auth::user()->isAgencia()) {
                $this->modelObj->agencia_id = Auth::user()->agencia->id;
            }

            if (Auth::user()->isMarca()) {
                $this->modelObj->agencia_id = Auth::user()->marca->agencia->id;
                $this->modelObj->marca_id = Auth::user()->marca->id;
            }

            if (!$this->modelObj->isValid($modelCls::DO_CREATE)) {
                throw new Exception('Error al procesar los datos. ' .
                    Helper::errorsToString($this->modelObj->errors), 422);
            }

            $this->modelObj->save();
            $this->modelObj->saveInfluenciadores($request->input('influenciadores'));

            // store file
            if ($request->hasFile('terminos') && $request->file('terminos')->isValid()) {
                $extension = $request->file('terminos')->getClientOriginalExtension();
                $request->terminos->storeAs('campanas', $this->modelObj->id . '.' . $extension, 'local');

                // saving file extension for latter retrieving
                $this->modelObj->terminos_extension = $extension;
                $this->modelObj->save();
            }

            $this->modelObj->sendCampanaToBackend('post', true);

            LogEvento::saveRegistroCampana($this->modelObj->id);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        $this->onAfterStore($request);

        return response()->jsonSuccess([]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        if (empty($this->modelObj)) {
            $this->response_info['error']   = true;
            $this->response_info['type']    = 'danger';
            $this->response_info['message'] = 'Datos no disponibles.';

            request()->session()->flash('response_info', $this->response_info);
            return redirect($this->resource);
        }

        $this->validPolicy('view', $this->modelObj);

        $this->onAfterShow($id, $this->response_info);

        return view($this->resource.'.show', [
            'model'         => $this->modelObj,
            // 'report'        => $this->modelObj->getReportFromBackend(),
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => $this->title_sin,
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function estadisticas($id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        if (empty($this->modelObj)) {
            $this->response_info['error']   = true;
            $this->response_info['type']    = 'danger';
            $this->response_info['message'] = 'Datos no disponibles.';

            request()->session()->flash('response_info', $this->response_info);
            return redirect($this->resource);
        }

        $this->validPolicy('view', $this->modelObj);

        $this->onAfterShow($id, $this->response_info);

        return view($this->resource.'.estadisticas_v2', [
            'model'         => $this->modelObj,
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        if (empty($this->modelObj)) {
            $this->response_info['error']   = true;
            $this->response_info['type']    = 'danger';
            $this->response_info['message'] = 'Datos no disponibles.';

            request()->session()->flash('response_info', $this->response_info);
            return redirect($this->resource);
        }

        $this->validPolicy('update', $this->modelObj);

        $this->onAfterEdit($id);

        if (Auth::user()->isAgencia()) {
            $this->catalogs['marca'] = Marca::orderBy('nombre', 'asc')
                ->where('agencia_id', Auth::user()->agencia->id)
                ->where('activo', 1)
                ->get();

            $this->catalogs['producto'] = Producto::orderBy('nombre', 'asc')
                ->whereIn('marca_id', $this->catalogs['marca']->pluck('id'))
                ->where('activo', 1)
                ->get()
                ->prepend(['id'=>'', 'nombre'=>$modelCls::$labels['producto_id']])
                ->pluck('nombre', 'id');

            $this->catalogs['marca'] = $this->catalogs['marca']->prepend(['id'=>'', 'nombre'=>$modelCls::$labels['marca_id']])->pluck('nombre', 'id');
        }

        if (Auth::user()->isMarca()) {
            $this->catalogs['producto'] = Producto::orderBy('nombre', 'asc')
                ->where('marca_id', Auth::user()->marca->id)
                ->where('activo', 1)
                ->get()
                ->prepend(['id'=>'', 'nombre'=>$modelCls::$labels['producto_id']])
                ->pluck('nombre', 'id');
        }

        return view($this->resource.'.edit', [
            'model'         => $this->modelObj,
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => $this->title_sin,
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    public function update(Request $request, $id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        try {
            DB::beginTransaction();

            if (empty($this->modelObj)) {
                throw new Exception('Datos no disponibles.', 404);
            }

            $this->validPolicy('update', $this->modelObj);

            $this->modelObj->fill(Helper::stripTagsArray(
                $request->except('influenciadores', '_token', 'hashtag')
            ));

            if (!$this->modelObj->isValid($modelCls::DO_UPDATE)) {
                throw new Exception('Error al procesar los datos. ' .
                    Helper::errorsToString($this->modelObj->errors), 422);
            }

            $this->modelObj->save();
            $this->modelObj->saveInfluenciadores(
                $request->input('influenciadores'),
                true,
                $request->input('influenciadores_to_delete', [])
            );

            // store file
            if ($request->hasFile('terminos') && $request->file('terminos')->isValid()) {
                \Storage::delete('campanas/' . $this->modelObj->id . '.' . $this->modelObj->terminos_extension);

                $extension = $request->file('terminos')->getClientOriginalExtension();
                $request->terminos->storeAs('campanas', $this->modelObj->id . '.' . $extension, 'local');

                // saving file extension for latter retrieving
                $this->modelObj->terminos_extension = $extension;
                $this->modelObj->save();
            }

            $this->modelObj->sendCampanaToBackend('put');

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        $this->onAfterStore($request);

        return response()->jsonSuccess([]);
    }

    /**
     * download campana's file
     *
     * @return Illuminate\Http\Response
     */
    public function downloadTerminos($campanaId)
    {
        $campana = Campana::find($campanaId);

        $path = storage_path() . '/app/campanas/' . $campanaId . '.' . $campana->terminos_extension;
        return response()->download($path, 'terminos_y_condiciones_' . $campanaId . '.' . $campana->terminos_extension);
    }

    /**
     * builds data for top location
     *
     * @param string $campanaId
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function graficaTopLocations($campanaId)
    {
        $response = [];
        $colors   = ['#0B0339', '#92D5F7', '#6B649B', '#00BDC5', '#544991'];

        if ($campanaId == 86) {
            return response()->jsonSuccess([
                'status' => 'success',
                'yAxis'  => ['Perú', 'España', 'Chile', 'EE.UU.', 'México'],
                'data'   => [
                    ['value' => 2, 'name' => 'Peru', 'itemStyle' => ['normal' => ['color' => $colors[0]]]],
                    ['value' => 5, 'name' => 'España', 'itemStyle' => ['normal' => ['color' => $colors[1]]]],
                    ['value' => 21, 'name' => 'Chile', 'itemStyle' => ['normal' => ['color' => $colors[2]]]],
                    ['value' => 109, 'name' => 'EE.UU.', 'itemStyle' => ['normal' => ['color' => $colors[3]]]],
                    ['value' => 493, 'name' => 'México', 'itemStyle' => ['normal' => ['color' => $colors[4]]]]
                ]
            ]);
        }

        $distribucionPaises = Campana::getTopLocations((int) $campanaId);
        if ($distribucionPaises->count() === 0) {
            $response['status']  = 'error';
            $response['message'] = 'No se encontró campaña con el id ' . $campanaId;

            return response()->jsonFormat($response, 404);
        }

        foreach ($distribucionPaises as $index => $pais) {
            $response['yAxis'][] = $pais->pais;
            $response['data'][]  = [
                'value'     => $pais->total,
                'name'      => $pais->pais,
                'itemStyle' => ['normal' => ['color' => $colors[$index]]]
            ];
        }

        $response['status'] = 'success';
        return response()->jsonSuccess($response);
    }

    /**
     * builds data for posts per hour
     *
     * @param string $campanaId
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function graficaPostsPerHour($campanaId)
    {
        $response = [];
        $posts    = Campana::getPostsPerHour((int) $campanaId);

        if (count($posts) === 0) {
            $response['status']  = 'error';
            $response['message'] = 'No se encontró campaña con el id ' . $campanaId;

            return response()->jsonNotFound($response);
        }

        $response['data'] = [];
        $schedules        = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];

        $encontrado = false;
        foreach ($schedules as $schedule) {
            foreach ($posts as $post) {
                if ($schedule === $post->hora) {
                    $response['data'][]  = [
                        'value'     => $post->total,
                        'itemStyle' => ['normal' => ['color' => '#92D5F7']]
                    ];
                    $response['xAxis'][] = $post->hora . ':00';
                    $encontrado = true;
                    break;
                } else {
                    $encontrado = false;
                }
            }

            if (!$encontrado) {
                $response['data'][]  = [
                    'value'     => 0,
                    'itemStyle' => ['normal' => ['color' => '#92D5F7']]
                ];
                $response['xAxis'][] = $schedule . ':00';
            }
        }

        $response['status'] = 'success';
        return response()->jsonSuccess($response);
    }

    /**
     * gets data for shares on social
     *
     * @param string $campanaId
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function graficaShareSocial($campanaId)
    {
        $response     = [];
        $campana      = Campana::find((int) $campanaId);
        $interactions = $campana->getCountInteractionsBySocialNetwork();

        $colors = [1 => '#92D5F7', 2 => '#6E95E4', 3 => '#FCAA56', 4 => '#FF7F93'];

        foreach ($interactions as $index => $value) {
            if ($index === 5) {
                break;
            }

            $redName = '';
            switch ($index) {
                case 1:
                    $redName = 'Twitter';
                    break;

                case 2:
                    $redName = 'Facebook';
                    break;

                case 3:
                    $redName = 'Instagram';
                    break;

                case 4:
                    $redName = 'Youtube';
                    break;

                case 5:
                    $redName = 'Snapchat';
                    break;
            }

            $response['categories'][] = $redName;
            $response['data'][]       = [
                'value'     => $interactions[$index]['retweets'] + $interactions[$index]['favorites'] + $interactions[$index]['replies'],
                'name'      => $redName,
                'itemStyle' => ['normal' => ['color' => $colors[$index]]]
            ];
        }

        $response['status'] = 'success';
        return response()->jsonSuccess($response);
    }

    /**
     * gets data for shares on twitter graphic
     *
     * @param string $campanaId
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function graficaSharesTwitter($campanaId)
    {
        $response     = [];
        $campana      = Campana::find((int) $campanaId);
        $interactions = $campana->getCountInteractionsBySocialNetwork();

        /*if ($interactions->count() === 0) {
            $response['status']  = 'error';
            $response['message'] = 'No se encontró campaña con el id ' . $campanaId;

            return response()->jsonFormat($response);
        }*/

        $response['categories'] = ['Retweets', 'Favorites', 'Replies'];
        $response['data']       = [
            ['value' => $interactions[1]['retweets'], 'name' => 'Retweets', 'itemStyle' => ['normal' => ['color' => '#322774']]],
            ['value' => $interactions[1]['favorites'], 'name' => 'Favorites', 'itemStyle' => ['normal' => ['color' => '#6B649B']]],
            ['value' => $interactions[1]['replies'], 'name' => 'Replies', 'itemStyle' => ['normal' => ['color' => '#D8D8D8']]]
        ];

        $response['status'] = 'success';
        return response()->jsonSuccess($response);
    }
}