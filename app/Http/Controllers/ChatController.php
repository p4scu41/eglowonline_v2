<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseResourceController;
use App\Models\Mensaje;
use App\Models\User;
use App\Models\Campana;
use App\Models\Influenciador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\AgencySentMessageToInfluencer;
use App\Notifications\InfluencerSentMessageToAgency;

class ChatController extends BaseResourceController
{
    public $modelClass = 'Mensaje';
    public $modelObj = null;
    public $resource = 'chat';

    public function onBeforeIndex(Request $request)
    {
        parent::onBeforeIndex($request);

        // Solo obtenemos la primera pagina de la lista de mensajes, los primeros 15 registros
        $request->merge(['page' => 1]);

        if ($request->has('campana_id')) {
            $this->filter['campana_id'] = $request->input('campana_id');
        }
    }

    public function index(Request $request)
    {
        $modelCls  = $this->modelNamespace . $this->modelClass;
        $paginator = [];
        // $this->filter['usuario_id'] = Auth::id();

        if ($request->has('usuario_id')) {
            $this->filter[] = ['usuario_id', '=', $request->input('usuario_id'), true];
            $this->filter[] = ['created_by', '=', $request->input('usuario_id'), true];
        }

        try {
            $result = Mensaje::getMensajesUsuarioCampana($request->input('campana_id'), $request->input('usuario_id'));

            foreach ($result as $item) {
                $item->setLeido();
            }

            $result = $result->toArray();
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'data' => $result,
            'extra' => $paginator
        ]);
    }

    public function store(Request $request)
    {
        // call parent's action first
        parent::store($request);

        $response = ['status' => 'success'];

        // check to which person send the notification
        $usuario       = User::find((int)$request->get('usuario_id'));
        $campana       = Campana::find((int)$request->get('campana_id'));
        $influenciador = Influenciador::find((int)$request->get('influenciador_id'));
        $usr_ses   = \Auth::user();
        $sendMailToIinfluencer = false;

        if (\Auth::check()) {
            $sendMailToIinfluencer = $usr_ses->isAdministrador() || $usr_ses->isAgencia() || $usr_ses->isMarca() || $usr_ses->isProducto();
        }

        // 5.- agency sent message to influencer
        if ($usuario->isAgencia() || $usuario->isAdministrador() || $sendMailToIinfluencer) {
            $influenciador->usuario->notify(new AgencySentMessageToInfluencer([
                'campaign_name' => $campana->nombre,
                'agency_name'   => $usuario->agencia ? $usuario->agencia->nombre : ($usr_ses->agencia ? $usr_ses->agencia->nombre : $usr_ses->nombre),
                'campaign_id'   => $campana->id
            ]));
        }

        // 6.- Influencer sent message to agency
        if ($usuario->isCelebridad() && !$sendMailToIinfluencer) {
            $campana->agencia->usuario->notify(new InfluencerSentMessageToAgency([
                'campaign_name'   => $campana->nombre,
                'influencer_name' => $usuario->influenciador->nombre,
                'campaign_id'     => $campana->id
            ]));
        }

        return response()->json($response);
    }
}