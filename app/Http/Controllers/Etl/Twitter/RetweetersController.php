<?php

namespace App\Http\Controllers\Etl\Twitter;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Etl\Twitter\Account;
use App\Models\Publicacion;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class RetweetersController
 *
 * @package App\Http\Controllers\Etl\Twitter
 * @category Controller
 * @author Gerardo Adrián Gómez Ruiz <gerardo@eglow.mx>
 */
class RetweetersController extends Controller
{
    /**
     * logger
     *
     * @var string
     */
    private $logger;

    /**
     * initalizes logger
     *
     * @method __construct
     */
    public function __construct()
    {
        $this->logger = Helper::createLogger('retweeters.log');
    }

    /**
     * stores new retweeters
     *
     * @method store
     *
     * @param string $id
     * @param Request $request
     *
     * @return void
     */
    public function store($id, Request $request)
    {
        try {
            if (env('APP_DEBUG') === 'true') {
                $this->logger->info('Publicacion::addRetweeters('.$id.')', json_encode($request->all()));
            }

            DB::beginTransaction();
            $publicacion = Publicacion::where('id_publicacion_red_social', $id)-first();

            $publicacion->addRetweeters($request->input('retweeters'));
            $publicacion->save();
        } catch (Exception $e) {
            $this->logger->info('Excepción encontrada:', json_encode($e->getLine() . ' ' . $e->getMessage()));
            $this->logger->info('', json_encode($e->getTraceAsString()));
        }
    }

    /**
     * stores or updates accounts
     *
     * @method perfiles
     *
     * @param string $id
     * @param Request $request
     *
     * @return void
     */
    public function perfiles($id, Request $request)
    {
        try {
            if (env('APP_DEBUG') === 'true') {
                $this->logger->info('Account::save('.$id.')', json_encode($request->all()));
            }

            DB::beginTransaction();
           foreach($request->input('perfiles') as $perfil) {
                $account = Account::find($perfil['id']);

                if (empty($account)) {
                    $account = new Account;
                }

                $account->fill($perfil);
                $account->save();
            }

        } catch (Exception $e) {
            $this->logger->info('Excepción encontrada:', json_encode($e->getLine() . ' ' . $e->getMessage()));
            $this->logger->info('', json_encode($e->getTraceAsString()));
        }
    }
}