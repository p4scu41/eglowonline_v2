<?php

namespace App\Http\Controllers\Etl\Twitter;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Etl\Twitter\Account;
use App\Models\Etl\Twitter\Reply;
use App\Models\Etl\Twitter\GeneroPublicacion;
use App\Models\Etl\Twitter\PaisPublicacion;
use App\Models\Etl\Twitter\HashtagPublicacion;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Procesa los datos enviados por el ETL Twitter
 *
 * @package App
 * @subpackage Http\Controllers\Etl\Twitter
 * @category Controller
 * @author Pascual
 */
class ReplyController extends Controller
{
    public static $debug = true;
    public $logger;

    public function __construct()
    {
        $this->logger = Helper::createLogger('replies.log');
    }

    /**
     * Guarda el listado de Replies en la tabla Reply
     *
     * @param int     $id      ID de la campaña
     * @param Request $request Instancia de Request con los datos enviados
     * @return void
     */
    public function store($id, Request $request)
    {
        $input = $request->input();

        if (self::$debug) {
            $this->logger->info('Reply::store('.$id.')', $input);
        }

        foreach($input['replies'] as $reply) {
            try {
                $model = Reply::find($reply['id']);

                if (empty($model)) {
                    $model = new Reply();
                }
                $reply['publicacion_id'] = $id;
                $reply['created_at'] = date('Y-m-d H:i:s');

                $model->fill($reply);
                $model->save();
            } catch (Exception $e) {
                $logger->error($e->getCode().' - '.$e->getMessage());
            }
        }

        /*$data = collect($input['replies'])
            ->map(function ($item, $key) use ($id){
                $item['publicacion_id'] = $id;
                $item['created_at'] = date('Y-m-d H:i:s');

                return $item;
            });

        Reply::insert($data->toArray());*/

        $data = collect($input['hashtags'])
            ->map(function ($item, $key) use ($id) {
                $item['publicacion_id'] = $id;
                $item['created_at'] = date('Y-m-d H:i:s');
                $item['ultima_revision'] = date('Y-m-d H:i:s');
                $item['cantidad'] = $item['total'];
                unset($item['total']);

                return $item;
            });

        HashtagPublicacion::insert($data->toArray());
    }

    /**
     * Guarda la distribución de género de la publicación
     *
     * @param int     $id      ID de la campaña
     * @param Request $request Instancia de Request con los datos enviados
     * @return void
     */
    public function distGenero($id, Request $request)
    {
        $input = $request->input();

        if (self::$debug) {
            $this->logger->info('Reply::distGenero('.$id.')', $input);
        }

        $genero_publicacion = new GeneroPublicacion();
        $data = [
            'publicacion_id' => $id,
            'hombres' => $input['distribucion_genero']['hombres'],
            'mujeres' => $input['distribucion_genero']['mujeres'],
            'ultima_revision' => date('Y-m-d H:i:s'),
        ];

        $genero_publicacion->fill($data);
        $genero_publicacion->save();
    }

    /**
     * Guarda la distribución de país de la publicación
     *
     * @param int     $id      ID de la campaña
     * @param Request $request Instancia de Request con los datos enviados
     * @return void
     */
    public function distPais($id, Request $request)
    {
        $input = $request->input();

        if (self::$debug) {
            $this->logger->info('Reply::distPais('.$id.')', $input);
        }

        $data = collect($input['distribucion_pais'])
            ->map(function ($item, $key) use ($id) {
                $item['created_at'] = date('Y-m-d H:i:s');
                $item['publicacion_id'] = $id;
                $item['ultima_revision'] = $item['created_at'];

                return $item;
            });

        PaisPublicacion::insert($data->toArray());
    }

    /**
     * Guarda los usuarios que hicieron reply
     *
     * @param int     $id      ID de la campaña
     * @param Request $request Instancia de Request con los datos enviados
     * @return void
     */
    public function perfiles($id, Request $request)
    {
        $input = $request->input();

        if (self::$debug) {
            $this->logger->info('Reply::perfiles('.$id.')', $input);
        }

        foreach($input['perfiles'] as $perfil) {
            $account = Account::find($perfil['id']);

            if (empty($account)) {
                $account = new Account();
            }

            $account->fill($perfil);
            $account->save();
        }
    }
}
