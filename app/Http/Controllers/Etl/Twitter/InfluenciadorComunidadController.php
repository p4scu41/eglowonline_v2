<?php

namespace App\Http\Controllers\Etl\Twitter;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Etl\Twitter\Influenciador;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class InfluenciadorComunidadController extends Controller
{
    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function topTweets($id, Request $request)
    {
        $data = null;

        try {
            DB::beginTransaction();

            Influenciador::saveTopTweetsComunidad($id, $request->input());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\InfluenciadorController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function palabrasClave($id, Request $request)
    {
        $data = null;

        try {
            DB::beginTransaction();

            Influenciador::savePalabrasClaveComunidad($id, $request->input());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\InfluenciadorController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function hashtags($id, Request $request)
    {
        $data = null;

        try {
            DB::beginTransaction();

            Influenciador::saveHashtagsComunidad($id, $request->input());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\InfluenciadorController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }
}
