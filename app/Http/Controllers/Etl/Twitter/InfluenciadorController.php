<?php

namespace App\Http\Controllers\Etl\Twitter;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Etl\Twitter\Influenciador;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class InfluenciadorController extends Controller
{
    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function perfil($id, Request $request)
    {
        $data = null;

        try {
            DB::beginTransaction();

            Influenciador::savePerfilInfluenciador($id, $request->input());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\InfluenciadorController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function topTweets($id, Request $request)
    {
        $data = $request->input();

        try {
            DB::beginTransaction();

            if (isset($data['totales'])) {
                $data['totales']['ultima_revision'] = $data['ultima_revision'];
                Influenciador::saveMultimedias($id, $data['totales']);
                unset($data['totales']);
            }

            Influenciador::saveTopTweets($id, $data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\InfluenciadorController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function topFollowers($id, Request $request)
    {
        $data = $request->input();

        try {
            DB::beginTransaction();

            Influenciador::saveTopFollowers($id, $data);

            DB::commit();

            DB::beginTransaction();

            if (isset($data['followers_fakes']) || isset($data['followers_revisados'])) {
                Influenciador::updateFollowersFakesRevisados($id, $data);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\InfluenciadorController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function palabrasClave($id, Request $request)
    {
        $data = null;

        try {
            DB::beginTransaction();

            Influenciador::savePalabrasClave($id, $request->input());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\InfluenciadorController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function hashtags($id, Request $request)
    {
        $data = null;

        try {
            DB::beginTransaction();

            Influenciador::saveHashtags($id, $request->input());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\InfluenciadorController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function distribucionGenero($id, Request $request)
    {
        $data = null;

        try {
            DB::beginTransaction();

            Influenciador::saveDistribucionGenero($id, $request->input());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\InfluenciadorController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function followersActivos($id, Request $request)
    {
        $data = null;

        try {
            DB::beginTransaction();

            Influenciador::updateFollowersActivos($id, $request->input());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\InfluenciadorController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function distribucionPais($id, Request $request)
    {
        $data = null;

        try {
            DB::beginTransaction();

            Influenciador::saveDistribucionPais($id, $request->input());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\InfluenciadorController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }
}
