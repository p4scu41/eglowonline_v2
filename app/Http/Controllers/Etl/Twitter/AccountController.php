<?php

namespace App\Http\Controllers\Etl\Twitter;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Etl\Twitter\Account;
use Illuminate\Http\Request;

/**
 * Procesa los datos enviados por el ETL Twitter
 *
 * @package App
 * @subpackage Http\Controllers\Etl\Twitter
 * @category Controller
 * @author Pascual
 */
class AccountController extends Controller
{
    public static $debug = true;
    public $logger;

    public function __construct()
    {
        $this->logger = Helper::createLogger('accounts.log');
    }

    /**
     * Guarda o alctualiza el Account
     *
     * @param Request $request Instancia de Request con los datos enviados
     * @return void
     */
    public function store(Request $request)
    {
        $input = $request->input();

        if (self::$debug) {
            $this->logger->info('Account::store() ', $input);
        }

        $account = Account::where('id', $input['id'].'')->first();

        if (empty($account)) {
            $account = new Account();
        }

        $account->fill($input);
        $account->id = $input['id'] . '';
        $account->save();
    }
}
