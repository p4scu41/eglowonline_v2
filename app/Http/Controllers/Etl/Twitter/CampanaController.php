<?php

namespace App\Http\Controllers\Etl\Twitter;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Etl\Twitter\Campana;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Procesa los datos enviados por el ETL Twitter
 *
 * @package App
 * @subpackage Http\Controllers\Etl\Twitter
 * @category Controller
 * @author Pascual
 */
class CampanaController extends Controller
{
    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function topTweets($id, Request $request)
    {
        $data = $request->input();

        try {
            DB::beginTransaction();

            if (isset($data['totales'])) {
                $data['totales']['ultima_revision'] = $data['ultima_revision'];
                Campana::saveMultimedias($id, $data['totales']);
                unset($data['totales']);
            }

            Campana::saveTopTweets($id, $data);

            if (Helper::isSetAndNotEmpty($data, 'perfiles')) {
                Campana::savePerfilesReplies($id, $data['perfiles']);
            }

            if (Helper::isSetAndNotEmpty($data, 'replies')) {
                Campana::saveReplies($id, $data['replies']);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\CampanaController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function palabrasClave($id, Request $request)
    {
        $data = null;

        try {
            DB::beginTransaction();

            Campana::savePalabrasClave($id, $request->input());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\CampanaController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function distribucionGenero($id, Request $request)
    {
        $data = null;

        try {
            DB::beginTransaction();

            Campana::saveDistribucionGenero($id, $request->input());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\CampanaController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID influenciador
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function distribucionPais($id, Request $request)
    {
        $data = null;

        try {
            DB::beginTransaction();

            Campana::saveDistribucionPais($id, $request->input());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\CampanaController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID Campaña
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function hashtags($id, Request $request)
    {
        $data = null;

        try {
            DB::beginTransaction();

            Campana::saveHashtags($id, $request->input());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\CampanaController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Save the data
     *
     * @param integer $id  ID Campaña
     * @param Illuminate\Http\Request $request  The data bag sended
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function topFollowers($id, Request $request)
    {
        $data = $request->input();

        try {
            DB::beginTransaction();

            Campana::saveTopFollowers($id, $data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\CampanaController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }
}
