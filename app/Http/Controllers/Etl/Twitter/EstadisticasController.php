<?php
namespace App\Http\Controllers\Etl\Twitter;

use App\Http\Controllers\Controller;
use App\Models\Etl\Twitter\HashtagComunidad;
use App\Models\Etl\Twitter\PalabraClaveComunidad;
use Illuminate\Http\Request;

/**
 * class for processing analytics from influencer
 *
 * @package App
 * @subpackage Http\Controllers\Etl\Twitter
 * @category Controller
 * @author Gerardo Adrián Gómez Ruiz <gerardo@eglow.mx>
 */
class EstadisticasController extends Controller
{
    /**
     * obtain and create array for top hashtags graphic
     *
     * @param Request $request
     * @return Illuminate\Response\JsonResponse
     */
    public function topHashtagsComunidad(Request $request)
    {
        $respuesta   = ['estatus' => 'OK'];
        $topHashtags = HashtagComunidad::getTopHashtags((int) $request->get('influenciadorId'));
        if ($topHashtags->count() === 0) {
            $respuesta['estatus'] = 'fail';
            $respuesta['mensaje'] = 'No existen datos para presentar';

            return response()->json($respuesta);
        }

        $words = [];
        foreach ($topHashtags as $hashtag) {
            $words[] = [
                'text'   => $hashtag->hashtag,
                'weight' => ($hashtag->cantidad / 10),
            ];
        }

        $respuesta['words'] = $words;

        return response()->json($respuesta);
    }

    /**
     * obtain and create array for top keywords graphic
     *
     * @param Request $request
     * @return Illuminate\Response\JsonResponse
     */
    public function topKeywordsComunidad(Request $request)
    {
        $respuesta   = ['estatus' => 'OK'];
        $topKeywords = PalabraClaveComunidad::getTopKeywords((int) $request->get('influenciadorId'));

        if ($topKeywords->count() === 0) {
            $respuesta['estatus'] = 'fail';
            $respuesta['mensaje'] = 'No existen datos para graficar';

            return response()->json($respuesta);
        }

        $words = [];
        foreach ($topKeywords as $topKeyword) {
            $words[] = [
                'text'   => $topKeyword->palabra,
                'weight' => $topKeyword->cantidad > 10 ? ($topKeyword->cantidad / 10) : $topKeyword->cantidad
            ];
        }

        $respuesta['words'] = $words;

        return response()->json($respuesta);
    }
}