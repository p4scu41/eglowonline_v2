<?php

namespace App\Http\Controllers\Etl\Twitter;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Publicacion;
use App\Models\Etl\Twitter\PublicacionEstadistica;
use DB;
use Exception;
use Illuminate\Http\Request;
use Log;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Logger;

/**
 * process data coming from back end
 *
 * @package App
 * @subpackage Http\Controllers\Etl\Twitter
 * @category Controller
 * @author Gerardo Adrián Gómez Ruiz <gerardo@eglow.mx>
 */
class PublicacionesController extends Controller
{
    public static $debug = true;

    /**
     * Save publicacion data on table
     *
     * @param integer $id  ID publicacion
     * @param Illuminate\Http\Request $request  The data bag sent
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function estadisticas($id, Request $request)
    {
        $logger = Helper::createLogger('estadisticas_publicaciones.log');

        if (self::$debug) {
            $logger->info('Publicaciones::estadisticas('.$id.')', $request->input());
        }

        $data = $request->input();
        // $data['replies'] = 0; // La cantidad de replies no se recibe en este endpoint

        try {
            DB::beginTransaction();

            // Obtenemos las últimas estadísticas recibidas
            $lastData = PublicacionEstadistica::where('publicacion_id', $id)
                ->orderBy('fecha', 'desc')
                ->limit(1)
                ->first();

            // No es necesario, porque este end point recibe todos los datos a las 12 am
            // Si solo se envia la cantidad de RTs y FVs
            /*if ($data['onlyTotalsRTFV']) {
                // Respaldamos el último dato de total_followers_rt
                if (!empty($lastData)) {
                    $data['total_followers_rt'] = !empty($lastData['total_followers_rt']) ? $lastData['total_followers_rt'] : $data['total_followers_rt'];
                }
            }*/

            $publicacion = Publicacion::find($id);

            if (!empty($publicacion)) {
                $publicacion->updated_at = date('Y-m-d H:i:s');
                $publicacion->save();
            }

            // Si no hay registro de estadisticas
            // Guardamos los datos
            if (empty($lastData)) {
                PublicacionEstadistica::create([
                    'publicacion_id'     => $id,
                    'retweets'           => $data['retweets'],
                    'favorites'          => $data['favorites'],
                    'total_followers_rt' => $data['total_followers_rt'],
                    'fecha'              => $data['ultima_revision'].' '.date('H:i:s')
                ]);
            }
            // De lo contrario
            // Solo guardar el registro en caso de que sea diferente al último recibido
            else if (
                $data['retweets'] != $lastData['retweets'] ||
                $data['favorites'] != $lastData['favorites'] ||
                $data['total_followers_rt'] != $lastData['total_followers_rt']
                ) {
                PublicacionEstadistica::create([
                    'publicacion_id'     => $id,
                    'retweets'           => $data['retweets'],
                    'favorites'          => $data['favorites'],
                    'total_followers_rt' => $data['total_followers_rt'],
                    'fecha'              => $data['ultima_revision'].' '.date('H:i:s')
                ]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\PublicacionesController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Guarda las estadísticas de las publicaciones cada 10 minutos
     *
     * @param Illuminate\Http\Request $request  The data bag sent
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function estadisticasdiezmin(Request $request)
    {
        $logger = Helper::createLogger('estadisticas_publicaciones_10min.log');

        if (self::$debug) {
            $logger->info('Publicaciones::estadisticasdiezmin()', $request->input());
        }

        $data = $request->input();

        foreach($data as $estadistica) {
            try {
                // Obtenemos las últimas estadísticas recibidas
                $lastData = PublicacionEstadistica::where('publicacion_id', $estadistica['publicacion_id'])
                    ->orderBy('fecha', 'desc')
                    ->limit(1)
                    ->first();

                // Respaldamos el último dato de total_followers_rt
                if (!empty($lastData)) {
                    $estadistica['total_followers_rt'] = !empty($lastData['total_followers_rt']) ? $lastData['total_followers_rt'] : $estadistica['total_followers_rt'];
                }

                $publicacion = Publicacion::find($estadistica['publicacion_id']);

                if (!empty($publicacion)) {
                    $publicacion->updated_at = date('Y-m-d H:i:s');
                    $publicacion->save();
                }

                // Si no hay registro de estadisticas
                // Guardamos los datos
                if (empty($lastData)) {
                    PublicacionEstadistica::create([
                        'publicacion_id'     => $estadistica['publicacion_id'],
                        'retweets'           => $estadistica['retweets'],
                        'favorites'          => $estadistica['favorites'],
                        'total_followers_rt' => $estadistica['total_followers_rt'],
                        'fecha'              => $estadistica['ultima_revision'].' '.date('H:i:s')
                    ]);
                }
                // De lo contrario
                // Solo guardar el registro en caso de que sea diferente al último recibido
                else if (
                    $estadistica['retweets'] != $lastData['retweets'] ||
                    $estadistica['favorites'] != $lastData['favorites'] ||
                    $estadistica['total_followers_rt'] != $lastData['total_followers_rt']
                    ) {
                    PublicacionEstadistica::create([
                        'publicacion_id'     => $estadistica['publicacion_id'],
                        'retweets'           => $estadistica['retweets'],
                        'favorites'          => $estadistica['favorites'],
                        'total_followers_rt' => $estadistica['total_followers_rt'],
                        'fecha'              => $estadistica['ultima_revision'].' '.date('H:i:s')
                    ]);
                }
            } catch (Exception $e) {
                Log::error('Etl\Twitter\PublicacionesController: ' . $e->getCode() . ' '. $e->getMessage());
                return response()->jsonException($e);
            }
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Update publication data on table
     *
     * @param integer $id  ID publicacion
     * @param Illuminate\Http\Request $request  The data bag sent
     *
     * @return JSON $request Data returned with Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $logger = Helper::createLogger('estadisticas_publicaciones.log');

        if (self::$debug) {
            $logger->info('Publicaciones::update('.$id.')', $request->input());
        }

        $data = null;

        try {
            DB::beginTransaction();
            $publicacion = Publicacion::find($id);

            if (!empty($publicacion)) {
                $publicacion->fill($request->input());

                $publicacion->save();
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Etl\Twitter\PublicacionesController: ' . $e->getCode() . ' '. $e->getMessage());
            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }
}