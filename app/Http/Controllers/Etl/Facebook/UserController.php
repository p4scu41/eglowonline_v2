<?php

namespace App\Http\Controllers\Etl\Facebook;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Models\Etl\Facebook\User;
use Illuminate\Support\Facades\Log;

/**
 * User ETL Facebook
 *
 * @package App\Http\Controllers
 * @subpackage Etl
 * @category Load
 * @author Pascual
 */
class UserController extends BaseFacebookController
{
    /**
     *
     * @param $id
     * @param $request
     */
    public function store(Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request->all();
        $user = new User;

        foreach($data['followers'] as $follower)
        {
            $user->storeUser([
                'id'   => $follower['id'],
                'name' => $follower['name']
            ]);
        }

        return response()->jsonSuccess(['result' => 'OK']);
    }
}
