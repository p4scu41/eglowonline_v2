<?php

namespace App\Http\Controllers\Etl\Facebook;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Log;

/**
 * Clase base para los controladores que reciben los datos del ETL Facebook
 *
 * @package App\Http\Controllers
 * @subpackage Etl
 * @category Load
 * @author Pascual
 */
class BaseFacebookController extends Controller
{
    public $logger;

    /**
     * Inicializa el logger
     *
     * @param object $instance
     * @param string $function
     *
     * @return void
     */
    public function initLogger($instance, $function)
    {
        $log_name = 'Facebook_' . Helper::getShortClassName(get_class($this)).'_'.$function.'.log';

        $this->logger = Helper::createLogger($log_name);
    }

    /**
     * Guarda los datos recibidos al archivo log
     *
     * @param Illuminate\Http\Request $request
     */
    public function logInputs(Request $request)
    {
        $data = json_encode($request->all());
        $route  = $request->route();

        if (!empty($route)) {
            if (!empty($route->parameter('id'))) {
                $data = 'ID: ' . $route->parameter('id') . ' => ' . $data;
            }
        }

        $this->logger->info($data);
    }
}
