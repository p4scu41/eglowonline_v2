<?php

namespace App\Http\Controllers\Etl\Facebook;

use App\Helpers\Helper;
use App\Models\Etl\Facebook\Influenciador;
use App\Models\Etl\Facebook\Post;
use App\Models\Etl\Facebook\Reaction;
use App\Models\Etl\Facebook\Statistic;
use App\Models\Etl\Facebook\Insights;
use DB;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Influenciador ETL Facebook
 *
 * @package App\Http\Controllers
 * @subpackage Etl
 * @category Load
 * @author Pascual
 */
class InfluenciadorController extends BaseFacebookController
{
    /**
     *
     * @param $id
     * @param $request
     */
    public function perfil($id, Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request->all();

        try {
            DB::beginTransaction();

            $perfil = $data['perfil'];
            $perfil['last_profile_review'] = $data['last_profile_review'];

            Influenciador::savePerfilInfluenciador($id, $perfil);
            Influenciador::saveDistribucionGenero($id, $perfil);
            Influenciador::saveDistribucionPais($id, $perfil);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error(Helper::getInfoException($e));

            return response()->jsonException($e);
        }

        return response()->jsonSuccess($data);
    }

    /**
     *
     * @param $id
     * @param $request
     */
    public function followers($id, Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request->all();

        Influenciador::saveFollowers($id, $data['followers']);

        return response()->jsonSuccess($data);
    }

    /**
     *
     * @param $id
     * @param $request
     */
    public function hashtags($id, Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request->all();

        Influenciador::saveHashtags($id, $data);

        return response()->jsonSuccess($data);
    }

    /**
     *
     * @param $id
     * @param $request
     */
    public function keywords($id, Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request->all();

        Influenciador::saveKeywords($id, $data);

        return response()->jsonSuccess($data);
    }

    /**
     *
     * @param $id
     * @param $request
     */
    public function posts($id, Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request['posts'];

        foreach($data as $item)
        {
            //save post
            $model = new Post();
            $model_id = $model->storePost([
                'owner_type'       => Post::OWNER_INFLUENCIADOR,
                'owner_id'         => $id,
                'facebook_id'      => $item['facebook_id'],
                'created_time'     => $item['created_time'],
                'message'          => $item['message'],
                'picture'          => $item['picture'],
                'type'             => $item['type'],
                'ultima_revision'  => $request['last_download_date'],
            ]);

            //save statistics
            $statistic = new Statistic();
            $statistic->storeStatistic([
                'facebook_post_id' => $model_id,
                'shares'           => $item['shares'],
                'comments'         => $item['comments'],
                'reaches_all'      => null,
                'reaches_unique'   => null,
                'ultima_revision'  => $request['last_download_date'],
            ]);

            //Save reactions
            if(count($item['reactions'])) {
                $reaction = new Reaction();
                $reaction->storeReaction([
                    "statistics_id"  => $statistic->id,
                    "wow"           => $item['reactions']['wow'],
                    "haha"          => $item['reactions']['haha'],
                    "like"          => $item['reactions']['like'],
                    "love"          => $item['reactions']['love'],
                    "anger"         => $item['reactions']['anger'],
                    "sorry"         => $item['reactions']['sorry'],
                ]);
            }

            //Save insights
            if(count($item['insights'])) {
                $insights = new Insights();
                $insights->storeInsights([
                    'statistics_id'                           => $statistic->id,
                    'post_impressions'                        => $item['insights']['post_impressions'],
                    'post_impressions_unique'                 => $item['insights']['post_impressions_unique'],
                    'post_engaged_users'                      => $item['insights']['post_engaged_users'],
                    'post_video_views'                        => $item['insights']['post_video_views'],
                    'post_video_view_time'                    => $item['insights']['post_video_view_time'],
                    'post_video_views_10s'                    => $item['insights']['post_video_views_10s'],
                    'post_video_views_paid'                   => $item['insights']['post_video_views_paid'],
                    'post_impressions_unique'                 => $item['insights']['post_impressions_unique'],
                    'post_video_views_unique'                 => $item['insights']['post_video_views_unique'],
                    'post_video_views_organic'                => $item['insights']['post_video_views_organic'],
                    'post_video_views_10s_paid'               => $item['insights']['post_video_views_10s_paid'],
                    'post_video_avg_time_watched'             => $item['insights']['post_video_avg_time_watched'],
                    'post_video_views_10s_unique'             => $item['insights']['post_video_views_10s_unique'],
                    'post_video_views_autoplayed'             => $item['insights']['post_video_views_autoplayed'],
                    'post_video_views_10s_organic'            => $item['insights']['post_video_views_10s_organic'],
                    'post_video_views_paid_unique'            => $item['insights']['post_video_views_paid_unique'],
                    'post_video_complete_views_paid'          => $item['insights']['post_video_complete_views_paid'],
                    'post_video_views_organic_unique'         => $item['insights']['post_video_views_organic_unique'],
                    'post_video_views_clicked_to_play'        => $item['insights']['post_video_views_clicked_to_play'],
                    'post_video_complete_views_organic'       => $item['insights']['post_video_complete_views_organic'],
                    'post_video_complete_views_paid_unique'   => $item['insights']['post_video_complete_views_paid_unique'],
                    'post_video_complete_views_organic_unique'=> $item['insights']['post_video_complete_views_organic_unique'],
                ]);
            }
        }

        return response()->jsonSuccess(['result' => 'OK']);
    }

    /**
     *
     * @param $id
     * @param $request
     */
    public function comments($id, Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request->all();

        return response()->jsonSuccess($data);
    }



}
