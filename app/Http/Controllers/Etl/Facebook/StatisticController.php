<?php

namespace App\Http\Controllers\Etl\Facebook;

use Illuminate\Http\Request;
use App\Models\Etl\Facebook\Statistic;
use App\Models\Etl\Facebook\Insight;


class StatisticController extends BaseFacebookController
{
    public function store(Request $request)
    {
        $data = $request->all();

        $model = new Statistic();
        $model->post_id = $request->post_id;
        $model->shares = $request->shares;
        $model->commets = $request->comments;
        $model->facebook_id = $request->facebook_id;
        $model->reaches_all = $request->reaches_all;
        $model->reaches_unique
        if($model->save())
        {
            $insight = new Insight();
            $insight->statistics_id = $model->id;
            $insight->post_impressions = $request->post_impressions;
            $insight->post_video_views = $request->post_video_views;
            $insight->post_engaged_users = $request->post_engaged_users;
            $insight->post_video_view_time = $request->post_video_view_time;
            $insight->post_video_views_10s = $request->post_video_views_10s;
            $insight->post_video_views_paid = $request->post_video_views_paid;
            $insight->post_impressions_unique = $request->post_impressions_unique;
            $insight->post_video_views_unique = $request->post_video_views_unique;
            $insight->post_video_views_organic = $request->post_video_views_organic;
            $insight->post_video_views_10s_paid = $request->post_video_views_10s_paid;
            $insight->post_video_avg_time_watched = $request->post_video_avg_time_watched;
            $insight->post_video_views_10s_unique = $request->post_video_views_10s_unique;
            $insight->post_video_views_autoplayed = $request->post_video_views_autoplayed;
            $insight->post_video_views_10s_organic = $request->post_video_views_10s_organic;
            $insight->post_video_views_paid_unique = $request->post_video_views_paid_unique;
            $insight->post_video_complete_views_paid = $request->post_video_complete_views_paid;
            $insight->post_video_views_organic_unique = $request->post_video_views_organic_unique;
            $insight->post_video_views_clicked_to_play = $request->post_video_views_clicked_to_play;
            $insight->post_video_complete_views_organic = $request->post_video_complete_views_organic;
            $insight->post_video_complete_views_paid_unique = $request->post_video_complete_views_paid_unique;
            $insight->post_video_complete_views_organic_unique = $request->post_video_complete_views_organic_unique;
            if($insight->save()) {
                return response()->json(['success' => true, 'insight' => $insight]);
            }
        }
    }
}