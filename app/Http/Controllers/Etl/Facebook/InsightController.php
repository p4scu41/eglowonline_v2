<?php

namespace App\Http\Controllers\Etl\Facebook;

use App\Models\Etl\Facebook\Insight;
use Illuminate\Http\Request;

class InsightController extends BaseFacebookController
{
    public function store(Request $request)
    {
        $model = new Insight();
        $model->statistics_id = $request->id;
        $model->post_impressions = $request->post_impressions;
        $model->post_video_views = $request->post_video_views;
        $model->post_engaged_users = $request->post_engaged_users;
        $model->post_video_view_time = $request->post_video_view_time;
        $model->post_video_views_10s = $request->post_video_views_10s;
        $model->post_video_views_paid = $request->post_video_views_paid;
        $model->post_impressions_unique = $request->post_impressions_unique;
        $model->post_video_views_unique = $request->post_video_views_unique;
        $model->post_video_views_organic = $request->post_video_views_organic;
        $model->post_video_views_10s_paid = $request->post_video_views_10s_paid;
        $model->post_video_avg_time_watched = $request->post_video_avg_time_watched;
        $model->post_video_views_10s_unique = $request->post_video_views_10s_unique;
        $model->post_video_views_autoplayed = $request->post_video_views_autoplayed;
        $model->post_video_views_10s_organic = $request->post_video_views_10s_organic;
        $model->post_video_views_paid_unique = $request->post_video_views_paid_unique;
        $model->post_video_complete_views_paid = $request->post_video_complete_views_paid;
        $model->post_video_views_organic_unique = $request->post_video_views_organic_unique;
        $model->post_video_views_clicked_to_play = $request->post_video_views_clicked_to_play;
        $model->post_video_complete_views_organic = $request->post_video_complete_views_organic;
        $model->post_video_complete_views_paid_unique = $request->post_video_complete_views_paid_unique;
        $model->post_video_complete_views_organic_unique = $request->post_video_complete_views_organic_unique;
        if($model->save())
        {
            return response()->json(['success' => true, 'model' => $model]);
        }
    }
}