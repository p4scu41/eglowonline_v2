<?php

namespace App\Http\Controllers\Etl\Facebook;

use App\Helpers\Helper;
use App\Models\Etl\Facebook\Campana;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Campana ETL Facebook
 *
 * @package App\Http\Controllers
 * @subpackage Etl
 * @category Load
 * @author Pascual
 */
class CampanaController extends BaseFacebookController
{
    /**
     *
     * @param $id
     * @param $request
     */
    public function followers($id, Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request->all();

        Campana::saveFollowers($id, $data['followers']);

        return response()->jsonSuccess($data);
    }

    /**
     *
     * @param $id
     * @param $request
     */
    public function hashtags($id, Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request->all();

        Campana::saveHashtags($id, $data);

        return response()->jsonSuccess($data);
    }

    /**
     *
     * @param $id
     * @param $request
     */
    public function keywords($id, Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request->all();

        Campana::saveKeywords($id, $data);

        return response()->jsonSuccess($data);
    }
}
