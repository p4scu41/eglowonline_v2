<?php

namespace App\Http\Controllers\Etl\Facebook;

use App\Models\Etl\Facebook\Post;
use App\Models\Etl\Facebook\Reaction;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PostController extends BaseFacebookController
{
    /**
     * @param $id
     * @param $request
     */
    public function store($id, Req $res)
    {
      $this->initLogger($this, __FUNCTION__);
      $this->logInputs($request);

      $data = $request->posts;
      foreach($data as $item)
      {
        $model = null;
        try{
          $model = Post::where('facebook_id', $item->facebook_id)->firstOrFail();
        } catch (ModelNotFoundException $error) {
          $model = new Post();
        }
        $model->owner_type = Post::OWNER_INFLUENCIADOR;
        $model->owner_id = $id;
        $model->facebook_id = $item->facebook_id;
        $model->created_time = $item->created_time;
        $model->message = $item->message;
        $model->picture = $item->picture;
        $model->type = $item->type;
        $model->ultima_revision = $request->last_download_date;
        $model->save();
        if(count($item->reactions))
        {
          $this->saveReaction($model->id, $item->posts->reactions);
        }
        if(count($item->insights))
        {
          $this->saveInsights($model->id, $item->posts->insights);
        }
      }
    }

    public function saveReaction($primarykey, $data)
    {
      Reaction::create([
        'statistics_id' => $primarykey,
        'wow' => $data->wow,
        'haha' => $data->haha,
        'like' => $data->like,
        'love' => $data->love,
        'anger' => $data->anger,
        'sorry' => $data->sorry
      ]);
    }

    public function saveInsights($primarykey, $data)
    {
      Insights::create([
        'statistics_id' => $primaryKey,
        'post_impressions' => $data->post_impressions,
        'post_impressions_unique' => $data->post_impressions_unique,
        'post_engaged_users' => $data->post_engaged_users,
        'post_video_views' => $data->post_video_views,
        'post_video_view_time' => $data->post_video_view_time,
        'post_video_views_10s' => $data->post_video_views_10s,
        'post_video_views_paid' => $data->post_video_views_paid,
        'post_impressions_unique' => $data->post_impressions_unique,
        'post_video_views_unique' => $data->post_video_views_unique,
        'post_video_views_organic' => $data->post_video_views_organic,
        'post_video_views_10s_paid' => $data->post_video_views_10s_paid,
        'post_video_avg_time_watched' => $data->post_video_avg_time_watched,
        'post_video_views_10s_unique' => $data->post_video_views_10s_unique,
        'post_video_views_autoplayed' => $data->post_video_views_autoplayed,
        'post_video_views_10s_organic' => $data->post_video_views_10s_organic,
        'post_video_views_paid_unique' => $data->post_video_views_paid_unique,
        'post_video_complete_views_paid' => $data->post_video_complete_views_paid,
        'post_video_views_organic_unique' => $data->post_video_views_organic_unique,
        'post_video_views_clicked_to_play' => $data->post_video_views_clicked_to_play,
        'post_video_complete_views_organic' => $data->post_video_complete_views_organic,
        'post_video_complete_views_paid_unique' => $data->post_video_complete_views_paid_unique,
        'post_video_complete_views_organic_unique' => $data->post_video_complete_views_organic_unique
      ]);
    }
}