<?php

namespace App\Http\Controllers\Etl\Facebook;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Models\Etl\Facebook\Comments;
use App\Models\Etl\Facebook\Insights;
use App\Models\Etl\Facebook\Post;
use App\Models\Etl\Facebook\Reaction;
use App\Models\Etl\Facebook\Statistic;
use Illuminate\Support\Facades\Log;

/**
 * Publicacion ETL Facebook
 *
 * @package App\Http\Controllers
 * @subpackage Etl
 * @category Load
 * @author Pascual
 */
class PublicacionController extends BaseFacebookController
{
    /**
     *
     * @param $id
     * @param $request
     */
    public function update($id, Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request->all();

        return response()->jsonSuccess($data);
    }

    /**
     *
     * @param $id
     * @param $request
     */
    public function statistics($id, Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request->all();

        //Save post
        $post = new Post();
        $post_id = $post->storePost([
            "owner_type"        => Post::OWNER_CAMPANA,
            "owner_id"          => $id,
            "facebook_id"       => $data['statistics']['facebook_id'],
            "created_time"      => $data['statistics']['created_time'],
            "ultima_revision"   => $data['last_download_date']
        ]);

        //save statistics
        $statistic = new Statistic();
        $statistic->storeStatistic([
            "facebook_post_id" => $post_id,
            "shares"           => $data['statistics']['shares'],
            "comments"         => $data['statistics']['comments'],
            "reaches_all"      => null,
            "reaches_unique"   => null,
            "ultima_revision"  => $data['last_download_date']
        ]);

        //save insights
        $insights = new Insights();
        $insights->storeInsights([
            'statistics_id'                            => $statistic->id,
            'post_impressions'                         => isset($data['statistics']['insights']['post_impressions'])                         ? $data['statistics']['insights']['post_impressions']                         : null,
            'post_video_views'                         => isset($data['statistics']['insights']['post_video_views'])                         ? $data['statistics']['insights']['post_video_views']                         : null,
            'post_engaged_users'                       => isset($data['statistics']['insights']['post_engaged_users'])                       ? $data['statistics']['insights']['post_engaged_users']                       : null,
            'post_video_view_time'                     => isset($data['statistics']['insights']['post_video_view_time'])                     ? $data['statistics']['insights']['post_video_view_time']                     : null,
            'post_video_views_10s'                     => isset($data['statistics']['insights']['post_video_views_10s'])                     ? $data['statistics']['insights']['post_video_views_10s']                     : null,
            'post_video_views_paid'                    => isset($data['statistics']['insights']['post_video_views_paid'])                    ? $data['statistics']['insights']['post_video_views_paid']                    : null,
            'post_impressions_unique'                  => isset($data['statistics']['insights']['post_impressions_unique'])                  ? $data['statistics']['insights']['post_impressions_unique']                  : null,
            'post_video_views_unique'                  => isset($data['statistics']['insights']['post_video_views_unique'])                  ? $data['statistics']['insights']['post_video_views_unique']                  : null,
            'post_video_views_organic'                 => isset($data['statistics']['insights']['post_video_views_organic'])                 ? $data['statistics']['insights']['post_video_views_organic']                 : null,
            'post_video_views_10s_paid'                => isset($data['statistics']['insights']['post_video_views_10s_paid'])                ? $data['statistics']['insights']['post_video_views_10s_paid']                : null,
            'post_video_avg_time_watched'              => isset($data['statistics']['insights']['post_video_avg_time_watched'])              ? $data['statistics']['insights']['post_video_avg_time_watched']              : null,
            'post_video_views_10s_unique'              => isset($data['statistics']['insights']['post_video_views_10s_unique'])              ? $data['statistics']['insights']['post_video_views_10s_unique']              : null,
            'post_video_views_autoplayed'              => isset($data['statistics']['insights']['post_video_views_autoplayed'])              ? $data['statistics']['insights']['post_video_views_autoplayed']              : null,
            'post_video_views_10s_organic'             => isset($data['statistics']['insights']['post_video_views_10s_organic'])             ? $data['statistics']['insights']['post_video_views_10s_organic']             : null,
            'post_video_views_paid_unique'             => isset($data['statistics']['insights']['post_video_views_paid_unique'])             ? $data['statistics']['insights']['post_video_views_paid_unique']             : null,
            'post_video_complete_views_paid'           => isset($data['statistics']['insights']['post_video_complete_views_paid'])           ? $data['statistics']['insights']['post_video_complete_views_paid']           : null,
            'post_video_views_organic_unique'          => isset($data['statistics']['insights']['post_video_views_organic_unique'])          ? $data['statistics']['insights']['post_video_views_organic_unique']          : null,
            'post_video_views_clicked_to_play'         => isset($data['statistics']['insights']['post_video_views_clicked_to_play'])         ? $data['statistics']['insights']['post_video_views_clicked_to_play']         : null,
            'post_video_complete_views_organic'        => isset($data['statistics']['insights']['post_video_complete_views_organic'])        ? $data['statistics']['insights']['post_video_complete_views_organic']        : null,
            'post_video_complete_views_paid_unique'    => isset($data['statistics']['insights']['post_video_complete_views_paid_unique'])    ? $data['statistics']['insights']['post_video_complete_views_paid_unique']    : null,
            'post_video_complete_views_organic_unique' => isset($data['statistics']['insights']['post_video_complete_views_organic_unique']) ? $data['statistics']['insights']['post_video_complete_views_organic_unique'] : null,
        ]);

        //save reactions
        $reaction = new Reaction();
        $reaction->storeReaction([
            "statistics_id"  => $statistic->id,
            "wow"           => $data['statistics']['reactions']['wow'],
            "haha"          => $data['statistics']['reactions']['haha'],
            "like"          => $data['statistics']['reactions']['like'],
            "love"          => $data['statistics']['reactions']['love'],
            "anger"         => $data['statistics']['reactions']['anger'],
            "sorry"         => $data['statistics']['reactions']['sorry'],
        ]);

        return response()->jsonSuccess(['result' => 'OK']);
    }

    /**
     *
     * @param $id
     * @param $request
     */
    public function comments($id, Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request->all();

        if(count($data['comments']))
        {
            foreach($data['comments'] as $comment)
            {
                $post_id = null;

                //get post
                $post = Post::where('facebook_id', $comment['facebook_id'])->first();
                if(empty($post))
                {
                    $post = new Post();
                    $post_id = $post->storePost([
                        "owner_type"        => Post::OWNER_CAMPANA,
                        "owner_id"          => $id,
                        "facebook_id"       => $comment['facebook_id'],
                        "created_time"      => $comment['created_time'],
                        "ultima_revision"   => $data['last_date'],
                    ]);
                }
                else
                {
                    $post_id = $post->id;
                }

                //save comments
                $comments = new Comments();
                $comments->storeComments([
                    'facebook_post_id' => $post_id,
                    'follower_id'      => $comment['follower_id'],
                    'message'          => $comment['message'],
                    'created_time'     => $comment['created_time'],
                ]);

            }
        }

        return response()->jsonSuccess($data);
    }

    /**
     *
     * @param $id
     * @param $request
     */
    public function reactions($id, Request $request)
    {
        $this->initLogger($this, __FUNCTION__);
        $this->logInputs($request);

        $data = $request->all();

        return response()->jsonSuccess($data);
    }
}
