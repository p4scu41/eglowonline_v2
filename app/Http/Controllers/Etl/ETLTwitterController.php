<?php

namespace App\Http\Controllers\Etl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Models\User;
use App\Models\PerfilTwitter;
use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Support\Facades\DB;

use App\Jobs\TestRequest;
use App\Jobs\RateRequest;

class ETLTwitterController extends Controller
{
    
    public function postTweet(Request $request)
    {
        try {
            
            //$perfilTwitter = PerfilTwitter::find(200);
            //dispatch(new TestRequest($perfilTwitter,array('mensaje'=>'Probando la api por colas','imagen'=>'img/user_icon.png')));
            //exit;
            
            $perfiles = DB::connection('etl_twitter_mysql')->table('twitter_apps')->get();
            
            $count = 0;
            
            foreach($perfiles as $perfil){
                
                dispatch((new RateRequest($perfil)));
                $count +=1;
            }
            
        } catch (Exception $e) {
            Log::error('Twitter:postTweet -> '.$e->getMessage());
                echo 'Twitter:postTweet -> '.$e->getMessage();
        }
    }
    
    public function getRateLimits(Request $request)
    {
        try {
            
            $perfiles = DB::connection('etl_twitter_mysql')->table('twitter_apps')->get();
            
            foreach($perfiles as $perfil){
                
                dispatch(new RateLimitRequest($perfil));
                
            }
            
        } catch (Exception $e) {
            Log::error('Twitter:getRateLimits -> '.$e->getMessage());
                echo 'Twitter:getRateLimits -> '.$e->getMessage();
        }
    }
}