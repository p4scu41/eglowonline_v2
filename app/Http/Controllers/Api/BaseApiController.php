<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Support\ExceptionSupport;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

/**
 * Clase Base para los controladores que son consumidos por Servicios Rest
 *
 * @package App\Http\Controllers
 * @author  Pascual <pascual@importare.mx>
 *
 * @method public \Illuminate\Http\Response index(\Illuminate\Http\Request $request)
 * @method public \Illuminate\Http\Response store(\Illuminate\Http\Request $request)
 * @method public \Illuminate\Http\Response show(\Illuminate\Http\Request $request, int $id)
 * @method public \Illuminate\Http\Response update(\Illuminate\Http\Request $request, int $id)
 * @method public \Illuminate\Http\Response destroy(\Illuminate\Http\Request $request, int $id)
 */
class BaseApiController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return \Illuminate\Http\Response
     *         200 OK, if the process was successful
     *         500 Internal Server Error, if something was wrong
     *         Any other valid http status code throw by the exception
     *
     * @Get("/")
     */
    public function index(Request $request)
    {
        $exception = null;
        $paginator = [];
        $modelCls  = $this->modelNamespace . $this->modelClass;

        try {
            $beforeAction = $this->onBeforeAction(__FUNCTION__, $request);

            // If the result of onBeforeAction is a instance of RedirectResponse, Response or View
            // it will be returned
            if ($this->isValidResponse($beforeAction)) {
                return $beforeAction;
            }

            $result = call_user_func_array(
                [$modelCls, 'filter'],
                [
                    // array_filter and strlen removes all NULL, FALSE and Empty Strings
                    // but leaves 0 (zero) values
                    $this->filter ?: array_filter($request->input('filter', []), 'strlen'),
                    true, // $request->has('page'),
                    $request->input('order_by', $this->order_by)
                ]
            );

            // When paginator exists
            // if ($request->has('page')) {
                // The paginator contents metadata of pagination
                $paginator = $result->toArray();
                $result = $paginator['data'];

                unset($paginator['data']);
            // }
        } catch (Exception $e) {
            ExceptionSupport::getMessageIfDebug($e, $this->error_logger);
            $exception = $e;
        }

        $afterAction = $this->onAfterAction(__FUNCTION__, $request);

        // If the result of onAfterAction is a instance of RedirectResponse, Response or View
        // it will be returned
        if ($this->isValidResponse($afterAction)) {
            return $afterAction;
        }

        if (!empty($exception)) {
            return response()->jsonException($exception);
        }

        return response()->jsonSuccess([
            'data' => $result,
            'extra' => $paginator
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return \Illuminate\Http\Response
     *         201 Created, if the process was successful
     *         422 Unprocessable Entity, if the validation fail
     *         500 Internal Server Error, if something was wrong
     *         Any other valid http status code throw by the exception
     *
     * @Post("/")
     */
    public function store(Request $request)
    {
        $data      = [];
        $exception = null;
        $modelCls  = $this->modelNamespace . $this->modelClass;

        try {
            DB::beginTransaction();

            $this->modelObj = new $modelCls;

            $beforeAction = $this->onBeforeAction(__FUNCTION__, $request);

            // If the result of onBeforeAction is a instance of RedirectResponse, Response or View
            // it will be returned
            if ($this->isValidResponse($beforeAction)) {
                return $beforeAction;
            }

            $this->modelObj->fill($request->input());

            if ($this->modelObj->isValid($modelCls::DO_CREATE)) {
                $this->modelObj->save();
                DB::commit();
            } else {
                $exception = ['extra' => $this->modelObj->errors ];
            }

            // If the parameter return is presented
            // the object created will be return
            if ($request->input('return')) {
                $data = [
                    'data' => $this->modelObj->toArray(),
                ];
            }
        } catch (Exception $e) {
            DB::rollBack();
            ExceptionSupport::getMessageIfDebug($e, $this->error_logger);
            $exception = $e;
        }

        $afterAction = $this->onAfterAction(__FUNCTION__, $request);

        // If the result of onAfterAction is a instance of RedirectResponse, Response or View
        // it will be returned
        if ($this->isValidResponse($afterAction)) {
            return $afterAction;
        }

        if (!empty($exception)) {
            if (is_array($exception)) {
                return response()->jsonInvalidData($exception);
            }

            return response()->jsonException($exception);
        }

        return response()->jsonSuccess($data, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     * @param int                      $id      Primary key value of the model
     *
     * @return \Illuminate\Http\Response
     *         200 Ok, if the process was successful
     *         404 Not Found, if the resource doesn't exist
     *         500 Internal Server Error, if something was wrong
     *         Any other valid http status code throw by the exception
     *
     * @Get("/{id}")
     */
    public function show(Request $request, $id)
    {
        $error = null;
        try {
            if (empty($this->modelObj)) {
                return response()->jsonNotFound();
            }

            $beforeAction = $this->onBeforeAction(__FUNCTION__, $request);

            // If the result of onBeforeAction is a instance of RedirectResponse, Response or View
            // it will be returned
            if ($this->isValidResponse($beforeAction)) {
                return $beforeAction;
            }
        } catch (Exception $e) {
            ExceptionSupport::getMessageIfDebug($e, $this->error_logger);
            $error = $e;
        }

        $afterAction = $this->onAfterAction(__FUNCTION__, $request);

        // If the result of onAfterAction is a instance of RedirectResponse, Response or View
        // it will be returned
        if ($this->isValidResponse($afterAction)) {
            return $afterAction;
        }

        if (!empty($error)) {
            return response()->jsonException($error);
        }

        return response()->jsonSuccess([
            'data' => $this->modelObj->toArray(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     * @param int                      $id      Primary key value of the model to update
     *
     * @return \Illuminate\Http\Response
     *         200 Ok, if the process was successful
     *         422 Unprocessable Entity, if the validation fail
     *         500 Internal Server Error, if something was wrong
     *         Any other valid http status code throw by the exception
     *
     * @Put("/{id}")
     */
    public function update(Request $request, $id)
    {
        $data      = [];
        $exception = null;
        $modelCls  = $this->modelNamespace . $this->modelClass;

        try {
            if (empty($this->modelObj)) {
                return response()->jsonNotFound();
            }

            $beforeAction = $this->onBeforeAction(__FUNCTION__, $request);

            // If the result of onBeforeAction is a instance of RedirectResponse, Response or View
            // it will be returned
            if ($this->isValidResponse($beforeAction)) {
                return $beforeAction;
            }

            DB::beginTransaction();
            $this->modelObj->fill($request->input());

            if ($this->modelObj->isValid($modelCls::DO_UPDATE)) {
                $this->modelObj->save();
                DB::commit();
            } else {
                $exception = ['extra' => $this->modelObj->errors];
            }

            // If the parameter return is presented
            // the object updated will be return
            if ($request->input('return')) {
                $data = [
                    'data' => $this->modelObj->toArray(),
                ];
            }
        } catch (Exception $e) {
            DB::rollBack();
            ExceptionSupport::getMessageIfDebug($e, $this->error_logger);
            $exception = $e;
        }

        $afterAction = $this->onAfterAction(__FUNCTION__, $request);

        // If the result of onAfterAction is a instance of RedirectResponse, Response or View
        // it will be returned
        if ($this->isValidResponse($afterAction)) {
            return $afterAction;
        }

        if (!empty($exception)) {
            if (is_array($exception)) {
                return response()->jsonInvalidData($exception);
            }

            return response()->jsonException($exception);
        }

        return response()->jsonSuccess($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     * @param int                      $id      Primary key value of the model to delete
     *
     * @return \Illuminate\Http\Response
     *         200 Ok, if the process was successful
     *         404 Not Found, if the resource doesn't exist
     *         500 Internal Server Error, if something was wrong
     *         Any other valid http status code throw by the exception
     *
     * @Delete("/{id}")
     */
    public function destroy(Request $request, $id)
    {
        $data  = [];
        $error = null;

        try {
            if (empty($this->modelObj)) {
                return response()->jsonNotFound();
            }

            $beforeAction = $this->onBeforeAction(__FUNCTION__, $request);

            // If the result of onBeforeAction is a instance of RedirectResponse, Response or View
            // it will be returned
            if ($this->isValidResponse($beforeAction)) {
                return $beforeAction;
            }

            // If the parameter return is presented
            // the object deleted will be return
            if ($request->input('return')) {
                $data = [
                    'data' => $this->modelObj->toArray(),
                ];
            }

            DB::beginTransaction();

            $this->modelObj->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            ExceptionSupport::getMessageIfDebug($e, $this->error_logger);
            $error = $e;
        }

        $afterAction = $this->onAfterAction(__FUNCTION__, $request);

        // If the result of onAfterAction is a instance of RedirectResponse, Response or View
        // it will be returned
        if ($this->isValidResponse($afterAction)) {
            return $afterAction;
        }

        if (!empty($error)) {
            return response()->jsonException($error);
        }

        return response()->jsonSuccess($data);
    }
}
