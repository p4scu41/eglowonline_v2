<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\BaseApiController;
use Illuminate\Http\Request;

/**
 * Implements end points related with users resource
 *
 * @package App\Http\Controllers\Api
 * @author  Pascual <pascual@importare.mx>
 * @Resource("users")
 */
class UserController extends BaseApiController
{
    protected $modelNamespace = 'App\Models\Resources\\';
    public $modelClass = 'User';
    public $resource   = 'users';

    /**
     * @inheritDoc
     */
    public function onBeforeIndex(\Illuminate\Http\Request $request)
    {
        parent::onBeforeIndex($request);

        $modelCls = $this->modelNamespace . $this->modelClass;

        // Eager Loading Relationship
        $modelCls::$withRelations = 'tipoUsuario';

        return true;
    }

    /**
     * @inheritDoc
     */
    public function onBeforeShow(\Illuminate\Http\Request $request)
    {
        parent::onBeforeShow($request);

        // Lazy Eager Loading Relationship
        $this->modelObj->load('tipoUsuario');

        return true;
    }

    /**
     * @apiDefine ModelData
     *
     * @apiSuccess {Object}   data                 User Data
     * @apiSuccess {Number}   data.id
     * @apiSuccess {Number}   data.tipo_usuario_id
     * @apiSuccess {String}   data.nombre
     * @apiSuccess {String}   data.email
     * @apiSuccess {DateTime} data.ultimo_acceso
     * @apiSuccess {Number}   data.activo
     * @apiSuccess {DateTime} data.created_at
     *
     * @apiSuccessExample Response 200 (example):
     *     HTTP/1.1 200 OK
     *     {
     *         "extra": null,
     *         "data": {
     *             "id": 631,
     *             "tipo_usuario_id": 1,
     *             "nombre": "Nuevo Usuario",
     *             "email": "usuario@mail.com",
     *             "ultimo_acceso": null,
     *             "activo": 1,
     *             "created_at": "2017-03-06 21:58:46"
     *         },
     *         "message": "Solicitud procesada exitosamente",
     *         "status": 200,
     *         "error": null
     *     }
     */

    /**
     * @api {get} /users Users List
     * @apiVersion 1.0.0
     * @apiName GetUsers
     * @apiGroup Users
     * @apiPermission Administrador
     *
     * @apiUse JwtHeader
     *
     * @apiParam {Number}              [page=1]          Page
     * @apiParam {String}              [sort=created_at] Field name by which the results will be sorted
     * @apiParam {String="asc","desc"} [order=asc]       Direction sort
     * @apiParam {String[]}            [filter]          Fields to be filtered
     *
     * @apiParamExample {String} Request (example):
     *     /users?page=2&sort=nombre&order=asc&filter[activo]=1
     *
     * @apiSuccess {Object} extra                Pagintation data
     * @apiSuccess {Number} extra.current_page
     * @apiSuccess {Url}    extra.first_page_url
     * @apiSuccess {Number} extra.from
     * @apiSuccess {Number} extra.last_page
     * @apiSuccess {Url}    extra.last_page_url
     * @apiSuccess {Url}    extra.next_page_url
     * @apiSuccess {Url}    extra.path
     * @apiSuccess {Number} extra.per_page
     * @apiSuccess {Url}    extra.prev_page_url
     * @apiSuccess {Number} extra.to
     * @apiSuccess {Number} extra.total
     *
     * @apiSuccess {Object[]} data                          Users List
     * @apiSuccess {Number}   data.id
     * @apiSuccess {Number}   data.tipo_usuario_id
     * @apiSuccess {String}   data.nombre
     * @apiSuccess {String}   data.email
     * @apiSuccess {DateTime} data.ultimo_acceso
     * @apiSuccess {Number}   data.activo
     * @apiSuccess {DateTime} data.created_at
     * @apiSuccess {Object}   data.tipo_usuario
     * @apiSuccess {Number}   data.tipo_usuario.id
     * @apiSuccess {String}   data.tipo_usuario.descripcion
     *
     * @apiSuccessExample Response 200 (example):
     *     HTTP/1.1 200 OK
     *     {
     *         "extra": {
     *             "current_page": 2,
     *             "first_page_url": "https://eglowonline.com/api/v1/users?page=1",
     *             "from": 16,
     *             "last_page": 14,
     *             "last_page_url": "https://eglowonline.com/api/v1/users?page=14",
     *             "next_page_url": "https://eglowonline.com/api/v1/users?page=3",
     *             "path": "https://eglowonline.com/api/v1/users",
     *             "per_page": 15,
     *             "prev_page_url": "https://eglowonline.com/api/v1/users?page=1",
     *             "to": 30,
     *             "total": 203
     *         },
     *         "data": [
     *             {
     *                 "id": 380,
     *                 "tipo_usuario_id": 5,
     *                 "nombre": "Ariel Levy",
     *                 "email": "Ariel_levy@mail.com",
     *                 "ultimo_acceso": null,
     *                 "activo": 1,
     *                 "created_at": "2017-03-06 21:58:46",
     *                 "tipo_usuario": {
     *                     "id": 5,
     *                     "descripcion": "Influenciador"
     *                 }
     *             },
     *             {
     *                 "id": 553,
     *                 "tipo_usuario_id": 5,
     *                 "nombre": "armando casas",
     *                 "email": "eglowtest@gmail.com",
     *                 "ultimo_acceso": "2017-07-13 10:04:27",
     *                 "activo": 1,
     *                 "created_at": "2017-03-06 21:58:46",
     *                 "tipo_usuario": {
     *                     "id": 5,
     *                     "descripcion": "Influenciador"
     *                 }
     *             },
     *             {
     *                 "id": 502,
     *                 "tipo_usuario_id": 5,
     *                 "nombre": "Arturo Supervicios",
     *                 "email": "supervicios@mail.com",
     *                 "ultimo_acceso": null,
     *                 "activo": 1,
     *                 "created_at": "2017-03-06 21:58:46",
     *                 "tipo_usuario": {
     *                     "id": 5,
     *                     "descripcion": "Influenciador"
     *                 }
     *             }
     *         ],
     *         "message": "Solicitud procesada exitosamente",
     *         "status": 200,
     *         "error": null
     *     }
     *
     * @apiUse TokenNotFoundError
     * @apiUse TokenExpiredError
     * @apiUse TokenBlacklistedError
     * @apiUse NoAccessRightError
     */
    public function index(Request $request)
    {
        return parent::index($request);
    }

    /**
     * @api {post} /users?return=true Users Store
     * @apiVersion 1.0.0
     * @apiName PostUser
     * @apiGroup Users
     * @apiPermission Administrador
     *
     * @apiUse JwtHeader
     *
     * @apiParam {Number=1,2,3,4,5} tipo_usuario_id 1 = Administrador, 2 = Agencia, 3 = Marca, 4 = Producto, 5 = Influenciador
     * @apiParam {String}           nombre
     * @apiParam {String}           email
     * @apiParam {String}           password
     * @apiParam {Number=0,1}       activo
     *
     * @apiParamExample {json} Request (example):
     *     {
     *         "tipo_usuario_id": 1,
     *         "nombre": "Nuevo Usuario",
     *         "email": "usuario@mail.com",
     *         "password": "asdf1234",
     *         "activo": 1,
     *     }
     *
     * @apiSuccess (201 Created) {Object}   data                 User Data
     * @apiSuccess (201 Created) {Number}   data.id
     * @apiSuccess (201 Created) {Number}   data.tipo_usuario_id
     * @apiSuccess (201 Created) {String}   data.nombre
     * @apiSuccess (201 Created) {String}   data.email
     * @apiSuccess (201 Created) {Number}   data.activo
     * @apiSuccess (201 Created) {DateTime} data.created_at
     *
     * @apiSuccessExample Response 201 (example):
     *     HTTP/1.1 201 Created
     *     {
     *         "extra": null,
     *         "data": {
     *             "id": 631,
     *             "tipo_usuario_id": 1,
     *             "nombre": "Nuevo Usuario",
     *             "email": "usuario@mail.com",
     *             "activo": 1,
     *             "created_at": "2017-03-06 21:58:46"
     *         },
     *         "message": "Solicitud procesada exitosamente",
     *         "status": 201,
     *         "error": null
     *     }
     *
     * @apiUse TokenNotFoundError
     * @apiUse TokenExpiredError
     * @apiUse TokenBlacklistedError
     * @apiUse ValidationError
     * @apiUse NoAccessRightError
     */
    public function store(Request $request)
    {
        return parent::store($request);
    }

    /**
     * @api {get} /users/:id Users Show
     * @apiVersion 1.0.0
     * @apiName GetUser
     * @apiGroup Users
     * @apiPermission Administrador
     *
     * @apiUse JwtHeader
     *
     * @apiParam {Number} id Users unique ID
     *
     * @apiSuccess {Object}   data                         User Data
     * @apiSuccess {Number}   data.id
     * @apiSuccess {Number}   data.tipo_usuario_id
     * @apiSuccess {String}   data.nombre
     * @apiSuccess {String}   data.email
     * @apiSuccess {DateTime} data.ultimo_acceso
     * @apiSuccess {Number}   data.activo
     * @apiSuccess {DateTime} data.created_at
     * @apiSuccess {Object}   data.tipo_usuario
     * @apiSuccess {Number}   data.tipo_usuario.id
     * @apiSuccess {String}   data.tipo_usuario.descripcion
     *
     * @apiSuccessExample Response 200 (example):
     *     HTTP/1.1 200 OK
     *     {
     *         "extra": null,
     *         "data": {
     *             "id": 631,
     *             "tipo_usuario_id": 1,
     *             "nombre": "Nuevo Usuario",
     *             "email": "usuario@mail.com",
     *             "ultimo_acceso": "2017-07-13 10:04:27",
     *             "activo": 1,
     *             "created_at": "2017-03-06 21:58:46",
     *             "tipo_usuario": {
     *                 "id": 5,
     *                 "descripcion": "Administrador"
     *             }
     *         },
     *         "message": "Solicitud procesada exitosamente",
     *         "status": 200,
     *         "error": null
     *     }
     *
     * @apiUse TokenNotFoundError
     * @apiUse TokenExpiredError
     * @apiUse TokenBlacklistedError
     * @apiUse NotFoundError
     * @apiUse NoAccessRightError
     */
    public function show(Request $request, $id)
    {
        return parent::show($request, $id);
    }

    /**
     * @api {post} /users/:id?return=true Users Update
     * @apiVersion 1.0.0
     * @apiName PutUser
     * @apiGroup Users
     * @apiPermission Administrador
     *
     * @apiUse JwtHeader
     *
     * @apiParam {Number}           id                id Users unique ID
     * @apiParam {Number=1,2,3,4,5} [tipo_usuario_id] 1 = Administrador, 2 = Agencia, 3 = Marca, 4 = Producto, 5 = Influenciador
     * @apiParam {String}           [nombre]
     * @apiParam {String}           [email]
     * @apiParam {String}           [password]
     * @apiParam {Number=0,1}       [activo]
     * @apiParam {String}           _method           PUT
     *
     * @apiParamExample {json} Request (example):
     *     {
     *         "tipo_usuario_id": 1,
     *         "nombre": "Nuevo Usuario",
     *         "email": "usuario@mail.com",
     *         "password": "asdf1234",
     *         "activo": 1,
     *         "_method": "PUT"
     *     }
     *
     * @apiUse ModelData
     *
     * @apiUse TokenNotFoundError
     * @apiUse TokenExpiredError
     * @apiUse TokenBlacklistedError
     * @apiUse ValidationError
     * @apiUse NotFoundError
     * @apiUse NoAccessRightError
     */
    public function update(Request $request, $id)
    {
        return parent::update($request, $id);
    }

    /**
     * @api {post} /users/:id?return=true Users Delete
     * @apiVersion 1.0.0
     * @apiName DeleteUser
     * @apiGroup Users
     * @apiPermission Administrador
     *
     * @apiUse JwtHeader
     *
     * @apiParam {Number} id      Users unique ID
     * @apiParam {String} _method DELETE
     *
     * @apiParamExample {json} Request (example):
     *     {
     *         "_method": "DELETE"
     *     }
     *
     * @apiUse ModelData
     *
     * @apiUse TokenNotFoundError
     * @apiUse TokenExpiredError
     * @apiUse TokenBlacklistedError
     * @apiUse NotFoundError
     * @apiUse NoAccessRightError
     */
    public function destroy(Request $request, $id)
    {
        return parent::destroy($request, $id);
    }
}
