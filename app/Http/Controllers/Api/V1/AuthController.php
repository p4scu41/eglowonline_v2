<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\LogEvento;
use Exception;
use Illuminate\Routing\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

/**
 * Implements end points related with the JSON Web Token Authentication
 *
 * @package App\Http\Controllers\Api
 * @author  Pascual <pascual@importare.mx>
 * @Resource("auth")
 */
class AuthController extends Controller
{
    /**
     * @apiDefine JwtSuccess
     *
     * @apiSuccess {Object[]} data
     * @apiSuccess {String}   data.token
     * @apiSuccess {Number}   data.user_id
     *
     * @apiSuccessExample Response 200 Token (example):
     *     HTTP/1.1 200 OK
     *         {
     *             "extra": null,
     *             "data": {
     *                 "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0L2VnbG93b25saW5lX3YyL3B1YmxpYy9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwNTkyMzQyNywiZXhwIjoxNTA2MTM5NDI3LCJuYmYiOjE1MDU5MjM0MjcsImp0aSI6InE0MFJsRmN4Mk9qOFp1UHAifQ.mT7OXNhRJotDG3mpJGXIKxhyvyn6rTfYzlXuj_hYQ4w",
     *                 "user_id": 631
     *             },
     *             "message": "Solicitud procesada exitosamente",
     *             "status": 200,
     *             "error": null
     *         }
     */

    /**
     * @api {post} /auth/login Users Login
     * @apiVersion 1.0.0
     * @apiName UserLogin
     * @apiGroup AuthJWT
     *
     * @apiParam {String} email
     * @apiParam {String} password
     *
     * @apiParamExample {json} Request (example):
     *     {
     *         "email": "usuario@mail.com",
     *         "password": "asdf1234",
     *     }
     *
     * @apiUse JwtSuccess
     *
     * @apiError InvalidCredentials User or Password incorrect
     *
     * @apiErrorExample {json} Response 409 InvalidCredentials (example):
     *     HTTP/1.1 409 Conflict
     *         {
     *             "extra": null,
     *             "data": [],
     *             "message": "Usuario o Contraseña incorrecta",
     *             "status": 409,
     *             "error": 409
     *         }
     */
    public function login(\Illuminate\Http\Request $request)
    {
        // Grab credentials from the request
        $credentials = $request->only('email', 'password');
        // Login only available for active users
        $credentials['activo'] = 1;
        $data = [
            'token'   => null,
            'user_id' => null,
        ];

        try {
            // Attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                // Utilizamos la clase personalizada para devolver la respuesta
                return response()->jsonFormat([
                    'error'   => 409,
                    'message' => 'Usuario o Contraseña incorrecta',
                ], 409);
            }

            // Get the authenticated user
            $user = JWTAuth::authenticate($token);

            // Store the datetime on last_login
            $user->ultimo_acceso = date('Y-m-d H:i:s');
            $user->save();

            LogEvento::saveInicioSesion();

            $data['token']   = $token;
            $data['user_id'] = $user->id;
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            return response()->jsonFormat([
                'error'   => 500,
                'message' => 'Error al iniciar sesión. Intentelo nuevamente.',
                'extra'   => $e->getMessage(),
            ], 500);
        } catch (Exception $e) {
            return response()->jsonException($e);
        }

        // All good so return the token
        return response()->jsonSuccess([
            'data' => $data,
        ]);
    }

    /**
     * @api {post} /auth/refresh Refresh Token
     * @apiVersion 1.0.0
     * @apiName RefreshToken
     * @apiGroup AuthJWT
     *
     * @apiUse JwtHeader
     *
     * @apiUse JwtSuccess
     *
     * @apiUse TokenNotFoundError
     * @apiUse TokenExpiredError
     * @apiUse TokenBlacklistedError
     */
    public function refresh(\Illuminate\Http\Request $request)
    {
        $data = [
            'token'   => null,
            'user_id' => null,
        ];

        try {
            $newToken = JWTAuth::setRequest($request)->parseToken()->refresh();

            // Get the authenticated user
            $user = JWTAuth::authenticate($newToken);

            // Store the datetime on last_login
            $user->ultimo_acceso = date('Y-m-d H:i:s');
            $user->save();

            $data['token']   = $newToken;
            $data['user_id'] = $user->id;
        } catch (TokenExpiredException $e) {
            return response()->jsonJwtException($e, ['message' => 'Token expirado']);
        } catch (TokenInvalidException $e) {
            return response()->jsonJwtException($e, ['message' => 'Token inválido']);
        } catch (JWTException $e) {
            return response()->jsonJwtException($e, ['message' => 'Error al procesar el Token']);
        } catch (Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'data' => $data,
        ]);
    }

    /**
     * @api {get} /auth/user Get User logged
     * @apiVersion 1.0.0
     * @apiName GetUserJwt
     * @apiGroup AuthJWT
     *
     * @apiUse JwtHeader
     *
     * @apiSuccess {Object}   data                 User Data
     * @apiSuccess {Number}   data.id
     * @apiSuccess {Number}   data.tipo_usuario_id
     * @apiSuccess {String}   data.nombre
     * @apiSuccess {String}   data.email
     * @apiSuccess {DateTime} data.ultimo_acceso
     * @apiSuccess {Number}   data.activo
     * @apiSuccess {DateTime} data.created_at
     *
     * @apiSuccessExample Response 200 (example):
     *     HTTP/1.1 200 OK
     *     {
     *         "extra": null,
     *         "data": {
     *             "id": 631,
     *             "tipo_usuario_id": 1,
     *             "nombre": "Nuevo Usuario",
     *             "email": "usuario@mail.com",
     *             "ultimo_acceso": null,
     *             "activo": 1,
     *             "created_at": "2017-03-06 21:58:46"
     *         },
     *         "message": "Solicitud procesada exitosamente",
     *         "status": 200,
     *         "error": null
     *     }
     *
     * @apiUse TokenNotFoundError
     * @apiUse TokenExpiredError
     * @apiUse TokenBlacklistedError
     */
    public function user(\Illuminate\Http\Request $request)
    {
        $user = null;
        $data = null;

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->jsonNotFound([
                    'message' => 'Usuario no encontrado',
                    'extra'   => 'Not Found',
                ]);
            }

            // Carga los datos de pasajero o conductor
            // dependiendo del tipo de usuario
            if (!$user->isActivo()) {
                return response()->jsonFormat([
                    'error'   => 409,
                    'message' => 'Usuario inactivo',
                ], 409);
            }

            // Lazy Eager Loading Relationship
            $user->load('tipoUsuario');

            $data = $user->toArray();
        } catch (TokenExpiredException $e) {
            return response()->jsonJwtException($e, ['message' => 'Token expirado']);
        } catch (TokenInvalidException $e) {
            return response()->jsonJwtException($e, ['message' => 'Token inválido']);
        } catch (JWTException $e) {
            return response()->jsonJwtException($e, ['message' => 'Error al procesar el Token']);
        } catch (Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'data' => $data,
        ]);
    }
}
