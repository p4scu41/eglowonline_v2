/**
 * @apiDefine TokenNotFoundError
 * @apiVersion 1.0.0
 *
 * @apiError TokenNotFound <code>Token</code> absent
 *
 * @apiErrorExample {json} Response 406 TokenNotFound (example):
 *     HTTP/1.1 406 Not Acceptable
 *     {
 *         "extra": null,
 *         "data": [],
 *         "message": "Token ausente",
 *         "status": 406,
 *         "error": 422
 *     }
 */

/**
 * @apiDefine TokenExpiredError
 * @apiVersion 1.0.0
 *
 * @apiError TokenExpired <code>Token</code> has expired
 *
 * @apiErrorExample {json} Response 401 TokenExpired (example):
 *     HTTP/1.1 401 Unauthorized
 *     {
 *         "extra": {
 *             "message": "Token has expired"
 *         },
 *         "data": [],
 *         "message": "Token expirado",
 *         "status": 401,
 *         "error": 401
 *     }
 */

/**
 * @apiDefine TokenBlacklistedError
 * @apiVersion 1.0.0
 *
 * @apiError TokenBlacklisted The <code>Token</code> has been blacklisted
 *
 * @apiErrorExample {json} Response 401 TokenBlacklisted (example):
 *     HTTP/1.1 401 Unauthorized
 *         {
 *             "extra": {
 *                  "message": "The token has been blacklisted"
 *              },
 *             "data": [],
 *             "message": "Token inválido",
 *             "status": 401,
 *             "error": 401
 *         }
 */

/**
 * @apiDefine JwtHeader
 * @apiVersion 1.0.0
 *
 * @apiHeader {String} Authorization JSON Web Token (Bearer Token)
 *
 * @apiHeaderExample {String} Request Header (example):
 *     Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0L2VnbG93b25saW5lX3YyL3B1YmxpYy9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwNTg4MTU2MSwiZXhwIjoxNTA1OTAzMTYxLCJuYmYiOjE1MDU4ODE1NjEsImp0aSI6InZKNXFBd01tbXVZMkdoQzIifQ.DUHWveTuRYnZfdNfdJX47bP2cMuWIckJV0FZcGhZNJM
 */

/**
 * @apiDefine ValidationError
 * @apiVersion 1.0.0
 *
 * @apiError ValidationInputs Some field is not valid
 *
 * @apiErrorExample {json} Response 422 ValidationInputs (example):
 *     HTTP/1.1 422 Unprocessable Entity
 *         {
 *             "extra": {
 *                 "email": [
 *                     "El campo E-mail no corresponde con una dirección de e-mail válida"
 *                 ]
 *             },
 *             "data": [],
 *             "message": "Datos incorrectos",
 *             "status": 422,
 *             "error": 422
 *         }
 */

/**
 * @apiDefine NotFoundError
 * @apiVersion 1.0.0
 *
 * @apiError NotFound Data not found
 *
 * @apiErrorExample {json} Response 404 NotFound (example):
 *     HTTP/1.1 404 Not Found
 *         {
 *             "extra": null,
 *             "data": [],
 *             "message": "Recurso no encontrado",
 *             "status": 404,
 *             "error": 404
 *         }
 */

/**
 * @apiDefine NoAccessRightError
 * @apiVersion 1.0.0
 *
 * @apiError NoAccessRight Forbidden
 *
 * @apiErrorExample {json} Response 403 NoAccessRight (example):
 *     HTTP/1.1 403 Forbidden
 *         {
 *             "extra": null,
 *             "data": [],
 *             "message": "Acceso no autorizado",
 *             "status": 403,
 *             "error": 403
 *         }
 */
