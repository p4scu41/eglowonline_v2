<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\BaseWebController;
use App\Models\PalabraClave;
use App\Models\ServiciosBackend;
use App\Models\TipoPalabraClave;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PalabraClaveController extends BaseWebController
{
    public $modelClass = 'PalabraClave';
    public $resource   = 'palabrasclaves';
    public $title_sin  = 'Palabra Clave';
    public $title_plu  = 'Palabras Claves';
    public $order_by   = 'palabra';

    public function onBeforeIndex(Request $request)
    {
        parent::onBeforeIndex($request);

        if ($request->has('filter.palabra')) {
            $this->filter[] = ['palabra', 'like', '%'.Helper::stripTags($request->input('filter.palabra')).'%'];
        }

        if ($request->has('filter.tipo_palabra_clave_id')) {
            $this->filter[] = ['tipo_palabra_clave_id', '=', $request->input('filter.tipo_palabra_clave_id')];
        }

        $this->order_by = 'palabra';
    }

    public function onBeforeStore(Request $request)
    {
        parent::onBeforeStore($request);

        // Por default todas las palabra claves registradas desde el front-end son del tipo palabra clave
        //$request->merge(['tipo_palabra_clave_id' => TipoPalabraClave::PALABRA_CLAVE]);
    }

    public function onAfterStore($request = null, $data = null)
    {
        $this->modelObj->sendPalabraClaveToBackend('post');
    }

    public function onAfterUpdate($request = null, $data = null)
    {
        $this->modelObj->sendPalabraClaveToBackend('put');
    }

    public function searchbytext(Request $request)
    {
        $whereIn = [];

        if ($request->has('profesion')) {
            $whereIn = [TipoPalabraClave::PROFESION];
        } else if ($request->has('related_marca')) {
            $whereIn = [TipoPalabraClave::MARCA, TipoPalabraClave::PRODUCTO];
        } elseif ($request->has('palabra_clave')) {
            $whereIn = [TipoPalabraClave::PALABRA_CLAVE];
        }

        return response()->json(PalabraClave::searchByText($request->input('term'), $whereIn));
    }
}
