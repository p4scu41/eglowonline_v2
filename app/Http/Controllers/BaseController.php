<?php

namespace App\Http\Controllers;

// use App\Helpers\PerformanceLogger;
use App\Support\LoggerSupport;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;

/**
 * Base Class Controller
 *
 * @package App\Http\Controllers
 * @author  Pascual <pascual@importare.mx>
 *
 * @property string $modelNamespace
 * @property string $modelClass
 * @property \Illuminate\Database\Eloquent\Model $modelObj
 * @property string $resource
 * @property string $resourceParameter
 * @property array $middleware
 * @property array $filter
 * @property string $order_by
 * @property boolean $is_tracking_performance
 * @property array $except_track_performance
 * @property \App\Helpers\PerformanceLogger $performanceLogger
 * @property \Monolog\Logger $debug_logger
 * @property \Monolog\Logger $error_logger
 *
 * @method public boolean isValidResponse(mixed $param)
 * @method public mixed onBeforeAction(string $action, \Illuminate\Http\Request $request)
 * @method public mixed onBeforeIndex(\Illuminate\Http\Request $request)
 * @method public mixed onBeforeCreate(\Illuminate\Http\Request $request)
 * @method public mixed onBeforeStore(\Illuminate\Http\Request $request)
 * @method public mixed onBeforeShow(\Illuminate\Http\Request $request)
 * @method public mixed onBeforeEdit(\Illuminate\Http\Request $request)
 * @method public mixed onBeforeUpdate(\Illuminate\Http\Request $request)
 * @method public mixed onBeforeDestroy(\Illuminate\Http\Request $request)
 * @method public mixed onAfterAction(string $action, \Illuminate\Http\Request $request)
 * @method public mixed onAfterIndex(\Illuminate\Http\Request $request)
 * @method public mixed onAfterCreate(\Illuminate\Http\Request $request)
 * @method public mixed onAfterStore(\Illuminate\Http\Request $request)
 * @method public mixed onAfterShow(\Illuminate\Http\Request $request)
 * @method public mixed onAfterEdit(\Illuminate\Http\Request $request)
 * @method public mixed onAfterUpdate(\Illuminate\Http\Request $request)
 * @method public mixed onAfterDestroy(\Illuminate\Http\Request $request)
 * @method public \Illuminate\Database\Eloquent\Model findModel(string $id)
 */
class BaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Model namespace.
     *
     * @var string
     */
    protected $modelNamespace = 'App\Models\\';

    /**
     * The model class name associated with the controller.
     *
     * @var string
     */
    protected $modelClass = '';

    /**
     * Current model instance.
     *
     * @var object
     */
    protected $modelObj = null;

    /**
     * Resource name. The Base URI.
     *
     * @var string
     */
    protected $resource = '';

    /**
     * By default, Route::resource will create the route parameters based on the "singularized" version
     * of the resource name. You can easily override this on a per resource basis by passing parameters
     * in the options array. The parameters array should be an associative array of resource names
     * and parameter names:
     *
     * Route::resource('user', 'AdminUserController', ['parameters' => [
     *     'user' => 'admin_user'
     * ]]);
     *
     * @var string
     */
    protected $resourceParameter = null;

    /**
     * The middleware registered on the controller.
     *
     * @var array
     */
    protected $middleware = [];

    /**
     * Filter to apply on index (search)
     *
     * @var array
     */
    public $filter = [];

    /**
     * The ORDER BY sql statement to apply on index.
     *
     * @var string
     */
    public $order_by = null;

    /**
     * If true, track of performance (Time, Memory usage and Query log)
     * if no set, it tackes the value of config app.debug
     *
     * @var boolean
     */
    // public $is_tracking_performance;

    /**
     * Wheter to log sql queries
     *
     * @var boolean
     */
    public $queryLog = true;

    /**
     * Ommit the tracking of this actions
     *
     * @var array
     */
    // public $except_track_performance = []; // 'create', 'show', 'edit'

    /**
     * Instance of PerformanceLogger to get time, memory usage and query log
     *
     * @var App\Helpers\PerformanceLogger
     */
    // public $performanceLogger;

    /**
     * Instance of Monolog\Logger to debug log
     *
     * @var Monolog\Logger
     */
    public $debug_logger;

    /**
     * Instance of Monolog\Logger to debug log
     *
     * @var Monolog\Logger
     */
    public $error_logger;

    /**
     * Call findModel to get the resource.
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return void
     */
    public function __construct(\Illuminate\Http\Request $request)
    {
        $id     = null;
        $route  = $request->route(); // \Illuminate\Support\Facades\Route::getCurrentRoute();
        $this->resourceParameter = $this->resourceParameter ?: str_singular($this->resource);
        // El archivo de logger tendrá el nombre de la clase Controller
        // seguido de AAA-MM-DD
        // Elimina App\ del inicio y reemplaza \ por _
        $className = str_replace(['App\\', '\\'], ['', '_'], get_class($this));
        $this->debug_logger = LoggerSupport::createLogger($className.'_'.date('Y-m-d').'.debug.log');
        $this->error_logger = LoggerSupport::createLogger($className.'_'.date('Y-m-d').'.error.log');
        // $this->queryLog     = false;

        // When execute php artisan route:list
        // $route is null
        if (!empty($route)) {
            // The ID could be in the parameter 'id' or in the parameter call as the name of the resource
            $id = $route->parameter($this->resourceParameter) ?
                $route->parameter($this->resourceParameter) :
                $route->parameter('id');

            $action = $route->getAction(); // \Illuminate\Support\Facades\Route::currentRouteName();
            $method = explode('@', $action['controller']); // Name of the Method in the Controller Class
            $method = $method[1];

            /*if (!isset($this->is_tracking_performance)) {
                $this->is_tracking_performance = config('app.debug');
            }

            if ($this->is_tracking_performance && !in_array($method, $this->except_track_performance)) {
                $this->performanceLogger           = new PerformanceLogger();
                $this->performanceLogger->queryLog = $this->queryLog;
                $this->performanceLogger->class    = get_class($this);
                $this->performanceLogger->function = $method;
                $inputs                            = $request->input();

                if (!empty($id)) {
                    $inputs = ['id' => $id] + $inputs;
                }

                $this->performanceLogger->start();

                if (!empty($inputs)) {
                    $this->performanceLogger->message(json_encode($inputs));
                }
            }*/
        }/* else {
            // Desactivamos el perfomance cuando se ejectua php artisan route:list
            $this->is_tracking_performance = false;
        }*/

        // Get the model with the id pass in the url
        $this->modelObj = $this->findModel($id);
    }

    /**
     * Return true if $params is instance of any:
     *  \Illuminate\Http\RedirectResponse
     *  \Illuminate\Http\Response
     *  \Illuminate\View\View
     *
     * @param mixed $param Instance of RedirectResponse, Response or View
     *
     * @return boolean
     */
    public function isValidResponse($param = null)
    {
        return $param instanceof \Illuminate\Http\RedirectResponse ||
               $param instanceof \Illuminate\Http\Response ||
               $param instanceof \Illuminate\View\View;
    }

    /**
     * Execute before action event
     * Example: if $action is index the method call is onBeforeIndex
     *
     * @param string                   $action  Action name to execute
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onBeforeAction($action, $request)
    {
        $callBack = 'onBefore' . ucfirst(strtolower($action));

        /**
         * Examples:
         *     index   -> onBeforeIndex
         *     create  -> onBeforeCreate
         *     store   -> onBeforeStore
         *     show    -> onBeforeShow
         *     edit    -> onBeforeEdit
         *     update  -> onBeforeUpdate
         *     destroy -> onBeforeDestroy
         */
        $resultBeforeAction = $this->{$callBack}($request);

        return $resultBeforeAction;
    }

    /**
     * Function executed before index
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onBeforeIndex(\Illuminate\Http\Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        // Eager Loading Relationship
        if (empty($modelCls::$withRelations)) {
            if (!empty($request->input('with'))) {
                $relations = explode(',', $request->input('with'));
                $relations = array_filter(array_map('trim', $relations));

                $modelCls::$withRelations = $relations;
            }
        }

        return null;
    }

    /**
     * Function executed before create
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onBeforeCreate(\Illuminate\Http\Request $request)
    {
        return null;
    }

    /**
     * Function executed before store
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onBeforeStore(\Illuminate\Http\Request $request)
    {
        return null;
    }

    /**
     * Function executed before show
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onBeforeShow(\Illuminate\Http\Request $request)
    {
        // Lazy Eager Loading Relationship
        if (!empty($request->input('with'))) {
            $relations = explode(',', $request->input('with'));
            $relations = array_filter(array_map('trim', $relations));

            $this->modelObj->load($relations);
        }

        if (!empty($request->input('appends'))) {
            $appends = explode(',', $request->input('appends'));
            $appends = array_filter(array_map('trim', $appends));

            foreach ($appends as $append) {
                $this->modelObj->{$append} = $this->modelObj->{$append};
            }
        }

        return null;
    }

    /**
     * Function executed before edit
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onBeforeEdit(\Illuminate\Http\Request $request)
    {
        return null;
    }

    /**
     * Function executed before update
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onBeforeUpdate(\Illuminate\Http\Request $request)
    {
        return null;
    }

    /**
     * Function executed before destroy
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onBeforeDestroy(\Illuminate\Http\Request $request)
    {
        return null;
    }

    /**
     * Execute after action event
     * Example: if $action is index the method call is onAfterIndex
     *
     * @param string                   $action  Action name to execute
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onAfterAction($action, $request)
    {
        $callBack = 'onAfter' . ucfirst(strtolower($action));

        /**
         * Examples:
         *     index   -> onAfterIndex
         *     create  -> onAfterCreate
         *     store   -> onAfterStore
         *     show    -> onAfterShow
         *     edit    -> onAfterEdit
         *     update  -> onAfterUpdate
         *     destroy -> onAfterDestroy
         */
        $resultAfterAction = $this->{$callBack}($request);

        // Se movió éste snipped al Middleware AfterAction para capturar también las consultas
        // que se hacen desde la vista
        /* if ($this->is_tracking_performance && !in_array($action, $this->except_track_performance)) {
            $this->performanceLogger->finish();

            $this->debug_logger->info($this->performanceLogger->getInfo());
        }*/

        return $resultAfterAction;
    }

    /**
     * Function executed after index
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onAfterIndex(\Illuminate\Http\Request $request)
    {
        return null;
    }

    /**
     * Function executed after create
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onAfterCreate(\Illuminate\Http\Request $request)
    {
        return null;
    }

    /**
     * Function executed after store
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onAfterStore(\Illuminate\Http\Request $request)
    {
        return null;
    }

    /**
     * Function executed after show
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onAfterShow(\Illuminate\Http\Request $request)
    {
        return null;
    }

    /**
     * Function executed after edit
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onAfterEdit(\Illuminate\Http\Request $request)
    {
        return null;
    }

    /**
     * Function executed after update
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onAfterUpdate(\Illuminate\Http\Request $request)
    {
        return null;
    }

    /**
     * Function executed after destroy
     *
     * @param \Illuminate\Http\Request $request Instance of Request with the input send
     *
     * @return mixed
     */
    public function onAfterDestroy(\Illuminate\Http\Request $request)
    {
        return null;
    }

    /**
     * Find the model based on its primary key value.
     *
     * @param string $id Primary key value of the model
     *
     * @return \Illuminate\Database\Eloquent\Model The loaded model
     */
    protected function findModel($id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        // Call the method like $modelCls::find($id)
        return call_user_func_array([$modelCls, 'find'], [$id]);
    }
}
