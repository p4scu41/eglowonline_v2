<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Agencia;
use App\Models\PalabraClave;
use App\Models\TipoPalabraClave;
use App\Models\Marca;
use App\Models\TipoUsuario;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Exception;
use DB;

class MarcaController extends BaseWebController
{
    public $modelClass = 'Marca';
    public $resource   = 'marcas';
    public $title_sin  = 'Marca';
    public $title_plu  = 'Marcas';
    public $order_by   = 'nombre';

    public function getCatalogs()
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        $this->catalogs = call_user_func_array([$modelCls, 'getCatalogs'], []);
    }

    public function onBeforeIndex(Request $request)
    {
        parent::onBeforeIndex($request);

        if ($request->has('filter.nombre')) {
            $this->filter[] = ['nombre', 'like', '%'.Helper::stripTags($request->input('filter.nombre')).'%'];
        }

        if ($request->has('filter.agencia_id')) {
            $this->filter[] = ['agencia_id', '=', $request->input('filter.agencia_id')];
        }

        $this->order_by = 'nombre';
    }

    public function index(Request $request)
    {
        // Cuando un tipo de usuario Agencia se loguea, solo le mostramos las marcas que le pertenecen
        if (Auth::user()->isAgencia()) {
            $this->filter[] = ['agencia_id', '=', Auth::user()->agencia->id];
        }

        return parent::index($request);
    }

    public function store(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $data = Helper::stripTagsArray($request->input());

        if (empty($data['usuario_id'])) {
            unset($data['usuario_id']);
        }

        $this->validPolicy('create', $modelCls);

        try {
            DB::beginTransaction();
            $this->modelObj = new $modelCls;
            $this->modelObj->fill($data);

            if (Auth::user()->isAgencia()) {
                $this->modelObj->agencia_id = Auth::user()->agencia->id;
            }

            if ($this->modelObj->isValid($modelCls::DO_CREATE)) {
                $this->modelObj->save();

                if ($request->hasFile('logotipo')) {
                    $this->modelObj->uploadFile($request->file('logotipo'));
                }

                PalabraClave::create([
                    'tipo_palabra_clave_id' => TipoPalabraClave::MARCA,
                    'palabra' => $this->modelObj->nombre,
                ]);

                DB::commit();

                $this->response_info['show']    = true;
                $this->response_info['message'] = 'Datos guardados exitosamente.';
                $this->response_info['type']    = 'success';
            } else {
                return redirect($this->resource . '/create')
                    ->withErrors($this->modelObj->validator)
                    ->withInput();
            }
        } catch (Exception $e) {
            DB::rollBack();

            return redirect($this->resource . '/create')
                ->withErrors([Helper::getMessageIfDebug($e)])
                ->withInput();
        }

        $request->session()->flash('response_info', $this->response_info);

        return redirect($this->resource);
    }

    public function update(Request $request, $id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $data = Helper::stripTagsArray($request->input());

        if (empty($data['usuario_id'])) {
            unset($data['usuario_id']);
        }

        try {
            DB::beginTransaction();

            if (empty($this->modelObj)) {
                throw new Exception('Datos no disponibles.', 404);
            }

            $this->validPolicy('update', $this->modelObj);

            $this->modelObj->fill($data);

            if ($this->modelObj->isValid($modelCls::DO_UPDATE)) {
                $this->modelObj->save();

                if ($request->hasFile('logotipo')) {
                    $this->modelObj->uploadFile($request->file('logotipo'));
                }

                DB::commit();

                $this->response_info['show']    = true;
                $this->response_info['message'] = 'Datos actualizados exitosamente.';
                $this->response_info['type']    = 'success';
            } else {
                return redirect($this->resource . '/' . $this->modelObj->id . '/edit')
                    ->withErrors($this->modelObj->validator)
                    ->withInput();
            }
        } catch (Exception $e) {
            DB::rollBack();
            return redirect($this->resource . '/' . $this->modelObj->id . '/edit')
                ->withErrors([Helper::getMessageIfDebug($e)])
                ->withInput();
        }

        $request->session()->flash('response_info', $this->response_info);

        return redirect($this->resource);
    }

    /**
     * Devuelve el listado de marcas que pertenecen a una agencia determinada
     *
     * @param  lluminate\Http\Request $request
     * @return json
     */
    public function getByAgencia(Request $request)
    {
        return response()->json(
            Marca::orderBy('nombre', 'asc')
                ->where('activo', 1)
                ->where('agencia_id', $request->input('id'))
                ->get()
                ->pluck('nombre', 'id')
        );
    }

    /**
     * Obtiene el logotipo de la marca
     *
     * @param  lluminate\Http\Request $request
     * @return json
     */
    public function getLogotipo(Request $request)
    {
        $marca = Marca::where('id', $request->input('id'))->first();

        return response()->json([
            'logotipo' => $marca ? asset($marca->getLogotipo()) : '',
        ]);
    }
}
