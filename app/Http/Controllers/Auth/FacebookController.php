<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Models\User;
use App\Models\PerfilFacebook;
use App\Models\Influenciador;
use App\Models\FacebookPage;
use Illuminate\Support\Facades\Log;
use Exception;
use Facebook;
use File;
use DB;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

/**
 * Class FacebookController
 *
 * @package App
 * @subpackage Http\Controllers\Auth
 * @category Controller
 */
class FacebookController extends Controller
{
    /**
     * @var LaravelFacebookSdk
     */
    private $fb;

    /**
     * FacebookController Constructor
     * Injecting the facebook service from the container
     *
     * @param LaravelFacebookSdk $fb
     */
    public function __construct(LaravelFacebookSdk $fb)
    {
        $this->fb = $fb;
    }

    /**
     * process login through facebook app - redirects to facebook's url
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Redirect
     */
    public function redirectToProvider(Request $request)
    {
        // Send an array of permissions to request
        $loginUrl = $this->fb->getLoginUrl(['email', 'public_profile', 'manage_pages', 'pages_show_list', 'publish_pages', 'publish_actions', 'read_insights']);
        return redirect($loginUrl);
    }

    /**
     * process login redirect from Facebook
     *
     * Checks the pages the user is admin of. If he/she has only one, sets the data on profile's table.
     * If he/she has more than one, the data is saved on a separated table for later processing.
     *
     * After that, the users is logged in the app
     *
     * @param Request $request
     * @return Illuminate\Http\Redirect
     * @throws Exception when there is an error from facebook
     */
    public function handleProviderCallback(Request $request)
    {
        try {
            DB::beginTransaction();
            $token = $this->fb->getAccessTokenFromRedirect();

            // Access token will be null if the user denied the request
            // or if someone just hit this URL outside of the OAuth flow.
            if (!$token) {
                // Get the redirect helper
                $helper = $this->fb->getRedirectLoginHelper();

                if (!$helper->getError()) {
                    abort(403, 'Unauthorized action.');
                }

                // User denied the request
                return redirect('/influenciadores/login')
                    ->with('status', 'Error al iniciar sesión con Facebook. ');
            }

            // if (!$token->isLongLived()) {
                // OAuth 2.0 client handler
            $oauth_client = $this->fb->getOAuth2Client();
            $token        = $oauth_client->getLongLivedAccessToken($token);
            // }

            $this->fb->setDefaultAccessToken($token);

            // Save for later
            // retrieve info for user
            $response = $this->fb->get('/me?fields=id,name,email,link,picture,accounts{id,access_token,about,cover,engagement,link,name,picture}');
            // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
            $facebookUser = $response->getGraphUser();

            // Log::info(json_encode($facebookUser->asArray()));

            $perfilFacebook = null;
            $usuario        = null;
            $influenciador  = null;
            $isNew          = false;

            // review user's facebook's pages
            $pages = $this->checkFacebookPagesFromUser($facebookUser);

            if (is_null($pages)) {
                throw new Exception('No tiene asociada algúna Facebook Page. Póngase en contacto con el administrador.', 422);
            }

            if (!isset($facebookUser['email'])) {
                throw new Exception('No se tiene registro del correo electrónico en Facebook. Póngase en contacto con el Administrador.', 422);
            }

            // checks for facebook user on perfil_facebook
            $perfilFacebook = PerfilFacebook::where('facebook_id', $facebookUser['id'])->first();

            if (empty($perfilFacebook)) {
                // Buscamos en la tabla de usuarios con su email
                $usuario = User::where('email', $facebookUser['email'])->first();

                // Si no existe el usuario con su email
                if (empty($usuario)) {
                    // Lo registramos
                    $usuario = new User;
                    $isNew = true;

                    $usuario->fill([
                        'tipo_usuario_id' => 5,
                        'nombre'          => $facebookUser['name'],
                        'email'           => $facebookUser['email'],
                        'ultimo_acceso'   => date('Y-m-d H:i:s'),
                        'activo'          => 1,
                    ]);

                    if (!$usuario->isValid(User::DO_CREATE)) {
                        return redirect('/influenciadores/login')->with('status', 'Error al registrar el Usuario en el sistema. '.
                            (config('app.debug') ? Helper::errorsToString($usuario->errors) : ''));
                    }

                    $usuario->save();

                    // registramos la cuenta influencer
                    $influenciador = new Influenciador;
                    $influenciador->fill([
                        'usuario_id'            => $usuario->id,
                        'nombre'                => $usuario->nombre,
                        'perfil_completado'     => 0,
                        'pais_id'               => 1,
                        'industria_id'          => 1,
                        'rango_etario_id'       => 1,
                        'tipo_influenciador_id' => 1,
                        'notificar_x_correo'    => true,
                        'genero'                => 3
                    ]);

                    if (!$influenciador->isValid(Influenciador::DO_CREATE)) {
                        return redirect('/influenciadores/login')->with('status', 'Error al registrar el usuario de Facebook en el sistema. '.
                            (config('app.debug') ? Helper::errorsToString($influenciador->errors) : ''));
                    }

                    // insert
                    $influenciador->save();
                } else {
                    $influenciador = $usuario->influenciador;
                }

                // check if profile is empty with influencer id anyways
                $perfilFacebook = PerfilFacebook::where('influenciador_id', $influenciador->id)->first();

                if (empty($perfilFacebook)) {
                    // registramos la cuenta de Facebook
                    $perfilFacebook = new PerfilFacebook;
                    $this->createFacebookProfile($pages, $token, $facebookUser['id'], $perfilFacebook, $influenciador);

                    if (!$perfilFacebook->isValid(PerfilFacebook::DO_CREATE)) {
                        return redirect('/influenciadores/login')->with('status', 'Error al registrar el usuario de Facebook en el sistema. '.
                            (config('app.debug') ? Helper::errorsToString($perfilFacebook->errors) : ''));
                    }

                    // insert
                    $perfilFacebook->save();
                    $isNew = true;
                } else {
                    // Actualizamos los datos con la información devuelta por Facebook
                    $this->createFacebookProfile($pages, $token, $facebookUser['id'], $perfilFacebook, $influenciador);

                    if (!$perfilFacebook->isValid(PerfilFacebook::DO_UPDATE)) {
                        return redirect('/influenciadores/login')->with('status', 'Error al actualizar los datos de la cuenta de Facebook en el sistema. '.
                            (config('app.debug') ? Helper::errorsToString($perfilFacebook->errors) : ''));
                    }

                    // update
                    $perfilFacebook->save();
                }
            } else {
                // Actualizamos los datos con la información devuelta por Facebook
                $this->createFacebookProfile($pages, $token, $facebookUser['id'], $perfilFacebook, $perfilFacebook->influenciador);

                if (!$perfilFacebook->isValid(PerfilFacebook::DO_UPDATE)) {
                    return redirect('/influenciadores/login')->with('status', 'Error al actualizar los datos de la cuenta de Facebook en el sistema. '.
                        (config('app.debug') ? Helper::errorsToString($perfilFacebook->errors) : ''));
                }

                // update
                $perfilFacebook->save();

                $usuario = $perfilFacebook->influenciador->usuario;
            }
            // Logueamos al usuario con Laravel
            Auth::login($usuario);

            $usuario->ultimo_acceso = date('Y-m-d H:i:s');
            $usuario->save();

            $usuario->influenciador->sendInfluenciadorToBackend('put', 'facebook');

            DB::commit();

            return redirect('/');

        } catch (Exception $e) {
            DB::rollBack();

            return redirect('/influenciadores/login')
                ->with('status', 'Error al iniciar sesión con Facebook. '.Helper::getMessageIfDebug($e));
        }

        return redirect('/influenciadores/login')->with('status', 'Error al establecer la comunicación con Facebook.');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }

    public function postTweet(Request $request)
    {
        try {
            $perfilFacebook = PerfilFacebook::find(17);

            if (!empty($perfilFacebook)) {
                Facebook::reconfig([
                    'token'  => $perfilFacebook->oauth_token,
                    'secret' => $perfilFacebook->oauth_token_secret,
                ]);

                // Texto
                $result = Facebook::postTweet([
                    'status' => 'Testing API',
                    //'format' => 'json', // Devuelve la respuesta como string, es necesario json_decode
                ]);

                if (!empty($result)) {
                    // dump($result->id_str);
                }

                // Imagen/Video
                $uploaded_media = Facebook::uploadMedia([
                    'media' => File::get(public_path('img/user_icon.png'))
                ]);

                $result = Facebook::postTweet([
                    'status' => 'Testing Media API',
                    'media_ids' => $uploaded_media->media_id_string,
                ]);

                if (!empty($result)) {
                    // dump($result->id_str);
                }
            }
        } catch (Exception $e) {
            Log::error('Facebook:postTweet -> '.
                $e->getMessage().'. '. Helper::errorsToString(Facebook::logs(), "\n"));
        }
    }

    /**
     * review facebook pages from given node and extract data
     *
     * @param Facebook/GraphNodes/GraphUser $facebookUser
     * @return array|null pages data
     */
    private function checkFacebookPagesFromUser($facebookUser)
    {
        // dd($facebookUser->accounts);
        if (!isset($facebookUser['accounts'])) {
            return null;
        }

        $numberOfPages = count($facebookUser['accounts']);
        $pagesData     = [];

        if ($numberOfPages === 0) {
            return null;
        }

        foreach ($facebookUser['accounts'] as $account) {
            $categories = [];
            if (array_key_exists('category_list', $account)) {
                foreach ($accout['category_list'] as $categoryList) {
                    $categories[] = $categoryList['name'];
                }
            }

            $pagesData[] = [
                'access_token' => $account['access_token'],
                'name'         => $account['name'],
                'screen_name'  => $account['name'],
                'id'           => $account['id'],
                'about'        => isset($account['about']) ? $account['about'] : '',
                'engagement'   => $account['engagement']['count'],
                'link'         => $account['link'],
                'picture'      => $account['picture']['url'],
                'categories'   => count($categories) > 0 ? implode(',', $categories) : ''
            ];
        }

        return $pagesData;
    }

    /**
     * create facebook profile
     *
     * @param array $pages
     * @param string $token
     * @param long $id - facebook id
     * @param PerfilFacebook $perfilFacebook
     * @param Influenciador $influenciador
     */
    private function createFacebookProfile($pages, $token, $id, PerfilFacebook $perfilFacebook, Influenciador $influenciador)
    {
        // save profile with basic data and save pages
        if (count($pages) > 1 && $perfilFacebook->selectedPage == 0) {
            $perfilFacebook->fill([
                'influenciador_id' => $influenciador->id,
                'facebook_id'      => $id,
                'access_token'     => (string) $token,
                'selectedPage'     => 0
            ]);

            // feed pages if profile hasn't completed
            foreach ($pages as $page) {
                $facebookPage = FacebookPage::where('id', $page['id'])
                    ->where('influenciador_id', $influenciador->id)
                    ->first();

                if (empty($facebookPage)) {
                    $facebookPage = new FacebookPage;
                }

                $facebookPage->fill([
                    'id'                  => $page['id'],
                    'influenciador_id'    => $influenciador->id,
                    'name'                => $page['name'],
                    'about'               => isset($page['about']) ? $page['about'] : '',
                    'link'                => $page['link'],
                    'profile_image_url'   => $page['picture'],
                    'engagement'          => $page['engagement'],
                    'access_token'        => $page['access_token'],
                    'categories'          => $page['categories']
                ]);

                $facebookPage->save();
            }

        } else {
            $pageToken = '';
            foreach ($pages as $page) {
                if ($page['id'] === $perfilFacebook->page_id) {

                    $pageToken = $page['access_token'];
                    break;
                }
            }

            $perfilFacebook->fill([
                'influenciador_id'  => $influenciador->id,
                'facebook_id'       => $id,
                'access_token'      => (string) $token,
                'page_access_token' => $pageToken,
                'selectedPage'      => 1
            ]);
        }
    }
}