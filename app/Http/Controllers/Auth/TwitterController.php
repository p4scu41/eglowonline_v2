<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Models\User;
use App\Models\Influenciador;
use App\Models\PerfilTwitter;
use Illuminate\Support\Facades\Log;
use Exception;
use Twitter;
use File;
use DB;

class TwitterController extends Controller
{
    /**
     * process login through twitter app
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider(Request $request)
    {
        try {
            Twitter::reconfig(['token' => '', 'secret' => '']);
            $requestToken = Twitter::getRequestToken(route('twitter.callback'));

            if (isset($requestToken['oauth_token_secret'])) {
                $url = Twitter::getAuthorizeURL($requestToken);

                $request->session()->put('oauth_request_token', $requestToken['oauth_token']);
                $request->session()->put('oauth_request_token_secret', $requestToken['oauth_token_secret']);

                return redirect($url);
            }

            return redirect('/influenciadores/login')->with('status', 'Error al establecer la comunicación con Twitter.');
        } catch (Exception $e) {
            return redirect('/influenciadores/login')
                ->with('status', 'Error al establecer la comunicación con Twitter. '. $e->getMessage().' '.
                        Helper::getMessageIfDebug($e).' '.
                        (config('app.debug') ? Helper::errorsToString(Twitter::logs(), "\n") : '')
                    );
        }
    }

    public function handleProviderCallback(Request $request)
    {
        try {
            if ($request->session()->has('oauth_request_token')) {
                DB::beginTransaction();

                Twitter::reconfig([
                    'token'  => $request->session()->get('oauth_request_token'),
                    'secret' => $request->session()->get('oauth_request_token_secret'),
                ]);

                $accessToken = Twitter::getAccessToken($request->get('oauth_verifier'));

                if (!isset($accessToken['oauth_token_secret'])) {
                    \Log::error(json_encode($request->get()));
                    \Log::error(json_encode($accessToken));

                    return redirect('/influenciadores/login')->with('status', 'Error al iniciar sesión con Twitter. No se puede obtener el token.');
                }

                $credentials = Twitter::getCredentials(['include_email' => 'true']);

                $perfilTwitter = null;
                $usuario       = null;
                $influenciador = null;
                $isNew         = false; // Bandera para determinar si un usuario al loguear se registrará por primera vez

                if (empty($credentials->email)) {
                    return redirect('/influenciadores/login')
                        ->with('status', 'No se puede obtener el Correo Electrónico de la cuenta proporcionada, revise su configuración de Privacidad en Twitter.');
                }

                if (is_object($credentials) && !isset($credentials->error)) {
                    $perfilTwitter = PerfilTwitter::where('screen_name', '@'.$accessToken['screen_name'])->first();

                    // Si el usuario de Twitter no existe en la Base de Datos
                    if (empty($perfilTwitter)) {
                        // Buscamos en la tabla de usuarios con su email
                        $usuario = User::where('email', $credentials->email)->first();

                        // Si no existe el usuario con su email
                        if (empty($usuario)) {
                            // Lo registramos
                            $usuario = new User;
                            $isNew = true;

                            if (empty($credentials->name)) {
                                return redirect('/influenciadores/login')
                                    ->with('status', 'No se puede obtener el Nombre de la cuenta proporcionada, revise su configuración de Privacidad en Twitter.');
                            }

                            if (empty($credentials->email)) {
                                return redirect('/influenciadores/login')
                                    ->with('status', 'No se puede obtener el Correo Electrónico de la cuenta proporcionada, revise su configuración de Privacidad en Twitter.');
                            }

                            $usuario->fill([
                                'tipo_usuario_id' => 5,
                                'nombre'          => $credentials->name,
                                'email'           => $credentials->email,
                                'ultimo_acceso'   => date('Y-m-d H:i:s'),
                                'activo'          => 1
                            ]);

                            if (!$usuario->isValid(User::DO_CREATE)) {
                                \Log::error(json_encode($usuario->errors));

                                return redirect('/influenciadores/login')->with('status', 'Error al registrar el Usuario en el sistema. '.
                                    Helper::errorsToString($usuario->errors));
                            }

                            $usuario->save();

                            // registramos la cuenta influencer
                            $influenciador = new Influenciador;
                            $influenciador->fill([
                                'usuario_id'            => $usuario->id,
                                'nombre'                => $usuario->nombre,
                                'perfil_completado'     => false,
                                'pais_id'               => 1,
                                'industria_id'          => 1,
                                'rango_etario_id'       => 1,
                                'tipo_influenciador_id' => 1,
                                'notificar_x_correo'    => true,
                                'genero'                => 3
                            ]);

                            if (!$influenciador->isValid(Influenciador::DO_CREATE)) {
                                \Log::error(json_encode($influenciador->errors));

                                return redirect('/influenciadores/login')->with('status', 'Error al registrar el usuario de Twitter en el sistema. '.
                                    Helper::errorsToString($influenciador->errors));
                            }

                            // insert
                            $influenciador->save();
                        } else {
                            $influenciador = $usuario->influenciador;

                            if (empty($influenciador)) {
                                // registramos la cuenta influencer
                                $influenciador = new Influenciador;
                                $influenciador->fill([
                                    'usuario_id'            => $usuario->id,
                                    'nombre'                => $usuario->nombre,
                                    'perfil_completado'     => false,
                                    'pais_id'               => 1,
                                    'industria_id'          => 1,
                                    'rango_etario_id'       => 1,
                                    'tipo_influenciador_id' => 1,
                                    'notificar_x_correo'    => true,
                                    'genero'                => 3
                                ]);

                                if (!$influenciador->isValid(Influenciador::DO_CREATE)) {
                                    \Log::error(json_encode($influenciador->errors));

                                    return redirect('/influenciadores/login')->with('status', 'Error al registrar el usuario de Twitter en el sistema. '.
                                        Helper::errorsToString($influenciador->errors));
                                }

                                $isNew = true;
                            }
                        }

                        // registramos la cuenta de Twitter
                        $perfilTwitter = new PerfilTwitter;
                        $isNew = true;

                        $perfilTwitter->fill([
                            'twitter_id'         => $accessToken['user_id'],
                            'influenciador_id'   => $influenciador->id,
                            'screen_name'        => '@' . $accessToken['screen_name'],
                            'oauth_token'        => $accessToken['oauth_token'],
                            'oauth_token_secret' => $accessToken['oauth_token_secret'],
                        ]);

                        if (!$perfilTwitter->isValid(PerfilTwitter::DO_CREATE)) {
                            \Log::error(json_encode($perfilTwitter->errors));

                            return redirect('/influenciadores/login')->with('status', 'Error al registrar el usuario de Twitter en el sistema. '.
                                Helper::errorsToString($perfilTwitter->errors));
                        }

                        // insert
                        $perfilTwitter->save();
                    } else {
                        // Actualizamos los datos con la información devuelta por Twitter
                        $perfilTwitter->fill([
                            'twitter_id'          => $accessToken['user_id'], //  $credentials->id_str,
                            'profile_image_url'   => $credentials->profile_image_url_https,
                            'cantidad_seguidores' => $credentials->followers_count,
                            'oauth_token'         => $accessToken['oauth_token'],
                            'oauth_token_secret'  => $accessToken['oauth_token_secret'],
                        ]);

                        if (!$perfilTwitter->isValid(PerfilTwitter::DO_UPDATE)) {
                            \Log::error(json_encode($perfilTwitter->errors));

                            return redirect('/influenciadores/login')->with('status', 'Error al actualizar los datos de la cuenta de Twitter en el sistema. '.
                                Helper::errorsToString($perfilTwitter->errors));
                        }

                        // update
                        $perfilTwitter->save();

                        $usuario = $perfilTwitter->influenciador->usuario;
                    }

                    // Se vuelve a obtener el usuario de twitter por que de lo contrario
                    // no actualiza los datos despues de ejecutar el insert
                    // $perfilTwitter = PerfilTwitter::where('screen_name', '@'.$accessToken['screen_name'])->first();

                    // Logueamos al usuario con Laravel
                    Auth::login($usuario);

                    $usuario->ultimo_acceso = date('Y-m-d H:i:s');
                    $usuario->save();

                    $request->session()->put('access_token', $accessToken);

                    if ($isNew) {
                        $usuario->influenciador->sendInfluenciadorToBackend('post', 'twitter');
                    } else {
                        $usuario->influenciador->sendInfluenciadorToBackend('put', 'twitter');
                    }

                    DB::commit();

                    return redirect('/');
                }

                return redirect('/influenciadores/login')->with('status', 'Error al iniciar sesión con Twitter.');
            }
        } catch (Exception $e) {
            DB::rollBack();

            return redirect('/influenciadores/login')
                ->with('status', 'Error al iniciar sesión con Twitter. '. $e->getMessage().' '.
                        Helper::getMessageIfDebug($e).' '.
                        (config('app.debug') ? Helper::errorsToString(Twitter::logs(), "\n") : '')
                    );
        }

        return redirect('/influenciadores/login')->with('status', 'Error al establecer la comunicación con Twitter.');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();

        return redirect('/influenciadores/login');
    }

    public function postTweet(Request $request)
    {
        try {
            $perfilTwitter = PerfilTwitter::find(17);

            if (!empty($perfilTwitter)) {
                Twitter::reconfig([
                    'token'  => $perfilTwitter->oauth_token,
                    'secret' => $perfilTwitter->oauth_token_secret,
                ]);

                // Texto
                $result = Twitter::postTweet([
                    'status' => 'Testing API',
                    //'format' => 'json', // Devuelve la respuesta como string, es necesario json_decode
                ]);

                if (!empty($result)) {
                    // dump($result->id_str);
                }

                // Imagen/Video
                $uploaded_media = Twitter::uploadMedia([
                    'media' => File::get(public_path('img/user_icon.png'))
                ]);

                $result = Twitter::postTweet([
                    'status' => 'Testing Media API',
                    'media_ids' => $uploaded_media->media_id_string,
                ]);

                if (!empty($result)) {
                    // dump($result->id_str);
                }
            }
        } catch (Exception $e) {
            Log::error('Twitter:postTweet -> '.Helper::getMessageIfDebug($e).' '.
                '. '. Helper::errorsToString(Twitter::logs(), "\n"));
        }
    }
}
