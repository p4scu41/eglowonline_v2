<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use DB;

class ChangePasswordController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        // $response_info = [];

        try {
            if (empty($user)) {
                throw new Exception('Datos no disponibles.', 404);
            }

            Helper::validPassword($request->input('password'));

            DB::beginTransaction();
            $user->password = bcrypt($request->input('password'));

            if ($user->isValid(User::DO_UPDATE)) {
                $user->save();
                DB::commit();

                return response()->jsonSuccess(['message' => 'Contraseña actualizada exitosamente.']);
                // $response_info['message'] = 'Contraseña actualizada exitosamente.';
                // $response_info['type'] = 'success';
            } else {
                return response()->jsonInvalidData(['message' => Helper::errorsToString($user->errors)]);
                // $response_info['message'] = Helper::errorsToString($user->errors);
                // $response_info['type'] = 'danger';
            }
        } catch (Exception $e) {
            DB::rollBack();

            return response()->jsonException($e);
            // $response_info['message'] = 'Error al procesar los datos. '.Helper::getMessageIfDebug($e);
            // $response_info['type'] = 'danger';
        }

        return response()->jsonSuccess([]);
        // return redirect('/')->with($response_info);
    }
}
