<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\LogEvento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Verifica que el usuario este activo, si no esta, cierra la sesión,
     * de lo contrario registra la fecha de acceso en ultimo_acceso
     *
     * @inheritdoc
     */
    public function authenticated($request, $user)
    {
        if ($user->activo != 1) {
            Auth::logout();

            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    $this->username() => 'El usuario no esta activo.',
                ]);
        }

        // Guardamos la fecha acceso
        $user->ultimo_acceso = date('Y-m-d H:i:s');

        $user->save();

        $isInfluencer = Auth::user()->isCelebridad();

        LogEvento::saveInicioSesion();

        if ($isInfluencer) {
            return redirect('influenciadores/miscampanas');
        }


    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        if (Auth::check()) {
            $isInfluencer = Auth::user()->isCelebridad();

            $this->guard()->logout();

            $request->session()->flush();

            $request->session()->regenerate();

            if ($isInfluencer) {
                return redirect('influenciadores/login');
            }
        }

        return redirect('/');
    }
}
