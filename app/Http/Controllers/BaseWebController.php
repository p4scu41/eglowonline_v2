<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route; // Get the parameter $route in method find
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use Exception;
use Gate;
use DB;

class BaseWebController extends Controller
{
    /**
     * Model namespace.
     *
     * @var String
     */
    protected $modelNamespace = 'App\Models\\';

    /**
     * The model class associated with the controller.
     *
     * @var string
     */
    protected $modelClass = '';

    /**
     * Current model instance.
     *
     * @var object
     */
    protected $modelObj = null;

    /**
     * Resource name.
     *
     * @var string
     */
    protected $resource = '';

    /**
     * By default, Route::resource will create the route parameters based on the "singularized" version
     * of the resource name. You can easily override this on a per resource basis by passing parameters
     * in the options array. The parameters array should be an associative array of resource names
     * and parameter names:
     *
     * Route::resource('user', 'AdminUserController', ['parameters' => [
     *     'user' => 'admin_user'
     * ]]);
     *
     * @var string
     */
    protected $resourceParameter = null;

    /**
     * The middleware registered on the controller.
     *
     * @var array
     */
    protected $middleware = [];

    /**
     * Set if the class has to implement the policy to check the authorization
     *
     * @var boolean
     */
    protected $policy = true;

    /**
     * Data to show in the view
     *
     * @var array
     */
    public $response_info = [
        'show'     => false,
        'error'    => false,
        'message'  => '',
        'type'     => 'info', // success, info, warning, danger
        'resource' => '', // Se asigna el valor de resource en el constructor
    ];

    /**
     * Titulo singular para breadcrumb y encabezado
     *
     * @var string
     */
    public $title_sin = '';

    /**
     * Titulo plural para breadcrumb y encabezado
     *
     * @var string
     */
    public $title_plu = '';

    /**
     * Catalogs used to search, create and update
     *
     * @var array
     */
    public $catalogs = [];

    /**
     * Filter to apply on index (search)
     *
     * @var array
     */
    public $filter = [];

    /**
     * The name of the column to sort.
     *
     * @var string
     */
    public $order_by = null;

    /**
     * Call findModel to get the resource
     *
     * @param  Illuminate\Routing\Route $route
     * @return void
     */
    public function __construct(\Illuminate\Http\Request $request)
    {
        $route  = $request->route(); // \Illuminate\Support\Facades\Route::getCurrentRoute();
        $action = \Illuminate\Support\Facades\Route::currentRouteName(); // $route->getAction();
        $id     = null;
        $this->resourceParameter = $this->resourceParameter ?: str_singular($this->resource);

        // When execute php artisan route:list
        // $route is null
        if (!empty($route)) {
            // The ID could be in the parameter 'id' or in the parameter call as the name of the resource
            $id = $route->parameter($this->resourceParameter) ?
                $route->parameter($this->resourceParameter) :
                $route->parameter('id');
        }

        $this->modelObj = $this->findModel($id);

        $this->response_info['resource'] = $this->resource;

        $this->onBeforeAction($action, $request);
    }

    /**
     * Populate catalogs, need to be implemented
     * call onBeforeIndex, onBeforeCreate, onBeforeEdit
     *
     * @return void
     */
    public function getCatalogs()
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        if (method_exists($modelCls, 'getCatalogs')) {
            $this->catalogs = call_user_func_array([$modelCls, 'getCatalogs'], []);
        }
    }

    /**
     * Execute before action event
     *
     * @param  array $action
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function onBeforeAction($action, $request)
    {
        //$action['as'] == \Illuminate\Support\Facades\Route::currentRouteName()
        $action_name = str_replace($this->resource.'.', '', $action);

        switch ($action_name) {
            case 'index':
                $this->onBeforeIndex($request);
                break;
            case 'create':
                $this->onBeforeCreate($request);
                break;
            case 'store':
                $this->onBeforeStore($request);
                break;
            case 'show':
                $this->onBeforeShow($request);
                break;
            case 'edit':
                $this->onBeforeEdit($request);
                break;
            case 'update':
                $this->onBeforeUpdate($request);
                break;
            case 'destroy':
                $this->onBeforeDestroy($request);
                break;
        }
    }

    /* ******* */
    // BEFORE
    /* ******* */

    public function onBeforeIndex(\Illuminate\Http\Request $request)
    {
        $this->getCatalogs();
        //
    }

    public function onBeforeCreate(\Illuminate\Http\Request $request)
    {
        $this->getCatalogs();
        //
    }

    public function onBeforeStore(\Illuminate\Http\Request $request)
    {
        //
    }

    public function onBeforeShow(\Illuminate\Http\Request $request)
    {
        //
    }

    public function onBeforeEdit(\Illuminate\Http\Request $request)
    {
        $this->getCatalogs();
        //
    }

    public function onBeforeUpdate(\Illuminate\Http\Request $request)
    {
        //
    }

    public function onBeforeDestroy(\Illuminate\Http\Request $request)
    {
        //
    }

    /* ******* */
    // BEFORE
    /* ******* */
    public function onAfterIndex($request = null, $data = null)
    {
        //
    }

    public function onAfterCreate($request = null, $data = null)
    {
        //
    }

    public function onAfterStore($request = null, $data = null)
    {
        //
    }

    public function onAfterShow($request = null, $data = null)
    {
        //
    }

    public function onAfterEdit($request = null, $data = null)
    {
        $this->getCatalogs();
        //
    }

    public function onAfterUpdate($request = null, $data = null)
    {
        //
    }

    public function onAfterDestroy($request = null, $data = null)
    {
        //
    }

    public function validPolicy($action, $param)
    {
        if ($this->policy) {
            $this->authorize($action, $param);
        }
    }

    /**
     * Finds the model based on its primary key value.
     *
     * @param  string $id
     * @return \Object the loaded model
     */
    protected function findModel($id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        // Call the method like $modelCls::find($id)
        return call_user_func_array([$modelCls, 'find'], [$id]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $models   = [];

        $this->validPolicy('index', $modelCls);

        if ($request->session()->has('response_info')) {
            $this->response_info = $request->session()->get('response_info');
        }

        try {
            $models = call_user_func_array([$modelCls, 'filter'], [
                $this->filter,//array_filter($request->input('filter', [])),
                true,
                $request->input('order_by', $this->order_by),
                $request->input('order_direction', 'ASC'),
            ]);
        } catch (Exception $e) {
            $this->response_info['error']   = true;
            $this->response_info['type']    = 'danger';
            $this->response_info['message'] = 'Error al procesar los datos. ' .
                                               Helper::getMessageIfDebug($e);
        }

        $this->onAfterIndex($request, $this->response_info);

        return view($this->resource.'.index', [
            'models'        => $models,
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => $this->title_plu,
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modelCls = $this->modelNamespace . $this->modelClass;
        $model    = new $modelCls;

        $this->validPolicy('create', $modelCls);

        $this->onAfterCreate();

        return view($this->resource.'.create', [
            'model'         => $model,
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => $this->title_sin,
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        $this->validPolicy('create', $modelCls);

        try {
            DB::beginTransaction();
            $this->modelObj = new $modelCls;
            $this->modelObj->fill(Helper::stripTagsArray($request->input()));

            if ($this->modelObj->isValid($modelCls::DO_CREATE)) {
                $this->modelObj->save();
                DB::commit();

                $this->response_info['show']    = true;
                $this->response_info['type']    = 'success';
                $this->response_info['message'] = 'Datos guardados exitosamente.';
            } else {
                return redirect($this->resource . '/create')
                    ->withErrors($this->modelObj->validator)
                    ->withInput();
            }
        } catch (Exception $e) {
            DB::rollBack();

            return redirect($this->resource . '/create')
                ->withErrors([Helper::getMessageIfDebug($e)])
                ->withInput();
        }

        $request->session()->flash('response_info', $this->response_info);

        $this->onAfterStore($request);

        return redirect($this->resource);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        if (empty($this->modelObj)) {
            $this->response_info['error']   = true;
            $this->response_info['type']    = 'danger';
            $this->response_info['message'] = 'Datos no disponibles.';

            request()->session()->flash('response_info', $this->response_info);
            return redirect($this->resource);
        }

        $this->validPolicy('view', $this->modelObj);

        $this->onAfterShow($id, $this->response_info);

        return view($this->resource.'.show', [
            'model'         => $this->modelObj,
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => $this->title_sin,
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        if (empty($this->modelObj)) {
            $this->response_info['error']   = true;
            $this->response_info['type']    = 'danger';
            $this->response_info['message'] = 'Datos no disponibles.';

            request()->session()->flash('response_info', $this->response_info);
            return redirect($this->resource);
        }

        $this->validPolicy('update', $this->modelObj);

        $this->onAfterEdit($id);

        return view($this->resource.'.edit', [
            'model'         => $this->modelObj,
            'labels'        => $modelCls::$labels,
            'response_info' => $this->response_info,
            'title'         => $this->title_sin,
            'catalogs'      => $this->catalogs,
            'modelClass'    => $this->modelNamespace . $this->modelClass,
            'policy'        => $this->policy,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modelCls = $this->modelNamespace . $this->modelClass;

        try {
            DB::beginTransaction();

            if (empty($this->modelObj)) {
                throw new Exception('Datos no disponibles.', 404);
            }

            $this->validPolicy('update', $this->modelObj);

            $this->modelObj->fill(Helper::stripTagsArray($request->input()));

            if ($this->modelObj->isValid($modelCls::DO_UPDATE)) {
                $this->modelObj->save();
                DB::commit();

                $this->response_info['show']    = true;
                $this->response_info['type']    = 'success';
                $this->response_info['message'] = 'Datos actualizados exitosamente.';
            } else {
                return redirect($this->resource . '/' . $this->modelObj->id . '/edit')
                    ->withErrors($this->modelObj->validator)
                    ->withInput();
            }
        } catch (Exception $e) {
            DB::rollBack();

            return redirect($this->resource . '/' . $this->modelObj->id . '/edit')
                ->withErrors([Helper::getMessageIfDebug($e)])
                ->withInput();
        }

        $request->session()->flash('response_info', $this->response_info);

        $this->onAfterUpdate($request);

        return redirect($this->resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            if (empty($this->modelObj)) {
                throw new Exception('Datos no disponibles.', 404);
            }

            $this->validPolicy('delete', $this->modelObj);

            $this->modelObj->delete();

            DB::commit();

            $this->response_info['show']    = true;
            $this->response_info['type']    = 'success';
            $this->response_info['message'] = 'Datos eliminados exitosamente.';
        } catch (Exception $e) {
            DB::rollBack();
            $this->response_info['error']   = true;
            $this->response_info['type']    = 'danger';
            $this->response_info['message'] = 'Error al procesar los datos. ' .
                                               Helper::getMessageIfDebug($e);
        }

        $request->session()->flash('response_info', $this->response_info);

        $this->onAfterDestroy($request, $this->response_info);

        return redirect($this->resource);
    }
}
