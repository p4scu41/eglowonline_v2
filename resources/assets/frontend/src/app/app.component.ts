import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public uri_original = localStorage.getItem('uri_original');
  public title = 'WebApp';

  constructor(public router: Router) {
    this.redirectURI()
  }

  redirectURI() {
    if (this.uri_original != null) {
      if (this.uri_original.length > 0) {
        this.router.navigate([this.uri_original]);
      }
    }

  }
}
