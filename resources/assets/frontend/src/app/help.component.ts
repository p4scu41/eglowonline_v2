import { Component } from '@angular/core';

@Component({
    selector: 'app-help',
    template: `<h1>{{title}}</h1>`
})
export class HelpComponent {
    title = 'Help';
}
