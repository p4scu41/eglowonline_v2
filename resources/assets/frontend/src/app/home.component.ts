import { Component } from '@angular/core';

@Component({
    selector: 'app-home',
    template: `<h1>{{title}}</h1> - Eglow`
})
export class HomeComponent {
    title = 'hello world';
}
