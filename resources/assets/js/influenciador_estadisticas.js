/* globals $, errorAlert, swal, echarts, config */
'use strict';

$(document).ready(function (){
    let $charCrecimientoFollowers = $('#chartCrecimientoFollowers');

    // graphic for followers's monthly growing
    function graphicForFollowersGrowing(div, datos) {
        var echartLine = echarts.init(document.getElementById(div));

        echartLine.setOption({
            tooltip : {
                trigger: 'axis'
            },
            // legend: {
            //     data:['Mes de mayo']
            // },
            toolbox: {
                show : true,
                feature : {
                    saveAsImage : {
                        show: true,
                        title: 'Guardar como imagen'
                    }
                }
            },
            grid: {
                left: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: datos.xAxis
            },
            yAxis: {
                type:  'value',
                scale: true,
                max:   datos.max,
                min:   datos.min
            },
            series: datos.series
        });
    }

    $.ajax({
        url:      $charCrecimientoFollowers.data('url'),
        type:     'POST',
        dataType: 'json',
        data:     {
            influenciadorId: $('#influenciadorId').val()
        },
        beforeSend: function () {
            $('#cargarGraficaFollowers').removeClass('hide');
        }
    })
    .done(function (respuesta) {
        $('#cargarGraficaFollowers').addClass('hide');

        if (respuesta.status === 'OK') {
            // draw graphic hashtags
            graphicForFollowersGrowing('chartCrecimientoFollowers', respuesta);
            $charCrecimientoFollowers.removeClass('hide');
        }

        if (respuesta.status === 'fail') {
            console.log('No existe información para graficar');
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        $('#cargarGraficaFollowers').addClass('hide');
        console.log(textStatus + ': ' + errorThrown);
    });
});