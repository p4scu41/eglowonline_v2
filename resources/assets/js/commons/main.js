/* globals $, config, helper, Cookies, moment */
'use strict';

$(document).ready(function() {
    // Establece el idioma a español de moment
    moment.locale('es');

    // Detectamos si estaba colapsado el sidebar
    if (Cookies.get('sidebar-collapse') != undefined && Cookies.get('sidebar-collapse') == 1 && $('section.sidebar').length != 0) {
        $('body').addClass('sidebar-collapse');
    }

    // Arregla el bug de Yii2, en la paginación que agrega doble ? a los enlaces
    /*$('a').each(function(){
        if ($(this).attr('href')) {
            $(this).attr('href', $(this).attr('href').replace('??', '?'));
        }
    });*/

    // Agrega un spinner al click del boton, link con la clase btn y los enlaces del paginador
    // omite los link de eliminar ya que causa conflicto al enviar el POST
    $('body').on('click', 'a.btn:not(.no-loading):not([title="Eliminar"]):not(.sidebar-toggle):not([data-original-title="Eliminar"]):not([class*="load-ajax-view"]):not([class^="kv-file"]), '+
        'button:not(.no-loading):not(.btn-box-tool):not([data-dismiss="modal"]):not(.dropdown-toggle):not([id^="mce"]):not([class^="mce"]):not([class*="load-ajax-view"]):not([class^="kv-file"]), '+
        '.pagination a', function (event){
            var $this = $(this),
                cache = $this.html();

            // Si ya tiene agregado el spinner
            // ya no lo volvemos ha agregar
            if ($this.data('spinner') != 1) {
                // Bandera para determinar que el spinner esta agregado
                $this.data('spinner', 1);
                $this.html(config.spinner);

                if (!$this.data('keepspinner')) {
                    setTimeout(function () {
                        $this.html(cache);
                        // Bandera para determinar que el spinner se ha quitado
                        $this.data('spinner', 0);
                    }, 3000);
                }
            }

            //return true;
    });

    // Agrega un spinner a los enlaces del menu
    $('.sidebar-menu, .side-menu').on('click', 'a:not(.no-loading):not(.sidebar-toggle):not([href="#"])', function () {
        $(this).append('<i class="fa fa-spinner fa-pulse" style="width: auto;"></i>');
    });

    // Agrega efecto al alert de notificación
    $('.alert.notification').each(function(){
        var $this = $(this);

        // Efecto blink
        $this.fadeOut(1000);
        $this.fadeIn(1000);

        /*setTimeout(function () {
            $this.fadeOut('slow');
        }, 10000);*/
    });

    // Al colapsar el sidebar se guarda una coockie para mantener su status
    $('.sidebar-toggle').click(function(event) {
        if ($('body').hasClass('sidebar-collapse')) {
            Cookies.set('sidebar-collapse', 0);
        } else {
            Cookies.set('sidebar-collapse', 1);
        }

        return true;
    });

    // Agrega la funcionalidad del box-header para poder ocultar su contenido al hacer click
    $('.box-header').click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        var _this = $(this);

        //Find the box parent
        var box = _this.parents('.box').first();
        //Find the button trigger
        var btnCollapse = _this.find('[data-widget="collapse"]').first();
        //Find the body and the footer
        var box_content = box.find('> .box-body, > .box-footer, > form  >.box-body, > form > .box-footer');

        if (!box.hasClass('collapsed-box')) {
            //Convert minus into plus
            btnCollapse.children(':first')
                .removeClass('fa-minus')
                .addClass('fa-plus');
            //Hide the content
            box_content.slideUp(500, function () {
                box.addClass('collapsed-box');
            });
        } else {
            //Convert plus into minus
            btnCollapse.children(':first')
                .removeClass('fa-plus')
                .addClass('fa-minus');
            //Show the content
            box_content.slideDown(500, function () {
                box.removeClass('collapsed-box');
            });
        }

    });

    // Habilita la carga de vistas por AJAX
    /*if (config.load_ajax_view) {
        $('body').on('click', '.load-ajax-view', helper.load_ajax_view);
    }*/

    $('#modalChangePass').on('shown.bs.modal', function (e) {
        $('#password').focus();
    });

    // Implementa la carga de select dependiente por Ajax
    $('body').on('change', '.load-ajax-onchange', function(event) {
        helper.fillSelectByAjax({
            type: 'GET',
            select: $(this).data('target'),
            url: $(this).data('url'),
            data: {id: $(this).val()},
        });
    });

    // Arregla el bug que cuando se muestra el modal
    // a veces se queda fijo el tooltip y no se oculta
    $('[id^="modal"]').on('shown.bs.modal', function() {
       $('[data-toggle="tooltip"]').tooltip('hide');
    });

    // Implementa el envio de form a partir de los data de un link
    // Por ejemplo el confirm al eliminar un elemento
    $('body').on('click', 'a[data-form]', function(event) {
        event.preventDefault();
        var $this = $(this);

        if ($this.data('confirm')) {
            $.jAlert({
                type: 'confirm',
                confirmQuestion: $this.data('confirm'),
                confirmBtnText: 'Aceptar',
                denyBtnText: 'Cancelar',
                onConfirm: function () {
                    $this.html(config.spinner);
                    helper.dynamicSendForm($this.attr('href'), $this.data('method'));
                }
            });
        } else {
            $this.html(config.spinner);
            helper.dynamicSendForm($this.attr('href'), $this.data('method'));
        }
    });

    // Gentelella
    // Agrega el evento click al title del panel para que colapse
    // o muestre el contenido
    $('body').on('click', '.x_panel .x_title', function(event) {
        event.preventDefault();
        event.stopPropagation();

        console.log(event);

        var $BOX_PANEL = $(this).closest('.x_panel'),
            $ICON = $(this).find('.collapse-link i'),
            $BOX_CONTENT = $BOX_PANEL.find('.x_content');

        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
            $BOX_CONTENT.slideToggle(200, function(){
                $BOX_PANEL.removeAttr('style');
            });
        } else {
            $BOX_CONTENT.slideToggle(200);
            $BOX_PANEL.css('height', 'auto');
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');

        return false;
    });

    // Gentelella
    // Deshabilita el evento debido a que se ejecuta dos veces,
    // debido al evento click que se agrego al x_title
    $('.collapse-link').off('click');

    // helper.fixHeightContent();

    // https://laravel.com/docs/5.3/csrf
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#formCambiarPass').submit(function (event) {
        event.preventDefault();
        event.stopPropagation();

        var $this = $(this);

        if (!$this.valid()) {
            return false;
        }

        $.ajax({
            url: $this.attr('action'),
            type: $this.attr('method'),
            dataType: 'json',
            data: $this.serialize(),
        })
        .done(function(response) {
            swal({
                    title: 'Contraseña actualizada exitosamente.',
                    type: 'success',
                    confirmButtonColor: config.color_red,
                    confirmButtonText: 'Aceptar',
                });
        })
        .fail(function(jqXHR) {
            errorAlert('Error al procesar los datos. ' + jqXHR.responseJSON.message);
        })
        .always(function() {
            $('#modalChangePass').modal('hide');
            $this.trigger('reset');
        });
    });

    // $.notify('Estamos trabajando para mejorar el servicio, <br>algunos datos no estarán disponibles.', 'info');
});

// Muestra el Pace al momento de cambiar de página
window.onbeforeunload = function () {
    // $('.pace').removeClass('pace-inactive').addClass('pace-active');
    NProgress.start();
    NProgress.set(0.3);
};

if (config.debug) {
    $(document).ajaxError(helper.traceAjaxError);
    window.onerror = helper.traceError;
}
