var config = {
    // Habilita el seguimiento de errores
    'debug': false,
    // Icono que se muestra en los botones para indicar que se esta procesando la solicitud
    spinner: '<i class="fa fa-pulse fa-spinner fa-lg"></i>',
    spinner_input: '<i class="fa fa-pulse fa-spinner fa-lg loading-indicator"></i>',
    // Determina si las vistas se cargarán por Ajax
    load_ajax_view: false,
    color_red: '#00bdc5',
    color_blue: '#4E6185',
    color_lightgray: '#D8D8D8',
    color_green: '#00D800',
    color_twitter: '#5BEDEA',
    color_facebook: '#3D5A98',
    color_instagram: '#DCD264',
    color_youtube: '#FF4A4A',
    color_retweets: '#25023E',
    color_favorites: '#4D0282',
    color_replies: '#8C54B4',
    grafica_newbie : '#D8D8D8',
    grafica_amateur: '#4E6185',
    grafica_pro : '#544991',
    grafica_expert:'#322774',
    grafica_mreglow: '#00bdc5',
    grafica_inactivos: '#C68BE1'
};