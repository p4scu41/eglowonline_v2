/* globals $, helper */
'use strict';

$(document).ready(function() {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
    });

    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY',
        locale: 'es',
        keepOpen: false,
        showClear: true,
        showClose: true,
        showTodayButton: true,
        calendarWeeks: true,
    }).on('dp.change', function (event) {
        $(this).trigger('change');
        $(this).trigger('focusout');
    });

    $('.timepicker').datetimepicker({
        format: 'HH:mm', //'hh:mm a', //'LT',
        locale: 'es',
        keepOpen: false,
        showClear: true,
        showClose: true,
        showTodayButton: true,
        calendarWeeks: true,
    }).on('dp.change', function (event) {
        $(this).trigger('change');
        $(this).trigger('focusout');
    });

    /*$('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
        locale: 'es',
    }).on('dp.change', function (event) {
        $(this).trigger('change');
        $(this).trigger('focusout');
    });*/

    $('.select2').each(function() {
        var options = {
            language: 'es',
            minimumInputLength: ($(this).data('minimuminputlength') ? $(this).data('minimuminputlength') : 0)
        };

        if ($(this).data('ajax--url')) {
            options.initSelection = function(element, callback) {
                var url = $(element).data('ajax--url')+ '?id=' + $(element).val();

                return $.getJSON(url, function(data) {
                        return callback(data.results);
                    });
            };
        }

        $(this).select2(options);

        if ($(this).data('ajax--url')) {
            $(this).trigger('change');
            $(this).trigger('focusout');
        }
    });

    $('.table').stickyTableHeaders();

    // Eliminamos la cabecera que agrega stickyTableHeaders debido a que se duplica
    // al exportar a pdf o excel
    /*$('.kv-grid-toolbar .export-pdf, .kv-grid-toolbar .export-xls').click(function(event) {
        $(this).closest('.grid-view').find('.tableFloatingHeader').remove();
        return true;
    });*/

    $('.btnResetSelect2').click(function() {
        $('#' + $(this).data('select2')).val(null).trigger('change');
    });

    /*$('.fancybox').fancybox();

    helper.colorpickerInit();*/

    helper.initValidate();

    // $('.selectToList').selectToList({});

    // $('.resizable').resizable();

    // $('[data-toggle="popover"]').popover();

    if (typeof(Handlebars) != "undefined") {
        // Registra Helpers para Handlebars
        /* http://stackoverflow.com/questions/8853396/logical-operator-in-a-handlebars-js-if-conditional */
        Handlebars.registerHelper("ifCond",function(v1,operator,v2,options) {
            switch (operator)
            {
                case "==":
                    return (v1==v2)?options.fn(this):options.inverse(this);

                case "!=":
                    return (v1!=v2)?options.fn(this):options.inverse(this);

                case "===":
                    return (v1===v2)?options.fn(this):options.inverse(this);

                case "!==":
                    return (v1!==v2)?options.fn(this):options.inverse(this);

                case "&&":
                    return (v1&&v2)?options.fn(this):options.inverse(this);

                case "||":
                    return (v1||v2)?options.fn(this):options.inverse(this);

                case "<":
                    return (v1<v2)?options.fn(this):options.inverse(this);

                case "<=":
                    return (v1<=v2)?options.fn(this):options.inverse(this);

                case ">":
                    return (v1>v2)?options.fn(this):options.inverse(this);

                case ">=":
                 return (v1>=v2)?options.fn(this):options.inverse(this);

                default:
                    return eval(""+v1+operator+v2)?options.fn(this):options.inverse(this);
            }
        });

        Handlebars.registerHelper('join', function(arreglo, options) {
            var result = '';

            for (var i in arreglo) {
                result += options.fn(arreglo[i]) + ', ';
            }

            return result.substr(0, result.length-2);
        });

        Handlebars.registerHelper('join_slash', function(arreglo, options) {
            var result = '';

            for (var i in arreglo) {
                result += options.fn(arreglo[i]) + ' / ';
            }

            return result.substr(0, result.length-2);
        });

        Handlebars.registerHelper('substr', function(str, start, length) {
            start = start | 0;
            length = length | 120;

            if (str.length > length) {
                return str.substr(start, length) + '...';
            }

            return str;
        });
    }
});
