/*global $, base_url, config, errorAlert, yii, tinyMCE */
'use strict';

var helper = {
    /**
     * Rellena por la izquierda con un caracter hasta llegar a cierta longitud
     *
     * @param {string} nr  Cadena a rellenar
     * @param {string} n   Longitud a completar
     * @param {string} str Caracter a insertar, default 0
     *
     * @return {string} Cadena rellenada por la izquierda
     */
    padLeft: function (nr, n, str) {
        return Array(n-String(nr).length+1).join(str||'0')+nr;
    },

    /**
     * Compara si un valor es undefined o null
     *
     * @param {mixed} val Valor a evaluar
     *
     * @return {boolean}
     */
    isEmpty: function (val) {
        return (typeof(val) === 'undefined' || val === null || val === '');
    },

    tinymceInit: function (selector) {
        selector = selector || 'textarea';

        tinyMCE.init({
            selector: selector,
            language: 'es_MX',
            plugins: [
                'responsivefilemanager advlist autolink lists link image charmap preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'responsivefilemanager | preview media | forecolor backcolor',
            image_advtab: true,
            //relative_urls: false,
            //document_base_url: urlBase,

            external_filemanager_path: base_url + 'filemanager/',
            filemanager_title: 'Archivos',
            filemanager_sort_by: 'name',
            external_plugins: {'filemanager': base_url + 'js/plugins/tinymce/plugins/responsivefilemanager/plugin.min.js'}
        });

        $('textarea').parents('form').on('beforeValidate', function() {
            tinyMCE.triggerSave();
        });
    },

    /**
     * Cargar las vistas por ajax en el modal modalAjaxView
     * @param  event event
     * @return void
     */
    load_ajax_view: function(event, modal) {
        event.preventDefault();
        event.stopPropagation();

        modal = modal || '#modalAjaxView';
        var $this = $(this),
            cache = $this.html();

        $this.html(config.spinner);

        $.get($(this).attr('href'), function(data) {
            $(modal + ' .modal-body').html(data);
            $(modal).modal('show');
        })
        .fail(function () {
            errorAlert('Error al obtener los datos.');
        })
        .always(function () {
            $this.html(cache);
        });

        $(modal).on('show.bs.modal', function () {
            $(this).find('input').iCheck({
                checkboxClass: 'icheckbox_square-orange',
                radioClass: 'iradio_square-orange',
            });
        });
    },

    /**
     * Obtiene información general del error y lo muestra en un alert
     *
     * @param {string} message
     * @param {string} url
     * @param {int} line
     * @param {int} col
     * @param {string} error
     */
    traceError: function (message, url, line, col, error) {
        if (message) {
            var info = 'Error: ' + message + "<br/>\n" +
                'Url: ' + url + "<br/>\n" +
                'Line: ' + line;

            info += !col ? '' : "<br/>\ncolumn: " + col;
            info += !error ? '' : "<br/>\nerror: " + error;

            console.log(info);
            //alert(info);
            errorAlert(info);
        }
    },

    /**
     * Obtiene información general del error Ajax y lo muestra en un alert
     *
     * @param {object} event
     * @param {object} jqxhr
     * @param {object} settings
     * @param {string} thrownError
     */
    traceAjaxError: function(event, jqxhr, settings, thrownError) {
        if (thrownError != '' && jqxhr.statusText != 'abort') {
            var info = 'Error Ajax: ' + thrownError + "<br/>\n" +
                'status: ' + jqxhr.status + "<br/>\n" +
                'statusText: ' + jqxhr.statusText + "<br/>\n" +
                'url: ' + settings.url + "<br/>\n" +
                'data: ' + settings.data + "<br/>\n" +
                'responseText: ' + (jqxhr.responseText ? jqxhr.responseText.substr(0, 1100) : '');

            console.log(info);
            //alert(info);
            errorAlert(info);
        }
    },

    /**
     * Llena un elemento select con el listado de elementos porporcionados
     *
     * @param {string} selector Selector CSS u Objecto jQuery que hace referencia al Select
     * @param {JSON}|{array} list Listado de elementos a insertar, deben ser un arreglo asociativo value -> text, o un JSON con pares [value: '', text: '']
     * @param {boolean} reset Indica si el Select será limpiado antes de insertar los nuevos datos, Default true
     * @param {string} value Si los items de list es un objeto, value es el atributo que contiene el valor
     * @param {string} text Si los items de list es un objeto, text es el atributo que contiene la descripción o etiqueta
     */
    fillSelect: function(selector, list, reset, value, text)
    {
        reset = reset || true;
        value = value || '';
        text = text || '';

        var item = null,
            $select = typeof(selector) === 'string' ? $(selector) : selector;

        if (reset == true) {
            $select.find('option:not(:first-child)').remove();
        }

        if (list.length != 0) {
            for (item in list) {
                // Si es un objeto JSON
                if (value != '' && text != '') {
                    // Si existen los pares de elementos
                    if (list[item][value] != undefined && list[item][text] != undefined) {
                        $select.append('<option value="'+list[item][value]+'">'+list[item][text]+'</option>');
                    }
                } else {
                    // Si es un Array
                    $select.append('<option value="'+item+'">'+list[item]+'</option>');
                }
            }
        }

        // Lanzamos el evento change debido a que se actualizó la lista de elementos
        $select.trigger('change');
    },

    /**
     * Llena un select a partir de la respuesta de una solicitud Ajax
     *
     * @param {JSON} options Las opciones válidas son:
     * @param {string} select Selector CSS u Objecto jQuery que hace referencia al Select
     * @param {boolean} resetSelect: Indica si las opciones del select serán eliminadas antes de cargar los datos, Default true
     * @param {boolean} showLoadIndicator: Indica si se mostrara un indicador de cargando mientras no se devuelva respuesta de la solicitud, Default true
     * @param {'' | string|object} containerIndicator: Indica el contenedor donde se mostrara el spinner, puede ser vacio (mostrará el indicador despues del elemento), selector u objecto (mostrará dentro del contenedor especificado)
     * @param {string} value: Si la respuesta es una lista de objetos este indica el key donde se encuentra el valor
     * @param {string} text: Si la respuesta es una lista de objetos este indica donde se encuentra el texto a utilizar dentro del option
     * @param {string} wrapper: Si la respuesta es una lista de objetos, indica la key donde se encuentra el listado de opciones
     *
     *      Los siguientes parametros son los por defecto de jQuery para la solicitud Ajax
     * @param {string} type Default POST
     * @param {string} url
     * @param {JSON} data
     * @param {string} dataType Default json
     *
     * @return {Promise} Promise del request ajax
     */
    fillSelectByAjax: function(options)
    {
        var request = null;
        var defaultOptions = {
            select: '',
            resetSelect: true,
            showLoadIndicator: true,
            containerIndicator: '',
            value: '',
            text: '',
            wrapper: '',
            type: 'POST',
            url: '',
            data: '',
            dataType: 'json',
        };

        options = $.extend(defaultOptions, options);
        var loadIndicator = $(config.spinner_input);

        // Se agrega el parametro _csrf, necesario para la solicitud Ajax
        if (typeof(options.data) == 'object') {
            options.data = $.extend({_csrf: $('meta[name="csrf-token"]').attr('content')}, options.data);
        } else if (typeof(options.data) == 'string') {
            options.data += '&_csrf='+$('meta[name="csrf-token"]').attr('content');
        } else {
            if (config.debug) {
                console.log('options.data invalid recived by helpers.fillSelectByAjax');
            }
        }

        request = $.ajax({
            type: options.type,
            url: options.url,
            data: options.data,
            dataType: options.dataType,
            beforeSend: function() {
                if (options.showLoadIndicator == true) {
                    if (options.containerIndicator) {
                        if (options.containerIndicator instanceof Object) {
                            options.containerIndicator.html(loadIndicator);
                        } else {
                            $(options.containerIndicator).html(loadIndicator);
                        }
                    } else {
                        $(options.select).after(loadIndicator);
                    }
                }
            }
        });

        request.done(function(data) {
            if (options.wrapper != '') {
                helper.fillSelect(options.select, data[options.wrapper], options.resetSelect, options.value, options.text);
            } else {
                helper.fillSelect(options.select, data, options.resetSelect, options.value, options.text);
            }
        });

        request.always(function() {
            if (options.showLoadIndicator == true) {
                if (options.containerIndicator) {
                    if (options.containerIndicator instanceof Object) {
                        options.containerIndicator.find(loadIndicator).remove();
                    } else {
                        $(options.containerIndicator).find(loadIndicator).remove();
                    }
                } else {
                    $(options.select).parent().find(loadIndicator).remove();
                }
            }
        });

        request.fail(function(xhr, status, error) {
            if (config.debug) {
                console.log('Error '+status+' by helper.fillSelectByAjax: '+error);
            }
        });

        // Se devuelve del Promise de la solicitud Ajax
        return request;
    },

    /**
     * Inicializa el colorpicker
     *
     * @param string selector .input-colorpicker
     */
    colorpickerInit: function (selector) {
        selector = selector || '.input-colorpicker';
        var $selector = typeof(selector) === 'string' ? $(selector) : selector;

        $selector.colorpicker();

        // Si el elemento tiene addon, le agregamos la funcionalidad de mostrar el
        // widget al activar el input y de ocultar al quitar el foco del input
        if ($selector.find('.input-group-addon').length !== 0) {
            $selector.find('input').focus(function() {
                $selector.colorpicker('show');
            });
            $selector.find('input').click(function() {
                $selector.colorpicker('show');
            });
            $selector.find('input').blur(function() {
                $selector.colorpicker('hide');
            });
        }
    },

    /**
     * Devuelve un color aleatorio en formato hexadecimal
     *
     * @return string Color hexadecimal
     */
    randomColor: function (init) {
        init = init || '#';
        var color = '';

        for (var i = 0; i < 6; i++) {
            color += parseInt(Math.random() * 16).toString(16);
        }

        return init + color;
    },

    /**
     * Estiliza el infoWindow en tiempo de ejecución
     */
    styleInfoWindowGMap: function () {
        var $container = $('.gm-style-iw'),
            $rectangulo = $container.prev().find('div:last'),
            $sombraRectangulo = $container.prev().children('div:nth-child(2)'),
            $triangulo = $rectangulo.prev(),
            //$sombraTriangulo = $container.prev().children('div:first'),
            bgColor = '#dd4b39';

        $rectangulo.css({
            backgroundColor: bgColor,
            borderRadius: 10,
        });

        $sombraRectangulo.css({
            borderRadius: 10,
        });

        $triangulo.find('div:first').find('div:first').css({
            backgroundColor: bgColor,
        });

        $triangulo.find('div:last').css({
            backgroundColor: bgColor,
        });
    },

    /**
     * Valida el formato de value como URL
     */
    validateURL: function (value){
      // URL validation from http://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
      var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
      var regex = new RegExp(expression);
      return value.match(regex);
    },

    /**
     * Valida el formato de la contraseña
     */
    validatePassword: function (value) {
        var longitudMinima      = 6;
        var mayusculas          = /[A-Z]/;
        var minusculas          = /[a-z]/;
        var numeros             = /[0-9]/;
        var caracteres_simbolos = /[áÁéÉíÍóÓúÚñÑüÜ\[\]\(\)\{\}\*\,\:\=\;\.\#\+\-\_\~\&\@\/\%\!\¡\¿\?\'\"]/;

        // - La longitud mínima debe ser de 6 caracteres
        if (value.length < longitudMinima) {
            return false;
        }

        // - Letras mayúsculas
        if (!mayusculas.test(value)) {
            return false;
        }

        // - Letras minúsculas
        if (!minusculas.test(value)) {
            return false;
        }

        // - Números
        if (!numeros.test(value)) {
            return false;
        }

        // - Números y/o símbolos {, ^, #, @, $, (, ), =, etc.
        // - Caracteres de puntuación (, ´ . - ! ?)
        if (!caracteres_simbolos.test(value)) {
            return false;
        }

        return true;
    },

    /**
     * Inicializa la validacion con jQuery Validation Plugin para los formularios con el atributo
     * data-validate="true"
     *
     */
    initValidate: function () {
        // Se agrega la función after y before para validar fechas de inicio y fin
        jQuery.validator.addMethod('after', function(value, element, params) {
            var start_date = moment($(params).val(), 'DD-MM-YYYY'),
                end_date   = moment(value, 'DD-MM-YYYY'),
                valid      = false;

            if (!start_date.isValid() || !end_date.isValid()) {
                return false;
            }

            valid = end_date.isAfter(start_date);

            if (valid) {
                // Elimina el mensaje de error
                $('#'+$(element).attr('id')+'-error').html('');
            }

            return valid;
        }, jQuery.validator.format('Fecha incorrecta, debe ser mayor.'));

        jQuery.validator.addMethod('before', function(value, element, params) {
            var end_date   = moment($(params).val(), 'DD-MM-YYYY'),
                start_date = moment(value, 'DD-MM-YYYY'),
                valid      = false;

            if (start_date.isValid() && end_date.isValid()) {
                valid = start_date.isBefore(end_date);

                if (valid) {
                    // Elimina el mensaje de error
                    $('#'+$(element).attr('id')+'-error').html('');
                }

                return valid;
            }

            return true;
        }, jQuery.validator.format('Fecha incorrecta, debe ser menor.'));

        // http://stackoverflow.com/a/13142475
        jQuery.validator.addMethod('urlvalidation', function(value, element, params) {
            return this.optional(element) || helper.validateURL(value);
        }, jQuery.validator.format('Por favor, escribe una URL válida.'));

        jQuery.validator.addMethod('passwordvalidation', function(value, element, params) {
            return helper.validatePassword(value);
        }, jQuery.validator.format('La contraseña debe contener letras en mayúsculas, minúsculas, números y símbolos o signos de puntuación.'));

        $('form[data-validate="true"]').each(function() {
            var validator = $(this).validate({
                errorElement: 'span',
                errorClass: 'help-block',
                ignore: '.ignore',
                /*submitHandler: function(form) {
                    form.submit();
                },*/
                highlight: function(element, errorClass) {
                    $(element)
                        .closest('.form-group')
                        .addClass('has-error')
                        .removeClass('has-success');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element)
                        .closest('.form-group')
                        // .addClass('has-success')
                        .removeClass('has-error');
                },
                /*invalidHandler: function(event, validator) {},*/
                success: function(label) {},
            });

            // validator.focusInvalid = function() {};
        });
    },

    /**
     * Crea dinamicamente el form, lo inserta al dom y lo envía
     *
     * @param  string action
     * @param  string method
     * @return void
     */
    dynamicSendForm: function (action, method) {
        var $form = $('<form style="display:none">');
        $form.attr('action', action);
        $form.attr('method', 'POST');
        $form.append('<input type="hidden" name="_method" value="'+method+'">');
        $form.append('<input type="hidden" name="_token" value="'+$('meta[name="csrf-token"]').attr('content')+'">');

        $('body').append($form);
        $form.trigger('submit');
    },

    fixHeightContent: function () {
        var window_height = $(document).height(),
            main_header = 0,
            main_footer = 0,
            padding = 0,
            new_height = 0;

        // gentelella
        if ($('body').hasClass('nav-md')) {
            main_header = $('.nav_menu').outerHeight();
            main_footer = $('footer').outerHeight();
            padding = 25;
        } else {
        // admin-lte
            main_header = $('.main-header').outerHeight();
            main_footer = $('.main-footer').outerHeight();
            padding = 15*4 + 7;
        }

        new_height = window_height-main_footer-main_header-padding;

        if ($('#main-content').outerHeight() < new_height) {
            $('#main-content').css({'min-height': (window_height-main_footer-main_header-padding)+'px'});
        }
    },

    /**
     * Ajuste decimal de un número.
     *
     * @param {String}  tipo  El tipo de ajuste.
     * @param {Number}  valor El numero.
     * @param {Integer} exp   El exponente (el logaritmo 10 del ajuste base).
     * @returns {Number} El valor ajustado.
     */
    decimalAdjust: function (type, value, exp) {
        // Si el exp no está definido o es cero...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // Si el valor no es un número o el exp no es un entero...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');

        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    },

    /**
     * Revisa si el mimeType proporcionado es tipo video
     *
     * @param  string mimeType
     * @return boolean
     */
    mimeTypeIsVideo: function (mimeType) {
        var pattern = new RegExp('video/');

        return pattern.test(mimeType);
    },

    /**
     * Revisa si el mimeType proporcionado es tipo image
     *
     * @param  string mimeType
     * @return boolean
     */
    mimeTypeIsImage: function (mimeType) {
        var pattern = new RegExp('image/');

        return pattern.test(mimeType);
    },

    formatNumber: function ($cantidad)
    {
        $formatted = parseInt($cantidad);

        if ($cantidad >= 1000000) {
            $formatted = Math.round($cantidad/1000000).formater() + 'M';
        } else if ($cantidad >= 1000) {
            $formatted = Math.round($cantidad/1000).formater() + 'K';
        } else {
            $formatted = ($cantidad*1).formater();
        }

        return $formatted;
    }
};

// http://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-money-in-javascript
Number.prototype.formater = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

Number.prototype.format = function(n, x, s, c) {
    var n = n | 2;
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Math/round
// Decimal round
if (!Math.round10) {
Math.round10 = function(value, exp) {
  return helper.decimalAdjust('round', value, exp);
};
}
// Decimal floor
if (!Math.floor10) {
Math.floor10 = function(value, exp) {
  return helper.decimalAdjust('floor', value, exp);
};
}
// Decimal ceil
if (!Math.ceil10) {
Math.ceil10 = function(value, exp) {
  return helper.decimalAdjust('ceil', value, exp);
};
}
