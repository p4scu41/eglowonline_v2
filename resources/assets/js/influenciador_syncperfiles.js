/* globals $, helper, config, urlSincronizarPerfil */
'use strict';

$(document).ready(function() {
    var timer     = null,
        segundos  = 0,
        $reloj    = $('#reloj'),
        $this     = null,
        cacheBtn  = null;

    function format_time(segundos)
    {
        var horas = Math.floor(segundos / (60*60)),
            minutos = Math.floor((segundos/60) % 60),//((segundos-(segundos%60))/60) - (horas*60),
            res_segundos = (segundos%60);

        return helper.padLeft(horas, 2)+':'+helper.padLeft(minutos, 2)+':'+helper.padLeft(res_segundos, 2);
    }

    function updateTimer()
    {
        segundos++;

        $reloj.html(format_time(segundos));
    }

    $('#btn-syncperfiles').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        $this    = $(this);
        cacheBtn = $this.html();

        $this.html(config.spinner);

        $('#info').html('<div class="alert alert-success">'+
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+
            'Sea paciente, este proceso puede tardar varios minutos.</div>'+
        '</div>');
        $reloj.html('00:00:00');
        timer = setInterval(updateTimer, 1000); // 1 segundo

        $('.row-influenciador').each(function(index, el) {
            var $this = $(this);

            $this.find('.fa-question')
                .removeClass('fa-question')
                .addClass('fa-spinner fa-spin');

            $.ajax({
                    url: urlSincronizarPerfil + '/' + $this.data('influenciador'),
                    type: 'GET',
                    dataType: 'json',
                })
                .done(function() {
                    $this.find('.fa-spinner')
                        .removeClass('fa-spinner fa-spin')
                        .addClass('fa-check');
                })
                .fail(function() {
                    $this.find('.fa-spinner')
                        .removeClass('fa-spinner fa-spin')
                        .addClass('fa-close');
                });
        });

        /*$.ajax({
            url: urlSincronizarJsons,
            type: 'GET',
            dataType: 'json',
        })
        .done(function() {
            swal({
                title: 'Sincronización exitosa.',
                type: 'success',
                confirmButtonColor: config.color_red,
                confirmButtonText: 'Aceptar',
            });
        })
        .fail(function() {
            errorAlert('Error al procesar los datos.');
        })
        .always(function() {
            clearTimeout(timer);
            segundos = 0;
            $('#info').html('');
            $this.html(cacheBtn);
        });*/
    });

    $(document).ajaxStop(function() {
        clearTimeout(timer);
        segundos = 0;
        $('#info').html('<div class="alert alert-info">'+
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+
            'Sincronización finalizada.</div>'+
        '</div>');
        $this.html(cacheBtn);
    });
});
