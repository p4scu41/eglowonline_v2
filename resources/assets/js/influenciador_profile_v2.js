/* globals $,echarts, config, urlInfluenciadoresIndex, twttr */
'use strict';
// https://blog.zingchart.com/2016/01/20/4-tips-better-charts-zingchart/
// https://blog.zingchart.com/2016/05/09/make-a-custom-tooltip/?q=make%20a%20custom%20tooltip%20in%20zingchart%20using%20functions
ZC.customFn = {};  //Global wrapper to ZingChart

	//@ http://jsfromhell.com/array/shuffle [v1.0]
	function shuffle(o){ //v1.0
			for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
			return o;
		}
	var colors = [
			['#C0C8DF', '#00BDC5'], ['#C0C8DF', '#0B0339'], ['#C0C8DF', '#00BDC5'], ['#C0C8DF', '#0B0339'], ['#C0C8DF', '#00BDC5']
	], circles = [

		  ];
    var theme  = {
    color: [
        '#26B99A', '#34495E', '#BDC3C7', '#3498DB',
        '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
    ],
    title: {
        itemGap: 8,
        textStyle: {
            fontWeight: 'normal',
            color: '#408829'
        }
    },

    dataRange: {
        color: ['#1f610a', '#97b58d']
    },

    toolbox: {
        color: ['#408829', '#408829', '#408829', '#408829']
    },

    tooltip: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        axisPointer: {
            type: 'line',
            lineStyle: {
                color: '#408829',
                type: 'dashed'
            },
            crossStyle: {
                color: '#408829'
            },
            shadowStyle: {
                color: 'rgba(200,200,200,0.3)'
            }
        }
    },

    dataZoom: {
        dataBackgroundColor: '#eee',
        fillerColor: 'rgba(64,136,41,0.2)',
        handleColor: '#408829'
    },
    grid: {
        borderWidth: 0
    },

    categoryAxis: {
        axisLine: {
            lineStyle: {
                color: '#408829'
            }
        },
        splitLine: {
            lineStyle: {
                color: ['#eee']
            }
        }
    },

    valueAxis: {
        axisLine: {
            lineStyle: {
                color: '#408829'
            }
        },
        splitArea: {
            show: true,
            areaStyle: {
                color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
            }
        },
        splitLine: {
            lineStyle: {
                color: ['#eee']
            }
        }
    },
    timeline: {
        lineStyle: {
            color: '#408829'
        },
        controlStyle: {
            normal: {color: '#408829'},
            emphasis: {color: '#408829'}
        }
    },

    k: {
        itemStyle: {
            normal: {
                color: '#68a54a',
                color0: '#a9cba2',
                lineStyle: {
                    width: 1,
                    color: '#408829',
                    color0: '#86b379'
                }
            }
        }
    },
    map: {
        itemStyle: {
            normal: {
                areaStyle: {
                    color: '#ddd'
                },
                label: {
                    textStyle: {
                        color: '#c12e34'
                    }
                }
            },
            emphasis: {
                areaStyle: {
                    color: '#99d2dd'
                },
                label: {
                    textStyle: {
                        color: '#c12e34'
                    }
                }
            }
        }
    },
    force: {
        itemStyle: {
            normal: {
                linkStyle: {
                    strokeColor: '#408829'
                }
            }
        }
    },
    chord: {
        padding: 4,
        itemStyle: {
            normal: {
                lineStyle: {
                    width: 1,
                    color: 'rgba(128, 128, 128, 0.5)'
                },
                chordStyle: {
                    lineStyle: {
                        width: 1,
                        color: 'rgba(128, 128, 128, 0.5)'
                    }
                }
            },
            emphasis: {
                lineStyle: {
                    width: 1,
                    color: 'rgba(128, 128, 128, 0.5)'
                },
                chordStyle: {
                    lineStyle: {
                        width: 1,
                        color: 'rgba(128, 128, 128, 0.5)'
                    }
                }
            }
        }
    },
    gauge: {
        startAngle: 225,
        endAngle: -45,
        axisLine: {
            show: true,
            lineStyle: {
                color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
                width: 8
            }
        },
        axisTick: {
            splitNumber: 10,
            length: 12,
            lineStyle: {
                color: 'auto'
            }
        },
        axisLabel: {
            textStyle: {
                color: 'auto'
            }
        },
        splitLine: {
            length: 18,
            lineStyle: {
                color: 'auto'
            }
        },
        pointer: {
            length: '90%',
            color: 'auto'
        },
        title: {
            textStyle: {
                color: '#333'
            }
        },
        detail: {
            textStyle: {
                color: 'auto'
            }
        }
    },
    textStyle: {
        fontFamily: 'Arial, Verdana, sans-serif'
    }
};

var echartCountryDistribution   = null;
var echartTopKeywords           = null;
var echartTopHashtags           = null;

$(document).ready(function(){


        var valor               = 1;
        var $chartSeguidores    = $('#chartSeguidores');
        var $chartSeguidoress   = $('#chartCategories');
        var seguidoresTexts     = $chartSeguidores.data('texts');
        var seguidoresValues    = $chartSeguidores.data('values');
        var $showComunidad      = $('#showComunidad');
        var densityCanvas       = document.getElementById("redsocial2");
        var color               = ['#92D5F7', '#6E95E4', '#FCAA56', '#FF7F93'];

        //var imagen = document.getElementById("img-menu-influencer");
       // imagen.src = $chartSeguidores.data('icons')[1];


        Chart.defaults.global.defaultFontFamily = '"Helvetica Neue", Roboto, Arial, "Droid Sans", sans-serif';
        Chart.defaults.global.defaultFontSize   = 12;

        //imagenes de las redes sociales
        for (var i = 1; i <= $chartSeguidores.data('values').length; i++) {
          //  if($chartSeguidores.data('values')[i-1] != 0){
                var midiv = document.createElement("div");
		        midiv.setAttribute("class","border-icon-social");
		        midiv.innerHTML = '<img class="img-responsive" src="' + $chartSeguidores.data('icons')[i-1] + '" width="35" height="32">';
                document.getElementById('divprueba').appendChild(midiv);
            //}
        }

        //se crean 5 circulos de top de marcas
    	for (var i = 1; i <= 5; i++) {
    			var child = document.getElementById('circles-' + i),
    				percentage = 10 + (i * 2);

            if (child) {
    			circles.push(Circles.create({
    				id:         child.id,
    				value:		$('#canvass').data('values')[i-1],
    				radius:     50,
    				width:      8,
    				colors:     colors[i - 1]
    			}));
            }
    		}
        //configuracion de los circulos para el responsive
        Chart.pluginService.register({
		beforeDraw: function (chart) {
			if (chart.config.options.elements.center) {
        //Get ctx from string
        var ctx = chart.chart.ctx;

				//Get options from the center object in options
        var centerConfig = chart.config.options.elements.center;
      	var fontStyle = centerConfig.fontStyle || 'Arial';
				var txt = centerConfig.text;
        var color = centerConfig.color || '#000';
        var sidePadding = centerConfig.sidePadding || 20;
        var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
        //Start with a base font of 30px
        ctx.font = "50px " + fontStyle;

				//Get the width of the string and also the width of the element minus 10 to give it 5px side padding
        var stringWidth = ctx.measureText(txt).width;
        var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

        // Find out how much the font can grow in width.
        var widthRatio = elementWidth / stringWidth;
        var newFontSize = Math.floor(30 * widthRatio);
        var elementHeight = (chart.innerRadius * 2);

        // Pick a new font size so it will not be larger than the height of label.
        var fontSizeToUse = Math.min(newFontSize, elementHeight);

				//Set font settings to draw it correctly.
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
        var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
        ctx.font = fontSizeToUse+"px " + fontStyle;
        ctx.fillStyle = color;

        //Draw text in center
        ctx.fillText(txt, centerX, centerY);
			}
		}
	});

        //Grafica de categorias de seguidores
		var piechart = new Chart(document.getElementById("mycanvas"), {
		    type: 'pie',
		    data: {
	    	    display: true,
		        labels: $chartSeguidoress.data('texts'),
		        datasets: [{
		            label: $chartSeguidoress.data('texts'),
		            backgroundColor: $chartSeguidoress.data('colors'),
		            data: $chartSeguidoress.data('values')
		        }]
		    },
		    options: {
		        responsive: true,
	            title: {
		            display: false,
		            text: $chartSeguidoress.data('texts')
		        },
		        tooltips: {
                    enabled: true,
                    mode: 'single',
                    callbacks: {
                        label: function(tooltipItems, data) {
                            let dataset     = data.datasets[tooltipItems.datasetIndex],
                                total       = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                }),
                                currentValue = dataset.data[tooltipItems.index],
                                currentLabel = dataset.label[tooltipItems.index],
                                precentage   = Math.floor(((currentValue/total) * 100)+0.5),
                                prefix       = '';

                                switch (currentLabel) {
                                    case 'Inactivo':
                                        prefix = '= 0: ';
                                        break;

                                    case 'Newbie':
                                        prefix = '1 - 10K: ';
                                        break;

                                    case 'Amateur':
                                        prefix = '> 10K - 100K: ';
                                        break;

                                    case 'Pro':
                                        prefix = '> 100K - 1M: ';
                                        break;

                                    case 'Expert':
                                        prefix = '> 1M - 5M: ';
                                        break;

                                    case 'Star':
                                        prefix = '> 5M: ';
                                        break;
                                }

                          return prefix + currentValue + ' (' + precentage + '%)';
                        }
                    }
                },
                legend: {
                    display: true,
                    position: 'right',
                    labels:{
                        fontSize:12
                    }
                }
		    }
		});
    buildCountryDistributionGraphics(seguidoresTexts, seguidoresValues, color);
    graficasHastagKeyWord();


    //activa las graficas hashtag y palabra clave de comunidad
    $('#showComunidad').on('shown.bs.tab', function (e) {
        graficasHastagKeyWordCommunity();


    });

    function buildCountryDistributionGraphics(countryTexts, countryValues, colors) {

        let newValues = [];

        countryValues.reverse();
        colors.reverse();
        // console.log(countryValues);
        for (let i = 0; i < countryValues.length; i++) {
            newValues.push({
                value: countryValues[i],
                itemStyle: {
                    normal: {
                        color: colors[i],
                        barBorderRadius: [5, 5, 0, 0],
                    },
                    label: {
                        textStyle:{
                            fontSize: 11
                        }
                    }

                }
            })
        }

        let countryDistributionsData  = {
            tooltip: {
                trigger: "axis",

                textStyle:{
                    fontSize: 11
                }
            },
            grid: {
                left: '0%',
                right: '12%',
                bottom: '0%',
                containLabel: true
            },
            xAxis: {
                show: false,
            },
            yAxis: {
                show: false,
                type: "category",
                data: [""],
                splitLine: {
                    show: false
                }
            },
            series: [{
                name: "Red Social",
                type: "bar",
                data: newValues,
                label: {
                    normal: {
                        show: true,
                        position: 'right',
                        formatter: function(params){
                                if(params.data.value != 0){
                                    return helper.formatNumber(params.data.value);
                                }else{
                                    return "";
                                }

                        }
                    }
                },
            }]
    };

    echartCountryDistribution = echarts.init(document.getElementById('chartSeguidores'));
    echartCountryDistribution.setOption(countryDistributionsData);
}

        //configracion de pie de las redes sociales
         //grafica pie de redes sociales
         /*
        var densityData = {
             display:true,
             label: '',
             data: $chartSeguidores.data('values'),
             backgroundColor: ["#92D5F7", "#6E95E4","#FCAA56", "#FF7F93" ],
             hoverBackgroundColor: ["#66A2EB", "#FCCE56"]
        };
        var value       = $chartSeguidores.data('values');

        var barChart    = new Chart(densityCanvas, {
                type: 'horizontalBar',
                data: {
                  display: true,
                  labels: $chartSeguidores.data('texts'),
                  datasets: [densityData]
                },
                options: {
                    legend: {
                        display: false
                    },
                    tooltips: {
                        enabled: true
                    },
                    hover: {
                        mode: null
                    },
                    scales: {
                        xAxes: [{
                            stacked: false,
                            display: false,
                            ticks: {
                                beginAtZero: true,
                                callback: function(value) {if (value % 1 === 0) {return value;}}
                            },
                            gridLines: {
                                drawBorder: true,
                                offsetGridLines: true
                            },
                        }],
                    yAxes: [{
                        stacked: false,
                        display: false,
                        barPercentage: 0.9,
                        ticks: {
                            beginAtZero: true,
                             callback: function(value) {if (value % 1 === 0) {return value;}}

                        },
                        gridLines: {
                            drawBorder: true,
                            offsetGridLines: true
                        },
                    }]
                    }
                }
    });*/

    /*var avatar_width = $('#avatar-influencer').get(0).width;
    console.log(avatar_width);
    var avatar_height = $('#avatar-influencer').get(0).height;
    console.log(avatar_height);

    if (avatar_width != avatar_height) {
        var crop_size = (avatar_width > avatar_height) ? avatar_height : avatar_width;
        console.log(crop_size);

        var rx = 100 / 200; // coords.w;
    	var ry = 100 / 200; //coords.h;
    	var x = (avatar_width - crop_size) / 2;
    	var y = (avatar_height - crop_size) / 2;

    	$('#avatar-influencer').css({
    		width: '200px',
    		height: '200px',
    		marginLeft: '-' + Math.round(rx * x) + 'px',
    		marginTop: '-' + Math.round(ry * y) + 'px'
    	});
    }*/
});

$(window).resize(function () {
    if (echartTopKeywords != null) {
       echartTopKeywords.resize();
    }

    if (echartTopHashtags != null) {
       echartTopHashtags.resize();
    }
});

    function graficasHastagKeyWord(){
         //grafias de palabras claves y hashtags
        if ($('#container-div-top-palabras').length != 0) {
            var jsonTopPalabras = $('#container-div-top-palabras').data('json');

            if (jsonTopPalabras.data.length != 0) {
                crearKeyWord('container-div-top-palabras', jsonTopPalabras);
            }
        }

        if ($('#container-div-top-hashtags').length != 0) {
            var jsonTopHashtags = $('#container-div-top-hashtags').data('json');

            if (jsonTopHashtags.data.length != 0) {
                crearHashtang('container-div-top-hashtags', jsonTopHashtags);
            }
        }
    }

    //Funcion que crea las graficas hashtags y palabra clave
    function crearHashtang(div, datos) {
        echartTopHashtags = echarts.init(document.getElementById(div), theme);
        // window.onresize = echartTopHashtags.resize;
          var options = {

            title: {
              text: 'Frecuencia Escritura',
              bottom: '3%',
              right: 'center',
              textStyle: {color: '#333', fontSize: 14}
              //subtext: 'Subtitle'
            },
            tooltip: {
                show:true,
              trigger: 'axis',
            },
            legend: {
              x: 220,
              y: 50,
              data: ''
            },
            toolbox: {
              show: false,
              feature: {
                saveAsImage: {
                  show: true,
                  title: "Save Image"
                }
              }
            },
            calculable: true,
            grid:{x2:150,x:10},
            yAxis: [{
              type: 'category',
              boundaryGap: false,
              data: datos.etiquetas,
              position:'right',
              nameTextStyle: {
                  color: '#f57c00'
              },
              axisLine:{show: false ,onZero:true },
              splitLine:{
                        onGap:false,
                        lineStyle:{
                            color: ['#D8D8D8'],
                            width: 23,
                            type: 'solid'}

                        },
              splitArea:{show: false},
              axisLabel:{
                  formatter:'{value}',
                  margin:13,

                      textStyle: {
                          color: '#0B0339',
                      }
              }
            }],
            xAxis: [{
              type: 'value',
              axisLine:{show:false}
            }],
            series: {
                name   : 'Menciones/Semana',
                type   : 'line',
                smooth : true,
                symbol : 'circles',
                step: false,
                itemStyle : {
                    normal : {
                        color: '#00BDC5',
                        borderColor: '#0B0339',
                        shadowColor: 'rgba(0, 0, 0, 0.5)',
                        shadowBlur: 10

                    }
                },
                 lineStyle : {
                    normal : {
                        color: 'rgba(138,221,45,0)',


                    }
                },

                data:  datos.data,


            }
          };

        echartTopHashtags.setOption(options);

        return echartTopHashtags;
    }
    function crearKeyWord(div, datos) {
        echartTopKeywords = echarts.init(document.getElementById(div), theme);
        // window.onresize = echartTopKeywords.resize;
          var options = {

            title: {
              text: 'Frecuencia Escritura',
              bottom: '3%',
              right: 'center',
              textStyle: {color: '#333', fontSize: 14}
              //subtext: 'Subtitle'
            },
            tooltip: {
                show: true,
              trigger: 'axis',
            },
            legend: {
              x: 220,
              y: 50,
              data: ''
            },
            toolbox: {
              show: false,
              feature: {
                saveAsImage: {
                  show: true,
                  title: "Save Image"
                }
              }
            },
            calculable: true,
            grid:{x2:150,x:10},
            yAxis: [{
              type: 'category',
              boundaryGap: false,
              data: datos.etiquetas,
              position:'right',
              nameTextStyle: {
                  color: '#f57c00'
              },
              axisLine:{show: false ,onZero:true },
              splitLine:{
                        onGap:false,
                        lineStyle:{
                            color: ['#D8D8D8'],
                            width: 23,
                            type: 'solid'}

                        },
              splitArea:{show: false},
              axisLabel:{
                  formatter:'{value}',
                  margin:13,

                      textStyle: {
                          color: '#0B0339',
                      }
              }
            }],
            xAxis: [{
              type: 'value',
              axisLine:{show:false}
            }],
            series: {
                name   : 'Menciones/Semana',
                type   : 'line',
                smooth : true,
                symbol : 'circles',
                step: false,
                itemStyle : {
                    normal : {
                        color: '#00BDC5',
                        borderColor: '#0B0339',
                        shadowColor: 'rgba(0, 0, 0, 0.5)',
                        shadowBlur: 10

                    }
                },
                 lineStyle : {
                    normal : {
                        color: 'rgba(138,221,45,0)',


                    }
                },

                data:  datos.data,


            }
          };

        echartTopKeywords.setOption(options);

        return echartTopKeywords;
    }

window.resize = function () {
    echartTopKeywords.resize();
    echartTopHashtags.resize();
};
