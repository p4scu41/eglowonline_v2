/* globals $, config, moment */
'use strict';

$(document).ready(function() {
    $('a.fancybox').each(function(){
        type = $(this).attr('type');
        config = {};

        if (type == '') {
            type = 'image';
        }

        if (type == 'video') {
            config = {
                type : 'iframe',
                href : $(this).attr('href'),
                title: 'Video'
            };
        } else if (type == 'image') {
            config = {
                type : 'image'
            };
        }

        $(this).fancybox(config);
    });

    $('#menu-estadisticas a').click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        $('.content-tab').hide();
        $('#menu-estadisticas a').removeClass('btn-red-rounded');
        $(this).addClass('btn-red-rounded');

        $($(this).attr('href')).show();
    });
});
