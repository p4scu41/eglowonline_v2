/* globals $, app_url, twttr, moment, Handlebars, config, urlInfluenciadorSearchByText, swal, urlGetImagenProducto, urlGetLogotipoMarca, urlCampanaIndex, urlSearchLugarNacimientoByText, urlInfluenciadorAdvancedSearch, errorAlert, $, urlPalabraClaveSearchByText, urlGetLogotipoAgencia */
'use strict';

$(document).ready(function() {
    var $campanaForm                     = $('#form-campana'),
        influenciadorItemResultSearchHbs = Handlebars.compile($('#influenciador-item-result-search-hbs').html()),
        influenciadorItemSelectedHbs     = Handlebars.compile($('#influenciador-item-selected-hbs').html()),
        palabraClaveItemHbs              = Handlebars.compile($('#palabra-clave-item-hbs').html()),
        hashtagItemHbs                   = Handlebars.compile($('#hashtag-item-hbs').html()),
        influenciadores_to_delete        = [],
        hasAdvancedSearch                = $('#hasAdvancedSearch').val(),
        pageForPagination                = $('#pageForPagination').val(),
        origin                           = $('#origin').val();

    $('#cantidad_seguidores').ionRangeSlider({
        keyboard : true,
        min      : 0,
        max      : 10000000,
        type     : 'double',
        step     : 1000,
        grid     : true,
        // postfix  : ' K',
    });

    $('#distribucion_genero').ionRangeSlider({
        keyboard : true,
        from     : 5,
        type     : 'single',
        values   : [
            'Solo Mujeres',
            '+40% Mujeres',
            '+30% Mujeres',
            '+20% Mujeres',
            '+10% Mujeres',
            'Mujeres y Hombres',
            '+10% Hombres',
            '+20% Hombres',
            '+30% Hombres',
            '+40% Hombres',
            'Solo Hombres'
        ]
    });

    $('#presupuesto').ionRangeSlider({
        keyboard : true,
        min      : 1000,
        max      : 5000000,
        type     : 'single',
        step     : 1000,
        grid     : true,
        postfix  : ' $',
    });

    $('#industria_id').selectToList({});
    $('#tipo_campana_id').selectToList({});
    $('#tipo_influenciador_id').selectToList({});
    $('#genero').selectToList({});
    $('#rango_etario_id').selectToList({});
    $('#activo').selectToList({});

    // setear imagen de industria
    $('#industria_id').on('change', function () {
        $('.campana_industria').html($('#industria_id').find('option:selected').text());
        $('#industria').attr('src', $(this).find('option:selected').data('imagen'));
    });

    // setear imagen de tipo de campaña
    $('#tipo_campana_id').on('change', function () {
        $('.campana_tipo').html( $('#tipo_campana_id').find('option:selected').text());
        $('#tipoCampana').attr('src', $(this).find('option:selected').data('imagen'));
    });

    function gatherDataCampana() {
        var datos = $campanaForm.serializeObject();

        datos.influenciadores_to_delete = influenciadores_to_delete;

        datos.influenciadores = $('#list-influenciadores-seleccionados .item-influenciador-seleccionado')
            .map(function(index, elem) {
                return $(elem).data('id');
            })
            .get();

        datos.hashtags = $('#hashtag-list .hashtag-item').map(function() {
                return $(this).text();
            }).get().join(' ');

        return datos;
    }

    function sendDataCampana($btn) {
        var cacheBtn = $btn.html();

        $btn.html(config.spinner);

        $.ajax({
            url: $campanaForm.attr('action'),
            type: $campanaForm.attr('method'),
            dataType: 'json',
            data: gatherDataCampana(),
        })
        .done(function() {
            swal({
                    title: 'Datos guardados exitosamente.',
                    type: 'success',
                    confirmButtonColor: config.color_red,
                    confirmButtonText: 'Aceptar',
                },
                function() {
                    location.href = urlCampanaIndex;
                });
        })
        .fail(function(jqXHR) {
            errorAlert('Error al procesar los datos. ' + jqXHR.responseJSON.message);
        })
        .always(function() {
            $btn.html(cacheBtn);
        });
    }

    function validateForm($form) {
        $('#hashtag-error').closest('.form-group').removeClass('has-error');

        if ($('#terminos')[0].files[0] != null) {
            let terminosSize = $('#terminos')[0].files[0].size;

            if (terminosSize > 2097152) {
                errorAlert('El peso del archivo de términos y condiciones debe ser menor a 2MB');
                return false;
            }
        }

        if ( !$form.valid() ) {
            //errorAlert('Revise sus datos, algo esta mal.');
            alertEglow.warning({message: 'Revise sus datos, algo esta mal.'});
            return false;
        }

        if ($('#hashtag-list .hashtag-item').length === 0) {
            $('#hashtag-error').show()
                .html('Debe colocar por lo menos un hashtag.')
                .closest('.form-group').addClass('has-error');

            errorAlert('Revise sus datos, algo esta mal.');

            return false;
        }

        return true;
    }

    $campanaForm.validate({
        debug: true,
        errorElement: 'span',
        errorClass: 'help-block',
        ignore: '.ignore',
        highlight: function(element) {
            $(element)
                .closest('.form-group')
                .addClass('has-error')
                .removeClass('has-success');
        },
        unhighlight: function(element) {
            $(element)
                .closest('.form-group')
                .removeClass('has-error');
        },
        /*submitHandler: function(form) {
            if (validateForm($(form))) {
                sendDataCampana($('.btn-save-campana'));
            }
        },*/
        invalidHandler: function(event, validator) {
            if (validator.errorList.length > 0) {
                $('a[href="#'+$(validator.errorList[0].element).closest('.tab-pane').attr('id')+'"]').trigger('click');
                $('body').scrollTop( $(validator.errorList[0].element).scrollTop() );
            }
        },
        success: function(label) {}
    });

    function gatherDataAdvancedSearch() {
        var data = {},
            cantidad_seguidores = $('#cantidad_seguidores').data('ionRangeSlider'),
            distribucion_genero = $('#distribucion_genero').data('ionRangeSlider');

        data.nombre                = $('#celebridad-name-search').val();
        data.genero                = $('#genero').val();
        // data.lugar_nacimiento      = $('#lugar_nacimiento').val();
        data.rango_etario_id       = $('#rango_etario_id').val();
        data.tipo_influenciador_id = $('#tipo_influenciador_id').val();
        data.manager_nombre        = $('#manager_nombre').val();
        data.cantidad_seguidores   = null;
        data.distribucion_genero   = null;
        data.pais_id               = $('#pais_id').val();
        data.palabra_clave_id      = $('#palabra-clave-list li')
            .map(function(index, elem) {
                return $(elem).data('id');
            })
            .get();

        // Solamente si cambio el slider se obtienen los valores y no hay advanced search
        // caso contrario, realiza
        if (hasAdvancedSearch === '1') {
            if (origin === '2') {
                data.cantidad_seguidores = [
                    cantidad_seguidores.result.from,
                    cantidad_seguidores.result.to
                ];
            }

        } else {
            if (
                cantidad_seguidores.options.from !== cantidad_seguidores.result.from ||
                cantidad_seguidores.options.to !== cantidad_seguidores.result.to
            ) {
                data.cantidad_seguidores = [
                    cantidad_seguidores.result.from,
                    cantidad_seguidores.result.to
                ];
            }
        }

        // distribución de genero
        if (hasAdvancedSearch === '1') {
            if (origin === '2') {
                data.distribucion_genero = [
                    distribucion_genero.result.from,
                    distribucion_genero.result.to
                ];
            }
        } else {
            if (
                distribucion_genero.options.from !== distribucion_genero.result.from ||
                distribucion_genero.options.to !== distribucion_genero.result.to
            ) {
                data.distribucion_genero = [
                    distribucion_genero.result.from,
                    distribucion_genero.result.to
                ];
            }
        }

        return data;
    }

    // Evita que el usuario pueda clickear sobre la pestaña, obliga a que presione el boton siguiente
    $('a[data-toggle="tab"]').click(function(event) {
        if ($(this).closest('li').hasClass('disabled')) {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();

            return false;
        }
    });

    $('#hashtag-list, #palabra-clave-list').on('click', '.btn-delete', function(event) {
        event.stopPropagation();
        event.preventDefault();

        $(this).closest('li').fadeOut('slow', function() {
            $(this).remove();
        });
    });

    $('#btn-add-hastag').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        $('#hashtag-error').html('');
        $(this).closest('.form-group').removeClass('has-error');

        // Antes de agregar se valida que el hashtag sea correcto
        // se utiliza la librería twitter-text-js
        // https://github.com/twitter/twitter-text/tree/master/js
        if (!twttr.txt.isValidHashtag($('#hashtag').val())) {
            $(this).closest('.form-group').addClass('has-error');
            $('#hashtag-error').html('El formato del hashtag es incorrecto, debe iniciar con #, seguido de letras o números, no se aceptan carácteres como . , - @ o espacios en blanco.');
            return false;
        }

        if ($('#hashtag-list li').length > 0) {
            errorAlert('Solo tiene permitido agregar un hashtag');
            $('#hashtag').val('');
            return false;
        }

        $('#hashtag-list').append(hashtagItemHbs({hashtag: $('#hashtag').val()}));

        $('#hashtag').val('');
    });

    $('#btn-next-campana').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        if (!validateForm($campanaForm)) {
            return false;
        }

        var info_campana = $campanaForm.serializeObject(),
            fecha_desde  = moment(info_campana.fecha_desde, 'DD-MM-YYYY'),
            fecha_hasta  = moment(info_campana.fecha_hasta, 'DD-MM-YYYY');

        $('.campana_nombre').html(info_campana.nombre);
        $('.campana_presupuesto').html('Presupuesto $ ' + Number($('#presupuesto').data('ionRangeSlider').result.from).format(0) + ' MXN');
        $('.campana_industria').html($('#industria_id').find('option:selected').text());
        $('#industria').attr('src', $('#industria_id').find('option:selected').data('imagen'));
        $('.campana_tipo').html( $('#tipo_campana_id').find('option:selected').text() );
        $('#tipoCampana').attr('src', $('#tipo_campana_id').find('option:selected').data('imagen'));
        $('.campana_fechas').html(
            fecha_desde.format('DD MMMM YYYY') + ' - ' +
            fecha_hasta.format('DD MMMM YYYY')
        );
        $('.campana_hashtags').html(
            $('#hashtag-list .hashtag-item').map(function() {
                return $(this).text();
            }).get().join(' ')
        );

        // Habilita el tab para el clickeo del usuario
        $('#influencer-tab').closest('li').removeClass('disabled');
        $('#influencer-tab').trigger('click');

        $('html, body').animate({
            scrollTop: $("#influencer-tab").offset().top
        }, 1500);
    });

    $('#agencia_id').change(function(event) {
        if ($(this).val() != '') {
            $.getJSON(urlGetLogotipoAgencia, {id: $(this).val()}, function(data) {
                $('#img-campana').attr('src', data.logotipo);
            });
        }
    });

    $('#marca_id').change(function(event) {
        if ($(this).val() != '') {
            $.getJSON(urlGetLogotipoMarca, {id: $(this).val()}, function(data) {
                $('#img-campana').attr('src', data.logotipo);
            });
        }
    });

    $('#producto_id').change(function(event) {
        if ($(this).val() != '') {
            $.getJSON(urlGetImagenProducto, {id: $(this).val()}, function(data) {
                $('#img-campana').attr('src', data.imagen);
            });
        }
    });

    $('#btn-toogle-busqueda-avanzada').click(function(event) {

        $('#celebridad-name-search').val(' ');
       // $('#celebridad-name-search').attr("placeholder", "Type here to search");
        event.stopPropagation();
        event.preventDefault();

        var $containerAvanzado = $('#busqueda-avanzada');

        if ($containerAvanzado.hasClass('off')) {
            $containerAvanzado.slideDown('slow');
            $containerAvanzado.removeClass('off');
            $(this).parent().find('.btn-encuentra-influenciador').hide('slow');
        } else {
            $containerAvanzado.slideUp('slow');
            $containerAvanzado.addClass('off');
            $(this).parent().find('.btn-encuentra-influenciador').show('slow');
        }
    });

    $('#btn-next-influenciadores-seleccionados').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        $('#panorama_influencers').html('');

        $('#list-influenciadores-seleccionados .item-influenciador-seleccionado').each(function(index, el) {
            $('#panorama_influencers').append('<img src="'+$(el).find('img').attr('src')+'" '+
                'class="avatar" data-toggle="tooltip" title="'+$(el).find('.nombre_influencer').text()+'">');
        });

        $('#panorama_influencers [data-toggle="tooltip"]').tooltip();

        // Habilita el tab para el clickeo del usuario
        $('#panorama-tab').closest('li').removeClass('disabled');
        $('#panorama-tab').trigger('click');

        $('html, body').animate({
            scrollTop: $("#panorama-tab").offset().top
        }, 1500);
    });

    // Para detectar enter al capturar el hashtag
    $('#hashtag').keyup(function(event) {
        var keyCode = event.keyCode || event.which;

        if (keyCode == 13) {
            $('#btn-add-hastag').trigger('click');
        }
    });

    // Para detectar enter al capturar el nombre del influencer en la busqueda básica
    $('#celebridad-name-search').keyup(function(event) {
        var keyCode = event.keyCode || event.which;

        if (keyCode == 13) {
            $('#btn-basic-search').trigger('click');
        }
    });

    $('.pagination').on('click', 'a', function(event) {
        event.preventDefault();
        event.stopPropagation();

        var $this = $(this);
        var $containerAvanzado = $('#busqueda-avanzada');
        var url = $containerAvanzado.hasClass('off') ? urlInfluenciadorSearchByText : urlInfluenciadorAdvancedSearch;
        var params =  $containerAvanzado.hasClass('off') ? {q: $('#celebridad-name-search').val()} : gatherDataAdvancedSearch();

        sendSearch($this, url, params, $this.attr('href'));
    });

    function buildPagination(current_page, last_page) {
        var $pagination = $('.pagination');
        var pages = '';
        var first = '';
        var last = '';
        var item_pages = 4;
        var i = 0;


        // First
        if ((current_page != last_page && current_page > (item_pages-1)) || current_page == last_page) {
            first = '<li><a href="1" rel="prev">« </a> </li> ';
        }

        // Prev
        if (current_page > 1) {

            for (i=current_page-1; i>current_page-item_pages && i>0; i--) {
                pages = ' <li><a href="'+(i)+'">O<br>'+(i)+'</a></li>' + pages;
            }
        }

        pages += '<li class="active"><span>O<br>'+current_page+'</span></li>';

        // Next
        if (current_page < last_page) {
            for (i=current_page+1; i<current_page+item_pages && i<=last_page && (i!=current_page); i++) {
                pages += '<li><a href="'+(i)+'">O<br>'+(i)+'</a></li>';
            }
        }

        if (last_page < item_pages) {
            for (i=current_page+1; i<=last_page && (i!=last_page); i++) {
                pages += '<li><a href="'+(i)+'">O<br>'+(i)+'</a></li>';
            }
        }

        // Last
        if (current_page < last_page-item_pages && (current_page != last_page)) {
            last = '<li><a href="'+(last_page)+'" rel="next">»</a></li>';
        }

        pages = first +'<li><a>eGL</a></li>'+ pages +'<li><a>W</a></li>'+last;

        $pagination.html(pages);
        $pagination.show();
    }

    function sendSearch($btn, url, params, page) {
        var $this = $btn,
            cacheBtn = $this.html();

        $this.html(config.spinner);

        params.page = page;

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            data: params,
        })
        .done(function(response) {
            var data = response.data;

            $('#count-result-search').html(response.total);
            $('#list-influenciadores-resultado-busqueda').html('');

            if (data.length) {
                for (var i = 0; i < data.length; i++) {
                     // alcance potencial de twitter
                    data[i].alcance_potencial = helper.formatNumber(data[i].alcance_potencial);

                    if (data[i].twitter_profile_image_url === null) {
                        data[i].twitter_profile_image_url = app_url + '/img/user_icon.png';
                    } else {
                        data[i].twitter_profile_image_url = data[i].twitter_profile_image_url.replace('_normal.','.');
                    }

                    data[i].qty_followers = helper.formatNumber(data[i].qty_followers);

                    if (data[i].twitter_cantidad_seguidores) {
                        data[i].twitter_cantidad_seguidores = helper.formatNumber(data[i].twitter_cantidad_seguidores);
                    } else {
                        data[i].twitter_cantidad_seguidores = '0';
                    }

                    // number format
                    if (data[i].twitter_precio_publicacion == 0) {
                        data[i].twitter_precio_publicacion = 'NA';
                    } else {
                        data[i].twitter_precio_publicacion = '$' + helper.formatNumber(data[i].twitter_precio_publicacion);
                    }

                    if (data[i].facebook_cantidad_seguidores) {
                        data[i].facebook_cantidad_seguidores = helper.formatNumber(data[i].facebook_cantidad_seguidores);
                    } else {
                        data[i].facebook_cantidad_seguidores = '0';
                    }

                    if (data[i].facebook_precio_publicacion == 0) {
                        data[i].facebook_precio_publicacion = 'NA';
                    } else {
                        data[i].facebook_precio_publicacion = '$' + helper.formatNumber(data[i].facebook_precio_publicacion);
                    }

                    if (data[i].instagram_cantidad_seguidores) {
                        data[i].instagram_cantidad_seguidores = helper.formatNumber(data[i].instagram_cantidad_seguidores);
                    } else {
                        data[i].instagram_cantidad_seguidores = '0';
                    }

                    if (data[i].instagram_precio_publicacion == 0) {
                        data[i].instagram_precio_publicacion = 'NA';
                    } else {
                        data[i].instagram_precio_publicacion = '$' + helper.formatNumber(data[i].instagram_precio_publicacion);
                    }

                    if (data[i].youtube_cantidad_seguidores) {
                        data[i].youtube_cantidad_seguidores = helper.formatNumber(data[i].youtube_cantidad_seguidores);
                    } else {
                        data[i].youtube_cantidad_seguidores = '0';
                    }

                    if (data[i].youtube_precio_publicacion == 0) {
                        data[i].youtube_precio_publicacion = 'NA';
                    } else {
                        data[i].youtube_precio_publicacion = '$' + helper.formatNumber(data[i].youtube_precio_publicacion);
                    }

                    if (data[i].snapchat_cantidad_seguidores) {
                        data[i].snapchat_cantidad_seguidores = helper.formatNumber(data[i].snapchat_cantidad_seguidores);
                    } else {
                        data[i].snapchat_cantidad_seguidores = '0';
                    }

                    if (data[i].snapchat_precio_publicacion == 0) {
                        data[i].snapchat_precio_publicacion = 'NA';
                    } else {
                        data[i].snapchat_precio_publicacion = '$' + helper.formatNumber(data[i].snapchat_precio_publicacion);
                    }

                    data[i].engagement_rate = data[i].engagement_rate.toFixed(2);

                    /*if (data[i].qty_followers) {
                        data[i].qty_followers = Math.round10(data[i].qty_followers/1000, 1);
                    }

                    if (data[i].engagement_ambos) {
                        data[i].engagement_ambos = Math.round10(data[i].engagement_ambos/1000, 1);
                    }*/

                    // Variable para pasar los datos al widget de seleccionado
                    data[i].json = JSON.stringify(data[i]);

                    $('#list-influenciadores-resultado-busqueda')
                        .append(influenciadorItemResultSearchHbs({influenciador: data[i]}));


                    $('#list-influenciadores-resultado-busqueda').find('i.fa').tooltip();
                    $('#list-influenciadores-resultado-busqueda').find('span.red-box').tooltip();
                    $('#list-influenciadores-resultado-busqueda').find('[data-toggle="tooltip"]').tooltip();
                }

                buildPagination(response.current_page, response.last_page);
            } else {
                $('.pagination').hide();
            }
        })
        .fail(function(jqXHR) {
            if (jqXHR.responseJSON.length != 0) {
                errorAlert('Error al procesar los datos. ' + jqXHR.responseJSON.message);
            } else {
                errorAlert('Error al procesar los datos. ');
            }
        })
        .always(function() {
            $this.html(cacheBtn);
            // $('#celebridad-name-search').val('');
            $('html, body').animate({
                scrollTop: $("#count-result-search").offset().top
            }, 1500);
        });
    }

    $('#btn-basic-search').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        sendSearch($(this), urlInfluenciadorSearchByText, {q: $('#celebridad-name-search').val()}, 1);
    });

    $('#btn-advanced-search').click(function(event) {
        event.stopPropagation();
        event.preventDefault();

        sendSearch($(this), urlInfluenciadorAdvancedSearch, gatherDataAdvancedSearch(), 1);
    });

    $('#list-influenciadores-seleccionados').on('click', '.btn-delete', function(event) {
        event.stopPropagation();
        event.preventDefault();

        $(this).closest('.item-influenciador-seleccionado').fadeOut('slow', function() {
            influenciadores_to_delete.push($(this).data('id'));
            $('.avatar[data-id="'+$(this).data('id')+'"]').remove();
            $(this).remove();
        });
    });

    $('#list-influenciadores-resultado-busqueda').on('click', '.link-perfil-influencer', function(event) {
        var id = $(this).attr('href').split('/');
        $.post(urlInfluenciadorClic, {id: id.pop()});
    });

    $('#list-influenciadores-resultado-busqueda').on('click', '.btn-add', function(event) {
        event.stopPropagation();
        event.preventDefault();

        var influenciador = $(this)
            .closest('.item-influenciador-search')
            .data('influenciador');

        $(this).closest('.item-influenciador-search').fadeOut('slow', function() {
            $(this).remove();
        });

        if ($('#list-influenciadores-seleccionados .item-influenciador-seleccionado[data-id="'+influenciador.id+'"]').length === 0) {
            $('#list-influenciadores-seleccionados')
                .append(influenciadorItemSelectedHbs(influenciador))
                .fadeIn('slow');
        }
    });

    $('#palabra_clave_search').select2({
        placeholder: 'Escriba una Palabra Clave',
        language: 'es',
        allowClear: true,
        minimumInputLength: 3,
        ajax: {
            url: urlPalabraClaveSearchByText,
            data: function (params) {
                params.palabra_clave = true;

                return params;
            },
            processResults: function(result) {
                return {
                    results: $.map(result, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.palabra,
                        };
                    })
                };
            }
        },
    })
    .on('select2:select', function(e) {
        // Detecta si la palabra clave ya esta en la lista de seleccionados
        if ($('#palabra-clave-list li[data-id=' + e.params.data.id + ']').length === 0) {
            $('#palabra-clave-list').append(palabraClaveItemHbs(e.params.data));
        }

        $(this).val(null).trigger('change');
    });

    /*$('#lugar_nacimiento').select2({
        placeholder: 'Lugar de Residencia',
        language: 'es',
        allowClear: true,
        minimumInputLength: 3,
        ajax: {
            url: urlSearchLugarNacimientoByText,
            processResults: function(result) {
                return {
                    results: $.map(result, function(obj) {
                        return {
                            id: obj.lugar_nacimiento,
                            text: obj.lugar_nacimiento,
                        };
                    })
                };
            }
        },
    });*/

    /*$('#pais_id').select2({
        placeholder: 'Pais',
        language: 'es',
        allowClear: true,
        minimumInputLength: 3,
        ajax: {
            url: urlSearchPaisByText,
            processResults: function(result) {
                return {
                    results: $.map(result, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.pais,
                        };
                    })
                };
            }
        },
    });*/


    $('.btn-save-campana').click(function(event) {
        event.preventDefault();
        var $button    = $(this),
            buttonHtml = $button.html();

        if (validateForm($campanaForm)) {
            // prepare form
            var opciones = {
                url:          $campanaForm.attr('action'),
                type:         $campanaForm.attr('method'),
                dataType:    'json',
                data:         gatherDataCampana(),
                beforeSend:   function () {
                    $button.html(config.spinner);
                },
                success: function(responseText, statusText, xhr, $form) {
                    console.log('response: ' + responseText);
                    $button.html(buttonHtml);

                    swal({
                        title: 'Datos guardados exitosamente.',
                        type: 'success',
                        confirmButtonColor: config.color_red,
                        confirmButtonText: 'Aceptar',
                    },
                    function() {
                        window.location.href = urlCampanaIndex;
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $button.html(buttonHtml);
                    console.log('error: ' + textStatus + ' -- ' + errorThrown);
                    errorAlert('Error al procesar los datos. ');
                }
            };

            // set form data and submit form
            $campanaForm.ajaxSubmit(opciones);
            return false;
        }
    });

    // if it has advanced search, then proceed with it on load
    // depends of the origin of the search (basic, advanced)
    if (hasAdvancedSearch === '1') {
        if (origin === '1') {
            sendSearch($('#btn-basic-search'), urlInfluenciadorSearchByText, {q: $('#celebridad-name-search').val()}, pageForPagination);
        }

        if (origin === '2') {
            sendSearch($('#btn-advanced-search'), urlInfluenciadorAdvancedSearch, gatherDataAdvancedSearch(), pageForPagination);
        }

    }
});
