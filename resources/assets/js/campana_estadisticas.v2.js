$(document).ready(function(){
    var myChart;

    option = {
        //  title : {
        //     text:'Distribution',
        //     x:'center'
        // },
        color:[config.grafica_newbie,
            config.grafica_amateur,
            config.grafica_pro,
            config.grafica_expert,
            config.grafica_mreglow],
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            x: 'center',
            y: 'bottom',
            data: []
        },
        // toolbox: {
        //     show : true,
        //     x : 'left',
        //     feature : {
        //         mark : {show: true},
        //         dataView : {show: false, readOnly: false,title:"Ver datos"},
        //         magicType : {
        //             show: true,
        //             type: ['pie', 'funnel'],
        //             option: {
        //                 funnel: {
        //                     x: '25%',
        //                     width: '50%',
        //                     funnelAlign: 'center',
        //                     max: 1548
        //                 }
        //             }
        //         },
        //         restore : {show: false,title:"Restaurar"},
        //         saveAsImage : {show: true,title:" "}
        //     }
        // },
        calculable : true,
        series : [
            {
                name: 'Categoría de influencer',
                type:'pie',
                radius : ['55%', '80%'],
                itemStyle : {
                    normal : {
                        label : {
                            show : false
                        },
                        labelLine : {
                            show : false
                        }
                    },
                    emphasis : {
                        label : {
                            show : true,
                            position : 'center',
                            textStyle : {
                                fontSize : '10',
                                fontWeight : 'bold'
                            }
                        }
                    }
                },
                data:[
                    {value:335, name:'uno'},
                    {value:310, name:'dos'},
                    {value:234, name:'tres'},
                    {value:135, name:'cuatro'},
                    {value:1548, name:'cinco'}
                ],
                avoidLabelOverlap: false,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '11.5'
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
            }
        ]
    };

    function crearTabla(div,repliers){
        $(div).html("");
        $(repliers).each(function(index,value){
            /*var html = "<div class='row' style='margin-left: 38px; margin-bottom: 1em; border-top-left-radius: 25px; border-bottom-left-radius:25px; border:2px solid #00BDC5;' > <div class='col-md-2 col-sm-2' style='padding-left:0px;'><img src='"+value.profile_image+"' class='img circle' style='height:50px;width:50px; margin-top:0px;' /></div>";
            html += "<div class='col-md-6 col-sm-6 text-left'><span class='small'>"+value.name+"</span><div class='clearfix'></div><span>@"+value.screen_name+"</span></div>";
            html += "<div class='col-md-4 col-sm-4 text-center'><span class='small'>Followers</span><div class='clearfix'></div><span>"+value.followers_count+"</span></div></div>";*/

            let html = "<div class='row' style='margin-left: 38px; margin-bottom: 1em; border-top-left-radius: 25px; border-bottom-left-radius:25px; border:2px solid #00BDC5;' > <div class='col-md-2 col-sm-2' style='padding-left:0px;'><img src='"+value.profile_image+"' class='img circle' style='height:50px;width:50px; margin-top:0px;' /></div>";
            html += "<div class='col-md-6 col-sm-6 text-left'><span class='small'>"+value.name+"</span><div class='clearfix'></div><span>"+value.screen_name+"</span></div>";
            html += "<div class='col-md-4 col-sm-4 text-center'><span class='small'>&nbsp;</span><div class='clearfix'></div><span><img src='"+value.icon+"' class='img-small'></span></div></div>";

            $(div).append(html);
        });
    }

    function crearTablaReplies(divId, repliers) {
        $(divId).html('');

        $(repliers).each(function(index, value) {
  //
            let html = "<div class='row' style='margin-left: 38px; margin-bottom: 1em; border-top-left-radius: 25px; border-bottom-left-radius:25px; border:2px solid #00BDC5;' > <div class='col-md-2 col-sm-2' style='padding-left:0px;'><img src='"+value.profile_image+"' class='img-circle img-responsive' style='height:50px; width:50px; margin-top:0px;' /></div>";
            html += "<div class='col-md-2 col-sm-2 text-left'><span class='small'>"+value.name+"</span><div class='clearfix'></div><span>"+value.screen_name+"</span></div>";
            html += "<div class='col-md-2 col-sm-2 text-center'><span class='small'>&nbsp;</span><div class='clearfix'></div><span><img src='"+value.icon+"' class='img-small'></span></div>";
            html += '<div class="col-md-6 col-sm-6"><span class="small">'+value.post+'</span></div></div>';

            $(divId).append(html);
        });
    }

    $('#modalReplies').on('shown.bs.modal', function () {

        if (myChart && myChart.dispose) {
            myChart.dispose();
        }

        myChart = echarts.init(document.getElementById("divgraphreplies"));
        window.onresize = myChart.resize;

        var datos = $("#modalReplies").data("data");
        var repliers = $("#modalReplies").data("repliers");

        option.legend.data = datos.labels;
        option.series[0].data = datos.data;
        myChart.setOption(option, true);

        crearTablaReplies("#divtablereplies",repliers);

    });

    $(document).on("click",".popuprepliesopener",function(){
        var datos = $(this).data('data');
        var repliers = $(this).data('repliers');

        $("#modalReplies").data("data",datos);
        $("#modalReplies").data("repliers",repliers);

    });

    $('#modalRetweets').on('shown.bs.modal', function () {

        if (myChart && myChart.dispose) {
            myChart.dispose();
        }

        myChart = echarts.init(document.getElementById("divgraphretweets"));
        window.onresize = myChart.resize;

        var datos = $("#modalRetweets").data("data");
        var repliers = $("#modalRetweets").data("retweeters");

        option.legend.data = datos.labels;
        option.series[0].data = datos.data;
        myChart.setOption(option, true);

        crearTabla("#divtableretweets",repliers);

    });

    $(document).on("click",".popupretweetsopener",function(){
        var datos = $(this).data('data');
        var retweeters = $(this).data('retweeters');

        $("#modalRetweets").data("data",datos);
        $("#modalRetweets").data("retweeters",retweeters);

    });

    $('a.fancybox').each(function(){
        type = $(this).attr('type');
        config = {};

        if (type == '') {
            type = 'image';
        }

        if (type == 'video') {
            config = {
                type : 'iframe',
                href : $(this).attr('href'),
                title: 'Video'
            };
        } else if (type == 'image') {
            config = {
                type : 'image'
            };
        }

        $(this).fancybox(config);
    });

    var $tblCampaignPosts = $('#tbl-campaign-posts').DataTable({
        paging: false,
        searching: false,
        info: false,
        order: [3, 'desc'],
        columns: [
            { orderable: false }, // Influencer
            { orderable: false }, // Posts and Interactions
            null, // Date
            null, // Reach
            null  // Engagement
        ]
    });

    $('#sort-tbl-campaign-posts').change(function() {
        $tblCampaignPosts.order([$(this).val(), 'desc']).draw();
    });

    var $tblInfluencers = $('#tbl-influencers').DataTable({
        paging: false,
        searching: false,
        info: false,
        order: [3, 'desc'],
        columns: [
            { orderable: false }, // Image
            { orderable: false }, // screen name
            { orderable: false }, // icon red social
            null, // Audience / Reach
            null, // Interactions
            null  // Engagement
        ]
    });

    $('#sort-tbl-influencers').change(function() {
        $tblInfluencers.order([$(this).val(), 'desc']).draw();
    });

    var $tblTopUsers = $('#tbl-top-users').DataTable({
        paging: false,
        searching: false,
        info: false,
        order: [3, 'desc'],
        columns: [
            { orderable: false }, // Image
            { orderable: false }, // screen name
            { orderable: false }, // icono red social
            null, // Audience / Reach
            null, // Friends
            null  // Engagement
        ]
    });

    $('#sort-tbl-top-users').change(function() {
        $tblTopUsers.order([$(this).val(), 'desc']).draw();
    });

    var $tblTopPost = $('#tbl-top-post').DataTable({
        paging: false,
        searching: false,
        info: false,
        order: [4, 'desc'],
        columns: [
            { orderable: false }, // Image and screen name
            { orderable: false }, // Post
            null, // Interactions
            null, // Audience / Reach
            null  // Engagement
        ]
    });

    $('#sort-tbl-top-post').change(function() {
        $tblTopPost.order([$(this).val(), 'desc']).draw();
    });

    // cambiar el ícono de tabs dependiendo si está activo o no
    $('a.campaign-tabs').on('click', function () {
        let $a        = $(this),
            $parent   = $a.parents('li'),
            $child    = $a.children('img'),
            gris      = $child.data('gris'),
            blanco    = $child.data('blanco'),
            rand      = Math.random() * 100000,
            $siblings = $parent.siblings('li');

        // setTimeout(function () {
            if ($parent.hasClass('active')) {
                $a.css('color', '#fff');
                $child.attr('src', blanco);
            } else {
                $a.css('color', '#979598');
                $child.attr('src', gris);
            }

            $siblings.each(function () {
                let $img = $(this).children('a').children('img'),
                    data = $img.data('gris');

                $(this).children('a').css('color', '#979598');
                $img.attr('src', data);
            });
        // }, 100);
    });
});