$(document).ready(function() {
    $('#wordcloudPalabrasClave').jQCloud($('#wordcloudPalabrasClave').data('json'));

    if ($('#wordcloudPalabrasClave').data('json').length === 0) {
        $('#mensajeNoEncontradoKeywords').removeClass('hide');
    } else {
        $('#wordcloudPalabrasClave').jQCloud('update', $('#wordcloudPalabrasClave').data('json'));
    }

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if ($(e.target).attr('href') == '#tab-audience') {
            $('#wordcloudPalabrasClave').jQCloud('update', $('#wordcloudPalabrasClave').data('json'));
        }
    })

});