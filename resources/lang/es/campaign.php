<?php

return [
    'statistics'                   => 'Statistics',
    'campaign_posts'               => 'Campaign Posts',
    'trends'                       => 'Trends',
    'audience'                     => 'Audience',
    'objective'                    => 'Objective',
    'hashtag'                      => 'Hashtag',
    'posts'                        => 'Posts',
    'influencers'                  => 'Influencers',
    'campaign_duration'            => 'Campaign Duration',
    'price'                        => 'Price',
    'posts'                        => 'Posts',
    'influencers'                  => 'Influencers',
    'interactions'                 => 'Interactions',
    'engagement'                   => 'Engagement',
    'reach'                        => 'Reach',
    'top_influencers'              => 'Top Influencers',
    'post_per_hour'                => 'Post Per Hour',
    'gender'                       => 'Gender',
    'men'                          => 'Men',
    'women'                        => 'Women',
    'top_locations'                => 'Top Locations',
    'share_of_social'              => 'Share of Social',
    'share_of_twitter'             => 'Share of Twitter',
    'share_of_facebook'            => 'Share of Facebook',
    'share_of_instagram'           => 'Share of Instagram',
    'share_of_youtube'             => 'Share of Youtube',
    'measured_time'                => 'Measured Time',
    'interactions'                 => 'Interactions',
    'no_encontrado_palabras_clave' => 'No se encontrarón palabras claves.',
    'palabras_mas_utilizadas'      => 'Palabras mas utilizadas',
    'Comments'                     => 'Comments',
    'key_words'                    => 'Keywords',
    'export_pdf'                   => 'Export to PDF',
    'date'                         => 'Date',
    'export_excel'                 => 'Export to Excel',
    'campaign_influencers'         => 'Campaign Influencers',
    'budget'                       => 'Budget',
];