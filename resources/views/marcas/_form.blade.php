@extends('layouts.' . config('app.layout'))

@section('title', $title)

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li><a href="{{ url($response_info['resource']) }}">{{ $title }}</a></li>
        <li class="active">@yield('section_title')</li>
    </ol>
@endsection

@section('content')
    <p>
        <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
            <span class="glyphicon glyphicon-arrow-left"></span> Regresar
        </a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Datos de la {{ $title }} </h3>
        </div>
        <div class="panel-body">
            @include('common.errors', ['response_info' => $response_info])

            {!! BootForm::horizontal($config_form) !!}

                {!! BootForm::text('nombre', false, null, [
                        'data-rule-required'  => 'true',
                        'data-rule-minlength' => '3',
                        'data-toggle'         => 'tooltip',
                        'placeholder'         => $labels['nombre'],
                        'title'               => $labels['nombre'],
                        'icon'                => '&nbsp; &nbsp;',
                    ])
                !!}

                @if (Auth::user()->isAdministrador())
                    {!! BootForm::select('agencia_id', false,
                            $catalogs['agencia'], null, [
                                'data-rule-required' => 'true',
                                'data-toggle'        => 'tooltip',
                                'title'              => $labels['agencia_id'],
                                'icon'               => '&nbsp; &nbsp;',
                                {{-- 'class' => 'select2', --}}
                            ]
                        )
                    !!}
                @endif

                {!! BootForm::select('usuario_id', false,
                        $catalogs['usuario'], null, [
                            'data-toggle' => 'tooltip',
                            'title'       => $labels['usuario_id'],
                            'icon'        => '&nbsp; &nbsp;',
                            {{-- 'class' => 'select2', --}}
                        ]
                    )
                !!}

                {!! BootForm::select('activo', false,
                        ['' => $labels['activo'], '1' => 'Si', '0' => 'No'], null, [
                            'data-rule-required' => 'true',
                            'data-toggle'        => 'tooltip',
                            'title'              => $labels['activo'],
                            'icon'               => '&nbsp; &nbsp;',
                        ]
                    )
                !!}

                {!! BootForm::file('logotipo', false, [
                        'accept'      => 'image/*',
                        'data-toggle' => 'tooltip',
                        'title'       => $labels['logotipo'],
                        'icon'        => '&nbsp; &nbsp;',
                    ])
                !!}

                {!! BootForm::textarea('direccion', false, null, [
                        'data-rule-minlength'  => '3',
                        'div_form_group_class' => 'col-xs-12 col-sm-12 col-md-12',
                        'right_column_class'   => 'col-xs-8 col-sm-10 col-md-8',
                        'left_column_class'    => 'col-xs-4 col-sm-2 col-md-2',
                        'rows'                 => 3,
                        'data-toggle'          => 'tooltip',
                        'placeholder'          => $labels['direccion'],
                        'title'                => $labels['direccion'],
                        'icon'                 => '&nbsp; &nbsp;',
                    ])
                !!}

                {!! BootForm::button('<span class="glyphicon glyphicon-floppy-save"></span> Guardar', [
                        'type'                     => 'submit',
                        'class'                    => 'btn btn-red-rounded',
                        'div_form_group_class'     => 'col-xs-12 col-sm-12 col-md-12',
                        'right_column_class'       => 'col-xs-12 col-sm-12 col-md-12',
                        'left_column_offset_class' => 'text-center',
                    ])
                !!}

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection
