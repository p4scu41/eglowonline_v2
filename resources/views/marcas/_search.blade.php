@extends('layouts.' . config('app.layout') . '_search')

@section('search')
    {!! BootForm::horizontal([
            'method' => 'GET',
            'url' => url($response_info['resource']),
        ])
    !!}

        {!! BootForm::text('filter[nombre]', $labels['nombre'],
                app('request')->input('filter.nombre'), []
            )
        !!}

        @if (Auth::user()->isAdministrador())
            {!! BootForm::select('filter[agencia_id]', $labels['agencia_id'],
                    $catalogs['agencia'], app('request')->input('filter.agencia_id'), []
                )
            !!}
        @endif

        <div class="form-group col-xs-12 col-md-12 text-center">
            {!! Form::button('<span class="glyphicon glyphicon-search"></span> Buscar', [
                    'type' => 'submit',
                    'class' => 'btn btn-red-rounded',
                ]) !!} &nbsp;
            <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
                <span class="glyphicon glyphicon-list"></span> Listar todos
            </a>
        </div>

    {!! BootForm::close() !!}
@endsection
