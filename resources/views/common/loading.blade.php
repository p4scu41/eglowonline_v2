<div class="modal fade bs-example-modal-sm" id="loading" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p class="lead"><i class="fa fa-spinner fa-spin fa-4x"></i> Procesando solicitud, por favor, espere...</p>
            </div>
        </div>
    </div>
</div>