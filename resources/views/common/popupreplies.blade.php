<div id="modalReplies" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="width: 842px; margin: 0 auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                &nbsp;
            </div>
            <div class="modal-body">
                <div class="container">
                    <h4 class="text-center">Replies</h4>
                    <br>
                    <div class="row">
                        <div class="col-sm-8 col-xs-12 row-scroll" id="divtablereplies" style="font-size:11px; max-height: 400px;">

                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div id="divgraphreplies" style="width: 198px; height: 400px; top: -135px; left: 20px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
