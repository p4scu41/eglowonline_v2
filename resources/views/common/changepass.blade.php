<div id="modalChangePass" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <strong>Cambiar Contraseña</strong>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form id="formCambiarPass" action="{{ url('/password/change') }}" method="POST" class="form-horizontal" role="form" data-validate="true">

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="col-xs-3 col-sm-3 control-label">Nueva Contraseña</label>
                                <div class="col-xs-9 col-sm-9">
                                    <input type="password" class="form-control" name="password" id="password" data-rule-minlength="8"
                                    data-rule-passwordvalidation="true" required style="margin-bottom: 25px;">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-3 col-sm-3 control-label">Confirmar Nueva Contraseña</label>
                                <div class="col-xs-9 col-sm-9">
                                    <input type="password" class="form-control" name="confirm_password" id="confirm_password" data-rule-minlength="8" data-rule-equalto="#password" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" class="btn btn-red-rounded" id="btnSaveCambiarPass">Aceptar <i class="fa fa-exchange"></i></button> &nbsp;
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar <i class="fa fa-close"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
