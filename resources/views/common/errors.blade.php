@if ($response_info['show'] || $response_info['error'])
    <div class="alert alert-{{ $response_info['type'] }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <div>
            {!! $response_info['message'] !!}
        </div>
    </div>
@endif

@if (count($errors) > 0)
    <!-- Form Error List -->
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <p>Error al procesar la solicitud:</p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
