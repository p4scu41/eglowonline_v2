{{-- Si se esta implementando Policy --}}
@if ($policy)
    @can('view', $model)
    <a class="btn btn-red-circle btn-xs" data-toggle="tooltip" title="Ver" href="{{ $url }}">
        <i class="fa fa-eye"></i>
    </a>
    @endcan

    @can('update', $model)
    <a class="btn btn-red-circle btn-xs" data-toggle="tooltip" title="Actualizar" href="{{ $url . '/edit' }}">
        <i class="fa fa-pencil"></i>
    </a>
    @endcan

    @can('delete', $model)
    <a class="btn btn-red-circle btn-xs" data-toggle="tooltip" title="Eliminar" href="{{ $url }}"
        data-form="true"
        data-confirm="¿Esta seguro que desea eliminar este registro?"
        data-method="delete">
            <i class="fa fa-trash-o"></i>
    </a>
    @endcan
@else
    <a class="btn btn-red-circle btn-xs" data-toggle="tooltip" title="Ver" href="{{ $url }}">
        <i class="fa fa-eye"></i>
    </a>
    <a class="btn btn-red-circle btn-xs" data-toggle="tooltip" title="Actualizar" href="{{ $url . '/edit' }}">
        <i class="fa fa-pencil"></i>
    </a>
    <a class="btn btn-red-circle btn-xs" data-toggle="tooltip" title="Eliminar" href="{{ $url }}"
        data-form="true"
        data-confirm="¿Esta seguro que desea eliminar este registro?"
        data-method="delete">
            <i class="fa fa-trash-o"></i>
    </a>
@endif
