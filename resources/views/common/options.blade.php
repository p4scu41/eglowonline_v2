{{-- Si se esta implementando Policy --}}
@if ($policy)
    @can('view', $model)
    <a class="btn btn-primary btn-sm" data-toggle="tooltip" title="Ver" href="{{ $url }}">
        <img src="{{ asset('img/iconos/ver.png') }}" width="18">
    </a>&nbsp;
    @endcan

    @can('update', $model)
    <a class="btn btn-warning btn-sm" data-toggle="tooltip" title="Actualizar" href="{{ $url . '/edit' }}">
        <!--<img src="{{ asset('img/iconos/editar.png') }}" width="18">-->
        <i class="fa fa-edit fa-lg"></i>
    </a>&nbsp;
    @endcan

    @can('delete', $model)
    <a class="btn btn-danger btn-sm" data-toggle="tooltip" title="Eliminar" href="{{ $url }}"
        data-form="true"
        data-confirm="¿Esta seguro que desea eliminar este registro?"
        data-method="delete">
            <img src="{{ asset('img/iconos/eliminar.png') }}" width="18">
    </a>
    @endcan
@else
    <a class="btn btn-primary btn-sm" data-toggle="tooltip" title="Ver" href="{{ $url }}">
        <i class="fa fa-eye"></i> Ver
    </a>&nbsp;
    <a class="btn btn-info btn-sm" data-toggle="tooltip" title="Actualizar" href="{{ $url . '/edit' }}">
        <i class="fa fa-pencil"></i> Editar
    </a>&nbsp;
    <a class="btn btn-danger btn-sm" data-toggle="tooltip" title="Eliminar" href="{{ $url }}"
        data-form="true"
        data-confirm="¿Esta seguro que desea eliminar este registro?"
        data-method="delete">
            <i class="fa fa-trash-o"></i> Eliminar
    </a>
@endif
