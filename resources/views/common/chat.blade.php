<div class="modal fade" id="chat-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div id="list-message-chat"></div>
                <div class="row">
                    <div class="col-xs-10 col-sm-10 col-md-10" style="background-color: transparent;">
                        <textarea name="message_chat" class="form-control" id="message_chat"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-red-rounded" id="btn-send-message-chat"
                    data-influenciador="" data-campana="" data-usuario="">Enviar</button>
            </div>
        </div>
    </div>
</div>
