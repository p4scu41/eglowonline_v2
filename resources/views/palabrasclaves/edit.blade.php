@section('section_title', 'Actualizar')

@include($response_info['resource'] . '._form', [
    'config_form' => [
        'model' => $model,
        'update' => $response_info['resource'] . '.update',
        'data-validate' => 'true',
    ],
    'catalogs' => $catalogs,
])
