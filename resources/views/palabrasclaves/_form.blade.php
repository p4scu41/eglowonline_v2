@extends('layouts.' . config('app.layout'))

@section('title', $title)

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li><a href="{{ url($response_info['resource']) }}">{{ $title }}</a></li>
        <li class="active">@yield('section_title')</li>
    </ol>
@endsection

@section('content')
    <p>
        <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
            <span class="glyphicon glyphicon-arrow-left"></span> Regresar
        </a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Datos de la {{ $title }} </h3>
        </div>
        <div class="panel-body">
            @include('common.errors', ['response_info' => $response_info])

            {!! BootForm::horizontal($config_form) !!}

                {{-- {!! BootForm::select('tipo_palabra_clave_id', $labels['tipo_palabra_clave_id'],
                        $catalogs['tipo_palabra_clave'], null, [
                            'data-rule-required' => 'true',
                        ]
                    )
                !!} --}}

                {!! BootForm::text('palabra', false, null, [
                        'data-rule-required'  => 'true',
                        'data-rule-minlength' => '3',
                        'data-toggle'         => 'tooltip',
                        'placeholder'         => $labels['palabra'],
                        'title'               => $labels['palabra'],
                        'icon'                => '&nbsp; &nbsp;',
                    ])
                !!}

                {!! BootForm::select('tipo_palabra_clave_id', false,
                        $catalogs['tipo_palabra_clave'], null, [
                            'data-rule-required' => 'true',
                            'data-toggle'        => 'tooltip',
                            'title'              => $labels['tipo_palabra_clave_id'],
                            'icon'               => '&nbsp; &nbsp;',
                        ]
                    )
                !!}

                {!! BootForm::select('activo', false,
                        ['' => 'Activo', '1' => 'Si', '0' => 'No'], null, [
                            'data-rule-required' => 'true',
                            'data-toggle'        => 'tooltip',
                            'title'              => $labels['activo'],
                            'icon'               => '&nbsp; &nbsp;',
                        ]
                    )
                !!}

                {!! BootForm::button('<span class="glyphicon glyphicon-floppy-save"></span> Guardar', [
                        'type'                     => 'submit',
                        'class'                    => 'btn btn-red-rounded',
                        'div_form_group_class'     => 'col-xs-12 col-sm-12 col-md-12',
                        'right_column_class'       => 'col-xs-12 col-sm-12 col-md-12',
                        'left_column_offset_class' => 'text-center',
                    ])
                !!}

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection
