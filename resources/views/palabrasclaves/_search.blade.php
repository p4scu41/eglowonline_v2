@extends('layouts.' . config('app.layout') . '_search')

@section('search')
    {!! BootForm::horizontal([
            'method' => 'GET',
            'url' => url($response_info['resource']),
        ])
    !!}

        {!! BootForm::text('filter[palabra]', $labels['palabra'],
                app('request')->input('filter.palabra'), []
            )
        !!}

        {!! BootForm::select('filter[tipo_palabra_clave_id]', $labels['tipo_palabra_clave_id'],
                $catalogs['tipo_palabra_clave'], app('request')->input('filter.tipo_palabra_clave_id'), []
            )
        !!}

        <div class="form-group col-xs-12 col-md-12 text-center">
            {!! Form::button('<span class="glyphicon glyphicon-search"></span> Buscar', [
                    'type' => 'submit',
                    'class' => 'btn btn-red-rounded',
                ]) !!} &nbsp;
            <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
                <span class="glyphicon glyphicon-list"></span> Listar todos
            </a>
        </div>

    {!! BootForm::close() !!}
@endsection
