@extends('layouts.' . config('app.layout'))

@section('title', $title)

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li><a href="{{ url($response_info['resource']) }}">{{ $title }}</a></li>
        <li class="active">Ver</li>
    </ol>
@endsection

@section('content')
    <p>
        <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
            <span class="glyphicon glyphicon-arrow-left"></span> Regresar
        </a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Datos de la {{ $title }} </h3>
        </div>
        <div class="panel-body">
            @include('common.errors', ['response_info' => $response_info])

            <div class="col-md-8 col-md-offset-2">
            @if ($model)
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td><label>{{ $labels['tipo_palabra_clave_id'] }}</label></td>
                            <td>{{ $model->tipoPalabraClave->tipo }}</td>
                        </tr>
                        <tr>
                            <td><label>{{ $labels['palabra'] }}</label></td>
                            <td>{{ $model->palabra }}</td>
                        </tr>
                        <tr>
                            <td><label>{{ $labels['activo'] }}</label></td>
                            <td>@boolean($model->activo)</td>
                        </tr>
                    </tbody>
                </table>
            @endif
            </div>

        </div>
    </div>
@endsection
