@section('section_title', 'Actualizar')

@php
    $catalogs['usuario'] = $model->updateCatalogUsuario();
@endphp

@include($response_info['resource'] . '._form', [
    'config_form' => [
        'model' => $model,
        'update' => $response_info['resource'] . '.update',
        'data-validate' => 'true',
        'enctype' => 'multipart/form-data',
    ],
    'catalogs' => $catalogs,
])
