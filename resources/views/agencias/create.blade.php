@section('section_title', 'Registrar')

@include($response_info['resource'] . '._form', [
    'config_form' => [
        'model' => $model,
        'store' => $response_info['resource'] . '.store',
        'data-validate' => 'true',
        'enctype' => 'multipart/form-data',
    ],
    'catalogs' => $catalogs,
])
