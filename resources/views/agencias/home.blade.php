@extends('layouts.' . config('app.layout'))

@section('title', $title)

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    Aqui va esta vista.
                    <img alt="" src="{{ asset('img/Agencia_0_Home-Log.png') }}" style="width: 100%">
                </div>
            </div>

        </div>
    </div>
@endsection
