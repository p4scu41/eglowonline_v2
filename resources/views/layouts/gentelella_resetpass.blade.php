<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="csrf-param" content="_token" />

    <link rel="shortcut icon" type="image/png" href="">
    <!-- <link rel="shortcut icon" type="image/x-icon" href=""> -->

    <title>
        {{ config('app.title') }}
        @hasSection('title')
            - @yield('title')
        @endif
    </title>

    <link href="{{ asset('node_modules/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('node_modules/gentelella/vendors/animate.css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('node_modules/gentelella/build/css/custom.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
</head>

<body class="login">
    <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>

        <div class="login_wrapper">

            <div class="animate form login_form">
                <section class="login_content">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        <h1>Actualizar Contraseña</h1>

                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control has-feedback-left" placeholder="E-mail" name="email" value="{{ $email or old('email') }}" required autofocus>
                            <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="form-control has-feedback-left" placeholder="Contraseña" name="password" required>
                            <span class="fa fa-key form-control-feedback left" aria-hidden="true"></span>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input id="password-confirm" type="password" class="form-control has-feedback-left" placeholder="Confirmar Contraseña" name="password_confirmation" required>
                            <span class="fa fa-key form-control-feedback left" aria-hidden="true"></span>
                        </div>

                        @if ($errors->has('email') || $errors->has('password') || $errors->has('password_confirmation'))
                            <div class="alert alert-danger col-md-12 col-sm-12 col-xs-12">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ $errors->first('email') }}
                                {{ $errors->first('password') }}
                                {{ $errors->first('password_confirmation') }}
                            </div>
                        @endif

                        <div>
                            <button type="submit" class="btn btn-default submit">
                                Aceptar <i class="fa fa-exchange"></i>
                            </button>

                            <a href="{{ url('/login') }}"> Iniciar Sesión </a>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <div>
                                <a href="{{ url('/') }}"><h1><i class="fa fa-users"></i> {{ config('app.title') }}</h1></a>
                                <p><strong>Copyright &copy; 2016 <a href="http://eglow.cl/">eGLOW</a></strong></p>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    <script src="{{ asset('node_modules/gentelella/vendors/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/gentelella/vendors/nprogress/nprogress.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        if (typeof NProgress != 'undefined') {
            $(document).ready(function () {
                NProgress.start();
            });

            $(window).load(function () {
                NProgress.done();
            });
        }
    </script>

    @stack('scripts')
</body>

</html>
