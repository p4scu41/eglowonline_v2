@extends('layouts.' . config('app.layout') )

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>{{ config('app.title') }}</b></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg"><strong>Actualizar Contraseña</strong></p>

            @if ($errors->has('email') || $errors->has('password') || $errors->has('password_confirmation'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $errors->first('email') }}
                    {{ $errors->first('password') }}
                    {{ $errors->first('password_confirmation') }}
                </div>
            @endif

            <form id="login-form" class="form-vertical" action="{{ url('/password/reset') }}" method="post">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" id="email" class="form-control" name="email" placeholder="E-mail" value="{{ $email or old('email') }}" autofocus required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" id="password" class="form-control" name="password" placeholder="Contraseña" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input type="password" id="password_confirmation" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <button type="submit" class="btn btn-red-rounded btn-block btn-flat" name="login-button">
                            Aceptar &nbsp; <i class="fa fa-exchange" aria-hidden="true"></i>
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
@endsection
