<div class="box box-default collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Opciones de búsqueda</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
        </div>
    </div>

    <div class="box-body">

        @yield('search')

    </div>
</div>
