<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li class="">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Usuarios</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#" class="no-loading">
                    <i class="fa fa-tablet"></i> <span>Dispositivos</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="#">
                            <i class="fa fa-list-ol"></i> <span>Listado</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#">
                            <i class="fa fa-map"></i> <span>Mapa</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
