@extends('layouts.' . config('app.layout') )

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>{{ config('app.title') }}</b></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg"><strong>Recuperar Contraseña</strong></p>

            @if (session('status'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ session('status') }}
                </div>
            @endif

            @if ($errors->has('email'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $errors->first('email') }}
                </div>
            @endif

            <form id="login-form" class="form-vertical" action="{{ url('/password/email') }}" method="post">
                {{ csrf_field() }}

                <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" id="email" class="form-control" name="email" placeholder="E-mail" autofocus required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="row">
                    <p class="col-xs-12 text-justify">
                        Se enviará un correo electrónico con las indicaciones para recuperar su contraseña.
                    </p>
                    <div class="col-xs-8">
                        <a href="{{ url('/login') }}">Iniciar Sesión</a>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-red-rounded btn-block btn-flat" name="login-button">
                            Aceptar &nbsp; <i class="fa fa-exchange" aria-hidden="true"></i>
                        </button>
                    </div>
                    <!-- /.col -->
                    <p class="col-xs-12"><br></p>
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
@endsection
