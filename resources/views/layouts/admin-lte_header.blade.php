
<!-- Main Header -->
<header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">{!! config('app.title-header-mini') !!}</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">{!! config('app.title-header-lg') !!}</span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{ url('img/user_icon.png') }}" class="user-image" alt="" />
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->

                        <span class="hidden-xs">
                            {{ Auth::user()->nombre }}
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="{{ url('img/user_icon.png') }}" class="img-circle" alt="" />
                            <p>
                                {{ Auth::user()->nombre }}
                                <small>Último acceso: {{ Auth::user()->ultimo_acceso }}</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- <li class="user-body">
                            <div class="row">
                            </div>
                        </li> -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="text-center">
                                <a href="{{ url('usuarios/perfil') }}" class="btn btn-default btn-flat btn-sm">Perfil</a>
                                <a href="#modalChangePass" data-toggle="modal" class="btn btn-default btn-flat btn-sm no-loading" id="btnCambiarPass">Cambiar Contraseña</a>
                                <a href="{{ url('/logout') }}" data-form="true" data-method="post" class="btn btn-default btn-flat btn-sm" id="btnSalir">Salir</a>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>
