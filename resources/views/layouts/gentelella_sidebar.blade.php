<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ url('/') }}" class="site_title">
                <img style="width: 100%" src="{{ asset(config('app.title-header-lg')) }}" alt="">
            </a>
        </div>
        <div class="clearfix"></div>
        <br />
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">

                    @if (Auth::user()->isAdministrador())
                        <li class="{{ Request::is('logeventos*') ? 'active' : '' }}">
                            <a href="{{ url('logeventos') }}"><i class="fa fa-list"></i> HOME</a>
                        </li>

                        @can('catalogo', 'App\Models\Influenciador')
                            <li class="{{ Request::is('influenciadores/catalogo') ? 'active' : '' }}">
                                <a href="{{ url('influenciadores/catalogo') }}"><i class="fa fa-users"></i> Catálogo</a>
                            </li>
                        @endcan

                        <li class="{{ (Request::is('usuarios*') || Request::is('influenciadores*')) && !Request::is('influenciadores/catalogo') ? 'active' : '' }}">
                            <a class="no-loading">
                                <i class="fa fa-users"></i> Personas
                                <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu" style="display: {{ (Request::is('usuarios*') || Request::is('influenciadores*')) && !Request::is('influenciadores/catalogo') ? 'block' : 'none' }};">
                                @can('index', 'App\Models\User')
                                    <li class="{{ Request::is('usuarios*') && Request::path()!='usuarios/perfil' ? 'active' : '' }}">
                                        <a href="{{ url('usuarios') }}"><i class="fa fa-users"></i> Usuarios</a>
                                    </li>
                                @endcan

                                @can('index', 'App\Models\Influenciador')
                                    <li class="{{ Request::is('influenciadores*') && !Request::is('influenciadores/sincronizacion') && !Request::is('influenciadores/catalogo') ? 'active' : '' }}">
                                        <a href="{{ url('influenciadores') }}"><i class="fa fa-smile-o"></i> Influenciadores</a>
                                    </li>
                                @endcan

                                @can('syncperfiles', 'App\Models\Influenciador')
                                    {{--<li class="{{ Request::is('influenciadores/sincronizacion') ? 'active' : '' }}">
                                        <a href="{{ url('influenciadores/sincronizacion') }}"><i class="fa fa-refresh"></i> Sincronización</a>
                                    </li>--}}
                                @endcan

                            </ul>
                        </li>

                        <li class="no-loading {{ Request::is('agencias*') || Request::is('marcas*') || Request::is('productos*') || Request::is('palabrasclaves*') ? 'active' : '' }}">
                            <a class="no-loading">
                                <i class="fa fa-cogs"></i> Gestión
                                <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu" style="display: {{ Request::is('agencias*') || Request::is('marcas*') || Request::is('productos*') || Request::is('palabrasclaves*') ? 'block' : 'none' }};">
                                @can('index', 'App\Models\Agencia')
                                    <li class="{{ Request::is('agencias*') ? 'active' : '' }}">
                                        <a href="{{ url('agencias') }}"><i class="fa fa-building"></i> Agencias</a>
                                    </li>
                                @endcan

                                @can('index', 'App\Models\Marca')
                                    <li class="{{ Request::is('marcas*') ? 'active' : '' }}">
                                        <a href="{{ url('marcas') }}"><i class="fa fa-list-ol"></i> Marcas</a>
                                    </li>
                                @endcan

                                @can('index', 'App\Models\Producto')
                                    <li class="{{ Request::is('productos*') ? 'active' : '' }}">
                                        <a href="{{ url('productos') }}"><i class="fa fa-cubes"></i> Productos</a>
                                    </li>
                                @endcan

                                @can('index', 'App\Models\PalabraClave')
                                    <li class="{{ Request::is('palabrasclaves*') ? 'active' : '' }}">
                                        <a href="{{ url('palabrasclaves') }}"><i class="fa fa-th-large"></i> Palabras Claves</a>
                                    </li>
                                @endcan
                            </ul>
                        </li>

                        @can('index', 'App\Models\Campana')
                            <li class="{{ Request::is('campanas*') ? 'active' : '' }}">
                                <a href="{{ url('campanas') }}"><i class="fa fa-bullhorn"></i> Campañas</a>
                            </li>
                        @endcan
                    @endif

                    @if (Auth::user()->isAgencia())
                        @can('catalogo', 'App\Models\Influenciador')
                            <li class="{{ Request::is('influenciadores/catalogo') ? 'active' : '' }}">
                                <a href="{{ url('influenciadores/catalogo') }}"><i class="fa fa-users"></i> HOME</a>
                            </li>
                        @endcan

                        @can('index', 'App\Models\Influenciador')
                            <li class="{{ Route::currentRouteName() == 'influenciadores.index' ? 'active' : '' }}">
                                <a href="{{ url('influenciadores') }}"><i class="fa fa-smile-o"></i> Influenciadores </a>
                            </li>
                        @endcan

                        <li class="no-loading {{ Request::is('marcas*') || Request::is('productos*') || Request::is('palabrasclaves*') ? 'active' : '' }}">
                            <a class="no-loading">
                                <i class="fa fa-cogs"></i> Gestión
                                <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu" style="display: {{ Request::is('marcas*') || Request::is('productos*')  ? 'block' : 'none' }};">
                                @can('index', 'App\Models\Marca')
                                    <li class="{{ Request::is('marcas*') ? 'active' : '' }}">
                                        <a href="{{ url('marcas') }}"><i class="fa fa-list-ol"></i> Marcas</a>
                                    </li>
                                @endcan

                                @can('index', 'App\Models\Producto')
                                    <li class="{{ Request::is('productos*') ? 'active' : '' }}">
                                        <a href="{{ url('productos') }}"><i class="fa fa-cubes"></i> Productos</a>
                                    </li>
                                @endcan

                                @can('index', 'App\Models\PalabraClave')
                                    <li class="{{ Request::is('palabrasclaves*') ? 'active' : '' }}">
                                        <a href="{{ url('palabrasclaves') }}"><i class="fa fa-th-large"></i> Palabras Claves</a>
                                    </li>
                                @endcan
                            </ul>
                        </li>

                        @can('index', 'App\Models\Campana')
                            <li class="{{ Request::is('campanas*') ? 'active' : '' }}">
                                <a href="{{ url('campanas') }}"><i class="fa fa-bullhorn"></i>Mis Campañas</a>
                            </li>
                        @endcan

                    @endif

                    @if (Auth::user()->isCelebridad())
                        @can('index', 'App\Models\Campana')
                            <li class="{{ Request::is('influenciadores/miscampanas/*') || Request::is('influenciadores/miscampanas') ? 'active' : '' }}">
                                <a href="{{ url('influenciadores/miscampanas') }}">
                                    <i class="fa fa-bullhorn
                                    {!! Auth::user()->influenciador->tieneCampanaPendiente() || Auth::user()->influenciador->tienePublicacionPendiente() ? 'animated infinite zoomIn' : '' !!}"></i>
                                    Mis Campanas
                                </a>
                            </li>
                        @endcan

                        <li class="{{ Request::is('influenciadores/miinfo') ? 'active' : '' }}">
                            <a href="{{ url('influenciadores/miinfo') }}"><i class="fa fa-user"></i> Mi perfil</a>
                        </li>

                    @endif

                    @if (Auth::user()->isMarca() || Auth::user()->isProducto())
                        @can('catalogo', 'App\Models\Influenciador')
                            <li class="{{ Request::is('influenciadores/catalogo') ? 'active' : '' }}">
                                <a href="{{ url('influenciadores/catalogo') }}"><i class="fa fa-users"></i> HOME</a>
                            </li>
                        @endcan
                    @endif
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>
