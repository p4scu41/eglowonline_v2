<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="csrf-param" content="_token" />

    <link rel="shortcut icon" type="image/png" href="">
    <!-- <link rel="shortcut icon" type="image/x-icon" href=""> -->

    <title>
        {{ config('app.title') }}
        @hasSection ('title')
            - @yield('title')
        @endif
    </title>

    <link href="{{ asset('node_modules/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('js/plugins/jAlert/jAlert-v4.css') }}" rel="stylesheet" type="text/css" />
    <!-- <link href="{{ asset('js/plugins/sweetalert/dist/sweetalert.css') }}" rel="stylesheet" type="text/css" /> -->
    <link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" />
    <link href="{{ asset('js/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('node_modules/admin-lte/plugins/iCheck/all.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('node_modules/admin-lte/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('js/plugins/tooltipster/dist/css/tooltipster.bundle.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('js/plugins/tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-light.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{ asset('node_modules/gentelella/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet"> --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet">
    <link href="{{ asset('node_modules/gentelella/build/css/custom.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/alert-eglow.css') }}" type="text/css" />

    @stack('styles')

    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />

    <?php config(['app.url' => url('/')]); ?>

    <script type="text/javascript">
        var app_url = "{{ url('/') }}";
    </script>

    @stack('scripts-head')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="{{ Auth::check() ? 'nav-md' : 'login' }}">
    <div class="container body">
        <div class="main_container">

            @if (Auth::check())
                @include('layouts.gentelella_sidebar')
                @include('layouts.gentelella_header')
            @endif

            <!-- page content -->
            <div class="right_col" role="main"
                style="{{ Auth::check() ? '' : 'margin-left: 0px' }}
                    {{ Route::currentRouteName() == 'influenciadores.show' ? 'background: white;' : '' }}">
                <div class="">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        @if (session('message'))
                            <div class="alert alert-{{ session('type') ? session('type') : 'info' }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session('message') }}
                            </div>
                        @endif

                        @yield('content')
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->
        </div>
    </div>

    @include('common.changepass')

    <script src="{{ asset('node_modules/gentelella/vendors/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/admin-lte/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/admin-lte/plugins/select2/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/admin-lte/plugins/select2/i18n/es.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/moment/min/moment-with-locales.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/jquery-serialize-object/dist/jquery.serialize-object.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/jAlert/jAlert-v4.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/echart/echarts.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/jAlert/jAlert-functions.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('js/plugins/sweetalert/dist/sweetalert.min.js') }}" type="text/javascript"></script> -->
    <script type="text/javascript" src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/js.cookie/js.cookie.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/StickyTableHeaders/jquery.stickytableheaders.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/jquery-validation/dist/jquery.validate.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/jquery-validation/dist/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/jquery-validation/dist/localization/messages_es.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/gentelella/vendors/nprogress/nprogress.js') }}" type="text/javascript"></script>
    {{-- <script src="{{ asset('node_modules/gentelella/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}" type="text/javascript"></script> --}}
    <script src="{{ asset('js/plugins/tooltipster/dist/js/tooltipster.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/selectToList.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/notify.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/gentelella/build/js/custom.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/alert-eglow.js') }}"></script>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>


    @stack('scripts')
</body>

</html>

