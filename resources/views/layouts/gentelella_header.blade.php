<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ url('/logout') }}" data-form="true" data-method="post" class="text-center">
                        <span class="fa fa-sign-out"></span><br>
                        <span class="color-white">Salir</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('usuarios/perfil') }}" class="text-center">
                        <span class="fa fa-user"></span><br>
                        <span class="color-white">Mi info</span>
                    </a>
                </li>
                <li>
                    <a href="#modalChangePass" data-toggle="modal" class="text-center" title="Cambiar Contraseña" data-toggle="tooltip" id="btnCambiarPass">
                        <span class="fa fa-key"></span><br>
                        <span class="color-white">Mi Pass</span>
                    </a>
                </li>
                {{--<li>
                    <a href="#" class="text-center">
                        <span class="fa fa-search"></span><br>
                        <span class="color-white">Busca</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="text-center">
                        <span class="fa fa-plus fa-2x"></span><br>
                    </a>
                </li>--}}
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->
