<div class="x_panel" style="height: auto;">
    <div class="x_title">
        <h2>Opciones de búsqueda</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="x_content" style="display: none;">

        <div class="row">
            <div class="col-md-12">
                @yield('search')
            </div>
        </div>
    </div>
</div>
