<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="csrf-param" content="_token" />

    <link rel="shortcut icon" type="image/png" href="">
    <!-- <link rel="shortcut icon" type="image/x-icon" href=""> -->

    <title>
        {{ config('app.title') }}
        @hasSection ('title')
            - @yield('title')
        @endif
    </title>

    <link href="{{ asset('node_modules/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('node_modules/admin-lte/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('node_modules/admin-lte/dist/css/skins/skin-red.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/icomoon/icomoon.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/ionicons/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('js/plugins/jAlert/jAlert-v4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('js/plugins/fancyBox/jquery.fancybox.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('js/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('node_modules/admin-lte/plugins/pace/pace.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('node_modules/admin-lte/plugins/iCheck/all.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('node_modules/admin-lte/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('node_modules/admin-lte/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />

    <?php config(['app.url' => url('/')]); ?>

    <script type="text/javascript">
        var app_url = "{{ url('/') }}";
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="hold-transition skin-red sidebar-mini">

    <div class="wrapper">

        @if (Auth::check())
            @include('layouts.admin-lte_header')
            @include('layouts.admin-lte_sidebar')
        @endif

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="{{ Auth::check() ? '' : 'margin-left: 0px;' }}">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>@yield('title')</h1>

                @yield('breadcrumbs')
            </section>

            <!-- Main content -->
            <section class="content">
                @if (session('message'))
                    <div class="alert alert-{{ session('type') ? session('type') : 'info' }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ session('message') }}
                    </div>
                @endif

                @if (Auth::check())
                    <div class="panel panel-default">
                        <div class="panel-body" id="main-content">
                            @yield('content')
                        </div>
                    </div>
                @else
                    @yield('content')
                @endif
            </section>
            <!-- /.content -->

        </div>
        <!-- /.content-wrapper -->

        @include('common.changepass')

        <!-- Main Footer -->
        <footer class="main-footer" style="{{ Auth::check() ? '' : 'margin-left: 0px;' }}">
            <!-- Default to the left -->
            <strong>Copyright &copy; 2016 <a href="http://www.importare.mx/">Importare.mx</a>.</strong> Todos los derechos reservados.
        </footer>
    </div>
    <!-- ./wrapper -->

    <script src="{{ asset('js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/admin-lte/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/admin-lte/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/admin-lte/plugins/select2/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/admin-lte/plugins/select2/i18n/es.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/admin-lte/plugins/colorpicker/bootstrap-colorpicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/admin-lte/dist/js/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/moment/min/moment-with-locales.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/jquery-serialize-object/dist/jquery.serialize-object.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/jAlert/jAlert-v4.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/echart/echarts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/jAlert/jAlert-functions.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/js.cookie/js.cookie.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/StickyTableHeaders/jquery.stickytableheaders.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/underscore/underscore-min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/fancyBox/jquery.fancybox.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/jquery-validation/dist/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/jquery-validation/dist/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/jquery-validation/dist/localization/messages_es.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/helper.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins.js') }}" type="text/javascript"></script>

    @stack('scripts')

</body>
</html>
