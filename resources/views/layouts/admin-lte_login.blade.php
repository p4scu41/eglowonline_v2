@extends('layouts.' . config('app.layout') )

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>{{ config('app.title') }}</b></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg"><strong>Inicia sesión con tu Correo Electrónico y Contraseña</strong></p>

            @if (session('status'))
                <div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ session('status') }}
                </div>
            @endif

            @if ($errors->has('email') || $errors->has('password'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $errors->first() }}
                </div>
            @endif

            <form id="login-form" class="form-vertical" action="{{ url('/login') }}" method="post">
                {{ csrf_field() }}

                <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" id="email" class="form-control" name="email" placeholder="E-mail" value="{{ $email or old('email') }}" autofocus required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" id="password" class="form-control" name="password" placeholder="Contraseña" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <a href="{{ url('/password/reset') }}">Olvidé mi contraseña</a>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-5 text-center">
                        <button type="submit" class="btn btn-red-rounded btn-flat" name="login-button">
                            Aceptar &nbsp; <i class="fa fa-key" aria-hidden="true"></i>
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p>&nbsp;</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <a href="{{ url('/auth/twitter/login') }}" class="btn btn-default">
                            <i class="fa fa-twitter"></i> Iniciar sesión con Twitter
                        </a>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
@endsection
