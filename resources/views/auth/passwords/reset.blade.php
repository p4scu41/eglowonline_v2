@include('layouts.' . config('app.layout') . '_resetpass', [
    'token' => $token,
    'email' => $email,
])
