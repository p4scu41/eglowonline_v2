@include('common.errors', ['response_info' => $response_info])
    {!! BootForm::horizontal($config_form) !!}

    {!! BootForm::text('nombre', false, null, [
            'data-rule-required'  => 'true',
            'data-rule-minlength' => '3',
            'data-rule-maxlength' => '60',
            'data-toggle'         => 'tooltip',
            'placeholder'         => $labels['nombre'],
            'title'               => $labels['nombre'],
            'icon'                => '&nbsp; &nbsp;',
        ])
    !!}

    {!! BootForm::email('usuario[email]', false, null, [
            'data-rule-required'  => 'true',
            'data-rule-minlength' => '3',
            'data-toggle'         => 'tooltip',
            'placeholder'         => $labels['email'],
            'title'               => $labels['email'],
            'icon'                => '&nbsp; &nbsp;',
        ])
    !!}

    <div class="form-group col-xs-12 col-sm-12 col-md-6">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="input-group">
                <span class="input-group-addon"></span>
                <div class="form-control float-initial">
                    <div>{{ $labels['genero'] }}</div>

                    <select data-rule-required="true" id="genero" name="genero" class="selectToList">
                        @foreach ($catalogs['genero'] as $genero)
                            <option value="{{ $genero['id'] }}"
                                data-imagen="{{ asset($genero['icono']) }}"
                                {{ $genero['id'] == $model->genero ? 'selected' : '' }}>
                                    {{ $genero['genero'] }}
                            </option>
                        @endforeach
                    </select>
                    <span id="genero-error" class="help-block"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group col-xs-12 col-sm-12 col-md-6">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="input-group">
                <span class="input-group-addon"></span>
                <div class="form-control float-initial">
                    <div>{{ $labels['tipo_influenciador_id'] }}</div>

                    <select data-rule-required="true" id="tipo_influenciador_id" name="tipo_influenciador_id" class="selectToList">
                        @foreach ($catalogs['tipo_influenciador'] as $tipo_influenciador)
                            <option value="{{ $tipo_influenciador['id'] }}"
                                data-imagen="{{ asset($tipo_influenciador['icono']) }}"
                                {{ $tipo_influenciador['id'] == $model->tipo_influenciador_id ? 'selected' : '' }}>
                                    {{ $tipo_influenciador['descripcion'] }}
                            </option>
                        @endforeach
                    </select>
                    <span id="tipo_influenciador_id-error" class="help-block"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group col-xs-12 col-sm-12 col-md-6">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="input-group">
                <span class="input-group-addon"></span>
                <div class="form-control float-initial">
                    <div>{{ $labels['industria_id'] }}</div>

                    <select data-rule-required="true" id="industria_id" name="industria_id" class="selectToList">
                        @foreach ($catalogs['industria'] as $industria)
                            <option value="{{ $industria['id'] }}"
                                data-imagen="{{ asset($industria['icono']) }}"
                                {{ $industria['id'] == $model->industria_id ? 'selected' : '' }}>
                                    {{ $industria['nombre'] }}
                            </option>
                        @endforeach
                    </select>
                    <span id="industria_id-error" class="help-block"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group col-xs-12 col-sm-12 col-md-6">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="input-group" id="div-rango-etario">
                <span class="input-group-addon"></span>
                <div class="form-control float-initial">
                    <div>{{ $labels['rango_etario_id'] }}</div>

                    <select data-rule-required="true" id="rango_etario_id" name="rango_etario_id" class="selectToList">
                        @foreach ($catalogs['rango_etario'] as $rango_etario)
                            <option value="{{ $rango_etario['id'] }}"
                                data-imagen="{{ asset($rango_etario['icono']) }}"
                                {{ $rango_etario['id'] == $model->rango_etario_id ? 'selected' : '' }}>
                                    {{ $rango_etario['descripcion'] }}
                            </option>
                        @endforeach
                    </select>
                    <span id="genero-error" class="help-block"></span>
                </div>
            </div>
        </div>
    </div>

    {!! BootForm::text('lugar_nacimiento', false, null, [
            'data-rule-maxlength' => '100',
            'data-toggle'         => 'tooltip',
            'placeholder'         => $labels['lugar_nacimiento'],
            'title'               => $labels['lugar_nacimiento'],
            'icon'                => '&nbsp; &nbsp;',
        ])
    !!}

    {!! BootForm::select('pais_id', false,
            $catalogs['pais'], null, [
                'data-rule-required' => 'true',
                'data-toggle'        => 'tooltip',
                'title'              => $labels['pais_id'],
                'icon'               => '&nbsp; &nbsp;',
            ]
        )
    !!}

    {!! BootForm::text('website', false, null, [
            'data-toggle'  => 'tooltip',
            'placeholder'  => 'Escriba el nombre de su sitio y presione enter o tab',
            'title'        => $labels['website'],
            'data-role'    => 'tagsinput',
            'icon'         => '&nbsp; &nbsp;',
        ])
    !!}

    {!! BootForm::text('telefono', false, null, [
            'data-rule-maxlength' => '20',
            'data-toggle'         => 'tooltip',
            'placeholder'         => $labels['telefono'],
            'title'               => $labels['telefono'],
            'icon'                => '&nbsp; &nbsp;',
        ])
    !!}

    {!! BootForm::text('manager_nombre', false, null, [
            'data-rule-maxlength' => '70',
            'data-toggle'         => 'tooltip',
            'placeholder'         => $labels['manager_nombre'],
            'title'               => $labels['manager_nombre'],
            'icon'                => '&nbsp; &nbsp;',
        ])
    !!}

    {!! BootForm::text('manager_telefono', false, null, [
            'data-rule-maxlength' => '20',
            'data-toggle'         => 'tooltip',
            'placeholder'         => $labels['manager_telefono'],
            'title'               => $labels['manager_telefono'],
            'icon'                => '&nbsp; &nbsp;',
        ])
    !!}

    <div class="form-group col-xs-12 col-sm-12 col-md-6">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="input-group">
                <span class="input-group-addon"></span>
                <div class="form-control float-initial">
                    <div>{{ $labels['notificar_x_correo'] }}</div>

                    <select data-rule-required="true" id="notificar_x_correo" name="notificar_x_correo" class="selectToList">
                        @foreach ($catalogs['si_no'] as $si_no)
                            <option value="{{ $si_no['id'] }}"
                                data-imagen="{{ asset($si_no['icono']) }}"
                                {{ $si_no['id'] === '' || empty($model->id) ? '' : ($si_no['id'] == $model->notificar_x_correo ? 'selected' : '') }}>
                                    {{ $si_no['descripcion'] }}
                            </option>
                        @endforeach
                    </select>
                    <span id="notificar_x_correo-error" class="help-block"></span>
                </div>
            </div>
        </div>
    </div>

    @if (Auth::user()->isAdministrador() || Auth::user()->isAgencia())
        <div class="form-group col-xs-12 col-sm-12 col-md-6">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="input-group">
                    <span class="input-group-addon"></span>
                    <div class="form-control float-initial">
                        <div>{{ $labels['activo'] }}</div>

                        <select data-rule-required="true" id="usuario[activo]" name="usuario[activo]" class="selectToList">
                            @foreach ($catalogs['si_no'] as $si_no)
                                <option value="{{ $si_no['id'] }}"
                                    data-imagen="{{ asset($si_no['icono']) }}"
                                    {{ $si_no['id'] === '' || empty($model->usuario_id) ? '' : ($si_no['id'] == $model->usuario->activo ? 'selected' : '' ) }}>
                                        {{ $si_no['descripcion'] }}
                                </option>
                            @endforeach
                        </select>
                        <span id="usuario.activo-error" class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="form-group col-xs-12 col-sm-12 col-md-6">
        <div class="input-group" style="padding-right: 10px; padding-left: 10px;">
            <span class="input-group-addon"></span>
            <div class="form-control float-initial">
                {{ $labels['profesion'] }}
                <select id="profesion_search" name="profesion_search" style="width: 100%;">
                </select>
            </div>
        </div>
        <div style="margin-right:10px;">
            <div id="palabra-clave-box" style="margin-left:50px;">
                <ul id="profesion-list">
                    @foreach ($model->influenciadorXProfesion as $profesion)
                        @include($response_info['resource'] . '/palabrasclave_tpl', [
                            'id'   => $profesion->palabra_clave_id,
                            'text' => $profesion->palabraClave->palabra,
                        ])
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div class="form-group col-xs-12 col-sm-12 col-md-6">
        <div class="input-group" style="padding-right: 10px; padding-left: 10px;">
            <span class="input-group-addon"></span>
            <div class="form-control float-initial">
                Me Interesa
                <select id="palabra_clave_search" name="palabra_clave_search" style="width: 100%;">
                </select>
            </div>
        </div>
        <div style="margin-right:10px;">
            <div id="palabra-clave-box" style="margin-left:50px;">
                <ul id="palabra-clave-list">
                    @foreach ($model->palabrasClavePalabra() as $palabra_clave)
                        @include($response_info['resource'] . '/palabrasclave_tpl', [
                            'id'   => $palabra_clave->palabra_clave_id,
                            'text' => $palabra_clave->palabraClave->palabra,
                        ])
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    {!! BootForm::textarea('biografia', false, null, [
            'data-rule-minlength'  => '3',
            'div_form_group_class' => 'col-xs-12 col-sm-12 col-md-12',
            'right_column_class'   => 'col-xs-8 col-sm-10 col-md-8',
            'left_column_class'    => 'col-xs-4 col-sm-2 col-md-2',
            'rows'                 => 4,
            'data-toggle'          => 'tooltip',
            'placeholder'          => $labels['biografia'],
            'title'                => $labels['biografia'],
            'icon'                 => '&nbsp; &nbsp;',
        ])
    !!}

    <div class="text-center col-xs-12 col-sm-12 col-md-12">
        <a class="btn btn-red-rounded btn-next-to no-loading" data-tab="#redes-sociales-tab">
            Siguiente
        </a> &nbsp;

        @if ($model->exists)
            <a class="btn btn-red-rounded btn-save-influencer">
                <span class="glyphicon glyphicon-floppy-save"></span> Guardar
            </a>
        @endif
    </div>
{!! BootForm::close() !!}