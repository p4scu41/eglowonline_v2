<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
        <div class="text-center black-box">
            <div>
                <div>
                    <button type="button" data-id="interacciones" class="content-tab tab-contenido text-center no-loading btn btn-red-rounded">INTERACCIONES</button>
                    <button type="button" data-id="alcance" class="content-tab tab-contenido text-center no-loading btn btn-default btn-round">ALCANCE</button>
                    <button type="button" data-id="ratioConversacion" class="content-tab tab-contenido text-center no-loading btn btn-default btn-round hide">RATIO DE CONVERSACIÓN</button>
                    <button type="button" data-id="ratioAmplificacion" class="content-tab tab-contenido text-center no-loading btn btn-default btn-round hide">RATIO DE AMPLIFICACIÓN</button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 content-tab" id="interacciones">
        @include('influenciadores.show_contenido_interacciones')
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 content-tab hide" id="alcance">
        @include('influenciadores.show_contenido_alcance')
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 content-tab hide" id="ratioConversacion">
        En construccion
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 content-tab hide" id="ratioAmplificacion">
        En construccion
    </div>
</div>