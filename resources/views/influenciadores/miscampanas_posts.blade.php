@extends('layouts.' . config('app.layout') )

@push('styles')
    <link href="{{ asset('node_modules/bootstrap-fileinput/css/fileinput.min.css') }}" rel="stylesheet">
@endpush

@push('scripts-head')
    <script type="text/javascript">
        var urlSavePublicaciones = '{{ url('publicaciones') }}',
            urlChat = '{{ url('chat') }}',
            urlAprobarPosts = '{{ url('influenciadores/aprobarpublicaciones') }}',
            urlAprobarPublicacion = '{{ url('publicaciones/aprobar') }}',
            hashtags = '{{ $campana->hashtags }}',
            maxFileSize = {{ \App\Helpers\Helper::returnKilobytes(ini_get('upload_max_filesize')) }};
    </script>
@endpush


@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <h2>GESTIONA LOS POSTS DE TU CAMPAÑA</h2>

                @php
                    $pubXInfluXCampTwitter  = $influenciador->pubXInfluXCampTwitter($campana->id);
                    $pubXInfluXCampFacebook = $influenciador->pubXInfluXCampFacebook($campana->id);
                    $participacionCampana   = $influenciador->influenciadorXCampana($campana->id);
                @endphp

                <div class="col-xs-12 col-sm-12 col-md-12 box-border-gray text-dark-gray">
                    <div>
                        &nbsp; &nbsp; &nbsp;
                        <i class="fa fa-calendar fa-2x text-red"></i>
                        &nbsp; &nbsp; &nbsp;&nbsp;
                        <span class="campana_fechas">
                            @if ($campana->fecha_desde)
                                @date($campana->fecha_desde)
                            @endif
                            -
                            @if ($campana->fecha_hasta)
                                @date($campana->fecha_hasta)
                            @endif
                        </span>
                    </div>
                    <br>
                    <div>
                        @if (!$campana->getUserOwner()->inEntornoTelevisa())
                            &nbsp; &nbsp; &nbsp;
                            <i class="fa fa-credit-card fa-2x text-red"></i>
                            &nbsp; &nbsp; &nbsp;
                            <span class="campana_presupuesto">{{ $participacionCampana ? $participacionCampana->precio_publicaciones_currency : 0 }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <p>&nbsp;</p>

            <div class="row container-posts" id="posts_campana" style="margin: 0">
                <div class="row row-post" style="margin: 0">
                    <div class="col-xs-2 col-sm-2 col-md-1 container-avatar">
                        <img src="{{ str_replace('_normal', '', $influenciador->imgPerfil) }}" class="avatar">
                        <span class="cantidad_seguidores red-box">
                            {{ \App\Helpers\Helper::formatNumber($influenciador->totalFollowers) }}
                        </span>
                    </div>
                    <div class="col-xs-10 col-sm-10 col-md-11 body">
                        <div class="row blue-box">
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <p>{{ $influenciador->nombre }}</p>
                                <p>{{ $influenciador->tipoInfluenciador->descripcion }}</p>
                                <p class="text-red">
                                    {{ $influenciador->perfilTwitter ? $influenciador->perfilTwitter->screen_name : ''}}
                                </p>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3"></div>

                            <div class="col-xs-3 col-sm-3 col-md-3 container-opciones-redes-sociales text-left">
                                {{-- ================================ Twitter ========================================== --}}
                                <div class="cantidad_publicaciones_widget twitter">
                                    <a class="cantidad-publicaciones twitter black-box tooltipster"
                                        title="{{ (empty($pubXInfluXCampTwitter) ? 0 : $pubXInfluXCampTwitter->cantidad_publicaciones) . ' Publicaciones en Twitter' }}">
                                        {{ empty($pubXInfluXCampTwitter) ? 0 : $pubXInfluXCampTwitter->cantidad_publicaciones }}
                                    </a>
                                    <span class="btn-cantidad-publicaciones twitter btn-red">
                                        <span><i class="fa fa-twitter text-black fa-2x"></i></span>
                                    </span>
                                    @if (!$campana->getUserOwner()->inEntornoTelevisa())
                                        <span class="precio-publicaciones">$
                                            {{ number_format(doubleval(empty($pubXInfluXCampTwitter) ?
                                                (empty($influenciador->perfilTwitter) ? 0 : $influenciador->perfilTwitter->precio_publicacion) :
                                                $pubXInfluXCampTwitter->calcPrecioPublicaciones())) }}
                                            MXN</span>
                                    @endif
                                </div>
                                {{-- =================================== Facebook ======================================= --}}
                                @if (!empty($influenciador->perfilFacebook))
                                    <div class="cantidad_publicaciones_widget facebook">
                                        <a class="cantidad-publicaciones facebook black-box tooltipster"
                                            title="{{ (empty($pubXInfluXCampFacebook) ? 0 : $pubXInfluXCampFacebook->cantidad_publicaciones) . ' Publicaciones en Facebook' }}">
                                                {{ empty($pubXInfluXCampFacebook) ? 0 : $pubXInfluXCampFacebook->cantidad_publicaciones }}
                                        </a>
                                        <span class="btn-cantidad-publicaciones facebook btn-red">
                                            <span><i class="fa fa-facebook text-black fa-2x"></i></span>
                                        </span>
                                        @if (!$campana->getUserOwner()->inEntornoTelevisa())
                                            <span class="precio-publicaciones">$
                                                {{ number_format(doubleval(empty($pubXInfluXCampFacebook) ?
                                                    (empty($influenciador->perfilFacebook) ? 0 : $influenciador->perfilFacebook->precio_publicacion) :
                                                    $pubXInfluXCampFacebook->calcPrecioPublicaciones())) }}
                                                MXN</span>
                                        @endif
                                    </div>
                                @endif
                                {{-- ========================================================================== --}}
                            </div>

                            <div class="col-xs-2 col-sm-2 col-md-2 container-opciones-posts">
                                <button type="button" class="btn btn-red-circle no-loading btn-chat" data-toggle="tooltip" title="Conversar"
                                    data-influenciador="{{ $influenciador->id }}"
                                    data-usuario="{{ $influenciador->usuario->id }}"
                                    data-campana="{{ $campana->id }}">
                                    <i class="fa fa-comments-o fa-lg"></i>
                                    @php
                                        $mensajesNoLeidos = App\Models\Mensaje::getMensajesUsuarioCampana($campana->id ,$influenciador->usuario->id, 0);
                                    @endphp
                                    @if (count($mensajesNoLeidos) > 0)
                                        <span class="badge bg-red">{{ count($mensajesNoLeidos) }}</span>
                                    @endif
                                </button>
                                @php
                                    $publicaciones = $influenciador->publicacionesCampana($campana->id);
                                    $isAprobadoInfluencer = true;
                                    $todasPublicacionesTienenFechaHora = true;
                                @endphp

                                {{--
                                @if ($isEsperandoAprobacionInfluencer)
                                    <a class="btn btn-red-circle no-loading" id="btn-aprobar-posts" data-toggle="tooltip" title="Aprobar"
                                        data-campana="{{ $campana->id }}">
                                        <i class="fa fa-check fa-lg"></i>
                                    </a>
                                @endif
                                 --}}
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-2 col-sm-2 col-md-1"></div>
                    <div class="col-xs-10 col-sm-10 col-md-11 blue-box container-list-posts">
                        @foreach ($publicaciones as $publicacion)
                            {{-- ====== El botón de actualizar foto se mostrara mientras tenga por lo menos una publicación pendiente de aprobar ====== --}}
                            @php
                                $isAprobadoInfluencer = $isAprobadoInfluencer && !$publicacion->isEsperandoAprobacionInfluencer();
                                $todasPublicacionesTienenFechaHora = $todasPublicacionesTienenFechaHora && !empty($publicacion->fecha_hora_publicacion);
                            @endphp

                            @if (!empty($publicacion->fecha_hora_publicacion))
                                <div class="row post-container {!! !$publicacion->isEsperandoAprobacionInfluencer() ? 'aprobado' : '' !!}" data-post="{{ $publicacion->id }}" data-redsocial="{{ $publicacion->red_social_id }}" data-estado="{{ $publicacion->estado }}">
                                    {{-- ====== Icono Estado de Publicación ====== --}}
                                    <div class="col-xs-1 col-sm-1 col-md-1">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="background-color: transparent; border: 0;">
                                                <span data-post="{{ $publicacion->id }}" class="fa-stack fa-lg tooltipster
                                                {!! $publicacion->isEsperandoAprobacionInfluencer() ? ' text-lightergray btn-aprobar-publicacion btn animated infinite pulse" title="Pendiente de Aprobación"' : (
                                                    $publicacion->isAprobadoInfluencer() ? ' text-black" title="Aprobado por Influenciador"' : (
                                                        $publicacion->isAprobadoPorPublicar() ? ' text-black" title="Aprobado por Agencia"' : (
                                                            $publicacion->isAprobadoPublicado() ? ' text-red" title="Publicado"' : (
                                                                $publicacion->isErrorPublicar() ? '" title=\'Error al Publicar: '.$publicacion->error_publicar.'\'' : ''
                                                            )
                                                        )
                                                    )
                                                ) !!}>
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-stack-1x fa-inverse fa-{!! $publicacion->isEsperandoAprobacionInfluencer() ? 'hourglass-o btn-red-rounded animated infinite zoomIn' : (
                                                        $publicacion->isAprobadoInfluencer() ? 'thumbs-o-up' : (
                                                            $publicacion->isAprobadoPorPublicar() ? 'check' : (
                                                                $publicacion->isAprobadoPublicado() ? 'check' : (
                                                                    $publicacion->isErrorPublicar() ? 'close text-red error' : ''
                                                                )
                                                            )
                                                        )
                                                    ) !!}"></i>
                                                </span>
                                            </span>
                                            <div class="form-control float-initial" style="visibility: hidden; padding: 0; margin: 0; width: 0;">
                                                <p>&nbsp;</p><p>&nbsp;</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="input-group container-file-input">
                                            {{-- ====== Icono de la Red Social ====== --}}
                                            <span class="input-group-addon tooltipster {!! $publicacion->isEsperandoAprobacionInfluencer() ? ' bg-lightergray" title="Pendiente de Aprobación"' : (
                                                    $publicacion->isAprobadoInfluencer() ? '" title="Aprobado por Influenciador"': (
                                                        $publicacion->isAprobadoPorPublicar() ? '" title="Aprobado por Agencia"': (
                                                            $publicacion->isAprobadoPublicado() ? ' icon-red" title="Publicado"' : (
                                                                $publicacion->isErrorPublicar() ? '" title=\'Error al Publicar: '.$publicacion->error_publicar.'\'' : ''
                                                            )
                                                        )
                                                    )
                                                ) !!}">
                                                <i class="fa fa-{!! \App\Models\RedSocial::getRedSocialText($publicacion->red_social_id); !!} fa-2x {!! $publicacion->isEsperandoAprobacionInfluencer() ? ' text-white"' : (
                                                    $publicacion->isAprobadoInfluencer() ? ' text-white"': (
                                                        $publicacion->isAprobadoPorPublicar() ? ' text-white"': (
                                                            $publicacion->isAprobadoPublicado() ? '"' : (
                                                                $publicacion->isErrorPublicar() ? ' text-red error"' : ''
                                                            )
                                                        )
                                                    )
                                                ) !!}></i>
                                            </span>

                                            <span data-toggle="tooltip" title="Post" class="form-control cursor-not-allowed" style="min-height: 95px;">
                                                {{ $publicacion->contenido == '' ? $campana->hashtags : $publicacion->contenido }}
                                            </span>
                                        </div>

                                        <div id="errorPublicacion_{{ $publicacion->id }}" class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                    </div>

                                    <div class="col-xs-2 col-sm-2 col-md-2 text-center" style="padding: 0;">
                                        <div class="kv-avatar center-block">
                                            <input type="file" name="archivo_multimedia"
                                                class="bootstrap-fileinput file-loading {!! !$publicacion->isEsperandoAprobacionInfluencer() ? 'aprobado' : '' !!}"
                                                data-publicacion="{{ $publicacion->id }}"
                                                data-campana="{{ $campana->id }}"
                                                data-redsocial="{{ $publicacion->red_social_id }}"
                                                {!! $publicacion->getArchivoMultimedia() ? '
                                                    data-multimedia="'.asset($publicacion->getArchivoMultimedia()).'"
                                                    data-type="'.$publicacion->getTypeMultimedia().'"
                                                    data-size="'.$publicacion->getSizeMultimedia().'"
                                                    data-filetype="'.$publicacion->getFileTypeMultimedia().'"
                                                    '
                                                : '' !!}>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lg fa-calendar"></i></span>
                                            <span class="form-control cursor-not-allowed" data-toggle="tooltip" placeholder="Fecha Publicación" title="Fecha publicación">
                                                @datetime_short($publicacion->fecha_hora_publicacion)
                                            </span>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lg fa-clock-o"></i></span>
                                            <span class="form-control cursor-not-allowed" data-toggle="tooltip" placeholder="Hora Publicación" title="Hora publicación">
                                                @time_long($publicacion->fecha_hora_publicacion)
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach

                        @if (!$isAprobadoInfluencer && $todasPublicacionesTienenFechaHora)
                            <div class="col-xs-2 col-sm-2 col-md-1"></div>
                            <div class="col-xs-10 col-sm-10 col-md-11 text-right">
                                <a class="btn btn-red-rounded no-loading btn-send-posts-influencer"
                                    data-influenciador="{{ $influenciador->id }}"
                                    data-campana="{{ $campana->id }}">
                                    Actualizar Fotos
                                </a>
                            </div>
                        @endif

                        @if (!$todasPublicacionesTienenFechaHora)
                            <h4 class="text-center">Publicaciones pendientes de creación</h3>
                        @endif

                    </div>
                </div>

                {{-- Modal Container for Bootstrap File Input --}}
                <div id="kvFileinputModal" class="file-zoom-dialog modal fade" tabindex="-1" aria-labelledby="kvFileinputModalLabel">
                </div>

                @include('common.chat')
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script src="{{ asset('node_modules/bootstrap-fileinput/js/fileinput.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/bootstrap-fileinput/js/locales/es.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/chat.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/campana_posts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/uploadfile_post.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/influenciador_posts.js') }}" type="text/javascript"></script>
@endpush
