<div class="col-md-12 col-xs-12 row-top-content" >
    <img src="{{ asset('img/campana/twitter.png') }}" class="img-responsive" width="38" height="38"/>
</div>
    @php
        $autolink = Twitter_Autolink::create();
    @endphp
<div class="col-md-12" syle="">
    @if (!empty($model->perfilTwitter))
        @foreach($model->perfilTwitter->topTweetsInteraction()->get() as $topTweet)

            <div class="col-md-2_5 row-top-content" style="background-color: white; font-size: 12px; ">
                @if($topTweet->hasMultimedia())
                    @if($topTweet->isImage())
                    <div class="foto-contenido">
                            <div class="img-container img-div" style="height: 212px;  background-image: url({{$topTweet->multimedia}});">
                            <div id="numero">{{ $loop->index + 1 }}</div>
                            <div id="fecha">{{ \App\Helpers\DateConverter::convert($topTweet->fecha) }}</div>
                            <p>{!! $autolink->autoLink($topTweet->texto) !!}</p>
                        </div>
                    </div>

                    @endif

                    @if($topTweet->isVideo())
                        <a class="fancy" href="{{ $topTweet->multimedia }}"><img src="{{ asset('img/logo.png') }}" class="desvanecer img-responsive" alt="Imagen responsive"></a>
                    @endif

                @else
                <div class="foto-contenido">
                    <div class="img-container img-div" style="height: 212px;  background-image: url({{$model->imgPerfil}});">
                        <div id="numero">{{ $loop->index + 1 }}</div>
                        <div id="fecha">{{ \App\Helpers\DateConverter::convert($topTweet->fecha) }}</div>
                        <p>{!! $autolink->autoLink($topTweet->texto) !!}</p>
                    </div>
                </div>
                 @endif
                <!--<div class="row " >-->
                    <div class="col-md-12 col-xs-12 box-below-content" >
                        <div class="row text-center">
                            <div class="col-md-5 col-xs-5">
                                Alcance<p>
                                {{ \App\Helpers\NumbersToStringConverter::toThousands($topTweet->total_followers_rt + $model->perfilTwitter->cantidad_seguidores) }}
                            </div>
                            <div class="col-md-4 col-xs-3">
                                RT <p>
                               {{ \App\Helpers\NumbersToStringConverter::toThousands($topTweet->retweets) }}
                            </div>
                            <div class="col-md-3 col-xs-1">
                                FV<p>
                               {{ \App\Helpers\NumbersToStringConverter::toThousands($topTweet->favorites) }}
                            </div>
                        </div>
                    </div>
                <!--</div>-->
            </div>
        @endforeach
            <div class="col-md-12 col-xs-12">
                <p class="info text-small text-right">{{ trans('influencer.last_month_data') }}</p>
            </div>
     @else
      <div class="col-md-11 row-top-content" style="background-color: white; font-size: 12px;">No hay datos para mostrar</div>
    @endif

</div>

