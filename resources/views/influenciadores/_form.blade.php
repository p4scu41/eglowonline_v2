@extends('layouts.' . config('app.layout'))

@section('title', $title)

@push('scripts-head')
    <script type="text/javascript">
        var urlPalabraClaveSearchByText  = '{{ url('palabrasclaves/searchbytext') }}',
            urlInfluenciadoresIndex  = '{{ url('influenciadores') }}';
    </script>
@endpush

@section('content')
    @if (Auth::user()->isAdministrador() || Auth::user()->isAgencia())
        <p>
            <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
                <span class="glyphicon glyphicon-arrow-left"></span> Regresar
            </a>
        </p>
    @endif
    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        <li role="presentation" class="active">
            <h2>
                <a href="#tab_datos_generales" id="datos-generales-tab" role="tab" data-toggle="tab" aria-expanded="true">
                    <i class="fa fa-file-text-o"></i>
                    Datos Generales
                </a>
            </h2>
        </li>
        <li role="presentation" class="">
            <h2>
                <a href="#tab_redes_sociales" id="redes-sociales-tab" role="tab" data-toggle="tab" aria-expanded="false">
                    <i class="fa fa-users"></i>
                    Redes Sociales
                </a>
            </h2>
        </li>
        <li role="presentation" class="">
            <h2>
                <a href="#tab_marcas" role="tab" id="marcas-tab" data-toggle="tab"
                    aria-expanded="false">
                    <i class="fa fa-paper-plane-o"></i>
                    Marcas
               </a>
            </h2>
        </li>

        <li role="presentation" class="">
            <h2>
                <a href="#tab_facturas" role="tab" id="facturas-tab" data-toggle="tab"
                    aria-expanded="false">
                    <i class="fa fa-file"></i>
                    Facturación
               </a>
            </h2>
        </li>
    </ul>

    <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="tab_datos_generales"
            aria-labelledby="datos-generales-tab">
            @include($response_info['resource'] . '._form_datos_generales', [
                'config_form' => $config_form,
                'catalogs'    => $catalogs,
            ])
        </div>

        <div role="tabpanel" class="tab-pane fade" id="tab_redes_sociales"
            aria-labelledby="redes-sociales-tab">
            @include($response_info['resource'] . '._form_redes_sociales', [
                'model'    => $model,
                'catalogs' => $catalogs,
            ])
        </div>

        <div role="tabpanel" class="tab-pane fade" id="tab_marcas" aria-labelledby="marcas-tab">
            @include($response_info['resource'] . '._form_marcas', [
                'model'    => $model,
                'catalogs' => $catalogs,
            ])
        </div>

        <div role="tabpanel" class="tab-pane fade" id="tab_facturas" aria-labelledby="facturas-tab">
            @include($response_info['resource'] . '._form_facturacion', [
                'model'    => $model,
                'catalogs' => $catalogs,
            ])
        </div>
    </div>

    <script id="palabra-clave-item-hbs" type="text/x-handlebars-template">
        @include($response_info['resource'] . '/palabrasclave_tpl')
    </script>

    @include('common.loading')
@endsection

@push('scripts')
    <script src="{{ asset('node_modules/handlebars/dist/handlebars.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/twitter-text/twitter-text.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/tagsinput/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('js/influenciador_form.js') }}" type="text/javascript"></script>
@endpush
