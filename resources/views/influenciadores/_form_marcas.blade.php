<h2>GESTIONA LAS MARCAS DEL INFLUENCER</h2>

<div class="row" id="container-marcas">
    @foreach ($catalogs['relacion_marca'] as $relacion_marca)
        <div class="col-xs-12 col-sm-12 col-md-5 box-border-gray" style="margin: 10px; min-height: 200px;">
            <h2>{{ $relacion_marca->descripcion }}</h2>

            <div class="form-group col-xs-12 col-sm-12 col-md-12 float-initial">
                <div class="input-group">
                    <span class="input-group-addon"></span>
                    <div class="form-control float-initial">
                        <select class="relacion_marca_search"
                            data-id="{{ $relacion_marca->id }}"style="width: 100%;"></select>
                    </div>
                </div>
            </div>

            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                <div id="palabra-clave-box">
                    <ul id="relacion_marca_{{ $relacion_marca->id }}"
                        class="container-item-list relacion_marca"
                        data-id="{{ $relacion_marca->id }}">
                        @foreach ($model->palabrasClaveOtros([$relacion_marca->id]) as $palabra_clave)
                            @include($response_info['resource'] . '/palabrasclave_tpl', [
                                'id'   => $palabra_clave->palabra_clave_id,
                                'text' => $palabra_clave->palabraClave->palabra,
                            ])
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endforeach

    <div class="text-center col-xs-12 col-sm-12 col-md-12">
        <a class="btn btn-red-rounded btn-next-to no-loading" data-tab="#facturas-tab">
            Siguiente
        </a> &nbsp;

        @if ($model->exists)
            <a class="btn btn-red-rounded btn-save-influencer">
                <span class="glyphicon glyphicon-floppy-save"></span> Guardar
            </a>
        @endif
    </div>

</div>
