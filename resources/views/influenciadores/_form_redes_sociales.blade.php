<h2>GESTIONA LAS REDES SOCIALES DEL INFLUENCER</h2>

<div class="row">
    @php
        $redesSocialesInfluencer = $model->redesSociales();
    @endphp

    @foreach ($catalogs['red_social'] as $red_social)
        <!-- start accordion -->
        <div class="accordion red_social_tbl" id="tabla_red_social_{{ $red_social->id }}" role="tablist" aria-multiselectable="true" data-id="{{ $red_social->id }}">
            <div class="panel">
                <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#tabla_red_social_{{ $red_social->id }}"
                    href="#red_social_{{ $red_social->id }}" aria-expanded="false" aria-controls="red_social_{{ $red_social->id }}"
                    data-toggle="tooltip" title="Click para expandir">
                    <span class="circle {{ isset($redesSocialesInfluencer[$red_social->id]) ? 'bg-navy-blue' : 'bg-gray' }}"><i class="fa fa-{{ strtolower($red_social->nombre) }}"></i></span>
                    <h4>{{ $red_social->nombre }}</h4>
                    <span><i class="fa fa-chevron-down"></i></span>
                </a>
                <div class="clearfix"></div>
                <div id="red_social_{{ $red_social->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        @if (strtolower($red_social->nombre) === 'facebook')
                            @php
                                $perfilFacebook = $model->perfilFacebook;
                                $pages          = !is_null($perfilFacebook) ? $perfilFacebook->pages()->get() : null;
                            @endphp
                            @if(is_null($perfilFacebook))
                                <h5>Selecciona iniciar sesión con Faceboook en la pantalla de login para generar un perfil</h5>
                            @elseif ($pages->count() > 0 && (int) $perfilFacebook->selectedPage === 0)
                                <h5>Hemos detectado que cuenta con más de una página de Facebook. Por favor, indiquenos con cuál trabajaremos:</h5>
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                        @foreach ($pages as $page)
                                            <div class="form-group">
                                                <label>
                                                    <input type="radio" name="pageId" value="{{ $page->id }}" data-id="{{ $page->id }}" data-rule-required="true">&nbsp;&nbsp;<img src="{{ $page->profile_image_url }}"> {{ $page->name }} ({{ $page->engagement }} likes)
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <input type="number" name="precio_publicacion" value="{{ isset($redesSocialesInfluencer[$red_social->id]) ? $redesSocialesInfluencer[$red_social->id]['precio_publicacion'] : '' }}" data-redsocial="{{ strtolower($red_social->nombre) }}" data-toggle="tooltip" placeholder="Precio por publicación" title="Precio por publicación" class="form-control">
                                            <input type="hidden" id="selectedPage" value="0">
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <input type="text" name="screen_name"
                                            value="{{ isset($redesSocialesInfluencer[$red_social->id]) ? (!empty($redesSocialesInfluencer[$red_social->id]['page_name']) ? $redesSocialesInfluencer[$red_social->id]['page_name'] : $redesSocialesInfluencer[$red_social->id]['screen_name']) : '' }}"
                                            data-redsocial="{{ strtolower($red_social->nombre) }}"
                                            data-toggle="tooltip" placeholder="Username" title="Escriba su screen name" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <p class="form-control-static" data-toggle="tooltip" title="Cantidad de seguidores">
                                            {!!
                                                isset($redesSocialesInfluencer[$red_social->id]) ? '<b>Cantidad de seguidores: </b>' .
                                                \App\Helpers\Helper::formatNumber($redesSocialesInfluencer[$red_social->id]['cantidad_seguidores']) :
                                                '-'
                                            !!}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <input type="number" name="precio_publicacion" value="{{ isset($redesSocialesInfluencer[$red_social->id]) ? $redesSocialesInfluencer[$red_social->id]['precio_publicacion'] : '' }}" data-redsocial="{{ strtolower($red_social->nombre) }}" data-toggle="tooltip" placeholder="Precio por publicación" title="Precio por publicación" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-1">
                                        <button class="btn btn-danger btn-sm no-loading btn-delete-red-social" data-toggle="tooltip" title="Eliminar" data-id="{{ $red_social->id }}">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                        <input type="hidden" id="selectedPage" value="1">
                                        <input type="hidden" name="delete_red_social_id" value="">
                                    </div>
                                </div>
                            @endif
                        @else
                            <div class="row">
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="screen_name" value="{{ isset($redesSocialesInfluencer[$red_social->id]) ?
                $redesSocialesInfluencer[$red_social->id]['screen_name'] : '' }}" data-redsocial="{{ strtolower($red_social->nombre) }}" data-toggle="tooltip" placeholder="{{ strtolower($red_social->nombre) === 'twitter' ? 'Escriba su usuario junto con @' : 'Username' }}" title="Escriba su screen name" class="form-control">
                                    </div>
                                </div>

                                @if (strtolower($red_social->nombre) != 'twitter')
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <input type="number" name="cantidad_seguidores" value="{{ isset($redesSocialesInfluencer[$red_social->id]) ? $redesSocialesInfluencer[$red_social->id]['cantidad_seguidores'] : '' }}" min="0" data-redsocial="{{ strtolower($red_social->nombre) }}" data-toggle="tooltip" placeholder="Cantidad de seguidores" title="Cantidad de seguidores" class="form-control">
                                        </div>
                                    </div>
                                @else
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <p class="form-control-static" data-toggle="tooltip" title="Cantidad de seguidores">
                                            {!!
                                                isset($redesSocialesInfluencer[$red_social->id]) ? '<b>Cantidad de seguidores: </b>' .
                                                \App\Helpers\Helper::formatNumber($redesSocialesInfluencer[$red_social->id]['cantidad_seguidores']) :
                                                '-'
                                            !!}
                                            </p>
                                        </div>
                                    </div>
                                @endif

                                <div class="col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <input type="number" name="precio_publicacion" value="{{ isset($redesSocialesInfluencer[$red_social->id]) ? $redesSocialesInfluencer[$red_social->id]['precio_publicacion'] : '' }}" data-redsocial="{{ strtolower($red_social->nombre) }}" data-toggle="tooltip" placeholder="Precio por publicación" title="Precio por publicación" class="form-control">
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-1">
                                    <button class="btn btn-danger btn-sm no-loading btn-delete-red-social" data-toggle="tooltip" title="Eliminar" data-id="{{ $red_social->id }}">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                    <input type="hidden" name="delete_red_social_id" value="">
                                </div>

                                @if (!isset($redesSocialesInfluencer[$red_social->id]))
                                    <div class="col-sm-12 col-md-12 text-center">
                                        <a class="btn bg-eglow" data-toggle="tooltip" title="" href="{{ url('influenciadores/vincular/' . strtolower($red_social->nombre)) }}"
                                            data-original-title="Vincular Cuenta de {{ $red_social->nombre }}">
                                            <span><i class="fa fa-{!! strtolower($red_social->nombre) !!}"></i></span>
                                            <span>Vincular Cuenta</span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- end of accordion -->
    @endforeach

    <div class="text-center col-xs-12 col-sm-12 col-md-12">
        <a class="btn btn-red-rounded btn-next-to no-loading" data-tab="#marcas-tab">
            Siguiente
        </a> &nbsp;

        @if ($model->exists)
            <a class="btn btn-red-rounded btn-save-influencer">
                <span class="glyphicon glyphicon-floppy-save"></span> Guardar
            </a>
        @endif
    </div>

</div>
