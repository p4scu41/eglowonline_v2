<!-- Top section -->
@php
    $paises      = $model->distribucion_paises;
    $lastWeek    = false;
    $mejorDia    = empty($model->perfilTwitter) ? null : $model->perfilTwitter->bestDayToPublish($lastWeek);
    $mejorHora   = empty($model->perfilTwitter) ? null : $model->perfilTwitter->bestHourToPublish($lastWeek);
    $professions = $model->getProfessions();
@endphp
<div id="influencer-profile-background" class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="col-app">
                <p class="text-center">
                    <img src="{{ asset('img/campana/twitter.png') }}" class="img-medium">
                    <img src="{{ asset('img/campana/facebook_gris.png') }}" class="img-medium">
                    <img src="{{ asset('img/campana/instagram_gris.png') }}" class="img-medium">
                    <img src="{{ asset('img/campana/youtube_gris.png') }}" class="img-medium">
                </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-xs-12 padding-right-none">
            <div class="col-app">
                <h5 class="text-center"><img src="{{ asset('img/influenciador_perfil/crecimiento_followers.png') }}"> {{ trans('influencer.monthly-growing') }}</h5>
                <div id="graph-followers-monthly-growing" style="width: 100%; height: 244px; top: -24px" data-id="{{ $model->id }}" data-url="{{ url('influenciadores/' . $model->id . '/graficas/crecimiento-followers') }}"></div>
                <p class="info text-small text-right">{{ trans('influencer.last_week_data') }}</p>
            </div>
        </div>

        <div class="col-sm-6 col-xs-12 padding-left-none">
            <div class="col-app">
                <h5 class="text-center"><img src="{{ asset('img/influenciador_perfil/alcances.png') }}"> {{ trans('influencer.reachment') }}</h5>
                <div id="graph-reachment" style="width: 100%; height: 244px; top: -25px;" data-id="{{ $model->id }}" data-real="{{ $model->alcance_real }}" data-potencial="{{ $model->alcance_potencial }}"></div>
                <p class="info text-small text-right">{{ trans('influencer.last_week_data') }}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="col-app">
                <h5 class="text-center"><img src="{{ asset('img/influenciador_perfil/metricas.png') }}"> {{ trans('influencer.metrics') }}</h5>
                <table width="100%">
                    <tr class="text-center">
                        <td>
                            <h5 class="text-center">{{ trans('influencer.engagement_rate') }}</h5>
                            <div class="box-light text-center">
                                <span>{{ number_format($model->getEngagement($lastWeek), 2) }}%</span>
                                @php
                                    $engagementBehaviour = $model->engagementGrow();
                                    $chevronIcon         = '';
                                @endphp
                                @if ($engagementBehaviour['type'] !== '' && $engagementBehaviour['type'] === 'increment')
                                    @php
                                        $chevronIcon = '<i class="fa fa-chevron-up"></i>';
                                    @endphp
                                @elseif ($engagementBehaviour['type'] !== '' && $engagementBehaviour['type'] === 'decrement')
                                    @php
                                        $chevronIcon = '<i class="fa fa-chevron-down"></i>';
                                    @endphp
                                @endif
                                <p class="text-small">{{ number_format($engagementBehaviour['percent'], 2) }}% {!! $chevronIcon !!}</p>
                            </div>
                        </td>

                        <td>
                            <h5 class="text-center">{{ trans('influencer.interactions_be_1000') }}</h5>
                            <div class="box-light text-center">
                                <span>{{ number_format($model->getInteractions1000($lastWeek), 2) }}</span>
                                @php
                                    $interactions1000 = $model->interactions1000Grow();
                                    $chevronIcon      = '';
                                @endphp
                                @if ($interactions1000['type'] !== '' && $interactions1000['type'] === 'increment')
                                    @php
                                        $chevronIcon = '<i class="fa fa-chevron-up"></i>';
                                    @endphp
                                @elseif ($interactions1000['type'] !== '' && $interactions1000['type'] === 'decrement')
                                    @php
                                        $chevronIcon = '<i class="fa fa-chevron-down"></i>';
                                    @endphp
                                @endif
                                <p class="text-small">{{ number_format($interactions1000['percent'], 2) }}% {!! $chevronIcon !!}</p>
                            </div>
                        </td>
                        <td>
                            <h5 class="text-center">{{ trans('influencer.conversation_rate') }}</h5>
                            <div class="box-light text-center">
                                <span>{{ number_format($model->getConversationRate($lastWeek), 2) }}%</span>
                                @php
                                    $conversationGrow = $model->conversationGrow();
                                    $chevronIcon      = '';
                                @endphp
                                @if ($conversationGrow['type'] !== '' && $conversationGrow['type'] === 'increment')
                                    @php
                                        $chevronIcon = '<i class="fa fa-chevron-up"></i>';
                                    @endphp
                                @elseif ($conversationGrow['type'] !== '' && $conversationGrow['type'] === 'decrement')
                                    @php
                                        $chevronIcon = '<i class="fa fa-chevron-down"></i>';
                                    @endphp
                                @endif
                                <p class="text-small">{{ number_format($conversationGrow['percent'], 2) }}% {!! $chevronIcon !!}</p>
                            </div>
                        </td>
                        <td>
                            <h5 class="text-center">{{ trans('influencer.amplification_rate') }}</h5>
                            <div class="box-light text-center">
                                <span>{{ number_format($model->getAmplificationRate($lastWeek), 2) }}%</span>
                                @php
                                    $amplificationGrow = $model->amplificationGrow();
                                    $chevronIcon      = '';
                                @endphp
                                @if ($amplificationGrow['type'] !== '' && $amplificationGrow['type'] === 'increment')
                                    @php
                                        $chevronIcon = '<i class="fa fa-chevron-up"></i>';
                                    @endphp
                                @elseif ($amplificationGrow['type'] !== '' && $amplificationGrow['type'] === 'decrement')
                                    @php
                                        $chevronIcon = '<i class="fa fa-chevron-down"></i>';
                                    @endphp
                                @endif
                                <p class="text-small">{{ number_format($amplificationGrow['percent'], 2) }}% {!! $chevronIcon !!}</p>
                            </div>
                        </td>
                        <td>
                            <h5 class="text-center">{{ trans('influencer.echo_rate') }}</h5>
                            <div class="box-light text-center">
                                <span>{{ number_format($model->getEchoRate($lastWeek), 2) }}%</span>
                                @php
                                    $echoGrow    = $model->echoGrow();
                                    $chevronIcon = '';
                                @endphp
                                @if ($echoGrow['type'] !== '' && $echoGrow['type'] === 'increment')
                                    @php
                                        $chevronIcon = '<i class="fa fa-chevron-up"></i>';
                                    @endphp
                                @elseif ($echoGrow['type'] !== '' && $echoGrow['type'] === 'decrement')
                                    @php
                                        $chevronIcon = '<i class="fa fa-chevron-down"></i>';
                                    @endphp
                                @endif
                                <p class="text-small">{{ number_format($echoGrow['percent'], 2) }}% {!! $chevronIcon !!}</p>
                            </div>
                        </td>
                    </tr>
                </table>
                <br>
                <p class="info text-small text-right">{{ trans('influencer.last_week_data') }}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-sm-6 col-xs-12 padding-right-none">
            <div class="col-app">
                <h5 class="text-center">{{ trans('influencer.best_day_post') }}</h5>
                <table class="table-days-short">
                    <tr>
                        <td {!! !empty($mejorDia) && $mejorDia->dia_semana === trans('influencer.monday') ? 'class="best-day"' : '' !!}>{{ trans('influencer.monday_short') }}</td>
                        <td {!! !empty($mejorDia) && $mejorDia->dia_semana === trans('influencer.tuesday') ? 'class="best-day"' : '' !!}>{{ trans('influencer.tuesday_short') }}</td>
                        <td {!! !empty($mejorDia) && $mejorDia->dia_semana === trans('influencer.wednesday') ? 'class="best-day"' : '' !!}>{{ trans('influencer.wednesday_short') }}</td>
                        <td {!! !empty($mejorDia) && $mejorDia->dia_semana === trans('influencer.thursday') ? 'class="best-day"' : '' !!}>{{ trans('influencer.thursday_short') }}</td>
                        <td {!! !empty($mejorDia) && $mejorDia->dia_semana === trans('influencer.friday') ? 'class="best-day"' : '' !!}>{{ trans('influencer.friday_short') }}</td>
                        <td {!! !empty($mejorDia) && $mejorDia->dia_semana === trans('influencer.saturday') ? 'class="best-day"' : '' !!}>{{ trans('influencer.saturday_short') }}</td>
                        <td {!! !empty($mejorDia) && $mejorDia->dia_semana === trans('influencer.sunday') ? 'class="best-day"' : '' !!}>{{ trans('influencer.sunday_short') }}</td>
                    </tr>
                </table>

                <h5 class="text-center">{{ trans('influencer.best_hour_post') }}</h5>
                <div class="box-light box-hour">
                    <div class="box-hour-clock">
                        <p>{{ !empty($mejorHora) ? $mejorHora->hora : '-' }}</p>
                    </div>
                </div>
                <br>
                <p class="info text-small text-right">{{ trans('influencer.last_month_data') }}</p>
                <br>
            </div>
        </div>

        <div class="col-lg-8 col-sm-6 col-xs-12 padding-left-none">
            <div class="col-app">
                <h5 class="text-center"><img src="{{ asset('img/influenciador_perfil/interacciones.png') }}"> {{ trans('influencer.interactions') }}</h5>
                <div id="graph-interactions" style="width: 100%; height: 306px;" data-id="{{ $model->id }}" data-url="{{ url('influenciadores/' . $model->id . '/graficas/interacciones-mes') }}"></div>
                <span><span id="total" style="float:left;"></span><p class="info text-small text-right">{{ trans('influencer.last_month_data') }}</p></span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-sm-4 col-xs-12 padding-right-none">
            <div class="col-app">
                <h5 class="text-center">{{ trans('influencer.country_distribution') }}</h5>
                <div id="graph-country-distribution" style="width: 100%; height: 250px;" data-id="{{ $model->id }}" data-texts="{{ json_encode($paises['paises']) }}" data-values="{{ json_encode($paises['cantidades']) }}"></div>

                <p class="info text-small text-right">{{ trans('influencer.last_week_data') }}</p>
                <p class="info text-small text-right">{{ trans('influencer.base_on_sample', ['percent' => $model->porcentaje_muestra]) }}</p>
            </div>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 padding-right-none padding-left-none">
            <div class="col-app" style="height: 329px;">
                <h5 class="text-center">{{ trans('influencer.gender_distribution') }}</h5>
                @php
                    $total = $model->totalHombres + $model->totalMujeres;
                @endphp
                <div class="row">
                    <div class="col-sm-6 col-xs-12 text-right">
                        <img src="{{ asset('img/campana/masculino.png') }}" class="img-large">
                    </div>
                    <div class="col-sm-6 col-xs-12 text-left">
                        <p class="important">{{ $total !== 0 ? number_format(($model->totalHombres * 100) / $total) : 0 }}%</p>
                        <p class=""> &nbsp; &nbsp; {{ trans('influencer.men') }}</p>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-6 col-xs-12 text-right">
                        <img src="{{ asset('img/campana/femenino.png') }}" class="img-large">
                    </div>
                    <div class="col-sm-6 col-xs-12 text-left">
                        <p class="important">{{ $total !== 0 ? number_format(($model->totalMujeres * 100) / $total) : 0 }}%</p>
                        <p class="">{{ trans('influencer.women') }}</p>
                    </div>
                </div>
                <br>
                <br>

                <p class="info text-small text-right">{{ trans('influencer.last_week_data') }}</p>
                <p class="info text-small text-right">{{ trans('influencer.base_on_sample', ['percent' => $model->porcentaje_muestra]) }}</p>
            </div>
        </div>

        <div class="col-lg-5 col-md-4 col-sm-4 col-xs-12 padding-left-none">
            <div class="col-app">
                <h5 class="text-center"><img src="{{ asset('img/influenciador_perfil/profesion.png') }}"> {{ trans('influencer.professions') }}</h5>
                <div id="graph-professions" style="width: 100%; height: 264px;" data-id="{{ $model->id }}" data-elements="{{ json_encode($professions) }}"></div>
                <p class="info text-small text-right">{{ trans('influencer.last_week_data') }}</p>
            </div>
        </div>
    </div>
</div>