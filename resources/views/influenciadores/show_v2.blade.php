@extends('layouts.' . config('app.layout') )

@push('styles')
    <link rel="stylesheet" href="{{ asset('js/plugins/fancyBox/jquery.fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/jQCloud/dist/jqcloud.css') }}">

@endpush

@push('scripts-head')
    <script>
        var id_influencer  = '{{ empty($model) ? 0 : $model->id }}';
    </script>
@endpush

@section('content')
    <!-- row de pestanas menu -->
    <div class="row main-container-statistics-influencer">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="tab-content ">
                <div role="tabpanel" class="tab-pane active" id="tab-statistics">
                    <ul class="nav nav-tabs light-tab" role="tablist" style="margin: 0 -10px;">
                        <li role="presentation" class="active">
                            <a href="#tab_perfil" class="campaign-tabs" aria-controls="tab-campaign-posts" role="tab" data-toggle="tab">
                                <img src="{{ asset('img/influenciador_perfil/perfil-azul.png?id='.rand()) }}" class="img-small"/>
                                <label>Profile</label>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#tab_estadisticas" class="campaign-tabs" id="profile-statistics" aria-controls="tab-trends" role="tab" data-toggle="tab">
                                <img src="{{ asset('img/influenciador_perfil/estadisticas-azul.png?id='.rand()) }}" class="img-small"/>
                                <label>Statistics</label>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#tab_contenido" class="campaign-tabs" aria-controls="tab-audience" role="tab" data-toggle="tab" >
                                <img src="{{ asset('img/influenciador_perfil/contenido.png?id='.rand()) }}" class="img-small"/>
                                <label>Content</label>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#tab_comunidad" id="showComunidad" class="campaign-tabs" aria-controls="tab-pdf" role="tab" data-toggle="tab" >
                            <img src="{{ asset('img/influenciador_perfil/comunidad.png?id='.rand()) }}" class="img-small"/>
                                 <label>Community</label>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane" id="tab_estadisticas" style="margin: 0px -10px 0 -10px;">
                            @include($response_info['resource'] . '.show_estadisticas_v2')
                        </div>
                        <div role="tabpanel" class="tab-pane active"  id="tab_perfil">
                            @include($response_info['resource'] . '.show_perfil_v2')
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab_contenido" >
                            @include($response_info['resource'] . '.show_contenido_v2')
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab_comunidad" >
                            @include($response_info['resource'] . '.show_comunidad_v2')
                        </div>
                    </div>
                </div>
            </div>
        <!--<div role="tabpanel" style="color: black; ">-->
        <!-- Menu pestañas-->

        <!--</div>-->
        </div>
    </div>
@endsection

@push('scripts')
    <!--<script src="{{ asset('js/chat_support.js') }}"></script>-->
    <!--  <script src="{{ asset('js/plugins/Chart.js/circles.min.js') }}"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script src="{{ asset('js/plugins/Chart.js/circles.min.js') }}"></script>
    <script src="{{ asset('js/plugins/fancyBox/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('js/plugins/jQCloud/dist/jqcloud.js') }}"></script>
    <script src="{{ asset('js/plugins/zingchart/zingchart.min.js') }}"></script>
    <script src="{{ asset('js/plugins/echart/echarts.js') }}"></script>
    <script src="{{ asset('js/influenciador_profile_v2.js') }}"></script>
    <script src="{{ asset('js/influenciador_contenido.js') }}"></script>
    <script src="{{ asset('js/influenciador_comunidad_v2.js') }}"></script>
    <script src="{{ asset('js/influenciador_estadisticas_v2.js') }}"></script>
@endpush