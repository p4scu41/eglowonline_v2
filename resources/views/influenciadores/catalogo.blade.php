@extends('layouts.' . config('app.layout'))

@section('title', $title)

@push('scripts-head')
    <script type="text/javascript">
        var urlInfluenciadorSearchByText     = '{{ url('influenciadores/searchbytext') }}',
            urlInfluenciadorClic             = '{{ url('influenciadores/clic') }}',
            urlPalabraClaveSearchByText      = '{{ url('palabrasclaves/searchbytext') }}',
            urlInfluenciadorAdvancedSearch   = '{{ url('influenciadores/advancedsearch') }}',
            urlSearchPaisByText              = '{{ url('influenciadores/searchpaisbytext') }}';
            //urlSearchLugarNacimientoByText = '{{ url('influenciadores/searchlugarnacimientobytext') }}';
    </script>
@endpush

@section('content')
    @php
        $displaySearch       = $search['foundSearch'] && $search['origin'] === 2 ? ''               : 'display: none;';
        $displayButtonBasic  = $search['foundSearch'] && $search['origin'] === 2 ? 'display: none;' : '';
        $classAdvancedSearch = $search['foundSearch'] && $search['origin'] === 2 ? ''               : 'off';

        $nombreInfluencer = '';
        if ($search['foundSearch']) {
            switch ($search['origin']) {
                case 1:
                    $nombreInfluencer = $search['params']->q;
                    break;

                case 2:
                    $nombreInfluencer = $search['params']->nombre;
                    break;
            }
        }
    @endphp
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <h3 class="title">{{ strtoupper($title) }}</h3>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-8">
                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    <input type="text" data-toggle="tooltip" title="Nombre de la celebridad"
                                    class="form-control" placeholder="Nombre de la celebridad"
                                    id="celebridad-name-search" name="celebridad-name-search" value="{{ $nombreInfluencer }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 text-right">
                            <button type="button" id="btn-basic-search"
                            class="hidden btn btn-red-rounded btn-encuentra-influenciador no-loading" style="{{ $displayButtonBasic }}">ENCUENTRA</button>
                            <button type="button" id="btn-toogle-busqueda-avanzada" class="btn btn-red-circle no-loading">
                                Búsqueda Avanzada
                            </button>
                        </div>
                    </div>

                    <div class="row {{ $classAdvancedSearch }}" id="busqueda-avanzada" style="{{ $displaySearch }} padding-bottom: 20px;">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                <div class="input-group" style="padding-right: 10px;padding-left: 10px;">
                                    <span class="input-group-addon"></span>
                                    <div class="form-control float-initial">
                                        Cantidad de Seguidores en Twitter
                                        <input type="text" id="cantidad_seguidores" name="cantidad_seguidores" data-from="{{ $search['foundSearch'] && $search['origin'] === 2 && is_array($search['params']->cantidad_seguidores) ? $search['params']->cantidad_seguidores[0] : '' }}" data-to="{{ $search['foundSearch'] && $search['origin'] === 2 && is_array($search['params']->cantidad_seguidores) ? $search['params']->cantidad_seguidores[1] : '' }}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                <div class="input-group" style="padding-right: 10px; padding-left: 10px;">
                                    <span class="input-group-addon"></span>
                                    <div class="form-control float-initial">
                                        <div>{{ $labels['rango_etario_id'] }}</div>

                                        <select id="rango_etario_id" name="rango_etario_id" class="selectToList" multiple="true">
                                            @foreach ($catalogs['rango_etario'] as $rango_etario)
                                                <option value="{{ $rango_etario['id'] }}"
                                                    data-imagen="{{ asset($rango_etario['icono']) }}" {{ $search['foundSearch'] && $search['origin'] === 2 && is_array($search['params']->rango_etario_id) && in_array($rango_etario['id'], $search['params']->rango_etario_id) ? 'selected' : '' }}>
                                                        {{ $rango_etario['descripcion'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                <div class="input-group" style="padding-right: 10px; padding-left: 10px;">
                                    <span class="input-group-addon"></span>
                                    <div class="form-control float-initial">
                                        <div>{{ $labels['tipo_influenciador_id'] }}</div>

                                        <select id="tipo_influenciador_id" name="tipo_influenciador_id" class="selectToList" multiple="true">
                                            @foreach ($catalogs['tipo_influenciador'] as $tipo_influenciador)
                                                <option value="{{ $tipo_influenciador['id'] }}"
                                                    data-imagen="{{ asset($tipo_influenciador['icono']) }}" {{ $search['foundSearch'] && $search['origin'] === 2 && is_array($search['params']->tipo_influenciador_id) && in_array($tipo_influenciador['id'], $search['params']->tipo_influenciador_id) ? 'selected' : '' }}>
                                                        {{ $tipo_influenciador['descripcion'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                <div class="input-group" style="padding-right: 10px;padding-left: 10px;">
                                    <span class="input-group-addon"></span>
                                    <div class="form-control float-initial">
                                        Distribución de Género
                                        <input type="text" id="distribucion_genero" name="distribucion_genero" data-from="{{ $search['foundSearch'] && $search['origin'] === 2 && is_array($search['params']->distribucion_genero) ? $search['params']->distribucion_genero[0] : '' }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <div class="form-control float-initial">
                                        <div>{{ $labels['genero'] }}</div>

                                        <select id="genero" name="genero" class="selectToList" multiple="true">
                                            @foreach ($catalogs['genero'] as $genero)
                                                <option value="{{ $genero['id'] }}"
                                                    data-imagen="{{ asset($genero['icono']) }}" {{ $search['foundSearch'] && $search['origin'] === 2 && is_array($search['params']->genero) && in_array($genero['id'], $search['params']->genero) ? 'selected' : '' }}>
                                                        {{ $genero['genero'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <div class="form-control float-initial">
                                        <input type="text" title="{{ $labels['manager_nombre'] }}" class="form-control"
                                            id="manager_nombre" name="manager_nombre" data-toggle="tooltip" placeholder="{{ $labels['manager_nombre'] }}" value="{{ $search['foundSearch'] && $search['origin'] === 2 ? $search['params']->manager_nombre : '' }}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <div class="form-control float-initial">
                                        <select data-toggle="tooltip" title="{{ $labels['pais_id'] }}" class="form-control"
                                            id="pais_id" name="pais_id" style="width: 100%;">
                                            @foreach ($catalogs['pais'] as $pais)
                                                <option value="{{ $pais['id'] }}" {{ $search['foundSearch'] && $search['origin'] === 2 && $pais['id'] == $search['params']->pais_id ? 'selected' : '' }}>
                                                        {{ $pais['pais'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <div class="form-control float-initial">
                                        <select data-toggle="tooltip" title="Palabra Clave" class="form-control"
                                            id="palabra_clave_search" name="palabra_clave_search" style="width: 100%;">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                <div id="palabra-clave-box">
                                    <ul id="palabra-clave-list">
                                        @if ($search['foundSearch'] && $search['origin'] === 2 && isset($search['params']->palabra_clave_id))
                                            @foreach ($search['params']->palabra_clave as $palabraClave)
                                                <li data-id="{{ $palabraClave['id'] }}">
                                                    <a href="#" class="btn-delete">
                                                        <i class="fa fa-times-circle fa-lg text-red"></i>
                                                    </a>
                                                    <span class="palabra-clave-item" data-id="{{ $palabraClave['id'] }}">{{ $palabraClave['palabra'] }}</span>
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 text-right">


                            <button type="button" id="btn-advanced-search"
                                class="btn btn-red-rounded btn-encuentra-influenciador no-loading">ENCUENTRA</button>
                            <input type="hidden" id="hasAdvancedSearch" value="{{ $search['foundSearch'] ? '1' : '0' }}">
                            <input type="hidden" id="origin" value="{{ $search['foundSearch'] ? $search['origin'] : '' }}">
                            <input type="hidden" id="pageForPagination" value="{{ $search['foundSearch'] ? (isset($search['params']->page) ? $search['params']->page : 1) : '0' }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <h2><span id="count-result-search"></span> INFLUENCERS</h2>
                            <strong></strong>
                            <div id="list-influenciadores-resultado-busqueda" class="row"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <ul class="pagination" style="display: none;">
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

<script id="influenciador-item-result-search-hbs" type="text/x-handlebars-template">
    @include('campanas/item_influenciador_search', [
        'tipo_influenciador' => null,
        'col' => '4',
        'btnAdd' => false,
    ])
</script>

<script id="influenciador-item-selected-hbs" type="text/x-handlebars-template">
    @include('campanas/item_influenciador_seleccionado', [
        'tipo_influenciador' => null,
    ])
</script>

<script id="palabra-clave-item-hbs" type="text/x-handlebars-template">
    @include('influenciadores/palabrasclave_tpl')
</script>

<script id="hashtag-item-hbs" type="text/x-handlebars-template">
    @include('campanas/hashtag_tpl')
</script>

@endsection

@push('styles')
    <link href="{{ asset('node_modules/gentelella/vendors/normalize-css/normalize.css') }}" rel="stylesheet">
    <link href="{{ asset('node_modules/gentelella/vendors/ion.rangeSlider/css/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('node_modules/gentelella/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('js/plugins/fancyBox/jquery.fancybox.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('node_modules/gentelella/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/handlebars/dist/handlebars.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/fancyBox/jquery.fancybox.js') }}"></script>
    <script src="{{ asset('js/campana_form.js') }}" type="text/javascript"></script>
@endpush
