
    <div class="container " style="background-color:#2A3F54; height: auto;">
        <div class="row box-line-gray" style="height: auto; padding: 5px 5px 5px 5px; ">

                <div class="col-md-12 col-xs-12" style="margin-top: 8px;">
                    <div class="row box-row-background-white " style=" background-color: #EEF1FB;  font-size: 14px;">
                        <div class="col-md-9 col-xs-9"></div>
                        <div class="col-md-3 col-xs-3">
                            <select class="box-dropdown-content" id="mySelect" style="padding: 2px;  margin-bottom: 5px;">
                                <option value="0">Interaciones</option>
                                <option value="1">Alcance</option>
                            </select>
                        </div>
                    </div>
                    <br>


                    <div class="row box-row-background-white" style="background-color: white;  height: auto; ">
                            <ul class="nav nav-tabs" id="myTab" style="display: none;">
                                <li class="active"><a href="#interaciones">Interaciones</a></li>
                                <li><a href="#alcance">Alcance</a></li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="interaciones">
                                    <div class="row">
                                         @include($response_info['resource'] . '.show_contenido_interaciones_v2')
                                    </div>
                                </div>
                                <div class="tab-pane" id="alcance">
                                    <div class="row">
                                         @include($response_info['resource'] . '.show_contenido_alcance_v2')
                                    </div>
                                </div>

                            </div>
                    </div>
                </div>
            </div>
        </div>
