@extends('layouts.' . config('app.layout') . '_search')

@section('search')
    {!! BootForm::horizontal([
            'method' => 'GET',
            'url' => url($response_info['resource']),
        ])
    !!}

        {!! BootForm::text('filter[nombre]', $labels['nombre'],
                app('request')->input('filter.nombre'), []
            )
        !!}

        {!! BootForm::select('filter[industria_id]', $labels['industria_id'],
                $catalogs['industria'], app('request')->input('filter.industria_id'), []
            )
        !!}

        {!! BootForm::select('filter[genero]', $labels['genero'],
                $catalogs['genero'], app('request')->input('filter.genero'), []
            )
        !!}

        {!! BootForm::select('filter[pais_id]', $labels['pais_id'],
                $catalogs['pais'], app('request')->input('filter.pais_id'), []
            )
        !!}

        {!! BootForm::select('filter[rango_etario_id]', $labels['rango_etario_id'],
                $catalogs['rango_etario'], app('request')->input('filter.rango_etario_id'), []
            )
        !!}

        {!! BootForm::select('filter[tipo_influenciador_id]', $labels['tipo_influenciador_id'],
                $catalogs['tipo_influenciador'], app('request')->input('filter.tipo_influenciador_id'), []
            )
        !!}

        <div class="form-group col-xs-12 col-md-12 text-center">
            {!! Form::button('<span class="glyphicon glyphicon-search"></span> Buscar', [
                    'type' => 'submit',
                    'class' => 'btn btn-red-rounded',
                ]) !!} &nbsp;
            <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
                <span class="glyphicon glyphicon-list"></span> Listar todos
            </a>
        </div>

    {!! BootForm::close() !!}
@endsection
