@if (!empty($model->perfilTwitter))
    @foreach($model->perfilTwitter->topTweetsAlcance()->get() as $topTweet)
        <div class="row blue-box">
            <div class="col-md-10 col-sm-12 no-padding">
                <div class="pull-left bg-lightgray">
                    @if($topTweet->hasMultimedia())
                        @if($topTweet->isImage())
                            <img src="{{ str_replace('_normal', '', $topTweet->multimedia) }}" style="width: 120px; height: 130px;">
                        @endif

                        @if($topTweet->isVideo())
                            <a class="fancy" href="{{ $topTweet->multimedia }}"><img src="{{ asset('img/logo.png') }}" style="width: 120px; height: 130px;"></a>
                        @endif
                    @else
                        <img src="{{ str_replace('_normal', '', $model->perfilTwitter->profile_image_url) }}" style="width: 120px; height: 130px;">
                    @endif
                </div>
                <div class="padding-min" style="margin-left:120px; margin-top: 5px;">
                    <p class="titulo-posts">TWEET <span class="text-small">| {{ \App\Helpers\DateConverter::convert($topTweet->fecha) }}</span></p>
                    <p class="text-small">{{ $topTweet->texto }}</p>
                    <p>
                        <button type="button" class="btn btn-red-rounded no-loading">RT {{ \App\Helpers\NumbersToStringConverter::toThousands($topTweet->retweets) }}</button>
                        <button type="button" class="btn btn-red-rounded no-loading">FV {{ \App\Helpers\NumbersToStringConverter::toThousands($topTweet->favorites) }}</button>
                        <button type="button" class="btn btn-red-rounded no-loading">Alcance {{ \App\Helpers\NumbersToStringConverter::toThousands($topTweet->total_followers_rt + $topTweet->retweets) }}</button>
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="col-md-2 text-center">
                <p class="circle">
                    <span class="ordinary">{{ $loop->index + 1 }}</span>
                </p>
            </div>
        </div>
        <br>
    @endforeach
@endif