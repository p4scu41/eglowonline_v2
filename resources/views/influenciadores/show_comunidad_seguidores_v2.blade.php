<div class="col-md-12 col-xs-12 row-top-content" >
    <img src="{{ asset('img/campana/twitter.png') }}" class="img-responsive" width="38" height="38"/>
</div>

<div class="col-md-12">

    @if (!empty($model->perfilTwitter))
        @foreach($model->perfilTwitter->topFollowers()->get() as $topFollower)

                <div class="col-md-2_5 col-xs-3 row-top-content" style="background-color: white; font-size: 12px; ">
                       @if(!is_null($topFollower->image_url))
                            <div class="img-container " style="background-color: #e0f7fa; padding: 5px 5px 5px 5px; ">
                                <img style="width: 194px; height:194px;" class="img-responsive img-circle" alt="Imagen responsive" src="{{ str_replace('_normal', '', $topFollower->image_url) }}" >
                                <div id="numero">{{ $loop->index + 1 }}</div>

                            </div>
                        @else
                            <img src="{{ asset('img/user_icon.png') }}" style="width: 120px; height: 130px;">
                        @endif

                        <div class="col-md-12 col-xs-12 box-below-comunidad" >
                            <div class="row text-center">
                                <div class="col-md-8 col-xs-12">
                                   {{ '@'.$topFollower->screen_name }}
                                </div>

                                <div class="col-md-4 col-xs-12">
                                     {{ \App\Helpers\NumbersToStringConverter::toThousands($topFollower->followers) }}
                                </div>

                            </div>
                        </div>
                </div>
        @endforeach
    @else

                <div style="background-color: gray; width: 100%; height: 100%;  z-index: 2; opacity: .75; " >
                    @for($i=0; $i<5; $i++)

                        <div class="col-md-2_5 row-top-content" style="background-color: white; font-size: 12px;  ">

                            <div class="img-container " style="background-color: #DBF4F5;  ">
                                <div class="text-center" style="left: 460px; top: 40px; position: absolute; padding: 5px 5px 5px 5px;  z-index: 3;">
                                 Próximamente
                            </div>
                                <img style=" width: 194px; height:194px;" class="img-square-comunidad img-responsive img-circle" alt="Proximamente" src="{{ asset('img/circle_gray.jpg') }}" >
                                <div id="numero">{{ $i + 1 }}</div>
                                <p>Proximamente</p>
                                <div id="capa"> </div>

                            </div>

                            <div class="col-md-12 col-xs-12 box-below-content" >
                                 <div class="row text-center">
                                     <div class="col-md-8 col-xs-12">
                                     </div>
                                     <div class="col-md-1 col-xs-12"></div>
                                     <div class="col-md-4 "></div>
                                     <div class="col-md-1 col-xs-2"></div>
                                 </div>
                             </div>
                        </div>
                    @endfor

                </div>
    @endif
</div>

    <!-- Facebook comunidad-->
    <div class="col-md-12 col-xs-12 row-top-content" style="">
        <img src="{{ asset('img/campana/facebook.png') }}" class="img-responsive" width="38" height="38"/>
    </div>

    <div class="col-md-12" style="  z-index: 1; margin-left: 5px;    padding: 5px 0px 5px 5px;">
        {{--@if (!empty($model->perfilFacebook))
                 <!-- TOP de Datos de facebook proximanente-->
            @else--}}

                     <div style="background-color: gray; width: 100%; height: 100%;  z-index: 2; opacity: .75; " >
                    @for($i=0; $i<5; $i++)

                       <div class="col-md-2_5 row-top-comunidad-gris" style="background-color: white; font-size: 12px;  ">
                            <div class="img-container " style="background-color: gray;  ">
                                <img style=" width: 194px; height:194px;" class="img-square-comunidad img-responsive img-circle" alt="Imagen responsive" src="{{ asset('img/circle_gray.jpg') }}" >

                                @if($i==2)
                                    <div class="text-center" id="prox">Próximamente</div>
                                @endif
                            </div>

                            <div class="col-md-12 col-xs-12 " >
                                 <div class="row text-center">
                                     <div class="col-md-8 col-xs-12">
                                     </div>
                                     <div class="col-md-1 col-xs-12"></div>
                                     <div class="col-md-4 "></div>
                                     <div class="col-md-1 col-xs-2"></div>
                                 </div>
                             </div>
                        </div>
                    @endfor
                </div>

        {{--@endif--}}
    </div>

    <!-- instagram comunidad-->
     <div class="col-md-12 col-xs-12 row-top-content" >
        <img src="{{ asset('img/campana/instagram.png') }}" class="img-responsive" width="38" height="38"/>
    </div>

    <div class="col-md-12" style="  z-index: 1; margin-left: 5px;    padding: 5px 0px 5px 5px;">
        {{--@if (!empty($model->perfilInstagram))
                 <!-- TOP de Datos de facebook proximanente-->
            @else--}}
                <div style="background-color: gray; width: 100%; height: 100%;  z-index: 2; opacity: .75; " >
                    @for($i=0; $i<5; $i++)
                         <div class="col-md-2_5 row-top-comunidad-gris" style="background-color: white; font-size: 12px;  ">
                            <div class="img-container " style="background-color: gray;  ">
                                <img style=" width: 194px; height:194px;" class="img-square-comunidad img-responsive img-circle" alt="Imagen responsive" src="{{ asset('img/circle_gray.jpg') }}" >

                                @if($i==2)
                                    <div class="text-center" id="prox">Próximamente</div>
                                @endif
                            </div>

                            <div class="col-md-12 col-xs-12 " >
                                 <div class="row text-center">
                                     <div class="col-md-8 col-xs-12">
                                     </div>
                                     <div class="col-md-1 col-xs-12"></div>
                                     <div class="col-md-4 "></div>
                                     <div class="col-md-1 col-xs-2"></div>
                                 </div>
                             </div>
                        </div>
                    @endfor
                </div>
            {{--@endif--}}
    </div>

    <!-- YouTube Comunidad-->
    <div class="col-md-12 col-xs-12 row-top-content" >
        <img src="{{ asset('img/campana/youtube.png') }}" class="img-responsive" width="38" height="38"/>
    </div>

    <div class="col-md-12" style="  z-index: 1; margin-left: 5px;  padding: 5px 0px 5px 5px;">
    {{--    @if (!empty($model->perfilYoutube))
                 <!-- TOP de Datos de facebook proximanente-->
            @else--}}
                <div style="background-color: gray; width: 100%; height: 100%;  z-index: 2; opacity: .75; " >
                    @for($i=0; $i<5; $i++)
                        <div class="col-md-2_5 row-top-comunidad-gris" style="background-color: white; font-size: 12px;  ">
                            <div class="img-container " style="background-color: gray;  ">
                                <img style=" width: 194px; height:194px;" class="img-square-comunidad img-responsive img-circle" alt="Imagen responsive" src="{{ asset('img/circle_gray.jpg') }}" >

                                @if($i==2)
                                    <div class="text-center" id="prox">Próximamente</div>
                                @endif
                            </div>

                            <div class="col-md-12 col-xs-12 " >
                                 <div class="row text-center">
                                     <div class="col-md-8 col-xs-12">
                                     </div>
                                     <div class="col-md-1 col-xs-12"></div>
                                     <div class="col-md-4 "></div>
                                     <div class="col-md-1 col-xs-2"></div>
                                 </div>
                             </div>
                        </div>
                    @endfor
                </div>
            {{--@endif--}}
    </div>

    <div class="col-md-12 " style="z-index: 1; margin-left: 5px;  padding: 5px 0px 5px 5px;">

            <div class="col-md-6 col-xs-12" style="padding: 15px 15px 15px 15px; ">
                <div class="row box-top-palabras">
                     <div class="text-center " >
                         <p style=""></p>
                         <img src="{{ asset('img/influenciador_perfil/palabras_clave.png') }}" >
                         {{ trans('influencer.top_keywords') }}
                     </div>
                    @php
                         $topPalabrasClaves = $model->getTopPalabrasClavesComunidadJsonGrafica()
                    @endphp

                    @if (empty($topPalabrasClaves['data']))
                        <p>&nbsp;</p><p>&nbsp;</p>
                        <div class="text-center">Sin datos que mostrar</div>
                    @else
                        <div class="col-md-12 col-xs-11" id="container-div-top-palabras-comunidad" style="height:250px; padding-bottom:20px;"
                            data-json="{{ json_encode($topPalabrasClaves) }}">
                         </div>
                    @endif
                </div>
            </div>
            <!-- Hashtag -->
            <div class="col-md-6 col-xs-12 box-padding-right-border" style="padding: 15px 27px 15px 10px;">
                <div class="row box-top-palabras">
                        <div class="text-center ">
                            <p style=""></p>
                            <img src="{{ asset('img/influenciador_perfil/hashtag.png') }}" >
                           {{ trans('influencer.top_hashtags') }}
                        </div>

                        @php
                             $topHashtags = $model->getTopHashtagsComunidadJsonGrafica()
                        @endphp

                        @if (empty($topHashtags['data']))
                            <p>&nbsp;</p><p>&nbsp;</p>
                            <div class="text-center">Sin datos que mostrar</div>
                        @else
                         <div class="col-md-12 col-xs-11" id="container-div-top-hashtags-comunidad" style=" height:250px; padding-bottom:20px;"
                            data-json="{{ json_encode($topHashtags) }}">
                         </div>
                        @endif
                </div>
            </div>

    </div>

