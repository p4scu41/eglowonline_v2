<div class="container">
        <div class="row box-line-gray" style=" padding: 10px 10px 10px 10px; display: flex; padding-right: 0px;">
            <div class="col-md-9 col-sm-9 col-xs-7" >
                <div class="row" style="background-color: white; " >
                    <div class="col-md-3 col-xs-3" style="background-color: transparent; margin-left: 20px; margin-top: 54px;">
                          <div id="avatar-influencer" class="img-responsive img-circle img-square" alt="Imagen responsive"
                             style="background-image:url('{!! str_replace('_normal', '', $model->imgPerfil) !!}')" >
                          </div>
                    </div>
                    <div class="col-md-8 col-xs-7" style="  background-color: white; margin-left: 25px; margin-top: 34px;" >
                        <div class="row">
                            <label >{{ $model->usuario->nombre }}</label>
                            <p style="color: blue;">
                                <p><strong class="text-red">{{ $model->perfilTwitter ? $model->perfilTwitter->screen_name : $model->nombre }}</strong></p>
                            </p>
                            <div class="col-sm-6" style="">
                                <img src="{{ asset('img/influenciador_perfil/ubicacion.png') }}" >
                                {{ trans('influencer.influence_location') }}
                                <p class="center p-l-21">{{ $model->pais->pais }}</p>
                                <img src="{{ asset('img/influenciador_perfil/rangoEtario.png') }}" >
                                {{ trans('influencer.age_range') }}
                                <p class="p-l-21">{{ $model->rangoEtario->descripcion }}</p>
                                <img src="{{ asset('img/influenciador_perfil/categoria.png') }}" >
                                {{ trans('influencer.type_') }}
                                <p class="p-l-21">{{ $model->tipoInfluenciador->descripcion }}</p>
                            </div>
                            <div class="col-sm-6">
                                <img src="{{ asset('img/influenciador_perfil/profesion.png') }}" >
                                {{ trans('influencer.profession_or_occupation') }}
                                <p class="p-l-21">{{ $model->getTextProfesiones() }}</p>
                                <img src="{{ asset('img/influenciador_perfil/lugardenacimiento.png') }}" >
                                {{ trans('influencer.place_birth') }}
                                <p class="p-l-21">{{ $model->lugar_nacimiento }}</p>
                                <img src="{{ asset('img/influenciador_perfil/categoria.png') }}" >
                                {{ trans('influencer.category_') }}
                                <p class="p-l-21">{{ $model->industria->nombre }}</p>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-md-12">
                                   <img src="{{ asset('img/influenciador_perfil/biografia.png') }}" >
                                   {{ trans('influencer.profile_') }}
                                   <p class="p-l-21">{{ $model->biografia }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- INICIO Audiencia-->
            <div class="col-md-3 col-sm-3 col-xs-5 box-padding-right-border" style="display: flex; padding-right: 0px;">
                <div class="row box-audience"  style="width: 100%;">
                    <div class="col-md-12 text-center" >
                        <br>
                        <p>{{ trans('influencer.total_audience') }}</p>
                        <div id="imagen">
                              <img class="img-responsive" src="{{ asset('img/influenciador_perfil/icono-catidad-de-followers.png') }}" width="208" height="197"  >
                              <div id="numero">@formatNumber($model->totalFollowers)</div>

                        </div>
                    </div>
                </div>
            </div>
            <!--FIN Audiencia -->
        </div>

    <!-- Segundo cuadro-->

        <div class="row box-line-gray" style="padding: 10px 10px 10px 10px; display: flex; padding-right: 0; margin-top: -8px;">
            <div class="col-md-4 col-xs-6 col-sm-8" style="display: flex; padding-right: 5px;">
                <div class="row cajita" style=" background-color: white; min-height: 290px; padding: 8px 8px 8px 8px; width: 100%;">
                    <br>
                    <div class="col-md-12 col-xs-12">
                        <div class="text-center"><img src="{{ asset('img/influenciador_perfil/crecimiento.png') }}" >
                         {{ trans('influencer.social_overview') }}
                        </div>
                        <div class="col-md-2 col-xs-3" id="divprueba"> </div>
                        <div class="col-md-10 col-xs-9 cajita" style="background-color: transparent;">
                                @php
                                    $seguidores = $model->json_grafica_total_seguidores;
                                @endphp
                            <div  id="chartSeguidores"
                                data-texts="{{ json_encode($seguidores['red_social']) }}"
                                data-values="{{ json_encode($seguidores['seguidores']) }}"
                                data-icons="{{ json_encode($seguidores['icons']) }}"
                                data-twitter="{{ empty($model->perfilTwitter) ? 0 : ($model->perfilTwitter->estadisPerfil ? $model->perfilTwitter->estadisPerfil->followers : 0) }}"
                                data-facebook="10"
                                style="margin-left: -70px; margin-top: -63px; width: 100%; height: 250px;">

                            </div>
                           <!-- <canvas id="redsocial2" style="width: 330px; height: 235px;"></canvas>-->
                        </div>

                    </div>

                </div>
            </div>
            <div class="col-md-4 col-xs-6 col-sm-6 box-padding-right-border" style="display: flex; padding-right: 5px; padding-left: 5px;">
                <div class="row box-three-audience">
                   <ul class="nav" style="padding: 20px 25px 0 35px;">
                        <li class="p-b-5">
                            <img class="text-center" src="{{ asset('img/influenciador_perfil/marcaspalomita.png') }}">
                            {{ trans('influencer.favorite_brands') }}

                            @if (!empty($model->getMarcasMeGusta(true)))
                                <p class="p-l-21">{{ $model->getMarcasMeGusta(true) }}</p>
                            @endif
                        </li>
                        <li class="p-b-5">
                            <img src="{{ asset('img/influenciador_perfil/marcastache.png') }}">
                            {{ trans('influencer.restricted_brands') }}
                            @if (!empty($model->getMarcasNoDeseoColaborado(true)))
                                <p class="p-l-21">{{ $model->getMarcasNoDeseoColaborado(true) }}</p>
                            @endif
                        </li>
                        <li class="p-b-5">
                            <img src="{{ asset('img/influenciador_perfil/marcaspalomita.png') }}">
                            {{ trans('influencer.colaborated_with') }}
                            @if (!empty($model->getMarcasHeColaborado(true)))
                                <p class="p-l-21">{{ $model->getMarcasHeColaborado(true) }}</p>
                            @endif
                        </li>
                        <li class="p-b-5">
                            <img src="{{ asset('img/influenciador_perfil/intereses.png') }}">
                            {{ trans('influencer.interest_') }}
                            @if (!empty($model->palabrasClavePalabra(true)))
                                <p class="p-l-21">{{ $model->palabrasClavePalabra(true) }}</p>
                            @endif
                       </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-xs-9 col-sm-12 box-padding-right-border" style="display: flex; padding-left: 5px; padding-right: 0;">

                <div class="row box-three-audience text-center" >
                    <br>
                    <div class="text-center">
                        <img src="{{ asset('img/influenciador_perfil/influencia.png') }}" >
                        {{ trans('influencer.type_of_audience') }}
                    </div>

                    <br>
                        @php
                            $categorias = $model->getArrayCategoriasInfluencers();
                            $inactivos  = $model->followersFakes();

                            $fakes      = $inactivos->followers_revisados != 0 ? number_format(($inactivos->followers_fakes / $inactivos->followers_revisados) * 100) : 0;

                            $values = array_pluck($categorias['data'], 'value');
                            $texts  = array_pluck($categorias['data'], 'name');
                            $colors = array_pluck($categorias['data'], 'colors');

                            $values[] = (int)$fakes;
                            $texts[]  = 'Inactivo';
                            $colors[] = '#C68BE1';
                        @endphp


                    <div id="chartCategories"
                        data-values="{{ json_encode($values) }}"
                        data-texts="{{ json_encode($texts) }}"
                        data-colors="{{ json_encode($colors) }}"
                        style="display: none; min-width:300px; min-height:250px;">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <canvas  id="mycanvas" width="150" height="80">

                        </canvas>
                    </div>

                </div>
            </div>
        </div>

    <!-- top de palabras claves-->
       <div class="row box-line-gray" style="padding: 10px 10px 10px 10px; display: flex; margin-top: -8px;">
            <div class="col-md-6 col-xs-12" style="">
                <div class="row box-top-palabras">
                     <div class="text-center " >
                         <p style=""></p>
                         <img src="{{ asset('img/influenciador_perfil/palabras_clave.png') }}" >
                         {{ trans('influencer.top_keywords') }}
                     </div>
                    @php
                         $topPalabrasClaves = $model->getTopPalabrasClavesJsonGrafica()
                    @endphp

                    @if (empty($topPalabrasClaves['data']))
                        <p>&nbsp;</p><p>&nbsp;</p>
                        <div class="text-center">Sin datos que mostrar</div>
                    @else
                        <div class="col-md-12 col-xs-11" id="container-div-top-palabras" style="height:250px; padding-bottom:20px;"
                            data-json="{{ json_encode($topPalabrasClaves) }}">
                         </div>
                    @endif
                </div>
            </div>

            <div class="col-md-6 col-xs-12 box-padding-right-border" style="">
                <div class="row box-top-palabras">
                        <div class="text-center ">
                            <p style=""></p>
                            <img src="{{ asset('img/influenciador_perfil/hashtag.png') }}" >
                           {{ trans('influencer.top_hashtags') }}
                        </div>

                        @php
                             $topHashtags = $model->getTopHashtagsJsonGrafica()
                        @endphp

                        @if (empty($topHashtags['data']))
                            <p>&nbsp;</p><p>&nbsp;</p>
                            <div class="text-center">Sin datos que mostrar</div>
                        @else
                         <div class="col-md-12 col-xs-11" id="container-div-top-hashtags" style=" height:250px; padding-bottom:20px;"
                            data-json="{{ json_encode($topHashtags) }}">
                         </div>
                        @endif
                </div>
            </div>
        </div>

    <!-- Top 5 marcas-->
        <div class="row box-line-gray" style="padding: 10px 10px 10px 10px; margin-top: -19px;">
             @php
                    $topmarcas= $model->getMarcas();
                @endphp
                    <div class="col-md-12 " style="" id="canvass" data-values="{{json_encode(array_pluck($topmarcas['data'], 'cantidad'))}}">
                        <div class="row">
                            <br>
                            <div class="text-center" style="font-size: 14pt;">
                                <img src="{{ asset('img/influenciador_perfil/palabras_clave.png') }}" >
                                {{ trans('influencer.top_mentioned') }}
                            </div>

                            @if (empty($topmarcas['data']))
                                <p>&nbsp;</p><p>&nbsp;</p>
                                <div class="col-md-12 text-center" style="font-size: 12pt; width:100%; height: 40px;">Sin datos que mostrar</div>
                                <br>
                                <br>
                            @else

                                @foreach($topmarcas['data'] as $topmarca)
                                <div class="col-md-2 col-sm-4" style=" margin-left:20px; text-align: center; ">
                                    <div class="circle text-center" id="circles-{!!($loop->index)+1!!}" style="background-color: transparent; color: black; padding-left: 20px;"></div>
                                    <div class="row" style="font-size: 12pt;">
                                        <div class="col-md-3 col-xs-3"></div>
                                        <div class="col-md-6 col-xs-6 box-word-mark-circle text-center" >
                                            {{ $topmarca['palabra'] }}
                                        </div>
                                        <div class="col-md-2 col-xs-2 text-center" style="background-color: #04949A; color: white; height: 20px;">
                                             {!!($loop->index)+1!!}
                                        </div>
                                    </div>
                                </div>

                            @endforeach

                            @endif

        		        </div>
        		    </div>
        </div>


    	<!--   <div class="row box-chat-support text-right box-form-input">
                   <div class="col-md-12 box-form-chat text-center">
                       <div class=col-md-2></div>
                       <p class="col-md-8">Email</p>
                       <img class="col-md-2 icon-arrow" src="{{asset('img/flecha_abajo1.png') }}" width="10" height="12" ></img>
                       <p><input class="col-md-12 box-form-input" type="text" placeholder="Correo Electronico">
                       <br>
                       <p>Message</p>
                       <p><textarea  class="col-md-12 box-form-textarea" placeholder="Escribe un comentario"></textarea>
                       <p></p><input class="col-md-12" id="btn_send_message" type="button" value="Enviar" >
                   </div>
            </div>

         	<div class="footer-chat-support text-right" >
                    <div class="btn-img-chat"> <img  src="{{ asset('img/img-chat-support.png') }}" width="50" height="50" ></div>
            </div>-->




</div>
