@extends('layouts.' . config('app.layout') . '_small' )

@push('styles')
    <link rel="stylesheet" href="{{ asset('js/plugins/fancyBox/jquery.fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/jQCloud/dist/jqcloud.css') }}">
@endpush

@push('scripts-head')
    <script type="text/javascript">
        var id_influencer  = '{{ empty($model) ? 0 : $model->id }}';
    </script>
@endpush

@section('title', $title)

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li><a href="{{ url($response_info['resource']) }}">{{ $title }}</a></li>
        <li class="active">Ver</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" role="tabpanel" data-example-id="togglable-tabs">
            @if(empty($model))
                @include('common.errors', ['response_info' => $response_info])
            @else
                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                    <li role="presentation" class="active">
                        <h2>
                            <a href="#tab_perfil" id="perfil-tab" role="tab" data-toggle="tab" aria-expanded="true">
                                <i class="fa fa-user"></i>
                                PERFIL
                            </a>
                        </h2>
                    </li>
                    <li role="presentation">
                        <h2>
                            <a href="#tab_estadisticas" class="campaign-tabs" id="profile-statistics" role="tab" data-toggle="tab" aria-expanded="false">
                                <i class="fa fa-bar-chart"></i>
                                ESTADÍSTICAS
                            </a>
                        </h2>
                    </li>
                    <li role="presentation">
                        <h2>
                            <a href="#tab_contenido" id="contenido-tab" role="tab" data-toggle="tab" aria-expanded="false">
                                <i class="fa fa-file-text-o"></i>
                                CONTENIDO
                            </a>
                        </h2>
                    </li>
                    <li role="presentation">
                        <h2>
                            <a href="#tab_comunidad" id="comunidad-tab" role="tab" data-toggle="tab" aria-expanded="false">
                                <i class="fa fa-users"></i>
                                COMUNIDAD
                            </a>
                        </h2>
                    </li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane" id="tab_estadisticas" style="margin: 0px -10px 0 -10px;">
                        @include($response_info['resource'] . '.show_estadisticas_v2')
                    </div>
                    <div role="tabpanel" class="tab-pane active"  id="tab_perfil">
                        @include($response_info['resource'] . '.show_perfil_v2')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab_contenido" >
                        @include($response_info['resource'] . '.show_contenido_v2')
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script src="{{ asset('js/plugins/Chart.js/circles.min.js') }}"></script>
    <script src="{{ asset('js/plugins/fancyBox/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('js/plugins/jQCloud/dist/jqcloud.js') }}"></script>
    <script src="{{ asset('js/plugins/zingchart/zingchart.min.js') }}"></script>
    <script src="{{ asset('js/plugins/echart/echarts.js') }}"></script>
    <script src="{{ asset('js/influenciador_profile_v2.js') }}"></script>
    <script src="{{ asset('js/influenciador_contenido.js') }}"></script>
    <script src="{{ asset('js/influenciador_estadisticas_v2.js') }}"></script>
@endpush