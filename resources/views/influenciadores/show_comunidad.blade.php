<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
        <div class="text-center black-box">
            <div>
                <div>
                    <button type="button" data-id="seguidores" class="content-tab tab-contenido text-center no-loading btn btn-red-rounded">SEGUIDORES CON + SEGUIDORES</button>
                    <button type="button" data-id="conQuienesConversa" class="content-tab tab-contenido text-center no-loading btn btn-default btn-round hide">CON QUIENES CONVERSA MÁS</button>
                    <button type="button" data-id="seguidoresQueRT" class="content-tab tab-contenido text-center no-loading btn btn-default btn-round hide">TOP SEGUIDORES QUE RT</button>
                    <button type="button" data-id="seguidoresQueFav" class="content-tab tab-contenido text-center no-loading btn btn-default btn-round hide">TOP SEGUIDORES QUE FAV</button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 content-tab" id="seguidores">
        @include('influenciadores.show_comunidad_seguidores')
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 content-tab hide" id="conQuienesConversa">
        En construccion
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 content-tab hide" id="seguidoresQueRT">
        En construccion
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 content-tab hide" id="seguidoresQueFav">
        En construccion
    </div>
</div>