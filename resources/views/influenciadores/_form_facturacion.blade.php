@php
    $factura = $model->factura()->first();
@endphp

<p class="lead text-primary">Esta información es opcional; puede completarla posteriormente.</p>
<br><br>
<form class="form-horizontal" id="formFactura">
    {!!
        BootForm::text('rfc', false, isset($factura) ? $factura->rfc : '', [
            'data-toggle'         => 'tooltip',
            'placeholder'         => 'RFC',
            'title'               => 'RFC',
            'icon'                => '&nbsp; &nbsp;',
        ])
    !!}

    <div class="form-group">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="radio">
                <label>
                    &nbsp;&nbsp;<input type="radio" name="tipoPersona" class="icheck" value="1" data-toggle="tooltip" title="Tipo de Persona" {{ isset($factura) && $factura->es_persona_moral === 0 ? 'checked' : '' }}> Persona Física
                </label>
            </div>

            <div class="radio">
                <label>
                    &nbsp;&nbsp;<input type="radio" name="tipoPersona" class="icheck" value="2" data-toggle="tooltip" title="Tipo de Persona" {{ isset($factura) && $factura->es_persona_moral === 1 ? 'checked' : '' }}> Persona Moral
                </label>
            </div>
        </div>
    </div>

    {!!
        BootForm::text('razonSocial', false, isset($factura) ? $factura->razon_social : '', [
            'data-toggle'        => 'tooltip',
            'placeholder'        => 'Razón Social',
            'title'              => 'Razón Social',
            'icon'               => '&nbsp; &nbsp;',
        ])
    !!}

    {!!
        BootForm::text('banco', false, isset($factura) ? $factura->banco : '', [
            'data-toggle'        => 'tooltip',
            'placeholder'        => 'Nombre del banco',
            'title'              => 'Nombre del banco',
            'icon'               => '&nbsp; &nbsp;',
        ])
    !!}

    {!!
        BootForm::text('cuenta', false, isset($factura) ? $factura->cuenta : '', [
            'data-toggle'        => 'tooltip',
            'placeholder'        => 'Cuenta / Clave',
            'title'              => 'Cuenta / Clave',
            'icon'               => '&nbsp; &nbsp;',
        ])
    !!}

    {!!
        BootForm::text('calle', false, isset($factura) ? $factura->calle : '', [
            'data-toggle'        => 'tooltip',
            'placeholder'        => 'Calle',
            'title'              => 'Calle',
            'icon'               => '&nbsp; &nbsp;',
        ])
    !!}

    {!!
        BootForm::text('numExterior', false, isset($factura) ? $factura->no_exterior : '', [
            'data-toggle'        => 'tooltip',
            'placeholder'        => 'Num. Exterior',
            'title'              => 'Num. Exterior',
            'icon'               => '&nbsp; &nbsp;',
        ])
    !!}

    {!!
        BootForm::text('numInterior', false, isset($factura) ? $factura->no_interior : '', [
            'data-toggle' => 'tooltip',
            'placeholder' => 'Num. Interior',
            'title'       => 'RFC',
            'icon'        => '&nbsp; &nbsp;',
        ])
    !!}

    {!!
        BootForm::text('colonia', false, isset($factura) ? $factura->colonia : '', [
            'data-toggle'        => 'tooltip',
            'placeholder'        => 'Colonia',
            'title'              => 'Colonia',
            'icon'               => '&nbsp; &nbsp;',
        ])
    !!}

    {!!
        BootForm::text('cp', false, isset($factura) ? $factura->codigo_postal : '', [
            'data-toggle'        => 'tooltip',
            'placeholder'        => 'Código Postal',
            'title'              => 'Código Postal',
            'icon'               => '&nbsp; &nbsp;',
        ])
    !!}

    {!!
        BootForm::text('localidad', false, isset($factura) ? $factura->localidad : '', [
            'data-toggle'        => 'tooltip',
            'placeholder'        => 'Localidad',
            'title'              => 'Localidad',
            'icon'               => '&nbsp; &nbsp;',
        ])
    !!}

    {!!
        BootForm::text('municipio', false, isset($factura) ? $factura->municipio : '', [
            'data-toggle'        => 'tooltip',
            'placeholder'        => 'Municipio',
            'title'              => 'Municipio',
            'icon'               => '&nbsp; &nbsp;',
        ])
    !!}

    {!!
        BootForm::text('estado', false, isset($factura) ? $factura->estado : '', [
            'data-toggle'        => 'tooltip',
            'placeholder'        => 'Estado',
            'title'              => 'Estado',
            'icon'               => '&nbsp; &nbsp;',
        ])
    !!}

    <div class="form-group">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="input-group">
                <span class="input-group-addon">&nbsp; &nbsp;</span>
                <select name="pais_id_fact" id="pais_id_fact" class="form-control" data-toggle="tooltip" title="País">
                    @foreach($catalogs['pais'] as $index => $pais)
                        <option value="{{ $index }}" {{ isset($factura) && $factura->pais_id === $index ? 'selected': '' }}>{{ $pais }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="text-center col-xs-12 col-sm-12 col-md-12">
        <a class="btn btn-red-rounded btn-save-influencer">
            <span class="glyphicon glyphicon-floppy-save"></span> Guardar
        </a>
    </div>
</form>