@extends('layouts.' . config('app.layout'))

@section('title', $title)

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <h3 class="title">{{ strtoupper($title) }}</h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    {{-- Si se esta implementando Policy --}}
                    @if ($policy)
                        @can('create', $modelClass)
                        <p>
                            <a class="btn btn-red-rounded" href="{{ url($response_info['resource'] . '/create') }}" role="button">
                                <span class="glyphicon glyphicon-plus-sign"></span> Registrar
                            </a>
                        </p>
                        @endcan
                    @else
                        <p>
                            <a class="btn btn-red-rounded" href="{{ url($response_info['resource'] . '/create') }}" role="button">
                                <span class="glyphicon glyphicon-plus-sign"></span> Registrar
                            </a>
                        </p>
                    @endif
                </div>
            </div>

            @include($response_info['resource'] . '/_search', [
                'response_info' => $response_info,
                'lables' => $labels,
                'catalogs' => $catalogs,
            ])

            @include('common.errors', ['response_info' => $response_info])

            <div class="text-center">
                {{ $models->appends(Request::query())->links() }}
            </div>

            @if (app('request')->input('filter'))
                <div class="well well-sm text-success">
                    <span class="glyphicon glyphicon-zoom-in"></span> Resultados de la búsqueda
                </div>
            @endif

            Mostrando {{ $models->firstItem() }}-{{ $models->lastItem() }} de {{ $models->total() }} elementos.

            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th style="width: 40px;" class="text-center"></th>
                        <th>{{ $labels['nombre'] }}</th>
                        <th>{{ $labels['email'] }}</th>
                        <th>{{ $labels['tipo_influenciador_id'] }}</th>
                        <th>{{ $labels['industria_id'] }}</th>
                        <th>{{ $labels['rango_etario_id'] }}</th>
                        <th>{{ $labels['activo'] }}</th>
                        <th>{{ $labels['ultimo_acceso'] }}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0; ?>
                    @forelse ($models as $model)
                        <tr>
                            <td class="text-center">{{ $models->firstItem() + ($i++) }}</td>
                            <td>{{ $model->nombre }}</td>
                            <td>{{ $model->usuario->email }}</td>
                            <td>{{ $model->tipoInfluenciador->descripcion }}</td>
                            <td>{{ $model->industria->nombre }}</td>
                            <td>{{ $model->rangoEtario->descripcion }}</td>
                            <td class="text-center">@boolean_thumb($model->usuario->activo)</td>
                            <td>@datetime($model->ultimo_acceso)</td>
                            <td class="text-center">
                                @include('common.options', [
                                    'policy' => $policy,
                                    'modelClass' => $modelClass,
                                    'model' => $model,
                                    'url' => url($response_info['resource'] . '/' . $model->id)
                                ])
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="9">No se encontrarón datos.</td>
                        </tr>
                    @endforelse

                </tbody>
            </table>

            <div class="text-center">
                {{ $models->appends(Request::query())->links() }}
            </div>
        </div>
    </div>
@endsection
