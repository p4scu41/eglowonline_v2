<li data-id="{{{ isset($id) ? $id : '@{{id}}' }}}">
    <a href="#" class="btn-delete">
        <i class="fa fa-times-circle fa-lg text-red"></i>
    </a>
    <span class="palabra-clave-item" data-id="{{{ isset($id) ? $id : '@{{id}}' }}}">
        {{{ isset($text) ? $text : '@{{text}}' }}}
    </span>
</li>
