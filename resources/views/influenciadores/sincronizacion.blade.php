@extends('layouts.' . config('app.layout'))

@section('title', $title)

@push('scripts-head')
    <script type="text/javascript">
        var urlSincronizarJsons = '{{ url('influenciadores/sincronizarjsons') }}';
        var urlSincronizarPerfil = '{{ url('influenciadores/sincronizarperfil') }}';
    </script>
@endpush

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <h3 class="title">{{ strtoupper($title) }}</h3>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <p>
                        <a class="btn btn-red-rounded" id="btn-syncperfiles" href="#" role="button">
                            <i class="fa fa-refresh"></i> Sincronizar Perfiles
                        </a>
                    </p>
                    Última fecha de sincronización: {{ $fecha_sincronizacion }}
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <span id="reloj"></span>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center" id="info"></div>
            </div>

            @include('common.errors', ['response_info' => $response_info])

            <table class="table table-hover table-stripped" id="tbl-influenciadores">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Influenciador</th>
                        <th>Usuario</th>
                        <th class="text-center">Sincronización</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($influenciadores as $influenciador)
                        <tr data-influenciador="{{ $influenciador->influenciador->id }}" class="row-influenciador">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $influenciador->nombre }}</td>
                            <td>{{ $influenciador->influenciador->perfilTwitter ? $influenciador->influenciador->perfilTwitter->screen_name : '' }}</td>
                            <td class="text-center"><span id="sync-{{ $influenciador->influenciador->id }}">
                                <span class="fa-stack fa-lg">
                                  <i class="fa fa-circle fa-stack-2x"></i>
                                  <i class="fa fa-question fa-stack-1x fa-inverse"></i>
                                </span>
                            </span></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('js/influenciador_syncperfiles.js') }}" type="text/javascript"></script>
@endpush
