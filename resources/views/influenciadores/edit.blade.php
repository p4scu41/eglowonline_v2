@section('section_title', 'Actualizar')

@include($response_info['resource'] . '._form', [
    'config_form' => [
        'id'     => 'influenciador-form',
        'model'  => $model,
        'update' => $response_info['resource'] . '.update',
    ],
    'catalogs' => $catalogs,
])
