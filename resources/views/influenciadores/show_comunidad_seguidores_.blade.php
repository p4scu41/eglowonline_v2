@php
$row = 1;
@endphp

@if (!empty($model->perfilTwitter))
    @foreach($model->perfilTwitter->topFollowers()->get() as $topFollower)
        @if(($loop->index + 1) === 1)
            <div class="row blue-box">
                <div class="col-md-10 col-sm-12 no-padding">
                    <div class="pull-left bg-lightgray">
                        @if(!is_null($topFollower->image_url))
                            <img src="{{ str_replace('_normal', '', $topFollower->image_url) }}" style="width: 120px; height: 130px;">
                        @else
                            <img src="{{ asset('img/user_icon.png') }}" style="width: 120px; height: 130px;">
                        @endif
                    </div>
                    <div class="padding-min" style="margin-left:120px; margin-top: 5px;">
                        <p class="titulo-posts"></p>
                        <p class="text-small">@ {{ $topFollower->screen_name }}</p>
                        <p>{{ $topFollower->biografia }}</p>
                        <p>
                            <button type="button" class="btn btn-red-rounded no-loading"><i class="fa fa-twitter"></i> {{ \App\Helpers\NumbersToStringConverter::toThousands($topFollower->followers) }}</button>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-2 text-center">
                    <p class="circle">
                        <span class="ordinary">{{ $loop->index + 1 }}</span>
                    </p>
                </div>
            </div>
            <br>
        @else
            @if($row === 1)
                <div class="row vertical-divider">
            @endif
                <div class="col-md-6">
                    <div class="row blue-box">
                        <div class="col-md-9 col-sm-9 no-padding">
                            <div class="pull-left bg-lightgray">
                               @if(!is_null($topFollower->image_url))
                                    <img src="{{ str_replace('_normal', '', $topFollower->image_url) }}" style="width: 95px; height: 95px;">
                                @else
                                    <img src="{{ asset('img/user_icon.png') }}" style="width: 95px; height: 95px;">
                                @endif
                            </div>
                            <div class="padding-min" style="margin-left:120px; margin-top: 5px;">
                                <p class="titulo-posts"></p>
                                <p class="text-small">@ {{ $topFollower->screen_name }}</p>
                                <p class="text-small">{{ $topFollower->biografia }}</p>
                                <p>
                                    <button type="button" class="btn btn-red-rounded no-loading"><i class="fa fa-twitter"></i> {{ \App\Helpers\NumbersToStringConverter::toThousands($topFollower->followers) }}</button>
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="col-md-2 text-center">
                            <p class="circle-small">
                                <span class="ordinary">{{ $loop->index + 1 }}</span>
                            </p>
                        </div>
                    </div>
                </div>
            @if($row === 2)
                </div>
                <br>
                @php
                    $row = 1;
                @endphp
            @else
                @if($loop->last)
                    </div>
                    <br>
                @else
                    @php
                        $row++;
                    @endphp
                @endif
            @endif
        @endif
    @endforeach
@endif

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="text-center black-box">
            <div>
                <div>
                    <button type="button" data-id="topHashtags" class="content-tab tab-contenido text-center no-loading btn btn-red-rounded">TOP HASHTAGS</button>
                    <button type="button" data-id="topPalabrasClave" class="content-tab tab-contenido text-center no-loading btn btn-default btn-round">TOP PALABRAS CLAVE</button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 content-tab" id="topHashtags">
        <span id="cargarGraficaHashtags" class="hide"><i class="fa fa-spinner fa-spin fa-2x"></i> Cargando gráfica...</span>
        <h2 class="hide" id="mensajeNoEncontradoHashtags">No existe información para graficar</h2>
        <div id="graficaHashtags" class="hide" style="width: 900px; height:400px;" data-url={{ route('etl.twitter.comunidad.top-hashtags') }}></div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 content-tab hide" id="topPalabrasClave">
        <span id="cargarGraficaTopKeywords" class="hide"><i class="fa fa-spinner fa-spin fa-2x"></i> Cargando gráfica...</span>
        <h2 class="hide" id="mensajeNoEncontradoKeywords">No existe información para graficar</h2>
        <div id="graficaPalabrasClave" class="hide" style="width: 900px; height:400px;" data-url={{ route('etl.twitter.comunidad.top-keywords') }}></div>
    </div>
    <input type="hidden" id="influenciadorId" value="{{ $model->id }}">
</div>