<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 box-border-gray text-dark-gray no-padding">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <ul class="list-icon">
                <li>
                    <span class="li-icon"><i class="fa fa-user fa-2x"></i></span>
                    <h4>CRECIMIENTO MENSUAL DE FOLLOWERS</h4>
                </li>
            </ul>
            <span id="cargarGraficaFollowers" class="hide"><i class="fa fa-spinner fa-spin fa-2x"></i> Cargando datos...</span>
            <div id="chartCrecimientoFollowers" class="hide"
                data-json=""
                style="width:400px; height:400px;"
                data-url="{{ url('influenciador/estadisticas/followers') }}"
                ></div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <ul class="list-icon">
                <li>
                    <span class="li-icon"><i class="fa fa-user fa-2x"></i></span>
                    <h4>ALCANCES</h4>
                    <div id="chartAlcances"
                        data-real="{{ $model->alcance_real }}"
                        data-potencial="{{ $model->alcance_potencial }}"
                        style="min-width:250px; min-height:250px;"></div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="row"><div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div></div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 box-border-gray text-dark-gray no-padding">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <ul class="list-icon">
                <li>
                    <span class="li-icon"><i class="fa fa-user fa-2x"></i></span>
                    <h4>ENGAGEMENT RATE</h4>
                    <div class="text-center">
                        <br> <br>
                        <strong class="text-center btn btn-red-rounded" data-toggle="tooltip" data-placement="left" title="Fórmula: Favorites + 2*Replys + 2*Retweets / Total de Followers">
                            <h4 style="display: inline-block;">
                                ENGAGEMENT <br> <span id="engagement">{{ $model->engagement_rate }}%</span>
                            </h4>
                        </strong> <br>
                    </div>
                    <div id="chartEngagementRate" style="display:none"
                        data-json=""
                        style="min-width:300px; min-height:300px;"></div>
                </li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <ul class="list-icon">
                <li>
                    <span class="li-icon"><i class="fa fa-user fa-2x"></i></span>
                    <h4>INTERACCIONES</h4>
                    <div class="text-center">
                        <strong class="text-center btn btn-red-rounded" data-toggle="tooltip" data-placement="left" title="Fórmula: Total Favorites + Total Retweets">
                            <h4 style="display: inline-block;">
                                TOTALES <br> <span id="interacciones_totales">@formatNumber($model->interacciones_totales)</span>
                            </h4>
                            <i class="fa fa-arrow-down fa-2x" aria-hidden="true"></i>
                        </strong> <br>
                        <strong class="text-center btn btn-red-rounded" data-toggle="tooltip" data-placement="left" title="Fórmula: (Interacciones totales * 1000) % Total de followers">
                            <h4 style="display: inline-block;">
                                CADA 1000 <br> <span id="interacciones_cada_10000">@formatNumber($model->interacciones_cada_10000)</span>
                            </h4>
                            <i class="fa fa-arrow-down fa-2x" aria-hidden="true"></i>
                        </strong> <br>
                        <strong class="text-center btn btn-red-rounded" data-toggle="tooltip" data-placement="left" title="Total de Publicaciones">
                            <h4 style="display: inline-block;">
                                ACTIVIDAD <br>TOTAL <br> <span id="actividad_mes">@formatNumber($model->publicaciones_total)</span>
                            </h4>
                            <i class="fa fa-arrow-up fa-2x" aria-hidden="true"></i>
                        </strong>
                    </div>
                    <div class="separator"></div>
                    @php
                    $mejorDia  = empty($model->perfilTwitter) ? null : $model->perfilTwitter->bestDayToPublish();
                    $mejorHora = empty($model->perfilTwitter) ? null : $model->perfilTwitter->bestHourToPublish();
                    @endphp
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <p class="lead">Mejor día para publicar</p>
                            <p class="lead text-primary">{{ !is_null($mejorDia) ? $mejorDia->dia_semana : '---' }}</p>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <p class="lead">Mejor hora para publicar</p>
                            <p class="lead text-primary">{{ !is_null($mejorHora) ? $mejorHora->hora : '---' }}</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="row"><div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div></div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 box-border-gray text-dark-gray no-padding">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <ul class="list-icon">
                <li>
                    <span class="li-icon"><i class="fa fa-user fa-2x"></i></span>
                    <h4>PROPORCIÓN DE GÉNERO</h4>
                </li>
            </ul>
            @if ($model->totalHombres == 0 && $model->totalMujeres == 0)
                No existen datos para mostrar
            @else
                <div id="chartProporcionGenero"
                    data-hombres="{{ $model->totalHombres }}"
                    data-mujeres="{{ $model->totalMujeres }}"
                    data-followerstwitter="{{ empty($model->perfilTwitter) ? 0 : ($model->perfilTwitter->estadisPerfil ? $model->perfilTwitter->estadisPerfil->followers : 0) }}"
                    style="min-width:300px; min-height:300px;"></div>
            @endif
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <ul class="list-icon">
                <li>
                    <span class="li-icon"><i class="fa fa-user fa-2x"></i></span>
                    <h4>PROPORCIÓN PAÍS ORIGEN</h4>
                </li>
            </ul>

            @php
                $paises = $model->distribucion_paises;
            @endphp

            @if (count($paises['paises']) == 0)
                No existen datos para mostrar
            @else
                <div id="chartProporcionPais"
                    data-texts="{{ json_encode($paises['paises']) }}"
                    data-values="{{ json_encode($paises['cantidades']) }}"
                    style="width:380px; height:250px; text-align: center;"></div>
            @endif
        </div>
    </div>
</div>

<div class="row"><div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div></div>

{{--<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 box-border-gray text-dark-gray no-padding">
        <div class="col-xs-6 col-sm-6 col-md-6">

        </div>
        <div class="col-xs-6 col-sm-6 col-md-6"></div>
    </div>
</div>--}}