@extends('layouts.' . config('app.layout'))

@push('scripts-head')
    <script type="text/javascript">
        var urlRechazarPropuesta = '{{ url('influenciadoresxcampana/rechazarpropuesta') }}',
            urlAceptarPropuesta = '{{ url('influenciadoresxcampana/aceptarpropuesta') }}',
            urlChat = '{{ url('chat') }}';
    </script>
@endpush

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <h3 class="title">{{ strtoupper($title) }}</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    {{-- {{ $campanas->appends(Request::query())->links() }} --}}
                </div>

                @if (app('request')->has('filter'))
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="well well-sm text-success">
                            <span class="glyphicon glyphicon-zoom-in"></span> Resultados de la búsqueda
                        </div>
                    </div>
                @endif

                {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                    <p>Mostrando {{ $campanas->firstItem() }}-{{ $campanas->lastItem() }} de {{ $campanas->total() }} elementos.</p>
                </div> --}}
            </div>

            @forelse ($campanas as $model)
                <div class="row black-box row-eq-height row-propuesta" data-campana="{{ $model->campana->id }}">
                    <div class="col-xs-8 col-sm-8 col-md-4">
                        <h2>
                            {{ $model->campana->nombre }}
                        </h2>
                        <p class="light-gray">{{ $model->cantidad_publicaciones }} Publicaciones</p>
                        <p class="text-red">{{ $model->campana->hashtags }}</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-2">
                        <br>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <i class="fa fa-calendar fa-2x"></i>&nbsp; &nbsp;
                                </td>
                                <td>
                                    @date($model->campana->fecha_desde)<br />
                                    @date($model->campana->fecha_hasta)
                                </td>
                            </tr>
                        </table>
                        <br>
                        <p>
                            <!-- A los Influenciadores que participan en Campañas de Televisa
                            NO se les muestra en Precio de Publicación-->
                            @if (!$model->campana->getUserOwner()->inEntornoTelevisa())
                                {!! '<i class="fa fa-credit-card fa-2x"></i>&nbsp;&nbsp;' .
                                    $model->precio_publicaciones_currency !!}
                            @endif
                        </p>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-3"></div>
                    <div class="col-xs-6 col-sm-6 col-md-3 text-right">
                        <p>&nbsp;</p>
                        <button type="button" class="btn btn-red-circle no-loading btn-chat" data-toggle="tooltip" title="Conversar"
                            data-influenciador="{{ $model->influenciador->id }}"
                            data-usuario="{{ $model->influenciador->usuario->id }}"
                            data-campana="{{ $model->campana->id }}">
                            <i class="fa fa-comments-o fa-lg"></i>
                            @php
                                $mensajesNoLeidos = App\Models\Mensaje::getMensajesUsuarioCampana($model->campana->id, $model->influenciador->usuario->id, 0);
                            @endphp
                            @if (count($mensajesNoLeidos) > 0)
                                <span class="badge bg-red">{{ count($mensajesNoLeidos) }}</span>
                            @endif
                        </button>
                        @if ($model->isEsperandoRespuesta())
                            <a class="no-loading btn-rechazar" data-toggle="tooltip"
                                title="Rechazar Propuesta" data-campana="{{ $model->campana->id }}">
                                <span class="fa-stack fa-lg text-red">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-trash fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                            <a class="no-loading btn-aceptar" data-toggle="tooltip" title="Aceptar Propuesta"
                                data-campana="{{ $model->campana->id }}"
                                data-url-terminos="{{ url('campanas/' . $model->campana->id . '/terminos') }}">
                                <span class="fa-stack fa-lg text-red">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-check fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        @endif

                        @if ($model->isAceptado())
                            <a data-toggle="tooltip" title="Ver Publicaciones"
                                href="{{ url('influenciadores/miscampanas/'.$model->campana->id.'/posts') }}">
                                <span class="fa-stack fa-lg text-red" style="font-size: 1.5em;">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-eye fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        @endif
                    </div>
                </div>
            @empty
                <strong>No se encontraron campañas.</strong>
            @endforelse

            <div class="text-center">
                {{-- $campanas->appends(Request::query())->links() --}}
            </div>

            <div class="modal fade" id="motivo-rechazo-modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                <h4>Motivo del rechazo</h4>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <textarea name="motivo_rechazo_text" class="form-control" id="motivo_rechazo_text"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-red-rounded" id="btn-send-rechazo"
                                data-campana="">Rechazar</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="aceptar-propuesta-modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <h4>¿Estas de acuerdo con la propuesta y con sus <a href="#" id="hrefCampanaTerminos" class="text-primary" style="text-decoration: underline;">términos y condiciones?</a></h4>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-red-rounded" id="btn-send-aceptar"
                                data-campana="">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="container-posts">
        @include('common.chat')
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('js/influenciador_miscampanas.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/chat.js') }}" type="text/javascript"></script>
@endpush
