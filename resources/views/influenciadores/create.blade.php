@section('section_title', 'Registrar')

@include($response_info['resource'] . '._form', [
    'config_form' => [
        'id'    => 'influenciador-form',
        'model' => $model,
        'store' => $response_info['resource'] . '.store',
    ],
    'catalogs' => $catalogs,
])
