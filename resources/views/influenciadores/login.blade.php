<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="csrf-param" content="_token" />

    <link rel="shortcut icon" type="image/png" href="">
    <link href="{{ asset('js/plugins/tooltipster/dist/css/tooltipster.bundle.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('js/plugins/tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-light.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- <link rel="shortcut icon" type="image/x-icon" href=""> -->

    <title>
        {{ config('app.title') }}
        @hasSection('title')
            - @yield('title')
        @endif
    </title>

    <link href="{{ asset('node_modules/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('node_modules/gentelella/vendors/animate.css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('node_modules/gentelella/build/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
</head>

<body class="login">
    <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>
        <div class="login_wrapper">
            <h1>¡BIENVENIDO A eGLOW!</h1>
            <div class="animate form login_form">
                <section class="login_content">
                    <h2>Influenciador</h2>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        @if (session('status'))
                            <div class="alert alert-warning">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ csrf_field() }}

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control has-feedback-left" placeholder="E-mail" name="email" value="{{ $email or old('email') }}" required autofocus autocomplete="off">
                            <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="form-control has-feedback-left" placeholder="Contraseña" name="password" required>
                            <span class="fa fa-key form-control-feedback left" aria-hidden="true"></span>
                        </div>

                        @if ($errors->has('email') || $errors->has('password'))
                            <div class="alert alert-danger col-md-12 col-sm-12 col-xs-12">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ $errors->first() }}
                            </div>
                        @endif

                        <div>
                            <button type="submit" class="btn btn-eglow bg-eglow submit">
                                Iniciar sesión <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                            </button>

                            <span class="submit"><a href="{{ url('/password/reset') }}">Olvidé</a> mi contraseña</span>
                        </div>
                        <div class="clearfix"></div>
                        <br><br>
                        <p>Ingresa con</p>
                        <a class="btn btn-eglow bg-eglow btn-redes btn-block text-large" data-toggle="tooltip" title="Iniciar sesión con Twitter" href="{{ url('/auth/twitter/login') }}">
                            <span><i class="fa fa-twitter"></i></span>
                            <span>Twitter</span>
                        </a>

                        <a class="btn btn-eglow bg-facebook btn-redes btn-block text-large" data-toggle="tooltip" title="Iniciar sesión con Facebook" href="{{ url('auth/facebook/login') }}">
                            <span><i class="fa fa-facebook"></i></span>
                            <span>Facebook</span>
                        </a>
                    </form>
                    <div class="clearfix"></div>
                </section>
            </div>
            <br><br>
            <p>Copyright &copy; {{ date('Y') }} <a href="http://eglow.cl/">eGLOW</a></p>
            <!-- <div id="register" class="animate form registration_form">
                <section class="login_content">
                    <form>
                        <h1>Recuperar contraseña</h1>
                        <div>
                            <input type="email" class="form-control" placeholder="Email" required="" />
                        </div>
                        <div>
                            <a class="btn btn-default submit" href="index.html">
                                Enviar <i class="fa fa-exchange"></i>
                            </a>
                            <a href="#signin" class="to_register"> Iniciar sesión </a>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <p class="change_link"></p>

                            <div class="clearfix"></div>

                            <div>
                                <h1>{{ config('app.title') }}</h1>
                                <p><strong>Copyright &copy; 2016 <a href="http://eglow.cl/">eGLOW</a></strong></p>
                            </div>
                        </div>
                    </form>
                </section>
            </div> -->
        </div>
    </div>

    <script src="{{ asset('node_modules/gentelella/vendors/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/gentelella/vendors/nprogress/nprogress.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/tooltipster/dist/js/tooltipster.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/gentelella/build/js/custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/notify.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        if (typeof NProgress != 'undefined') {
            $(document).ready(function () {
                NProgress.start();
            });

            $(window).load(function () {
                NProgress.done();
            });
        }

        $.notify.defaults({autoHideDelay: 5000});
        $.notify('Estamos trabajando para mejorar el servicio, \nalgunos datos no estarán disponibles.', 'info');
    </script>

    @stack('scripts')
</body>

</html>

