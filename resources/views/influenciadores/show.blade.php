@extends('layouts.' . config('app.layout') )

@push('styles')
    <link rel="stylesheet" href="{{ asset('js/plugins/fancyBox/jquery.fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/jQCloud/dist/jqcloud.css') }}">
@endpush

@push('scripts-head')
    <script type="text/javascript">
        var id_influencer  = '{{ empty($model) ? 0 : $model->id }}';
    </script>
@endpush

@section('title', $title)

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li><a href="{{ url($response_info['resource']) }}">{{ $title }}</a></li>
        <li class="active">Ver</li>
    </ol>
@endsection

@section('content')
    @if (!Auth::user()->isCelebridad())
        <p>
            <a class="btn btn-default" href="{{ \URL::previous() }}" role="button">
                <span class="glyphicon glyphicon-arrow-left"></span> Regresar
            </a>
        </p>
    @endif

    <div class="row">
        <div class="col-md-11 col-sm-11 col-xs-12" role="tabpanel" data-example-id="togglable-tabs">
            @if(empty($model))
                @include('common.errors', ['response_info' => $response_info])
            @else
                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                    <li role="presentation" class="active">
                        <h2>
                            <a href="#tab_perfil" id="perfil-tab" role="tab" data-toggle="tab" aria-expanded="true">
                                <i class="fa fa-user"></i>
                                PERFIL
                            </a>
                        </h2>
                    </li>
                    <li role="presentation">
                        <h2>
                            <a href="#tab_estadisticas" id="estadisticas-tab" role="tab" data-toggle="tab" aria-expanded="false">
                                <i class="fa fa-bar-chart"></i>
                                ESTADÍSTICAS
                            </a>
                        </h2>
                    </li>
                    <li role="presentation">
                        <h2>
                            <a href="#tab_contenido" id="contenido-tab" role="tab" data-toggle="tab" aria-expanded="false">
                                <i class="fa fa-file-text-o"></i>
                                CONTENIDO
                            </a>
                        </h2>
                    </li>
                    <li role="presentation">
                        <h2>
                            <a href="#tab_comunidad" id="comunidad-tab" role="tab" data-toggle="tab" aria-expanded="false">
                                <i class="fa fa-users"></i>
                                COMUNIDAD
                            </a>
                        </h2>
                    </li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="tab_perfil" aria-labelledby="perfil-tab">
                        @include($response_info['resource'] . '.show_perfil')
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tab_estadisticas" aria-labelledby="estadisticas-tab">
                        @include($response_info['resource'] . '.show_estadisticas')
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tab_contenido" aria-labelledby="contenido-tab">
                        @include($response_info['resource'] . '.show_contenido')
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tab_comunidad" aria-labelledby="comunidad-tab">
                        @include($response_info['resource'] . '.show_comunidad')
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('js/plugins/Chart.js/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('js/plugins/fancyBox/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('js/plugins/jQCloud/dist/jqcloud.js') }}"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="{{ asset('js/plugins/zingchart/zingchart.min.js') }}"></script>
    <script src="{{ asset('js/plugins/echart/echarts.js') }}"></script>
    <script src="{{ asset('js/influenciador_profile.js') }}"></script>
    <script src="{{ asset('js/influenciador_contenido.js') }}"></script>
    <!--<script src="{{ asset('js/influenciador_comunidad.js') }}"></script>-->
    <script src="{{ asset('js/influenciador_estadisticas.js') }}"></script>
@endpush
