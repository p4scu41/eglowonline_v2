<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 box-border-gray text-dark-gray no-padding">
        <div class="col-xs-10 col-sm-10 col-md-8" style="padding-left: 0;">
            <div style="float: left; padding-right: 10px;">
                <img src="{{ str_replace('_normal', '', $model->imgPerfil) }}" style="min-width: 110px; max-width: 115px;"></img>
            </div>
            <h3 class="text-black">{{ $model->usuario->nombre }}</h3>
            <p>{{ $model->tipoInfluenciador->descripcion }}</p>
            <p><strong class="text-red">{{ $model->perfilTwitter ? $model->perfilTwitter->screen_name : $model->nombre }}</strong> <span class="cantidad_seguidores red-box">@formatNumber($model->totalFollowers)</span></p>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-4 text-right">
            <h3>&nbsp;</h3>
        </div>
    </div>
</div>

<div class="row"><div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div></div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 box-border-gray text-dark-gray no-padding">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <ul class="list-icon">
                <li>
                    <span class="li-icon"><img src="{{ asset('img/iconos/seguidores.png') }}" width="32"></span>
                    <h4>AUDIENCIA TOTAL</h4>

                    {{--<div class="pull-right">
                        <span class="btn btn-xs btn-primary btn_custom_export" data-type="png" data-target="chartSeguidores"><i class="fa fa-download"></i></span>
                        <span class="btn btn-xs btn-primary btn_custom_export" data-type="print" data-target="chartSeguidores"><i class="fa fa-print"></i></span>
                        <span class="btn btn-xs btn-primary btn_custom_export" data-type="pdf" data-target="chartSeguidores"><i class="fa fa-file-pdf-o"></i></span>
                    </div>--}}

                    @php
                        $seguidores = $model->json_grafica_total_seguidores;
                    @endphp

                    <div id="chartSeguidores"
                        data-texts="{{ json_encode($seguidores['red_social']) }}"
                        data-values="{{ json_encode($seguidores['seguidores']) }}"
                        data-twitter="{{ empty($model->perfilTwitter) ? 0 : ($model->perfilTwitter->estadisPerfil ? $model->perfilTwitter->estadisPerfil->followers : 0) }}"
                        data-facebook="10"
                        style="min-width:300px; min-height:250px;"></div>
                </li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <ul class="list-icon">
                <li>
                    <span class="li-icon"><img src="{{ asset('img/iconos/ha-colaborado-con.png') }}" width="32"></span>
                    <h4>MARCAS PREFERIDAS</h4>
                    {{ $model->getMarcasMeGusta(true) }}
                </li>
                <li>
                    <span class="li-icon"><img src="{{ asset('img/iconos/marcas-evitadas.png') }}" width="32"></span>
                    <h4>NO DESEO COLABORAR CON MARCA/INDUSTRÍA</h4>
                    {{ $model->getMarcasNoDeseoColaborado(true) }}
                </li>
                <li>
                    <span class="li-icon"><img src="{{ asset('img/iconos/ha-colaborado-con.png') }}" width="32"></span>
                    <h4>HA COLABORADO CON</h4>
                    {{ $model->getMarcasHeColaborado(true) }}
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="row"><div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div></div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 box-border-gray text-dark-gray no-padding">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <ul class="list-icon">
                <li>
                    <span class="li-icon"><img src="{{ asset('img/iconos/biografia.png') }}" width="32"></span>
                    <h4>BIOGRAFÍA</h4>
                    {{ $model->biografia }}
                </li>
                <li>
                    <span class="li-icon"><img src="{{ asset('img/iconos/profesion.png') }}" width="32"></span>
                    <h4>PROFESIÓN</h4>
                    {{ $model->getTextProfesiones() }}
                </li>
                <li>
                    <span class="li-icon"><img src="{{ asset('img/industria/' . $model->industria->id . '.png') }}" width="32"></span>
                    <h4>CATEGORÍA DE INFLUENCIA</h4>
                    {{ $model->industria->nombre }}
                </li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <ul class="list-icon">
                <li>
                    <span class="li-icon"><img src="{{ asset('img/iconos/intereses.png') }}" width="32"></span>
                    <h4>INTERESES</h4>
                    {{ $model->palabrasClavePalabra(true) }}
                </li>
                <li>
                    <span class="li-icon"><img src="{{ asset('img/rango_etario/' . $model->rangoEtario->id . '.png') }}" width="32"></span>
                    <h4>RANGO ETARIO</h4>
                    {{ $model->rangoEtario->descripcion }}
                </li>
                <li>
                    <span class="li-icon"><img src="{{ asset('img/iconos/pais.png') }}" width="32"></span>
                    <h4>PAÍS</h4>
                    {{ $model->lugar_nacimiento }}
                </li>
                <li>
                    <span class="li-icon"><img src="{{ asset('img/iconos/lugar-influencia.png') }}" width="32"></span>
                    <h4>LUGAR DE INFLUENCIA</h4>
                    {{ $model->pais->pais }}
                </li>
                <li>
                    <span class="li-icon"><img src="{{ asset('img/genero/' . $model->genero . '.png') }}" width="32"></span>
                    <h4>GENERO</h4>
                    {{ $model->descripcionGenero }}
                </li>
            </ul>
        </div>
    </div>
</div>