@extends('layouts.' . config('app.layout') )

@push('scripts-head')
    <script type="text/javascript">
        var urlInfluenciadorSearchByText   = '{{ url('influenciadores/searchbytext') }}',
            urlPalabraClaveSearchByText    = '{{ url('palabrasclaves/searchbytext') }}',
            urlInfluenciadorAdvancedSearch = '{{ url('influenciadores/advancedsearch') }}',
            urlInfluenciadorClic           = '{{ url('influenciadores/clic') }}',
            urlSearchLugarNacimientoByText = '{{ url('influenciadores/searchlugarnacimientobytext') }}',
            urlGetLogotipoMarca            = '{{ url('marcas/logotipo') }}',
            urlGetImagenProducto           = '{{ url('productos/imagen') }}',
            urlGetLogotipoAgencia          = '{{ url('agencias/logotipo') }}',
            urlCampanaIndex                = '{{ url('campanas') }}',
            redesSocialesActivas           = {!! $catalogs['red_social'] !!};
    </script>
@endpush

@section('content')

@php
    $model->loadInfluenciadores();
@endphp

<div class="row">
    <div class="col-md-11 col-sm-11 col-xs-12" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active">
                <h2>
                    <a href="#tab_campana" id="campana-tab" role="tab" data-toggle="tab" aria-expanded="true">
                        <i class="fa fa-paper-plane-o"></i>
                        CAMPAÑA
                        <i class="fa fa-check-circle"></i>
                    </a>
                </h2>
            </li>
            <li role="presentation" class="{{ $model->exists ? '' : 'disabled' }}">
                <h2>
                    <a href="#tab_influencer" id="influencer-tab" role="tab" data-toggle="tab" aria-expanded="false">
                        <i class="fa fa-users"></i>
                        INFLUENCER
                        <i class="fa fa-check-circle-o"></i>
                    </a>
                </h2>
            </li>
            <li role="presentation" class="{{ $model->exists ? '' : 'disabled' }}">
                <h2>
                    <a href="#tab_panorama" role="tab" id="panorama-tab" data-toggle="tab" aria-expanded="false">
                        <i class="fa fa-file-text-o"></i>
                        PANORAMA
                        <i class="fa fa-check-circle-o"></i>
                   </a>
                </h2>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_campana" aria-labelledby="campana-tab">
                @include($response_info['resource'] . '._form_datos_generales', [
                    'mode'     => $model,
                    'catalogs' => $catalogs,
                    'labels'   => $labels,
                ])
            </div>

            <div role="tabpanel" class="tab-pane fade" id="tab_influencer" aria-labelledby="influencer-tab">
                @include($response_info['resource'] . '._form_influenciadores', [
                    'mode'     => $model,
                    'catalogs' => $catalogs,
                    'labels'   => $labels,
                ])
            </div>

            <div role="tabpanel" class="tab-pane fade" id="tab_panorama" aria-labelledby="panorama-tab">
                @include($response_info['resource'] . '._form_panorama')
            </div>
        </div>
    </div>
</div>

<script id="influenciador-item-result-search-hbs" type="text/x-handlebars-template">
    @include($response_info['resource'] . '/item_influenciador_search', [
        'btnAdd' => true,
    ])
</script>

<script id="influenciador-item-selected-hbs" type="text/x-handlebars-template">
    @include($response_info['resource'] . '/item_influenciador_seleccionado')
</script>

<script id="palabra-clave-item-hbs" type="text/x-handlebars-template">
    @include('influenciadores/palabrasclave_tpl')
</script>

<script id="hashtag-item-hbs" type="text/x-handlebars-template">
    @include($response_info['resource'] . '/hashtag_tpl')
</script>

@endsection

@push('styles')
    <link href="{{ asset('node_modules/gentelella/vendors/normalize-css/normalize.css') }}" rel="stylesheet">
    <link href="{{ asset('node_modules/gentelella/vendors/ion.rangeSlider/css/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('node_modules/gentelella/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('js/plugins/fancyBox/jquery.fancybox.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('node_modules/twitter-text/twitter-text.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/gentelella/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/handlebars/dist/handlebars.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/fancyBox/jquery.fancybox.js') }}"></script>
    <script src="{{ asset('js/campana_form.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/campana_influenciador_popup.js') }}" type="text/javascript"></script>
@endpush
