<div class="row row-post">
    <div class="col-xs-2 col-sm-2 col-md-1 container-avatar">
        <img src="{{ str_replace('_normal', '', $influenciador->imgPerfil) }}" class="avatar">
        <span class="cantidad_seguidores red-box">
            {{ \App\Helpers\Helper::formatNumber($influenciador->total_followers) }}
        </span>
    </div>
    <div class="col-xs-10 col-sm-10 col-md-11 body">
        <div class="row blue-box">
            <div class="col-xs-4 col-sm-4 col-md-4">
                <p>{{ $influenciador->nombre }}</p>
                <p>{{ $influenciador->tipoInfluenciador->descripcion }}</p>
                <p class="text-red">
                    {{ $influenciador->perfilTwitter ? $influenciador->perfilTwitter->screen_name : $influenciador->nombre }}
                </p>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3"></div>

            @php
                $pubXInfluXCampTwitter  = $influenciador->pubXInfluXCampTwitter($model->id);
                $pubXInfluXCampFacebook = $influenciador->pubXInfluXCampFacebook($model->id);
                $participacionCampana   = $influenciador->influenciadorXCampana($model->id);
                $noTieneRedSocial      = true; // Bandera para determinar si tiene o no alguna red social asociada
            @endphp

            <div class="col-xs-3 col-sm-3 col-md-3 container-opciones-redes-sociales text-left">
                {{-- ================================ Twitter ========================================== --}}
                @if (!empty($influenciador->perfilTwitter) || $model->agencia->id==57)
                    @if (!empty($influenciador->perfilTwitter->oauth_token) || $model->agencia->id==57)
                        @php
                        $noTieneRedSocial = false;
                        @endphp

                        <div class="cantidad_publicaciones_widget twitter">
                            <a class="cantidad-publicaciones twitter black-box tooltipster"
                                title="{{ (empty($pubXInfluXCampTwitter) ? 0 : $pubXInfluXCampTwitter->cantidad_publicaciones) . ' Publicaciones en Twitter' }}"
                                data-cantidadpublicaciones="{{ empty($pubXInfluXCampTwitter) ? 0 : $pubXInfluXCampTwitter->cantidad_publicaciones }}"
                                data-redsocial="{{ App\Models\RedSocial::$TWITTER }}"
                                data-preciopublicacion="{{ empty($pubXInfluXCampTwitter) ?
                                    (empty($influenciador->perfilTwitter) ? 0 : $influenciador->perfilTwitter->precio_publicacion) :
                                    $pubXInfluXCampTwitter->precio_publicacion }}">
                                {{ empty($pubXInfluXCampTwitter) ? 0 : $pubXInfluXCampTwitter->cantidad_publicaciones }}
                            </a>
                            <span class="btn-cantidad-publicaciones twitter btn-red">
                                <span>
                                    <a class="{{ empty($pubXInfluXCampTwitter) ?
                                        'btn-plus-cantidad-publicaciones' :
                                        ($pubXInfluXCampTwitter->isAceptado() ? 'disabled' : 'btn-plus-cantidad-publicaciones') }}">
                                        <i class="fa fa-sort-asc"></i>
                                    </a>
                                    <a class="{{ empty($pubXInfluXCampTwitter) ?
                                        'btn-minus-cantidad-publicaciones' :
                                        ($pubXInfluXCampTwitter->isAceptado() ? 'disabled' : 'btn-minus-cantidad-publicaciones') }}">
                                        <i class="fa fa-sort-desc"></i>
                                    </a>
                                </span>
                                <span><i class="fa fa-twitter text-black fa-2x"></i></span>
                            </span>
                            <span class="precio-publicaciones">$
                                {{ number_format(doubleval(empty($pubXInfluXCampTwitter) ?
                                    (empty($influenciador->perfilTwitter) ? 0 : $influenciador->perfilTwitter->precio_publicacion) :
                                    $pubXInfluXCampTwitter->calcPrecioPublicaciones())) }}
                                MXN</span>
                        </div>
                    @endif
                @endif
                {{-- =================================== Facebook ======================================= --}}
                @if (!empty($influenciador->perfilFacebook) || $model->agencia->id==57)
                    @if (!empty($influenciador->perfilFacebook->page_access_token) || $model->agencia->id==57)
                        @php
                        $noTieneRedSocial = false;
                        @endphp

                        <div class="cantidad_publicaciones_widget facebook">
                            <a class="cantidad-publicaciones facebook black-box tooltipster"
                                title="{{ (empty($pubXInfluXCampFacebook) ? 0 : $pubXInfluXCampFacebook->cantidad_publicaciones) . ' Publicaciones en Facebook' }}"
                                data-cantidadpublicaciones="{{ empty($pubXInfluXCampFacebook) ? 0 : $pubXInfluXCampFacebook->cantidad_publicaciones }}"
                                data-redsocial="{{ App\Models\RedSocial::$FACEBOOK }}"
                                data-preciopublicacion="{{ empty($pubXInfluXCampFacebook) ?
                                    (empty($influenciador->perfilFacebook) ? 0 : $influenciador->perfilFacebook->precio_publicacion) :
                                    $pubXInfluXCampFacebook->precio_publicacion }}">
                                    {{ empty($pubXInfluXCampFacebook) ? 0 : $pubXInfluXCampFacebook->cantidad_publicaciones }}
                            </a>
                            <span class="btn-cantidad-publicaciones facebook btn-red">
                                <span>
                                    <a class="{{ empty($pubXInfluXCampFacebook) ?
                                        'btn-plus-cantidad-publicaciones' :
                                        ($pubXInfluXCampFacebook->isAceptado() ? 'disabled' : 'btn-plus-cantidad-publicaciones') }}">
                                        <i class="fa fa-sort-asc"></i>
                                    </a>
                                    <a class="{{ empty($pubXInfluXCampFacebook) ?
                                        'btn-minus-cantidad-publicaciones' :
                                        ($pubXInfluXCampFacebook->isAceptado() ? 'disabled' : 'btn-minus-cantidad-publicaciones') }}">
                                        <i class="fa fa-sort-desc"></i>
                                    </a>
                                </span>
                                <span><i class="fa fa-facebook text-black fa-2x"></i></span>
                            </span>
                            <span class="precio-publicaciones">$
                                {{ number_format(doubleval(empty($pubXInfluXCampFacebook) ?
                                    (empty($influenciador->perfilFacebook) ? 0 : $influenciador->perfilFacebook->precio_publicacion) :
                                    $pubXInfluXCampFacebook->calcPrecioPublicaciones())) }}
                            MXN</span>
                        </div>
                    @endif
                @endif
                {{-- ========================================================================== --}}

                @if ($noTieneRedSocial && $model->agencia->id!=57)
                    <div class="alert alert-danger text-center" role="alert" data-toggle="tooltip"
                        title="Antes de enviar Publicaciones, el Influenciador debe asociar sus cuentas en la sección Redes Sociales de su Perfil">
                        No tiene Red <br>Social Asociada
                    </div>
                @endif
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 container-opciones-posts">
                <div><p>&nbsp;</p></div>
                <button type="button" class="btn btn-red-circle no-loading btn-chat" data-toggle="tooltip" title="Conversar"
                    data-influenciador="{{ $influenciador->id }}"
                    data-usuario="{{ $influenciador->usuario->id }}"
                    data-campana="{{ $model->id }}">
                    <i class="fa fa-comments-o fa-lg"></i>
                    @php
                        $mensajesNoLeidos = App\Models\Mensaje::getMensajesUsuarioCampana($model->id ,$influenciador->usuario->id, 0);
                    @endphp
                    @if (count($mensajesNoLeidos) > 0)
                        <span class="badge bg-red">{{ count($mensajesNoLeidos) }}</span>
                    @endif
                </button>
                @if ($participacionCampana)
                    @if (!$participacionCampana->isAceptado())
                    <a class="btn btn-red-circle btn-delete" data-toggle="tooltip" title="Eliminar"
                        data-influenciador="{{ $influenciador->id }}"
                        data-campana="{{ $model->id }}">
                        <i class="fa fa-trash-o fa-lg"></i>
                    </a>
                    @endif
                    @if ($participacionCampana->isInsertado() || $participacionCampana->isRechazado())
                        <button type="button" class="btn btn-red-circle no-loading btn-send-propuesta-posts"
                            data-toggle="tooltip" title="Enviar propuesta de Posts"
                            data-influenciador="{{ $influenciador->id }}"
                            data-campana="{{ $model->id }}">
                            <i class="fa fa-share"></i>
                        </button>
                    @elseif ($participacionCampana->isAceptado())
                        <span class="fa-stack fa-lg text-red btn-toggle-list-posts cursor-pointer" data-toggle="tooltip"
                            title="Visualizar Posts">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-chevron-right fa-stack-1x fa-inverse"></i>
                        </span>
                    @endif
                @endif
            </div>
        </div>
    </div>

    <div class="col-xs-2 col-sm-2 col-md-1"></div>
    <div class="col-xs-10 col-sm-10 col-md-11 blue-box container-list-posts off" style="display: none;">
        @php
            $publicaciones = $influenciador->publicacionesCampana($model->id);
            $isEsperandoAprobacionInfluencer = false;
            $isAprobadoInfluencer = false;
        @endphp

        @foreach ($publicaciones as $publicacion)
            @php
                $isEsperandoAprobacionInfluencer = $publicacion->isEsperandoAprobacionInfluencer();
                $isAprobadoInfluencer = $publicacion->isAprobadoInfluencer();
            @endphp

            <div class="row post-container {!! !$publicacion->isEsperandoAprobacionInfluencer() ? 'aprobado' : '' !!}"
                data-post="{{ $publicacion->id }}"
                data-redsocial="{{ $publicacion->red_social_id }}"
                data-estado="{{ $publicacion->estado }}">
                {{-- ====== Icono Estado de Publicación ====== --}}
                <div class="col-xs-1 col-sm-1 col-md-1">
                    <div class="input-group">
                        <span class="input-group-addon" style="background-color: transparent; border: 0;">
                            <span data-post="{{ $publicacion->id }}" class="fa-stack fa-lg tooltipster
                            {!! $publicacion->isEsperandoAprobacionInfluencer() ? ' text-lightergray" title="Pendiente de Aprobación"' : (
                                $publicacion->isAprobadoInfluencer() ? ' btn-publicar-post btn animated infinite pulse" title="Aprobado por Influenciador"': (
                                    $publicacion->isAprobadoPorPublicar() ? ' text-black" title="Aprobado por Agencia"': (
                                        $publicacion->isAprobadoPublicado() ? ' text-red" title="Publicado"' : (
                                            $publicacion->isErrorPublicar() ? '" title=\'Error al Publicar: '.$publicacion->error_publicar.'\'' : ''
                                        )
                                    )
                                )
                            ) !!}>
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-stack-1x fa-inverse fa-{!! $publicacion->isEsperandoAprobacionInfluencer() ? 'hourglass-o text-white' : (
                                    $publicacion->isAprobadoInfluencer() ? 'thumbs-o-up btn-red-rounded animated infinite zoomIn' : (
                                        $publicacion->isAprobadoPorPublicar() ? 'check' : (
                                            $publicacion->isAprobadoPublicado() ? 'check' : (
                                                $publicacion->isErrorPublicar() ? 'close text-red error' : ''
                                            )
                                        )
                                    )
                                ) !!}"></i>
                            </span>
                        </span>
                        <div class="form-control float-initial" style="visibility: hidden; padding: 0; margin: 0; width: 0;">
                            <p>&nbsp;</p><p>&nbsp;</p>
                        </div>
                    </div>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="input-group container-file-input">
                        {{-- ====== Icono de la Red Social ====== --}}
                        <span class="input-group-addon tooltipster {!! $publicacion->isEsperandoAprobacionInfluencer() ? ' bg-lightergray" title="Pendiente de Aprobación"' : (
                                $publicacion->isAprobadoInfluencer() ? '" title="Aprobado por Influenciador"' : (
                                    $publicacion->isAprobadoPorPublicar() ? '" title="Aprobado por Agencia"' : (
                                        $publicacion->isAprobadoPublicado() ? ' icon-red" title="Publicado"' : (
                                            $publicacion->isErrorPublicar() ? '" title=\'Error al Publicar: '.$publicacion->error_publicar.'\'' : ''
                                        )
                                    )
                                )
                            ) !!}>
                            <i class="fa fa-{!! \App\Models\RedSocial::getRedSocialText($publicacion->red_social_id); !!} fa-2x {!! $publicacion->isEsperandoAprobacionInfluencer() ? ' text-white"' : (
                                $publicacion->isAprobadoInfluencer() ? ' text-white"': (
                                    $publicacion->isAprobadoPorPublicar() ? '"': (
                                        $publicacion->isAprobadoPublicado() ? '"' : (
                                            $publicacion->isErrorPublicar() ? ' text-red error"' : ''
                                        )
                                    )
                                )
                            ) !!}></i>
                        </span>

                        @if ($publicacion->isEsperandoAprobacionInfluencer())
                            <textarea {!! $publicacion->isTwitter() ? 'data-rule-maxlength="140" maxlength="140"' : '' !!} rows="4" data-toggle="tooltip" placeholder="Post" title="Post" class="form-control"
                                name="contenido" data-publicacion="{{ $publicacion->id }}">{{ $publicacion->contenido == '' ? $model->hashtags : $publicacion->contenido }}</textarea>
                        @else
                            <span data-toggle="tooltip" title="Post" class="form-control cursor-not-allowed" style="min-height: 95px;">
                                {{ $publicacion->contenido }}
                            </span>
                        @endif
                    </div>

                    <div id="errorPublicacion_{{ $publicacion->id }}" class="kv-fileinput-error file-error-message" style="display: none;"></div>
                </div>

                <div class="col-xs-2 col-sm-2 col-md-2 text-center" style="padding: 0;">
                    <div class="kv-avatar center-block">
                        <input type="file" name="archivo_multimedia"
                            class="bootstrap-fileinput file-loading {!! !$publicacion->isEsperandoAprobacionInfluencer() ? 'aprobado' : '' !!}"
                            data-publicacion="{{ $publicacion->id }}"
                            data-influenciador="{{ $influenciador->id }}"
                            data-campana="{{ $model->id }}"
                            data-redsocial="{{ $publicacion->red_social_id }}"
                            {!! $publicacion->getArchivoMultimedia() ? '
                                    data-multimedia="'.asset($publicacion->getArchivoMultimedia()).'"
                                    data-type="'.$publicacion->getTypeMultimedia().'"
                                    data-size="'.$publicacion->getSizeMultimedia().'"
                                    data-filetype="'.$publicacion->getFileTypeMultimedia().'"
                                    '
                                : '' !!}>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lg fa-calendar"></i></span>
                        @if ($publicacion->isEsperandoAprobacionInfluencer())
                            <input class="form-control datepicker" data-toggle="tooltip" placeholder="Fecha Publicación"
                                title="Fecha publicación" name="fecha" type="text" value="@datetime_short($publicacion->fecha_hora_publicacion)"
                                data-publicacion="{{ $publicacion->id }}" aria-required="true">
                        @else
                            <span class="form-control cursor-not-allowed" data-toggle="tooltip" placeholder="Fecha Publicación" title="Fecha publicación">
                                @datetime_short($publicacion->fecha_hora_publicacion)
                            </span>
                        @endif
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lg fa-clock-o"></i></span>
                        @if ($publicacion->isEsperandoAprobacionInfluencer())
                            <input class="form-control timepicker" data-toggle="tooltip" name="hora" type="text"
                                data-publicacion="{{ $publicacion->id }}" placeholder="Hora Publicación" title="Hora publicación"
                                value="@time_long($publicacion->fecha_hora_publicacion)" aria-required="true">
                        @else
                            <span class="form-control cursor-not-allowed" data-toggle="tooltip" placeholder="Hora Publicación" title="Hora publicación">
                                @time_long($publicacion->fecha_hora_publicacion)
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach

        @if ($isEsperandoAprobacionInfluencer)
            <div class="col-xs-2 col-sm-2 col-md-1"></div>
            <div class="col-xs-10 col-sm-10 col-md-11 text-right">
                <a class="btn btn-red-rounded no-loading btn-send-posts-influencer"
                    data-influenciador="{{ $influenciador->id }}"
                    data-campana="{{ $model->id }}">
                    {{ $influenciador->tienePublicacionPendiente($model->id) ? 'Actualizar contenidos' : 'Enviar' }}
                </a>
            </div>
        {{-- @elseif ($isAprobadoInfluencer)
            <div class="col-xs-2 col-sm-2 col-md-1"></div>
            <div class="col-xs-10 col-sm-10 col-md-11 text-right">
                <a class="btn btn-red-rounded no-loading btn-send-publicar-posts-influencer"
                    data-influenciador="{{ $influenciador->id }}"
                    data-campana="{{ $model->id }}">
                    Publicar
                </a>
            </div> --}}
        @endif
    </div>
</div>
