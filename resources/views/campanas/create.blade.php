@section('section_title', 'Registrar')

@include($response_info['resource'] . '._form', [
    'config_form' => [
        'id'                   => 'form-campana',
        'enctype'              => 'multipart/form-data',
        'model'                => $model,
        'store'                => $response_info['resource'] . '.store',
        'div_form_group_class' => 'col-xs-12 col-sm-12 col-md-12',
    ],
    'catalogs' => $catalogs,
])
