@extends('layouts.' . config('app.layout') )

@section('content')

@php
    $model->loadInfluenciadoresAprobados();
@endphp

<p>
    <a class="btn btn-default" href="{{ \URL::previous() }}" role="button">
        <span class="glyphicon glyphicon-arrow-left"></span> Regresar
    </a>
</p>
<div class="row">
    <div class="col-md-11 col-sm-11 col-xs-12" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active">
                <h2>
                    <a href="#tab_panorama" id="panorama-tab" role="tab" data-toggle="tab" aria-expanded="true">
                        <i class="fa"></i>
                        PANORAMA
                        <i class="fa fa-check-circle-o"></i>
                    </a>
                </h2>
            </li>
            <li role="presentation">
                <h2>
                    <a href="#tab_posts" id="posts-tab" role="tab" data-toggle="tab" aria-expanded="false">
                        <i class="fa fa-file-text-o"></i>
                        POSTS
                        <i class="fa fa-check-circle-o"></i>
                    </a>
                </h2>
            </li>
            <!--<li role="presentation">
                <h2>
                    <a href="#tab_estadisticas" id="estadisticas-tab" role="tab" data-toggle="tab" aria-expanded="false">
                        <i class="fa fa-file-text-o"></i>
                        ESTADISTICAS
                        <i class="fa fa-check-circle-o"></i>
                    </a>
                </h2>
            </li>-->
        </ul>
        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_panorama" aria-labelledby="panorama-tab">
                @include($response_info['resource'] . '.panorama')
            </div>

            <div role="tabpanel" class="tab-pane fade" id="tab_posts" aria-labelledby="posts-tab">
                @include($response_info['resource'] . '.posts')
            </div>

            <!-- <div role="tabpanel" class="tab-pane fade" id="tab_estadisticas" aria-labelledby="estadisticas-tab">
                {{-- @include($response_info['resource'] . '.estadisticas') --}}
            </div> -->
        </div>
    </div>
</div>
@endsection
