@push('styles')
    <link rel="stylesheet" href="{{ asset('js/plugins/fancyBox/jquery.fancybox.css') }}">
@endpush

<div class="row">
    <div class="col-sm-10 col-md-10"></div>
    <div class="col-sm-2 col-md-2 text-left">
        <select class="" id="sort-tbl-campaign-posts" style="padding: 2px;  margin-bottom: 5px; font-family: 'Cooper Hewitt'; font-weight: bold;">
            <option value="3" selected>{{ trans('campaign.reach') }}</option>
            <option value="4">{{ trans('campaign.engagement') }}</option>
            <option value="2">{{ trans('campaign.date') }}</option>
        </select>
    </div>
    </div>

<table class="table table-stripped table-hover" id="tbl-campaign-posts">
    <thead>
        <tr>
            <th class="text-center" style="width: 90px;">Influencer</th>
            <th class="text-center">Posts</th>
            <th class="text-center" style="width: 120px; align:center">Date</th>
            <th class="text-center">Reach</th>
            <th class="text-center">Engagement</th>
        </tr>
    </thead>
    <tbody>
        @php
            $autolink = Twitter_Autolink::create();
        @endphp

        @foreach ($model->influenciadoresAprobados as $influenciador)
            @if ($influenciador->perfilTwitter)
                @php
                    $publicaciones = $influenciador->publicacionesCampana($model->id);
                @endphp

                 @foreach ($publicaciones as $publicacion)

                    @if ($publicacion->isAprobadoPublicado())

                    @php
                        $publicacion->loadInteracciones();
                        $datos = $publicacion->getArrayCategoriasInfluencers();
                        $repliers = $publicacion->getUsersReply();
                        $repliers = collect($repliers);
                        $repliers->transform(function ($item, $key) {
                            $item = (array)$item;
                            $item["followers_count"] = \App\Helpers\Helper::formatNumber($item["followers_count"]);
                            $item['icon']            = url($item['icon']);
                            $item['post']            = !is_null($item['post']) ? $item['post'] : '-';
                            return $item;
                        });
                        $repliers = array_values($repliers->toArray());


                        $retweeters = $publicacion->getUsersRetweet();
                        $retweeters = collect($retweeters);
                        $retweeters->transform(function ($item, $key) {
                            $item = (array)$item;
                            $item["followers_count"] = \App\Helpers\Helper::formatNumber($item["followers_count"]);

                            return $item;
                        });
                        $retweeters = $retweeters->toArray();
                    @endphp

                        <tr style="font-size:11px;" data-id="{{ $publicacion->id }}" data-publicacion="{{ $publicacion->id_publicacion_red_social }}">
                            <td class="col-md-2 col-xs-2 text-center">
                                <a class="fancybox" href="{{ str_replace('_normal', '', $influenciador->imgPerfil) }}"><img src="{{ str_replace('_normal', '', $influenciador->imgPerfil) }}" class="img-thumbnail img-circle" style="height:76px; width:76px;" /></a>
                                <div class="clearfix"></div>
                                <img src="{{ asset('img/campana/'.(
                                    $publicacion->isTwitter() ? 'twitter' : (
                                        $publicacion->isFacebook() ? 'facebook' : (
                                            $publicacion->isInstragram() ? 'instragram' : (
                                                $publicacion->isYoutube() ? 'youtube' : ''
                                            )
                                        )
                                    )
                                ).'.png') }}" style="max-height:22px; max-width:22px; padding-right:1px;" />
                                <span class="color-light">{!! $influenciador->perfilTwitter ? $autolink->autoLink($influenciador->perfilTwitter->screen_name) : $influenciador->nombre !!}</span>
                            </td>

                            <td class="content_post row-fluid">
                                <div class="col-md-9 col-sm-9">
                                <a class="fancybox" type="{{ $publicacion->getArchivoMultimedia() ?
                                    ($publicacion->getTypeMultimedia() == 'image' ? 'image' :
                                        ($publicacion->getTypeMultimedia() == 'video' ? 'video' : 'image')
                                    ) : 'image' }}" href="{{ $publicacion->getArchivoMultimedia() ?
                                    ($publicacion->getTypeMultimedia() == 'image' || $publicacion->getTypeMultimedia() == 'video' ? asset($publicacion->getArchivoMultimedia()) : asset('img/campana/blank.png')) :
                                    asset('img/campana/blank.png') }}">
                                    <img src="{{ $publicacion->getArchivoMultimedia() ?
                                        ($publicacion->getTypeMultimedia() == 'image' ? asset($publicacion->getArchivoMultimedia()) : asset('img/campana/blank.png')) :
                                        asset('img/campana/blank.png') }}"
                                class="pull-left img-thumbnail" style="max-height:78px; max-width:78px;margin-right:5px;" /></a>
                                {!! $autolink->autoLink($publicacion->contenido) !!}
                                </div>
                                <div class="col-md-3 col-sm-3">
                                <div class="pull-right container-fluid">

                                    <span href="#modalRetweets" class="popupretweetsopener" data-toggle="modal" data-retweeters="{{json_encode($retweeters)}}" data-data="{{json_encode($datos)}}" style="padding-right:10px; cursor: pointer;">
                                        <img src="{{ asset('img/campana/'.(
                                            $publicacion->isTwitter() ? 'retweet-azul' : (
                                                $publicacion->isFacebook() ? 'compartir-blue' : (
                                                    $publicacion->isInstragram() ? 'retweet-amarillo' : (
                                                        $publicacion->isYoutube() ? 'compartir-gris' : ''
                                                    )
                                                )
                                            )
                                        ).'.png') }}" style="max-height:22px; max-width:22px; padding-right:2px;"
                                        />{{ $publicacion->interacciones ? \App\Helpers\Helper::formatNumber($publicacion->interacciones["retweets"], 0) : '0' }}
                                    </span><br/>

                                    <span style="padding-right:10px;">
                                        <img src="{{ asset('img/campana/'.(
                                            $publicacion->isTwitter() ? 'corazon-azul' : (
                                                $publicacion->isFacebook() ? 'like-blue' : (
                                                    $publicacion->isInstragram() ? 'corazon-amarillo' : (
                                                        $publicacion->isYoutube() ? 'like-gris' : ''
                                                    )
                                                )
                                            )
                                        ).'.png') }}" style="max-height:22px; max-width:22px; padding-right:2px;"
                                        />{{ $publicacion->interacciones ? \App\Helpers\Helper::formatNumber($publicacion->interacciones["favorites"], 0) : '0' }}
                                    </span><br/>

                                    @php
                                    $dataToggleReplies = '';
                                    @endphp
                                    @if(isset($publicacion->interacciones['replies']) && $publicacion->interacciones["replies"] > 0)
                                        @php
                                            $dataToggleReplies = 'modal';
                                        @endphp
                                    @endif
                                    <span href="#modalReplies" class="popuprepliesopener" data-toggle="{{ $dataToggleReplies }}" data-repliers="{{json_encode($repliers)}}" data-data="{{json_encode($datos)}}" style="cursor: pointer;">
                                        <img src="{{ asset('img/campana/'.(
                                            $publicacion->isTwitter() ? 'mensaje-azul' : (
                                                $publicacion->isFacebook() ? 'mensaje-blue' : (
                                                    $publicacion->isInstragram() ? 'mensaje-amarillo' : (
                                                        $publicacion->isYoutube() ? 'mensaje-gris' : ''
                                                    )
                                                )
                                            )
                                        ).'.png') }}" style="max-height:22px; max-width:22px; padding-right:2px;"
                                        />{{ $publicacion->interacciones ? \App\Helpers\Helper::formatNumber($publicacion->interacciones["replies"], 0) : '0' }}
                                    </span>

                                </div>
                                </div>

                            </td>

                            <td style="text-align:center;" data-order="{{ strtotime($publicacion->fecha_hora_publicacion) }}">
                                {!! date('d-m H:i', strtotime($publicacion->fecha_hora_publicacion)) !!}
                            </td>

                            <td class="text-center" data-order="{{ $publicacion->alcance() }}">
                                <span
                                    title="{{ number_format($influenciador->perfilTwitter->cantidad_seguidores) . ' + ' .
                                    ($publicacion->estadisTwitter ? number_format($publicacion->estadisTwitter->total_followers_rt) : '0') . ' = ' .
                                    number_format($publicacion->alcance()) }}"
                                    data-toggle="tooltip">
                                    {{ \App\Helpers\Helper::formatNumber($publicacion->alcance()) }}
                                </span>
                            </td>

                            <td class="text-center" data-order="{{ $publicacion->engagement() }}"
                                data-toggle="tooltip" title="{{ $publicacion->engagement_tooltip() }}">
                                {{ $publicacion->engagement() }}%
                            </td>

                        </tr>
                    @endif
                @endforeach
            @endif
        @endforeach
    </tbody>
</table>
@include('common.popupreplies')
@include('common.popupretweets')

@push('scripts')
    <script src="{{ asset('js/campana_estadisticas.v2.js') }}"></script>
    <script src="{{ asset('js/plugins/fancyBox/jquery.fancybox.js') }}"></script>
@endpush