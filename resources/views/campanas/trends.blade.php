@php
    use App\Helpers\NumbersToStringConverter;
@endphp
<!-- Top section -->
<br>
<div class="row trends border-bottom-gray-trend" style="max-height: 328px;">
    <div class="col-sm-12 col-md-6 border-right-gray-trend">
        <h4 class="text-center"><img src="{{ asset('img/campana/top-influencers.png') }}"> {{ trans('campaign.campaign_influencers') }}</h4>
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <p>
                    <img src="{{ asset('img/campana/twitter.png') }}" class="img-small"> &nbsp;&nbsp;&nbsp;
                    <img src="{{ asset('img/campana/facebook_gris.png') }}" class="img-small"> &nbsp;&nbsp;&nbsp;
                    <img src="{{ asset('img/campana/instagram_gris.png') }}" class="img-small"> &nbsp;&nbsp;&nbsp;
                    <img src="{{ asset('img/campana/youtube_gris.png') }}" class="img-small">
                </p>
            </div>
            <div class="col-sm-12 col-md-4">
                <select class="" id="sort-tbl-influencers"  style="padding: 2px;  margin-bottom: 5px; font-family: 'Cooper Hewitt'; font-weight: bold;">
                    <option value="3" selected>{{ trans('campaign.reach') }}</option>
                    <option value="5">{{ trans('campaign.engagement') }}</option>
                    <option value="4">{{ trans('campaign.interactions') }}</option>
                </select>
            </div>
        </div>
        <br><br>
        <div class="row-scroll row-height-audience" style="max-height: 189px;">
        <table id="tbl-influencers">
        <tbody>
            @foreach($model->influenciadoresAprobados() as $influenciador)
                <tr>
                    <td class="col-md-2 col-sm-6 col-xs-12 border-top border-bottom">
                        <img src="{{ $influenciador->perfilTwitter->profile_image_url }}" class="img-circle img-influencer">
                    </td>
                    <td class="col-md-3 col-sm-6 col-xs-12 border-top border-bottom">
                        <p>{{ $influenciador->nombre }}</p>
                        <p>{{ $influenciador->perfilTwitter->screen_name }}</p>
                    </td>
                    <td class="col-md-2 col-sm-6 col-xs-12 border-top border-bottom">
                       <img src="{{ asset('img/campana/twitter.png') }}" class="img-small">
                    </td>
                    <td data-order="{{ $influenciador->total_followers }}" class="col-md-2 col-sm-6 col-xs-12 border-top border-bottom">
                        <p class="text-center">{{ trans('campaign.audience') }}</p>
                        <p class="text-center">{{ NumbersToStringConverter::toThousands($influenciador->total_followers) }}</p>
                    </td>
                    <td data-order="{{ $influenciador->getInteractionsByCampana($model->id) }}" class="col-md-2 col-sm-6 col-xs-12 border-top border-bottom">
                        <p class="text-center">{{ trans('campaign.interactions') }}</p>
                        <p class="text-center">{{ NumbersToStringConverter::toThousands($influenciador->getInteractionsByCampana($model->id)) }}</p>
                    </td>
                    <td data-order="{{ $influenciador->getEngagementByCampana($model->id) }}" class="col-md-3 col-sm-6 col-xs-12 border-top border-bottom border-right">
                        <p class="text-center">Engagement</p>
                        <p class="text-center">{{ number_format($influenciador->getEngagementByCampana($model->id), 2) }}%</p>
                    </td>
                </tr>
            @endforeach
            </tbody>
            </table>
        </div>
    </div>

    <div class="col-sm-12 col-md-6">
        <h4 class="text-center"><img src="{{ asset('img/campana/time.png') }}"> {{ trans('campaign.post_per_hour') }}</h4>
        <div id="graph-posts-per-hour" style="min-width: 300px; min-height: 300px; position: relative; top: -40px;" data-id="{{ $model->id }}" data-url="{{ url('campanas/' . $model->id . ' /graficas/posts-per-hour') }}"></div>
    </div>
</div>
<br>
<!-- middle section -->
<div class="row trends border-bottom-gray-trend">
    <div class="col-md-2 col-sm-6 col-xs-12 border-right-gray-trend">
        <h4 class="text-center">{{ trans('campaign.gender') }}</h4>
        <div class="row trends" style="margin-left: -25px; margin-right: -25px;">
            <div class="col-md-6 col-sm-12">
                <img src="{{ asset('img/campana/masculino.png') }}" class="img-campana">
            </div>
            <div class="col-md-6 col-sm-12">
                @php
                $dist = $model->getDistribucionGenero();
                @endphp
                <p class="important">{{ $dist->total !== 0 ? number_format(($dist->male * 100) / $dist->total) : 0 }}%</p>
                <p>{{ trans('campaign.men') }}</p>
            </div>
        </div>
        <br>
        <div class="row border-bottom-gray-trend" style="margin-right: -25px;">
            <div class="col-md-6 col-sm-12">
                <img src="{{ asset('img/campana/femenino.png') }}" class="img-campana">
            </div>
            <div class="col-md-6 col-sm-12">
                <p class="important">{{ $dist->total !== 0 ? number_format(($dist->female * 100) / $dist->total) : 0 }}%</p>
                <p>{{ trans('campaign.women') }}</p>
            </div>
        </div>

        <div class="row text-center">
            <h4 class="text-center"><img src="{{ asset('img/campana/time.png') }}"> {{ trans('campaign.measured_time') }}</h4>
            <div class="box-light text-center"><span>0 hrs</span></div>
        </div>
    </div>

    <div class="col-md-5 col-sm-6 col-xs-12 border-right-gray-trend">
        <h4 class="text-center"><img src="{{ asset('img/campana/top-location.png') }}"> {{ trans('campaign.top_locations') }}</h4>
        <div id="graph-top-locations" style="min-width: 300px; min-height: 250px;" data-id="{{ $model->id }}" data-url="{{ url('campanas/' . $model->id . ' /graficas/top-locations') }}"></div>
        <br>
    </div>

    <div class="col-md-5 col-sm-6 col-xs-12 border-right-gray-trend">
        <h4 class="text-center"> {{ trans('campaign.share_of_social') }}</h4>
        <div id="graph-share-social" style="min-width: 300px; min-height: 250px;" data-id="{{ $model->id }}" data-url="{{ url('campanas/' . $model->id . ' /graficas/share-social') }}"></div>
    </div>
</div>
<br>
<!-- lower section -->
<div class="row border-bottom-gray-trend">
    <div class="col-md-3 col-sm-6 col-xs-12 border-right-gray-trend">
        <h4 class="text-center"><img src="{{ asset('img/campana/twitter.png') }}" class="img-small"> {{ trans('campaign.share_of_twitter') }}</h4>
        <div id="graph-shares-twitter" style="width: 200px; height: 300px; position: relative; top: -40px;" data-id="{{ $model->id }}" data-url="{{ url('campanas/' . $model->id . ' /graficas/shares-twitter') }}"></div>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12 border-right-gray-trend coming-soon">
        <h4 class="text-center"><img src="{{ asset('img/campana/facebook.png') }}" class="img-small"> {{ trans('campaign.share_of_facebook') }}</h4>
        <div id="graph-shares-facebook" style="width: 200px; height: 300px;">
            <p class="lead">Coming soon</p>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12 border-right-gray-trend coming-soon">
        <h4 class="text-center"><img src="{{ asset('img/campana/instagram.png') }}" class="img-small"> {{ trans('campaign.share_of_instagram') }}</h4>
        <div id="graph-shares-instagram" style="width: 200px; height: 300px;">
            <p class="lead">Coming soon</p>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12 coming-soon">
        <h4 class="text-center"><img src="{{ asset('img/campana/youtube.png') }}" class="img-small"> {{ trans('campaign.share_of_youtube') }}</h4>
        <div id="graph-shares-youtube" style="width: 200px; height: 325px;">
            <p class="lead">Coming soon</p>
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{ asset('js/plugins/zingchart/zingchart.min.js') }}"></script>
    <script src="{{ asset('js/campana_trends.js') }}"></script>
@endpush