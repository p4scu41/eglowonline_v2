@php
    $model->loadInteracciones();
@endphp

<div class="row border-bottom-gray">
    <div class="col-md-8">
        <table width="100%">
            <tr class="text-center">
                <td>
                    <p><strong>@lang('campaign.posts')</strong></p><br>
                    <div class="box-light text-center"><span>{{ $model->getCountPublicacionesEnviadas() }}</span></div>
                </td>
                <td>
                    <p><strong>@lang('campaign.influencers')</strong></p><br>
                    <div class="box-light text-center"><span>{{ $model->getCountInfluenciadoresPublicados() }}</span></div>
                </td>
                <td>
                    <p><strong>@lang('campaign.interactions')</strong></p><br>
                    <div class="box-light text-center" data-toggle="tooltip"
                        title="{{ 'RT ' . number_format($model->interacciones['retweets']).' + '.
                                  'FV ' . number_format($model->interacciones['favorites']).' + '.
                                  'RP ' .number_format($model->interacciones['replies']) }}">
                        <span>{{ \App\Helpers\Helper::formatNumber($model->interacciones['total']) }}</span>
                    </div>
                </td>
                <td>
                    <p><strong>@lang('campaign.engagement')</strong></p><br>
                    <div class="box-light text-center">
                        <span>{{ $model->engagement() }}%</span><br>
                        <img src="{{ asset('img/campana/refrescar.png') }}" class="img-small m-t--13" data-toggle="tooltip"
                            title="Update in {{ str_replace('after', '', \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d 23:59:59'))->diffForHumans(\Carbon\Carbon::now())) }}">
                    </div>
                </td>
                <td>
                    <p><strong>@lang('campaign.reach')</strong></p><br>
                    <div class="box-light text-center">
                        <span data-toggle="tooltip" title="{{ number_format($model->alcanceOrganico()) }}">{{ \App\Helpers\Helper::formatNumber($model->alcanceOrganico()) }}</span><br>
                        <img src="{{ asset('img/campana/refrescar.png') }}" class="img-small m-t--13" data-toggle="tooltip"
                            title="Update in {{ str_replace('after', '', \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d 23:59:59'))->diffForHumans(\Carbon\Carbon::now())) }}">
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div class="col-md-4 text-center border-left-gray">
        <strong>{{ trans('campaign.statistics') }}</strong>
        @php
            $interacciones_redes_sociales = $model->getCountInteractionsBySocialNetwork();
        @endphp

        <table class="table-spacing" width="90%">
            <tr class="text-left">
                <td><img src="{{ asset('img/campana/twitter.png') }}" class="img-small"></td>
                <td><img src="{{ asset('img/campana/corazon-azul.png') }}" class="img-small">
                    {{ $interacciones_redes_sociales[\App\Models\RedSocial::$TWITTER]['favorites'] }}</td>
                <td><img src="{{ asset('img/campana/mensaje-azul.png') }}" class="img-small">
                    {{ $interacciones_redes_sociales[\App\Models\RedSocial::$TWITTER]['replies'] }}</td>
                <td><img src="{{ asset('img/campana/retweet-azul.png') }}" class="img-small">
                    {{ $interacciones_redes_sociales[\App\Models\RedSocial::$TWITTER]['retweets'] }}</td>
            </tr>
            <tr class="text-left">
                <td><img src="{{ asset('img/campana/facebook.png') }}" class="img-small"></td>
                <td><img src="{{ asset('img/campana/like-blue.png') }}" class="img-small">
                    {{ $interacciones_redes_sociales[\App\Models\RedSocial::$FACEBOOK]['favorites'] }}</td>
                <td><img src="{{ asset('img/campana/mensaje-blue.png') }}" class="img-small">
                    {{ $interacciones_redes_sociales[\App\Models\RedSocial::$FACEBOOK]['replies'] }}</td>
                <td><img src="{{ asset('img/campana/compartir-blue.png') }}" class="img-small">
                    {{ $interacciones_redes_sociales[\App\Models\RedSocial::$FACEBOOK]['retweets'] }}</td>
            </tr>
            <tr class="text-left">
                <td><img src="{{ asset('img/campana/instagram.png') }}" class="img-small"></td>
                <td><img src="{{ asset('img/campana/corazon-amarillo.png') }}" class="img-small">
                    {{ $interacciones_redes_sociales[\App\Models\RedSocial::$INSTAGRAM]['favorites'] }}</td>
                <td><img src="{{ asset('img/campana/mensaje-amarillo.png') }}" class="img-small">
                    {{ $interacciones_redes_sociales[\App\Models\RedSocial::$INSTAGRAM]['replies'] }}</td>
                <td><img src="{{ asset('img/campana/retweet-amarillo.png') }}" class="img-small">
                    {{ $interacciones_redes_sociales[\App\Models\RedSocial::$INSTAGRAM]['retweets'] }}</td>
            </tr>
            <tr class="text-left">
                <td><img src="{{ asset('img/campana/youtube.png') }}" class="img-small"></td>
                <td><img src="{{ asset('img/campana/like-gris.png') }}" class="img-small">
                    {{ $interacciones_redes_sociales[\App\Models\RedSocial::$YOUTUBE]['favorites'] }}</td>
                <td><img src="{{ asset('img/campana/mensaje-gris.png') }}" class="img-small">
                    {{ $interacciones_redes_sociales[\App\Models\RedSocial::$YOUTUBE]['replies'] }}</td>
                <td><img src="{{ asset('img/campana/compartir-gris.png') }}" class="img-small">
                    {{ $interacciones_redes_sociales[\App\Models\RedSocial::$YOUTUBE]['retweets'] }}</td>
            </tr>
        </table>
    </div>
</div>