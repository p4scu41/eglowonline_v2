<div class="item-influenciador-search col-xs-12 col-sm-{{ isset($col) ? $col : '6' }} col-md-{{ isset($col) ? $col : '6' }}"
    data-id="{{{ isset($id) ? $id : '@{{influenciador.id}}' }}}"
    data-influenciador="@{{influenciador.json}}">
    <div class="col-xs-12 col-sm-12 col-md-12 div-img"
        style="background-image: url({{{ isset($twitter_profile_image_url) ? $twitter_profile_image_url : '@{{influenciador.twitter_profile_image_url}}' }}});">
        @if ($btnAdd != false)
            <span class="btn-add">
                <i class="fa fa-check fa-2x text-red" data-toggle="tooltip" title="Elegir influenciador"></i>
            </span>
        @endif
        <a class="spacer link-perfil-influencer" href="{{ url('influenciadores/') }}/{{{ '@{{influenciador.id}}' }}}" data-toggle="tooltip" title="Ver Perfil"></a>
        <div class="footer">
            Engagement Rate <br>
            <!--<span class="bigger">{{{ isset($engagement_rate) ? $engagement_rate : '@{{influenciador.engagement_rate}}' }}}</span>-->
            <span class="bigger">{{{ isset($engagement) ? $engagement : '@{{influenciador.engagement_rate}}' }}}%</span>
            {{--<span class="bigger">{{{ isset($engagement_ambos) ? $engagement_ambos : '@{{influenciador.engagement_ambos}}' }}}</span>
            <span class="light-gray"><i class="fa fa-arrow-up"></i> {{{ isset($engagement_rate) ? $engagement_rate : '@{{influenciador.engagement_rate}}' }}} %</span>--}}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 div-body" style="height: 64px;">
        <div class="col-xs-3 col-sm-3 col-md-3">
            <span class="red-box" title="Cantidad total de seguidores" data-toggle="tooltip">
                {{{ isset($qty_followers) ? $qty_followers : '@{{influenciador.qty_followers}}' }}}
            </span>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <span class="red-box" title="Alcance potencial" data-toggle="tooltip">
                {{{ isset($alcance_potencial) ? $alcance_potencial : '@{{influenciador.alcance_potencial}}' }}}
            </span>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            @if ($btnAdd == false)
                <a class="link-perfil-influencer text-medium" href="{{ url('influenciadores/') }}/{{{ '@{{influenciador.id}}' }}}" data-toggle="tooltip" title="Ver Perfil">
                        <b>{{{ isset($nombre) ? $nombre : '@{{influenciador.nombre}}' }}}</b>
                </a>
            @else
                {{{ isset($nombre) ? $nombre : '@{{influenciador.nombre}}' }}}
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 div-body">
        <div class="col-xs-3 col-sm-3 col-md-3">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="col-xs-9 col-sm-9 col-md-9">
            {{{ isset($tipo_influenciador) ? $tipo_influenciador : '@{{influenciador.tipo_influenciador}}' }}} <br>
            <span class="light-gray">
                @if ($btnAdd == false)
                    <a class="link-perfil-influencer" href="{{ url('influenciadores/') }}/{{{ '@{{influenciador.id}}' }}}" data-toggle="tooltip" title="Ver Perfil">
                        {{{ isset($twitter_screen_name) ? $twitter_screen_name : '@{{influenciador.twitter_screen_name}}' }}}
                    </a>
                @else
                    {{{ isset($twitter_screen_name) ? $twitter_screen_name : '@{{influenciador.twitter_screen_name}}' }}}
                @endif
            </span>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 footer">

        <div class="mini-red-social">
            <div class="logo-red-social">
                <img src="{{ asset('img/campana/twitter.png') }}" class="img-small">
            </div>
            <div>
                <span class="red-box" data-toggle="tooltip" title="Cantidad de seguidores">
                {{{ isset($twitter_cantidad_seguidores) ? $twitter_cantidad_seguidores : '@{{influenciador.twitter_cantidad_seguidores}}' }}}
                </span>
            </div>
            <br>
            <div>
                <span class="red-box" data-toggle="tooltip" title="Precio por publicación">
                    {{{ isset($twitter_precio_publicacion) ? $twitter_precio_publicacion : '@{{influenciador.twitter_precio_publicacion}}' }}}
                </span>
            </div>
        </div>

        {!! '@{{#ifCond influenciador.facebook_cantidad_seguidores "!=" 0}}' !!}
        <div class="mini-red-social">
            <div class="logo-red-social">
                <img src="{{ asset('img/campana/facebook.png') }}" class="img-small">
            </div>
            <div>
                <span class="red-box">
                    {{{ isset($facebook_cantidad_seguidores) ? $facebook_cantidad_seguidores : '@{{influenciador.facebook_cantidad_seguidores}}' }}}
                </span>
            </div>
            <br>
            <div>
                <span class="red-box" data-toggle="tooltip" title="Precio por publicación">
                    {{{ isset($facebook_precio_publicacion) ? $facebook_precio_publicacion : '@{{influenciador.facebook_precio_publicacion}}' }}}
                </span>
            </div>
        </div>
        {{{ "@{{/ifCond}}" }}}

        {!! '@{{#ifCond influenciador.youtube_cantidad_seguidores "!=" 0}}' !!}
        <div class="mini-red-social">
            <div class="logo-red-social">
                <img src="{{ asset('img/campana/youtube.png') }}" class="img-small">
            </div>
            <div>
                <span class="red-box">
                    {{{ isset($youtube_cantidad_seguidores) ? $youtube_cantidad_seguidores : '@{{influenciador.youtube_cantidad_seguidores}}' }}}
                </span>
            </div>
            <br>
            <div>
                <span class="red-box" data-toggle="tooltip" title="Precio por publicación">
                    {{{ isset($youtube_precio_publicacion) ? $youtube_precio_publicacion : '@{{influenciador.youtube_precio_publicacion}}' }}}
                </span>
            </div>
        </div>
        {{{ "@{{/ifCond}}" }}}

        {!! '@{{#ifCond influenciador.instagram_cantidad_seguidores "!=" 0}}' !!}
        <div class="mini-red-social">
            <div class="logo-red-social">
                <img src="{{ asset('img/campana/instagram.png') }}" class="img-small">
            </div>
            <div>
                <span class="red-box">
                    {{{ isset($instagram_cantidad_seguidores) ? $instagram_cantidad_seguidores : '@{{influenciador.instagram_cantidad_seguidores}}' }}}
                </span>
            </div>
            <br>
            <div>
                <span class="red-box" data-toggle="tooltip" title="Precio por publicación">
                    {{{ isset($instagram_precio_publicacion) ? $instagram_precio_publicacion : '@{{influenciador.instagram_precio_publicacion}}' }}}
                </span>
            </div>
        </div>
        {{{ "@{{/ifCond}}" }}}

        {!! '@{{#ifCond influenciador.snapchat_cantidad_seguidores "!=" 0}}' !!}
        <div class="mini-red-social">
            <div class="logo-red-social">
                <img src="{{ asset('img/campana/snapchat.png') }}" class="img-small">
            </div>
            <div>
                <span class="red-box">
                    {{{ isset($snapchat_cantidad_seguidores) ? $snapchat_cantidad_seguidores : '@{{influenciador.snapchat_cantidad_seguidores}}' }}}
                </span>
            </div>
            <br>
            <div>
                <span class="red-box" data-toggle="tooltip" title="Precio por publicación">
                    {{{ isset($snapchat_precio_publicacion) ? $snapchat_precio_publicacion : '@{{influenciador.snapchat_precio_publicacion}}' }}}
                </span>
            </div>
        </div>
        {{{ "@{{/ifCond}}" }}}
    </div>
</div>
