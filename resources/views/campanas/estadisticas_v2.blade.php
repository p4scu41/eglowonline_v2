@extends('layouts.' . config('app.layout') )

@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
@endpush

@section('content')

    @php
        $model->loadInfluenciadoresAprobados();
    @endphp

    <div class="row main-container-statistics">
        <div class="col-md-12">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#tab-statistics" aria-controls="tab-statistics" role="tab" data-toggle="tab">
                    <i class="fa fa-bar-chart"></i> {{ trans('campaign.statistics') }}
                </a>
            </li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content gray-border">
            <div role="tabpanel" class="tab-pane active" id="tab-statistics">

                @include('campanas.general_information')

                @include('campanas.posts_information')

                <div class="row">
                    <div class="col-md-12">
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs light-tab" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#tab-campaign-posts" class="campaign-tabs" aria-controls="tab-campaign-posts" role="tab" data-toggle="tab">
                                    <img src="{{ url('img/campana/campana-blanco.png') }}" data-gris="{{ url('img/campana/campana-gris.png') }}" data-blanco="{{ url('img/campana/campana-blanco.png') }}"> {{ ucwords(trans('campaign.campaign_posts')) }}
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#tab-trends" class="campaign-tabs" id="showTrends" aria-controls="tab-trends" role="tab" data-toggle="tab" style="color: #979598">
                                    <img src="{{ url('img/campana/trends-gris.png') }}" data-gris="{{ url('img/campana/trends-gris.png') }}" data-blanco="{{ url('img/campana/trends-blanco.png') }}"> {{ trans('campaign.trends') }}
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#tab-audience" class="campaign-tabs" aria-controls="tab-audience" role="tab" data-toggle="tab" style="color: #979598">
                                    <img src="{{ url('img/campana/audiencia-gris.png') }}" data-gris="{{ url('img/campana/audiencia-gris.png') }}" data-blanco="{{ url('img/campana/audiencia-blanco.png') }}"> {{ trans('campaign.audience') }}
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#tab-pdf" class="campaign-tabs" aria-controls="tab-pdf" role="tab" data-toggle="tab" style="color: #979598">
                                    <img src="{{ url('img/campana/descarga-gris.png') }}" data-gris="{{ url('img/campana/descarga-gris.png') }}" data-blanco="{{ url('img/campana/descarga-blanco.png') }}"> {{ trans('campaign.export_pdf') }}
                                </a>
                            </li>
                          </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab-campaign-posts">
                                @include('campanas.campaign_posts')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="tab-trends">
                                @include('campanas.trends')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="tab-audience">
                                @include('campanas.audience')
                            </div>
                          </div>
                    </div>
                </div>

            </div>
          </div>

        </div>
    </div>

@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
@endpush