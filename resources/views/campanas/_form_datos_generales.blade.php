<div class="row">
    <div class="col-xs-12 col-sm-10 col-md-11">
        <h2>INFO PARA TU CAMPAÑA</h2>

        <p>
            <span class="text-red">Bienvenidos a {{ config('app.title') }}.</span>
            Empieza completando la información para crear tu campaña.
        </p>
    </div>

    <div class="col-xs-12 col-sm-10 col-md-11">
        {!! BootForm::vertical($config_form) !!}

            {!! BootForm::text('nombre', false, null, [
                    'data-rule-required'  => 'true',
                    'data-rule-minlength' => '3',
                    'data-rule-maxlength' => '45',
                    'data-toggle'         => 'tooltip',
                    'placeholder'         => $labels['nombre'],
                    'title'               => $labels['nombre'],
                    'icon'                => '&nbsp; &nbsp;',
                ])
            !!}

            {!! BootForm::text('fecha_desde', false, null, [
                    'div_form_group_class' => 'col-xs-12 col-sm-12 col-md-6',
                    'data-rule-required'   => 'true',
                    'data-rule-before'     => '#fecha_hasta',
                    'class'                => 'datepicker',
                    'data-toggle'          => 'tooltip',
                    'placeholder'          => $labels['fecha_desde'],
                    'title'                => $labels['fecha_desde'],
                    'icon'                 => '<i class="fa fa-lg fa-calendar"></i>',
                ])
            !!}

            {!! BootForm::text('fecha_hasta', false, null, [
                    'div_form_group_class' => 'col-xs-12 col-sm-12 col-md-6',
                    'data-rule-required'   => 'true',
                    'data-rule-after'      => '#fecha_desde',
                    'class'                => 'datepicker',
                    'data-toggle'          => 'tooltip',
                    'placeholder'          => $labels['fecha_hasta'],
                    'title'                => $labels['fecha_hasta'],
                    'icon'                 => '<i class="fa fa-lg fa-calendar"></i>',
                ])
            !!}

            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                <div class="input-group">
                    <span class="input-group-addon"></span>
                    <div class="form-control float-initial">
                        <div>{{ $labels['industria_id'] }}</div>

                        <select data-rule-required="true" id="industria_id" name="industria_id" class="selectToList">
                            @foreach ($catalogs['industria'] as $industria)
                                <option value="{{ $industria['id'] }}"
                                    data-imagen="{{ asset($industria['icono']) }}"
                                    {{ $industria['id'] == $model->industria_id ? 'selected' : '' }}>
                                        {{ $industria['nombre'] }}
                                </option>
                            @endforeach
                        </select>
                        <span id="industria_id-error" class="help-block"></span>
                    </div>
                </div>
            </div>

            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                <div class="input-group">
                    <span class="input-group-addon"></span>
                    <div class="form-control float-initial">
                        {{ $labels['presupuesto'] }}
                        <input type="text" id="presupuesto" name="presupuesto" value="{{ $model->presupuesto }}" />
                    </div>
                </div>
            </div>

            @if (Auth::user()->isAdministrador())
                {!! BootForm::select('agencia_id', false,
                        $catalogs['agencia'], null, [
                            'class'              => 'load-ajax-onchange',
                            'data-target'        => '#marca_id',
                            'data-url'           => url('marcas/byagencia'),
                            'data-toggle'        => 'tooltip',
                            'title'              => $labels['agencia_id'],
                            'data-rule-required' => 'true',
                            'icon'               => '&nbsp; &nbsp;',
                        ]
                    )
                !!}
            @endif

            @if (Auth::user()->isAgencia() || Auth::user()->isAdministrador())
                {!! BootForm::select('marca_id', false,
                        $catalogs['marca'], null, [
                            'class'       => 'load-ajax-onchange',
                            'data-target' => '#producto_id',
                            'data-url'    => url('productos/bymarca'),
                            'data-toggle' => 'tooltip',
                            'title'       => $labels['marca_id'],
                            'icon'        => '&nbsp; &nbsp;',
                        ]
                    )
                !!}
            @endif

            {!! BootForm::select('producto_id', false,
                    $catalogs['producto'], null, [
                        'data-toggle' => 'tooltip',
                        'title'       => $labels['producto_id'],
                        'icon'        => '&nbsp; &nbsp;',
                    ]
                )
            !!}

            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa-lg">#</i></span>
                    <input data-toggle="tooltip" placeholder="Hashtag" title="Hashtag" class="form-control" id="hashtag" name="hashtag" type="text" value="">
                    <div class="input-group-addon btn-red" id="btn-add-hastag">
                        <i class="fa fa-plus"></i>
                    </div>
                </div>
                <span id="hashtag-error" class="help-block" style="bottom: -27px;"></span>
            </div>

            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                <div id="hashtag-box">
                    <ul id="hashtag-list">
                        @if (!empty($model->hashtags))
                            @foreach ($model->hashtags as $hashtag)
                                @include($response_info['resource'] . '/hashtag_tpl', [
                                    'hashtag' => $hashtag,
                                ])
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>

            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                <div class="input-group">
                    <span class="input-group-addon"></span>
                    <div class="form-control float-initial">
                        <div>Objetivo de la Campaña</div>

                        <select data-rule-required="true" id="tipo_campana_id" name="tipo_campana_id" class="selectToList">
                            @foreach ($catalogs['tipo_campana'] as $tipo_campana)
                                <option value="{{ $tipo_campana['id'] }}"
                                    data-imagen="{{ asset($tipo_campana['icono']) }}"
                                    {{ $tipo_campana['id'] == $model->tipo_campana_id ? 'selected' : '' }}>
                                        {{ $tipo_campana['tipo'] }}
                                </option>
                            @endforeach
                        </select>

                        <span id="tipo_campana_id-error" class="help-block"></span>
                    </div>
                </div>
            </div>
              <label>Seleccione el archivo con los Terminos y condiciones de campaña</label>
            @if ($model->exists)
                <div class="form-group col-xs-12 col-sm-12 col-md-12">
                    <div class="input-group">
                        <span class="input-group-addon"></span>
                        <div class="form-control float-initial">
                            <div>{{ $labels['activo'] }}</div>

                            <select data-rule-required="true" id="activo" name="activo" class="selectToList">
                                @foreach ($catalogs['si_no'] as $si_no)
                                    <option value="{{ $si_no['id'] }}"
                                        data-imagen="{{ asset($si_no['icono']) }}"
                                        {{ $si_no['id'] === '' || empty($model->id) ? '' : ($si_no['id'] == $model->activo ? 'selected' : '' ) }}>
                                            {{ $si_no['descripcion'] }}
                                    </option>
                                @endforeach
                            </select>
                            <span id="activo-error" class="help-block"></span>
                        </div>
                    </div>
                </div>
            @endif

            {!! BootForm::file('terminos', false, [
                    'data-rule-required'  => $model->exists ? 'false' : 'true',
                    'data-rule-extension' => 'txt|pdf|doc|docx',
                    'class'               => 'form-control',
                    'data-toggle'         => 'tooltip',
                    'placeholder'         => 'Adjunte el archivo en formato txt',
                    'title'               => 'Agregue el archivo de términos y condiciones. Formatos aceptados .txt, .doc, .docx, .pdf',
                    'icon'                => '<i class="fa fa-lg fa-file"></i>',
                ])
            !!}
            @if($model->exists && $model->tieneTerminosYCondiciones())
                <div class="form-group col-xs-12 col-sm-12 col-md-12">
                    <a href="{{ url('campanas/' . $model->id . '/terminos') }}" class="text-info"><i class="fa fa-file fa-2x"></i> Ver archivo actual</a></p>
                    <p class="form-control-static text-danger"><b>Si adjunta un nuevo archivo, reeemplazará al actual</b></p>
                </div>
            @endif

        {!! BootForm::close() !!}
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-right">
        @if ($model->exists)
            <a class="btn btn-red-rounded btn-save-campana">
                <span class="glyphicon glyphicon-floppy-save"></span> Guardar
            </a>
            &nbsp;
        @endif
        <button type="button" id="btn-next-campana" class="btn btn-red-rounded">SIGUIENTE</button>
    </div>
</div>
