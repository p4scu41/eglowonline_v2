@php
    // $model->getReport();
    $autolink = Twitter_Autolink::create();
@endphp

<div class="row border-bottom-gray">
    <div class="col-md-1 text-center">
        @if ($model->producto_id)
            <img src="{{ asset($model->producto->getImagen()) }}" class="img-campana img-circle">
        @elseif ($model->marca_id)
            <img src="{{ asset($model->marca->getLogotipo()) }}" class="img-campana img-circle">
        @else
            <img src="{{ asset($model->agencia->getLogotipo()) }}" class="img-campana img-circle">
        @endif
        <br>
        <strong>{{ $model->nombre }}</strong>
    </div>

    <div class="col-md-1 text-center">
        <p class="title-cam-g-info"><strong>@lang('campaign.objective')</strong></p><p>&nbsp;</p>
        <p>{{ $model->tipoCampana->tipo }}</p>
    </div>

    <div class="col-md-2 text-center">
        <p class="title-cam-g-info"><strong>@lang('campaign.hashtag')</strong></p><p>&nbsp;</p>
        <p class="color-light">{!! $autolink->autoLink($model->hashtags) !!}</p>
    </div>

    <div class="col-md-3 text-center">
        <p class="title-cam-g-info"><strong>@lang('campaign.posts')</strong></p><p>&nbsp;</p>
        <p>
            @php
                $postBySocialNetworks = $model->getCountPostBySocialNetwork();
            @endphp
            <img src="{{ asset('img/campana/twitter'.($postBySocialNetworks[\App\Models\RedSocial::$TWITTER]==0 ? '_gris' : '').'.png') }}" class="img-small">
                {{ $postBySocialNetworks[\App\Models\RedSocial::$TWITTER] }} &nbsp; &nbsp; &nbsp;
            <img src="{{ asset('img/campana/facebook'.($postBySocialNetworks[\App\Models\RedSocial::$FACEBOOK]==0 ? '_gris' : '').'.png') }}" class="img-small">
                {{ $postBySocialNetworks[\App\Models\RedSocial::$FACEBOOK] }} &nbsp; &nbsp; &nbsp;
            <img src="{{ asset('img/campana/instagram'.($postBySocialNetworks[\App\Models\RedSocial::$INSTAGRAM]==0 ? '_gris' : '').'.png') }}" class="img-small">
                {{ $postBySocialNetworks[\App\Models\RedSocial::$INSTAGRAM] }} &nbsp; &nbsp; &nbsp;
            <img src="{{ asset('img/campana/youtube'.($postBySocialNetworks[\App\Models\RedSocial::$YOUTUBE]==0 ? '_gris' : '').'.png') }}" class="img-small">
                {{ $postBySocialNetworks[\App\Models\RedSocial::$YOUTUBE] }}
        </p>
    </div>

    <div class="col-md-1 text-center">
        <p class="title-cam-g-info"><strong>@lang('campaign.influencers')</strong></p><p>&nbsp;</p>
        <p>{{ $model->getCountAutores() }}</p>
    </div>

    <div class="col-md-2 text-center">
        <p class="title-cam-g-info"><strong>@lang('campaign.campaign_duration')</strong></p>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr class="text-center">
                <td><span class="color-lightgray">To</span><br>@date_short($model->fecha_desde)</td>
                <td><span class="color-lightgray">From</span><br>@date_short($model->fecha_hasta)</td>
            </tr>
        </table>
    </div>

    <div class="col-md-2 text-center">
        <p class="title-cam-g-info"><strong>@lang('campaign.budget')</strong></p><p>&nbsp;</p>
        <p>{{ $model->presupuesto_currency }}</p>
    </div>
</div>