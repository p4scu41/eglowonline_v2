<li>
    <a href="#" class="btn-delete">
        <i class="fa fa-times-circle fa-lg text-red"></i>
    </a>
     <span class="hashtag-item">{{{ isset($hashtag) ? $hashtag : '@{{hashtag}}' }}}</span>
</li>
