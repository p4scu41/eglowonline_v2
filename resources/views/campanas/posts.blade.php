@push('styles')
    <link href="{{ asset('node_modules/bootstrap-fileinput/css/fileinput.min.css') }}" rel="stylesheet">
@endpush

@push('scripts-head')
    <script type="text/javascript">
        var urlDeletePropuestaPost = '{{ url('influenciadoresxcampana/destroy') }}',
            urlUpdatePropuestaPost = '{{ url('publicacionesxcampana/update') }}',
            urlSavePublicaciones = '{{ url('publicaciones') }}',
            urlPublicarPost = '{{ url('publicaciones/publicar') }}',
            urlChat = '{{ url('chat') }}',
            hashtags = '{{ $model->hashtags }}',
            maxFileSize = {{ \App\Helpers\Helper::returnKilobytes(ini_get('upload_max_filesize')) }};
    </script>
@endpush

<h2>GESTIONA LOS POSTS DE TU CAMPAÑA</h2>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 box-border-gray text-dark-gray">
        <div>
            &nbsp; &nbsp; &nbsp;
            <i class="fa fa-calendar fa-2x text-red"></i>
            &nbsp; &nbsp; &nbsp;&nbsp;
            <span class="campana_fechas">
                @if ($model->fecha_desde)
                    @date($model->fecha_desde)
                @endif
                -
                @if ($model->fecha_hasta)
                    @date($model->fecha_hasta)
                @endif
            </span>
        </div>
        <br>
        <div>
            &nbsp; &nbsp; &nbsp;
            <i class="fa fa-credit-card fa-2x text-red"></i>
            &nbsp; &nbsp; &nbsp;
            <span class="campana_presupuesto">{{ $model->presupuesto_currency }}</span>
        </div>
    </div>
</div>

<p>&nbsp;</p>

@php
    $model->loadInfluenciadores();
    $listInfluenciadores = $model->influenciadores;
@endphp

<div class="row container-posts" id="posts_campana">
    @foreach ($listInfluenciadores as $influenciador)
        @include('campanas.posts_influenciador')
    @endforeach

    {{-- Modal Container for Bootstrap File Input --}}
    <div id="kvFileinputModal" class="file-zoom-dialog modal fade" tabindex="-1" aria-labelledby="kvFileinputModalLabel">
    </div>

    @include('common.chat')
</div>

@push('scripts')
    <script src="{{ asset('node_modules/bootstrap-fileinput/js/fileinput.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('node_modules/bootstrap-fileinput/js/locales/es.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/chat.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/campana_posts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/fileValidator.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/uploadfile_post.js') }}" type="text/javascript"></script>
@endpush
