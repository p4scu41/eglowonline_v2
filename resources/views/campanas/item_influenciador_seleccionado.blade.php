<div class="item-influenciador-seleccionado blue-box row"
    data-id="{{{ isset($id) ? $id : '@{{id}}' }}}"
    data-influenciador="">
    <div class="col-xs-4 col-sm-4 col-md-4 div-img">
        <span class="btn-delete">
            <i class="fa fa-times text-red"></i>
        </span>
        <img src="{{{ isset($twitter_profile_image_url) ? str_replace('_normal', '', $twitter_profile_image_url) : '@{{twitter_profile_image_url}}' }}}" class="img-influenciador">
    </div>
    <div class="col-xs-8 col-sm-8 col-md-8 div-body">
        <p class="nombre_influencer">{{{ isset($nombre) ? $nombre : '@{{nombre}}' }}}</p>
        <p>{{{ isset($tipo_influenciador) ? $tipo_influenciador : '@{{tipo_influenciador}}' }}}</p>
        <p>
            {{{ isset($twitter_screen_name) ? $twitter_screen_name : '@{{twitter_screen_name}}' }}}
            <span class="pull-right red-box">{{{ isset($qty_followers) ? \App\Helpers\Helper::formatNumber($qty_followers) : '@{{qty_followers}}' }}}</span>
        </p>
    </div>
</div>
