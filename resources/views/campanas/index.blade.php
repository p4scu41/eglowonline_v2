@extends('layouts.' . config('app.layout'))

@section('title', $title)

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <h3 class="title">MIS {{ strtoupper($title) }}</h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    {{-- Si se esta implementando Policy --}}
                    @if ($policy)
                        @can('create', $modelClass)
                        <p>
                            <a class="btn btn-red-rounded" href="{{ url($response_info['resource'] . '/create') }}" role="button">
                                <span class="glyphicon glyphicon-plus-sign"></span> Crear Campaña
                            </a>
                        </p>
                        @endcan
                    @else
                        <p>
                            <a class="btn btn-red-rounded" href="{{ url($response_info['resource'] . '/create') }}" role="button">
                                <span class="glyphicon glyphicon-plus-sign"></span> Crear Campaña
                            </a>
                        </p>
                    @endif
                </div>
            </div>

            @include($response_info['resource'] . '/_search', [
                'response_info' => $response_info,
                'lables' => $labels,
                'catalogs' => $catalogs,
            ])

            @include('common.errors', ['response_info' => $response_info])

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    {{ $models->appends(Request::query())->links() }}
                </div>

                @if (app('request')->has('filter'))
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="well well-sm text-success">
                            <span class="glyphicon glyphicon-zoom-in"></span> Resultados de la búsqueda
                        </div>
                    </div>
                @endif

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p>Mostrando {{ $models->firstItem() }}-{{ $models->lastItem() }} de {{ $models->total() }} elementos.</p>
                </div>
            </div>

            @forelse ($models as $model)
                <div class="row black-box row-eq-height">
                    <div class="col-xs-8 col-sm-8 col-md-4">
                        <h2>
                            <a href="{{ url('campanas', $model->id) }}">{{ $model->nombre }}</a>
                        </h2>
                        <p class="light-gray">{{ $model->tipoCampana->tipo }}</p>
                        <p class="text-red">{{ $model->hashtags }}</p>
                        <p>
                            @include('common.options_xs', [
                                'policy' => $policy,
                                'modelClass' => $modelClass,
                                'model' => $model,
                                'url' => url($response_info['resource'] . '/' . $model->id)
                            ])
                            @if($model->tieneTerminosYCondiciones())
                                <a class="btn btn-red-circle btn-xs" data-toggle="tooltip" title="Ver términos y condiciones" href="{{ url($response_info['resource'] . '/' . $model->id) . '/terminos' }}"><i class="fa fa-file"></i></a>
                            @endif
                            <a class="btn btn-red-circle btn-xs" data-toggle="tooltip" title="Estadísticas" href="{{ url($response_info['resource'] . '/' . $model->id) . '/estadisticas' }}">
                                <i class="fa fa-bar-chart"></i>
                            </a>
                        </p>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-2">
                        <br>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <i class="fa fa-calendar fa-2x"></i>&nbsp; &nbsp;
                                </td>
                                <td>
                                    @date($model->fecha_desde)<br />
                                    @date($model->fecha_hasta)
                                </td>
                            </tr>
                        </table>
                        <br>
                        <p>
                            <i class="fa fa-credit-card fa-2x"></i>&nbsp;&nbsp;
                            {{ $model->presupuesto_currency }}
                        </p>
                    </div>
                    @php
                        $influenciadoresMostrar   = 4;
                        $influenciadoresAprobados = $model->influenciadoresAprobados();
                        $totalAprobados           = count($influenciadoresAprobados);
                        $influenciadoresPorAprobar = $model->influenciadoresPorAprobar();
                        $totalPorAprobar           = count($influenciadoresPorAprobar);
                    @endphp
                    {{-- Crea el espacio cuando no hay pendientes por aprobar --}}
                    @if ($totalPorAprobar == 0)
                        <div class="col-xs-6 col-sm-6 col-md-3"></div>
                    @endif
                    <div class="col-xs-6 col-sm-6 col-md-3">
                        @if ($totalPorAprobar == 0 && $totalAprobados == 0)
                            <strong>NO SE ENCONTRARON INFLUENCERS PARA ESTA CAMPAÑA</strong>
                        @elseif ($totalPorAprobar == 0)
                            <strong>TODOS APROBADOS</strong>
                        @else
                            <strong>APROBADOS</strong>
                        @endif
                        <div>
                            @for ($i = 0; $i < $influenciadoresMostrar && $i < $totalAprobados; $i++)
                                @if ($influenciadoresAprobados[$i]->perfilTwitter)
                                    <img src="{{ str_replace('_normal', '', $influenciadoresAprobados[$i]->imgPerfil) }}" class="avatar small"
                                    data-toggle="tooltip" title="{{ $influenciadoresAprobados[$i]->nombre }}" data-id="{{ $influenciadoresAprobados[$i]->id }}">
                                @endif
                            @endfor

                            @if ($totalAprobados > $influenciadoresMostrar)
                                <span class="plus-influenciadores">+ {{ $totalAprobados - $influenciadoresMostrar }}</span>
                            @endif
                        </div>
                    </div>
                    {{-- Solo se muestra cuando hay pendientes por aprobar --}}
                    @if ($totalPorAprobar > 0)
                        <div class="col-xs-6 col-sm-6 col-md-3 gray-box">
                            <strong>POR APROBAR</strong>
                            <div>
                                @for ($i = 0; $i < $influenciadoresMostrar && $i < $totalPorAprobar; $i++)
                                    @if ($influenciadoresPorAprobar[$i]->perfilTwitter)
                                        <img src="{{ str_replace('_normal', '', $influenciadoresPorAprobar[$i]->imgPerfil) }}" class="avatar small"
                                        data-toggle="tooltip" title="{{ $influenciadoresPorAprobar[$i]->nombre }}" data-id="{{ $influenciadoresPorAprobar[$i]->id }}">
                                    @endif
                                @endfor

                                @if ($totalPorAprobar > $influenciadoresMostrar)
                                    <span class="plus-influenciadores">+ {{ $totalPorAprobar - $influenciadoresMostrar }}</span>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
            @empty
                <strong>No se encontraron campañas registradas.</strong>
            @endforelse

            <div class="text-center">
                {{ $models->appends(Request::query())->links() }}
            </div>
        </div>
    </div>
@endsection
