@php
$row = 1;
@endphp
@foreach($model->topFollowers() as $topFollower)
    @if($row === 1)
        <div class="row vertical-divider">
    @endif
        <div class="col-md-6">
            <div class="row blue-box">
                <div class="col-md-9 col-sm-9 no-padding">
                    <div class="pull-left bg-lightgray">
                       @if(!is_null($topFollower->image_url))
                            <img src="{{ str_replace('_normal', '', $topFollower->image_url) }}" style="width: 95px; height: 95px;">
                        @else
                            <img src="{{ asset('img/user_icon.png') }}" style="width: 95px; height: 95px;">
                        @endif
                    </div>
                    <div class="padding-min" style="margin-left:120px; margin-top: 5px;">
                        <p class="titulo-posts"></p>
                        <p class="text-small">@ {{ $topFollower->screen_name }}</p>
                        <p class="text-small">{{ $topFollower->biografia }}</p>
                        <p>
                            <button type="button" class="btn btn-red-rounded no-loading"
                                title="{{ number_format($topFollower->followers) }}" data-toggle="tooltip">
                                <i class="fa fa-twitter"></i>
                                {{ \App\Helpers\NumbersToStringConverter::toThousands($topFollower->followers) }}
                                </button>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-2 text-center">
                    <p class="circle-small">
                        <span class="ordinary">{{ $loop->index + 1 }}</span>
                    </p>
                </div>
            </div>
        </div>
    @if($row === 2)
        </div>
        <br>
        @php
            $row = 1;
        @endphp
    @else
        @if($loop->last)
            <div class="col-md-6"></div>
            </div>
            <br>
        @else
            @php
                $row++;
            @endphp
        @endif
    @endif
@endforeach