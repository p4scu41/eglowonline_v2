@section('section_title', 'Actualizar')

@include($response_info['resource'] . '._form', [
    'config_form' => [
        'id'                   => 'form-campana',
        'enctype'              => 'multipart/form-data',
        'model'                => $model,
        'update'               => $response_info['resource'] . '.update',
        'div_form_group_class' => 'col-xs-12 col-sm-12 col-md-12',
    ],
    'catalogs' => $catalogs,
])
