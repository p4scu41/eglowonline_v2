@push('styles')
    <link rel="stylesheet" href="{{ asset('js/plugins/jQCloud/dist/jqcloud.css') }}">
@endpush

    <div class="row trends border-bottom-gray" style="margin: 0px -51px; padding: 5px 36px; ">
        <!-- top user -->
         <div class="col-md-6 margin-top-row row-height-audience border-right-gray">
            <h4 class="text-center"><img src="{{ asset('img/campana/top_user.png') }}"> Top Users</h4>
                <div class="row ">
                    <div class="col-md-8">
                         <p>
                            <img src="{{ asset('img/campana/twitter.png') }}" class="img-small"> &nbsp;&nbsp;&nbsp;
                            <img src="{{ asset('img/campana/facebook_gris.png') }}" class="img-small"> &nbsp;&nbsp;&nbsp;
                            <img src="{{ asset('img/campana/instagram_gris.png') }}" class="img-small"> &nbsp;&nbsp;&nbsp;
                            <img src="{{ asset('img/campana/youtube_gris.png') }}" class="img-small">
                        </p>
                    </div>
                    <div class="col-md-3">
                        <select class="" id="sort-tbl-top-users"  style="padding: 2px;  margin-bottom: 5px; font-family: 'Cooper Hewitt'; font-weight: bold;">
                            <option value="4">Friends</option>
                            <option value="3" selected>Audience</option>
                            <option value="5">Post</option>
                        </select>
                    </div>
                </div>
                <div class="row-scroll row-height-audience" style="height: 315px;  padding:10px 10px 10px 10px;">
        @php
            $topFollowers = $model->topFollowers();
            $autolink = Twitter_Autolink::create();
        @endphp

        @if (count($topFollowers) != 0)
            <table id="tbl-top-users">
            <tbody>
        @endif

        @foreach($topFollowers as $topFollower)
         <tr class="row text-small " style="padding:5px 5px 5px 5px;">
            <td class="col-md-2 border-bottom  border-top" >
                 @if(!is_null($topFollower->image_url))
                    <img class="img-circle img-influencer " src="{{ str_replace('_normal', '', $topFollower->image_url) }}">
                @else
                    <img src="{{ asset('img/user_icon.png') }}" class="img-circle img-influencer">
                @endif
            </td>
            <td class="col-md-2 border-bottom  border-top">
                <p>{{ $topFollower->screen_name }}</p>
                <p class="text-center"><br></p>
            </td>
            <td class="col-md-2 border-bottom  border-top">
                <p><img src="{{ asset('img/campana/twitter.png') }}" class="img-small"></p>
                <p class="text-center"><br></p>
            </td>
            <td data-order="{{ $topFollower->followers }}" class="col-md-2 border-bottom  border-top"  >
                <p class="text-center">Audience</p>
                <p class="text-center">{{ \App\Helpers\NumbersToStringConverter::toThousands($topFollower->followers) }}</p>
            </td>
            <td data-order="{{ $topFollower->friends }}" class="col-md-2 border-bottom  border-top"  >
                <p class="text-center">Friends</p>
                <p class="text-center">{{ \App\Helpers\NumbersToStringConverter::toThousands($topFollower->friends) }} </p>
            </td>
            <td data-order="{{ $topFollower->tweets_top + $topFollower->tweets_eglow }}" class="col-md-2 border-bottom  border-right border-top" >
                <p class="text-center">Post</p>
                <p class="text-center">{{ $topFollower->tweets_top + $topFollower->tweets_eglow }}</p>
            </td>
        </tr>
     @endforeach

     @if(count($topFollowers) == 0)
        <div class="text-center">
            <strong >No hay Top User que mostrar</strong>
        </div>
      @else
          </tbody>
          </table>
      @endif
        </div>
        </div>
        <!-- top post -->
        <div class="col-md-6 margin-top-row  row-height-audience  ">
                <h4 class="text-center"><img src="{{ asset('img/campana/top_user.png') }}"> Top Post</h4>
                    <div class="row ">
                        <div class="col-md-8">
                            <img src="{{ asset('img/campana/twitter.png') }}" class="img-small"> &nbsp;&nbsp;&nbsp;
                            <img src="{{ asset('img/campana/facebook_gris.png') }}" class="img-small"> &nbsp;&nbsp;&nbsp;
                            <img src="{{ asset('img/campana/instagram_gris.png') }}" class="img-small"> &nbsp;&nbsp;&nbsp;
                            <img src="{{ asset('img/campana/youtube_gris.png') }}" class="img-small">
                        </div>
                        <div class="col-md-4">
                            <select class="form-select-drow " id="sort-tbl-top-post"  style="padding: 2px;  margin-bottom: 5px; font-family: 'Cooper Hewitt'; font-weight: bold;">
                                <option value="4" selected>Engagement</option>
                                <option value="3">Reach</option>
                                <option value="2">Interactions</option>
                            </select>
                        </div>
                    </div>

                    <div class="row-scroll row-height-audience" style="height: 315px; min-width:516px; padding:10px 10px 10px 10px; ">

                            @if (count($model->topTweets) != 0)
                                    <table id="tbl-top-post">
                                    <tbody>
                                @endif

                            @foreach ($model->topTweets as $publicacion)

                                        <tr class="row text-small  "  >
                                            <td class="col-md-1  border-bottom  border-top" >
                                                <a class="fancybox " href="{{ !empty($publicacion->multimedia) ? $publicacion->multimedia : str_replace('_normal', '', $publicacion->image_url) }}">
                                                    <img src="{{ !empty($publicacion->multimedia) ? $publicacion->multimedia : str_replace('_normal', '', $publicacion->image_url) }}" class="img-circle img-influencer">
                                                    <img src="{{ asset('img/campana/twitter.png') }}" class="img-small">
                                                </a>
                                                    <p> {{ '@'.$publicacion->screen_name }}</p>
                                            </td>
                                            <td class="col-md-6  border-bottom  border-top" >
                                                <!--Publicacion -->
                                                <p class="text-small"  >
                                                @php
                                                 $publ =  $publicacion->texto;

                                                $enlaces = null;
                                                $count_apariciones = preg_match('/(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/', $publ, $enlaces);


                                                 $url = empty ( $enlaces ) ? ' # ' : $enlaces[0];
                                                 $target = "_self";

                                                    if(! empty ( $enlaces )){
                                                        $target ="_blank";
                                                    }


                                                    if(strlen($publ) > 70){
                                                         $rest = substr($publ, 0, -95);
                                                         echo $rest. "...";
                                                    }else{
                                                        echo $publ;
                                                    }

                                               echo '<p><a style="color: blue;" href="' . $url . '" target="'. $target .'">View Post</a>'


                                                @endphp


                                                </p>

                                            </td>
                                            <td data-order="{{$publicacion->favorites }}"  class="col-md-2  border-bottom  border-top"  style="font-size:11px;" >
                                                <p class="text-center">Interactions</p>
                                                <p class="text-center"> {{ number_format($publicacion->favorites)}}</p>
                                            </td>
                                            <td data-order="{{$publicacion->alcance() }} " class="col-md-1  border-bottom  border-top" >
                                                <p class="text-center">Reach</p>
                                                <p class="text-center"> {{ \App\Helpers\Helper::formatNumber($publicacion->alcance()) }}</p>
                                            </td>
                                            <td data-order="{{$publicacion->engagement() }}" class="col-md-2  border-bottom  border-top border-right" >
                                                <p class="text-center">Engagement</p>
                                                <p class="text-center">{{ $publicacion->engagement() }}%</p>
                                            </td>
                                        </tr>

                             @endforeach

                              @if(count($model->topTweets) == 0)
                                <div class="text-center">
                                    <strong >No hay Top Post que mostrar</strong>
                                </div>
                              @else
                                  </tbody>
                                  </table>
                              @endif

                    </div>
        </div>

    </div>
    <div class="row border-bottom-gray " style="margin: 0px -51px; padding: 5px 36px;">
        <div class="col-md-6 content-tab border-right-gray" >
            <h4 class="text-center">@lang('campaign.key_words')</h4>
            <p class="hide text-center" id="mensajeNoEncontradoKeywords">
                <strong>@lang('campaign.no_encontrado_palabras_clave')</strong>
            </p>
            <div id="wordcloudPalabrasClave" class="jqcloud render-cloud" data-json="{{ json_encode( $model->WordCloudFromReplies()) }}"></div>
        </div>

        <div class="col-md-6">
            <h4 class="text-center">@lang('campaign.Comments')</h4>
            <div class="col-md-12" style="padding: 10px 40px 30px 30px;">

            <br><br>

              <textarea class="box-textarea-comment" rows="5" id="comment"></textarea>
              <button  class="text-center button-comment"  name="" >
                  <img src="{{ asset('img/campana/dico-duro.png')}}"/>
                  save
              </button>
            </div>
        </div>

    </div>

@push('scripts')
    <script src="{{ asset('js/plugins/jQCloud/dist/jqcloud.js') }}"></script>
    <script src="{{ asset('js/audience.js') }}"></script>
@endpush