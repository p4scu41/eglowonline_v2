@extends('layouts.' . config('app.layout') . '_search')

@section('search')
    {!! BootForm::horizontal([
            'method' => 'GET',
            'url' => url($response_info['resource']),
        ])
    !!}

        {!! BootForm::text('filter[nombre]', $labels['nombre'],
                app('request')->input('filter.nombre'), []
            )
        !!}

        {!! BootForm::select('filter[industria_id]', $labels['industria_id'],
                $catalogs['industria'], app('request')->input('filter.industria_id'), []
            )
        !!}

        {!! BootForm::text('filter[fecha_desde]', $labels['fecha_desde'],
                app('request')->input('filter.fecha_desde'), [
                    'class' => 'datepicker',
                ]
            )
        !!}

        {!! BootForm::text('filter[fecha_hasta]', $labels['fecha_hasta'],
                app('request')->input('filter.fecha_hasta'), [
                    'class' => 'datepicker',
                ]
            )
        !!}

        @if (Auth::user()->isAdministrador())
            {!! BootForm::select('filter[agencia_id]', $labels['agencia_id'],
                    $catalogs['agencia'], app('request')->input('filter.agencia_id'), []
                )
            !!}
        @endif

        {!! BootForm::select('filter[tipo_campana_id]', $labels['tipo_campana_id'],
                $catalogs['tipo_campana'], app('request')->input('filter.tipo_campana_id'), []
            )
        !!}

        <div class="form-group col-xs-12 col-md-12 text-center">
            {!! Form::button('<span class="glyphicon glyphicon-search"></span> Buscar', [
                    'type' => 'submit',
                    'class' => 'btn btn-red-rounded',
                ]) !!} &nbsp;
            <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
                <span class="glyphicon glyphicon-list"></span> Listar todos
            </a>
        </div>

    {!! BootForm::close() !!}
@endsection
