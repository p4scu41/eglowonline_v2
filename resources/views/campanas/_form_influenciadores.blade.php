<h2>SUMARIO CAMPAÑA</h2>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 info_marca">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <span class="gray_square">
                <img src="{{ asset('img/industria/7.png') }}" alt="Nombre Campaña">
            </span>
            <span class="campana_nombre campana-dato">{{ $model->nombre }}</span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <span class="gray_square">
                <img src="{{ asset('img/industria/7.png') }}"  alt="Presupuesto">
            </span>
            <span class="campana_presupuesto campana-dato">{{ $model->presupuesto_currency }}</span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <span class="gray_square">
                <img src="{{ asset('img/industria/7.png') }}" alt="Presupuesto">
            </span>
            <span class="campana_fechas campana-dato">
                @if ($model->fecha_desde)
                    @datelocate($model->fecha_desde)
                @endif
                -
                @if ($model->fecha_hasta)
                    @datelocate($model->fecha_hasta)
                @endif
            </span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <span class="gray_square">
                @if ($model->tipo_campana_id)
                    <img src="{{ asset('img/tipo_campana/' . $model->tipo_campana_id . '.png') }}" alt="Tipo de campaña" id="tipoCampana">
                @else
                    <img src="" alt="Tipo de campaña" id="tipoCampana">
                @endif
            </span>
            <span class="campana_tipo campana-dato">
                @if ($model->tipo_campana_id)
                    {{ $model->tipoCampana->tipo }}
                @endif
            </span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <span class="gray_square">
                @if ($model->industria_id)
                    <img src="{{ asset('img/industria/' . $model->industria_id . '.png') }}" alt="Industria" id="industria">
                @else
                    <img src="" alt="Industria" id="industria">
                @endif

            </span>
            <span class="campana_industria campana-dato">
                @if ($model->industria_id)
                    {{ $model->industria->nombre }}
                @endif
            </span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <span class="gray_square">
                <img src="{{ asset('img/industria/7.png') }}" alt="Hashtag">
            </span>
            <span class="campana_hashtags campana-dato">
                @if ($model->hashtags)
                    {{ implode(' ', $model->hashtags) }}
                @endif
            </span>
        </div>
    </div>
</div>

<p><br></p>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <h2 id="busca-text">BUSCA</h2>
        <p>Encuentra al mejor influenciador para tu Campaña.</p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8">
        <div class="form-group col-xs-12 col-sm-12 col-md-12">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" data-toggle="tooltip" title="Nombre de la celebridad"
                class="form-control" placeholder="Nombre de la celebridad"
                id="celebridad-name-search" name="celebridad-name-search">
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 text-right">
        <button type="button" id="btn-basic-search"
        class="hidden btn btn-red-rounded btn-encuentra-influenciador no-loading">ENCUENTRA</button>
        <button type="button" id="btn-toogle-busqueda-avanzada" class="btn btn-red-circle no-loading">
            <i class="fa fa-chevron-down" style=""></i>
        </button>
    </div>
</div>

<div class="row off" id="busqueda-avanzada" style="display: none; padding-bottom: 20px;">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group col-xs-12 col-sm-12 col-md-12">
            <div class="input-group" style="padding-right: 10px;padding-left: 10px;">
                <span class="input-group-addon"></span>
                <div class="form-control float-initial">
                    Cantidad de Seguidores
                    <input type="text" id="cantidad_seguidores" name="cantidad_seguidores" value="" />
                </div>
            </div>
        </div>

        <div class="form-group col-xs-12 col-sm-12 col-md-12">
            <div class="input-group" style="padding-right: 10px; padding-left: 10px;">
                <span class="input-group-addon"></span>
                <div class="form-control float-initial">
                    <div>{{ $labels['rango_etario_id'] }}</div>

                    <select id="rango_etario_id" name="rango_etario_id" class="selectToList" multiple="true">
                        @foreach ($catalogs['rango_etario'] as $rango_etario)
                            <option value="{{ $rango_etario['id'] }}"
                                data-imagen="{{ asset($rango_etario['icono']) }}">
                                    {{ $rango_etario['descripcion'] }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group col-xs-12 col-sm-12 col-md-12">
            <div class="input-group" style="padding-right: 10px; padding-left: 10px;">
                <span class="input-group-addon"></span>
                <div class="form-control float-initial">
                    <div>{{ $labels['tipo_influenciador_id'] }}</div>

                    <select id="tipo_influenciador_id" name="tipo_influenciador_id" class="selectToList" multiple="true">
                        @foreach ($catalogs['tipo_influenciador'] as $tipo_influenciador)
                            <option value="{{ $tipo_influenciador['id'] }}"
                                data-imagen="{{ asset($tipo_influenciador['icono']) }}">
                                    {{ $tipo_influenciador['descripcion'] }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group col-xs-12 col-sm-12 col-md-12">
            <div class="input-group" style="padding-right: 10px;padding-left: 10px;">
                <span class="input-group-addon"></span>
                <div class="form-control float-initial">
                    Distribución de Género
                    <input type="text" id="distribucion_genero" name="distribucion_genero" value="" />
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group col-xs-12 col-sm-12 col-md-12">
            <div class="input-group">
                <span class="input-group-addon"></span>
                <div class="form-control float-initial">
                    <div>{{ $labels['genero'] }}</div>

                    <select id="genero" name="genero" class="selectToList" multiple="true">
                        @foreach ($catalogs['genero'] as $genero)
                            <option value="{{ $genero['id'] }}"
                                data-imagen="{{ asset($genero['icono']) }}">
                                    {{ $genero['genero'] }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group col-xs-12 col-sm-12 col-md-12">
            <div class="input-group">
                <span class="input-group-addon"></span>
                <div class="form-control float-initial">
                    <input type="text" title="{{ $labels['manager_nombre'] }}" class="form-control"
                        id="manager_nombre" name="manager_nombre" data-toggle="tooltip" placeholder="{{ $labels['manager_nombre'] }}">
                </div>
            </div>
        </div>

        <div class="form-group col-xs-12 col-sm-12 col-md-12">
            <div class="input-group">
                <span class="input-group-addon"></span>
                <div class="form-control float-initial">
                    <select data-toggle="tooltip" title="{{ $labels['pais_id'] }}" class="form-control"
                        id="pais_id" name="pais_id" style="width: 100%;">
                        @php
                            foreach($catalogs['pais'] as $id => $pais) {
                                echo '<option value="'.$id.'">'.$pais.'</option>';
                            }
                        @endphp
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group col-xs-12 col-sm-12 col-md-12">
            <div class="input-group">
                <span class="input-group-addon"></span>
                <div class="form-control float-initial">
                    <select data-toggle="tooltip" title="Palabra Clave" class="form-control"
                        id="palabra_clave_search" name="palabra_clave_search" style="width: 100%;">
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group col-xs-12 col-sm-12 col-md-12">
            <div id="palabra-clave-box">
                <ul id="palabra-clave-list">
                </ul>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-right">
        <button type="button" id="btn-advanced-search"
            class="btn btn-red-rounded btn-encuentra-influenciador no-loading">ENCUENTRA</button>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8">
        <h2><span id="count-result-search"></span> INFLUENCERS</h2>
        <strong>Arma tu selección con los influencers disponibles.</strong>
        <div id="list-influenciadores-resultado-busqueda" class="row"></div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <ul class="pagination" style="display: none;">
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 container-list-influenciadores-seleccionados">
        <strong>Selección de Influenciadores</strong>
        <p class="text-right">
            <button type="button" id="btn-next-influenciadores-seleccionados"
                class="btn btn-red-rounded no-loading">SIGUIENTE</button>
        </p>

        <div id="list-influenciadores-seleccionados">
            @foreach ($model->influenciadores as $influenciador)
                @php
                    $influenciador->setJsonProfileStatistics();
                @endphp
                @include($response_info['resource'] . '/item_influenciador_seleccionado', [
                    'id'                          => $influenciador->id,
                    'nombre'                      => $influenciador->nombre,
                    'tipo_influenciador'          => $influenciador->tipoInfluenciador->descripcion,
                    'qty_followers'               => $influenciador->qty_followers,
                    'twitter_profile_image_url'   => ($influenciador->perfilTwitter ? $influenciador->perfilTwitter->avatar() : ''),
                    'twitter_screen_name'         => ($influenciador->perfilTwitter ? $influenciador->perfilTwitter->screen_name : ''),
                    'twitter_cantidad_seguidores' => ($influenciador->perfilTwitter ? $influenciador->perfilTwitter->cantidad_seguidores_k : ''),
                ])
            @endforeach
        </div>
    </div>
</div>

@if ($model->exists)
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <a class="btn btn-red-rounded btn-save-campana">
                <span class="glyphicon glyphicon-floppy-save"></span> Guardar
            </a>
        </div>
    </div>
@endif
