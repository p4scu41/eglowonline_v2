@push('styles')
    <link rel="stylesheet" href="{{ asset('js/plugins/fancyBox/jquery.fancybox.css') }}">
@endpush

<h2>ESTADISTICAS DE TU CAMPAÑA</h2>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 box-border-gray">
        <div class="col-xs-2 col-sm-2 col-md-2 text-center">
            @if ($model->producto_id)
                <img src="{{ asset($model->producto->getImagen()) }}" class="img-campana" id="img-campana">
            @elseif ($model->marca_id)
                <img src="{{ asset($model->marca->getLogotipo()) }}" class="img-campana" id="img-campana">
            @else
                <img src="{{ asset($model->agencia->getLogotipo()) }}" class="img-campana" id="img-campana">
            @endif
        </div>
        <div class="col-xs-5 col-sm-5 col-md-5">
            <h3 class="campana_nombre">{{ $model->nombre }}</h3>
            <h4 class="campana_tipo text-dark-gray">
                @if ($model->tipo_campana_id)
                    {{ $model->tipoCampana->tipo }}
                @endif
            </h4>
        </div>
        <div class="col-xs-5 col-sm-5 col-md-5">
            <p>&nbsp;</p>
            <label class="campana_hashtags text-red">
                {{ $model->hashtags }}
            </label>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>

    @php
        $estadisticas = $model->getReport();
    @endphp

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <strong class="text-center btn btn-red-rounded btn-estadistica">
            PUBLICACIONES<br>
            {{ $model->estadisticas ? $model->estadisticas->total_publicaciones : '0' }}
        </strong>

        <strong class="text-center btn btn-red-rounded btn-estadistica">
            INFLUENCERS<br>
            {{ $model->estadisticas ? $model->estadisticas->total_autores : '0' }}
        </strong>

        <strong class="text-center btn btn-red-rounded btn-estadistica"
            title="{{ number_format($model->interacciones()) }}" data-toggle="tooltip">
            INTERACCIONES<br>
            {{ \App\Helpers\Helper::formatNumber($model->interacciones()) }}
        </strong>

        <strong class="text-center btn btn-red-rounded btn-estadistica">
            ENGAGEMENT<br>
            {{ $model->engagement() }}%
        </strong>

        <strong class="text-center btn btn-red-rounded btn-estadistica"
            title="{{ number_format($model->alcanceOrganico()) }}" data-toggle="tooltip">
            ALCANCE ORGÁNICO<br>
            {{ \App\Helpers\Helper::formatNumber($model->alcanceOrganico()) }}
        </strong>

        <strong class="text-center btn btn-red-rounded btn-estadistica"
            title="{{ number_format($model->alcanceTotal()) }}" data-toggle="tooltip">
            ALCANCE TOTAL<br>
            {{ \App\Helpers\Helper::formatNumber($model->alcanceTotal()) }}
        </strong>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center black-box" id="menu-estadisticas">
        <div>
            <a class="text-center btn no-loading btn-red-rounded" href="#tab_publicaciones_campana" >PUBLICACIONES DE CAMPAÑA</a>
            <a class="text-center btn no-loading" href="#tab_alcance_real">ALCANCE REAL</a>
            <a class="text-center btn no-loading" href="#tab_influencers_comunidad">INFLUENCERS Y COMUNIDAD</a>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 content-tab" id="tab_publicaciones_campana">
        @include('campanas.estadisticas_publicaciones')
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 content-tab" id="tab_alcance_real" style="display: none;">
         @include('campanas.estadisticas_toppublicaciones')
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 content-tab" id="tab_influencers_comunidad" style="display: none;">
        @include('campanas.estadisticas_influencers')
    </div>

</div>

@push('scripts')
    <script src="{{ asset('js/campana_estadisticas.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/fancyBox/jquery.fancybox.js') }}"></script>
@endpush
