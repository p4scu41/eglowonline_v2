<table class="table table-stripped table-hover">
    <thead>
        <tr>
            <th class="text-center" style="width: 250px;">INFLUENCER</th>
            <th class="text-center">ENGAGEMENT</th>
            <th class="text-center">FECHA</th>
            <th class="text-center">CONTENIDO</th>
            <th class="text-center">FAVORITOS</th>
            <th class="text-center">RT</th>
            <th class="text-center">ALCANCE</th>
        </tr>
    </thead>
    <tbody>
        @php
            $autolink = Twitter_Autolink::create();
        @endphp

        @foreach ($model->influenciadoresAprobados as $influenciador)
            @if ($influenciador->perfilTwitter)
                @php
                    $publicaciones = $influenciador->publicacionesCampanaTwitter($model->id)
                @endphp

                 @foreach ($publicaciones as $publicacion)
                    @if ($publicacion->isAprobadoPublicado())
                        <tr>
                            <td>
                                <img src="{{ str_replace('_normal', '', $influenciador->imgPerfil) }}" class="avatar">
                                {{ $influenciador->perfilTwitter ? $influenciador->perfilTwitter->screen_name : $influenciador->nombre }}<br><br>
                                <span class="btn btn-xs btn-red"><i class="fa fa-twitter text-black fa-2x"></i></span>
                            </td>
                            <td class="text-center">{{ $publicacion->engagement() }}%</td>
                            <td>@datetime($publicacion->fecha_hora_publicacion)</td>
                            <td class="content_post">{!! $autolink->autoLink($publicacion->contenido) !!}</td>
                            <td class="text-center">{{ $publicacion->estadisTwitter ? number_format($publicacion->estadisTwitter->favorites, 0) : '0' }}</td>
                            <td class="text-center">{{ $publicacion->estadisTwitter ? number_format($publicacion->estadisTwitter->retweets, 0) : '0' }}</td>
                            <td class="text-center">
                                <span
                                    title="{{ number_format($influenciador->perfilTwitter->cantidad_seguidores) . ' + ' .
                                    ($publicacion->estadisTwitter ? number_format($publicacion->estadisTwitter->total_followers_rt) : '0') . ' = ' .
                                    number_format($publicacion->alcance()) }}"
                                    data-toggle="tooltip">
                                    {{ \App\Helpers\Helper::formatNumber($publicacion->alcance()) }}
                                </span>
                            </td>
                        </tr>
                    @endif
                @endforeach
            @endif
        @endforeach
    </tbody>
</table>