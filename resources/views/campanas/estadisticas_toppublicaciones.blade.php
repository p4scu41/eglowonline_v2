<table class="table table-stripped table-hover">
    <thead>
        <tr>
            <th class="text-center" style="width: 250px;">INFLUENCER</th>
            <th class="text-center">ENGAGEMENT</th>
            <th class="text-center">FECHA</th>
            <th class="text-center">CONTENIDO</th>
            <th class="text-center">FAVORITOS</th>
            <th class="text-center">RT</th>
            <th class="text-center">ALCANCE</th>
        </tr>
    </thead>
    <tbody>
        @php
            $autolink = Twitter_Autolink::create();
        @endphp

        @foreach ($model->topTweets as $publicacion)
            <tr>
                <td>
                    <a class="fancybox" href="{{ !empty($publicacion->multimedia) ? $publicacion->multimedia : str_replace('_normal', '', $publicacion->image_url) }}">
                        <img src="{{ !empty($publicacion->multimedia) ? $publicacion->multimedia : str_replace('_normal', '', $publicacion->image_url) }}" class="avatar">
                    </a>
                    {{ $publicacion->screen_name }}
                    <br><br>
                    <span class="btn btn-xs btn-red"><i class="fa fa-twitter text-black fa-2x"></i></span>
                </td>
                <td class="text-center">{{ $publicacion->engagement() }}%</td>
                <td>@datetime($publicacion->fecha)</td>
                <td class="content_post">{!! $autolink->autoLink($publicacion->texto) !!}</td>
                <td class="text-center">{{ number_format($publicacion->favorites, 0) }}</td>
                <td class="text-center">{{ number_format($publicacion->retweets, 0) }}</td>
                <td class="text-center">
                    <span title="{{ number_format($publicacion->followers) . ' + ' .
                        number_format($publicacion->total_followers_rt) . ' = '.
                        number_format($publicacion->alcance(), 0) }}" data-toggle="tooltip">
                        {{ \App\Helpers\Helper::formatNumber($publicacion->alcance()) }}
                    </span>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>