<h2>REVISA EL PANORAMA DE TU CAMPAÑA</h2>

<div class="row">
    <div class="col-xs-2 col-sm-2 col-md-2 text-center">
        @if ($model->producto_id)
            <img src="{{ asset($model->producto->getImagen()) }}" class="img-campana" id="img-campana">
        @elseif ($model->marca_id)
            <img src="{{ asset($model->marca->getLogotipo()) }}" class="img-campana" id="img-campana">
        @else
            <img src="{{ $model->agencia ? asset($model->agencia->getLogotipo()) : (Auth::user()->isAgencia() ? asset(Auth::user()->agencia->getLogotipo()) : '') }}" class="img-campana" id="img-campana">
        @endif
    </div>
    <div class="col-xs-5 col-sm-5 col-md-5">
        <h3 class="campana_nombre">{{ $model->nombre }}</h3>
        <h4 class="campana_tipo text-dark-gray">
            @if ($model->tipo_campana_id)
                {{ $model->tipoCampana->tipo }}
            @endif
        </h4>
    </div>
    <div class="col-xs-5 col-sm-5 col-md-5">
        <p>&nbsp;</p>
        <label class="campana_hashtags text-red">
            @if ($model->hashtags)
                {{ implode(' ', $model->hashtags) }}
            @endif
        </label>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>

    <div class="col-xs-12 col-sm-12 col-md-12 box-border-gray text-dark-gray">
        <div>
            &nbsp; &nbsp; &nbsp;
            <i class="fa fa-calendar fa-2x text-red"></i>
            &nbsp; &nbsp; &nbsp;&nbsp;
            <span class="campana_fechas capitalize">
                @if ($model->fecha_desde)
                    @datelocate($model->fecha_desde)
                @endif
                -
                @if ($model->fecha_hasta)
                    @datelocate($model->fecha_hasta)
                @endif
            </span>
        </div>
        <br>
        <div>
            &nbsp; &nbsp; &nbsp;
            <i class="fa fa-credit-card fa-2x text-red"></i>
            &nbsp; &nbsp; &nbsp;
            <span class="campana_presupuesto">{{ $model->presupuesto_currency }}</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>

    <div class="col-xs-12 col-sm-12 col-md-12 box-border-gray">
        <strong class="text-dark-gray">INFLUENCERS</strong>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12" id="panorama_influencers">
                @foreach ($model->influenciadores as $influenciador)
                    @if ($influenciador->perfilTwitter)
                        <img src="{{ str_replace('_normal', '', $influenciador->imgPerfil) }}" class="avatar"
                        data-toggle="tooltip" title="{{ $influenciador->nombre }}" data-id="{{ $influenciador->id }}">
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <p>&nbsp;</p>
    </div>

    <div class="text-right col-xs-12 col-sm-12 col-md-12">
        <a class="btn btn-red-rounded" href="{{ url('campanas') }}">
            CANCELA
        </a>&nbsp;
        <a class="btn btn-red-rounded btn-save-campana">
            <span class="glyphicon glyphicon-floppy-save"></span> Guardar
        </a>
    </div>
</div>
