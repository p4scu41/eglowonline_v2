@section('section_title', 'Actualizar')

@include($response_info['resource'] . '._form', [
    'config_form' => [
        'id' => 'usuario-form',
        'model' => $model,
        'update' => $response_info['resource'] . '.update',
        //'right_column_class' => 'col-sm-10 col-md-8',
        'data-validate' => 'true',
    ],
    'catalogs' => $catalogs,
])
