@extends('layouts.' . config('app.layout'))

@section('title', $title)

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li><a href="{{ url($response_info['resource']) }}">{{ $title }}</a></li>
        <li class="active">@yield('section_title')</li>
    </ol>
@endsection

@section('content')
    @if (Auth::user()->isAdministrador())
        <p>
            <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
                <span class="glyphicon glyphicon-arrow-left"></span> Regresar
            </a>
        </p>
    @endif

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Datos del {{ $title }} </h3>
        </div>
        <div class="panel-body">
            @include('common.errors', ['response_info' => $response_info])

            {!! BootForm::horizontal($config_form) !!}

                @if (Auth::user()->isAdministrador())
                    {!! BootForm::select('tipo_usuario_id', false,
                            $catalogs['tipo_usuario'], null, [
                                'data-rule-required' => 'true',
                                'data-toggle'        => 'tooltip',
                                'title'              => $labels['tipo_usuario_id'],
                                'icon'               => '&nbsp; &nbsp;',
                            ]
                        )
                    !!}
                @endif

                {!! BootForm::text('nombre', false, null, [
                        'data-rule-required'  => 'true',
                        'data-rule-minlength' => '3',
                        'data-toggle'         => 'tooltip',
                        'placeholder'         => $labels['nombre'],
                        'title'               => $labels['nombre'],
                        'icon'                => '&nbsp; &nbsp;',
                    ])
                !!}

                {!! BootForm::email('email', false, null, [
                        'data-rule-required'  => 'true',
                        'data-rule-minlength' => '3',
                        'data-toggle'         => 'tooltip',
                        'placeholder'         => $labels['email'],
                        'title'               => $labels['email'],
                        'icon'                => '&nbsp; &nbsp;',
                    ])
                !!}

                @if (Request::is('*/create'))
                    {!! BootForm::password('password', false, [
                            'id'                  => 'password',
                            'data-rule-required'  => 'true',
                            'data-rule-minlength' => '8',
                            'data-rule-passwordvalidation' => 'true',
                            'data-toggle'         => 'tooltip',
                            'placeholder'         => $labels['password'],
                            'title'               => $labels['password'],
                            'icon'                => '&nbsp; &nbsp;',
                        ])
                    !!}

                    {!! BootForm::password('password_confirmation', false, [
                            'data-rule-required'  => 'true',
                            'data-rule-equalto'   => '#password',
                            'data-rule-minlength' => '8',
                            'data-toggle'         => 'tooltip',
                            'placeholder'         => $labels['password_confirmation'],
                            'title'               => $labels['password_confirmation'],
                            'icon'                => '&nbsp; &nbsp;',
                        ])
                    !!}
                @endif

                @if (Auth::user()->isAdministrador())
                    {!! BootForm::select('activo', false,
                            ['' => 'Activo', '1' => 'Si', '0' => 'No'], null, [
                                'data-rule-required' => 'true',
                                'data-toggle'        => 'tooltip',
                                'title'              => $labels['activo'],
                                'icon'               => '&nbsp; &nbsp;',
                            ]
                        )
                    !!}
                @endif

                {{-- {!! BootForm::checkbox('activo', $labels['activo'], 1, Request::is('*/create') ? true : null) !!} --}}

                {!! BootForm::button('<span class="glyphicon glyphicon-floppy-save"></span> Guardar', [
                        'type'                     => 'submit',
                        'class'                    => 'btn btn-red-rounded',
                        'div_form_group_class'     => 'col-xs-12 col-sm-12 col-md-12',
                        'right_column_class'       => 'col-xs-12 col-sm-12 col-md-12',
                        'left_column_offset_class' => 'text-center',
                    ])
                !!}

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection
