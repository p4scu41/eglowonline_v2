@extends('layouts.' . config('app.layout'))

@section('title', $title)

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li><a href="{{ url($response_info['resource']) }}">{{ $title }}</a></li>
        <li class="active">Ver</li>
    </ol>
@endsection

@section('content')
    <p>
        <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
            <span class="glyphicon glyphicon-arrow-left"></span> Regresar
        </a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Datos del {{ $title }} </h3>
        </div>
        <div class="panel-body">
            @include('common.errors', ['response_info' => $response_info])

            <div class="col-md-8 col-md-offset-2">
            @if ($model)
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td><label>{{ $labels['tipo_usuario_id'] }}</label></td>
                            <td>{{ $model->tipoUsuario->descripcion }}</td>
                        </tr>
                        <tr>
                            <td><label>{{ $labels['nombre'] }}</label></td>
                            <td>{{ $model->nombre }}</td>
                        </tr>
                        <tr>
                            <td><label>{{ $labels['email'] }}</label></td>
                            <td>{{ $model->email }}</td>
                        </tr>
                        <tr>
                            <td><label>{{ $labels['activo'] }}</label></td>
                            <td>@boolean($model->activo)</td>
                        </tr>
                        <tr>
                            <td><label>{{ $labels['ultimo_acceso'] }}</label></td>
                            <td>@datetime($model->ultimo_acceso)</td>
                        </tr>

                        @if ($model->isAgencia())
                            <tr>
                                <td><label>Agencia</label></td>
                                <td>
                                    @if ($model->agencia)
                                        <a href="{{ url('agencias', $model->agencia->id) }}">
                                            {{ $model->agencia->nombre }}
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @elseif ($model->isMarca())
                            <tr>
                                <td><label>Marca</label></td>
                                <td>
                                    @if ($model->marca)
                                        <a href="{{ url('marcas', $model->marca->id) }}">
                                            {{ $model->marca->nombre }}
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @elseif ($model->isProducto())
                            <tr>
                                <td><label>Producto</label></td>
                                <td>
                                    @if ($model->producto)
                                        <a href="{{ url('productos', $model->producto->id) }}">
                                            {{ $model->producto->nombre }}
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @elseif ($model->isCelebridad())
                            <tr>
                                <td><label>Celebridad</label></td>
                                <td>
                                    @if ($model->influenciador)
                                        <a href="{{ url('influenciadores', $model->influenciador->id) }}">
                                            {{ $model->influenciador->nombre }}
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endif

                    </tbody>
                </table>
            @endif
            </div>

        </div>
    </div>
@endsection
