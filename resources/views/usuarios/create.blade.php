@section('section_title', 'Registrar')

@include($response_info['resource'] . '._form', [
    'config_form' => [
        'id' => 'usuario-form',
        'model' => $model,
        'store' => $response_info['resource'] . '.store',
        //'right_column_class' => 'col-sm-10 col-md-8',
        'data-validate' => 'true',
    ],
    'catalogs' => $catalogs,
])
