@extends('layouts.' . config('app.layout') )

@section('title', $title)

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li><a href="{{ url($response_info['resource']) }}">{{ $title }}</a></li>
    </ol>
@endsection

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Datos del usuario</h3>
        </div>
        <div class="panel-body">
            @include('common.errors', ['response_info' => $response_info])

            {!! BootForm::horizontal([
                    'model' => $model,
                    'update' => $response_info['resource'] . '.updateperfil',
                    'data-validate' => 'true',
                ])
            !!}
                {!! BootForm::text('nombre', $labels['nombre'], null, [
                        'data-rule-required' => 'true',
                    ])
                !!}

                {!! BootForm::email('email', $labels['email'], null, [
                        'data-rule-required' => 'true',
                    ])
                !!}

                <div class="form-group col-xs-12 col-md-12 text-center">
                    {!! Form::button('<span class="glyphicon glyphicon-floppy-save"></span> Guardar', [
                            'type' => 'submit',
                            'class' => 'btn btn-red-rounded',
                        ]) !!} &nbsp;
                    <a class="btn btn-default no-loading" href="#modalChangePass" data-toggle="modal">
                        <i class="fa fa-key pull-right"></i> Cambiar Contraseña
                    </a>
                </div>

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection
