@extends('layouts.' . config('app.layout') . '_search')

@section('search')
    {!! BootForm::horizontal([
            'method' => 'GET',
            'url' => url($response_info['resource']),
            //'right_column_class' => 'col-sm-10 col-md-8',
        ])
    !!}

        {!! BootForm::text('filter[nombre]', $labels['nombre'],
                app('request')->input('filter.nombre'), []
            )
        !!}

        {!! BootForm::text('filter[email]', $labels['email'],
                app('request')->input('filter.email'), []
            )
        !!}

        {!! BootForm::select('filter[tipo_usuario_id]', $labels['tipo_usuario_id'],
                $catalogs['tipo_usuario'], app('request')->input('filter.tipo_usuario_id'), []
            )
        !!}

        {{-- <div class="form-group col-xs-12 col-sm-12 col-md-6">
            {!! Form::label('filter[clave]', $labels['clave'], [
                    'class' => 'control-label col-xs-4 col-sm-2 col-md-3',
                ]) !!}
            <div class="col-xs-8 col-sm-10 col-md-8">
                {!! Form::text('filter[clave]', app('request')->input('filter.clave'), [
                    'class' => 'form-control',
                ]) !!}
            </div>
        </div>

        <div class="form-group col-xs-12 col-sm-12 col-md-6">
            {!! Form::label('filter[descripcion]', $labels['descripcion'], [
                    'class' => 'control-label col-xs-4 col-sm-2 col-md-3',
                ]) !!}
            <div class="col-xs-8 col-sm-10 col-md-8">
                {!! Form::text('filter[descripcion]', app('request')->input('filter.descripcion'), [
                    'class' => 'form-control',
                ]) !!}
            </div>
        </div> --}}

        <div class="form-group col-xs-12 col-md-12 text-center">
            {!! Form::button('<span class="glyphicon glyphicon-search"></span> Buscar', [
                    'type' => 'submit',
                    'class' => 'btn btn-red-rounded',
                ]) !!} &nbsp;
            <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
                <span class="glyphicon glyphicon-list"></span> Listar todos
            </a>
        </div>

    {!! BootForm::close() !!}
@endsection
