@extends('layouts.' . config('app.layout') . '_search')

@section('search')
    {!! BootForm::horizontal([
            'method' => 'GET',
            'url' => url($response_info['resource']),
        ])
    !!}

        {!! BootForm::text('filter[fecha_desde]', $labels['fecha_desde'],
                app('request')->input('filter.fecha_desde'), [
                    'class' => 'datepicker',
                ]
            )
        !!}

        {!! BootForm::text('filter[fecha_hasta]', $labels['fecha_hasta'],
                app('request')->input('filter.fecha_hasta'), [
                    'class' => 'datepicker',
                ]
            )
        !!}

        {!! BootForm::select('filter[tipo]', $labels['tipo'],
                $catalogs['tipo'], app('request')->input('filter.tipo'), []
            )
        !!}

        <div class="form-group col-xs-12 col-md-12 text-center">
            {!! Form::button('<span class="glyphicon glyphicon-search"></span> Buscar', [
                    'type' => 'submit',
                    'class' => 'btn btn-red-rounded',
                ]) !!} &nbsp;
            <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
                <span class="glyphicon glyphicon-list"></span> Listar todos
            </a>
        </div>

    {!! BootForm::close() !!}
@endsection
