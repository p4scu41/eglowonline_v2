@extends('layouts.' . config('app.layout'))

@section('title', $title)

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <h3 class="title">{{ strtoupper($title) }}</h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    {{-- Si se esta implementando Policy --}}
                    @if ($policy)
                        @can('create', $modelClass)
                        <p>
                            <a class="btn btn-red-rounded" href="{{ url($response_info['resource'] . '/create') }}" role="button">
                                <span class="glyphicon glyphicon-plus-sign"></span> Registrar
                            </a>
                        </p>
                        @endcan
                    @else
                        <p>
                            <a class="btn btn-red-rounded" href="{{ url($response_info['resource'] . '/create') }}" role="button">
                                <span class="glyphicon glyphicon-plus-sign"></span> Registrar
                            </a>
                        </p>
                    @endif
                </div>
            </div>

            @include($response_info['resource'] . '/_search', [
                'response_info' => $response_info,
                'lables' => $labels,
                'catalogs' => $catalogs,
            ])

            @include('common.errors', ['response_info' => $response_info])

            <div class="text-center">
                {{ $models->appends(Request::query())->links() }}
            </div>

            @if (app('request')->input('filter'))
                <div class="well well-sm text-success">
                    <span class="glyphicon glyphicon-zoom-in"></span> Resultados de la búsqueda
                </div>
            @endif

            Mostrando {{ $models->firstItem() }}-{{ $models->lastItem() }} de {{ $models->total() }} elementos.

            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th style="width: 40px;" class="text-center"></th>
                        <th>{{ $labels['created_by'] }}</th>
                        <th>{{ $labels['tipo_usuario'] }}</th>
                        <th>{{ $labels['tipo'] }}</th>
                        <th>{{ $labels['created_at'] }}</th>
                        <th>{{ $labels['campana_id'] }}</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0; ?>
                    @forelse ($models as $model)
                        <tr>
                            <td class="text-center">{{ $models->firstItem() + ($i++) }}</td>
                            <td>{{ $model->createdBy ? $model->createdBy->nombre : '' }}</td>
                            <td>{{ $model->createdBy ? $model->createdBy->tipoUsuario->descripcion : '' }}</td>
                            <td>{{ $model->tipo_descripcion }}</td>
                            <td>@datetime($model->created_at)</td>
                            <td>{{ $model->campana ? $model->campana->nombre : '' }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">No se encontrarón datos.</td>
                        </tr>
                    @endforelse

                </tbody>
            </table>

            <div class="text-center">
                {{ $models->appends(Request::query())->links() }}
            </div>
        </div>
    </div>
@endsection
