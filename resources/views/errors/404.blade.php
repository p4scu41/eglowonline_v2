@extends('layouts.' . config('app.layout') )

@section('title', 'Error 404')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li class="active">Error 404</li>
    </ol>
@endsection

@section('content')
    <h2 class="text-danger text-center">Datos no disponibles.</h2>

    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Recurso no encontrado.</strong> No se encuentra disponible el recurso solicitado.
    </div>

    <div class="text-center">
        <a href="{{ url('/') }}">Regresar a Inicio</a>
    </div>
@endsection
