@extends('layouts.' . config('app.layout') )

@section('title', 'Error 403')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li class="active">Error 403</li>
    </ol>
@endsection

@section('content')
    <h2 class="text-danger text-center">Acceso denegado</h2>

    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Acceso no autorizado.</strong> No tiene privilegios para acceder a este recurso.
    </div>

    <div class="text-center">
        <a href="{{ url('/') }}">Regresar a Inicio</a>
    </div>
@endsection
