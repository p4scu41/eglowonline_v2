@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
           <li class="disabled"><span>&laquo; eGL</span></li>
        @else
           <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo; eGL</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
               <!--<li class="disabled"><span>{{ $element }}</span></li>-->
            @endif

            <!-- Ignora los items que tengan menos de 2 elementos -->
            @if (count($element) <= 2)
               @php
                   continue;
               @endphp
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                       <li class="active"><span>{!! 'O<br>'.$page !!}</span></li>
                    @else
                          <li><a href="{{ $url }}">
                            {!! 'O<br>'.$page !!}
                            </a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
           <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">W &raquo; </a></li>
        @else
            <li class="disabled"><span>W &raquo;</span></li>
        @endif
    </ul>
@endif