<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style type="text/css" rel="stylesheet" media="all">
        /* Media Queries */
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>

<?php

$style = [
    /* Layout ------------------------------ */
    'body'                    => 'margin: 0 auto; padding: 0; width: 650px;',
    'email-wrapper'           => 'width: 100%; margin: 0; padding: 0;',
    'blue-bar'                => 'width: 100%; height:5px !important; background: #59cecc;',

    /* Masthead ----------------------- */
    'email-masthead'          => 'height:91px; background: linear-gradient(to-right, #020833, #000053); background: -webkit-linear-gradient(left, #020833, #000053); background: -o-linear-gradient(right, #020833, #000053); background: -moz-linear-gradient(right, #020833, #000053);',
    'email-masthead_name'     => 'font-size: 38pt; color: #FFF; text-decoration: none; text-shadow: 0 1px 0 white;',
    'email-masthead-small'    => 'font-size: 10px; font-weight: bold; color: #59cecc;',
    'email-body'              => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;',
    'email-body_inner'        => 'width: auto;  margin: 0 auto; padding: 0;',
    'email-body_cell'         => 'padding: 35px 76px !important; color: #0b0339 !important; font-size: 12pt !important; text-align: justify;',
    'email-footer'            => 'width: auto; height:47px; margin: 0 auto; padding: 0; text-align: center;',
    'email-footer_cell'       => 'color: #FFF; height:47px; text-align: center;',
    'email-footer-background' => 'background: #0b0339',
    'email-logos'             => 'margin-top: 35px; height: 91px;',
    'logos'                   => 'margin-right: 19px; height: 22px;',

    /* Body ------------------------------ */
    'body_action'             => 'width: 100%; margin: 30px auto; padding: 0; text-align: center;',
    'body_sub'                => 'margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;',

    /* Type ------------------------------ */
    'anchor'                  => 'color: #3869D4;',
    'header-1'                => 'margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;',
    'paragraph'               => 'font-size: 12pt; margin-top: 0; line-height: 1.25em;',
    'paragraph-sub'           => 'margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;',
    'paragraph-center'        => 'text-align: center;',
    'paragraph-footer'        => 'color: #FFF;',
    'link-footer'             => 'color: #3869D4; text-decoration: none;',

    /* Buttons ------------------------------ */
    'button'                  => 'width: 200px; min-height: 26px; padding: 15px; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px; text-align: center; text-decoration: none; -webkit-text-size-adjust: none; font-weight: bold;',
    'button--green'           => 'background-color: #22BC66;',
    'button--red'             => 'background-color: #dc4d2f;',
    'button--blue'            => 'background-color: #00BDC5;',
];
$fontFamily = 'font-family: Helvetica, sans-serif;';
?>

<body style="{{ $style['body'] }}">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="{{ $style['email-wrapper'] }}" align="center">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <!-- Logo -->
                    <tr style="{{ $style['email-masthead'] }}">
                        <td style="padding-left: 35px;">
                            <img src="https://www.eglowonline.com/public/img/email/logo.png" alt="eGLOW">
                        </td>
                        <td style="{{ $style['email-logos'] }}">
                            <table>
                                <tr>
                                    <td>
                                        <a href="https://www.facebook.com/eglowlatam/" target="_blank">
                                            <img src="https://www.eglowonline.com/public/img/email/facebook.png" alt="facebook" style="{{ $style['logos'] }}">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="https://www.instagram.com/eglow_latam/?hl=es" target="_blank">
                                            <img src="https://www.eglowonline.com/public/img/email/instagram.png" alt="instagram" style="{{ $style['logos'] }}">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="https://twitter.com/EglowLatam" target="_blank">
                                            <img src="https://www.eglowonline.com/public/img/email/twitter.png" alt="twitter" style="{{ $style['logos'] }}">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="https://www.linkedin.com/company-beta/10522915/" target="_blank">
                                            <img src="https://www.eglowonline.com/public/img/email/linkedin.png" alt="linkedin" style="{{ $style['logos'] }} margin-right: 0px;">
                                        </a>
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="{{ $style['blue-bar'] }}" colspan="2"></td>
                    </tr>

                    <!-- Email Body -->
                    <tr>
                        <td style="{{ $style['email-body'] }}" width="100%" colspan="2">
                            <table style="{{ $style['email-body_inner'] }}" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="{{ $fontFamily }} {{ $style['email-body_cell'] }}">
                                        <!-- Greeting -->
                                        <h1>
                                            @if (! empty($greeting))
                                                {{ $greeting }}
                                            @else
                                                @if ($level == 'error')
                                                    !Lo sentimos!
                                                @else
                                                    ¡Hola!
                                                @endif
                                            @endif
                                        </h1>

                                        <!-- Intro -->
                                        @foreach ($introLines as $line)
                                            <p style="{{ $style['paragraph'] }}">
                                                {{ $line }}
                                            </p>
                                        @endforeach

                                        <!-- Action Button -->
                                        @if (isset($actionText))
                                            <table style="{{ $style['body_action'] }}" align="center" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="center">
                                                        <?php
                                                            switch ($level) {
                                                                case 'success':
                                                                    $actionColor = 'button--green';
                                                                    break;
                                                                case 'error':
                                                                    $actionColor = 'button--red';
                                                                    break;
                                                                default:
                                                                    $actionColor = 'button--blue';
                                                            }
                                                        ?>

                                                        <a href="{{ str_replace('wwww', '', $actionUrl) }}"
                                                            style="{{ $fontFamily }} {{ $style['button'] }} {{ $style[$actionColor] }}"
                                                            class="button"
                                                            target="_blank">
                                                            {{ $actionText }}
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        @endif

                                        <!-- Outro -->
                                        @foreach ($outroLines as $line)
                                            <p style="{{ $style['paragraph'] }}">
                                                {{ $line }}
                                            </p>
                                        @endforeach

                                        <!-- Salutation -->
                                        <p style="{{ $style['paragraph'] }}">
                                            Saludos cordiales,<br>{{ config('app.name') }}
                                        </p>

                                        <!-- Sub Copy -->
                                        @if (isset($actionText))
                                            <table style="{{ $style['body_sub'] }}">
                                                <tr>
                                                    <td style="{{ $fontFamily }}">
                                                        <p style="{{ $style['paragraph-sub'] }}">
                                                            Si tienes problemas con el botón "{{ $actionText }}",
                                                            copia y pega el siguiente enlace en la URL del navegador:
                                                        </p>

                                                        <p style="{{ $style['paragraph-sub'] }}">
                                                            <a style="{{ $style['anchor'] }}" href="{{ str_replace('wwww', '', $actionUrl) }}" target="_blank">
                                                                {{ str_replace('wwww', '', $actionUrl) }}
                                                            </a>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <!-- Footer -->
                    <tr>
                        <td style="{{ $style['blue-bar'] }}" colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="{{ $style['email-footer-background'] }}">
                            <table style="{{ $style['email-footer'] }}" align="center" width="650" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="{{ $fontFamily }} {{ $style['email-footer_cell'] }}">
                                        <p style="{{ $style['paragraph-footer'] }}">
                                            &copy; {{ date('Y') }}
                                            <a style="{{ $style['link-footer'] }}" href="{{ str_replace('wwww', '', url('/')) }}" target="_blank">{{ config('app.name') }}</a>.
                                            Todos los derechos reservados.
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>