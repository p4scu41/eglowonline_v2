@extends('layouts.' . config('app.layout'))

@section('title', $title)

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li class="active">{{ $title }}</li>
    </ol>
@endsection

@section('content')
    {{-- Si se esta implementando Policy --}}
    @if ($policy)
        @can('create', $modelClass)
        <p>
            <a class="btn btn-red-rounded" href="{{ url($response_info['resource'] . '/create') }}" role="button">
                <span class="glyphicon glyphicon-plus-sign"></span> Registrar
            </a>
        </p>
        @endcan
    @else
        <p>
            <a class="btn btn-red-rounded" href="{{ url($response_info['resource'] . '/create') }}" role="button">
                <span class="glyphicon glyphicon-plus-sign"></span> Registrar
            </a>
        </p>
    @endif

    @include('common.errors', ['response_info' => $response_info])

    <div class="text-center">
        {{ $models->appends(Request::query())->links() }}
    </div>

    Mostrando {{ $models->firstItem() }}-{{ $models->lastItem() }} de {{ $models->total() }} elementos.

    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th style="width: 40px;" class="text-center"></th>
                <th>{{ $labels['campana_id'] }}</th>
                <th>{{ $labels['influenciador_id'] }}</th>
                <th>{{ $labels['red_social_id'] }}</th>
                <th>{{ $labels['created_at'] }}</th>
                <th>{{ $labels['estado'] }}</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 0; ?>
            @forelse ($models as $model)
                <tr>
                    <td class="text-center">{{ $models->firstItem() + ($i++) }}</td>
                    <td>{{ $model->campana->nombre }}</td>
                    <td>{{ $model->influenciador->nombre }}</td>
                    <td>{{ $model->redSocial->nombre }}</td>
                    <td>@datetime($model->created_at)</td>
                    <td>{{ $model->descripcionEstado }}</td>
                    <td class="text-center">
                        @include('common.options', [
                            'policy' => $policy,
                            'modelClass' => $modelClass,
                            'model' => $model,
                            'url' => url($response_info['resource'] . '/' . $model->id)
                        ])
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="7">No se encontrarón datos.</td>
                </tr>
            @endforelse

        </tbody>
    </table>

    <div class="text-center">
        {{ $models->appends(Request::query())->links() }}
    </div>
@endsection
