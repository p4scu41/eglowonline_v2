@extends('layouts.' . config('app.layout'))

@section('title', $title)

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li><a href="{{ url($response_info['resource']) }}">{{ $title }}</a></li>
        <li class="active">@yield('section_title')</li>
    </ol>
@endsection

@section('content')
    <p>
        <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
            <span class="glyphicon glyphicon-arrow-left"></span> Regresar
        </a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Datos del {{ $title }} </h3>
        </div>
        <div class="panel-body">
            @include('common.errors', ['response_info' => $response_info])

            {!! BootForm::horizontal($config_form) !!}


                {!! BootForm::select('campana_id', $labels['campana_id'],
                        $catalogs['campana'], null, [
                            'data-rule-required' => 'true',
                            {{-- 'class' => 'select2', --}}
                        ]
                    )
                !!}

                {!! BootForm::select('influenciador_id', $labels['influenciador_id'],
                        $catalogs['influenciador'], null, [
                            'data-rule-required' => 'true',
                            {{-- 'class' => 'select2', --}}
                        ]
                    )
                !!}

                {!! BootForm::select('red_social_id', $labels['red_social_id'],
                        $catalogs['red_social'], null, [
                            'data-rule-required' => 'true',
                            {{-- 'class' => 'select2', --}}
                        ]
                    )
                !!}

                {!! BootForm::select('activo', $labels['activo'],
                        ['' => '', '1' => 'Si', '0' => 'No'], null, [
                            'data-rule-required' => 'true',
                        ]
                    )
                !!}

                {!! BootForm::select('estado', $labels['estado'],
                        [
                            0 => 'Esperando Aprobación - Influencer',
                            1 => 'Por Publicar',
                            2 => 'Publicado',
                            3 => 'Esperando Aprobación de Modificación  - Agencia',
                            4 => 'Rechazado',
                            5 => 'Error al publicar',
                        ], null,
                        [
                            'data-rule-required' => 'true',
                            {{-- 'class' => 'select2', --}}
                        ]
                    )
                !!}

                {!! BootForm::file('archivo_multimedia', $labels['archivo_multimedia'], [
                        'accept' => 'image/*,video/*,audio/*',
                    ])
                !!}

                {!! BootForm::textarea('contenido', $labels['contenido'], null, [
                        'data-rule-required' => 'true',
                        'data-rule-minlength' => '3',
                        'div' => 'col-xs-12 col-sm-12 col-md-12',
                        'right_column_class' => 'col-xs-8 col-sm-10 col-md-8',
                        'left_column_class' => 'col-xs-4 col-sm-2 col-md-2',
                        'rows' => 4,
                    ])
                !!}

                {!! BootForm::button('<span class="glyphicon glyphicon-floppy-save"></span> Guardar', [
                        'type'                     => 'submit',
                        'class'                    => 'btn btn-red-rounded',
                        'div_form_group_class'     => 'col-xs-12 col-sm-12 col-md-12',
                        'right_column_class'       => 'col-xs-12 col-sm-12 col-md-12',
                        'left_column_offset_class' => 'text-center',
                    ])
                !!}

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection
