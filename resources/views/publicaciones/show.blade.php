@extends('layouts.' . config('app.layout'))

@section('title', $title)

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li><a href="{{ url($response_info['resource']) }}">{{ $title }}</a></li>
        <li class="active">Ver</li>
    </ol>
@endsection

@section('content')
    <p>
        <a class="btn btn-default" href="{{ url($response_info['resource']) }}" role="button">
            <span class="glyphicon glyphicon-arrow-left"></span> Regresar
        </a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Datos del {{ $title }} </h3>
        </div>
        <div class="panel-body">
            @include('common.errors', ['response_info' => $response_info])

            <div class="col-md-8 col-md-offset-2">
            @if ($model)
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td><label>{{ $labels['campana_id'] }}</label></td>
                            <td>{{ $model->campana->nombre }}</td>
                        </tr>
                        <tr>
                            <td><label>{{ $labels['influenciador_id'] }}</label></td>
                            <td>{{ $model->influenciador->nombre }}</td>
                        </tr>
                        <tr>
                            <td><label>{{ $labels['red_social_id'] }}</label></td>
                            <td>{{ $model->redSocial->nombre }}</td>
                        </tr>
                        <tr>
                            <td><label>{{ $labels['contenido'] }}</label></td>
                            <td>{{ $model->contenido }}</td>
                        </tr>
                        <tr>
                            <td><label>{{ $labels['created_at'] }}</label></td>
                            <td>@datetime($model->created_at)</td>
                        </tr>
                        <tr>
                            <td><label>{{ $labels['estado'] }}</label></td>
                            <td>{{ $model->descripcionEstado }}</td>
                        </tr>
                        <tr>
                            <td><label>{{ $labels['archivo_multimedia'] }}</label></td>
                            <td><img src="{{ asset($model->getArchivoMultimedia()) }}" class="logotipo"></td>
                        </tr>
                    </tbody>
                </table>
            @endif
            </div>

        </div>
    </div>
@endsection
